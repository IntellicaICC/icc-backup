package net.intellica.icc.template.onetoone;

import java.sql.JDBCType;
import java.util.Arrays;
import java.util.Objects;
import javax.cache.Cache.Entry;
import net.intellica.icc.template.exception.ValidationException;
import net.intellica.icc.template.model.CacheProcessor;
import net.intellica.icc.template.model.Row;
import net.intellica.icc.template.model.TemplateJob;

public class OneToOneJob extends TemplateJob {

	// CONSTANTS
	private static final long serialVersionUID = 1L;

	// TEMPLATE VARIABLES
	public static final String FIRST_DATASET = "FirstDataSet";
	public static final String SECOND_DATASET = "SecondDataSet";
	public static final String REPORT_IDENTICAL = "ReportIdentical";

	// REPORT COLUMNS
	public static final String COLUMN_ID = "ID";
	public static final String COLUMN_KEY = "KEY";
	public static final String COLUMN_FIRST_COLUMN = "FIRST_COLUMN";
	public static final String COLUMN_FIRST_VALUE = "FIRST_VALUE";
	public static final String COLUMN_SECOND_COLUMN = "SECOND_COLUMN";
	public static final String COLUMN_SECOND_VALUE = "SECOND_VALUE";

	// REPORT COLUMN TYPES
	public static final JDBCType TYPE_ID = JDBCType.NUMERIC;
	public static final JDBCType TYPE_KEY = JDBCType.VARCHAR;
	public static final JDBCType TYPE_FIRST_COLUMN = JDBCType.VARCHAR;
	public static final JDBCType TYPE_FIRST_VALUE = JDBCType.VARCHAR;
	public static final JDBCType TYPE_SECOND_COLUMN = JDBCType.VARCHAR;
	public static final JDBCType TYPE_SECOND_VALUE = JDBCType.VARCHAR;

	// REPORT COLUMN LENGTHS
	public static final Integer LENGTH_ID = 0;
	public static final Integer LENGTH_KEY = 2000;
	public static final Integer LENGTH_FIRST_COLUMN = 2000;
	public static final Integer LENGTH_FIRST_VALUE = 2000;
	public static final Integer LENGTH_SECOND_COLUMN = 2000;
	public static final Integer LENGTH_SECOND_VALUE = 2000;

	// RESULTS
	public static final String RESULT_COMPARE_COUNT = "CompareCount";
	public static final String RESULT_DIFFERENT_COUNT = "DifferentCount";
	public static final String RESULT_SAME_COUNT = "SameCount";

	// GRID VARIABLES
	public static final String VARIABLE_INDEX = "INDEX";

	// INTERNAL VARIABLES
	private Boolean reportIdentical;

	public OneToOneJob(Object arg) {
		super(arg);
	}

	@Override
	protected void validatialization() throws Exception {
		if (!cacheExists(getVariable(FIRST_DATASET)))
			throw new ValidationException(null, "First dataset does not exist!");
		if (!cacheExists(getVariable(SECOND_DATASET)))
			throw new ValidationException(null, "Second dataset does not exist!");
		reportIdentical = Boolean.parseBoolean(getVariable(REPORT_IDENTICAL));
		createGridLong(VARIABLE_INDEX, 1L);
		createGridLong(RESULT_COMPARE_COUNT, 0L);
		createGridLong(RESULT_DIFFERENT_COUNT, 0L);
		createGridLong(RESULT_SAME_COUNT, 0L);
	}

	@Override
	public void run() throws Exception {
		localCacheRun(getVariable(FIRST_DATASET), new FirstProcessor());
		localCacheRun(getVariable(SECOND_DATASET), new SecondProcessor());
	}

	public class FirstProcessor implements CacheProcessor {
		@Override
		public void process(Iterable<Entry<Object, Object>> entries) {
			logDebugMessage("Starting iteration of first dataset...");
			for (Entry<Object, Object> entry : entries) {
				String key = (String)entry.getKey();
				Row firstRow = (Row)entry.getValue();
				Row secondRow = (Row)getFromCache(getVariable(SECOND_DATASET), key);
				if (secondRow == null || !secondRow.equals(firstRow)) {
					incrementAndGetGridLong(RESULT_DIFFERENT_COUNT);
					incrementAndGetGridLong(RESULT_COMPARE_COUNT);
					Row row = createNewRow();
					if (!reportIdentical) {
						String firstColumn = "";
						Object firstValue = null;
						String secondColumn = "";
						Object secondValue = null;
						if (secondRow == null) {
							firstColumn = firstRow.getRowMetadata().getColumns().get(0);
							firstValue = firstRow.get(0);
						}
						else {
							Integer maxColumnSize = Math.max(firstRow.getRowMetadata().getColumns().size(), secondRow.getRowMetadata().getColumns().size());
							Object[] firstRowColumns = Arrays.copyOf(firstRow.getRowMetadata().getColumns().toArray(), maxColumnSize);
							Object[] firstRowValues = Arrays.copyOf(firstRow.getValues().toArray(), maxColumnSize);
							Object[] secondRowColumns = Arrays.copyOf(secondRow.getRowMetadata().getColumns().toArray(), maxColumnSize);
							Object[] secondRowValues = Arrays.copyOf(secondRow.getValues().toArray(), maxColumnSize);
							for (int i = 0; i < maxColumnSize; i++) {
								if (!Objects.equals(firstRowColumns[i], secondRowColumns[i]) ||
									!Objects.equals(firstRowValues[i], secondRowValues[i])) {
									firstColumn = (String)firstRowColumns[i];
									firstValue = firstRowValues[i];
									secondColumn = (String)secondRowColumns[i];
									secondValue = secondRowValues[i];
									break;
								}
							}
						}
						row.put(COLUMN_ID, getAndIncrementGridLong(VARIABLE_INDEX));
						row.put(COLUMN_KEY, key);
						row.put(COLUMN_FIRST_COLUMN, firstColumn);
						row.put(COLUMN_FIRST_VALUE, firstValue);
						row.put(COLUMN_SECOND_COLUMN, secondColumn);
						row.put(COLUMN_SECOND_VALUE, secondValue);
						putIntoCache(key, row);
					}
				}
				else {
					incrementAndGetGridLong(RESULT_SAME_COUNT);
					incrementAndGetGridLong(RESULT_COMPARE_COUNT);
					if (reportIdentical) {
						Row row = createNewRow();
						row.put(COLUMN_ID, getAndIncrementGridLong(VARIABLE_INDEX));
						row.put(COLUMN_KEY, key);
						row.put(COLUMN_FIRST_COLUMN, "");
						row.put(COLUMN_FIRST_VALUE, "");
						row.put(COLUMN_SECOND_COLUMN, "");
						row.put(COLUMN_SECOND_VALUE, "");
						putIntoCache(key, row);
					}
				}
			}
			logDebugMessage("Finished iteration of first dataset.");
		}
	}

	public class SecondProcessor implements CacheProcessor {
		@Override
		public void process(Iterable<Entry<Object, Object>> entries) {
			logDebugMessage("Starting iteration of second dataset...");
			for (Entry<Object, Object> entry : entries) {
				String key = (String)entry.getKey();
				if (!cacheKeyExists(getVariable(FIRST_DATASET), key)) {
					incrementAndGetGridLong(RESULT_COMPARE_COUNT);
					incrementAndGetGridLong(RESULT_DIFFERENT_COUNT);
					if (!reportIdentical) {
						Row secondRow = (Row)entry.getValue();
						Row row = createNewRow();
						String firstColumn = "";
						Object firstValue = null;
						String secondColumn = secondRow.getRowMetadata().getColumns().get(0);
						Object secondValue = secondRow.get(0);
						row.put(COLUMN_ID, getAndIncrementGridLong(VARIABLE_INDEX));
						row.put(COLUMN_KEY, key);
						row.put(COLUMN_FIRST_COLUMN, firstColumn);
						row.put(COLUMN_FIRST_VALUE, firstValue);
						row.put(COLUMN_SECOND_COLUMN, secondColumn);
						row.put(COLUMN_SECOND_VALUE, secondValue);
						putIntoCache(key, row);
					}
				}
			}
			logDebugMessage("Finished iteration of second dataset.");
		}
	}

	private Row createNewRow() {
		Row result = new Row();
		result
			.addColumn(COLUMN_ID, TYPE_ID, LENGTH_ID, null)
			.addColumn(COLUMN_KEY, TYPE_KEY, LENGTH_KEY, null)
			.addColumn(COLUMN_FIRST_COLUMN, TYPE_FIRST_COLUMN, LENGTH_FIRST_COLUMN, null)
			.addColumn(COLUMN_FIRST_VALUE, TYPE_FIRST_VALUE, LENGTH_FIRST_VALUE, null)
			.addColumn(COLUMN_SECOND_COLUMN, TYPE_SECOND_COLUMN, LENGTH_SECOND_COLUMN, null)
			.addColumn(COLUMN_SECOND_VALUE, TYPE_SECOND_VALUE, LENGTH_SECOND_VALUE, null);
		result.getKeyColumns().add(COLUMN_KEY);
		return result;
	}

	@Override
	protected void setResults() {
		putIntoResultCache(RESULT_COMPARE_COUNT, getGridLong(RESULT_COMPARE_COUNT));
		putIntoResultCache(RESULT_DIFFERENT_COUNT, getGridLong(RESULT_DIFFERENT_COUNT));
		putIntoResultCache(RESULT_SAME_COUNT, getGridLong(RESULT_SAME_COUNT));
	}

	@Override
	protected void cleanUp() {
		removeGridLong(VARIABLE_INDEX);
		removeGridLong(RESULT_COMPARE_COUNT);
		removeGridLong(RESULT_DIFFERENT_COUNT);
		removeGridLong(RESULT_SAME_COUNT);
	}

}



import net.intellica.icc.template.model.TemplateJob;
import net.intellica.icc.template.model.TemplateTask;
import net.intellica.icc.template.onetoone.OneToOneJob;

public class OneToOne extends TemplateTask {

	private static final long serialVersionUID = 1L;

	@Override
	protected TemplateJob getJob(Object arg) {
		return new OneToOneJob(arg);
	}

}

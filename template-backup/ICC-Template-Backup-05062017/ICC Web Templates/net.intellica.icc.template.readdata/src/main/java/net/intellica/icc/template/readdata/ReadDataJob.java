package net.intellica.icc.template.readdata;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import net.intellica.icc.template.model.TemplateJob;
import org.apache.ignite.IgniteException;
import org.springframework.util.StringUtils;

public class ReadDataJob extends TemplateJob {

	// CONSTANTS
	private static final long serialVersionUID = 1L;

	// TEMPLATE VARIABLES
	public static final String CONNECTION = "Connection";
	public static final String QUERY = "Query";
	public static final String KEYS = "Keys";

	// RESULTS
	public static final String RESULT_READ_COUNT = "ReadCount";

	// GRID VARIABLES
	public static final String VARIABLE_INDEX = "INDEX";

	// INITIALIZED VARIABLES
	private String query;
	private Set<String> keys;

	public ReadDataJob(Object arg) {
		super(arg);
	}

	@Override
	protected void validatialization() throws Exception {
		query = getVariable(QUERY).trim();
		keys = Arrays.asList(getVariable(KEYS).split(",", -1)).stream()
			.filter(s -> !StringUtils.isEmpty(s))
			.collect(Collectors.toSet());
		createGridLong(VARIABLE_INDEX, 0L);
	}

	@Override
	public void run() throws Exception {
		iterateQuery(getVariable(CONNECTION), query, row -> {
			incrementAndGetGridLong(VARIABLE_INDEX);
			row.getKeyColumns().addAll(keys);
			String keyString = row.getKeyString();
			if (row.getKeyValues().stream().anyMatch(v -> v == null))
				throw new IgniteException("Null key detected! (" + keyString + ")");
			else if (cacheKeyExists(keyString))
				throw new IgniteException("Duplicate key detected! (" + keyString + ")");
			putIntoCache(keyString, row);
		});
	}

	@Override
	protected void setResults() {
		putIntoResultCache(RESULT_READ_COUNT, getGridLong(VARIABLE_INDEX));
	}

	@Override
	protected void cleanUp() {
		removeGridLong(VARIABLE_INDEX);
	}

}

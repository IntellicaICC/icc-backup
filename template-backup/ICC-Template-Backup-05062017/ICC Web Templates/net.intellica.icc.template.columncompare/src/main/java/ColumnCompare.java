

import net.intellica.icc.template.columndifference.ColumnCompareJob;
import net.intellica.icc.template.model.TemplateJob;
import net.intellica.icc.template.model.TemplateTask;

public class ColumnCompare extends TemplateTask {

	private static final long serialVersionUID = 1L;

	@Override
	protected TemplateJob getJob(Object arg) {
		return new ColumnCompareJob(arg);
	}

}

package net.intellica.icc.template.columndifference;

import java.sql.JDBCType;
import java.util.Arrays;
import java.util.List;
import javax.cache.Cache.Entry;
import net.intellica.icc.template.exception.ValidationException;
import net.intellica.icc.template.model.CacheProcessor;
import net.intellica.icc.template.model.Row;
import net.intellica.icc.template.model.TemplateJob;

public class ColumnCompareJob extends TemplateJob {

	// CONSTANTS
	private static final long serialVersionUID = 1L;
	private static final List<String> ALLOWED_OPERATORS = Arrays.asList(
		">",
		">=",
		"<",
		"<=",
		"<>",
		"="//
	);

	// PARAMETERS
	public static final String FIRST_DATASET = "FirstDataSet";
	public static final String FIRST_COLUMN = "FirstColumn";
	public static final String SECOND_DATASET = "SecondDataSet";
	public static final String SECOND_COLUMN = "SecondColumn";
	public static final String OPERATOR = "Operator";

	// REPORT COLUMNS
	public static final String COLUMN_ID = "ID";
	public static final String COLUMN_KEY = "KEY";
	public static final String COLUMN_FIRST_COLUMN = "FIRST_COLUMN";
	public static final String COLUMN_FIRST_VALUE = "FIRST_VALUE";
	public static final String COLUMN_SECOND_COLUMN = "SECOND_COLUMN";
	public static final String COLUMN_SECOND_VALUE = "SECOND_VALUE";
	public static final String COLUMN_OPERATOR = "OPERATOR";

	// REPORT COLUMN TYPES
	public static final JDBCType TYPE_ID = JDBCType.NUMERIC;
	public static final JDBCType TYPE_KEY = JDBCType.VARCHAR;
	public static final JDBCType TYPE_FIRST_COLUMN = JDBCType.VARCHAR;
	public static final JDBCType TYPE_FIRST_VALUE = JDBCType.VARCHAR;
	public static final JDBCType TYPE_SECOND_COLUMN = JDBCType.VARCHAR;
	public static final JDBCType TYPE_SECOND_VALUE = JDBCType.VARCHAR;
	public static final JDBCType TYPE_OPERATOR = JDBCType.VARCHAR;

	// REPORT COLUMN LENGTHS
	public static final Integer LENGTH_ID = 0;
	public static final Integer LENGTH_KEY = 2000;
	public static final Integer LENGTH_FIRST_COLUMN = 2000;
	public static final Integer LENGTH_FIRST_VALUE = 2000;
	public static final Integer LENGTH_SECOND_COLUMN = 2000;
	public static final Integer LENGTH_SECOND_VALUE = 2000;
	public static final Integer LENGTH_OPERATOR = 2000;

	// TEMPLATE RESULTS
	public static final String RESULT_MATCH_COUNT = "MatchCount";

	// GRID VARIABLES
	public static final String VARIABLE_INDEX = "INDEX";

	// INTERNAL VARIABLES
	private String firstColumn;
	private String secondColumn;
	private String operator;

	public ColumnCompareJob(Object arg) {
		super(arg);
	}

	@Override
	protected void validatialization() throws Exception {
		if (!cacheExists(getVariable(FIRST_DATASET)))
			throw new ValidationException(null, "First dataset does not exist!");
		firstColumn = getVariable(FIRST_COLUMN).trim();
		Row firstSample = (Row)getSampleFromCache(getVariable(FIRST_DATASET));
		if (!firstSample.getRowMetadata().getColumns().stream().anyMatch(c -> c.equalsIgnoreCase(firstColumn)))
			throw new ValidationException(null, "First dataset does not contain a column named " + firstColumn + "!");
		if (!cacheExists(getVariable(SECOND_DATASET)))
			throw new ValidationException(null, "Second dataset does not exist!");
		secondColumn = getVariable(SECOND_COLUMN).trim();
		Row secondSample = (Row)getSampleFromCache(getVariable(SECOND_DATASET));
		if (!secondSample.getRowMetadata().getColumns().stream().anyMatch(c -> c.equalsIgnoreCase(secondColumn)))
			throw new ValidationException(null, "Second dataset does not contain a column named " + secondColumn + "!");
		operator = getVariable(OPERATOR).trim();
		if (!ALLOWED_OPERATORS.contains(operator))
			throw new ValidationException(null, "Allowed operators are " + String.join(",", ALLOWED_OPERATORS) + ".");
		createGridLong(VARIABLE_INDEX, 1L);
	}

	@Override
	public void run() throws Exception {
		localCacheRun(getVariable(FIRST_DATASET), new DataProcessor());
	}

	public class DataProcessor implements CacheProcessor {
		@Override
		public void process(Iterable<Entry<Object, Object>> entries) {
			logDebugMessage("Starting iteration of dataset...");
			for (Entry<Object, Object> entry : entries) {
				String key = (String)entry.getKey();
				Row firstRow = (Row)entry.getValue();
				Row secondRow = (Row)getFromCache(getVariable(SECOND_DATASET), key);
				if (secondRow != null) {
					Object firstValue = firstRow.get(firstColumn);
					Object secondValue = secondRow.get(secondColumn);
					if (isMatch(firstValue, secondValue)) {
						String firstValueString = firstValue == null ? null : firstValue.toString();
						String secondValueString = secondValue == null ? null : secondValue.toString();
						Row matchedRow = createNewRow();
						matchedRow.put(COLUMN_ID, getAndIncrementGridLong(VARIABLE_INDEX));
						matchedRow.put(COLUMN_KEY, key);
						matchedRow.put(COLUMN_FIRST_COLUMN, firstColumn);
						matchedRow.put(COLUMN_FIRST_VALUE, firstValueString);
						matchedRow.put(COLUMN_SECOND_COLUMN, secondColumn);
						matchedRow.put(COLUMN_SECOND_VALUE, secondValueString);
						matchedRow.put(COLUMN_OPERATOR, operator);
						putIntoCache(matchedRow.getKeyString(), matchedRow);
					}
				}
			}
			logDebugMessage("Finished iteration of dataset.");
		}
	}

	private Boolean isMatch(Object firstValue, Object secondValue) {
		Boolean result = false;
		if (firstValue == null && secondValue == null) {
			switch (operator) {
				case "=":
					result = true;
					break;
				default:
					break;
			}
		}
		else if (firstValue != null && firstValue instanceof Comparable<?> && secondValue instanceof Comparable<?>) {
			switch (operator) {
				case ">":
					result = ((Comparable<Object>)firstValue).compareTo((Comparable<Object>)secondValue) > 0;
					break;
				case ">=":
					result = ((Comparable<Object>)firstValue).compareTo((Comparable<Object>)secondValue) >= 0;
					break;
				case "<":
					result = ((Comparable<Object>)firstValue).compareTo((Comparable<Object>)secondValue) < 0;
					break;
				case "<=":
					result = ((Comparable<Object>)firstValue).compareTo((Comparable<Object>)secondValue) <= 0;
					break;
				case "<>":
					result = ((Comparable<Object>)firstValue).compareTo((Comparable<Object>)secondValue) != 0;
					break;
				case "=":
					result = ((Comparable<Object>)firstValue).compareTo((Comparable<Object>)secondValue) == 0;
					break;
				default:
					break;
			}
		}
		return result;
	}

	private Row createNewRow() {
		Row result = new Row();
		result
			.addColumn(COLUMN_ID, TYPE_ID, LENGTH_ID, null)
			.addColumn(COLUMN_KEY, TYPE_KEY, LENGTH_KEY, null)
			.addColumn(COLUMN_FIRST_COLUMN, TYPE_FIRST_COLUMN, LENGTH_FIRST_COLUMN, null)
			.addColumn(COLUMN_FIRST_VALUE, TYPE_FIRST_VALUE, LENGTH_FIRST_VALUE, null)
			.addColumn(COLUMN_SECOND_COLUMN, TYPE_SECOND_COLUMN, LENGTH_SECOND_COLUMN, null)
			.addColumn(COLUMN_SECOND_VALUE, TYPE_SECOND_VALUE, LENGTH_SECOND_VALUE, null)
			.addColumn(COLUMN_OPERATOR, TYPE_OPERATOR, LENGTH_OPERATOR, null);
		result.getKeyColumns().add(COLUMN_KEY);
		return result;
	}

	@Override
	protected void setResults() {
		putIntoResultCache(RESULT_MATCH_COUNT, getGridLong(VARIABLE_INDEX) - 1);
	}

	@Override
	protected void cleanUp() {
		removeGridLong(VARIABLE_INDEX);
	}

}

package net.intellica.icc.template.writedata;

import java.sql.JDBCType;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import javax.cache.Cache.Entry;
import net.intellica.icc.template.exception.ValidationException;
import net.intellica.icc.template.model.Constants;
import net.intellica.icc.template.model.GlobalVariable;
import net.intellica.icc.template.model.Row;
import net.intellica.icc.template.model.TemplateJob;
import org.springframework.dao.DataAccessException;
import org.springframework.util.StringUtils;

public class WriteDataJob extends TemplateJob {

	// CONSTANTS
	private static final long serialVersionUID = 1L;
	private static final Integer BATCH_SIZE = 50000;
	private static final Integer REPORT_VARCHAR_LENGTH = 2000;
	private static final Integer REPORT_NUMBER_LENGTH = 0;

	// TEMPLATE VARIABLES
	public static final String DATASET = "DataSet";
	public static final String CONNECTION = "Connection";
	public static final String TABLE = "Table";
	public static final String DROP_BEFORE_CREATE = "DropBeforeCreate";
	public static final String REPORT_FORMAT = "ReportFormat";

	// REPORT COLUMNS
	public static final String COLUMN_REPORT_ID = "REPORT_ID";
	public static final String COLUMN_REPORT_DATE = "REPORT_DATE";
	public static final String COLUMN_REPORT_USER = "REPORT_USER";

	// REPORT COLUMN TYPES
	public static final JDBCType TYPE_REPORT_ID = JDBCType.NUMERIC;
	public static final JDBCType TYPE_REPORT_DATE = JDBCType.TIMESTAMP;
	public static final JDBCType TYPE_REPORT_USER = JDBCType.VARCHAR;

	// REPORT COLUMN LENGTHS
	public static final Integer LENGTH_REPORT_ID = 0;
	public static final Integer LENGTH_REPORT_DATE = 0;
	public static final Integer LENGTH_REPORT_USER = 2000;

	// RESULTS
	public static final String RESULT_TABLE_EXISTS = "TableExists";
	public static final String RESULT_TABLE_DROPPED = "TableDropped";
	public static final String RESULT_TABLE_CREATED = "TableCreated";
	public static final String RESULT_WRITE_COUNT = "WriteCount";

	// INTERNAL VARIABLES
	private String dataset;
	private String table;
	private Integer writeCount;
	private Boolean tableExists;
	private Boolean tableDropped;
	private Boolean tableCreated;
	private Row reportRow;

	public WriteDataJob(Object arg) {
		super(arg);
	}

	@Override
	protected void validatialization() throws Exception {
		dataset = getVariable(DATASET);
		if (StringUtils.isEmpty(dataset))
			dataset = getLastDataset();
		if (!cacheExists(dataset))
			throw new ValidationException(null, "Dataset does not exist!");
		table = getVariable(TABLE).trim();
		if (StringUtils.isEmpty(table))
			throw new ValidationException(null, "Table can not be left empty!");
		try {
			validateTableName(getVariable(CONNECTION), table);
		}
		catch (Exception e) {
			validateQualifiedTableName(getVariable(CONNECTION), table);
		}
		writeCount = 0;
		tableExists = false;
		tableDropped = false;
		tableCreated = false;
	}

	@Override
	public void run() throws Exception {
		Boolean createTable = false;
		try {
			queryCount(getVariable(CONNECTION), table);
			tableExists = true;
			logDebugMessage("Table '%s' exists.", table);
		}
		catch (DataAccessException e) {
			tableExists = false;
			logDebugMessage("Table '%s' does not exist.", table);
		}

		if (tableExists && Boolean.parseBoolean(getVariable(DROP_BEFORE_CREATE))) {
			dropTable(getVariable(CONNECTION), table);
			tableDropped = true;
			logDebugMessage("Table '%s' dropped.", table);
		}

		if (!tableExists || tableDropped)
			createTable = true;

		Iterator<Entry<Object, Object>> iterator = iterateCache(dataset);
		Row[] batch = new Row[BATCH_SIZE];
		Integer count = 0;
		while (iterator.hasNext()) {
			checkCanceled();
			Entry<Object, Object> entry = iterator.next();
			Row row = (Row)entry.getValue();
			if (Boolean.parseBoolean(getVariable(REPORT_FORMAT))) {
				Row reportRow = createReportRow();
				for (int i = 0; i < row.getRowMetadata().getColumns().size(); i++) {
					Integer columnLength = row.getRowMetadata().getLengths().get(i);
					JDBCType columnType = row.getRowMetadata().getTypes().get(i);
					columnLength = selectColumnLength(columnType, columnLength);
					reportRow.addColumn(
						row.getRowMetadata().getColumns().get(i),
						row.getRowMetadata().getTypes().get(i),
						columnLength,
						row.get(i)//
					);
				}
				reportRow.getKeyColumns().addAll(row.getKeyColumns());
				row = reportRow;
			}

			if (createTable && !tableCreated) {
				createTable(getVariable(CONNECTION), table, row);
				tableCreated = true;
				logDebugMessage("Table '%s' created.", table);
			}
			batch[count++] = row;
			if (BATCH_SIZE.equals(count)) {
				batchInsertToTable(getVariable(CONNECTION), table, batch);
				logDebugMessage("Batch inserted %d records into table '%s'.", count, table);
				writeCount += count;
				count = 0;
			}
			else if (!iterator.hasNext()) {
				Row[] copy = Arrays.copyOf(batch, count);
				batchInsertToTable(getVariable(CONNECTION), table, copy);
				writeCount += count;
				logDebugMessage("Batch inserted final %d records into table '%s'.", count, table);
			}
		}
	}

	private Row createReportRow() {
		return new Row(getReportRow());
	}

	private Row getReportRow() {
		if (reportRow == null) {
			reportRow = new Row();
			reportRow
				.addColumn(COLUMN_REPORT_ID, TYPE_REPORT_ID, LENGTH_REPORT_ID, Math.abs(new Random().nextLong()))
				.addColumn(COLUMN_REPORT_DATE, TYPE_REPORT_DATE, LENGTH_REPORT_DATE, new Timestamp(new Date().getTime()))
				.addColumn(COLUMN_REPORT_USER, TYPE_REPORT_USER, LENGTH_REPORT_USER, getParameter(GlobalVariable.Username.toString()));
			reportRow.getKeyColumns().add(COLUMN_REPORT_ID);
		}
		return reportRow;
	}

	private Integer selectColumnLength(JDBCType type, Integer length) {
		Integer result = length;
		if (Constants.JDBC.CHAR_TYPES.contains(type))
			result = REPORT_VARCHAR_LENGTH;
		else if (Constants.JDBC.NUMERIC_TYPES.contains(type))
			result = REPORT_NUMBER_LENGTH;
		return result;
	}

	@Override
	protected void setResults() {
		putIntoResultCache(RESULT_WRITE_COUNT, writeCount);
		putIntoResultCache(RESULT_TABLE_EXISTS, tableExists);
		putIntoResultCache(RESULT_TABLE_DROPPED, tableDropped);
		putIntoResultCache(RESULT_TABLE_CREATED, tableCreated);
	}

}

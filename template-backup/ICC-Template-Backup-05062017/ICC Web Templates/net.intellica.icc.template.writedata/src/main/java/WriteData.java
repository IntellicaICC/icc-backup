

import net.intellica.icc.template.model.TemplateJob;
import net.intellica.icc.template.model.TemplateTask;
import net.intellica.icc.template.writedata.WriteDataJob;

public class WriteData extends TemplateTask {

	private static final long serialVersionUID = 1L;

	@Override
	protected TemplateJob getJob(Object arg) {
		return new WriteDataJob(arg);
	}

}

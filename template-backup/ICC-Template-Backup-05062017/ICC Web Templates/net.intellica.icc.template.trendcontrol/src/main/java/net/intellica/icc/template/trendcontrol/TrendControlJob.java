
package net.intellica.icc.template.trendcontrol;

import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.JDBCType;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import javax.cache.Cache.Entry;
import net.intellica.icc.template.exception.ValidationException;
import net.intellica.icc.template.model.CacheProcessor;
import net.intellica.icc.template.model.Row;
import net.intellica.icc.template.model.TemplateJob;
import org.apache.ignite.IgniteException;
import org.springframework.util.StringUtils;

public class TrendControlJob extends TemplateJob {

	// CONSTANTS
	private static final long serialVersionUID = 1L;
	private static final Integer MIN_EXTENT = 1;
	private static final Integer MAX_EXTENT = 365;
	private static final List<String> ALLOWED_OPERATORS = Arrays.asList(
		">",
		">=",
		"<",
		"<=",
		"<>",
		"="//
	);

	// TEMPLATE VARIABLES
	public static final String DATASET = "DataSet";
	public static final String TREND_COLUMN = "TrendColumn";
	public static final String DATE_COLUMN = "DateColumn";
	public static final String THRESHOLD_VALUE = "ThresholdValue";
	public static final String THRESHOLD_PERCENTAGE = "ThresholdPercentage";
	public static final String EXTENT = "Extent";
	public static final String OPERATOR = "Operator";

	// REPORT COLUMNS
	public static final String COLUMN_ID = "ID";
	public static final String COLUMN_DATE1 = "DATE1";
	public static final String COLUMN_VALUE1 = "VALUE1";
	public static final String COLUMN_DATE2 = "DATE2";
	public static final String COLUMN_VALUE2 = "VALUE2";
	public static final String COLUMN_DIFFERENCE = "DIFFERENCE";
	public static final String COLUMN_PERCENT_DIFFERENCE = "PERCENT_DIFFERENCE";

	// REPORT COLUMN TYPES
	public static final JDBCType TYPE_ID = JDBCType.NUMERIC;
	public static final JDBCType TYPE_DATE1 = JDBCType.DATE;
	public static final JDBCType TYPE_VALUE1 = JDBCType.NUMERIC;
	public static final JDBCType TYPE_DATE2 = JDBCType.DATE;
	public static final JDBCType TYPE_VALUE2 = JDBCType.NUMERIC;
	public static final JDBCType TYPE_DIFFERENCE = JDBCType.NUMERIC;
	public static final JDBCType TYPE_PERCENT_DIFFERENCE = JDBCType.NUMERIC;

	// REPORT COLUMN LENGTHS
	public static final Integer LENGTH_ID = 0;
	public static final Integer LENGTH_DATE1 = 0;
	public static final Integer LENGTH_VALUE1 = 0;
	public static final Integer LENGTH_DATE2 = 0;
	public static final Integer LENGTH_VALUE2 = 0;
	public static final Integer LENGTH_DIFFERENCE = 0;
	public static final Integer LENGTH_PERCENT_DIFFERENCE = 0;

	// RESULTS
	public static final String RESULT_THRESHOLD_EXCEEDED = "ThresholdExceeded";
	public static final String RESULT_EXCEED_COUNT = "ExceedCount";

	// GRID VARIABLES
	public static final String VARIABLE_INDEX = "INDEX";

	// INTERNAL VARIABLES
	private String dataset;
	private String trendColumn;
	private String dateColumn;
	private BigDecimal thresholdValue;
	private BigDecimal thresholdPercentage;
	private Integer extent;
	private String operator;
	private LocalDate minDate;

	public TrendControlJob(Object arg) {
		super(arg);
	}

	@Override
	protected void validatialization() throws Exception {
		dataset = getVariable(DATASET);
		if (StringUtils.isEmpty(dataset))
			dataset = getLastDataset();
		if (!cacheExists(dataset))
			throw new ValidationException(null, "Dataset does not exist!");
		trendColumn = getVariable(TREND_COLUMN).trim();
		dateColumn = getVariable(DATE_COLUMN).trim();
		Row sample = (Row)getSampleFromCache(dataset);
		if (!sample.getRowMetadata().getColumns().stream().anyMatch(c -> c.equalsIgnoreCase(trendColumn)))
			throw new ValidationException(null, "Dataset does not contain a column named " + trendColumn + "!");
		if (!JDBC_NUMERICS.contains(sample.getRowMetadata().getTypes().get(sample.getRowMetadata().getColumns().indexOf(trendColumn))))
			throw new ValidationException(null, "Trend column must be of numeric column type!");
		if (!sample.getRowMetadata().getColumns().stream().anyMatch(c -> c.equalsIgnoreCase(dateColumn)))
			throw new ValidationException(null, "Dataset does not contain a column named " + dateColumn + "!");
		if (!JDBC_DATES.contains(sample.getRowMetadata().getTypes().get(sample.getRowMetadata().getColumns().indexOf(dateColumn))))
			throw new ValidationException(null, "Date column must be of date column type!");
		thresholdValue = null;
		if (!StringUtils.isEmpty(getVariable(THRESHOLD_VALUE))) {
			try {
				thresholdValue = BigDecimal.valueOf(Double.parseDouble(getVariable(THRESHOLD_VALUE)));
			}
			catch (Exception e) {
				throw new ValidationException(null, "Threshold value must be a valid number!");
			}
		}
		thresholdPercentage = null;
		if (!StringUtils.isEmpty(getVariable(THRESHOLD_PERCENTAGE))) {
			try {
				thresholdPercentage = BigDecimal.valueOf(Double.parseDouble(getVariable(THRESHOLD_PERCENTAGE)));
			}
			catch (Exception e) {
				throw new ValidationException(null, "Threshold percentage must be a valid number!");
			}
			if (thresholdPercentage.doubleValue() < 0)
				throw new ValidationException(null, "Threshold percentage must be greater than 0!");
		}
		if (thresholdValue == null && thresholdPercentage == null)
			throw new ValidationException(null, "Either threshold value or percentage must be supplied!");
		extent = 1;
		if (!StringUtils.isEmpty(getVariable(EXTENT))) {
			try {
				extent = Integer.parseInt(getVariable(EXTENT));
			}
			catch (Exception e) {
				throw new ValidationException(null, "Extent must be a valid integer!");
			}
			if (extent < MIN_EXTENT || extent > MAX_EXTENT)
				throw new ValidationException(null, "Extent must be between " + MIN_EXTENT + " and " + MAX_EXTENT + " days!");
		}
		minDate = LocalDate.now().minusDays(extent);
		operator = getVariable(OPERATOR).trim();
		if (!ALLOWED_OPERATORS.contains(operator))
			throw new ValidationException(null, "Allowed operators are " + String.join(",", ALLOWED_OPERATORS) + ".");
		createGridLong(VARIABLE_INDEX, 0L);
	}

	@Override
	public void run() throws Exception {
		localCacheRun(dataset, new FirstProcessor());
	}

	public class FirstProcessor implements CacheProcessor {
		@Override
		public void process(Iterable<Entry<Object, Object>> entries) {
			logDebugMessage("Starting iteration of dataset...");
			for (Entry<Object, Object> entry : entries) {
				Row row = (Row)entry.getValue();
				BigDecimal value = castToBigDecimal(row.get(trendColumn));
				Timestamp timestamp = (Timestamp)row.get(dateColumn);
				if (timestamp != null) {
					LocalDate date = timestamp.toLocalDateTime().toLocalDate();
					if (date.isAfter(minDate) && date.isBefore(LocalDate.now().plusDays(1))) {
						LocalDate previousDate = date.minusDays(1);
						Iterable<Entry<Object, Object>> queryResults = queryCache(dataset, (k, v) -> {
							Boolean result = false;
							Row r = (Row)v;
							Timestamp t = (Timestamp)r.get(dateColumn);
							if (t != null) {
								LocalDate ld = t.toLocalDateTime().toLocalDate();
								result = previousDate.equals(ld);
							}
							return result;
						});
						Iterator<Entry<Object, Object>> queryIterator = queryResults.iterator();
						if (queryIterator.hasNext()) {
							Entry<Object, Object> queryResult = queryIterator.next();
							Row queryRow = (Row)queryResult.getValue();
							BigDecimal previousValue = castToBigDecimal(queryRow.get(trendColumn));
							if (value != null && previousValue != null) {
								BigDecimal difference = value.subtract(previousValue);
								BigDecimal percentDifference = difference.divide(previousValue, MathContext.DECIMAL32).multiply(new BigDecimal(100, MathContext.DECIMAL32));
								if (thresholdExceeded(difference, percentDifference)) {
									Row reportRow = createRow();
									reportRow.put(COLUMN_ID, incrementAndGetGridLong(VARIABLE_INDEX));
									reportRow.put(COLUMN_DATE1, Timestamp.valueOf(previousDate.atStartOfDay()));
									reportRow.put(COLUMN_VALUE1, previousValue);
									reportRow.put(COLUMN_DATE2, Timestamp.valueOf(date.atStartOfDay()));
									reportRow.put(COLUMN_VALUE2, value);
									reportRow.put(COLUMN_DIFFERENCE, difference);
									reportRow.put(COLUMN_PERCENT_DIFFERENCE, percentDifference);
									putIntoCache(reportRow.getKeyString(), reportRow);
								}
							}
						}
					}
				}
			}
			logDebugMessage("Finished iteration of dataset.");
		}
	}

	private Boolean thresholdExceeded(BigDecimal difference, BigDecimal percentDifference) {
		Boolean result = false;
		switch (operator) {
			case ">":
				result = (thresholdValue != null && difference.compareTo(thresholdValue) > 0) ||
					(thresholdPercentage != null && percentDifference.compareTo(thresholdPercentage) > 0);
				break;
			case ">=":
				result = (thresholdValue != null && difference.compareTo(thresholdValue) >= 0) ||
					(thresholdPercentage != null && percentDifference.compareTo(thresholdPercentage) >= 0);
				break;
			case "<":
				result = (thresholdValue != null && difference.compareTo(thresholdValue) < 0) ||
					(thresholdPercentage != null && percentDifference.compareTo(thresholdPercentage) < 0);
				break;
			case "<=":
				result = (thresholdValue != null && difference.compareTo(thresholdValue) <= 0) ||
					(thresholdPercentage != null && percentDifference.compareTo(thresholdPercentage) <= 0);
				break;
			case "<>":
				result = (thresholdValue != null && difference.compareTo(thresholdValue) != 0) ||
					(thresholdPercentage != null && percentDifference.compareTo(thresholdPercentage) != 0);
				break;
			case "=":
				result = (thresholdValue != null && difference.compareTo(thresholdValue) == 0) ||
					(thresholdPercentage != null && percentDifference.compareTo(thresholdPercentage) == 0);
				break;
			default:
				break;
		}
		return result;
	}

	private Row createRow() {
		Row result = new Row();
		result
			.addColumn(COLUMN_ID, TYPE_ID, LENGTH_ID, null)
			.addColumn(COLUMN_DATE1, TYPE_DATE1, LENGTH_DATE1, null)
			.addColumn(COLUMN_VALUE1, TYPE_VALUE1, LENGTH_VALUE1, null)
			.addColumn(COLUMN_DATE2, TYPE_DATE2, LENGTH_DATE2, null)
			.addColumn(COLUMN_VALUE2, TYPE_VALUE2, LENGTH_VALUE2, null)
			.addColumn(COLUMN_DIFFERENCE, TYPE_DIFFERENCE, LENGTH_DIFFERENCE, null)
			.addColumn(COLUMN_PERCENT_DIFFERENCE, TYPE_PERCENT_DIFFERENCE, LENGTH_PERCENT_DIFFERENCE, null);
		result.getKeyColumns().add(COLUMN_ID);
		return result;
	}

	private BigDecimal castToBigDecimal(Object object) {
		BigDecimal result = null;
		if (object instanceof BigDecimal)
			result = (BigDecimal)object;
		else if (object instanceof Float)
			result = new BigDecimal((Float)object);
		else if (object instanceof Double)
			result = new BigDecimal((Double)object);
		else if (object instanceof Integer)
			result = new BigDecimal((Integer)object);
		else if (object != null)
			throw new IgniteException(String.format("Object type '%s' not supported for column difference!", object.getClass().getSimpleName()));
//		result.round(MathContext.DECIMAL32);
		return result;
	}

	@Override
	protected void setResults() {
		Long exceedCount = getGridLong(VARIABLE_INDEX);
		putIntoResultCache(RESULT_EXCEED_COUNT, exceedCount);
		putIntoResultCache(RESULT_THRESHOLD_EXCEEDED, exceedCount > 0 ? true : false);
	}

	@Override
	protected void cleanUp() {
		removeGridLong(VARIABLE_INDEX);
	}

}
package net.intellica.icc.template.nullcheck;

import java.sql.JDBCType;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import javax.cache.Cache.Entry;
import net.intellica.icc.template.exception.ValidationException;
import net.intellica.icc.template.model.CacheProcessor;
import net.intellica.icc.template.model.Row;
import net.intellica.icc.template.model.TemplateJob;
import org.springframework.util.StringUtils;

public class NullCheckJob extends TemplateJob {

	// CONSTANTS
	private static final long serialVersionUID = 1L;

	// TEMPLATE VARIABLES
	public static final String DATASET = "DataSet";
	public static final String COLUMNS = "Columns";
	public static final String SHOW_SUMMARY = "ShowSummary";

	// REPORT COMMON COLUMNS
	public static final String COLUMN_ID = "ID";
	public static final String COLUMN_KEY = "KEY";

	// REPORT DETAIL COLUMNS
	public static final String COLUMN_NULL_COLUMN = "NULL_COLUMN";

	// REPORT SUMMARY COLUMNS
	public static final String COLUMN_NULL_FLAG = "NULL_FLAG";
	public static final String COLUMN_NULL_COLUMNS = "NULL_COLUMNS";
	public static final String COLUMN_NULL_COUNT = "NULL_COUNT";

	// REPORT COMMON COLUMN TYPES
	public static final JDBCType TYPE_ID = JDBCType.NUMERIC;
	public static final JDBCType TYPE_KEY = JDBCType.VARCHAR;

	// REPORT DETAIL COLUMN TYPES
	public static final JDBCType TYPE_NULL_COLUMN = JDBCType.VARCHAR;

	// REPORT SUMMARY COLUMN TYPES
	public static final JDBCType TYPE_NULL_FLAG = JDBCType.NUMERIC;
	public static final JDBCType TYPE_NULL_COLUMNS = JDBCType.VARCHAR;
	public static final JDBCType TYPE_NULL_COUNT = JDBCType.NUMERIC;

	// REPORT COMMON COLUMN LENGTHS
	public static final Integer LENGTH_ID = 0;
	public static final Integer LENGTH_KEY = 2000;

	// REPORT DETAIL COLUMN LENGTHS
	public static final Integer LENGTH_NULL_COLUMN = 2000;

	// REPORT SUMMARY COLUMN LENGTHS
	public static final Integer LENGTH_NULL_FLAG = 1;
	public static final Integer LENGTH_NULL_COLUMNS = 2000;
	public static final Integer LENGTH_NULL_COUNT = 0;

	// RESULTS
	public static final String RESULT_NULL_EXISTS = "NullExists";
	public static final String RESULT_NULL_COLUMNS = "NullColumns";
	public static final String RESULT_NULL_COUNT = "NullCount";

	// GRID VARIABLES
	public static final String VARIABLE_INDEX = "INDEX";
	public static final String VARIABLE_NULL_COLUMNS = "NULL_COLUMNS";

	// INTERNAL VARIABLES
	private String dataset;
	private Set<String> columns;
	private Boolean showSummary;

	public NullCheckJob(Object arg) {
		super(arg);
	}

	@Override
	protected void validatialization() throws Exception {
		dataset = getVariable(DATASET);
		if (StringUtils.isEmpty(dataset))
			dataset = getLastDataset();
		if (!cacheExists(dataset))
			throw new ValidationException(null, "Dataset does not exist!");
		Row sample = (Row)getSampleFromCache(dataset);
		List<String> rowColumns = sample.getRowMetadata().getColumns();
		Set<String> checkColumns = new HashSet<>();
		String columnString = getVariable(COLUMNS);
		if (!StringUtils.isEmpty(columnString) && !StringUtils.isEmpty(columnString.trim())) {
			Set<String> userColumns = Arrays.asList(columnString.split(",")).stream()
				.filter(c -> !StringUtils.isEmpty(c))
				.map(c -> c.trim())
				.collect(Collectors.toSet());
			checkColumns.addAll(rowColumns.stream().filter(c -> userColumns.contains(c)).collect(Collectors.toSet()));
			logDebugMessage(String.format("Checking columns %s...", String.join(",", checkColumns)));
		}
		else {
			checkColumns.addAll(rowColumns);
			logDebugMessage("Checking all columns...");
		}
		columns = checkColumns;
		showSummary = Boolean.parseBoolean(getVariable(SHOW_SUMMARY));
		createGridLong(VARIABLE_INDEX, 0L);
		Set<String> nullColumns = ConcurrentHashMap.newKeySet();
		createGridVariable(VARIABLE_NULL_COLUMNS, nullColumns);
		createGridLong(RESULT_NULL_COUNT, 0L);
	}

	@Override
	public void run() throws Exception {
		localCacheRun(dataset, new FirstProcessor());
		createSummary();
	}

	public class FirstProcessor implements CacheProcessor {
		@Override
		public void process(Iterable<Entry<Object, Object>> entries) {
			Set<String> nullColumns = new HashSet<>();
			logDebugMessage("Starting iteration of dataset...");
			for (Entry<Object, Object> entry : entries) {
				String key = (String)entry.getKey();
				Row row = (Row)entry.getValue();
				Set<String> rowNullColumns = columns.stream()
					.filter(c -> row.get(c) == null)
					.collect(Collectors.toSet());
				nullColumns.addAll(rowNullColumns);
				if (showSummary)
					rowNullColumns.forEach(c -> incrementAndGetGridLong(VARIABLE_INDEX));
				else {
					for (String rnc : rowNullColumns) {
						Row detailRow = createDetailRow();
						detailRow.put(COLUMN_ID, incrementAndGetGridLong(VARIABLE_INDEX));
						detailRow.put(COLUMN_KEY, key);
						detailRow.put(COLUMN_NULL_COLUMN, rnc);
						putIntoCache(detailRow.getKeyString(), detailRow);
					}
				}
			}
			logDebugMessage("Finished iteration of dataset.");
			setGridVariable(VARIABLE_NULL_COLUMNS, v -> {
				((Set<String>)v).addAll(nullColumns);
				return v;
			});
		}
	}

	public void createSummary() {
		if (showSummary) {
			Row summaryRow = createSummaryRow();
			Set<String> nullColumns = (Set<String>)getGridVariable(VARIABLE_NULL_COLUMNS);
			String nullColumnString = String.join(",", nullColumns);
			Boolean flag = nullColumns.size() == 0 ? false : true;
			summaryRow.put(COLUMN_ID, 1);
			summaryRow.put(COLUMN_NULL_FLAG, flag);
			summaryRow.put(COLUMN_NULL_COLUMNS, nullColumnString);
			summaryRow.put(COLUMN_NULL_COUNT, getGridLong(VARIABLE_INDEX));
			putIntoCache(summaryRow.getKeyString(), summaryRow);
		}
	}

	private Row createDetailRow() {
		Row result = new Row();
		result
			.addColumn(COLUMN_ID, TYPE_ID, LENGTH_ID, null)
			.addColumn(COLUMN_KEY, TYPE_KEY, LENGTH_KEY, null)
			.addColumn(COLUMN_NULL_COLUMN, TYPE_NULL_COLUMN, LENGTH_NULL_COLUMN, null);
		result.getKeyColumns().add(COLUMN_KEY);
		result.getKeyColumns().add(COLUMN_NULL_COLUMN);
		return result;
	}

	private Row createSummaryRow() {
		Row result = new Row();
		result
			.addColumn(COLUMN_ID, TYPE_ID, LENGTH_ID, null)
			.addColumn(COLUMN_NULL_COLUMNS, TYPE_NULL_COLUMNS, LENGTH_NULL_COLUMNS, null)
			.addColumn(COLUMN_NULL_COUNT, TYPE_NULL_COUNT, LENGTH_NULL_COUNT, null)
			.addColumn(COLUMN_NULL_FLAG, TYPE_NULL_FLAG, LENGTH_NULL_FLAG, null);
		return result;
	}

	@Override
	protected void setResults() {
		Set<String> nullColumns = (Set<String>)getGridVariable(VARIABLE_NULL_COLUMNS);
		putIntoResultCache(RESULT_NULL_EXISTS, nullColumns.size() == 0 ? false : true);
		putIntoResultCache(RESULT_NULL_COLUMNS, String.join(",", nullColumns));
		putIntoResultCache(RESULT_NULL_COUNT, getGridLong(VARIABLE_INDEX));
	}

	@Override
	protected void cleanUp() {
		removeGridLong(VARIABLE_INDEX);
		removeGridVariable(VARIABLE_NULL_COLUMNS);
		removeGridLong(RESULT_NULL_COUNT);
	}

}
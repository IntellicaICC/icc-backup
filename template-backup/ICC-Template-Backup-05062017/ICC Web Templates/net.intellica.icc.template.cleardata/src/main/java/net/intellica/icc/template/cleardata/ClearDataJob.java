package net.intellica.icc.template.cleardata;

import net.intellica.icc.template.model.TemplateJob;
import org.springframework.util.StringUtils;

public class ClearDataJob extends TemplateJob {

	private static final long serialVersionUID = 1L;

	public static final String FIRST_DATASET = "FirstDataSet";
	public static final String SECOND_DATASET = "SecondDataSet";
	public static final String THIRD_DATASET = "ThirdDataSet";
	public static final String FOURTH_DATASET = "FourthDataSet";
	public static final String FIFTH_DATASET = "FifthDataSet";

	public ClearDataJob(Object arg) {
		super(arg);
	}

	@Override
	protected void validatialization() throws Exception {
		// TODO ClearDataJob.validations()
	}

	@Override
	public void run() throws Exception {
		clearData(getVariable(FIRST_DATASET));
		clearData(getVariable(SECOND_DATASET));
		clearData(getVariable(THIRD_DATASET));
		clearData(getVariable(FOURTH_DATASET));
		clearData(getVariable(FIFTH_DATASET));
	}

	private void clearData(String dataset) {
		if (!StringUtils.isEmpty(dataset)) {
			logDebugMessage(String.format("Clearing dataset %s...", dataset));
			clearCache(dataset);
		}
	}

}



import net.intellica.icc.template.cleardata.ClearDataJob;
import net.intellica.icc.template.model.TemplateJob;
import net.intellica.icc.template.model.TemplateTask;

public class ClearData extends TemplateTask {

	private static final long serialVersionUID = 1L;

	@Override
	protected TemplateJob getJob(Object arg) {
		return new ClearDataJob(arg);
	}

}

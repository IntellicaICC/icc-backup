import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Properties;
import net.intellica.icc.template.model.DataLink;
import net.intellica.icc.template.model.Row;
import org.apache.ignite.Ignite;

public class DatabaseUtils {

	private static DataLink link;

	static {
		Properties properties = new Properties();
		try {
			properties.load(DatabaseUtils.class.getResourceAsStream("test.properties"));
			link = new DataLink();
			link.setUrl(properties.getProperty("url"));
			link.setUsername(properties.getProperty("user"));
			link.setPassword(properties.getProperty("pass"));
		}
		catch (IOException e) {
			throw new RuntimeException("Error reading properties!", e);
		}
	}

	public static Integer getCount(String tableName) {
		int result = 0;
		try (Connection conn = DriverManager.getConnection(link.getUrl(), link.getUsername(), link.getPassword())) {
			try (Statement stmt = conn.createStatement()) {
				try (ResultSet rs = stmt.executeQuery("SELECT COUNT(*) FROM " + tableName)) {
					if (rs.next())
						result = rs.getInt(1);
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static void dropTable(String tableName) {
		try (Connection conn = DriverManager.getConnection(link.getUrl(), link.getUsername(), link.getPassword())) {
			try (Statement stmt = conn.createStatement()) {
				stmt.executeUpdate("DROP TABLE " + tableName);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static DataLink getDataLink() {
		return link;
	}

	public static void query(String sql, String cache, Ignite ignite, String... keys) {
		ignite.cache(cache).removeAll();
		ignite.destroyCache(cache);
		try (Connection conn = DriverManager.getConnection(link.getUrl(), link.getUsername(), link.getPassword())) {
			try (Statement stmt = conn.createStatement()) {
				try (ResultSet rs = stmt.executeQuery(sql)) {
					ResultSetMetaData metaData = rs.getMetaData();
					while (rs.next()) {
						Row row = new Row(metaData);
						for (int i = 0; i < metaData.getColumnCount(); i++)
							row.put(i, rs.getObject(i + 1));
						Arrays.asList(keys).forEach(k -> row.getKeyColumns().add(k));
						ignite.cache(cache).put(row.getKeyString(), row);
					}
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}

package net.intellica.icc.template.duplicatecheck;

import java.sql.JDBCType;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import javax.cache.Cache.Entry;
import net.intellica.icc.template.exception.ValidationException;
import net.intellica.icc.template.model.CacheProcessor;
import net.intellica.icc.template.model.Row;
import net.intellica.icc.template.model.TemplateJob;
import org.springframework.util.StringUtils;

public class DuplicateCheckJob extends TemplateJob {

	// CONSTANTS
	private static final long serialVersionUID = 1L;

	// TEMPLATE VARIABLES
	public static final String DATASET = "DataSet";
	public static final String COLUMNS = "Columns";
	public static final String SHOW_SUMMARY = "ShowSummary";

	// REPORT COMMON COLUMNS
	public static final String COLUMN_ID = "ID";
	public static final String COLUMN_KEY = "KEY";

	// REPORT DETAIL COLUMNS
	public static final String COLUMN_DUPLICATE_COLUMN = "DUPLICATE_COLUMN";
	public static final String COLUMN_DUPLICATE_VALUE = "DUPLICATE_VALUE";

	// REPORT SUMMARY COLUMNS
	public static final String COLUMN_DUPLICATE_FLAG = "DUPLICATE_FLAG";
	public static final String COLUMN_DUPLICATE_COLUMNS = "DUPLICATE_COLUMNS";
	public static final String COLUMN_DUPLICATE_COUNT = "DUPLICATE_COUNT";

	// REPORT COMMON COLUMN TYPES
	public static final JDBCType TYPE_ID = JDBCType.NUMERIC;
	public static final JDBCType TYPE_KEY = JDBCType.VARCHAR;

	// REPORT DETAIL COLUMN TYPES
	public static final JDBCType TYPE_DUPLICATE_COLUMN = JDBCType.VARCHAR;
	public static final JDBCType TYPE_DUPLICATE_VALUE = JDBCType.VARCHAR;

	// REPORT SUMMARY COLUMN TYPES
	public static final JDBCType TYPE_DUPLICATE_FLAG = JDBCType.NUMERIC;
	public static final JDBCType TYPE_DUPLICATE_COLUMNS = JDBCType.VARCHAR;
	public static final JDBCType TYPE_DUPLICATE_COUNT = JDBCType.NUMERIC;

	// REPORT COMMON COLUMN LENGTHS
	public static final Integer LENGTH_ID = 0;
	public static final Integer LENGTH_KEY = 2000;

	// REPORT DETAIL COLUMN LENGTHS
	public static final Integer LENGTH_DUPLICATE_COLUMN = 2000;
	public static final Integer LENGTH_DUPLICATE_VALUE = 2000;

	// REPORT SUMMARY COLUMN LENGTHS
	public static final Integer LENGTH_DUPLICATE_FLAG = 0;
	public static final Integer LENGTH_DUPLICATE_COLUMNS = 2000;
	public static final Integer LENGTH_DUPLICATE_COUNT = 0;

	// RESULTS
	public static final String RESULT_DUPLICATE_EXISTS = "DuplicateExists";
	public static final String RESULT_DUPLICATE_COLUMNS = "DuplicateColumns";
	public static final String RESULT_DUPLICATE_COUNT = "DuplicateCount";

	// GRID VARIABLES
	public static final String VARIABLE_INDEX = "INDEX";
	public static final String VARIABLE_DUPLICATE_COLUMNS = "DUPLICATE_COLUMNS";

	// INTERNAL VARIABLES
	private String dataset;
	private Set<String> columns;
	private Boolean showSummary;

	public DuplicateCheckJob(Object arg) {
		super(arg);
	}

	@Override
	protected void validatialization() throws Exception {
		dataset = getVariable(DATASET);
		if (StringUtils.isEmpty(dataset))
			dataset = getLastDataset();
		if (!cacheExists(dataset))
			throw new ValidationException(null, "Dataset does not exist!");
		Row sample = (Row)getSampleFromCache(dataset);
		List<String> rowColumns = sample.getRowMetadata().getColumns();
		Set<String> checkColumns = new HashSet<>();
		String columnString = getVariable(COLUMNS);
		if (!StringUtils.isEmpty(columnString) && !StringUtils.isEmpty(columnString.trim())) {
			Set<String> userColumns = Arrays.asList(columnString.split(",")).stream()
				.filter(c -> !StringUtils.isEmpty(c))
				.map(c -> c.trim())
				.collect(Collectors.toSet());
			checkColumns.addAll(rowColumns.stream().filter(c -> userColumns.contains(c)).collect(Collectors.toSet()));
			logDebugMessage(String.format("Checking columns %s...", String.join(",", checkColumns)));
		}
		else {
			checkColumns.addAll(rowColumns);
			logDebugMessage("Checking all columns...");
		}
		columns = checkColumns;
		showSummary = Boolean.parseBoolean(getVariable(SHOW_SUMMARY));
		createGridLong(VARIABLE_INDEX, 0L);
		Set<String> duplicateColumns = ConcurrentHashMap.newKeySet();
		createGridVariable(VARIABLE_DUPLICATE_COLUMNS, duplicateColumns);
		createGridLong(RESULT_DUPLICATE_COUNT, 0L);
	}

	@Override
	public void run() throws Exception {
		localCacheRun(dataset, new FirstProcessor());
		createSummary();
	}

	public class FirstProcessor implements CacheProcessor {

		private static final long serialVersionUID = 1L;

		@Override
		public void process(Iterable<Entry<Object, Object>> entries) {
			Set<String> duplicateColumns = new HashSet<>();
			logDebugMessage("Starting iteration of dataset...");
			for (Entry<Object, Object> entry : entries) {
				String key = (String)entry.getKey();
				Row row = (Row)entry.getValue();
				for (String column : columns) {
					if (!row.getKeyColumns().contains(column)) {
						Object value = row.get(column);
						Iterable<Entry<Object, Object>> queryResults = queryCache(dataset, (k, v) -> {
							Boolean result = false;
							Row r = (Row)v;
							Object otherValue = r.get(column);
							result = Objects.equals(value, otherValue) && !Objects.equals(r.getKeyString(), row.getKeyString());
							return result;
						});
						Iterator<Entry<Object, Object>> queryIterator = queryResults.iterator();
						while (queryIterator.hasNext()) {
							duplicateColumns.add(column);
							Entry<Object, Object> queryResult = queryIterator.next();
							Row queryRow = (Row)queryResult.getValue();
							Row detailRow = createDetailRow();
							detailRow.put(COLUMN_KEY, key);
							detailRow.put(COLUMN_DUPLICATE_COLUMN, column);
							detailRow.put(COLUMN_DUPLICATE_VALUE, queryRow.get(column));
							if (!cacheKeyExists(detailRow.getKeyString())) {
								Long id = incrementAndGetGridLong(VARIABLE_INDEX);
								if (!showSummary) {
									detailRow.put(COLUMN_ID, id);
									putIntoCache(detailRow.getKeyString(), detailRow);
								}
							}
						}
					}
				}
			}
			logDebugMessage("Finished iteration of dataset.");
			setGridVariable(VARIABLE_DUPLICATE_COLUMNS, v -> {
				((Set<String>)v).addAll(duplicateColumns);
				return v;
			});
		}
	}

	public void createSummary() {
		if (showSummary) {
			Row summaryRow = createSummaryRow();
			Set<String> duplicateColumns = (Set<String>)getGridVariable(VARIABLE_DUPLICATE_COLUMNS);
			String duplicateColumnString = String.join(",", duplicateColumns);
			Boolean flag = duplicateColumns.size() == 0 ? false : true;
			summaryRow.put(COLUMN_ID, 1);
			summaryRow.put(COLUMN_DUPLICATE_FLAG, flag);
			summaryRow.put(COLUMN_DUPLICATE_COLUMNS, duplicateColumnString);
			summaryRow.put(COLUMN_DUPLICATE_COUNT, getGridLong(VARIABLE_INDEX));
			putIntoCache(summaryRow.getKeyString(), summaryRow);
		}
	}

	private Row createDetailRow() {
		Row result = new Row();
		result
			.addColumn(COLUMN_ID, TYPE_ID, LENGTH_ID, null)
			.addColumn(COLUMN_KEY, TYPE_KEY, LENGTH_KEY, null)
			.addColumn(COLUMN_DUPLICATE_COLUMN, TYPE_DUPLICATE_COLUMN, LENGTH_DUPLICATE_COLUMN, null)
			.addColumn(COLUMN_DUPLICATE_VALUE, TYPE_DUPLICATE_VALUE, LENGTH_DUPLICATE_VALUE, null);
		result.getKeyColumns().add(COLUMN_KEY);
		result.getKeyColumns().add(COLUMN_DUPLICATE_COLUMN);
		return result;
	}

	private Row createSummaryRow() {
		Row result = new Row();
		result
			.addColumn(COLUMN_ID, TYPE_ID, LENGTH_ID, null)
			.addColumn(COLUMN_DUPLICATE_COLUMNS, TYPE_DUPLICATE_COLUMNS, LENGTH_DUPLICATE_COLUMNS, null)
			.addColumn(COLUMN_DUPLICATE_COUNT, TYPE_DUPLICATE_COUNT, LENGTH_DUPLICATE_COUNT, null)
			.addColumn(COLUMN_DUPLICATE_FLAG, TYPE_DUPLICATE_FLAG, LENGTH_DUPLICATE_FLAG, null);
		return result;
	}

	@Override
	protected void setResults() {
		Set<String> duplicateColumns = (Set<String>)getGridVariable(VARIABLE_DUPLICATE_COLUMNS);
		putIntoResultCache(RESULT_DUPLICATE_EXISTS, duplicateColumns.size() == 0 ? false : true);
		putIntoResultCache(RESULT_DUPLICATE_COLUMNS, String.join(",", duplicateColumns));
		putIntoResultCache(RESULT_DUPLICATE_COUNT, getGridLong(VARIABLE_INDEX));
	}

	@Override
	protected void cleanUp() {
		removeGridLong(VARIABLE_INDEX);
		removeGridVariable(VARIABLE_DUPLICATE_COLUMNS);
		removeGridLong(RESULT_DUPLICATE_COUNT);
	}

}

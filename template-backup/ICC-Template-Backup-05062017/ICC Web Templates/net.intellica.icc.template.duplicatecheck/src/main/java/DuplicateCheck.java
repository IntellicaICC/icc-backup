import net.intellica.icc.template.duplicatecheck.DuplicateCheckJob;
import net.intellica.icc.template.model.TemplateJob;
import net.intellica.icc.template.model.TemplateTask;

public class DuplicateCheck extends TemplateTask {

	private static final long serialVersionUID = 1L;

	@Override
	protected TemplateJob getJob(Object arg) {
		return new DuplicateCheckJob(arg);
	}

}

SELECT 
	'%1$s' AS %5$s,
	'%2$s' AS %6$s,
	'%3$s' AS %7$s,
	%3$s(%2$s) AS %8$s
FROM %1$s
%4$s

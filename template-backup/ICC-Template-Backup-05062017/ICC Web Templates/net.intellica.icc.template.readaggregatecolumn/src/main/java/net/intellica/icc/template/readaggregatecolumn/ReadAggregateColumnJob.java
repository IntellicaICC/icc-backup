package net.intellica.icc.template.readaggregatecolumn;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import net.intellica.icc.template.exception.ValidationException;
import net.intellica.icc.template.model.Row;
import net.intellica.icc.template.model.TemplateJob;
import org.springframework.util.StringUtils;

public class ReadAggregateColumnJob extends TemplateJob {

	// CONSTANTS
	private static final long serialVersionUID = 1L;

	// TEMPLATE VARIABLES
	public static final String CONNECTION = "Connection";
	public static final String TABLE = "Table";
	public static final String COLUMN = "Column";
	public static final String CONDITION = "Condition";
	public static final String AGGREGATE_FUNCTION = "AggregateFunction";

	// RESULT COLUMNS
	private static final String COLUMN_TABLE_NAME = "TABLE_NAME";
	private static final String COLUMN_COLUMN_NAME = "COLUMN_NAME";
	private static final String COLUMN_AGG_FUNC = "AGG_FUNC";
	private static final String COLUMN_VALUE = "VALUE";

	// RESULTS
	public static final String RESULT_AGGREGATE_VALUE = "AggregateValue";

	// INTERNAL VARIABLES
	private final String SQL;
	private static final List<String> aggregateFunctions = Arrays.asList(
		"COUNT",
		"SUM",
		"AVG",
		"MIN",
		"MAX"//
	);

	// INTERNAL VARIABLES
	private String table;
	private String column;
	private String condition;
	private String aggregateFunction;

	public ReadAggregateColumnJob(Object arg) {
		super(arg);
		try (Scanner scanner = new Scanner(ReadAggregateColumnJob.class.getResourceAsStream("/select.sql"), "UTF-8")) {
			SQL = scanner.useDelimiter("\\A").next();
		}
	}

	@Override
	protected void validatialization() throws Exception {
		table = getVariable(TABLE).trim();
		if (StringUtils.isEmpty(table))
			throw new ValidationException(null, "Table can not be left empty!");
		try {
			validateTableName(getVariable(CONNECTION), table);
		}
		catch (Exception e) {
			validateQualifiedTableName(getVariable(CONNECTION), table);
		}
		column = getVariable(COLUMN).trim();
		validateColumnName(getVariable(CONNECTION), column);
		condition = getVariable(CONDITION).trim();
		if (!StringUtils.isEmpty(condition)) {
			if (!condition.toUpperCase(Locale.US).startsWith("WHERE"))
				condition = "WHERE " + condition;
		}
		aggregateFunction = getVariable(AGGREGATE_FUNCTION);
		if (!aggregateFunctions.stream().anyMatch(f -> f.equalsIgnoreCase(aggregateFunction)))
			throw new ValidationException(null, "Allowed aggregate functions are " + String.join(",", aggregateFunctions));
	}

	@Override
	public void run() throws Exception {
		String query = String.format(SQL,
			table,
			column,
			aggregateFunction,
			condition,
			COLUMN_TABLE_NAME,
			COLUMN_COLUMN_NAME,
			COLUMN_AGG_FUNC,
			COLUMN_VALUE//
		);
		iterateQuery(getVariable(CONNECTION), query, row -> {
			row.getKeyColumns().add(COLUMN_AGG_FUNC);
			putIntoCache(row.getKeyString(), row);
		});
	}

	@Override
	protected void setResults() {
		Row row = (Row)getSampleFromCache();
		Object aggregateValue = row.get(COLUMN_VALUE);
		if (aggregateValue instanceof Date)
			aggregateValue = String.format(Locale.US, "%1$tB %1$td, %1$tY %1$tI:%1$tS %1$Tp", aggregateValue);
		putIntoResultCache(RESULT_AGGREGATE_VALUE, aggregateValue);
	}

}

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.Map;
import javax.cache.Cache;
import javax.cache.Cache.Entry;
import net.intellica.icc.template.model.DataLink;
import net.intellica.icc.template.model.Row;
import net.intellica.icc.template.model.WorkItem;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

// ana kontroller
// SUM
// a - condition vermeden integer kolonun SUM değerini alma
// b - condition vererek integer kolonun SUM değerini alma
// c - condition vermeden string bir kolonun SUM değerini alma - HATA MESAJI
// ALINACAK
// d - condition vererek string bir kolonun SUM değerini alma - HATA MESAJI
// ALINACAK
// e - condition vermeden date bir kolonun SUM değerini alma - HATA MESAJI
// ALINACAK
// f - condition vererek date bir kolonun SUM değerini alma - HATA MESAJI
// ALINACAK

// MAX
// g - condition vermeden integer kolonun MAX değerini alma
// h - condition vererek integer kolonun MAX değerini alma
// i - condition vermeden date kolonun MAX değerini alma
// J - condition vererek date kolonun MAX değerini alma
// k - condition vermeden string kolonun MAX değerini alma
// l - condition vererek string kolonun MAX değerini alma

// MIN
// m - condition vermeden integer kolonun MIN değerini alma
// n - condition vererek integer kolonun MIN değerini alma
// o - condition vermeden date kolonun MIN değerini alma
// p - condition vererek date kolonun MIN değerini alma
// q - condition vermeden string kolonun MIN değerini alma
// r - condition vererek string kolonun MIN değerini alma

// COUNT
// s - condition vermeden integer kolonun COUNT değerini alma
// t - condition vererek integer kolonun COUNT değerini alma
// u - condition vermeden date bir kolonun COUNT değerini alma
// v - condition vererek date bir kolonun COUNT değerini alma
// w - condition vermeden string bir kolonun COUNT değerini alma
// x - condition vererek string bir kolonun COUNT değerini alma

// AVG
// y - condition vermeden integer kolonun AVG değerini alma
// z - condition vererek integer kolonun AVG değerini alma
// z1 - condition vermeden date bir kolonun AVG değerini alma - HATA MESAJI
// ALINACAK
// z2 - condition vererek date bir kolonun AVG değerini alma - HATA MESAJI
// ALINACAK
// z3 - condition vermeden string bir kolonun AVG değerini alma - HATA MESAJI
// ALINACAK
// z4 - condition vererek string bir kolonun AVG değerini alma - HATA MESAJI
// ALINACAK

// DIGER CASELER
// z5 - hatalı tablo adı verilerek sum almak
// z6 - hatalı kolon adı verilerek sum almak
// z7 - tablo adı boş geçilecek
// z8 - kolon adı boş geçilecek
// z9 - hatalı koşul verilerek sum almak
// z10 - içinde null satır olan bir kolonu SUM almak
// z11 - içinde null satır olan bir kolonu AVG almak
// z12 - içinde null satır olan bir kolonu COUNT almak
// z13 - içinde null satır olan bir kolonu MAX almak
// z14 - içinde null satır olan bir kolonu MIN almak

public class ReadAggregateColumnTest {

	private static Ignite ignite;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ignite = Ignition.start("ignite.xml");
	}

	@Test
	// ana kontroller
	public void rac01() {
		String cacheName = "rac01";
		WorkItem work = new WorkItem();

		work.getLinks().put("1", DatabaseUtils.getDataLink());

		work.getArguments().put("Connection", "1");
		work.getArguments().put("Table", "DENEME4");
		work.getArguments().put("Column", "X3");
		work.getArguments().put("Condition", "");
		work.getArguments().put("Aggregate Function", "COUNT");

		//work.setDatasetName(cacheName);

		ignite.compute().execute("ReadAggregateColumn", work);

		IgniteCache<Object, Object> cache = ignite.cache(cacheName);
		Assert.assertNotNull(cache);
		Assert.assertEquals(cache.size(), 1);
		Iterator<Entry<Object, Object>> iterator = cache.iterator();
		Assert.assertTrue(iterator.hasNext());
		Cache.Entry<Object, Object> entry = (Cache.Entry<Object, Object>)iterator.next();
		Assert.assertTrue(entry.getKey() instanceof Map);
		Assert.assertTrue(entry.getValue() instanceof Row);
		Map<String, Object> keys = (Map<String, Object>)entry.getKey();
		Assert.assertEquals(keys.size(), 1);
		Assert.assertEquals("COUNT", keys.get("AGG_FUNC"));
		Row row = (Row)entry.getValue();
		Assert.assertArrayEquals(new Object[] { "TABLE_NAME", "COLUMN_NAME", "AGG_FUNC", "VALUE" },
			row.getRowMetadata().getColumns().toArray());
		Assert.assertEquals("DENEME4", row.get("TABLE_NAME"));
		Assert.assertEquals("X3", row.get("COLUMN_NAME"));
		Assert.assertEquals("COUNT", row.get("AGG_FUNC"));
		Assert.assertEquals(new BigDecimal(3), row.get("VALUE"));
	}

	@Test
	// a - condition vermeden integer kolonun SUM değerini alma
	public void a_SumControlForInteger_NoCondition() {

		int ExpectedCount = 6;
		int result = 0;
		SumControlForInteger_NoCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result += diff.intValue();
		}

		System.out.println("Sonuç :" + result);
		Assert.assertEquals(ExpectedCount, result);

	}

	public void SumControlForInteger_NoCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X3");
		readDataWorkItem.getArguments().put("Condition", "");
		readDataWorkItem.getArguments().put("Aggregate Function", "SUM");

		//readDataWorkItem.setDatasetName("readDataSet");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// b - condition vererek integer kolonun SUM değerini alma
	public void b_SumControlForInteger_WithCondition() {

		int ExpectedCount = 5;
		int result = 0;
		SumControlForInteger_WithCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet1");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result += diff.intValue();
		}

		System.out.println("Sonuç :" + result);
		Assert.assertEquals(ExpectedCount, result);

	}

	public void SumControlForInteger_WithCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X3");
		readDataWorkItem.getArguments().put("Condition", "X1='AA' OR X1='CC'");
		readDataWorkItem.getArguments().put("Aggregate Function", "SUM");

		//readDataWorkItem.setDatasetName("readDataSet1");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// c - condition vermeden string bir kolonun SUM değerini alma
	public void c_SumControlForString_NoCondition() {

		int result = 0;
		SumControlForString_NoCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet2");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result += diff.intValue();
		}

		System.out.println("Sonuç :" + result);

	}

	public void SumControlForString_NoCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X2");
		readDataWorkItem.getArguments().put("Condition", "");
		readDataWorkItem.getArguments().put("Aggregate Function", "SUM");

		//readDataWorkItem.setDatasetName("readDataSet2");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// d - condition vererek string bir kolonun SUM değerini alma
	public void d_SumControlForString_WithCondition() {

		int result = 0;
		SumControlForForString_WithCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet3");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result += diff.intValue();
		}

		System.out.println("Sonuç :" + result);
	}

	public void SumControlForForString_WithCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X2");
		readDataWorkItem.getArguments().put("Condition", "X1='AA' OR X1='CC'");
		readDataWorkItem.getArguments().put("Aggregate Function", "SUM");

		//readDataWorkItem.setDatasetName("readDataSet3");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// e - condition vermeden date bir kolonun SUM değerini alma
	public void e_SumControlForDate_NoCondition() {

		int result = 0;
		SumControlForDate_NoCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet4");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result += diff.intValue();
		}

		System.out.println("Sonuç :" + result);

	}

	public void SumControlForDate_NoCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME5");
		readDataWorkItem.getArguments().put("Column", "X5");
		readDataWorkItem.getArguments().put("Condition", "");
		readDataWorkItem.getArguments().put("Aggregate Function", "SUM");

		//readDataWorkItem.setDatasetName("readDataSet4");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// f - condition vererek date bir kolonun SUM değerini alma
	public void f_SumControlForDate_WithCondition() {

		int result = 0;
		SumControlForForDate_WithCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet5");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result += diff.intValue();
		}

		System.out.println("Sonuç :" + result);
	}

	public void SumControlForForDate_WithCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME5");
		readDataWorkItem.getArguments().put("Column", "X5");
		readDataWorkItem.getArguments().put("Condition", "X1='AA' OR X1='CC'");
		readDataWorkItem.getArguments().put("Aggregate Function", "SUM");

		//readDataWorkItem.setDatasetName("readDataSet5");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// g - condition vermeden integer kolonun MAX değerini alma
	public void g_MaxControlForInteger_NoCondition() {

		int ExpectedCount = 3;
		int result = 0;
		MaxControlForInteger_NoCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet6");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result = diff.intValue();
		}

		System.out.println("Sonuç :" + result);
		Assert.assertEquals(ExpectedCount, result);

	}

	public void MaxControlForInteger_NoCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X3");
		readDataWorkItem.getArguments().put("Condition", "");
		readDataWorkItem.getArguments().put("Aggregate Function", "MAX");

		//readDataWorkItem.setDatasetName("readDataSet6");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// h - condition vererek integer kolonun MAX değerini alma
	public void h_MaxControlForInteger_WithCondition() {

		int ExpectedCount = 3;
		int result = 0;
		MaxControlForInteger_WithCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet7");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result = diff.intValue();
		}

		System.out.println("Sonuç :" + result);
		Assert.assertEquals(ExpectedCount, result);

	}

	public void MaxControlForInteger_WithCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X3");
		readDataWorkItem.getArguments().put("Condition", "X1='AA' OR X1='CC'");
		readDataWorkItem.getArguments().put("Aggregate Function", "MAX");

		//readDataWorkItem.setDatasetName("readDataSet7");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// i - condition vermeden date kolonun MAX değerini alma
	public void i_MaxControlForDate_NoCondition() {
		String expected = "2016-06-24 00:00:00.0";
		MaxControlForDate_NoCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet8");

		Timestamp dt = null;
		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			dt = (Timestamp)row.get("VALUE");
			break;
		}

		System.out.println("Sonuç=" + dt.toString());
		Assert.assertEquals(expected, dt.toString());

	}

	public void MaxControlForDate_NoCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME5");
		readDataWorkItem.getArguments().put("Column", "X5");
		readDataWorkItem.getArguments().put("Condition", "");
		readDataWorkItem.getArguments().put("Aggregate Function", "MAX");

		//readDataWorkItem.setDatasetName("readDataSet8");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// J - condition vererek date kolonun MAX değerini alma
	public void j_MaxControlForDate_WithCondition() {

		String expected = "2016-06-13 00:00:00.0";
		MaxControlForForDate_WithCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet9");

		Timestamp dt = null;
		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			dt = (Timestamp)row.get("VALUE");
			break;
		}

		System.out.println("Sonuç=" + dt.toString());
		Assert.assertEquals(expected, dt.toString());
	}

	public void MaxControlForForDate_WithCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME5");
		readDataWorkItem.getArguments().put("Column", "X5");
		readDataWorkItem.getArguments().put("Condition", "X1='JJ' OR X1='CC'");
		readDataWorkItem.getArguments().put("Aggregate Function", "MAX");

		//readDataWorkItem.setDatasetName("readDataSet9");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// k - condition vermeden string kolonun MAX değerini alma
	public void k_MaxControlForString_NoCondition() {

		String ExpectedValue = "GG";
		MaxControlForString_NoCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet10");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			Assert.assertEquals(ExpectedValue, row.get("VALUE"));
		}

	}

	public void MaxControlForString_NoCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X2");
		readDataWorkItem.getArguments().put("Condition", "");
		readDataWorkItem.getArguments().put("Aggregate Function", "MAX");

		//readDataWorkItem.setDatasetName("readDataSet10");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// l - condition vererek string kolonun MAX değerini alma
	public void l_MaxControlForString_WithCondition() {

		String ExpectedValue = "CC";
		MaxControlForForString_WithCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet11");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			Assert.assertEquals(ExpectedValue, row.get("VALUE"));
		}
	}

	public void MaxControlForForString_WithCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X2");
		readDataWorkItem.getArguments().put("Condition", "X1='AA' OR X1='CC'");
		readDataWorkItem.getArguments().put("Aggregate Function", "MAX");

		//readDataWorkItem.setDatasetName("readDataSet11");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// m - condition vermeden integer kolonun MIN değerini alma
	public void m_MinControlForInteger_NoCondition() {

		int ExpectedCount = 1;
		int result = 0;
		MinControlForInteger_NoCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet12");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result = diff.intValue();
		}

		System.out.println("Sonuç :" + result);
		Assert.assertEquals(ExpectedCount, result);

	}

	public void MinControlForInteger_NoCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X3");
		readDataWorkItem.getArguments().put("Condition", "");
		readDataWorkItem.getArguments().put("Aggregate Function", "MIN");

		//readDataWorkItem.setDatasetName("readDataSet12");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// n - condition vererek integer kolonun MIN değerini alma
	public void n_MinControlForInteger_WithCondition() {

		int ExpectedCount = 2;
		int result = 0;
		MinControlForInteger_WithCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet13");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result = diff.intValue();
		}

		System.out.println("Sonuç :" + result);
		Assert.assertEquals(ExpectedCount, result);

	}

	public void MinControlForInteger_WithCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X3");
		readDataWorkItem.getArguments().put("Condition", "X1='AA' OR X1='CC'");
		readDataWorkItem.getArguments().put("Aggregate Function", "MIN");

		//readDataWorkItem.setDatasetName("readDataSet13");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// o - condition vermeden date kolonun MIN değerini alma
	public void o_MinControlForDate_NoCondition() {

		String expected = "2016-06-12 00:00:00.0";
		MinControlForDate_NoCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet14");

		Timestamp dt = null;
		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			dt = (Timestamp)row.get("VALUE");
			break;
		}

		System.out.println("Sonuç=" + dt.toString());
		Assert.assertEquals(expected, dt.toString());

	}

	public void MinControlForDate_NoCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME5");
		readDataWorkItem.getArguments().put("Column", "X5");
		readDataWorkItem.getArguments().put("Condition", "");
		readDataWorkItem.getArguments().put("Aggregate Function", "MIN");

		//readDataWorkItem.setDatasetName("readDataSet14");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// p - condition vererek date kolonun MIN değerini alma
	public void p_MinControlForDate_WithCondition() {

		String expected = "2016-06-13 00:00:00.0";
		MinControlForForDate_WithCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet15");

		Timestamp dt = null;
		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			dt = (Timestamp)row.get("VALUE");
			break;
		}

		System.out.println("Sonuç=" + dt.toString());
		Assert.assertEquals(expected, dt.toString());

	}

	public void MinControlForForDate_WithCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME5");
		readDataWorkItem.getArguments().put("Column", "X5");
		readDataWorkItem.getArguments().put("Condition", "X1='AA' OR X1='CC'");
		readDataWorkItem.getArguments().put("Aggregate Function", "MIN");

		//readDataWorkItem.setDatasetName("readDataSet15");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// q - condition vermeden string kolonun MIN değerini alma
	public void q_MinControlForString_NoCondition() {

		String ExpectedValue = "BB";
		MinControlForString_NoCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet16");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			Assert.assertEquals(ExpectedValue, row.get("VALUE"));
		}

	}

	public void MinControlForString_NoCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X2");
		readDataWorkItem.getArguments().put("Condition", "");
		readDataWorkItem.getArguments().put("Aggregate Function", "MIN");

		//readDataWorkItem.setDatasetName("readDataSet16");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// r- condition vererek string kolonun MIN değerini alma
	public void r_MinControlForString_WithCondition() {

		String ExpectedValue = "CC";
		MinControlForForString_WithCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet17");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			Assert.assertEquals(ExpectedValue, row.get("VALUE"));
		}
	}

	public void MinControlForForString_WithCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X2");
		readDataWorkItem.getArguments().put("Condition", "X1='AA' OR X1='JJ'");
		readDataWorkItem.getArguments().put("Aggregate Function", "MIN");

		//readDataWorkItem.setDatasetName("readDataSet17");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// s- condition vermeden integer kolonun COUNT değerini alma
	public void s_CountControlForInteger_NoCondition() {

		int ExpectedCount = 3;
		int result = 0;
		CountControlForInteger_NoCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet18");
		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result += diff.intValue();
		}

		System.out.println("Sonuç :" + result);
		Assert.assertEquals(ExpectedCount, result);

	}

	public void CountControlForInteger_NoCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X3");
		readDataWorkItem.getArguments().put("Condition", "");
		readDataWorkItem.getArguments().put("Aggregate Function", "COUNT");

		//readDataWorkItem.setDatasetName("readDataSet18");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// t - condition vererek integer kolonun Count değerini alma
	public void t_CountControlForInteger_WithCondition() {

		int ExpectedCount = 2;
		int result = 0;
		CountControlForInteger_WithCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet19");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result += diff.intValue();
		}

		System.out.println("Sonuç :" + result);
		Assert.assertEquals(ExpectedCount, result);

	}

	public void CountControlForInteger_WithCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X3");
		readDataWorkItem.getArguments().put("Condition", "X1='AA' OR X1='CC'");
		readDataWorkItem.getArguments().put("Aggregate Function", "COUNT");

		//readDataWorkItem.setDatasetName("readDataSet19");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// u - condition vermeden date kolonun Count değerini alma
	public void u_CountControlForDate_NoCondition() {

		int ExpectedCount = 3;
		int result = 0;
		CountControlForDate_NoCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet20");
		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result += diff.intValue();
		}

		System.out.println("Sonuç :" + result);
		Assert.assertEquals(ExpectedCount, result);

	}

	public void CountControlForDate_NoCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME5");
		readDataWorkItem.getArguments().put("Column", "X5");
		readDataWorkItem.getArguments().put("Condition", "");
		readDataWorkItem.getArguments().put("Aggregate Function", "COUNT");

		//readDataWorkItem.setDatasetName("readDataSet20");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// v - condition vererek date kolonun Count değerini alma
	public void v_CountControlForDate_WithCondition() {

		int ExpectedCount = 2;
		int result = 0;
		CountControlForForDate_WithCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet21");
		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result += diff.intValue();
		}

		System.out.println("Sonuç :" + result);
		Assert.assertEquals(ExpectedCount, result);

	}

	public void CountControlForForDate_WithCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME5");
		readDataWorkItem.getArguments().put("Column", "X5");
		readDataWorkItem.getArguments().put("Condition", "X1='AA' OR X1='CC'");
		readDataWorkItem.getArguments().put("Aggregate Function", "COUNT");

		//readDataWorkItem.setDatasetName("readDataSet21");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// w - condition vermeden string kolonun Count değerini alma
	public void w_CountControlForString_NoCondition() {

		int ExpectedCount = 3;
		int result = 0;
		CountControlForString_NoCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet22");
		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result += diff.intValue();
		}

		System.out.println("Sonuç :" + result);
		Assert.assertEquals(ExpectedCount, result);

	}

	public void CountControlForString_NoCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X2");
		readDataWorkItem.getArguments().put("Condition", "");
		readDataWorkItem.getArguments().put("Aggregate Function", "COUNT");

		//readDataWorkItem.setDatasetName("readDataSet22");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// x- condition vererek string kolonun Count değerini alma
	public void x_CountControlForString_WithCondition() {

		int ExpectedCount = 2;
		int result = 0;
		CountControlForForString_WithCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet23");
		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result += diff.intValue();
		}

		System.out.println("Sonuç :" + result);
		Assert.assertEquals(ExpectedCount, result);
	}

	public void CountControlForForString_WithCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X2");
		readDataWorkItem.getArguments().put("Condition", "X1='AA' OR X1='JJ'");
		readDataWorkItem.getArguments().put("Aggregate Function", "COUNT");

		//readDataWorkItem.setDatasetName("readDataSet23");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// v- condition vermeden integer kolonun AVG değerini alma
	public void v_AvgControlForInteger_NoCondition() {

		int ExpectedCount = 2;
		int result = 0;
		AvgControlForInteger_NoCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet24");
		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result += diff.intValue();
		}

		System.out.println("Sonuç :" + result);
		Assert.assertEquals(ExpectedCount, result);

	}

	public void AvgControlForInteger_NoCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X3");
		readDataWorkItem.getArguments().put("Condition", "");
		readDataWorkItem.getArguments().put("Aggregate Function", "AVG");

		//readDataWorkItem.setDatasetName("readDataSet24");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// y - condition vererek integer kolonun AVG değerini alma
	public void y_AvgControlForInteger_WithCondition() {

		float ExpectedCount = 2.5f;
		float result = 0;
		AvgControlForInteger_WithCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet25");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result = diff.floatValue();
		}

		System.out.println("Sonuç :" + result);
		Assert.assertEquals(ExpectedCount, result, 1e-15);

	}

	public void AvgControlForInteger_WithCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X3");
		readDataWorkItem.getArguments().put("Condition", "X1='AA' OR X1='CC'");
		readDataWorkItem.getArguments().put("Aggregate Function", "AVG");

		//readDataWorkItem.setDatasetName("readDataSet25");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// z - condition vermeden date kolonun AVG değerini alma
	public void z_AvgControlForDate_NoCondition() {

		AvgControlForDate_NoCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet26");

	}

	public void AvgControlForDate_NoCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME5");
		readDataWorkItem.getArguments().put("Column", "X5");
		readDataWorkItem.getArguments().put("Condition", "");
		readDataWorkItem.getArguments().put("Aggregate Function", "AVG");

		//readDataWorkItem.setDatasetName("readDataSet26");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// z1 - condition vererek date kolonun AVG değerini alma
	public void z1_AvgControlForDate_WithCondition() {

		AvgControlForForDate_WithCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet27");

	}

	public void AvgControlForForDate_WithCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME5");
		readDataWorkItem.getArguments().put("Column", "X5");
		readDataWorkItem.getArguments().put("Condition", "X1='AA' OR X1='CC'");
		readDataWorkItem.getArguments().put("Aggregate Function", "AVG");

		//readDataWorkItem.setDatasetName("readDataSet27");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// z2 - condition vermeden string kolonun Count değerini alma
	public void z2_AvgControlForString_NoCondition() {

		AvgControlForString_NoCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet28");
	}

	public void AvgControlForString_NoCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X2");
		readDataWorkItem.getArguments().put("Condition", "");
		readDataWorkItem.getArguments().put("Aggregate Function", "AVG");

		//readDataWorkItem.setDatasetName("readDataSet28");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// z3- condition vererek string kolonun Count değerini alma
	public void z3_AvgControlForString_WithCondition() {

		AvgControlForForString_WithCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet29");
	}

	public void AvgControlForForString_WithCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X2");
		readDataWorkItem.getArguments().put("Condition", "X1='AA' OR X1='JJ'");
		readDataWorkItem.getArguments().put("Aggregate Function", "AVG");

		//readDataWorkItem.setDatasetName("readDataSet29");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// z5 - hatalı tablo adı verilerek sum almak
	public void z5_WrongTableName() {

		WrongTableName();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet30");
	}

	public void WrongTableName() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME422");
		readDataWorkItem.getArguments().put("Column", "X2");
		readDataWorkItem.getArguments().put("Condition", "X1='AA' OR X1='JJ'");
		readDataWorkItem.getArguments().put("Aggregate Function", "SUM");

		//readDataWorkItem.setDatasetName("readDataSet30");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// z6 - hatalı kolon adı verilerek sum almak
	public void z6_WrongColumnName() {

		WrongColumnName();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet31");
	}

	public void WrongColumnName() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "X");
		readDataWorkItem.getArguments().put("Condition", "X1='AA' OR X1='JJ'");
		readDataWorkItem.getArguments().put("Aggregate Function", "SUM");

		//readDataWorkItem.setDatasetName("readDataSet31");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// z7 - tablo adı boş geçilecek
	public void z7_NullTableName() {

		NullTableName();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet32");
	}

	public void NullTableName() {
		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "");
		readDataWorkItem.getArguments().put("Column", "X3");
		readDataWorkItem.getArguments().put("Condition", "X1='AA' OR X1='JJ'");
		readDataWorkItem.getArguments().put("Aggregate Function", "SUM");

		//readDataWorkItem.setDatasetName("readDataSet32");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// z8 - kolon adı boş geçilecek
	public void z8_NullColumnName() {

		NullColumnName();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet33");
	}

	public void NullColumnName() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "");
		readDataWorkItem.getArguments().put("Condition", "X1='AA' OR X1='JJ'");
		readDataWorkItem.getArguments().put("Aggregate Function", "SUM");

		//readDataWorkItem.setDatasetName("readDataSet33");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// z9 - hatalı koşul verilerek sum almak
	public void z9_WrongCondition() {

		WrongCondition();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet34");
	}

	public void WrongCondition() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME4");
		readDataWorkItem.getArguments().put("Column", "");
		readDataWorkItem.getArguments().put("Condition", "X1='AA' OR X5='JJ'");
		readDataWorkItem.getArguments().put("Aggregate Function", "SUM");

		//readDataWorkItem.setDatasetName("readDataSet34");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// z10 - içinde null satır olan bir kolonu SUM almak

	public void z10_SumControlForInteger_ContainsNullRow() {

		int ExpectedCount = 4;
		int result = 0;
		SumControlForInteger_ContainsNullRow();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet35");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result += diff.intValue();
		}

		System.out.println("Sonuç :" + result);
		Assert.assertEquals(ExpectedCount, result);

	}

	public void SumControlForInteger_ContainsNullRow() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME6");
		readDataWorkItem.getArguments().put("Column", "X3");
		readDataWorkItem.getArguments().put("Condition", "");
		readDataWorkItem.getArguments().put("Aggregate Function", "SUM");

		//readDataWorkItem.setDatasetName("readDataSet35");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// z11 - içinde null satır olan bir kolonu AVG almak

	public void z11_AvgControlForInteger_ContainsNullRow() {

		int ExpectedCount = 2;
		int result = 0;
		AvgControlForInteger_ContainsNullRow();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet36");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result += diff.intValue();
		}

		System.out.println("Sonuç :" + result);
		Assert.assertEquals(ExpectedCount, result);

	}

	public void AvgControlForInteger_ContainsNullRow() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME6");
		readDataWorkItem.getArguments().put("Column", "X3");
		readDataWorkItem.getArguments().put("Condition", "");
		readDataWorkItem.getArguments().put("Aggregate Function", "AVG");

		//readDataWorkItem.setDatasetName("readDataSet36");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// z12 - içinde null satır olan bir kolonu COUNT almak
	public void z12_CountControlForInteger_ContainsNullRow() {

		int ExpectedCount = 2;
		int result = 0;
		CountControlForInteger_ContainsNullRow();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet37");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result += diff.intValue();
		}

		System.out.println("Sonuç :" + result);
		Assert.assertEquals(ExpectedCount, result);

	}

	public void CountControlForInteger_ContainsNullRow() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME6");
		readDataWorkItem.getArguments().put("Column", "X3");
		readDataWorkItem.getArguments().put("Condition", "");
		readDataWorkItem.getArguments().put("Aggregate Function", "COUNT");

		//readDataWorkItem.setDatasetName("readDataSet37");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// z13 - içinde null satır olan bir kolonu MAX almak

	public void z13_MaxControlForInteger_ContainsNullRow() {

		int ExpectedCount = 3;
		int result = 0;
		MaxControlForInteger_ContainsNullRow();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet38");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result += diff.intValue();
		}

		System.out.println("Sonuç :" + result);
		Assert.assertEquals(ExpectedCount, result);

	}

	public void MaxControlForInteger_ContainsNullRow() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME6");
		readDataWorkItem.getArguments().put("Column", "X3");
		readDataWorkItem.getArguments().put("Condition", "");
		readDataWorkItem.getArguments().put("Aggregate Function", "MAX");

		//readDataWorkItem.setDatasetName("readDataSet38");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

	@Test
	// z14 - içinde null satır olan bir kolonu MIN almak

	public void z14_MinControlForInteger_ContainsNullRow() {

		int ExpectedCount = 1;
		int result = 0;
		MinControlForInteger_ContainsNullRow();
		IgniteCache<Object, Object> cache1 = ignite.cache("readDataSet39");

		for (Entry<Object, Object> entry : cache1) {
			Row row = (Row)entry.getValue();
			BigDecimal diff = (BigDecimal)row.get("VALUE");
			result += diff.intValue();
		}

		System.out.println("Sonuç :" + result);
		Assert.assertEquals(ExpectedCount, result);

	}

	public void MinControlForInteger_ContainsNullRow() {

		WorkItem readDataWorkItem = new WorkItem();

		readDataWorkItem.getArguments().put("Connection", "1231");
		readDataWorkItem.getArguments().put("Table", "DENEME6");
		readDataWorkItem.getArguments().put("Column", "X3");
		readDataWorkItem.getArguments().put("Condition", "");
		readDataWorkItem.getArguments().put("Aggregate Function", "MIN");

		//readDataWorkItem.setDatasetName("readDataSet39");
		DataLink link = DatabaseUtils.getDataLink();
		readDataWorkItem.getLinks().put("1231", link);
		ignite.compute().execute("ReadAggregateColumn", readDataWorkItem);
	}

}

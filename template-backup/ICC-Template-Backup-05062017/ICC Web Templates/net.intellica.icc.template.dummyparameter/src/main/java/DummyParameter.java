

import net.intellica.icc.template.dummywait.DummyParameterJob;
import net.intellica.icc.template.model.TemplateJob;
import net.intellica.icc.template.model.TemplateTask;

public class DummyParameter extends TemplateTask {

	private static final long serialVersionUID = 1L;

	@Override
	protected TemplateJob getJob(Object arg) {
		return new DummyParameterJob(arg);
	}

}

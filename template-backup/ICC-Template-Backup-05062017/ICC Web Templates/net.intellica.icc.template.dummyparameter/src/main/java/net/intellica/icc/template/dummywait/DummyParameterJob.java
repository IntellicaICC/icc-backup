package net.intellica.icc.template.dummywait;

import net.intellica.icc.template.model.TemplateJob;
import org.springframework.util.StringUtils;

public class DummyParameterJob extends TemplateJob {

	private static final long serialVersionUID = 1L;

	public static final String PARAMETER_NAME = "ParameterName";
	public static final String PARAMETER_VALUE = "ParameterValue";

	public DummyParameterJob(Object arg) {
		super(arg);
	}

	@Override
	public void run() throws Exception {
	}

	@Override
	protected void setResults() {
		if (!isEmpty(getVariable(PARAMETER_NAME)))
			putIntoResultCache(getVariable(PARAMETER_NAME), getVariable(PARAMETER_VALUE));
	}

	private Boolean isEmpty(String s) {
		return StringUtils.isEmpty(s) || StringUtils.isEmpty(s.trim());
	}

}

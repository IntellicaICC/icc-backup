package net.intellica.icc.template.readfile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.JDBCType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import net.intellica.icc.template.exception.IOException;
import net.intellica.icc.template.exception.ParseException;
import net.intellica.icc.template.exception.ValidationException;
import net.intellica.icc.template.model.Row;
import net.intellica.icc.template.model.TemplateJob;
import org.apache.ignite.IgniteException;
import org.springframework.util.StringUtils;

public class ReadFileJob extends TemplateJob {

	// CONSTANTS
	private static final long serialVersionUID = 1L;
	private static final String DEFAULT_SEPARATOR = ",";
	private static final String NUMBER_REGEX = "\\d+";
	private static final List<String> ALLOWED_TYPES = Arrays.asList(
		"NUMBER",
		"CHAR"//
	);

	// TEMPLATE VARIABLES
	public static final String FILE_PATH = "FilePath";
	public static final String WAIT_FOR_FILE = "WaitForFile";
	public static final String SEPARATOR = "Separator";
	public static final String COLUMNS = "Columns";
	public static final String KEYS = "Keys";
	public static final String TYPES = "Types";
	public static final String LENGTHS = "Lengths";
	public static final String HEADER_ROW = "HeaderRow";
	public static final String COPY_AFTER_READ = "CopyAfterRead";
	public static final String COPY_PATH = "CopyPath";
	public static final String COPY_OVERWRITE = "CopyOverwrite";
	public static final String DELETE_AFTER_READ = "DeleteAfterRead";

	// RESULTS
	public static final String RESULT_READ_COUNT = "ReadCount";

	// INITIALIZED VARIABLES
	private File file;
	private String separator;
	private List<String> columnNames;
	private List<Integer> columnIndices;
	private List<String> keyNames;
	private List<Integer> keyIndices;
	private List<JDBCType> columnTypes;
	private List<Integer> columnLengths;
	private File copyPath;

	public ReadFileJob(Object arg) {
		super(arg);
	}

	@Override
	protected void validatialization() throws Exception {
		columnNames = new ArrayList<>();
		columnIndices = new ArrayList<>();
		keyNames = new ArrayList<>();
		keyIndices = new ArrayList<>();
		columnTypes = new ArrayList<>();
		columnLengths = new ArrayList<>();
		String filePath = getVariable(FILE_PATH).trim();
		try {
			file = new File(filePath);
			if (Boolean.parseBoolean(getVariable(WAIT_FOR_FILE))) {
				while (!file.exists()) {
					waitFor(1000);
				}
			}
			if (!file.isFile())
				throw new ValidationException(null, "Unable to access given path '" + filePath + "'!");
		}
		catch (Exception e) {
			throw new ValidationException(null, "Unable to access given path '" + filePath + "'!");
		}
		Boolean useSeparator = false;
		separator = getVariable(SEPARATOR);
		if (isStringEmpty(separator)) {
			if (isStringEmpty(getVariable(LENGTHS))) {
				separator = DEFAULT_SEPARATOR;
				useSeparator = true;
			}
			else {
				String lengthString = getVariable(LENGTHS);
				List<String> lengths = Arrays.asList(lengthString.split(",")).stream()
					.filter(c -> !isStringEmpty(c))
					.map(c -> c.trim())
					.collect(Collectors.toList());
				if (lengths.size() == 0)
					throw new ValidationException(null, "Constant column lengths must be specified when separator is not used!");
				if (lengths.stream().allMatch(c -> c.matches(NUMBER_REGEX)))
					lengths.stream().map(Integer::parseInt).forEach(columnLengths::add);
				else
					throw new ValidationException(null, "Column lengths must be integers!");
			}
		}
		else {
			separator = separator.trim();
			useSeparator = true;
		}
		String columnString = getVariable(COLUMNS);
		List<String> columns = Arrays.asList(columnString.split(",")).stream()
			.filter(c -> !isStringEmpty(c))
			.map(c -> c.trim())
			.collect(Collectors.toList());
		if (columns.size() == 0)
			throw new ValidationException(null, "All columns to be read must be specified, either by name or number!");
		else if (!useSeparator && columns.size() > columnLengths.size())
			throw new ValidationException(null, "Number of column names exceed number of column lengths!");
		if (columns.stream().allMatch(c -> c.matches(NUMBER_REGEX))) {
			columns.stream().map(Integer::parseInt).map(i -> i - 1).forEach(columnIndices::add);
			if (!useSeparator) {
				Optional<Integer> optional = columnIndices.stream().filter(i -> i > columnLengths.size()).findFirst();
				if (optional.isPresent())
					throw new ValidationException(null, "Column index '" + optional.get() + "' exceeds the number of columns!");
			}
		}
		List<String> sampleRow = new ArrayList<>();
		try (FileReader fReader = new FileReader(file)) {
			try (BufferedReader bReader = new BufferedReader(fReader)) {
				String line;
				while ((line = bReader.readLine()) != null) {
					if (useSeparator) {
						sampleRow.addAll(Arrays.asList(line.split(Pattern.quote(separator), -1)));
					}
					else {
						Integer lastIndex = 0;
						for (Integer length : columnLengths) {
							String value = line.substring(lastIndex, lastIndex + length).trim();
							sampleRow.add(value);
							lastIndex += length;
						}
					}
					break;
				}
			}
		}
		catch (Exception e) {
			throw new ValidationException(null, "Unable to read line from given file!");
		}
		if (Boolean.parseBoolean(getVariable(HEADER_ROW))) {
			// header row var
			if (columnIndices.size() == 0) {
				// columns degiskeni sutun isimleri
				for (String column : columns) {
					Boolean headerFound = false;
					for (String header : sampleRow) {
						if (column.equalsIgnoreCase(header)) {
							columnIndices.add(sampleRow.indexOf(header));
							columnNames.add(column);
							headerFound = true;
							break;
						}
					}
					if (!headerFound)
						throw new ValidationException(null, "Column '" + column + "' does not exist in the file!");
				}
			}
			else {
				// columns degiskeni sutun numaralari
				Optional<Integer> optional = columnIndices.stream().filter(i -> i > sampleRow.size()).findFirst();
				if (optional.isPresent())
					throw new ValidationException(null, "Given index (" + (optional.get() + 1) + ") exceeds the number of columns (" + sampleRow.size() + ") in the file!");
				columnIndices.forEach(i -> columnNames.add(sampleRow.get(i)));
			}
		}
		else {
			// header row yok
			if (columnIndices.size() == 0)
				// columns degiskeni sutun isimleri
				throw new ValidationException(null, "Can not determine columns to read since header row is absent!");
			else {
				// columns degiskeni sutun numaralari
				Optional<Integer> optional = columnIndices.stream().filter(i -> i > sampleRow.size()).findFirst();
				if (optional.isPresent())
					throw new ValidationException(null, "Column index (" + (optional.get() + 1) + ") exceeds the number of columns (" + sampleRow.size() + ") in the file!");
				columnIndices.forEach(i -> columnNames.add("COLUMN_" + (i + 1)));
			}
		}
		String keyString = getVariable(KEYS);
		List<String> keys = Arrays.asList(keyString.split(",")).stream()
			.filter(c -> !isStringEmpty(c))
			.map(c -> c.trim())
			.collect(Collectors.toList());
		if (keys.size() == 0)
			throw new ValidationException(null, "All key columns must be specified, either by name or number!");
		if (keys.stream().allMatch(c -> c.matches(NUMBER_REGEX)))
			keys.stream().map(Integer::parseInt).map(i -> i - 1).forEach(keyIndices::add);
		if (Boolean.parseBoolean(getVariable(HEADER_ROW))) {
			// header row var
			if (keyIndices.size() == 0) {
				// keys degiskeni sutun isimleri
				for (String key : keys) {
					Boolean headerFound = false;
					for (String header : sampleRow) {
						if (key.equalsIgnoreCase(header)) {
							keyIndices.add(sampleRow.indexOf(header));
							keyNames.add(key);
							headerFound = true;
							break;
						}
					}
					if (!headerFound)
						throw new ValidationException(null, "Key column '" + key + "' does not exist in the file!");
				}
			}
			else {
				// keys degiskeni sutun numaralari
				Optional<Integer> optional = keyIndices.stream().filter(i -> i > sampleRow.size()).findFirst();
				if (optional.isPresent())
					throw new ValidationException(null, "Key index (" + (optional.get() + 1) + ") exceeds the number of columns (" + sampleRow.size() + ") in the file!");
				keyIndices.forEach(i -> keyNames.add(sampleRow.get(i)));
			}
		}
		else {
			// header row yok
			if (keyIndices.size() == 0)
				// keys degiskeni sutun isimleri
				throw new ValidationException(null, "Can not determine key columns since header row is absent!");
			else {
				// keys degiskeni sutun numaralari
				Optional<Integer> optional = keyIndices.stream().filter(i -> i > sampleRow.size()).findFirst();
				if (optional.isPresent())
					throw new ValidationException(null, "Key index (" + (optional.get() + 1) + ") exceeds the number of columns (" + sampleRow.size() + ") in the file!");
				keyIndices.forEach(i -> keyNames.add("COLUMN_" + (i + 1)));
			}
		}
		String typeString = getVariable(TYPES);
		List<String> types = Arrays.asList(typeString.split(",")).stream()
			.filter(c -> !isStringEmpty(c))
			.map(c -> c.trim())
			.collect(Collectors.toList());
		if (types.size() == 0)
			throw new ValidationException(null, "All column types must be specified!");
		else if (types.size() != columns.size())
			throw new ValidationException(null, "Column types and column names are inconsistent!");
		else {
			for (String type : types) {
				Boolean typeFound = false;
				for (String allowedType : ALLOWED_TYPES) {
					if (allowedType.equalsIgnoreCase(type)) {
						switch (allowedType) {
							case "NUMBER":
								columnTypes.add(JDBCType.NUMERIC);
								break;
							case "CHAR":
								columnTypes.add(JDBCType.VARCHAR);
								break;
							default:
								break;
						}
						typeFound = true;
						break;
					}
				}
				if (!typeFound)
					throw new ValidationException(null, "Allowed database types are " + String.join(",", ALLOWED_TYPES) + ".");
			}
		}
		if (Boolean.parseBoolean(getVariable(COPY_AFTER_READ))) {
			if (isStringEmpty(getVariable(COPY_PATH)))
				throw new ValidationException(null, "Copy path can not be left empty!");
			copyPath = new File(getVariable(COPY_PATH).trim());
			if (copyPath.isFile() && !Boolean.parseBoolean(getVariable(COPY_OVERWRITE)))
				throw new ValidationException(null, "Copy path already contains a file with the same name!");
		}
	}

	@Override
	public void run() throws Exception {
		try (FileReader fReader = new FileReader(file)) {
			try (BufferedReader bReader = new BufferedReader(fReader)) {
				String line;
				Integer lineNumber = 0;
				while ((line = bReader.readLine()) != null) {
					if (lineNumber > 0 || !Boolean.parseBoolean(getVariable(HEADER_ROW))) {
						List<String> values = new ArrayList<>();
						if (isStringEmpty(separator)) {
							Integer lastIndex = 0;
							for (Integer length : columnLengths) {
								String value = line.substring(lastIndex, lastIndex + length).trim();
								values.add(value);
								lastIndex += length;
							}
						}
						else {
							values.addAll(Arrays.asList(line.split(Pattern.quote(separator), -1)));
						}
						Row row = createNewRow();
						for (int i = 0; i < columnIndices.size(); i++) {
							Object value = parseValue(values.get(columnIndices.get(i)), columnTypes.get(i), lineNumber + 1, columnIndices.get(i) + 1);
							row.put(i, value);
						}
						String keyString = row.getKeyString();
						if (row.getKeyValues().stream().anyMatch(v -> v == null))
							throw new IgniteException("Null key detected! (" + keyString + ")");
						else if (cacheKeyExists(keyString))
							throw new IgniteException("Duplicate key detected! (" + keyString + ")");
						putIntoCache(keyString, row);
					}
					lineNumber++;
				}
				putIntoResultCache(RESULT_READ_COUNT, lineNumber);
			}
		}
		if (Boolean.parseBoolean(getVariable(COPY_AFTER_READ))) {
			Path source = file.toPath();
			Path target = copyPath.toPath();
			if (copyPath.isFile()) {
				if (Boolean.parseBoolean(getVariable(COPY_OVERWRITE))) {
					Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
				}
				else
					throw new IOException("Copy path already contains a file with the same name!");
			}
			else if (copyPath.isDirectory()) {
				for (File folderFile : copyPath.listFiles()) {
					if (folderFile.getName().equals(file.getName()) && !Boolean.parseBoolean(getVariable(COPY_OVERWRITE)))
						throw new IOException("Copy path already contains a file with the same name!");
				}
				Files.copy(source, target.resolve(source.getFileName()), StandardCopyOption.REPLACE_EXISTING);
			}
			else {
				// dosya ve/veya dizin henuz yok
				if (copyPath.getName().contains(".")) {
					// bu bir dosya adi gibi davran
					if (copyPath.getParentFile() != null) {
						if (!copyPath.getParentFile().exists())
							copyPath.getParentFile().mkdirs();
						Files.copy(source, target);
					}
					else
						throw new IOException("Unknown copy path '" + copyPath + "'!");
				}
				else {
					// bu bir klasor adi gibi davran
					copyPath.mkdirs();
					Files.copy(source, target);
				}
			}
		}
		if (Boolean.parseBoolean(getVariable(DELETE_AFTER_READ))) {
			file.delete();
		}
	}

	private Row createNewRow() {
		Row result = new Row();
		for (int i = 0; i < columnNames.size(); i++) {
			Integer length = 0;
			if (columnTypes.get(i) == JDBCType.VARCHAR)
				length = 2000;
			result.addColumn(columnNames.get(i), columnTypes.get(i), length, null);
		}
		keyNames.forEach(k -> result.getKeyColumns().add(k));
		return result;
	}

	private Object parseValue(String value, JDBCType type, Integer lineNumber, Integer columnNumber) {
		Object result = null;
		switch (type) {
			case NUMERIC:
				try {
					result = new BigDecimal(value);
				}
				catch (Exception e) {
					throw new ParseException("Unable to parse value '" + value + "' at line " + lineNumber + ", column " + columnNumber + " as number!");
				}
				break;
			case VARCHAR:
				result = value;
				break;
			default:
				break;
		}
		return result;
	}

	private Boolean isStringEmpty(String s) {
		return StringUtils.isEmpty(s) || StringUtils.isEmpty(s.trim());
	}

}

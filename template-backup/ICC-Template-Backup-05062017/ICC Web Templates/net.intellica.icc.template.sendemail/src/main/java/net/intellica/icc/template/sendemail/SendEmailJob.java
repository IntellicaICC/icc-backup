package net.intellica.icc.template.sendemail;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import net.intellica.icc.template.exception.ValidationException;
import net.intellica.icc.template.model.TemplateJob;
import org.springframework.util.StringUtils;

public class SendEmailJob extends TemplateJob {

	// CONSTANTS
	private static final long serialVersionUID = 1L;
	private static final String EMAIL_REGEX = "[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}";

	// TEMPLATE VARIABLES
	public static final String TO = "To";
	public static final String CC = "Cc";
	public static final String SUBJECT = "Subject";
	public static final String TEXT = "Text";

	// INTERNAL VARIABLES
	private Set<String> toAddresses;
	private Set<String> ccAddresses;

	public SendEmailJob(Object arg) {
		super(arg);
	}

	@Override
	protected void validatialization() throws Exception {
		toAddresses = Arrays.asList(getVariable(TO).split(",")).stream()
			.filter(s -> !StringUtils.isEmpty(s))
			.map(String::trim)
			.filter(s -> !StringUtils.isEmpty(s))
			.collect(Collectors.toSet());
		ccAddresses = Arrays.asList(getVariable(CC).split(",")).stream()
			.filter(s -> !StringUtils.isEmpty(s))
			.map(String::trim)
			.filter(s -> !StringUtils.isEmpty(s))
			.collect(Collectors.toSet());
		if (toAddresses.size() == 0)
			throw new ValidationException(null, "'to' field is required!");
		for (String toAddress : toAddresses) {
			if (!toAddress.matches(EMAIL_REGEX))
				throw new ValidationException(null, "'" + toAddress + "' is not a valid email address!");
		}
		for (String ccAddress : ccAddresses) {
			if (!ccAddress.matches(EMAIL_REGEX))
				throw new ValidationException(null, "'" + ccAddress + "' is not a valid email address!");
		}
	}

	@Override
	public void run() throws Exception {
		sendEmail(toAddresses, ccAddresses, getVariable(SUBJECT), getVariable(TEXT));
	}

}
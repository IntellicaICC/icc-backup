

import net.intellica.icc.template.columndifference.ColumnDifferenceJob;
import net.intellica.icc.template.model.TemplateJob;
import net.intellica.icc.template.model.TemplateTask;

public class ColumnDifference extends TemplateTask {

	private static final long serialVersionUID = 1L;

	@Override
	protected TemplateJob getJob(Object arg) {
		return new ColumnDifferenceJob(arg);
	}

}

package net.intellica.icc.template.columndifference;

import java.math.BigDecimal;
import java.sql.JDBCType;
import javax.cache.Cache.Entry;
import net.intellica.icc.template.exception.ValidationException;
import net.intellica.icc.template.model.CacheProcessor;
import net.intellica.icc.template.model.Row;
import net.intellica.icc.template.model.TemplateJob;
import org.apache.ignite.IgniteException;
import org.springframework.util.StringUtils;

public class ColumnDifferenceJob extends TemplateJob {

	private static final long serialVersionUID = 1L;

	// PARAMETERS
	public static final String FIRST_DATASET = "FirstDataSet";
	public static final String FIRST_COLUMN = "FirstColumn";
	public static final String SECOND_DATASET = "SecondDataSet";
	public static final String SECOND_COLUMN = "SecondColumn";

	// REPORT COLUMNS
	public static final String COLUMN_ID = "ID";
	public static final String COLUMN_KEY = "KEY";
	public static final String COLUMN_FIRST_COLUMN = "FIRST_COLUMN";
	public static final String COLUMN_FIRST_VALUE = "FIRST_VALUE";
	public static final String COLUMN_SECOND_COLUMN = "SECOND_COLUMN";
	public static final String COLUMN_SECOND_VALUE = "SECOND_VALUE";
	public static final String COLUMN_DIFFERENCE = "DIFFERENCE";
	public static final String COLUMN_ABS_DIFFERENCE = "ABS_DIFFERENCE";

	// REPORT COLUMN TYPES
	public static final JDBCType TYPE_ID = JDBCType.NUMERIC;
	public static final JDBCType TYPE_KEY = JDBCType.VARCHAR;
	public static final JDBCType TYPE_FIRST_COLUMN = JDBCType.VARCHAR;
	public static final JDBCType TYPE_SECOND_COLUMN = JDBCType.VARCHAR;
	public static final JDBCType TYPE_FIRST_VALUE = JDBCType.NUMERIC;
	public static final JDBCType TYPE_SECOND_VALUE = JDBCType.NUMERIC;
	public static final JDBCType TYPE_DIFFERENCE = JDBCType.NUMERIC;
	public static final JDBCType TYPE_ABS_DIFFERENCE = JDBCType.NUMERIC;

	// REPORT COLUMN LENGTHS
	public static final Integer LENGTH_ID = 0;
	public static final Integer LENGTH_KEY = 2000;
	public static final Integer LENGTH_FIRST_COLUMN = 2000;
	public static final Integer LENGTH_SECOND_COLUMN = 2000;
	public static final Integer LENGTH_FIRST_VALUE = 0;
	public static final Integer LENGTH_SECOND_VALUE = 0;
	public static final Integer LENGTH_DIFFERENCE = 0;
	public static final Integer LENGTH_ABS_DIFFERENCE = 0;

	// TEMPLATE RESULTS
	public static final String RESULT_MAX_DIFFERENCE = "MaxDifference";
	public static final String RESULT_MIN_DIFFERENCE = "MinDifference";
	public static final String RESULT_MAX_ABS_DIFFERENCE = "MaxAbsDifference";
	public static final String RESULT_MIN_ABS_DIFFERENCE = "MinAbsDifference";

	// GRID VARIABLES
	public static final String VARIABLE_INDEX = "INDEX";

	// INTERNAL VARIABLES
	private String firstColumn;
	private String secondColumn;

	public ColumnDifferenceJob(Object arg) {
		super(arg);
	}

	@Override
	protected void validatialization() throws Exception {
		if (!cacheExists(getVariable(FIRST_DATASET)))
			throw new ValidationException(null, "First dataset does not exist!");
		Row firstSample = (Row)getSampleFromCache(getVariable(FIRST_DATASET));
		firstColumn = getVariable(FIRST_COLUMN).trim();
		if (!StringUtils.isEmpty(firstColumn)) {
			if (!firstSample.getRowMetadata().getColumns().stream().anyMatch(c -> c.equalsIgnoreCase(firstColumn)))
				throw new ValidationException(null, "First dataset does not contain a column named " + firstColumn + "!");
			if (!JDBC_NUMERICS.contains(firstSample.getRowMetadata().getTypes().get(firstSample.getRowMetadata().getColumns().indexOf(firstColumn))))
				throw new ValidationException(null, "First column must be of numeric column type!");
		}
		else
			firstColumn = (String)firstSample.getRowMetadata().getColumns().get(findFirstNumericColumn(firstSample));
		if (!cacheExists(getVariable(SECOND_DATASET)))
			throw new ValidationException(null, "Second dataset does not exist!");
		Row secondSample = (Row)getSampleFromCache(getVariable(SECOND_DATASET));
		secondColumn = getVariable(SECOND_COLUMN).trim();
		if (!StringUtils.isEmpty(secondColumn)) {
			if (!secondSample.getRowMetadata().getColumns().stream().anyMatch(c -> c.equalsIgnoreCase(secondColumn)))
				throw new ValidationException(null, "Second dataset does not contain a column named " + secondColumn + "!");
			if (!JDBC_NUMERICS.contains(secondSample.getRowMetadata().getTypes().get(secondSample.getRowMetadata().getColumns().indexOf(secondColumn))))
				throw new ValidationException(null, "Second column must be of numeric column type!");
		}
		else
			secondColumn = (String)secondSample.getRowMetadata().getColumns().get(findFirstNumericColumn(secondSample));
		createGridLong(VARIABLE_INDEX, 1L);
		createGridVariable(RESULT_MAX_DIFFERENCE, new BigDecimal(Long.MIN_VALUE));
		createGridVariable(RESULT_MAX_ABS_DIFFERENCE, new BigDecimal(Long.MIN_VALUE));
		createGridVariable(RESULT_MIN_DIFFERENCE, new BigDecimal(Long.MAX_VALUE));
		createGridVariable(RESULT_MIN_ABS_DIFFERENCE, new BigDecimal(Long.MAX_VALUE));
	}

	@Override
	public void run() throws Exception {
		localCacheRun(getVariable(FIRST_DATASET), new DataProcessor());
	}

	public class DataProcessor implements CacheProcessor {
		@Override
		public void process(Iterable<Entry<Object, Object>> entries) {
			logDebugMessage("Starting iteration of dataset...");
			BigDecimal maxDifference = new BigDecimal(Long.MIN_VALUE);
			BigDecimal minDifference = new BigDecimal(Long.MAX_VALUE);
			BigDecimal maxAbsDifference = new BigDecimal(Long.MIN_VALUE);
			BigDecimal minAbsDifference = new BigDecimal(Long.MAX_VALUE);
			for (Entry<Object, Object> entry : entries) {
				String key = (String)entry.getKey();
				Row firstRow = (Row)entry.getValue();
				Row secondRow = (Row)getFromCache(getVariable(SECOND_DATASET), key);
				if (secondRow != null) {
					BigDecimal firstValue = castToBigDecimal(firstRow.get(firstColumn));
					BigDecimal secondValue = castToBigDecimal(secondRow.get(secondColumn));
					BigDecimal difference = null;
					BigDecimal absDifference = null;

					Row row = createNewRow();
					if (firstValue != null && secondValue != null) {
						difference = firstValue.subtract(secondValue);
						if (difference.compareTo(maxDifference) > 0)
							maxDifference = difference;
						if (difference.compareTo(minDifference) < 0)
							minDifference = difference;

						absDifference = difference.abs();
						if (absDifference.compareTo(maxAbsDifference) > 0)
							maxAbsDifference = absDifference;
						if (absDifference.compareTo(minAbsDifference) < 0)
							minAbsDifference = absDifference;
					}

					row.put(COLUMN_ID, getAndIncrementGridLong(VARIABLE_INDEX));
					row.put(COLUMN_KEY, key);
					row.put(COLUMN_FIRST_COLUMN, firstColumn);
					row.put(COLUMN_FIRST_VALUE, firstValue);
					row.put(COLUMN_SECOND_COLUMN, secondColumn);
					row.put(COLUMN_SECOND_VALUE, secondValue);
					row.put(COLUMN_DIFFERENCE, difference);
					row.put(COLUMN_ABS_DIFFERENCE, absDifference);

					putIntoCache(row.getKeyString(), row);
				}
			}
			BigDecimal maxd = maxDifference;
			BigDecimal mind = minDifference;
			BigDecimal maxad = maxAbsDifference;
			BigDecimal minad = minAbsDifference;
			setGridVariable(RESULT_MAX_DIFFERENCE, d -> {
				BigDecimal result = (BigDecimal)d;
				if (maxd.compareTo((BigDecimal)d) > 0)
					result = maxd;
				return result;
			});
			setGridVariable(RESULT_MIN_DIFFERENCE, d -> {
				BigDecimal result = (BigDecimal)d;
				if (mind.compareTo((BigDecimal)d) < 0)
					result = mind;
				return result;
			});
			setGridVariable(RESULT_MAX_ABS_DIFFERENCE, d -> {
				BigDecimal result = (BigDecimal)d;
				if (maxad.compareTo((BigDecimal)d) > 0)
					result = maxad;
				return result;
			});
			setGridVariable(RESULT_MIN_ABS_DIFFERENCE, d -> {
				BigDecimal result = (BigDecimal)d;
				if (minad.compareTo((BigDecimal)d) < 0)
					result = minad;
				return result;
			});
			logDebugMessage("Finished iteration of dataset.");
		}
	}

	private Row createNewRow() {
		Row result = new Row();
		result
			.addColumn(COLUMN_ID, TYPE_ID, LENGTH_ID, null)
			.addColumn(COLUMN_KEY, TYPE_KEY, LENGTH_KEY, null)
			.addColumn(COLUMN_FIRST_COLUMN, TYPE_FIRST_COLUMN, LENGTH_FIRST_COLUMN, null)
			.addColumn(COLUMN_FIRST_VALUE, TYPE_FIRST_VALUE, LENGTH_FIRST_VALUE, null)
			.addColumn(COLUMN_SECOND_COLUMN, TYPE_SECOND_COLUMN, LENGTH_SECOND_COLUMN, null)
			.addColumn(COLUMN_SECOND_VALUE, TYPE_SECOND_VALUE, LENGTH_SECOND_VALUE, null)
			.addColumn(COLUMN_DIFFERENCE, TYPE_DIFFERENCE, LENGTH_DIFFERENCE, null)
			.addColumn(COLUMN_ABS_DIFFERENCE, TYPE_ABS_DIFFERENCE, LENGTH_ABS_DIFFERENCE, null);
		result.getKeyColumns().add(COLUMN_KEY);
		return result;
	}

	private Integer findFirstNumericColumn(Row row) {
		Integer result = null;
		for (int i = 0; i < row.getRowMetadata().getTypes().size(); i++) {
			if (JDBC_NUMERICS.contains(row.getRowMetadata().getTypes().get(i))) {
				result = i;
				logDebugMessage("Found numeric column '%s'", row.getRowMetadata().getColumns().get(result));
				break;
			}
		}
		if (result == null)
			throw new IgniteException("No numeric column exists! Comparison can not continue.");
		return result;
	}

	private BigDecimal castToBigDecimal(Object object) {
		BigDecimal result = null;
		if (object instanceof BigDecimal)
			result = (BigDecimal)object;
		else if (object instanceof Float)
			result = new BigDecimal((Float)object);
		else if (object instanceof Double)
			result = new BigDecimal((Double)object);
		else if (object instanceof Integer)
			result = new BigDecimal((Integer)object);
		else if (object != null)
			throw new IgniteException(String.format("Object type '%s' not supported for column difference!", object.getClass().getSimpleName()));
		return result;
	}

	@Override
	protected void setResults() {
		putIntoResultCache(RESULT_MAX_DIFFERENCE, getGridVariable(RESULT_MAX_DIFFERENCE));
		putIntoResultCache(RESULT_MAX_ABS_DIFFERENCE, getGridVariable(RESULT_MAX_ABS_DIFFERENCE));
		putIntoResultCache(RESULT_MIN_DIFFERENCE, getGridVariable(RESULT_MIN_DIFFERENCE));
		putIntoResultCache(RESULT_MIN_ABS_DIFFERENCE, getGridVariable(RESULT_MIN_ABS_DIFFERENCE));
	}

	@Override
	protected void cleanUp() {
		removeGridLong(VARIABLE_INDEX);
		removeGridVariable(RESULT_MAX_DIFFERENCE);
		removeGridVariable(RESULT_MAX_ABS_DIFFERENCE);
		removeGridVariable(RESULT_MIN_DIFFERENCE);
		removeGridVariable(RESULT_MIN_ABS_DIFFERENCE);
	}

}

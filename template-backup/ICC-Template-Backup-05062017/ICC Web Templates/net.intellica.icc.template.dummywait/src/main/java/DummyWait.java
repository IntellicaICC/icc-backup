

import net.intellica.icc.template.dummywait.DummyWaitJob;
import net.intellica.icc.template.model.TemplateJob;
import net.intellica.icc.template.model.TemplateTask;

public class DummyWait extends TemplateTask {

	private static final long serialVersionUID = 1L;

	@Override
	protected TemplateJob getJob(Object arg) {
		return new DummyWaitJob(arg);
	}

}

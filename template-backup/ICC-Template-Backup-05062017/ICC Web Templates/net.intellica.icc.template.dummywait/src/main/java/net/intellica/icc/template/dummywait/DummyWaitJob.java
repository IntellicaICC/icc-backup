package net.intellica.icc.template.dummywait;

import net.intellica.icc.template.model.TemplateJob;

public class DummyWaitJob extends TemplateJob {

	private static final long serialVersionUID = 1L;

	public static final String TIME_TO_WAIT = "TimeToWait";

	private static final Integer MIN_WAIT_MS = 1000;
	private static final Integer MAX_WAIT_MS = 300000;

	public DummyWaitJob(Object arg) {
		super(arg);
	}

	@Override
	public void run() {
		try {
			Integer wait = Integer.parseInt(getVariable(TIME_TO_WAIT)) * 1000;
			if (wait < MIN_WAIT_MS)
				wait = MIN_WAIT_MS;
			else if (wait > MAX_WAIT_MS)
				wait = MAX_WAIT_MS;
			logDebugMessage("Started wait for " + wait / 1000 + " seconds...");
			waitFor(wait);
		}
		catch (Exception e) {
		}
	}

}

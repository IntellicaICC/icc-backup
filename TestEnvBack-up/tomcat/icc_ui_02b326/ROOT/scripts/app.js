﻿var appVersion = "1.0.52";
var app = angular.module('iccApp', ["ngRoute", "ngContextMenu", "ngSanitize", "ui.dateTimeInput", "ngtimeago", "angucomplete-alt"])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.

                //EXPLORER ROUTES
                //when('/', { templateUrl: 'views/home.html?v=' + appVersion, controller: "homeController" }).
                when('/', { templateUrl: 'views/explorer.html?v=' + appVersion, controller: "explorerController" }).
                when('/login', { templateUrl: 'views/login.html?v=' + appVersion, controller: "loginController" }).
                when('/explorer', { templateUrl: 'views/explorer.html?v=' + appVersion, controller: "explorerController" }).
                when('/explorer/:folderId', { templateUrl: 'views/explorer.html?v=' + appVersion, controller: "explorerController" }).

                //JOB ROUTES
                when('/editjob/:jobId', { templateUrl: 'views/newjob.html?v=' + appVersion, controller: "newJobController" }).
                when('/newjob', { templateUrl: 'views/newjob.html?v=' + appVersion, controller: "newJobController" }).
                when('/newjob/:folderId', { templateUrl: 'views/newjob.html?v=' + appVersion, controller: "newJobController" }).

               //RULE ROUTES
                when('/editrule/:ruleId', { templateUrl: 'views/newrule.html?v=' + appVersion, controller: "newRuleController" }).
                when('/newrule', { templateUrl: 'views/newrule.html?v=' + appVersion, controller: "newRuleController" }).
                when('/newrule/:folderId', { templateUrl: 'views/newrule.html?v=' + appVersion, controller: "newRuleController" }).
                
                //USER ROUTES
                when('/edituser/:userId', { templateUrl: 'views/newuser.html?v=' + appVersion, controller: "newUserController" }).
                when('/newuser', { templateUrl: 'views/newuser.html?v=' + appVersion, controller: "newUserController" }).
                when('/newuser/:folderId', { templateUrl: 'views/newuser.html?v=' + appVersion, controller: "newUserController" }).

                //ROLE ROUTES
                when('/editrole/:roleId', { templateUrl: 'views/newrole.html?v=' + appVersion, controller: "newRoleController" }).
                when('/newrole', { templateUrl: 'views/newrole.html?v=' + appVersion, controller: "newRoleController" }).
                when('/newrole/:folderId', { templateUrl: 'views/newrole.html?v=' + appVersion, controller: "newRoleController" }).

                //FOLDER ROUTES
                when('/editfolder/:fid', { templateUrl: 'views/newfolder.html?v=' + appVersion, controller: "newFolderController" }).
                when('/newfolder', { templateUrl: 'views/newfolder.html?v=' + appVersion, controller: "newFolderController" }).
                when('/newfolder/:folderId', { templateUrl: 'views/newfolder.html?v=' + appVersion, controller: "newFolderController" }).

                //CONNECTION ROUTES
                when('/editconnection/:connectionId', { templateUrl: 'views/newconnection.html?v=' + appVersion, controller: "newConnectionController" }).
                when('/newconnection', { templateUrl: 'views/newconnection.html?v=' + appVersion, controller: "newConnectionController" }).
                when('/newconnection/:folderId', { templateUrl: 'views/newconnection.html?v=' + appVersion, controller: "newConnectionController" }).

                //LOCALPARAMETER ROUTES
                when('/editlocalparameter/:localParameterId', { templateUrl: 'views/newlocalparameter.html?v=' + appVersion, controller: "newLocalParameterController" }).
                when('/newlocalparameter', { templateUrl: 'views/newlocalparameter.html?v=' + appVersion, controller: "newLocalParameterController" }).
                when('/newlocalparameter/:folderId', { templateUrl: 'views/newlocalparameter.html?v=' + appVersion, controller: "newLocalParameterController" }).

                //GLOBALPARAMETER ROUTES
                when('/editglobalparameter/:globalParameterId', { templateUrl: 'views/newglobalparameter.html?v=' + appVersion, controller: "newGlobalParameterController" }).
                when('/newglobalparameter', { templateUrl: 'views/newglobalparameter.html?v=' + appVersion, controller: "newGlobalParameterController" }).
                when('/newglobalparameter/:folderId', { templateUrl: 'views/newglobalparameter.html?v=' + appVersion, controller: "newGlobalParameterController" }).


                when('/users', { templateUrl: 'views/users.html?v=' + appVersion, controller: "usersController" }).
                when('/roles', { templateUrl: 'views/roles.html?v=' + appVersion, controller: "rolesController" }).
                when('/connections', { templateUrl: 'views/connections.html?v=' + appVersion, controller: "connectionsController" }).
                when('/globalparameters', { templateUrl: 'views/globalparameters.html?v=' + appVersion, controller: "globalParametersController" }).
                when('/notifications', { templateUrl: 'views/notifications.html?v=' + appVersion, controller: "notificationsController" }).
                when('/monitoring', { templateUrl: 'views/monitoring.html?v=' + appVersion, controller: "monitoringController" }).
                when('/settings', { templateUrl: 'views/settings.html?v=' + appVersion, controller: "settingsController" }).
                when('/reports', { templateUrl: 'views/reports.html?v=' + appVersion, controller: "reportsController" }).
                when('/help', { templateUrl: 'views/help/index.html?v=' + appVersion, controller: "helpController" }).
                when('/help/:section', {
                    templateUrl: function (params) { return "views/help/" + params.section + "/index.html?v=" + appVersion; }, controller: "helpController"
                }). otherwise({ redirectTo: '/' });
    }]);


app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers.common               = {};
    $httpProvider.defaults.headers.post                 = {};
    $httpProvider.defaults.headers.put                  = {};
    $httpProvider.defaults.headers.patch                = {};
    $httpProvider.defaults.withCredentials              = true;
    $httpProvider.defaults.headers.post['Accept']       = 'application/json';
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
}]);



app.directive('ngRightClick', function ($parse) {
    return function (scope, element, attrs) {
        var fn = $parse(attrs.ngRightClick);
        element.bind('contextmenu', function (event) {
            scope.$apply(function () {
                event.preventDefault();
                fn(scope, { $event: event });
            });
        });
    };
});


//Help page left menu directive
app.directive('helpLeftMenu', function () {
    return {
        restrict: 'E',
        scope:true,
        link: function (scope, element, attrs) {
            scope.pageName = attrs.pageName;
        },
        templateUrl: 'views/help/leftMenu.html?v='+ appVersion
    }
});
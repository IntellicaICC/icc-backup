﻿Array.prototype.sortByProp = function (property, sortyBy) {

    this.sort(function (item1, item2) {

        var value1 = item1[property];
        var value2 = item2[property];


        var tempProp1, tempProp2;

        if (property.indexOf(".") != -1) {
            var props = property.split(".");

            for (var i = 0; i < props.length; i++) {
                var prop = props[i];

                if (tempProp1 != undefined)
                    tempProp1 = tempProp1[prop];
                else
                    tempProp1 = item1[prop];

                if (tempProp2 != undefined)
                    tempProp2 = tempProp2[prop];

                else
                    tempProp2 = item2[prop];

                if (typeof tempProp1 === 'object' ) {
                    value1 = tempProp1[prop];
                } else {
                    value1 = tempProp1;
                }

                if (typeof tempProp2 === 'object') {
                    value2 = tempProp2[prop];
                } else {
                    value2 = tempProp2;
                }

            }
        }

        if (value1 == null)
            value1 = "z";

        if (value2 == null)
            value2 = "z";

        if (typeof value1 === 'string')
            value1 = value1.trim().toLowerCase();

        if (typeof value2 === 'string')
            value2 = value2.trim().toLowerCase();

        if (sortyBy == "asc") {
            if (value1 < value2) return -1;
            if (value1 > value2) return 1;
        } else {
            if (value2 < value1) return -1;
            if (value2 > value1) return 1;
        }

        return 0;

    });
};



Array.prototype.groupBy = function (key) {
    var newArr = [],
                   types = {},
                   newItem, i, j, cur;
    for (i = 0, j = this.length; i < j; i++) {
        cur = this[i];
        if (!(cur[key] in types)) {
            types[cur[key]] = { key: cur[key], values: [] };
            newArr.push(types[cur[key]]);
        }
        types[cur[key]].values.push(cur);
    }

    return newArr;
}


function strip(html) {
    var tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
}

function cutString(text) {

    if (text == "Explorer")
        return text;


    if (text.length > 4) {
        return text.substring(0,4) + "...";
    } else {
        return text;
    }
}

String.prototype.cutText = function cutText(count) {

    if (this.length > count) {
        return (this.substring(0, count) + "...").toString();
    } else {
        return this.toString();
    }
};

String.format = function () {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }
    return s;
}

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

var hideDetailBoxRoutes = ["#/-", "#/reports", "#/help", "#/settings", "#/notifications", "#/monitoring", "#/newrule", "#/editrule", "#/newjob", "#/editjob", "#/newconnection", "#/editconnection", "#/newuser", "#/edituser", "#/newrole", "#/editrole", "#/newlocalparameter", "#/editlocalparameter", "#/newglobalparameter", "#/editglobalparameter", "#/newfolder", "#/editfolder"];

window.onhashchange = function () {

    var hash = location.hash;
    if (hash == "#/")
        hash = "#/-";

    $("#menuBox a[href]").parent().removeClass("active");
    $("#menuBox a[href='" + hash + "']").parent().addClass("active");

    $("#menuBox a[data-href]").parent().removeClass("active");
    $("#menuBox a[data-href='" + decodeURI( hash) + "']").parent().addClass("active");

    var existsToHide = hideDetailBoxRoutes.filter(function (h) { return hash.indexOf(h) != -1; }).length > 0;
    if (existsToHide) {
        hideDetailsBox();
    } else {
        showHideDetailsBox();
    }

    checkLoginStatus();
    disableDecadesClickForDatePickers();
    $("div.modal-backdrop,div.k-animation-container").remove();
}

window.onload = function () {

    var hash = location.hash;
   
    if (hash == "#/")
        hash = "#/-";

    setTimeout(function () {
        $("#menuBox a[href]").parent().removeClass("active");
        $("#menuBox a[href='" + hash + "']").parent().addClass("active");
        $("#menuBox a[data-href]").parent().removeClass("active");
        $("#menuBox a[data-href='" + hash + "']").parent().addClass("active");
    }, 1000);


    $("#closeDetailBarButton").click(function () {
        sessionStorage.setItem("detailsBoxIsShown", false);
        $("#mainBox").removeClass("col-lg-8").addClass("col-lg-10");
        $("#mainBox").removeClass("col-md-8").addClass("col-md-10");
        $("#detailBox").removeClass("col-lg-2").hide();
    });

    $("#toggleDetailBoxButton").click(function () {
        var detailsBoxIsShown = sessionStorage.getItem("detailsBoxIsShown");
        if (detailsBoxIsShown == "true" || detailsBoxIsShown == undefined) {
            hideDetailsBox();
            sessionStorage.setItem("detailsBoxIsShown", false);
        } else {
            showDetailsBox();
            sessionStorage.setItem("detailsBoxIsShown", true);
        }
    });


    var existsToHide = hideDetailBoxRoutes.filter(function (h) { return hash.indexOf(h) != -1; }).length > 0;
    if (existsToHide == true) {
        hideDetailsBox();
    } else {
        showHideDetailsBox();
    }


    checkLoginStatus();

    setTimeout(function () {
        checkLoginStatus();
    }, 1000);


   
    disableDecadesClickForDatePickers();

   

}

function disableDecadesClickForDatePickers() {
    setTimeout(function () {

        $("input.datepicker_textbox").focus(function () {
            setTimeout(function () {
                $(".datepicker-years th[data-action=pickerSwitch]").click(function () {
                    return false;
                });
            }, 500);
        });
    }, 1000);
}

function checkLoginStatus() {
    var isLoggedin = sessionStorage.getItem("isLoggedin");
    if (isLoggedin == "false" || isLoggedin == undefined) {
        $("#menuBox,#detailBox,#subnavbar,#mainNavBar").hide();
        $("#mainBox").addClass("col-lg-8").removeClass("col-lg-10");;
        $("#mainBox").addClass("col-md-8").removeClass("col-md-10");;
        $("body").css("background", "#f5f5f5");
    } else {
        $("#menuBox,#subnavbar,#mainNavBar").show();
        $("body").css("background", "white");
    }
}

function showHideDetailsBox() {
    var detailsBoxIsShown = sessionStorage.getItem("detailsBoxIsShown");

    if (detailsBoxIsShown == "true" || detailsBoxIsShown == undefined) {
        showDetailsBox();
    } else {
        hideDetailsBox();
    }
}

function showDetailsBox() {
    $("#mainBox").addClass("col-lg-8").removeClass("col-lg-10");
    $("#mainBox").addClass("col-md-8").removeClass("col-md-10");
    $("#detailBox").addClass("col-lg-2");
    $("#detailBox").show();
}

function hideDetailsBox() {
    $("#mainBox").removeClass("col-lg-8").addClass("col-lg-10");
    $("#mainBox").removeClass("col-md-8").addClass("col-md-10");
    $("#detailBox").removeClass("col-lg-2");
    $("#detailBox").hide();
}

function handleTreeViewScroll() {

    $("#scrollable_folders").removeClass("scrolly").removeClass("scrollx");

    if ($("#scrollable_folders").height() > 390) {
        $("#scrollable_folders").addClass("scrolly");
    } else {
        $("#scrollable_folders").removeClass("scrolly");
    }

    if ($("#scrollable_folders").width() > 600) {
        $("#scrollable_folders").addClass("scrollx");
    } else {
        $("#scrollable_folders").removeClass("scrollx");
    }

}


function extendTreeViewFolder(folderId,source) {
    var extendButton = $("a[data-extend=" + folderId + "]");

    if (extendButton.data("extended") == true && source == "file_click")
        return;

    extendButton.data("extended",true);

    extendButton.parents('li:first').find('ul:first').toggle();
    extendButton.parents('li:first').find('span').toggle();

    setTimeout(function () { handleTreeViewScroll() }, 200);
}


function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}

function getBrowser() {
    var nVer = navigator.appVersion;
    var nAgt = navigator.userAgent;
    var browserName = navigator.appName;
    var fullVersion = '' + parseFloat(navigator.appVersion);
    var majorVersion = parseInt(navigator.appVersion, 10);
    var nameOffset, verOffset, ix;

    // In Opera 15+, the true version is after "OPR/" 
    if ((verOffset = nAgt.indexOf("OPR/")) != -1) {
        browserName = "Opera";
        fullVersion = nAgt.substring(verOffset + 4);
    }
        // In older Opera, the true version is after "Opera" or after "Version"
    else if ((verOffset = nAgt.indexOf("Opera")) != -1) {
        browserName = "Opera";
        fullVersion = nAgt.substring(verOffset + 6);
        if ((verOffset = nAgt.indexOf("Version")) != -1)
            fullVersion = nAgt.substring(verOffset + 8);
    }
        // In MSIE, the true version is after "MSIE" in userAgent
    else if ((verOffset = nAgt.indexOf("MSIE")) != -1) {
        browserName = "Microsoft Internet Explorer";
        fullVersion = nAgt.substring(verOffset + 5);
    }
        // In Chrome, the true version is after "Chrome" 
    else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
        browserName = "Chrome";
        fullVersion = nAgt.substring(verOffset + 7);
    }
        // In Safari, the true version is after "Safari" or after "Version" 
    else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
        browserName = "Safari";
        fullVersion = nAgt.substring(verOffset + 7);
        if ((verOffset = nAgt.indexOf("Version")) != -1)
            fullVersion = nAgt.substring(verOffset + 8);
    }
        // In Firefox, the true version is after "Firefox" 
    else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
        browserName = "Firefox";
        fullVersion = nAgt.substring(verOffset + 8);
    }
        // In most other browsers, "name/version" is at the end of userAgent 
    else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) <
              (verOffset = nAgt.lastIndexOf('/'))) {
        browserName = nAgt.substring(nameOffset, verOffset);
        fullVersion = nAgt.substring(verOffset + 1);
        if (browserName.toLowerCase() == browserName.toUpperCase()) {
            browserName = navigator.appName;
        }
    }
    // trim the fullVersion string at semicolon/space if present
    if ((ix = fullVersion.indexOf(";")) != -1)
        fullVersion = fullVersion.substring(0, ix);
    if ((ix = fullVersion.indexOf(" ")) != -1)
        fullVersion = fullVersion.substring(0, ix);

    majorVersion = parseInt('' + fullVersion, 10);
    if (isNaN(majorVersion)) {
        fullVersion = '' + parseFloat(navigator.appVersion);
        majorVersion = parseInt(navigator.appVersion, 10);
    }

    return { name: browserName, version: fullVersion, majorVersion: majorVersion, userAgent: navigator.userAgent };
}

function getFormattedPageTitle(page) {
    return "ICC - " + page;
}

function getQueryString(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

window.setLogginedUser = function(user) {
    window.sessionStorage.setItem("user", JSON.stringify(user));
}

window.getLogginedUser = function() {
    return JSON.parse(window.sessionStorage.getItem("user"));
}

//PR['registerLangHandler'](
//    PR['createSimpleLexer'](
//        [
//         // Whitespace
//         [PR['PR_PLAIN'], /^[\t\n\r \xA0]+/, null, '\t\n\r \xA0'],
//         // A double or single quoted, possibly multi-line, string.
//         [PR['PR_STRING'], /^(?:"(?:[^\"\\]|\\.)*"|'(?:[^\'\\]|\\.)*')/, null,
//          '"\'']
//        ],
//        [
//         // A comment is either a line comment that starts with two dashes, or
//         // two dashes preceding a long bracketed block.
//         [PR['PR_COMMENT'], /^(?:--[^\r\n]*|\/\*[\s\S]*?(?:\*\/|$))/],
//         [PR['PR_KEYWORD'], /^(?:ADD|ALL|ALTER|AND|ANY|AS|ASC|AUTHORIZATION|BACKUP|BEGIN|BETWEEN|BREAK|BROWSE|BULK|BY|CASCADE|CASE|CHECK|CHECKPOINT|CLOSE|CLUSTERED|COALESCE|COLLATE|COLUMN|COMMIT|COMPUTE|CONSTRAINT|CONTAINS|CONTAINSTABLE|CONTINUE|CONVERT|CREATE|CROSS|CURRENT|CURRENT_DATE|CURRENT_TIME|CURRENT_TIMESTAMP|CURRENT_USER|CURSOR|DATABASE|DBCC|DEALLOCATE|DECLARE|DEFAULT|DELETE|DENY|DESC|DISK|DISTINCT|DISTRIBUTED|DOUBLE|DROP|DUMMY|DUMP|ELSE|END|ERRLVL|ESCAPE|EXCEPT|EXEC|EXECUTE|EXISTS|EXIT|FETCH|FILE|FILLFACTOR|FOR|FOREIGN|FREETEXT|FREETEXTTABLE|FROM|FULL|FUNCTION|GOTO|GRANT|GROUP|HAVING|HOLDLOCK|IDENTITY|IDENTITYCOL|IDENTITY_INSERT|IF|IN|INDEX|INNER|INSERT|INTERSECT|INTO|IS|JOIN|KEY|KILL|LEFT|LIKE|LINENO|LOAD|MATCH|MERGE|NATIONAL|NOCHECK|NONCLUSTERED|NOT|NULL|NULLIF|OF|OFF|OFFSETS|ON|OPEN|OPENDATASOURCE|OPENQUERY|OPENROWSET|OPENXML|OPTION|OR|ORDER|OUTER|OVER|PERCENT|PLAN|PRECISION|PRIMARY|PRINT|PROC|PROCEDURE|PUBLIC|RAISERROR|READ|READTEXT|RECONFIGURE|REFERENCES|REPLICATION|RESTORE|RESTRICT|RETURN|REVOKE|RIGHT|ROLLBACK|ROWCOUNT|ROWGUIDCOL|RULE|SAVE|SCHEMA|SELECT|SESSION_USER|SET|SETUSER|SHUTDOWN|SOME|STATISTICS|SYSTEM_USER|TABLE|TEXTSIZE|THEN|TO|TOP|TRAN|TRANSACTION|TRIGGER|TRUNCATE|TSEQUAL|UNION|UNIQUE|UPDATE|UPDATETEXT|USE|USER|USING|VALUES|VARYING|VIEW|WAITFOR|WHEN|WHERE|WHILE|WITH|WRITETEXT)(?=[^\w-]|$)/i, null],
//         // A number is a hex integer literal, a decimal real literal, or in
//         // scientific notation.
//         [PR['PR_LITERAL'],
//          /^[+-]?(?:0x[\da-f]+|(?:(?:\.\d+|\d+(?:\.\d*)?)(?:e[+\-]?\d+)?))/i],
//         // An identifier
//         [PR['PR_PLAIN'], /^[a-z_][\w-]*/i],
//         // A run of punctuation
//         [PR['PR_PUNCTUATION'], /^[^\w\t\n\r \xA0\"\'][^\w\t\n\r \xA0+\-\"\']*/]
//        ]),
//    ['sql']);



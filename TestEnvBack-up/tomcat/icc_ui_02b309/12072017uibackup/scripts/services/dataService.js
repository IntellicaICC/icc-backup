﻿
app.service('dataService', ['$http', 'bussinessService', 'Base64', function ($http, bussinessService, Base64) {

    var self    = this;
    this.config = null;

  
    this.returnToLogin = function (response) {
        if (response.data != null && response.data.errorCode == "ICC-001") {
            sessionStorage.removeItem("authKey");
            sessionStorage.removeItem("username");
            sessionStorage.removeItem("userId");
            sessionStorage.setItem("isLoggedin", false);
            sessionStorage.removeItem("users");
            sessionStorage.removeItem("user");
            window.location.hash = 'login';
            return;
        }
    }


    this.getConfig = function (callback) {
        if (self.config == null) {
            $http.get("/config.json?v=" + (new Date()).getUTCMilliseconds()).then(function (res) {
                self.config = res.data;
                if (callback)
                    callback();
            });
        }
    }

    //#region Explorer Page Endpoints
    var getEndpointUrl = function (name, callback) {
        if (self.config == null) {
            self.getConfig(function () {
                callback(self.config.urls.filter(function (u) {  return u.name == name; })[0]);
            });
        } else {
            callback(self.config.urls.filter(function (u) {  return u.name == name; })[0]);
        }
    }

    this.getVersion = function (successCallback, errorCallback) {

        getEndpointUrl("get_version", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;

            $http.defaults.headers.common['authorization'] = 'Basic ' + window.sessionStorage.getItem("authKey");
        
            $http({
                method: ep.method,
                url: uri,
                withCredentials : true,
            }).then(function (response) {
                var version = response.data.object;
                sessionStorage.setItem("version", version);
                successCallback(version);
            }, function (response) {

                self.returnToLogin(response);

                errorCallback(response);
            });

        });
    }

    this.getExplorerFolders = function (folderId,successCallback, errorCallback) {

        getEndpointUrl("get_explorer_folders", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;

            if (folderId != 0 && folderId != null && folderId != undefined)
                uri += "?id=" + folderId;


            $http.defaults.headers.common['authorization'] = 'Basic ' + sessionStorage.getItem("authKey");

            $http({
                method: ep.method,
                url: uri,
                withCredentials: true
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.getExplorerJobs = function (folderId, successCallback, errorCallback) {

        getEndpointUrl("get_explorer_jobs", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;

            if (folderId != 0 && folderId != null && folderId != undefined)
                uri += "?id=" + folderId;


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");

            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });
      
        });

    }

    this.getExplorerRules = function (folderId, successCallback, errorCallback) {
        
        getEndpointUrl("get_explorer_rules", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            if (folderId != 0 && folderId != null && folderId != undefined)
                uri += "?id=" + folderId;


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");

            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.getExplorerParameters = function (folderId,successCallback, errorCallback) {
        getEndpointUrl("get_explorer_parameters", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            if (folderId != 0 )
                uri += "?id=" + folderId;


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");

            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });

    }

    this.getExplorer = function (folderId,successCallback, errorCallback) {

        var explorerData = [];

        self.getExplorerFolders(folderId, function (folders) {
            
            if (folders != null && folders != undefined) {
             
                folders.forEach(function (f) {
                    f.Type = "folder";
                    explorerData.push(f);
                });
            }

            self.getExplorerJobs(folderId, function (jobs) {

                if (jobs != null && jobs != undefined) {
                    jobs.forEach(function (j) {
                        j.Type = "job";
                        explorerData.push(j);
                    });
                }

                self.getExplorerRules(folderId, function (rules) {
                    if (rules != null && rules != undefined) {
                        rules.forEach(function (r) {
                            r.Type = "rule";
                            explorerData.push(r);
                        });
                    }

                    self.getExplorerParameters(folderId, function (parameters) {

                        if (parameters != null && parameters != undefined) {
                            parameters.forEach(function (p) {
                                p.Type = "localparameter";
                                explorerData.push(p);
                            });

                            self.getUserList(function (users) {

                                explorerData = bussinessService.getExplorerDataWithUsers(explorerData, users);

                                successCallback(explorerData);

                            }, function () { successCallback(explorerData); });

                    
                        }
                        
                    }, function (error) { errorCallback(error, explorerData); });

                }, function (error) { errorCallback(error, explorerData); });
            }, function (error) { errorCallback(error, explorerData); })

        }, function (error) { errorCallback(error, explorerData); });
    }

    this.run = function (objectType,objectId, successCallback, errorCallback) {
        getEndpointUrl("run_" + objectType, function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);
           

            var uri = self.config.server + ep.url;
            uri = uri.replace("{id}", objectId);


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.schedule = function (objectTye, objectId, postData, successCallback, errorCallback) {
        getEndpointUrl("schedule_" + objectTye, function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            uri = uri.replace("{id}", objectId);


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri,
                data: postData
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.getFolder = function (folderId, successCallback, errorCallback) {
        getEndpointUrl("get_folder_details", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            uri = uri.replace("{id}", folderId);


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.saveFolder = function (folder, successCallback, errorCallback) {
        getEndpointUrl("save_folder", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;

            $http({
                method: ep.method,
                url: uri,
                data: folder
            }).then(function (response) {
                successCallback(response.data);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }
   
    

    //#endregion

    //#region Create a new job Page Endpoints

    this.getUserInfo = function (userId, successCallback, errorCallBack) {
        getEndpointUrl("get_user_info", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);


            var uri = self.config.server + ep.url;
            uri = uri.replace("{id}", userId);

            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.getTemplateList = function (successCallback, errorCallback) {
        getEndpointUrl("get_template_list", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.getTemplateVariables = function (templateId, successCallback, errorCallback) {
        getEndpointUrl("get_template_variables", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            uri = uri.replace("{id}", templateId);


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }



    this.saveJob = function (job, successCallback, errorCallback) {
        getEndpointUrl("save_job", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;

            $http({
                method: ep.method,
                url: uri,
                data: job
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.getJob = function (jobId, successCallback, errorCallback) {
        getEndpointUrl("get_job_details", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            uri = uri.replace("{id}", jobId);


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }


    this.getJobVariables = function (jobId, successCallback, errorCallback) {
        getEndpointUrl("get_job_variables", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            uri = uri.replace("{id}", jobId);


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }
    //#endregion

    //#region Sql query Result Endpoints
    this.getSqlResults = function (query, successCallback, errorCallback) {
        getEndpointUrl("get_sql_results", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri,
                data: query
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });

    }
    //#endregion

    //#region Users & Roles
    this.resetSettings = function (successCallback, errorCallback) {

        getEndpointUrl("reset_settings", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback();

            var uri = self.config.server + ep.url;
            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });

    }
    this.listSettings = function ( successCallback, errorCallback) {

        getEndpointUrl("list_settings", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback();

            var uri = self.config.server + ep.url;
            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });

    }

    this.saveSettings = function (settings, successCallback, errorCallback) {

        getEndpointUrl("save_settings", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback();

            var uri = self.config.server + ep.url;
            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri,
                data: settings
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });

    }

    this.updateProfile = function (profile, successCallback, errorCallback) {

        getEndpointUrl("save_profile", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback();

            var uri = self.config.server + ep.url;
            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri,
                data: profile
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });

    }

    this.addLicence = function (licenceKey,successCallback, errorCallback) {

        getEndpointUrl("license_add", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback();

            var uri = self.config.server + ep.url;
                    
            $http({
                method: ep.method,
                url: uri,
                data: { license: licenceKey }
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {

                self.returnToLogin(response);

                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");

            });

        });

    }

    this.getLicence = function (successCallback, errorCallback) {

        getEndpointUrl("license_get", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });

    }
  

    this.login = function (username,password,successCallback, errorCallback) {
        getEndpointUrl("login", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;

            $http.defaults.headers.common['Authorization'] = "";
            delete $http.defaults.headers.common['Authorization'];

            $http({
                timeout:10000,
                method: ep.method,
                url: uri,
                data: {username: username, password: password}
            }).then(function (response) {
                successCallback(response.data);
            }, function (response) {

                errorCallback(response.data);
            });

        });
    }

    this.getProfile = function (authKey, successCallback, errorCallback) {
        getEndpointUrl("get_profile", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;

            console.log(authKey);

            $http({
                method: ep.method,
                url: uri,
                headers: { Authorization: 'Basic ' + (authKey == null ? sessionStorage.getItem("authKey") : authKey) }
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.getUserList = function (successCallback, errorCallback) {
        getEndpointUrl("get_users", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.getRoleList = function (successCallback, errorCallback) {
        getEndpointUrl("get_roles", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");

            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.getApproverList = function (successCallback, errorCallback) {
        var userAndRoles = [];
        self.getUserList(function (users) {
            users.forEach(function (user) { user.Type = 'User'; userAndRoles.push(user); });
            self.getRoleList(function (roles) {
                roles.forEach(function (role) { role.Type = 'Role'; userAndRoles.push(role); });

                successCallback(userAndRoles);

            }, function (errorText) { self.returnToLogin(response); errorCallback(errorText, userAndRoles); });

        }, function () { self.returnToLogin(response); errorCallback(errorText, userAndRoles); });
    };


    this.getUser = function (userId, successCallback, errorCallback) {
        getEndpointUrl("get_user_details", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            uri = uri.replace("{id}", userId);


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.saveUser = function (user, successCallback, errorCallback) {
        getEndpointUrl("save_user", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;

            $http({
                method: ep.method,
                url: uri,
                data: user
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.getRoleUsers = function (roleId, successCallback, errorCallback) {
        getEndpointUrl("get_role_users", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            uri = uri.replace("{id}", roleId);


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }


    this.getRole = function (roleId, successCallback, errorCallback) {
        getEndpointUrl("get_role_details", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            uri = uri.replace("{id}", roleId);


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    

    this.saveRole = function (role, successCallback, errorCallback) {
        getEndpointUrl("save_role", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;

            $http({
                method: ep.method,
                url: uri,
                data: role
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    //#endregion


    //#region Global Parameter Endpoints

    this.getGlobalParameters = function (successCallback, errorCallback) {
        getEndpointUrl("get_global_parameter_list", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.getGlobalParameter = function (globalParameterId, successCallback, errorCallback) {
        getEndpointUrl("get_global_parameter_details", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            uri = uri.replace("{id}", globalParameterId);


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.saveGlobalParameter = function (globalParameter, successCallback, errorCallback) {
        getEndpointUrl("save_global_parameter", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;

            $http({
                method: ep.method,
                url: uri,
                data: globalParameter
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    //#endregion

    
    //#region Connections Endpoints

    this.getTemplateVariableConnection = function (successCallback, errorCallback) {
        getEndpointUrl("get_template_variable_connection", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;

            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.getConnection = function (connectionId, successCallback, errorCallback) {
        getEndpointUrl("get_connection_details", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            uri = uri.replace("{id}", connectionId);


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.saveConnection = function (connection, successCallback, errorCallback) {
        getEndpointUrl("save_connection", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;

            $http({
                method: ep.method,
                url: uri,
                data: connection
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    //#endregion

    //#region Local Parameter Endpoints


    this.getLocalParameter = function (localParamterId, successCallback, errorCallback) {
        getEndpointUrl("get_local_parameter_details", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            uri = uri.replace("{id}", localParamterId);


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.saveLocalParameter = function (localparameter, successCallback, errorCallback) {
        getEndpointUrl("save_local_parameter", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;

            $http({
                method: ep.method,
                url: uri,
                data: localparameter
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }


    //#endregion


    //#region Rule Endpoints


    this.getRule = function (ruleId, successCallback, errorCallback) {
        getEndpointUrl("get_rule_details", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            uri = uri.replace("{id}", ruleId);


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.saveRule = function (rule, successCallback, errorCallback) {
        getEndpointUrl("save_rule", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;

            $http({
                method: ep.method,
                url: uri,
                data: rule
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }


    //#endregion

    //#region Notifications Endpoints

    
    this.getNotifications = function (page,size, successCallback, errorCallback) {
        getEndpointUrl("get_notifications", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            uri = uri.replace("{page}", page);
            uri = uri.replace("{size}", size);

            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.deleteNotifications = function ( successCallback, errorCallback) {
        getEndpointUrl("delete_notifications", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;

            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.peekNotifications = function (successCallback, errorCallback) {
        getEndpointUrl("peek_notifications", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;

            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }


    this.countCotifications = function (successCallback, errorCallback) {
        getEndpointUrl("count_notifications", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;

            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);

                if (errorCallback)
                    errorCallback("Internal Server Error");
            });

        });
    }

    //#endregion



    //#region Monitoring Endpoints

    this.getLiveMonitoringStates = function (id, successCallback, errorCallback) {
        
        getEndpointUrl("monitoring_live", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            uri = uri.replace("{id}", id);


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.getMonitoringToday = function ( successCallback, errorCallback) {
        getEndpointUrl("monitoring_today", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;

            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }
    this.getMonitoringDetail = function (id,successCallback, errorCallback) {
        getEndpointUrl("monitoring_details", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            uri = uri.replace("{id}", id);

            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.getMonitoringSchedule = function (from,to,page,size,successCallback, errorCallback) {
        getEndpointUrl("monitoring_schedule", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            uri    += ("?from=" + from);
            uri    += ("&to=" + to);
            uri    += ("&page=" + page);
            uri    += ("&size=" + size);

            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }
    this.getMonitoringHistory = function (from, to, page, size,successCallback, errorCallback) {
        getEndpointUrl("monitoring_history", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;

            uri += ("?from=" + from);
            uri += ("&to=" + to);
            uri += ("&page=" + page);
            uri += ("&size=" + size);

            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.takeMonitoringAction = function (id, action, successCallback, errorCallback) {
        var endointName = "monitoring_" + action;

        getEndpointUrl(endointName, function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            uri = uri.replace("{id}", id);


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }


    this.monitoringReschedule = function (id, data, successCallback, errorCallback) {


        getEndpointUrl("monitoring_reschedule", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            uri = uri.replace("{id}", id);


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri,
                data: data
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    //#endregion

    //#region Reports Endpoints

    this.getReport = function (reportEndpoint, data, successCallback, errorCallback) {


        getEndpointUrl(reportEndpoint, function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri,
                data: data
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

   

    //#endregion


    this.delete = function (id, objectType, successCallback, errorCallback) {
        getEndpointUrl("delete_" + objectType, function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            uri = uri.replace("{id}", id);


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

    this.getFolderParents = function (id, successCallback, errorCallback) {
        getEndpointUrl("get_folder_parents", function (ep) {

            if (ep == null || ep == undefined)
                return successCallback([]);

            var uri = self.config.server + ep.url;
            uri = uri.replace("{id}", id);


            $http.defaults.headers.common['Authorization'] = 'Basic ' + sessionStorage.getItem("authKey");
            $http({
                method: ep.method,
                url: uri
            }).then(function (response) {
                successCallback(response.data.object);
            }, function (response) {
                self.returnToLogin(response);
                errorCallback(response.data != null && response.data.errorMessage != undefined ? response.data.errorMessage : "Internal Server Error");
            });

        });
    }

}]);
﻿
app.service('bussinessService', ['$filter', function ($filter) {

    var self = this;

    this.getExplorerDataWithUsers = function (explorerData, users) {
        explorerData.forEach(function (e) {
           
            var user = users.filter(function (u) { return u.id == e.rights.owner; })[0];
            e.rights.user = user;
        });

        return explorerData;
    }


    this.groupFolders = function (folders) {
       
        //Data Structure
        //$scope.folders = [{
        //    props: { name: "Burhan" },
        //    folders: [{ props: { name: "Burhan1" }, folders: [{ props: { name: "Burhan1-1" }, folders: [], id: "asd" }, { props: { name: "Burhan1-2" }, folders: [], id: "asd" }, { props: { name: "Burhan1-4" }, folders: [], id: "asd" }], id: "asd" }, { props: { name: "Burhan2" }, folders: [], id: "asd" }],
        //    id: "asd"
        //}];

        var grouppedFolders = [];

        folders.forEach(function (folder) {

            if (folder.parentFolder != null) {
                var parentFolder = folders.filter(function (f) { return f.id == folder.parentFolder; })[0];
                if (parentFolder != null) {
                    if (parentFolder.folders == undefined) {
                        parentFolder.folders = [];
                    }

                    if (parentFolder.folders.filter(function (f) { return f.id == folder.id; }).length == 0) {
                        parentFolder.folders.push(folder);
                    }
                }
            } else {
                if (grouppedFolders.filter(function (f) { return f.id == folder.id; }).length == 0) {
                    grouppedFolders.push(folder);
                }
            }

        });
        return grouppedFolders;
    }

    this.generateBreadcrumb = function (navigatedFolderId, folders) {
        var breadcrumb = [{ id: 0, text: "Explorer", shortText: "Explorer", link: "/#explorer" }];
        if (navigatedFolderId) {
            for (var i = 0; i < folders.length; i++) {
                var folder = folders[i];

                if (folder.id == navigatedFolderId) {
                    breadcrumb.push({ id: folder.id, text: folder.props.name, shortText: cutString(folder.props.name), link: "/#explorer/" + folder.id + "-" + folder.props.name });
                    break;
                }

                if (folder.folders == undefined)
                    continue;


                var subFolder = generateSubFolders(navigatedFolderId, folder.folders);

                if (subFolder != null) {
                    breadcrumb.push({ id: subFolder.id, text: subFolder.props.name, shortText: cutString(subFolder.props.name), link: "/#explorer/" + subFolder.id + "-" + subFolder.props.name });
                    break;
                }
                  
            }
        }

        return breadcrumb;
    }

    var folders = [];
    this.contactSubFolders = function (folderId, source_folders, addFolders) {
        if (source_folders) {
            for (var i = 0; i < source_folders.length; i++) {
                if (source_folders[i].id == folderId) {
                    source_folders[i].folders = addFolders;
                    folders = source_folders;
                    return folders;
                }

                var found = self.contactSubFolders(folderId, source_folders[i].folders, addFolders);
                if (found) {
                    found.folders = addFolders;
                    folders = source_folders;
                    return folders;
                }

                return folders;
            }
        }
        return folders;
    };

}]);

app.service('detailsBarShareData', ['$filter', function ($filter) {
    var props = null;

    return {
        get: function () {
            return props;
        },
        set: function (_props) {
            props = _props;
        }
    };
}]);

app.service('menuShareData', ['$filter', function ($filter) {
    var folders = [];

    return {
        get: function () {
            return folders;
        },
        set: function (_folders) {
            folders = _folders;
        }
    };
}]);

app.service('breadcrumbShareData', ['$filter', function ($filter) {
    var folders = [];

    return {
        get: function () {
            return folders;
        },
        set: function (_folders) {
            folders = _folders;
        }
    };
}]);

app.directive("contenteditable", function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, element, attrs, ngModel) {

            function read() {
                ngModel.$setViewValue(element.html());
            }

            ngModel.$render = function () {
                element.html(ngModel.$viewValue || "");
            };

            element.bind("blur keyup change", function () {
                scope.$apply(read);
            });
        }
    };
});


app.factory('Base64', function () {
    var keyStr = 'ABCDEFGHIJKLMNOP' +
            'QRSTUVWXYZabcdef' +
            'ghijklmnopqrstuv' +
            'wxyz0123456789+/' +
            '=';
    return {
        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output +
                        keyStr.charAt(enc1) +
                        keyStr.charAt(enc2) +
                        keyStr.charAt(enc3) +
                        keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);

            return output;
        },

        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                console.log("There were invalid base64 characters in the input text.\n" +
                        "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                        "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

            do {
                enc1 = keyStr.indexOf(input.charAt(i++));
                enc2 = keyStr.indexOf(input.charAt(i++));
                enc3 = keyStr.indexOf(input.charAt(i++));
                enc4 = keyStr.indexOf(input.charAt(i++));

                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;

                output = output + String.fromCharCode(chr1);

                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }

                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";

            } while (i < input.length);

            return output;
        }
    };
});


app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});
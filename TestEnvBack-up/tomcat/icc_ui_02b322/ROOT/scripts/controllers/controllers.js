﻿/// <reference path="../../index.html" />

app.controller('loginController', function ($rootScope, $scope, $routeParams, $route, $location, dataService, detailsBarShareData, Base64) {

    $rootScope.activePage = "login";
    document.title = getFormattedPageTitle("Login");

    if (sessionStorage.getItem("isLoggedin") == "true" || sessionStorage.getItem("isLoggedin") == true) {
        $location.path("/");
    }

    $scope.login = function () {
 
        if ($scope.username == undefined)
            $scope.username = "";

        if ($scope.password == undefined)
            $scope.password = "";

        dataService.login($scope.username, $scope.password, function (data) {
            if (data.errorMessage == null) {
                $scope.error = undefined;

                var authToken = Base64.encode($scope.username + ':' + $scope.password);

                dataService.getProfile(authToken, function (user) {

                    var userType = user.userType;
                    window.sessionStorage.setItem("userId", user.id);

                    storeLoginInfo();

                    sessionStorage.setItem("authKey", authToken);

                    window.setLogginedUser(user);

                    document.location.href = "/";
                }, function () {
                    $scope.error = "Server Internal Error";
                });
               
            }
        }, function (err) {

            sessionStorage.removeItem("authKey");
            sessionStorage.removeItem("username");
            sessionStorage.removeItem("user");
            sessionStorage.removeItem("userId");
            sessionStorage.setItem("isLoggedin", false);

            if (err == null) {
                $scope.error = "ICC is Offline";
                return;
            }
          
            if (err.errorMessage == undefined) {
                $scope.error = "Server Internal Error";
            } else {
                if (err.errorCode == "ICC-001") {
                    $("#noLicencePopup").modal("show");
                } else {
                    $scope.error = err.errorMessage;
                }
            }

        });
    }

    $scope.disableLoginForm = function () {
        $scope.disableToLogin = true;
    }

    $scope.showEnterLicencePopup = function () {
        $("#noLicencePopup").modal("hide");
        $("#enterLicenceKeyPopup").modal("show");
    }

    $scope.saveLicenceKey = function (licenceKey) {
        $scope.disableToLogin = false;

        licenceKey = licenceKey.replaceAll("\t", "").replaceAll("\r\n", "").replaceAll("\n","");
      
        dataService.addLicence(licenceKey, function (response) {

            $scope.licenceError = undefined;
            $scope.licenceSuccess = response;

            storeLoginInfo();

            var authToken = Base64.encode($scope.username + ':' + $scope.password);
            sessionStorage.setItem("authKey", authToken);

            setTimeout(function () { document.location.href = "/"; }, 3 * 1000);

        }, function (error) {
            $scope.licenceSuccess = undefined;
            $scope.licenceError = error;
        });

    }

    function storeLoginInfo(callback) {
        sessionStorage.setItem("isLoggedin", true);
        sessionStorage.setItem("username", $scope.username);

        if (callback)
            callback();
    }

});


app.controller('homeController', function ($rootScope, $scope, $routeParams, $route, $location, dataService, detailsBarShareData) {
    $rootScope.activePage = "home";
    document.title = getFormattedPageTitle("Home");

    $scope.showPage = false;
    $rootScope.showDetailButton = false;
    if (sessionStorage.getItem("isLoggedin") == "false" || sessionStorage.getItem("isLoggedin") == undefined) {
        $location.path("/login");
        return;
    }
    $scope.showPage = true;

    dataService.peekNotifications(function (notifications) {
        $scope.notifications = notifications;
    }, function (error) {
        $rootScope.showNotifyError(error);
    });

});


app.controller('menuController', function ($rootScope, $scope, $routeParams, $route, $location, dataService, menuShareData, bussinessService, breadcrumbShareData, $interval, $timeout) {

    $rootScope.newNotifications         = undefined;
    $rootScope.menuItems                = [];
    $rootScope.explorerFolders          = [];
    $rootScope.lastNewNotificationsCount = 0;

    var tempFolders                     = [];
    $rootScope.logginedUser             = getLogginedUser();

    if (sessionStorage.getItem("isLoggedin") == "true") {

     
        dataService.getLicence(function (licence) {
            $rootScope.logginedUser.licence = licence;
        }, function (error) {
            $rootScope.showNotifyError(error);
        });


        if ($rootScope.logginedUser.userType == "Admin" || $rootScope.logginedUser.userType == "Normal") {
            //Sync explorer view with menu view to show folder nodes under explorer
            $scope.$watch(function () { return menuShareData.get(); }, function (newValue, oldValue) {
                if (newValue !== oldValue) {
                    if ($rootScope.explorerFolders == undefined || document.location.hash == "#/explorer" || document.location.hash == "#/") {
                        $rootScope.explorerFolders = newValue;
                        tempFolders = newValue;
                    }
                    else {
                        newValue.forEach(function (f) {
                            $rootScope.explorerFolders.push(f);
                            tempFolders.push(f);
                        });
                    }

                    var folders = bussinessService.groupFolders($rootScope.explorerFolders);
                    $rootScope.folders = folders;
                }
            });

        }


        dataService.countCotifications(function (count) {

            if ($rootScope.logginedUser.userType == "Admin") {
                $scope.showExplorerMenuItem = true;
                //$scope.menuItems.push({ text: "Explorer", link: "#/explorer" });
                $rootScope.menuItems.push({ id: 1, text: "Global Parameters", link: "#/globalparameters" });
                $rootScope.menuItems.push({ id: 2, text: "Monitoring", link: "#/monitoring" });

                $rootScope.menuItems.push({ id: 3, text: "Notifications ", link: "#/notifications", count: count });
                $rootScope.menuItems.push({ id: 4, text: "Connections", link: "#/connections" });
                $rootScope.menuItems.push({ id: 5, text: "Users", link: "#/users" });
                $rootScope.menuItems.push({ id: 6, text: "Roles ", link: "#/roles" });
                $rootScope.menuItems.push({ id: 7, text: "Reports ", link: "#/reports" });

            } else if ($rootScope.logginedUser.userType == "Normal") {
                $scope.showExplorerMenuItem = true;
                //$scope.menuItems.push({ text: "Explorer", link: "#/explorer" });
                $rootScope.menuItems.push({ id: 1, text: "Global Parameters", link: "#/globalparameters" });
                $rootScope.menuItems.push({ id: 2, text: "Monitoring", link: "#/monitoring" });
                $rootScope.menuItems.push({ id: 3, text: "Notifications ", link: "#/notifications", count: count });
                $rootScope.menuItems.push({ id: 7, text: "Reports ", link: "#/reports" });

            } else { //Operator
                $rootScope.showExplorerMenuItem = false;
                $rootScope.menuItems.push({ id: 2, text: "Monitoring", link: "#/monitoring" });
                $rootScope.menuItems.push({ id: 3, text: "Notifications ", link: "#/notifications", count: count });
            }

        }, function () { });


        dataService.getVersion(function (version) {
            $scope.version = version;
        }, function (error) {
            $rootScope.showNotifyError(error);
        });


        //Get notification count every 10 seconds for all pages
        var notificationCountInterval = $interval(function () {


            if (sessionStorage.getItem("isLoggedin") == "false" || sessionStorage.getItem("isLoggedin") == undefined) {
                $interval.cancel(notificationCountInterval);
                return;
            }

            //Dont get counts on notifications page
            if ($rootScope.activePage != "notifications" && $rootScope.activePage != "monitoring_live") {

                dataService.countCotifications(function (count) {

                    var notificationMenu = $rootScope.menuItems.filter(function (m) { return m.id == 3 })[0];
                    if (notificationMenu)
                        notificationMenu.count = count;


                    //Write logic here 
                    $rootScope.newNotificationCount = count;
                    var showOnly1Notification = count - $rootScope.lastNewNotificationsCount == 1;

                    if (count == 0) {
                        $rootScope.newNotifications = undefined;
                        $rootScope.lastNewNotificationsCount = 0;
                        return;
                    }

                    //If new 1 new notification has been added to count, show it like 1 new notification
                    if (showOnly1Notification == true) {
                        $rootScope.newNotificationCount = 1;
                    }

                    if (count > 0 && count > $rootScope.lastNewNotificationsCount) {
                        dataService.peekNotifications(function (newNotifications) {

                            if (newNotifications.length > 0) {
                                $rootScope.newNotifications = newNotifications;
                                $timeout(function () { $rootScope.newNotifications = undefined; }, 3000);
                                $rootScope.lastNewNotificationsCount = count;
                            } else {
                                $rootScope.newNotifications = undefined;
                                $rootScope.lastNewNotificationsCount = 0;
                            }

                        }, function () { });
                    }

                });
            }


        }, 1000 * 10);

        $scope.closeNewNotifications = function () {
            $rootScope.newNotifications = undefined;
        }

    }

    $scope.viewFolder = function (folder) {
        $location.path("/explorer/" + folder.id);
    }

    $scope.viewSubFolders = function (folder) {
        var folderId = folder.id;

        if (folder.folders == undefined) {

            dataService.getExplorerFolders(folderId, function (_folders) {
                _folders.forEach(function (f) {
                    tempFolders.push(f);
                });

                $rootScope.folders = bussinessService.groupFolders(tempFolders);

                extendTreeViewFolder(folderId, "tree_click");

            }, function (error) { $rootScope.showNotifyError( error); });

        } else {
            extendTreeViewFolder(folderId, "tree_click");
        }


    }

});


app.controller('subHeaderController', function ($rootScope, $scope, $routeParams, $route, $location, dataService, $timeout) {

    $rootScope.notifyErrorMessage = undefined;

    $rootScope.showNotifyError = function (errorMessage) {
        $rootScope.notifyErrorMessage = errorMessage;

        $timeout(function () {
            $scope.closeNotifyError();
        }, 8 * 1000);
    }

    $scope.show = false;
    $scope.folderId;
    $scope.folderName = "";
    $scope.pageName = "Explorer";
    $rootScope.showDetailButton = false;
    $rootScope.breadcrumb = [];

    if (sessionStorage.getItem("isLoggedin") == "true") {
        if (sessionStorage.getItem("users") == undefined || sessionStorage.getItem("users") == null) {
            dataService.getUserList(function (data) {
                sessionStorage.setItem("users", JSON.stringify(data));
            }, function () { });
        }
    }


    //1890px - 
    $rootScope.$on('$routeChangeSuccess', function (e, current, pre) {

        if (current.$$route == undefined)
            return;

        $("#contextMenu").remove();

        $scope.controllerName = current.$$route.controller;

        if (current.$$route.controller == "explorerController") {

            $rootScope.showDetailButton = true;

            var folderId = current.params.folderId;

            if (folderId)
                $scope.folderId = folderId;
            else
                $scope.folderId = "";

            $rootScope.breadcrumb = [{ id: 0, text: "Explorer", shortText: "Explorer", link: "/#explorer", showText: "Explorer" }];

            if (folderId) {
                dataService.getFolderParents(folderId, function (folders) {

                    if (folders == null || folders == undefined)
                        return;

                    folders.reverse().forEach(function (f) {
                        var breadcrumb = { id: f.id, text: f.props.name, shortText: cutString(f.props.name), link: "/#explorer/" + f.id};

                        if ($rootScope.breadcrumb.indexOf(breadcrumb) == -1)
                            $rootScope.breadcrumb.push(breadcrumb);

                    });


                }, function (error) {
                    $rootScope.showNotifyError( error);
                });
            }


        } else if (current.$$route.controller == "usersController") {
            $rootScope.breadcrumb = [];
            $rootScope.breadcrumb.push({ id: 0, text: "Users", link: "/#users", showText: "Users" });
            $rootScope.showDetailButton = true;
        }
        else if (current.$$route.controller == "rolesController") {
            $rootScope.breadcrumb = [];
            $rootScope.breadcrumb.push({ id: 0, text: "Roles", link: "/#roles", showText: "Roles" });
            $rootScope.showDetailButton = true;
        }
        else if (current.$$route.controller == "connectionsController") {
            $rootScope.breadcrumb = [];
            $rootScope.breadcrumb.push({ id: 0, text: "Connections", link: "/#connections", showText: "Connections" });
            $rootScope.showDetailButton = true;
        }
        else if (current.$$route.controller == "globalParametersController") {
            $rootScope.breadcrumb = [];
            $rootScope.breadcrumb.push({ id: 0, text: "Global Parameters", link: "/#globalparameters", showText: "Global Parameters" });
            $rootScope.showDetailButton = true;
        }
        else if (current.$$route.controller == "homeController") {
            $rootScope.breadcrumb = [];
            $rootScope.breadcrumb.push({ id: 0, text: "Home", link: "/#/", showText: "Home" });
            $rootScope.showDetailButton = false;
        }
        else if (current.$$route.controller == "notificationsController") {
            $rootScope.breadcrumb = [];
            $rootScope.breadcrumb.push({ id: 0, text: "Notifications", link: "/#notifications", showText: "Notifications" });
            $rootScope.showDetailButton = false;
        }
        else if (current.$$route.controller == "monitoringController") {
            $rootScope.breadcrumb = [];
            $rootScope.breadcrumb.push({ id: 0, text: "Monitoring", link: "/#monitoring", showText: "Monitoring" });
            $rootScope.showDetailButton = false;
        }
        else if (current.$$route.controller == "settingsController") {
            $rootScope.breadcrumb = [];
            $rootScope.breadcrumb.push({ id: 0, text: "Settings", link: "/#settings", showText: "Settings" });
            $rootScope.showDetailButton = false;
        }
        else if (current.$$route.controller == "helpController") {
            $rootScope.breadcrumb = [];
            $rootScope.breadcrumb.push({ id: 0, text: "Help", link: "/#help", showText: "Help" });
            $rootScope.showDetailButton = false;
        }
        else {
            $scope.folderId = "";
            $rootScope.showDetailButton = false;
        }

    });

    $scope.showLicence = function () {
        $("#licencePopup").modal("show");
    }

 
    $scope.logout = function () {
        sessionStorage.removeItem("authKey");
        sessionStorage.removeItem("username");
        sessionStorage.setItem("isLoggedin", false);
        sessionStorage.removeItem("users");
        sessionStorage.removeItem("user");
        sessionStorage.removeItem("userId");
        $location.path("/login");
    }

    $scope.closeNotifyError = function () {
        $rootScope.notifyErrorMessage = undefined;
    }

});

app.controller('detailBarController', function ($scope, $rootScope, $routeParams, detailsBarShareData, dataService) {


    $scope.$watch(function () { return detailsBarShareData.get(); }, function (newValue, oldValue) {

        if (newValue !== oldValue) {
            $scope.props = newValue;

            if ($scope.props != null) {
                var users = JSON.parse(sessionStorage.getItem("users"));

                var modifiedUserId = $scope.props.modifiedBy;
                var createdByUserId = $scope.props.createdBy;

                var modifiedUser = users.filter(function (u) { return u.id == modifiedUserId })[0];
                var createdUser = users.filter(function (u) { return u.id == createdByUserId })[0];

                $scope.props.createdByUser = createdUser;
                $scope.props.modifiedByUser = modifiedUser;
            }

        }
    });

    $rootScope.$on('$routeChangeSuccess', function (e, current, pre) { detailsBarShareData.set(null); });
});



app.controller('explorerController', function ($scope, $rootScope, $routeParams, $location, dataService, detailsBarShareData, menuShareData, $timeout) {
    $rootScope.activePage = "explorer";
    $scope.currentOrderProp = "props.name";
    $scope.currentOrderBy = "asc";
    $scope.loading = true;
    document.title = getFormattedPageTitle("Explorer");
  
    if (sessionStorage.getItem("isLoggedin") == "false" || sessionStorage.getItem("isLoggedin") == undefined) {
        $location.path('/login');
        return;
    }

    dataService.getVersion(function () {

        if ($rootScope.logginedUser.userType == "Operator") {
            $location.path("/monitoring");
            return;
        }

        sessionStorage.setItem("isLoggedin", true);
        $scope.explorer = [];

        var folderId = 0;
        if ($routeParams.folderId)
            folderId = $routeParams.folderId;

        if (isNaN(folderId) == true)
            folderId = 0;


        //Get folder detail for the last breadcrum because getParentFolders endpoint does not return the results that includes this folder.
        if (folderId != 0) {
            dataService.getFolder(folderId, function (folder) {
                $rootScope.breadcrumb.push({ id: folderId, text: folder.props.name, shortText: cutString(folder.props.name), link: "/#explorer/" + folder.id});
            }, function (error) { $rootScope.showNotifyError( error); });
        }


        dataService.getExplorer(folderId, function (data) {
            $scope.loading = false;

            var explorerData = data.filter(function (d) { return d.Type == "folder" });
            explorerData.sortByProp($scope.currentOrderProp, "asc");

            var otherObjects = data.filter(function (d) { return d.Type != "folder" });
            otherObjects.sortByProp($scope.currentOrderProp, "asc");
            for (var i = 0; i < otherObjects.length; i++) {
                var obj = otherObjects[i];
                explorerData.push(obj);
            }

            //Set modifiedUser Details
            var users = JSON.parse(sessionStorage.getItem("users"));

            explorerData.forEach(function (explorerDataItem) {
                var modifiedUserId = explorerDataItem.props.modifiedBy;

                var modifiedUser = users.filter(function (u) { return u.id == modifiedUserId })[0];
                explorerDataItem.props.modifiedByUser = modifiedUser;
            });


            $scope.explorer = explorerData;


            var selectedItemBeforePage = $rootScope.selectedItem;
            if (selectedItemBeforePage && selectedItemBeforePage.page == "explorer") {
                $scope.selectItem(selectedItemBeforePage.item,undefined, true);
            }

            $rootScope.selectedItem = undefined;

            data.forEach(function (r) { r.allowToSetRights = $rootScope.logginedUser.userType == "Admin"; });

            //Sync explorer view with menu view to show folder nodes under explorer
            var folders = data.filter(function (ed) { return ed.Type == "folder"; });

            folders.sortByProp("props.name", "asc");
          

            menuShareData.set(folders);
            extendTreeViewFolder(folderId, "file_click");

        }, function (error) {
            $scope.loading = false;
            $rootScope.showNotifyError( error);
        });

        $scope.viewItemDetail = function (itemId, itemName, itemType) {
            if (itemType == "folder") {
                $location.path('/explorer/' + itemId);
            } else {
                if (itemType == "job") {
                    $scope.jobDetail(itemId);
                }
                else if (itemType == "localparameter") {
                    $scope.parameterDetail(itemId);
                }
                else if (itemType == "rule") {
                    $scope.ruleDetail(itemId);
                }
            }
        }

        $scope.runJob = function (jobId) {
            dataService.run("job", jobId, function (data) { }, function (error) { $rootScope.showNotifyError( error); });
        }

        $scope.runRule = function (ruleId) {
            dataService.run("rule", ruleId, function (data) { }, function (error) { $rootScope.showNotifyError( error); });
        }

        $scope.jobDetail = function (jobId) {
            $location.path('/editjob/' + jobId );
        }

        $scope.ruleDetail = function (ruleId) {
            $location.path('/editrule/' + ruleId);
        }

        $scope.folderDetail = function (folderId) {
            $location.path('/editfolder/' + folderId);
        }

        $scope.parameterDetail = function (parameterId) {
            $location.path('/editlocalparameter/' + parameterId );
        }

        $scope.selectItem = function (item, $index, onPageLoad) {
            var props = item.props;
            var rights = item.rights;
            props.type = "explorer";
            props.rights = rights;
            detailsBarShareData.set(props);

            $scope.explorer.forEach(function (x) { x.selected = false; });


            if (item.Type != "folder") {
                $rootScope.selectedItem = { page: "explorer", item: item };
            }

            if (onPageLoad && onPageLoad == true) {
                $scope.explorer.forEach(function (x) {
                    if (x.id == item.id)
                    { x.selected = true; } 
                });
            } else {
                item.selected = true;
                if ($index) {
                    $scope.explorer[$index].selected = true;
                }
            }

        }

        //Get active roles for the rigths popup
        if ($rootScope.roles == undefined || $rootScope.roles.length == 0)
            dataService.getRoleList(function (roles) { $rootScope.roles = roles.filter(function (r) { return r.props.active == true; }); $scope.roles = $rootScope.roles; }, function () { });
        else
            $scope.roles = $rootScope.roles;

        //Get active users for the rigths popup
        if ($rootScope.users == undefined || $rootScope.users.length == 0) {
            dataService.getUserList(function (users) {
                $rootScope.users = users.filter(function (u) { return u.props.active == true; });
                $scope.users = $rootScope.users.filter(function (u) { return u.userType == "Normal"; });//Set normal users for rights popup
                $scope.ownerUsers = $rootScope.users.filter(function (u) { return u.userType != "Operator"; });  //Set users for owner popup
            }, function () { });
        }
        else {
            $scope.users = $rootScope.users.filter(function (u) { return u.userType == "Normal"; });
            $scope.ownerUsers = $rootScope.users.filter(function (u) { return u.userType != "Operator"; });
        }

        //Yeni role veya user eklenince rootscope daki cachei temizle

    }, function () {
        sessionStorage.setItem("isLoggedin", false);
        document.location.href = "/#/login";
        return;
    });


    handleTreeViewScroll();

    $scope.orderBy = function (property) {

        var orderBy = $scope.currentOrderBy == "asc" ? "desc" : "asc";

        var explorerData = $scope.explorer.filter(function (d) { return d.Type == "folder" });
        explorerData.sortByProp(property, orderBy);

        var otherObjects = $scope.explorer.filter(function (d) { return d.Type != "folder" });
        otherObjects.sortByProp(property, orderBy);
        for (var i = 0; i < otherObjects.length; i++) {
            var obj = otherObjects[i];
            explorerData.push(obj);
        }

        $scope.explorer = explorerData;
        $scope.currentOrderProp = property;
        $scope.currentOrderBy = orderBy;

    }


    $scope.deleteObject = {};
    $scope.showDeletePopup = function (object) {
        $scope.deleteObject = object;
        $scope.notifyError = undefined;
        $("#contextMenu").remove();
        $("#deletePopup").modal("show");
    }

    $scope.delete = function () {

        $scope.notifyError = undefined;
        dataService.delete($scope.deleteObject.id, $scope.deleteObject.Type, function () {
            $scope.explorer = $scope.explorer.filter(function (u) { return u.id != $scope.deleteObject.id });
            deleteFolder($scope.deleteObject.id, $scope.deleteObject.parentFolder, $rootScope.folders);
            $rootScope.explorerFolders = $rootScope.explorerFolders.filter(function (f) { return f.id != $scope.deleteObject.id; });
            $("#deletePopup").modal("hide");
        }, function (error) { $scope.notifyError = error; $timeout(function () { $scope.notifyError = undefined; }, 8 * 1000); });
    }

    function deleteFolder(folderId, parentFolderId, folders) {

        if (folders == undefined)
            return;

        for (var i = 0; i < folders.length; i++) {
            var f = folders[i];
            if (f.id == parentFolderId && f.folders != undefined) {
                f.folders = f.folders.filter(function (fx) { return fx.id != folderId });
                return;
            } else if (f.id == folderId) {
                $rootScope.folders = folders.filter(function (f) { return f.id != folderId; });
             
                return;
            } else {
                deleteFolder(folderId, parentFolderId, f.folders);
            }
        }
    }


    $scope.ownerObject = null;
    var tempOwnerObject = null;
    var isOwnerSaved = false;
    var ownerIndex = null;
    $scope.showOwnerPopup = function (object, index) {
        $scope.ownerObject = object;
        ownerIndex = index;
        tempOwnerObject = $.extend(true, {}, object);

        $scope.notifyError = undefined;

        $("#contextMenu").remove();
        $("#ownerPopup").modal("show");
    }

    $scope.changeOwner = function () {

        $scope.notifyError = undefined;
        var postData = $scope.ownerObject;

        delete postData.$$hashKey;

        if ($scope.ownerObject.Type == "folder") {
            dataService.saveFolder(postData, function () { }, function (error) { $scope.notifyError = error; $timeout(function () { $scope.notifyError = undefined; }, 8 * 1000); });
        } else if ($scope.ownerObject.Type == "job") {
            dataService.saveJob(postData, function () { }, function (error) { $scope.notifyError = error; $timeout(function () { $scope.notifyError = undefined; }, 8 * 1000); });
        }
        else if ($scope.ownerObject.Type == "localparameter") {
            dataService.saveLocalParameter(postData, function () { }, function (error) { $scope.notifyError = error; $timeout(function () { $scope.notifyError = undefined; }, 8 * 1000); });
        }
        else if ($scope.ownerObject.Type == "rule") {
            dataService.saveRule(postData, function () { }, function (error) { $scope.notifyError = error; $timeout(function () { $scope.notifyError = undefined; }, 8 * 1000); });
        }


        var users = JSON.parse(sessionStorage.getItem("users"));
        $scope.ownerObject.rights.user = users != null ? users.filter(function (u) { return u.id == $scope.ownerObject.rights.owner })[0] : {};
        $scope.ownerObject.props.modifiedByUser = $rootScope.logginedUser;


        isOwnerSaved = true;

        $("#ownerPopup").modal("hide");
    }

    $scope.closeOwnerPopup = function () {
        if (isOwnerSaved == false) {
            $scope.ownerObject = tempOwnerObject;
            $scope.explorer[ownerIndex] = $scope.ownerObject;
        }

        isOwnerSaved = false;
    }

    $("#ownerPopup").on('hidden.bs.modal', function () {
        $scope.closeOwnerPopup();
    })

    $scope.rightsObject = null;
    var tempRightsObject = null;
    var rightsIndex = null;
    var isRightSaved = false;

    $scope.showRightsPopup = function (object, index) {
        rightsIndex = index;
        $scope.rightsObject = object;

        tempRightsObject = $.extend(true, {}, object);

        $scope.roles.forEach(function (r) {
            var roleRightValue = tempRightsObject.rights.roleRights[r.id];

            if (roleRightValue == 1 || roleRightValue == 3 || roleRightValue == 5 || roleRightValue == 7) {
                r.roleReadRight = 1;
            }
            else {
                r.roleReadRight = -1;
            }

            if (roleRightValue == 2 || roleRightValue == 3 || roleRightValue == 6 || roleRightValue == 7) {
                r.roleWriteRight = 2;
            }
            else {
                r.roleWriteRight = -2;
            }

            if (roleRightValue == 5 || roleRightValue == 4 || roleRightValue == 6 || roleRightValue == 7) {
                r.roleExecuteRight = 4;
            }
            else {
                r.roleExecuteRight = -4;
            }
        });

        $scope.users.forEach(function (u) {
            var userRightValue = $scope.rightsObject.rights.userRights[u.id];
            if (userRightValue == 1 || userRightValue == 3 || userRightValue == 5 || userRightValue == 7) {
                u.userReadRight = 1;
            }
            else {
                u.userReadRight = -1;
            }

            if (userRightValue == 2 || userRightValue == 3 || userRightValue == 6 || userRightValue == 7) {
                u.userWriteRight = 2;
            }
            else {
                u.userWriteRight = -2;
            }

            if (userRightValue == 5 || userRightValue == 4 || userRightValue == 6 || userRightValue == 7) {
                u.userExecuteRight = 4;
            }
            else {
                u.userExecuteRight = -4;
            }
        });

        $("#contextMenu").remove();
        $scope.notifyError = undefined;
        $("#rightsPopup").modal("show");
    }


    function toggleObjectRight(id, rwxValue, objectProperty) {
        var rightValue = $scope.rightsObject.rights[objectProperty][id];
        var alreadyHasThisRight = rightValue != undefined;

        if (alreadyHasThisRight == true) {
            var allRightsDeletedForThis = false;

            rightValue += rwxValue;

            if (rightValue <= 0)
                rightValue = 0;

            if (rightValue > 7)
                rightValue = 7;

            allRightsDeletedForThis = rightValue == 0;

            $scope.rightsObject.rights[objectProperty][id] = rightValue;

            if (allRightsDeletedForThis) {
                delete $scope.rightsObject.rights[objectProperty][id];
            }

        } else {
            var allRights = $scope.rightsObject.rights[objectProperty];
            allRights[id] = rwxValue;
            $scope.rightsObject.rights[objectProperty] = allRights;
        }
    }

    $scope.toggleRoleRight = function (roleId, rwxValue) {
        toggleObjectRight(roleId, rwxValue, "roleRights");
    }

    $scope.toggleUserRight = function (userId, rwxValue) {
        toggleObjectRight(userId, rwxValue, "userRights");
    }

    $scope.saveObjectRights = function () {

        $scope.notifyError = undefined;

        var postData = $scope.rightsObject;

        delete postData.$$hashKey;


        if ($scope.rightsObject.Type == "folder") {
            dataService.saveFolder(postData, rightsSavedSuccessfully
             , function (error) { $scope.notifyError = error; $timeout(function () { $scope.notifyError = undefined; }, 8 * 1000); });
        } else if ($scope.rightsObject.Type == "job") {

            dataService.saveJob(postData, rightsSavedSuccessfully, function (error) { $scope.notifyError = error; $timeout(function () { $scope.notifyError = undefined; }, 8 * 1000); });
        }
        else if ($scope.rightsObject.Type == "localparameter") {
            dataService.saveLocalParameter(postData, rightsSavedSuccessfully, function (error) { $scope.notifyError = error; $timeout(function () { $scope.notifyError = undefined; }, 8 * 1000); });
        }
        else if ($scope.rightsObject.Type == "rule") {
            dataService.saveRule(postData, rightsSavedSuccessfully, function (error) { $scope.notifyError = error; $timeout(function () { $scope.notifyError = undefined; }, 8 * 1000); });
        }

    }

    function rightsSavedSuccessfully() {
        isRightSaved = true;
        $scope.rightsObject.props.modifiedByUser = $rootScope.logginedUser;
        $("#rightsPopup").modal("hide");
    }


    $("#rightsPopup").on('hidden.bs.modal', function () {
        $scope.closeRightsPopup();
    })

    $scope.closeRightsPopup = function () {
        if (isRightSaved == false) {
            $scope.rightsObject = tempRightsObject;
            $scope.explorer[rightsIndex] = tempRightsObject;
        }

        isRightSaved = false;
    }


    $scope.scheduleObject = null;
    $scope.schedulePostData = {  };

    $scope.schedule = function (_object) {
        $scope.scheduleObject = _object;

        $scope.schedulePostData.period = undefined;
        $scope.schedulePostData.interval = "NONE";
        $scope.schedulePostData.firstRunDate = undefined;
        $scope.repeatEveryData = false;
        $scope.scheduledError = undefined;
        $scope.notifyError = undefined;
        $("#contextMenu").remove();
        $("#schedulePopup").modal("show");
    }

    $scope.scheduleProcessing = false;
    $scope.scheduled = false;
    $scope.saveSchedule = function () {

        $scope.scheduleProcessing = true;
        $scope.scheduledError = undefined;
        $scope.notifyError = undefined;
        dataService.schedule($scope.scheduleObject.Type, $scope.scheduleObject.id, $scope.schedulePostData, function () {
            $scope.scheduled = true;
            $scope.scheduleProcessing = false;
            $timeout(function () {
                $("#schedulePopup").modal("hide");
                $timeout(function () { $scope.scheduled = false; }, 200);
            }, 2000);

        }, function (error) {

            $scope.scheduleProcessing = false;
            $scope.scheduled = false;

            $scope.notifyError = error;
            $timeout(function () { $scope.notifyError = undefined; }, 8 * 1000);
        });

    }


    $("#schedulePopup").on('hidden.bs.modal', function () {
        $("#scheduleDatePicker").data("DateTimePicker").clear();
        $scope.schedulePostData.firstRunDate = undefined;
    })

    $("#scheduleDatePicker").datetimepicker({
        inline: true,
        sideBySide: true,
        minDate : new Date()
    });

    $("#scheduleDatePicker").on("dp.change", function (date) {
        $scope.$apply(function () {
            $scope.schedulePostData.firstRunDate = new Date(date.date);
        });
    });

    $("div.modal").on('hidden.bs.modal', function () {
        $scope.notifyError = undefined;
    })

});



app.controller('newJobController', function ($rootScope, $scope, $route, $routeParams, $location, dataService, $timeout) {

    $rootScope.activePage = "job";
    
   
    dataService.getVersion(function () { sessionStorage.setItem("isLoggedin", true); }, function () { sessionStorage.setItem("isLoggedin", false); $location.path('/login'); return; });

    var jobSql = "";

    $scope.currentStep = 1;
    $scope.editmode = false;

    $scope.loading = true;
    var folder = "";
    var folderName = "";

    dataService.getUserList(function (data) {
        $scope.users = data;
    }, function () { });

    dataService.getRoleList(function (data) {
        $scope.roles = data;
    }, function () { });


    initJobFinderPopup();

    //Edit job
    if ($routeParams.jobId != undefined) {
        dataService.getJob($routeParams.jobId, function (job) {

            document.title = getFormattedPageTitle("Edit Job");

            job.props.active = job.props.active.toString();
            $scope.job       = job;
            $scope.editmode  = true;

            $scope.job.variables = [];
            $scope.variableDefinations = [];


            dataService.getTemplateVariables($scope.job.template, function (variableDefinations) {
                $scope.variableDefinations = variableDefinations;


                //Set job variables based on its definations
                var sortedVariableDefinations = variableDefinations;
                sortedVariableDefinations.sortByProp("position", "asc");
                sortedVariableDefinations.forEach(function (vdef, index) { $scope.job.variables.push({ definition: vdef.id, value: "" }); });


                var anyConnectionTypeExists = variableDefinations.filter(function (v) { return v.type == "Connection" })[0] != null;
                if(anyConnectionTypeExists){
                    dataService.getTemplateVariableConnection(function (connections) { $scope.connections = connections; }, function (error) { $rootScope.showNotifyError( error); });
                }


                //Get job variables
                dataService.getJobVariables($routeParams.jobId, function (variables) {

                    //for each job variable, load and select the job variable values
                    for (var i = 0; i < variables.length; i++) {
                        var jobVariable = variables[i];

                       var jV =  $scope.job.variables.filter(function (v) { return v.definition == jobVariable.definition })[0];

                        jV.value = jobVariable.value;
                        jV.id    = jobVariable.id;

                     

                        var vDef = $scope.variableDefinations.filter(function (vd) { return vd.id == jobVariable.definition })[0];
                        if (vDef.type == "SQL") {
                            $scope.sql = jV.value;
                            $scope.striptedSql = strip(jV.value);
                            jobSql = jV.value;
                        } else if (vDef.type == "Connection") {
                            $scope.connectionid = jV.value;
                        } else if (vDef.type == "Dataset") {
                            if (jV.value != null) {
                                dataService.getJob(jV.value, function (jobDetail) {

                                    //Get job variable to set its jobname
                                    //jV = $scope.job.variables.filter(function (v) { return v.value == jobDetail.id })[0];
                                    //jV.jobName = jobDetail.props.name;
                                    //console.log(jobDetail);

                                    $scope.job.variables.filter(function (v) { return v.value == jobDetail.id }).forEach(function (x) {
                                        jV = x;
                                        jV.jobName = jobDetail.props.name;
                                    });

                                }, function (error) { $rootScope.showNotifyError(error); });
                            }
                        }

                    }

                    if ($scope.sql != "" && $scope.connectionid != "") {
                        $scope.getSqlVariableList(function () {

                            var listVariableDef = $scope.variableDefinations.filter(function (vd) { return vd.type == "List" })[0];
                            var listVariableValue = job.variables.filter(function (v) { return v.definition == listVariableDef.id })[0];

                            var values = listVariableValue.value.split(',');
                            values.forEach(function (val) {

                                if(val != "")
                                    $scope.selectListItem(listVariableDef.id, { data: val });

                            });
                        });
                    }

                }, function (error) { $rootScope.showNotifyError(error); });


            }, function (error) { $rootScope.showNotifyError( error); });



        }, function (error) { $rootScope.showNotifyError(error); });
    } else { // create new job

        document.title  = getFormattedPageTitle("New Job");

        folder = $routeParams.folderId == undefined ? "" : $routeParams.folderId;
        folderName = $routeParams.folderName == undefined ? "" : $routeParams.folderName;

        var logginedUserId = window.sessionStorage.getItem("userId");

        $scope.job = { template: '', variables: [], rights: { owner: logginedUserId }, priority: "Normal", props: { active: "true" } };


        if (folder != "") {
            $scope.job.folder = folder;
        }

        $scope.editmode = false;
    }


    $scope.variableDefinations = [];

    $scope.connections = [];
    $scope.list = [];
    $scope.sqlColumns = [];
    $scope.sqlRows = [];

    $scope.connectionid = "";
    $scope.sql = "";
    $scope.striptedSql = "";

    $scope.errorText = "";

    $scope.nextStep = function () {
        var cs = $scope.currentStep;
        cs++;
        if (cs > 3) {
            cs = 3;
        }

        $scope.currentStep = cs;
    }

    $scope.prevStep = function () {
        var cs = $scope.currentStep;
        cs--;
        if (cs < 1) {
            cs = 1;
        }

        if (cs == 3) {
            cs = 2;
        }

        $scope.currentStep = cs;
    }

    dataService.getTemplateList(function (templates) {
        $scope.loading = false;
        $scope.templates = templates;

    }, function (error) { $rootScope.showNotifyError(error); });

    var returnUrl = "/explorer/" + folder;
    $scope.saveJob = function () {

        $scope.jobEdited = false;
        if ($scope.job.props.active == "") {
            $scope.errorText = "Please select job status.";
            return;
        }

        dataService.saveJob($scope.job, function (data) {

            $scope.currentStep = 4;

            //Editing a job
            if ($scope.job.id != undefined) {
                $scope.jobEdited = true;

                if ($scope.job.folder == null) {
                    $timeout(function () { $location.path("/explorer"); }, 1000);
                } else {
                    $timeout(function () { $location.path("/explorer/" + $scope.job.folder); }, 1000);
                }
                return;
            } else {

                if (folder == "") {
                    $timeout(function () { $location.path("/explorer"); }, 1000);
                } else {
                    $location.path(returnUrl);
                }

            }

        }, function (error) {
            $rootScope.showNotifyError(error);
        });
    };

    $scope.storeConnectionId = function (connectionid) {
        $scope.connectionid = connectionid;
    };


    $scope.getVariables = function (templateId) {

        $scope.job.variables       = [];
        $scope.variableDefinations = [];

        if (templateId == "")
            return;

        dataService.getTemplateVariables(templateId, function (variables) {
            $scope.variableDefinations = variables;

            var anyConnectionTypeExists = variables.filter(function (v) { return v.type == "Connection" })[0] != null;
            if (anyConnectionTypeExists) {
                dataService.getTemplateVariableConnection(function (connections) { $scope.connections = connections; }, function (error) { $rootScope.showNotifyError( error); });
            }

        }, function (error) { $rootScope.showNotifyError( error); });
    }

    $scope.selectListItem = function (variableId, data) {

        var listItem = $scope.list.filter(function (l) { return l.data == data.data; })[0];

        if (listItem)
            listItem.selected = listItem.selected == undefined || listItem.selected == false ? true : false;


        var variable = $scope.job.variables.filter(function (v) { return v.definition == variableId })[0];
        variable.value = "";

        $scope.list.forEach(function (l) { if (l.selected == true) { variable.value += (l.data + ","); } });
        if (variable.value.endsWith(",")) {
            variable.value = variable.value.slice(0, variable.value.length - 1);
        }
    };

    $scope.getSqlVariableList = function (callback) {
        $scope.notifyError = undefined;
        $scope.sql = strip($scope.sql);
        $scope.striptedSql = strip($scope.sql);
        $scope.list = [];

        if (tempSqlColumns.length == 0) {
            dataService.getSqlResults({ connectionId: $scope.connectionid, sql: strip($scope.sql), folderId: $scope.job.folder == null ? "" : $scope.job.folder }, function (list) {
                list.columns.forEach(function (l) { $scope.list.push({ data: l, selected: false }); });

                tempSqlColumns = [];

                if (callback)
                    callback();

            }, function (error) { $rootScope.showNotifyError( error); });
        } else {
            $scope.list = tempSqlColumns;
        }


        $scope.sqlColumns = [];
        $scope.sqlRows = [];

        $("#sql_modal").modal("hide");

        var sqlvariableDefinition = $scope.variableDefinations.filter(function (v) { return v.type == "SQL"; })[0];
        if (sqlvariableDefinition) {
            var sqlvariable = $scope.job.variables.filter(function (v) { return v.definition == sqlvariableDefinition.id; })[0];
            if (sqlvariable)
                sqlvariable.value = $scope.striptedSql;
        }
    }

    var tempSqlColumns = [];
    $scope.getSqlResults = function () {
        $scope.notifyError = undefined;
        $scope.sql = strip($scope.sql);
        dataService.getSqlResults({ connectionId: $scope.connectionid, sql: strip($scope.sql), folderId: $scope.job.folder == null ? "" : $scope.job.folder }, function (list) {

            tempSqlColumns = [];
            list.columns.forEach(function (l) { tempSqlColumns.push({ data: l, selected: false }); });

            $scope.sqlColumns = list.columns;
            $scope.sqlRows    = list.rows;

            $("#sql_modal").scrollTop(0);
    
        }, function (error) { $scope.sqlColumns = []; $scope.sqlRows = []; $scope.notifyError = error; });
    }

    //Deprecated
    $scope.selectDatasetItem = function (variableId, data) {
        var sqlvariableDefinition = $scope.variableDefinations.filter(function (v) { return v.id == variableId; })[0];
        if (sqlvariableDefinition) {
            var listItem = sqlvariableDefinition.dataset.filter(function (l) { return l.data == data.data; })[0];
            listItem.selected = listItem.selected == undefined || listItem.selected == false ? true : false;

            var variable = $scope.job.variables.filter(function (v) { return v.definition == variableId })[0];
            variable.value = "";

            sqlvariableDefinition.dataset.forEach(function (l) { if (l.selected == true) { variable.value += (l.data + ","); } });
            if (variable.value.endsWith(",")) {
                variable.value = variable.value.slice(0, variable.value.length - 1);
            }
        }
    };

    $scope.closeSqlPopup = function () {

        if ($scope.striptedSql == '') {
            $scope.sql = '';
        } else {
            $scope.sql = $scope.striptedSql;
        }

        $scope.sqlColumns = [];
        $scope.sqlRows = [];
    }


    //#region * Job & Rule Finder START*//
    $scope.variableIndex = 0;
    $scope.findAJob = function (variableIndex) {
        $scope.variableIndex = variableIndex;
        $scope.activeFolderId = 0;
        bindJobsDataToPopup(0);
        setTimeout(function () { toogleMenuTreViewScroll(); }, 200);
    
        $("#selectJobPopup").modal("show");
    }

    $scope.jobs = [];
    $scope.menuFolders = [];
    $scope.selectedMenuFolderId = 0;

    $scope.selectAndSaveJob = function (item) {
        $scope.selectJob(item);
        $scope.saveJobSelection();
    }

    $scope.saveJobSelection = function () {
        var item = $scope.selectedJob;

       //Datasete ata
        $scope.job.variables[$scope.variableIndex].value = item.id;
        $scope.job.variables[$scope.variableIndex].jobName = item.props.name;
        $("#selectJobPopup").modal("hide");
    }

    $scope.clearSelectedJob = function (index) {
        $scope.job.variables[index].value = "";
        $scope.job.variables[index].jobName = undefined;
    }

    $scope.selectJob = function (item) {
        $scope.jobs.forEach(function (i) { i.selected = false; });
        item.selected = true;
        $scope.selectedJob = item;
    }

    $scope.viewSubFolders = function (folder) {
        if (folder.expended == true) {
            extendTreeViewFolderForPopup(folder.id, "tree_click");
            return;
        }

        dataService.getExplorerFolders(folder.id, function (folders) {
            folder.folders = folders;
            folder.expended = true;
            extendTreeViewFolderForPopup(folder.id, "tree_click");
        }, function (error) { $rootScope.showNotifyError( error); });
    }

    $scope.viewJobs= function (folderId) {
        $scope.activeFolderId = folderId;
        bindJobsDataToPopup(folderId);
    }

    function initJobFinderPopup(folderId) {
        dataService.getExplorerFolders(0, function (folders) {
            $scope.activeFolderId = folderId;
            $scope.menuFolders = folders;
            if (parseInt(folderId) > 0) {
                dataService.getFolderParents(folderId, function (_folders) {
                    _folders.push({ id: folderId });
                    _folders.reverse();
                    _folders.forEach(function (_folder, index) {
                        index++
                        setTimeout(function () {
                            $("a[data-expand-button='job'][data-expand='" + _folder.id + "']").click();
                        }, index * 1000);
                    });
                }, function () { });
            }
        }, function () { });
    }

    function bindJobsDataToPopup(folderId) {
        $scope.popupDataLoading = true;
        $scope.jobs = [];
        $scope.selectedJob = undefined;

        dataService.getExplorerJobs(folderId, function (jobs) {
            jobs = jobs.filter(function (r) { return r.props.active == true; });

            var filteredJobs = [];
            jobs.forEach(function (job) {
                var hasDataSet = $scope.templates.filter(function (template) { return template.hasDataset == true && template.id == job.template; }).length > 0;
                if (hasDataSet)
                    filteredJobs.push(job);
            });

            $scope.jobs = filteredJobs;
            $scope.popupDataLoading = false;
        }, function () { });
    }

    function extendTreeViewFolderForPopup(folderId, source) {
        var extendButton = $("a[data-expand-button='job'][data-expand=" + folderId + "]:last");

        extendButton.parents('li:first').find('ul:first').toggle();
        extendButton.parents('li:first').find('span').toggle();
        setTimeout(function () { toogleMenuTreViewScroll() }, 200);
    }

    function toogleMenuTreViewScroll() {
        $("#scrollable_treeview").removeClass("scrolly").removeClass("scrollx");

        if ($("#scrollable_treeview").height() > 340) {
            $("#scrollable_treeview").addClass("scrolly");
        } else {
            $("#scrollable_treeview").removeClass("scrolly");
        }

        if ($("#scrollable_treeview").width() > 194) {
            $("#scrollable_treeview").addClass("scrollx");
        } else {
            $("#scrollable_treeview").removeClass("scrollx");
        }
    }

    //#endregion *Job & Rule Finder END*//


    $("div.modal").on('hidden.bs.modal', function () {
        $scope.notifyError = undefined;
    })

});


app.controller('usersController', function ($rootScope, $scope, $routeParams, $route, $location, dataService, detailsBarShareData, $timeout) {

    $rootScope.activePage = "users";
    document.title = getFormattedPageTitle("Users");

    $scope.currentOrderProp = "props.name";
    $scope.currentOrderBy = "asc";

    dataService.getVersion(function () { sessionStorage.setItem("isLoggedin", true); }, function () { sessionStorage.setItem("isLoggedin", false); $location.path('/login'); return; });

    dataService.getUserList(function (data) {

        data.sortByProp($scope.currentOrderProp, $scope.currentOrderBy);
        $scope.users = data;

        var selectedItemBeforePage = $rootScope.selectedItem;
        if (selectedItemBeforePage && selectedItemBeforePage.page == "users") {
            $scope.selectItem(selectedItemBeforePage.item, true);
        }
        $rootScope.selectedItem = undefined;

    }, function (error) { $rootScope.showNotifyError( error); });

    $scope.selectItem = function (item, onPageLoad) {
      
        detailsBarShareData.set(item.props);

        $scope.users.forEach(function (x) { x.selected = false; });
        $rootScope.selectedItem = { page: "users", item: item };

        if (onPageLoad && onPageLoad == true) {
            $scope.users.forEach(function (x) {
                if (x.id == item.id) { x.selected = true; }
            });

        } else {
            item.selected = true;
        }
    }

    $scope.userDetail = function (user) {
        $location.path('/edituser/' + user.id);
    }


    $scope.orderBy = function (property) {
        var orderBy = $scope.currentOrderBy == "asc" ? "desc" : "asc";

        $scope.users.sortByProp(property, orderBy);

        $scope.currentOrderProp = property;
        $scope.currentOrderBy = orderBy;
    }

    $scope.deleteObject = {};
    $scope.showDeletePopup = function (object) {
        $scope.notifyError = undefined;
        $scope.deleteObject = object;
        $("#deletePopup").modal("show");
    }

    $scope.delete = function () {
        $scope.notifyError = undefined;
        dataService.delete($scope.deleteObject.id, "user", function () {
            $scope.users = $scope.users.filter(function (u) { return u.id != $scope.deleteObject.id });
            $("#deletePopup").modal("hide");

            //Clear the cache for the authorization popup
            $rootScope.users = undefined;

        }, function (error) { $scope.notifyError = error; $timeout(function () { $scope.notifyError = undefined; }, 8 * 1000); });
    }


    $("div.modal").on('hidden.bs.modal', function () {
        $scope.notifyError = undefined;
    })

});

app.controller('newUserController', function ($rootScope, $scope, $routeParams, $route, $location, dataService, $timeout) {

    $rootScope.activePage = "user";

    dataService.getVersion(function () { sessionStorage.setItem("isLoggedin", true); }, function () { sessionStorage.setItem("isLoggedin", false); $location.path('/login'); return; });

    $scope.user = { props: { active: "true" }, loginType: "Default", userType: "Normal" };

    $scope.editmode = false;
    $scope.loading = false;
    var editUserId = $routeParams.userId;
    $scope.updated = false;

    //Edit user
    if (editUserId != undefined) {
        document.title = getFormattedPageTitle("Edit User");
        $scope.editmode = true;
        dataService.getUser(editUserId, function (user) {
            user.props.active = user.props.active.toString();
            $scope.user = user;
        }, function (error) { $rootScope.showNotifyError( error); });

    } else { // create new user
        document.title = getFormattedPageTitle("New User");
        $scope.editmode = false;
    }

    $scope.saveUser = function () {
        $scope.buttonClicked = true;

        //if ($scope.user.props.name == undefined || $scope.user.userType == "") {
        //    return;
        //}

        $scope.loading = true;
        dataService.saveUser($scope.user, function (user) {

            $scope.buttonClicked = false;
            $scope.loading = false;

            //Clear the cache for the authorization popup
            $rootScope.users = undefined;

            if ($scope.editmode == true) {
                if ($scope.user.id == $rootScope.logginedUser.id) {
                    $rootScope.logginedUser = $scope.user;
                    window.setLogginedUser($scope.user);
                }


                dataService.getUserList(function (data) {
                    sessionStorage.setItem("users", JSON.stringify(data));
                }, function () { });


                $scope.updated = true;
                $timeout(function () { $location.path('/users'); }, 1000);
            } else {
                $location.path('/users');
            }



        }, function (error) { $scope.buttonClicked = false; $scope.loading = false; $rootScope.showNotifyError( error); });


    }

});

app.controller('rolesController', function ($rootScope, $scope, $routeParams, $route, $location, dataService, detailsBarShareData, $timeout) {

    $rootScope.activePage = "roles";
    document.title = getFormattedPageTitle("Roles");

    dataService.getVersion(function () { sessionStorage.setItem("isLoggedin", true); }, function () { sessionStorage.setItem("isLoggedin", false); $location.path('/login'); return; });

    $scope.currentOrderProp = "props.name";
    $scope.currentOrderBy = "asc";

    dataService.getRoleList(function (data) {

        data.sortByProp($scope.currentOrderProp, $scope.currentOrderBy);
        $scope.roles = data;

        var selectedItemBeforePage = $rootScope.selectedItem;
        if (selectedItemBeforePage && selectedItemBeforePage.page == "roles") {
            $scope.selectItem(selectedItemBeforePage.item, true);
        }

        $rootScope.selectedItem = undefined;

    }, function (error) { $rootScope.showNotifyError( error); });

    $scope.selectItem = function (item, onPageLoad) {

        detailsBarShareData.set(item.props);

        $rootScope.selectedItem = { page: "roles", item: item };
        $scope.roles.forEach(function (x) { x.selected = false; });
        if (onPageLoad && onPageLoad == true) {
            $scope.roles.forEach(function (x) {
                if (x.id == item.id) { x.selected = true; }
            });
        } else {
            item.selected = true;
        }
    }


    $scope.roleDetail = function (role) {
        $location.path('/editrole/' + role.id);
    }

    $scope.orderBy = function (property) {
        var orderBy = $scope.currentOrderBy == "asc" ? "desc" : "asc";
        $scope.roles.sortByProp(property, orderBy);
        $scope.currentOrderProp = property;
        $scope.currentOrderBy = orderBy;
    }

    $scope.deleteObject = {};
    $scope.showDeletePopup = function (object) {
        $scope.deleteObject = object;
        $scope.notifyError = undefined;
        $("#deletePopup").modal("show");
    }

    $scope.delete = function () {
        $scope.notifyError = undefined;
        dataService.delete($scope.deleteObject.id, "role", function () {
            $scope.roles = $scope.roles.filter(function (u) { return u.id != $scope.deleteObject.id });
            $("#deletePopup").modal("hide");

            //Clear the cache for the authorization pextendTreeViewFolderopup
            $rootScope.roles = undefined;

        }, function (error) { $scope.notifyError = error; $timeout(function () { $scope.notifyError = undefined; }, 8 * 1000); });
    }


    $("div.modal").on('hidden.bs.modal', function () {
        $scope.notifyError = undefined;
    })

});

app.controller('newRoleController', function ($rootScope, $scope, $routeParams, $route, $location, dataService, $timeout) {

    $rootScope.activePage = "role";

    dataService.getVersion(function () { sessionStorage.setItem("isLoggedin", true); }, function () { sessionStorage.setItem("isLoggedin", false); $location.path('/login'); return; });

    $scope.role = { props: { active: "true" }, users: [] };

    $scope.selectedAddUsers = [];
    $scope.selectedRemoveUsers = [];


    $scope.editmode = false;
    $scope.loading = false;
    $scope.updated = false;

    var editRoleId = $routeParams.roleId;

    dataService.getUserList(function (users) { $scope.users = users; }, function (error) { $rootScope.showNotifyError(error); });


    //Edit role
    if (editRoleId != undefined) {
        document.title = getFormattedPageTitle("Edit Role");
        $scope.editmode = true;
        dataService.getRole(editRoleId, function (role) {


            dataService.getRoleUsers(role.id, function (users) {

                $scope.role.users = users;
                var userIds = [];
                $scope.role.users.forEach(function (_user) { userIds.push(_user.id); });
                $scope.users = $scope.users.filter(function (_user) { return userIds.indexOf(_user.id) == -1; });



            }, function (error) { $rootScope.showNotifyError( error); });

            role.props.active = role.props.active.toString();

            $scope.role = role;
        }, function (error) { $rootScope.showNotifyError( error); });

    } else { // create new role
        document.title = getFormattedPageTitle("New Role");
        $scope.editmode = false;
    }

    $scope.saveRole = function () {
        $scope.buttonClicked = true;

        //if ($scope.role.props.name == undefined) {
        //    $scope.loading = false;
        //    return;
        //}

        $scope.loading = true;
        dataService.saveRole($scope.role, function (role) {
            $scope.buttonClicked = false;
            $scope.loading = false;

            //Clear the cache for the authorization popup
            $rootScope.roles = undefined;

            if ($scope.editmode == true) {
                $scope.updated = true;
                $timeout(function () { $location.path('/roles'); }, 1000);
            } else {
                $location.path('/roles');
            }


        }, function (error) { $rootScope.showNotifyError( error); $scope.buttonClicked = false; $scope.loading = false; });
    }

    $scope.addSelectedUser = function (user) {
        var userIds = [];
        $scope.selectedAddUsers.forEach(function (_user) {
            userIds.push(_user.id);
            $scope.role.users.push(_user);
        });

        $scope.users = $scope.users.filter(function (_user) { return userIds.indexOf(_user.id) == -1; });

        $scope.selectedAddUsers = [];
    }

    $scope.removeSelectedUser = function (user) {
        var userIds = []
        $scope.selectedRemoveUsers.forEach(function (_user, index) {
            userIds.push(_user.id);
        });


        $scope.role.users = $scope.role.users.filter(function (_user) {
            if (userIds.indexOf(_user.id) != -1) {
                $scope.users.push(_user);
                return false;
            }

            return true;
        });

        $scope.selectedRemoveUsers = [];
    }


    $scope.selectAddUser = function (user) {
        if ($scope.selectedAddUsers.indexOf(user) != -1) {
            $scope.selectedAddUsers = $scope.selectedAddUsers.filter(function (_user) { return _user.id != user.id; });
        } else {
            $scope.selectedAddUsers.push(user);
        }
    }

    $scope.selectRemoveUser = function (user) {
        if ($scope.selectedRemoveUsers.indexOf(user) != -1) {
            $scope.selectedRemoveUsers = $scope.selectedRemoveUsers.filter(function (_user) { return _user.id != user.id; });
        } else {
            $scope.selectedRemoveUsers.push(user);
        }
    }

});

app.controller('connectionsController', function ($rootScope, $scope, $routeParams, $route, $location, dataService, detailsBarShareData, $timeout) {

    $rootScope.activePage = "connections";
    document.title = getFormattedPageTitle("Connections");

    dataService.getVersion(function () { sessionStorage.setItem("isLoggedin", true); }, function () { sessionStorage.setItem("isLoggedin", false); $location.path('/login'); return; });

    $scope.currentOrderProp = "props.name";
    $scope.currentOrderBy = "asc";


    dataService.getTemplateVariableConnection(function (connections) {

        connections.sortByProp($scope.currentOrderProp, $scope.currentOrderBy);
        $scope.connections = connections;

        var selectedItemBeforePage = $rootScope.selectedItem;
        if (selectedItemBeforePage && selectedItemBeforePage.page == "connections") {
            $scope.selectItem(selectedItemBeforePage.item, true);
        } 

        $rootScope.selectedItem = undefined;

    }, function (error) { $rootScope.showNotifyError( error); });


    $scope.selectItem = function (item,onPageLoad) {
        $scope.connections.forEach(function (x) { x.selected = false;});
 
        detailsBarShareData.set(item.props);
        $rootScope.selectedItem = { page: "connections", item: item };

        if (onPageLoad && onPageLoad == true) {

            $scope.connections.forEach(function (x) {
                if (x.id == item.id) { x.selected = true; }
            });

        } else {
            item.selected = true;
        }

    }

    $scope.connectionDetail = function (connection) {
        $location.path('/editconnection/' + connection.id);
    }


    $scope.orderBy = function (property) {
        var orderBy = $scope.currentOrderBy == "asc" ? "desc" : "asc";
        $scope.connections.sortByProp(property, orderBy);
        $scope.currentOrderProp = property;
        $scope.currentOrderBy = orderBy;
    }


    $scope.deleteObject = {};
    $scope.showDeletePopup = function (object) {
        $scope.deleteObject = object;
        $scope.notifyError = undefined;
        $("#deletePopup").modal("show");
    }

    $scope.delete = function () {
        $scope.notifyError = undefined;
        dataService.delete($scope.deleteObject.id, "connection", function () {
            $scope.connections = $scope.connections.filter(function (u) { return u.id != $scope.deleteObject.id });
            $("#deletePopup").modal("hide");

        }, function (error) { $scope.notifyError = error; $timeout(function () { $scope.notifyError = undefined; }, 8 * 1000); });
    }

    $("div.modal").on('hidden.bs.modal', function () {
        $scope.notifyError = undefined;
    })

});

app.controller('newConnectionController', function ($rootScope, $scope, $routeParams, $route, $location, dataService, $timeout) {

    $rootScope.activePage = "connection";

    dataService.getVersion(function () { sessionStorage.setItem("isLoggedin", true); }, function () { sessionStorage.setItem("isLoggedin", false); $location.path('/login'); return; });

    $scope.connection = { props: { active: "true" } };

    $scope.editmode = false;
    $scope.loading = false;
    var editConnectionId = $routeParams.connectionId;
    $scope.updated = false;

    //Edit connection
    if (editConnectionId != undefined) {
        document.title = getFormattedPageTitle("Edit Connection");
        $scope.editmode = true;
        dataService.getConnection(editConnectionId, function (connection) {
            connection.props.active = connection.props.active.toString();
            $scope.connection = connection;
        }, function (error) {
            $rootScope.showNotifyError( error);
        });
    } else { // create new connection
        $scope.editmode = false;
        document.title = getFormattedPageTitle("New Connection");
    }


    $scope.saveConnection = function () {
        $scope.buttonClicked = true;

        //if ($scope.connection.props.name == undefined || $scope.connection.databaseUrl == undefined || $scope.connection.databaseUser == undefined || $scope.connection.databasePassword == undefined) {
        //    return;
        //}

        $scope.loading = true;
        dataService.saveConnection($scope.connection, function (connection) {

            $scope.buttonClicked = false;
            $scope.loading = false;

            if ($scope.editmode == true) {
                $scope.updated = true;
                $timeout(function () { $location.path('/connections'); }, 1000);
            } else {
                $location.path('/connections');
            }

        }, function (error) { $rootScope.showNotifyError( error); $scope.buttonClicked = false; $scope.loading = false; });

    }

});



app.controller('globalParametersController', function ($rootScope, $scope, $routeParams, $route, $location, dataService, detailsBarShareData, $timeout) {

    $rootScope.activePage = "globalparameters";
    document.title = getFormattedPageTitle("Global Parameters");

    dataService.getVersion(function () { sessionStorage.setItem("isLoggedin", true); }, function () { sessionStorage.setItem("isLoggedin", false); $location.path('/login'); return; });

    $scope.currentOrderProp = "props.name";
    $scope.currentOrderBy = "asc";

    dataService.getGlobalParameters(function (parameters) {

        parameters.sortByProp($scope.currentOrderProp, $scope.currentOrderBy);
        $scope.parameters = parameters;

        var selectedItemBeforePage = $rootScope.selectedItem;
        if (selectedItemBeforePage && selectedItemBeforePage.page == "globalparameters") {
            $scope.selectItem(selectedItemBeforePage.item, true);
        }
        $rootScope.selectedItem = undefined;

    }, function (error) { $rootScope.showNotifyError(error); });

    $scope.selectItem = function (item, onPageLoad) {
        detailsBarShareData.set(item.props);

        $rootScope.selectedItem = { page: "globalparameters", item: item };
        $scope.parameters.forEach(function (x) { x.selected = false; });
        if (onPageLoad && onPageLoad == true) {
            $scope.parameters.forEach(function (x) {
                if (x.id == item.id) { x.selected = true; }
            });
        } else {
            item.selected = true;
        }
    }

    $scope.globalParameterDetail = function (parameter) {
        $location.path('/editglobalparameter/' + parameter.id);
    }

    $scope.orderBy = function (property) {
        var orderBy = $scope.currentOrderBy == "asc" ? "desc" : "asc";
        $scope.parameters.sortByProp(property, orderBy);
        $scope.currentOrderProp = property;
        $scope.currentOrderBy = orderBy;
    }

    $scope.deleteObject = {};
    $scope.showDeletePopup = function (object) {
        $scope.notifyError = undefined;
        $scope.deleteObject = object;
        $("#deletePopup").modal("show");
    }

    $scope.delete = function () {
        $scope.notifyError = undefined;
        dataService.delete($scope.deleteObject.id, "globalparameter", function () {
            $scope.parameters = $scope.parameters.filter(function (u) { return u.id != $scope.deleteObject.id });
            $("#deletePopup").modal("hide");

        }, function (error) { $scope.notifyError = error; $timeout(function () { $scope.notifyError = undefined; }, 8 * 1000); });
    }

    $("div.modal").on('hidden.bs.modal', function () {
        $scope.notifyError = undefined;
    })

});

app.controller('newGlobalParameterController', function ($rootScope, $scope, $routeParams, $route, $location, dataService, $timeout) {

    $rootScope.activePage = "globalparameter";

    dataService.getVersion(function () { sessionStorage.setItem("isLoggedin", true); }, function () { sessionStorage.setItem("isLoggedin", false); $location.path('/login'); return; });

    $scope.globalparameter = { props: { active: "true" } };

    $scope.editmode = false;
    $scope.loading = false;
    var globalParameterId = $routeParams.globalParameterId;

    //Edit connection
    if (globalParameterId != undefined) {
        document.title = getFormattedPageTitle("Edit Parameter");
        $scope.editmode = true;
        dataService.getGlobalParameter(globalParameterId, function (globalParameter) {
            globalParameter.props.active = globalParameter.props.active.toString();
            $scope.globalparameter = globalParameter;
        }, function (error) { $rootScope.showNotifyError(error); });
    } else { // create new connection
        document.title = getFormattedPageTitle("New Parameter");
        $scope.editmode = false;
    }

    $scope.saveGlobalParameter = function () {
        $scope.buttonClicked = true;
        //if ($scope.globalparameter.props.name == undefined) {
        //    return;
        //}

        $scope.loading = true;
        dataService.saveGlobalParameter($scope.globalparameter, function (globalparameter) {
            $scope.buttonClicked = false;
            $scope.loading = false;

            if ($scope.editmode == true) {
                $scope.updated = true;
                $timeout(function () { $location.path('/globalparameters'); }, 1000);
            } else {
                $location.path('/globalparameters');
            }

        }, function (error) { $rootScope.showNotifyError( error); $scope.buttonClicked = false; $scope.loading = false; });

    }

});


app.controller('newLocalParameterController', function ($rootScope, $scope, $routeParams, $route, $location, dataService, $timeout) {

    $rootScope.activePage = "localparameter";


    dataService.getVersion(function () { sessionStorage.setItem("isLoggedin", true); }, function () { sessionStorage.setItem("isLoggedin", false); $location.path('/login'); return; });

    $scope.localparameter = { props: { active: "true" } };

    $scope.editmode = false;
    $scope.loading = false;
    $scope.updated = false;

    var folderId = $routeParams.folderId == undefined ? "" : $routeParams.folderId;
    var folderName = $routeParams.folderName == undefined ? "" : $routeParams.folderName;

    var returnUrl = folderId != "" ? ("/explorer/" + folderId) : "/explorer";

    var localParameterId = $routeParams.localParameterId;

    //Edit connection
    if (localParameterId != undefined) {
        document.title = getFormattedPageTitle("Edit Local Parameter");
        $scope.editmode = true;
        dataService.getLocalParameter(localParameterId, function (localparameter) {
            localparameter.props.active = localparameter.props.active.toString();
            $scope.localparameter = localparameter;
        }, function (error) { $rootScope.showNotifyError( error); });
    } else { // create new connection
        document.title = getFormattedPageTitle("New Local Parameter");
        $scope.editmode = false;
        if (folderId != "") {
            $scope.localparameter.folder = folderId;
        }
    }

    $scope.saveLocalParameter = function () {
        $scope.buttonClicked = true;

        //if ($scope.localparameter.props.name == undefined) {
        //    return;
        //}

        $scope.loading = true;

        dataService.saveLocalParameter($scope.localparameter, function (localparameter) {

            $scope.buttonClicked = false;
            $scope.loading = false;

            if ($scope.editmode == true) {
                $scope.updated = true;

                if ($scope.localparameter.folder == null) {
                    $timeout(function () { $location.path("/explorer"); }, 1000);
                } else {
                    $timeout(function () { $location.path("/explorer/" + $scope.localparameter.folder); }, 1000);
                }

            } else {
                $location.path(returnUrl);
            }

        }, function (error) { $rootScope.showNotifyError( error); $scope.buttonClicked = false; $scope.loading = false; });
    }

});

app.controller('newFolderController', function ($rootScope, $scope, $routeParams, $route, $location, dataService, $timeout) {

    $rootScope.activePage = "folder";
    document.title = getFormattedPageTitle("New Folder");

    dataService.getVersion(function () { sessionStorage.setItem("isLoggedin", true); }, function () { sessionStorage.setItem("isLoggedin", false); $location.path('/login'); return; });

    $scope.folder = { props: { active: "true" } };

    $scope.editmode = false;
    $scope.loading = false;
    $scope.updated = false;

    var folderId = $routeParams.folderId == undefined ? "" : $routeParams.folderId;
    var folderName = $routeParams.folderName == undefined ? "" : $routeParams.folderName;

    var returnUrl = folderId != "" ? ("/explorer/" + folderId) : "/explorer";


    var editFolderId = $routeParams.fid;

    //Edit connection
    if (editFolderId != undefined) {
        document.title = getFormattedPageTitle("Edit Folder");
        $scope.editmode = true;
        dataService.getFolder(editFolderId, function (folder) {
            folder.props.active = folder.props.active.toString();
            $scope.folder = folder;
        }, function (error) { $rootScope.showNotifyError(error); });
    } else { // create new connection
        $scope.editmode = false;
        document.title = getFormattedPageTitle("New Folder");
        if (folderId != "") {
            $scope.folder.parentFolder = folderId;
        }
    }

    $scope.saveFolder = function () {
        $scope.buttonClicked = true;

        //if ($scope.folder.props.name == undefined) {
        //    return;
        //}
        $scope.loading = true;
        dataService.saveFolder($scope.folder, function (folder) {

            $scope.buttonClicked = false;
            $scope.loading = false;

            if ($scope.editmode == true) {
                $scope.updated = true;

                if ($scope.folder.parentFolder == null) {
                    $timeout(function () { $location.path("/explorer"); }, 1000);
                } else {
                    $timeout(function () { $location.path("/explorer/" + $scope.folder.parentFolder); }, 1000);
                }


            } else {
                $location.path(returnUrl);
            }

        }, function (error) { $rootScope.showNotifyError( error); $scope.buttonClicked = false; $scope.loading = false; });

    }

});


app.controller('newRuleController', function ($rootScope, $scope, $route, $routeParams, $location, dataService, $timeout) {


    $rootScope.activePage = "rule";

    var browser = getBrowser();

    dataService.getVersion(function () { sessionStorage.setItem("isLoggedin", true); }, function () { sessionStorage.setItem("isLoggedin", false); $location.path('/login'); return; });

    $scope.currentStep = 1;
    $scope.editmode = false;

    $scope.deletingShape = null;
    $scope.loading = true;

    var addingNodesBy = "drag";

    var folder = "";
    $scope.activeFolderId = 0;
    var lastCreatedShape = null;

    var folderName = "";

    var diagram;
    $scope.selectedDiagramTool = "select";
    var editingShapeId = null;

    $scope.droppedPosition = { x: 0, y: 0 };

    var lastClickedShape = null;

    var lineWidth = 2;
    var defaultColor = "#2e3f50";
    var yesColor = "#46cc25";
    var noColor = "#e5252a";
    var jobColor = "#46cc25";
    var ruleColor = "#f39d38";

    var connectorsDefaults = [
                    { name: "top", width: 1, height: 1, fill: "transparent", stroke: { color: "transparent" } },
                    { name: "right", width: 1, height: 1, fill: "transparent", stroke: { color: "transparent" } },
                    { name: "bottom", width: 1, height: 1, fill: "transparent", stroke: { color: "transparent" } },
                    { name: "left", width: 1, height: 1, fill: "transparent", stroke: { color: "transparent" } },
                    { name: "auto", width: 20, height: 20, fill: "transparent", stroke: { color: "#999" }, hover: { fill: "#ccc", stroke: "#ededed" } }
    ];


    initDraggableShapes();

    hideDetailsBox();

    initRuleDiagram();

    var folderId = 0;

    //Edit rule
    if ($routeParams.ruleId != undefined) {
        document.title = getFormattedPageTitle("Edit Rule");
        dataService.getRule($routeParams.ruleId, function (rule) {

            $scope.editmode   = true;
            rule.props.active = rule.props.active.toString();
            $scope.rule       = rule;

            folderId = rule.folder;

            initJobAndRuleFinderPopup(folderId);

            drawRuleDiagram(JSON.parse(rule.detail));

        }, function (error) { $rootScope.showNotifyError( error); });
    } else { // create new rule
        document.title = getFormattedPageTitle("New Rule");

        folder = $routeParams.folderId == undefined ? "" : $routeParams.folderId;
        folderName = $routeParams.folderName == undefined ? "" : $routeParams.folderName;

        folderId = $routeParams.folderId == undefined ? 0 : $routeParams.folderId;

        initJobAndRuleFinderPopup(folderId);

        var logginedUserId = window.sessionStorage.getItem("userId");

        $scope.rule = { rights: { owner: logginedUserId }, props: { active: "true" } };


        if (folder != "") {
            $scope.rule.folder = folder;
        }

        $scope.editmode = false;


        addShapeToDiagram("start", $scope.droppedPosition, "init");
    }

    $scope.errorText = "";

    $scope.nextStep = function () {
        var cs = $scope.currentStep;
        cs++;
        if (cs > 3) {
            cs = 3;
        }
        if (cs == 2) {
            setTimeout(function () { diagram.bringIntoView(diagram.shapes); }, 1);
        }

        $scope.currentStep = cs;

        $("div.k-animation-container").hide();
    }

    $scope.prevStep = function () {
        var cs = $scope.currentStep;
        cs--;
        if (cs < 1) {
            cs = 1;
        }

        if (cs == 3) {
            cs = 2;
        }

        $scope.currentStep = cs;
        $("div.k-animation-container").hide();
    }

    var returnUrl = "/explorer/" + folder;
    $scope.saveRule = function () {


        $scope.ruleEdited = false;
        //if ($scope.rule.props.active == "") {
        //    $scope.errorText = "Please select rule status.";
        //    return;
        //}

        $scope.rule.detail = generateRuleJson();


        getRuleDiagramImageAsBinary(function (base64) {
            $scope.rule.image = base64;

            dataService.saveRule($scope.rule, function (data) {

                $scope.currentStep = 4;

                //Editing a job
                if ($scope.rule.id != undefined) {
                    $scope.ruleEdited = true;

                    if ($scope.rule.folder == null) {
                        $timeout(function () { $location.path("/explorer"); }, 1000);
                    } else {

                        $timeout(function () { $location.path("/explorer/" + $scope.rule.folder); }, 1000);
                    }
                    return;
                } else {

                    if (folder == "") {
                        $timeout(function () { $location.path("/explorer"); }, 1000);
                    } else {
                        $location.path(returnUrl);
                    }

                }

            }, function (error) { $rootScope.showNotifyError(error); });

        });
    };

    $("#addEditDecisionConnectionPopup").on('hidden.bs.modal', function () {
        $scope.closeDecisionTypePopup();
    })

    $scope.closeDecisionTypePopup = function () {
        if (lastCreatedShape != null) {
            removeShapeConnections(lastCreatedShape);
            diagram.remove(lastCreatedShape);
            lastCreatedShape = null;
        }
        $("#addEditDecisionConnectionPopup").modal("hide");
    }

    $scope.showPopupToAddDiagramShape = function (shapeType, callingFromAngularMethod, addingBy) {

        $scope.openedPopupType = shapeType;

        addingNodesBy = addingBy;

        $(".k-animation-container").hide();

        switch (shapeType) {
            case "job":
            case "rule": {

                $scope.activeFolderId = folderId;

                bindJobsOrRulesDataToPopup(folderId, shapeType);
                $("#selectJobAndRulePopup").modal("show");
                break;
            }
            case "decision": {
                if (callingFromAngularMethod) {
                    $scope.expression = undefined;
                } else {
                    $scope.$apply(function () { $scope.expression = undefined; });
                }

                $("#addEditDecisionExpressionPopup").modal("show");
                break;
            }
            case "comment": {

                if (callingFromAngularMethod) {
                    $scope.comment = undefined;
                } else {
                    $scope.$apply(function () { $scope.comment = undefined; });
                }


                $("#addEditCommentPopup").modal("show");
                break;
            }
        }


        editingShapeId = null;
    }


    $scope.addShapeToDiagram = function (shapeType, data, addingBy) {

        if (addingBy != undefined)
            addingNodesBy = addingBy;

        if (editingShapeId == null) {
            if ($scope.autoConnectWhenAddingNewShape == true) {
                addShapeAndConnect(shapeType, data);
                $scope.autoConnectWhenAddingNewShape == false;
            } else {
                addShapeToDiagram(shapeType, data, addingNodesBy);
                $scope.droppedPosition = { x: 0, y: 0 };
            }

            if (shapeType.toLowerCase() == "decision") {
                $("#addEditDecisionExpressionPopup").modal("hide");
            } else if (shapeType.toLowerCase() == "comment") {
                $("#addEditCommentPopup").modal("hide");
            } else {
                $("#selectJobAndRulePopup").modal("hide");
            }

        } else {

            var editingShape = diagram.getShapeById(editingShapeId);
            if (editingShape) {

                if (shapeType.toLowerCase() == "decision") {
                    editingShape.dataItem.expression = data.expression;
                    $("#addEditDecisionExpressionPopup").modal("hide");
                }
                else if (shapeType.toLowerCase() == "comment") {
                    editingShape.dataItem.comment = data.comment;
                    $("#addEditCommentPopup").modal("hide");
                    editingShape.redrawVisual();
                } else if (shapeType.toLowerCase() == "job") {

                    editingShape.dataItem.jobId = data.jobId;
                    editingShape.dataItem.name = data.name;
                    editingShape.dataItem.description = data.description;
                    $("#selectJobAndRulePopup").modal("hide");
                } else if (shapeType.toLowerCase() == "rule") {
                    editingShape.dataItem.ruleId = data.ruleId;
                    editingShape.dataItem.name = data.name;
                    editingShape.dataItem.description = data.description;
                    $("#selectJobAndRulePopup").modal("hide");
                }


            }

            editingShapeId = null;
        }

        $scope.autoConnectWhenAddingNewShape = false;

    }

    $scope.changeDecisionConnection = function (decisionType) {
        changeDecisionTypeOfConnection(editingShapeId, decisionType);
        lastCreatedShape = null;
        $("#addEditDecisionConnectionPopup").modal("hide");
    }


    $scope.selectDiagramTool = function (toolType) {
        selectDiagramTool(toolType);
        $scope.selectedDiagramTool = toolType;
    }

    $scope.zoomToDiagram = function (zoomValue) {
        zoom(zoomValue);
    }

    $scope.resetViewport = function () {
        centerDiagram();
    }

    $scope.generateJson = function () {
        generateRuleJson();
    }

    $scope.validateDiagram = function () {
        validateDiagramJson(generateRuleJson());
    }

    $scope.getDiagramImage = function () {
        getRuleDiagramImageAsBinary(function (base64) {

            var win = window.open(base64, '_blank');
            win.focus();
        });
    }

    $scope.deleteShape = function () {



        deleteSelectedShapes();
        $("#deletePopup").modal("hide");
    }

    $scope.fullScreen = function () {

        var fsElement = document.fullscreenElement ||
                       document.webkitFullscreenElement ||
                       document.mozFullScreenElement ||
                       document.msFullscreenElement;

        if (fsElement == undefined) {
            requestFullScreen();
        } else {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
        }

    }


    //#region * Job & Rule Finder START*//
    $scope.jobsOrRules = [];
    $scope.menuFolders = [];
    $scope.selectedMenuFolderId = 0;

    $scope.selectAndSaveJobOrRule = function (item) {
        $scope.selectJobOrRule(item);
        $scope.saveJobOrRuleSelection();
    }

    $scope.saveJobOrRuleSelection = function () {

        var item = $scope.selectedJobOrRule;

        if (editingShapeId != null) {

            var shape = diagram.getShapeById(editingShapeId);
            if ($scope.openedPopupType == "rule") {
                shape.dataItem.ruleId = item.id;
            } else {
                shape.dataItem.jobId = item.id;
            }

            shape.dataItem.name = item.props.name;
            shape.dataItem.description = item.props.description;

            shape.redrawVisual();

        } else {

           

            var diagramShapeDataItem = { name: item.props.name, description: item.props.description, x: $scope.droppedPosition.x, y: $scope.droppedPosition.y };
            if ($scope.openedPopupType == "rule") {
                diagramShapeDataItem.ruleId = item.id;
            } else {
                diagramShapeDataItem.jobId = item.id;
            }

            $scope.addShapeToDiagram($scope.openedPopupType, diagramShapeDataItem);
        }

        $("#selectJobAndRulePopup").modal("hide");

    }

    $scope.selectJobOrRule = function (item) {
        $scope.jobsOrRules.forEach(function (i) { i.selected = false; });
        item.selected = true;
        $scope.selectedJobOrRule = item;
    }

    $scope.viewSubFolders = function (folder) {
        if (folder.expended == true) {
            extendTreeViewFolderForPopup(folder.id, "tree_click");
            return;
        }

        dataService.getExplorerFolders(folder.id, function (folders) {
            folder.folders = folders;
            folder.expended = true;
            extendTreeViewFolderForPopup(folder.id, "tree_click");
        }, function () { });
    }

    $scope.viewJobsAndRules = function (folderId) {
        $scope.activeFolderId = folderId;
        bindJobsOrRulesDataToPopup(folderId, $scope.openedPopupType);
    }

    function initJobAndRuleFinderPopup(folderId) {
        dataService.getExplorerFolders(0, function (folders) {

            $scope.activeFolderId = folderId;

            $scope.menuFolders = folders;

            if (parseInt(folderId) > 0) {
                dataService.getFolderParents(folderId, function (_folders) {
                    _folders.push({ id: folderId });
                    _folders.reverse();
                    _folders.forEach(function (_folder, index) {
                        index++
                        setTimeout(function () {
                            $("a[data-expand-button='rule'][data-expand='" + _folder.id + "']").click();
                        }, index * 1000);
                    });
                }, function () { });
            }

        }, function () { });
    }


    function bindJobsOrRulesDataToPopup(_folderId, itemType) {
        $scope.popupDataLoading = true;
        $scope.jobsOrRules = [];
        $scope.selectedJobOrRule = undefined;
        if ($scope.openedPopupType == "rule") {
            dataService.getExplorerRules(_folderId, function (rules) {
                rules = rules.filter(function (r) { return r.props.active == true; });
                $scope.popupDataLoading = false;
                $scope.jobsOrRules = rules;
            }, function () { });
        } else {
            dataService.getExplorerJobs(_folderId, function (jobs) {
                jobs = jobs.filter(function (r) { return r.props.active == true; });
                $scope.jobsOrRules = jobs;
                $scope.popupDataLoading = false;
            }, function () { });

        }
    }

    //#endregion *Job & Rule Finder END*//

    //#region Rule Diagram Functions



    function initDraggableShapes() {
        $("a[data-drag-shape]").draggable({ helper: "clone", delay: 200 });
        $("#diagram").droppable({
            revert: true,
            drop: function (event, ui) {

                if (event.type == "drop") {

                    var shapeType = $(ui.draggable[0]).attr("data-drag-shape");

                    var shapeX = 0;
                    var shapeY = 0;

                    if (shapeType == "start" || shapeType == "end") {
                        shapeX = 20;
                        shapeY = 20;
                    } else if (shapeType == "rule" || shapeType == "job") {
                        shapeX = 70;
                        shapeY = 35;
                    } else if (shapeType == "decision") {
                        shapeX = 30;
                        shapeY = 30;
                    }

                    var clientX = event.pageX - shapeX;
                    var clientY = event.pageY - shapeY;

                    $scope.droppedPosition = diagram.documentToModel({ x: clientX, y: clientY });

                    $scope.autoConnectWhenAddingNewShape == false;

                    if (shapeType == "start" || shapeType == "end") {

                        addingNodesBy = "drag";

                        $scope.addShapeToDiagram(shapeType, { x: $scope.droppedPosition.x, y: $scope.droppedPosition.y });
                    } else {
                        $scope.showPopupToAddDiagramShape(shapeType, false);
                    }

                }
            }
        });
    }

    function initRuleDiagram() {
        //init rule diagram

        $("#diagram").kendoDiagram({

            pannable: { key: "ctrl" },

            layout: { subtype: "right", type: "tree", horizontalSeparation: 1, grid: { componentSpacingX: 1, componentSpacingY: 1, offsetX: 1, offsetY: 1 } },

            connectionDefaults: {
                stroke: { color: defaultColor, width: lineWidth }, type: "cascading", startCap: "", endCap: { type: "ArrowEnd", fill: defaultColor }
            },

            editable: { resize: false },
            zoomStart: function (e) {
                if (browser.name == "Firefox") {
                    e.point = new kendo.dataviz.diagram.Point(0, 0);
                }
            },

            select: function (e) {

                $("body").removeClass("hideTooltip");
                $("div.k-tooltip").parents(".k-animation-container").first().hide();
                $("div.k-tooltip").hide();

   
                if (e.selected.length == 0)
                    return;

                if ((window.fullScreen)
                    || (window.innerWidth == screen.width && window.innerHeight == screen.height)) {

                    setTimeout(function () {
                        $(".k-animation-container").appendTo("#diagram_container");
                    }, 1);

                } else {
                    repositionTooltipBasedOnScrollForFirefox();
                }

                $("body").addClass("hideTooltip");
            },
            mouseEnter: function (e) {

                //Mouse enter on the connection
                if (e.item.hasOwnProperty('sourceConnector') && e.item.sourceConnector != undefined) {
                    lastClickedShape = lastClickedShape = e.item.sourceConnector;
                }

                var selectedElement = diagram.select()[0];
                if (selectedElement != undefined && e.item.id == selectedElement.id) {
                    setTimeout(function () {
                        $("div.k-tooltip").parents(".k-animation-container").first().hide();
                    }, 1000);
                }
            },

            click: onDoubleClick,
            add: function (e) {
                //if new connection has been added by clicking the connectors, configure editable tools
                if (e.connection != undefined) {

                    if (e.connection.sourceConnector != undefined && e.connection.sourceConnector.shape.dataItem.type.toLowerCase() == "decision") {
                        e.connection.options.editable.tools = generateToolbox("decision_connection");
                    } else {
                        e.connection.options.editable.tools = generateToolbox("connection");
                    }
                } else {

                    if (e.shape.dataItem.type.toLowerCase() == "start") {
                        if (isThereAnyShapeWithSameType("start") == true) {
                            setTimeout(function () { diagram.remove(e.shape); }, 0);

                        }
                    }
                }
            },
            remove: function (e) {
                //Delete removed shapes connections too
                if (e.shape) {
                    removeShapeConnections(e.shape);
                }
            },
            drag: function (e) {
            },
            dragEnd: function (e) {

                repositionTooltipBasedOnScrollForFirefox();

                //Connection is dropping
                if (e.connections.length == 1 && e.shapes.length == 0) {
                    var connection = e.connections[0];


                    //if its not connected
                    if (connection.targetConnector == undefined
                        || (connection.targetConnector != null && connection.targetConnector.shape.dataItem != undefined && connection.targetConnector.shape.dataItem.type.toLowerCase() == "start")
                        || (connection.sourceConnector != null && connection.sourceConnector.shape.dataItem != undefined && connection.sourceConnector.shape.dataItem.type.toLowerCase() == "end")
                        || (connection.targetConnector != null && connection.targetConnector.shape.dataItem != undefined && connection.targetConnector.shape.dataItem.type.toLowerCase() == "comment")

                        ) {
                        //remove connection
                        diagram.remove(connection, false);
                        return;
                    }

                    if (connection.targetConnector != undefined && connection.sourceConnector != undefined && connection.sourceConnector.shape.dataItem.type.toLowerCase() == "decision" && connection.dataItem != undefined && connection.dataItem.decisionType == undefined) {

                        var estimatedDecisionType = estimateDecisionType(connection.sourceConnector.shape);
                        $scope.$apply(function () { $scope.decisionType = estimatedDecisionType; });
                        editingShapeId = connection.id;

                        $("#addEditDecisionConnectionPopup").modal("show");

                        setTimeout(function () { $("div.k-animation-container").hide(); }, 100);

                    }

                    if (connection.sourceConnector == undefined) {
                        var connectedShape = connection.targetConnector.shape;
                        diagram.remove(connection, false);
                        diagram.connect(lastClickedShape, connectedShape);
                    }
                }
            }
        });

        diagram = $("#diagram").getKendoDiagram();
    }

    function repositionTooltipBasedOnScrollForFirefox() {

        if (browser.name == "Firefox") {
            setTimeout(function () {
                var top = (window.pageYOffset || document.documentElement.scrollTop) - (document.documentElement.clientTop || 0);
                var currentTop = parseFloat($("div.k-animation-container").css("top"));
                currentTop = currentTop - top;
                $("div.k-animation-container").css("top", currentTop);
            }, 1);
        }

      
    }

    function generateRuleJson() {

        var data = [];
        var shapes = diagram.shapes;

        shapes.forEach(function (item) {
            var itemConnections = [];

            item.connectors.filter(function (c) { return c.connections.length > 0; }).forEach(function (connection) {
                connection.connections.forEach(function (cn) {
                    var connectedTo = cn.to;
                    if (connectedTo.dataItem != undefined) {
                        itemConnections.push({ parentId: item.id, connectionId: cn.id, id: connectedTo.id, dataItem: { x: connectedTo.dataItem.x, y: connectedTo.dataItem.y, name: connectedTo.dataItem.name, type: connectedTo.dataItem.type, links: connectedTo.dataItem.links, expression: connectedTo.dataItem.expression, id: connectedTo.dataItem.id } });
                    } else {
                        connectedTo = connectedTo.shape;
                        itemConnections.push({ parentId: item.id, connectionId: cn.id, id: connectedTo.id, dataItem: { x: connectedTo.dataItem.x, y: connectedTo.dataItem.y, name: connectedTo.dataItem.name, type: connectedTo.dataItem.type, links: connectedTo.dataItem.links, expression: connectedTo.dataItem.expression, id: connectedTo.dataItem.id } });
                    }
                });
            });

            itemConnections = itemConnections.filter(function (tc) { return item.id != tc.id });

            data.push({ uid: item.dataItem.uid, id: item.id, x: item.position().x, y: item.position().y, dataItem: { x: item.dataItem.x, y: item.dataItem.y, name: item.dataItem.name, type: item.dataItem.type, links: item.dataItem.links, expression: item.dataItem.expression, id: item.dataItem.id }, connectedTo: itemConnections });
        });

        diagramJson = { nodes: [], links: [], trueLinks: [], falseLinks: [] };

        data.forEach(function (s, index) {
            var shape = diagram.getShapeById(s.id);

            var shapeModelId = s.dataItem.id;

            var connections = new Object();
            connections[shapeModelId] = [];

            var yesConnections = new Object();
            yesConnections[shapeModelId] = [];

            var noConnections = new Object();
            noConnections[shapeModelId] = [];


            diagram.connections.filter(function (c) { return c.sourceConnector.shape.id == s.id }).forEach(function (fc) {

                if (fc.to.dataItem == undefined) {
                    fc.to.dataItem = fc.to.shape.dataItem;
                }

                if (fc.dataItem == undefined || fc.dataItem.decisionType == undefined) {
                    connections[shapeModelId].push(fc.to.dataItem.id);
                } else if (fc.dataItem.decisionType == true) {
                    yesConnections[shapeModelId].push(fc.to.dataItem.id);
                } else {
                    noConnections[shapeModelId].push(fc.to.dataItem.id);
                }

            });

            if (connections[shapeModelId] && connections[shapeModelId].length > 0) {
                diagramJson.links.push(connections);
            }


            if (yesConnections[shapeModelId] && yesConnections[shapeModelId].length > 0)
                diagramJson.trueLinks.push(yesConnections);

            if (noConnections[shapeModelId] && noConnections[shapeModelId].length > 0)
                diagramJson.falseLinks.push(noConnections);

            var node = { id: s.dataItem.id, x: parseInt(shape.position().x), y: parseInt(shape.position().y), type: shape.dataItem.type };



            if (shape.dataItem.type.toLowerCase() == "job") {
                node.jobId = shape.dataItem.jobId;
                node.name = shape.dataItem.name;
                node.description = shape.dataItem.description;
            }
            else if (shape.dataItem.type.toLowerCase() == "rule") {
                node.ruleId = shape.dataItem.ruleId;
                node.name = shape.dataItem.name;
                node.description = shape.dataItem.description;
            }
            else if (shape.dataItem.type.toLowerCase() == "decision") {
                node.expression = shape.dataItem.expression;
            }
            else if (shape.dataItem.type.toLowerCase() == "comment") {
                node.comment = shape.dataItem.comment;
            }

            diagramJson.nodes.push(node);
        });


        var newJson = { nodes: diagramJson.nodes, links: {}, trueLinks: {}, falseLinks: {} }
        diagramJson.links.forEach(function (link) {
            var props = Object.keys(link);
            newJson.links[props[0]] = link[props[0]];
        });

        diagramJson.trueLinks.forEach(function (tlink) {
            var props = Object.keys(tlink);
            newJson.trueLinks[props[0]] = tlink[props[0]];
        });

        diagramJson.falseLinks.forEach(function (flink) {
            var props = Object.keys(flink);
            newJson.falseLinks[props[0]] = flink[props[0]];
        });



        return JSON.stringify(newJson);
    }


    function drawRuleDiagram(dataModel) {

        //generate nodes
        dataModel.nodes.forEach(function (shape) {

            var editableOptions = { connect: true };

            var dataItem = { id: shape.id, x: shape.x, y: shape.y, type: shape.type };

            if (shape.type.toLocaleLowerCase() == "job") {
                dataItem.jobId = shape.jobId;
                dataItem.name = shape.name;
                dataItem.description = shape.description;
            } else if (shape.type.toLocaleLowerCase() == "rule") {
                dataItem.ruleId = shape.ruleId;
                dataItem.name = shape.name;
                dataItem.description = shape.description;
            } else if (shape.type.toLocaleLowerCase() == "decision") {
                dataItem.expression = shape.expression;
            }
            else if (shape.type.toLocaleLowerCase() == "comment") {
                dataItem.comment = shape.comment;
                editableOptions.connect = false;
            } else if (shape.type.toLocaleLowerCase() == "end") {
                editableOptions.connect = false;
            }

            var newCreatedShape = new kendo.dataviz.diagram.Shape({
                editable: editableOptions,
                dataItem: dataItem,
                x: shape.x, y: shape.y, visual: drawRuleShape,

                connectors: connectorsDefaults
            });

            diagram.addShape(newCreatedShape);

        });

        //Generate default links
        drawConnections(dataModel.links, "default");
        //Generate true links
        drawConnections(dataModel.trueLinks, "yes");
        //Generate false links
        drawConnections(dataModel.falseLinks, "no");

    }

    function drawConnections(sourceLinksObject, linkType) {

        //Convert object map to array
        var sourceLinks = [];
        var props = Object.keys(sourceLinksObject);
        props.forEach(function (prop) {
            var cnn = new Object();
            cnn[prop] = sourceLinksObject[prop];

            sourceLinks.push(cnn);
        });

        sourceLinks.forEach(function (connection) {

            var shapeId = Object.keys(connection)[0];
            var connectToShapes = connection[shapeId.toString()];
            var fromShape = getShapeByDataItem("id", shapeId.toString());

            connectToShapes.forEach(function (connectShapeId) {
                var connectionOptions = { width: lineWidth, editable: { tools: generateToolbox("connection") } };

                if (linkType == "default") {

                } else if (linkType == "yes") {
                    connectionOptions = { stroke: { color: yesColor, width: lineWidth }, endCap: { type: "ArrowEnd", fill: yesColor }, content: { text: "YES", color: yesColor, fontFamily: "Lato", fontWeight: "700", fontSize: 12 }, dataItem: { decisionType: true }, editable: { tools: generateToolbox("decision_connection") } };
                } else {
                    connectionOptions = { stroke: { color: noColor, width: lineWidth }, endCap: { type: "ArrowEnd", fill: noColor }, content: { text: "NO", color: noColor, fontFamily: "Lato", fontWeight: "700", fontSize: 12 }, dataItem: { decisionType: false }, editable: { tools: generateToolbox("decision_connection") } };
                }

                var connectShape = getShapeByDataItem("id", connectShapeId.toString());
                diagram.connect(fromShape, connectShape, connectionOptions);



            });

        });
    }

    function drawRuleShape(options) {

        //Draw rule diagram shapes based on its dataItem
        var dataviz = kendo.dataviz;
        var g = new dataviz.diagram.Group();
        var dataItem = options.dataItem;

        g.x = options.dataItem.x;
        g.y = options.dataItem.y;

        options.editable.tools = generateToolbox(dataItem.type);

        if (dataItem.type.toLowerCase() == "start") {

            g.append(new dataviz.diagram.Circle({ width: 42, stroke: { width: lineWidth, color: defaultColor }, fill: "#fff" }));

            var textBlockY = 2;
            if (browser.name == "Firefox" || browser.name == "Opera" || browser.name == "Safari") {
                var textBlockY = 6;
            }

            g.append(new dataviz.diagram.TextBlock({ x: 12, y: textBlockY, text: "S", fontSize: 26, fill: defaultColor, fontWeight: "800" }));

        } else if (dataItem.type.toLowerCase() == "end") {

            g.append(new dataviz.diagram.Circle({ width: 39, stroke: { width: 5, color: defaultColor }, fill: "#fff" }));

            var textBlockY = 0;
            if (browser.name == "Firefox" || browser.name == "Opera" || browser.name == "Safari") {
                var textBlockY = 4;
            }

            g.append(new dataviz.diagram.TextBlock({ x: 9, y: textBlockY, text: "E", fontSize: 26, fill: defaultColor, fontWeight: "800" }));

        }
        else if (dataItem.type.toLowerCase() == "comment") {

            if (dataItem.comment == undefined || dataItem.comment == null) {
                dataItem.comment = "";
            }

            //calculate rect height based on text char count
            var rectHeight = 45;
            for (var i = 0; i < Math.round(dataItem.comment.trim().length / 22) ; i++) {
                var text = dataItem.comment.substr(i * 22, 22);
                if (text.length > 0) {
                    if (i > 0)
                        rectHeight += (i * 2) + 8;
                }
            }

            var rect = new dataviz.diagram.Rectangle({ width: 150, height: rectHeight, fill: "#f4f2e4", stroke: { width: 0.5, color: "#bcad8d", dashType: "dot" } });
            g.append(rect);

            for (var i = 0; i < dataItem.comment.trim().length / 22 ; i++) {
                var text = dataItem.comment.substr(i * 22, 22);
                if (text.length > 0) {
                    g.append(new dataviz.diagram.TextBlock({ x: 8, y: (i * 15) + 8, fontFamily: "Lato", text: text, fontSize: 11, fill: "#666666" }));

                }
            }

        }
        else if (dataItem.type.toLowerCase() == "decision") {

            g.append(new dataviz.diagram.Rectangle({ width: 57.98, height: 57.98, stroke: { width: lineWidth, color: defaultColor }, fill: "#fff" }));
            g.append(new dataviz.diagram.TextBlock({ x: 15, y: 2, text: "+", fontSize: 48, fill: defaultColor }));

            g.rotate(45, { x: 18, y: 51 });

            g.drawingElement.options.tooltip = {
                content: generateDecisionTooltip(dataItem),
                position: "bottom",
                showAfter: 1000,
                shared: "false"
            };

        }
        else if (dataItem.type.toLowerCase() == "job") {
            g.append(new dataviz.diagram.Rectangle({ width: 142, height: 82, stroke: { width: lineWidth, color: defaultColor }, fill: "#fff" }));

            g.append(new dataviz.diagram.Rectangle({ width: 142, height: 20, fill: jobColor, stroke: { width: lineWidth, color: defaultColor } }));

            g.append(new dataviz.diagram.TextBlock({ x: 56, y: 3, text: "JOB", fontSize: 12, lineHeight: 12, fill: "#fff", fontFamily: "Lato", fontWeight: "700" }));


            var itemName = dataItem.name;
            if (dataItem.name.length > 16)
                dataItem.name = dataItem.name.substr(0, 16).trim() + "...";

            var textBlockXPosition = 70 - (dataItem.name.length) * 3;

            g.append(new dataviz.diagram.TextBlock({ x: textBlockXPosition, y: 40, text: dataItem.name, fontSize: 12, fill: defaultColor, fontWeight: "700", fontFamily: "Lato" }));

            g.drawingElement.options.tooltip = {
                content: generateTooltip(itemName, dataItem.description),
                position: "bottom",
                showAfter: 1000,
                shared: "false"
            };

         
        } else {

            g.append(new dataviz.diagram.Rectangle({ width: 142, height: 82, stroke: { width: lineWidth, color: defaultColor }, fill: "#fff" }));

            g.append(new dataviz.diagram.Rectangle({ width: 142, height: 20, fill: ruleColor, stroke: { width: lineWidth, color: defaultColor } }));

            g.append(new dataviz.diagram.TextBlock({ x: 52, y: 3, text: "RULE", fontSize: 12, lineHeight: 12, fill: "#fff", fontFamily: "Lato", fontWeight: "700" }));

            var itemName = dataItem.name;
            if (dataItem.name.length > 16)
                dataItem.name = dataItem.name.substr(0, 16).trim() + "...";

            var textBlockXPosition = 70 - (dataItem.name.length) * 3;

            g.append(new dataviz.diagram.TextBlock({ x: textBlockXPosition, y: 40, text: dataItem.name, fontSize: 12, fill: defaultColor, fontWeight: "700", fontFamily: "Lato" }));

            g.drawingElement.options.tooltip = {
                content: generateTooltip(itemName, dataItem.description),
                position: "bottom",
                showAfter: 1000,
                shared: "false"
            };

        }

        return g;
    }

    function zoom(zoomValue) {
        //Zoom in/out to diagram
        var currentZoom = diagram.zoom();
        diagram.zoom(currentZoom + zoomValue);
        diagram.bringIntoView(diagram.shapes);
    }

    function centerDiagram() {
        //Center the diagram
        diagram.zoom(1, { x: 0, y: 0 });
        diagram.pan(new kendo.dataviz.diagram.Point(0, 0));
        diagram.bringIntoView(diagram.shapes);
    }

    function selectDiagramTool(toolType) {
        //Select pan tool or select tool
        if (toolType == "pan") {
            diagram.options.pannable.key = "none";
        } else {
            diagram.options.pannable.key = "ctrl";
        }
    }

    function getRuleDiagramImageAsBinary(callback) {
        diagram.exportImage().then(function (base64) {
            callback(base64);
        });
    }

    function changeDecisionTypeOfConnection(selectedConnectionId, decisionType) {

        var obj = diagram.getShapeById(selectedConnectionId);

        if (obj.dataItem == undefined)
            obj.dataItem = {};
        if (obj) {
            if (decisionType == "yes") {

                obj.redraw({ stroke: { color: yesColor }, endCap: { type: "ArrowEnd", fill: yesColor }, content: { text: "YES", color: yesColor, fontFamily: "Lato", fontWeight: "700", fontSize: 12 } });
                obj.dataItem.decisionType = true;
            } else {
                obj.redraw({ stroke: { color: noColor }, endCap: { type: "ArrowEnd", fill: noColor }, content: { text: "NO", color: noColor, fontFamily: "Lato", fontWeight: "700", fontSize: 12 } });
                obj.dataItem.decisionType = false;
            }
        }
    }

    function generateToolbox(type) {

        var toolboxButtons = [];

        var seperatorHtml = "<span class='seperator'></span>";

        if (type.toLowerCase() == "end" || type.toLowerCase() == "connection") {
            toolboxButtons.push({
                type: "button",
                imageUrl: "/images/icon_delete.png",
                attributes: {},
                click: showDeletePopup
            });
        }
        else if (type.toLowerCase() == "comment") {
            toolboxButtons.push({
                type: "button",
                imageUrl: "/images/icon_gear.png",
                attributes: {},
                click: configureSelectedShape
            });

            toolboxButtons.push({
                template: seperatorHtml
            });

            toolboxButtons.push({
                type: "button",
                imageUrl: "/images/icon_delete.png",
                attributes: {},
                click: showDeletePopup
            });
        }
        else if (type.toLowerCase() == "decision_connection") {

            toolboxButtons.push({
                type: "button",
                imageUrl: "/images/icon_gear.png",
                attributes: {},
                click: configureSelectedShape
            });
            toolboxButtons.push({
                template: seperatorHtml
            });
            toolboxButtons.push({
                type: "button",
                imageUrl: "/images/icon_delete.png",
                attributes: {},
                click: showDeletePopup
            });
        }
        else {
            toolboxButtons.push({
                type: "button",
                imageUrl: "/images/icon_end.png",
                attributes: {},
                click: function () { addShapeAndConnect("end"); }
            });

            toolboxButtons.push({
                template: seperatorHtml
            });
            toolboxButtons.push({
                type: "button",
                imageUrl: "/images/icon_job.png",
                attributes: {},
                click: function () {

                    $scope.autoConnectWhenAddingNewShape = true;
                    $scope.$apply(function () { $scope.openedPopupType = "job"; });
                    $scope.showPopupToAddDiagramShape("job", false);

                }
            });

            toolboxButtons.push({
                template: seperatorHtml
            });

            toolboxButtons.push({
                type: "button",
                imageUrl: "/images/icon_rule.png",
                attributes: {},
                click: function () {

                    $scope.autoConnectWhenAddingNewShape = true;
                    $scope.$apply(function () { $scope.openedPopupType = "rule"; });
                    $scope.showPopupToAddDiagramShape("rule", false);
                }
            });

            toolboxButtons.push({
                template: seperatorHtml
            });

            toolboxButtons.push({
                type: "button",
                imageUrl: "/images/icon_decision.png",
                attributes: {},
                click: function () {
                    $scope.autoConnectWhenAddingNewShape = true;
                    $scope.$apply(function () { $scope.expression = undefined; });
                    $scope.showPopupToAddDiagramShape("decision", false);
                }
            });

            toolboxButtons.push({
                template: seperatorHtml
            });

            if (type != "start") {
                toolboxButtons.push({
                    type: "button",
                    imageUrl: "/images/icon_gear.png",
                    attributes: {},
                    click: configureSelectedShape
                });

                toolboxButtons.push({
                    template: seperatorHtml
                });
            }


            toolboxButtons.push({
                type: "button",
                imageUrl: "/images/icon_delete.png",
                attributes: {},
                click: showDeletePopup
            });
        }

        return toolboxButtons;
    }

    function renewDiagramShapeDataItemIds() {
        diagram.shapes.forEach(function (shape, index) {
            shape.dataItem.id = index + 1;
        });
    }

    function showDeletePopup() {
        $scope.$apply(function () { $scope.deletingShape = diagram.select(); });
        $("div.k-animation-container").hide();
        $("#deletePopup").modal("show");
    }

    function deleteSelectedShapes() {

        diagram.connections.filter(function (connection) { return connection.isSelected; }).forEach(function (selectedConnection) {
            diagram.remove(selectedConnection);
        });

        diagram.shapes.filter(function (shape) { return shape.isSelected; }).forEach(function (selectedShape) {

            // Delete selected shape with its connection
            if (selectedShape.connectors) {
                var connections = selectedShape.connectors.filter(function (ctr) { return ctr.connections.length > 0 })[0];

                if (connections != undefined) {
                    connections = connections.connections;
                    connections.forEach(function (connection, index) {
                        index++
                        setTimeout(function () { diagram.remove(connection); }, 30 * index);
                    });
                }
            }

            diagram.remove(selectedShape);

        });

        renewDiagramShapeDataItemIds();
    }


    function addShapeToDiagram(type, data, source) {

        if (type != "connection") {

            var hasShapeWithSamePosition = isThereAnyShapeWithSamePosition(data.x, data.y);
            if (hasShapeWithSamePosition) {

                data.y += 25;

                var blankFound = false;
                while (blankFound == false) {
                    blankFound = !isThereAnyShapeWithSamePosition(data.x, data.y);
                    if (blankFound == false) {
                        data.y += 25;
                    }
                }
            }

            //Only one start shape can be
            if (type.toLowerCase() == "start") {
                if (isThereAnyShapeWithSameType("start") == true) {
                    return;
                }
            }

            //generate id
            var shapeId = diagram.shapes.length + 1;
            var dataItemOptions = { id: shapeId, type: type, x: data.x, y: data.y };
            var editableOptions = { connect: true };

            if (type.toLowerCase() == "decision") {
                dataItemOptions.expression = data.expression;
            } else if (type.toLowerCase() == "job") {
                dataItemOptions.jobId = data.jobId;
                dataItemOptions.name = data.name;
                dataItemOptions.description = data.description;
            } else if (type.toLowerCase() == "rule") {
                dataItemOptions.ruleId = data.ruleId;
                dataItemOptions.name = data.name;
                dataItemOptions.description = data.description;
            }
            else if (type.toLowerCase() == "comment") {
                dataItemOptions.comment = data.comment;
                editableOptions.connect = false;
            }
            else if (type.toLowerCase() == "end") {
                editableOptions.connect = false;
            }

            var shape = new kendo.dataviz.diagram.Shape({
                editable: editableOptions,
                x: data.x, y: data.y, dataItem: dataItemOptions, visual: drawRuleShape,
                connectors: connectorsDefaults

            });


            diagram.addShape(shape);
            diagram.select(shape);



            //if (source == "click") {
            //    setTimeout(function () { diagram.bringIntoView(diagram.shapes); }, 100);
            //}

            lastCreatedShape = shape;

        }

    }

    function addShapeAndConnect(shapeType, data) {

        var shapeId = diagram.shapes.length + 1;
        var selectedShape = diagram.select()[0];

        if (selectedShape == undefined)
            return;

        var selectedPosition = selectedShape.position();


        var selectedX = selectedPosition.x + 200;
        var selectedY = selectedPosition.y;

        if (selectedShape.dataItem.type == "job" || selectedShape.dataItem.type == "rule") {

            if (shapeType == "start" || shapeType == "end") {
                selectedY += 20;
            }

        } else if (selectedShape.dataItem.type == "start" || selectedShape.dataItem.type == "end") {

            if(shapeType == "job" || shapeType == "rule"){
                selectedY -= 20;
            }
            else if (shapeType == "decision") {
                selectedY -= 20;
            }

        } else { // decision
            if (shapeType == "start" || shapeType == "end") {
                selectedY += 20;
            }
            else if (shapeType == "job" || shapeType == "rule") {
                selectedY += 0;
            }
        }

        var hasShapeWithSamePosition = isThereAnyShapeWithSamePosition(selectedX, selectedY);
        if (hasShapeWithSamePosition) {
            selectedY += 15;
            selectedX -= 25;

            var blankFound = false;
            while (blankFound == false) {
                blankFound = !isThereAnyShapeWithSamePosition(selectedX, selectedY);
                if (blankFound == false) {
                    selectedY += 15;
                    selectedX -= 25;
                }
            }
        }

  
        var dataItemOptions = { id: shapeId, type: shapeType, x: selectedX, y: selectedY };
        var editableOptions = { connect: true };


        if (shapeType.toLowerCase() == "decision" && data != undefined) {
            dataItemOptions.expression = data.expression;
        } else if (shapeType.toLowerCase() == "job") {
            dataItemOptions.jobId = data.jobId;
            dataItemOptions.name = data.name;
            dataItemOptions.description = data.description;
        } else if (shapeType.toLowerCase() == "rule") {
            dataItemOptions.ruleId = data.ruleId;
            dataItemOptions.name = data.name;
            dataItemOptions.description = data.description;
        } else if (shapeType.toLowerCase() == "end") {
            editableOptions.connect = false;
        }


        var shape = new kendo.dataviz.diagram.Shape({
            x: selectedX, y: selectedY,
            editable: editableOptions,
            dataItem: dataItemOptions, visual: drawRuleShape,
            connectors: connectorsDefaults
        });


        diagram.addShape(shape);


        lastCreatedShape = shape;

        var createdConnection;
        if (selectedShape.dataItem.type.toLowerCase() == "decision") {
            //Connect from decision with configure and delete buttons
            createdConnection = diagram.connect(selectedShape, shape, { editable: { tools: generateToolbox("decision_connection") } });
        } else {
            //Connect from decision with configure and delete buttons
            createdConnection = diagram.connect(selectedShape, shape, { editable: { tools: generateToolbox("connection") } });
        }

        if (selectedShape.dataItem.type.toLowerCase() == "decision") {
            editingShapeId = createdConnection.id;

            //Configure default decision type based on other decision links of the selected shape
            $scope.decisionType = estimateDecisionType(selectedShape);
            $("div.k-animation-container").hide();
            $("#addEditDecisionConnectionPopup").modal("show");
        }

        setTimeout(function () { selectedShape.select(false); }, 200);
        setTimeout(function () { shape.select(true); }, 300);
    }

    function estimateDecisionType(decisionShape) {

        var connections = decisionShape.connectors.filter(function (ctr) { return ctr.connections.length > 0 })[0];

        var decisionConnections = connections.connections.filter(function (c) { return (c.from.id == decisionShape.id || (c.from.shape != undefined && c.from.shape.id == decisionShape.id)) && c.dataItem != undefined; });

        var trueLinksCount = decisionConnections.filter(function (r) { return r.dataItem.decisionType == true }).length;

        var falseLinksCount = decisionConnections.filter(function (r) { return r.dataItem.decisionType == false }).length;

        return trueLinksCount == falseLinksCount || falseLinksCount > trueLinksCount ? "yes" : "no";
    }

    function configureSelectedShape() {

        var selectedShape = diagram.select()[0];
        editingShapeId = selectedShape.id;

        if (selectedShape == undefined || selectedShape.dataItem == undefined || selectedShape.dataItem == null)
            return;

        //Decision connection configuration
        if (selectedShape.dataItem.hasOwnProperty("decisionType") == true) {
            //Edit decision connections' decision type
            lastCreatedShape = null;
            $scope.$apply(function () { $scope.decisionType = selectedShape.dataItem != undefined ? (selectedShape.dataItem.decisionType ? "yes" : "no") : "yes" });
            $("div.k-animation-container").hide();
            $("#addEditDecisionConnectionPopup").modal("show");
        } else if (selectedShape.dataItem.type.toLowerCase() == "rule") {

            $scope.$apply(function () { $scope.openedPopupType = "rule"; });

            dataService.getRule(selectedShape.dataItem.ruleId, function (rule) {

                $scope.activeFolderId = rule.folder == null ? 0 : rule.folder;

                bindJobsOrRulesDataToPopup(rule.folder, "rule");
                $("div.k-animation-container").hide();
                $("#selectJobAndRulePopup").modal("show");

            }, function (error) { $rootScope.showNotifyError(error); });


        } else if (selectedShape.dataItem.type.toLowerCase() == "job") {

            $scope.$apply(function () { $scope.openedPopupType = "job"; });

            dataService.getJob(selectedShape.dataItem.jobId, function (job) {
                $scope.activeFolderId = job.folder == null ? 0 : job.folder;

                bindJobsOrRulesDataToPopup(job.folder, "job");
                $("div.k-animation-container").hide();
                $("#selectJobAndRulePopup").modal("show");

            }, function (error) { $rootScope.showNotifyError(error); });


        }
        else if (selectedShape.dataItem.type.toLowerCase() == "comment") {

            $scope.$apply(function () { $scope.comment = selectedShape.dataItem.comment; });
            $("div.k-animation-container").hide();
            $("#addEditCommentPopup").modal("show");

        } else if (selectedShape.dataItem.type.toLowerCase() == "decision") {

            $scope.$apply(function () { $scope.expression = selectedShape.dataItem.expression; });
            $("div.k-animation-container").hide();
            $("#addEditDecisionExpressionPopup").modal("show");

        }

    }

    function getShapeByDataItem(property, value) {
        return diagram.shapes.filter(function (shape) {
            return (shape.dataItem[property] == value);
        })[0];
    }

    var clickCount = 0;
    var selectedShapeDBL = null;
    function onDoubleClick(e) {
        clickCount++;
     
        if (clickCount === 1) {
            singleClickTimer = setTimeout(function () {
                clickCount = 0; return;
            }, 250);
            setTimeout(function () {
                selectedShapeDBL = diagram.select()[0].id;
            }, 10);
        } else if (clickCount === 2) {

            setTimeout(function () {

                if (selectedShapeDBL != diagram.select()[0].id) {
                    clearTimeout(singleClickTimer);
                    clickCount = 0;
                } else {

                    configureSelectedShape();
                }

            }, 100);

        }
    }

    function isThereAnyShapeWithSamePosition(x, y) {
        var isSamePosition = diagram.shapes.filter(function (shape) {

            var xPosition = shape.position().x;
            var yPosition = shape.position().y;

            //return (   xPosition == x  && yPosition == y);


            return (((xPosition - 100) <= x) && ((xPosition + 100) >= x) && ((yPosition - 100) <= y) && ((yPosition + 100) >= y));
        })[0];

        return isSamePosition != null && isSamePosition != undefined;
    }

    function isThereAnyShapeWithSameType(shapeType) {
        var isSameType = diagram.shapes.filter(function (shape) {
            return shape.dataItem != undefined && shape.dataItem.type.toLowerCase() == shapeType;
        })[0];

        return isSameType != null && isSameType != undefined;
    }

    function validateDiagramJson(jsonData) {

        var rule = JSON.parse(jsonData);

        var validatableNodes = rule.nodes.filter(function (node) { return node.type.toLowerCase() != "end" && node.type.toLowerCase() != "comment" });

        var allNodesAreConnected = validatableNodes.filter(function (node) {

            var nodeId = node.id;

            //Default link check
            var nodeHasDefaultLinkFrom = rule.links.filter(function (connection) {
                return connection[nodeId] != undefined || (connection[nodeId] != undefined && connection[nodeId].length > 0);
            })[0] != null;


            //Yes link check
            var nodeHasYesLink = rule.trueLinks.filter(function (connection) {
                return connection[nodeId] != undefined || (connection[nodeId] != undefined && connection[nodeId].length > 0);
            })[0] != null;


            //Yes link check
            var nodeHasNoLink = rule.falseLinks.filter(function (connection) {
                return connection[nodeId] != undefined || (connection[nodeId] != undefined && connection[nodeId].length > 0);
            })[0] != null;

            return (nodeHasDefaultLinkFrom == true || (nodeHasYesLink == true || nodeHasNoLink == true));

        }).length == validatableNodes.length;


        var endNodes = rule.nodes.filter(function (node) { return node.type.toLowerCase() == "end" });
        var allEndNodesConnected = endNodes.filter(function (node) {

            var nodeId = node.id;

            //Default link check
            var nodeHasDefaultLinkFrom = rule.links.filter(function (connection) {
                return (connection[nodeId] != undefined && connection[nodeId].length > 0);
            })[0] != null;


            //Yes link check
            var nodeHasYesLink = rule.trueLinks.filter(function (connection) {
                return (connection[nodeId] != undefined && connection[nodeId].length > 0);
            })[0] != null;


            //Yes link check
            var nodeHasNoLink = rule.falseLinks.filter(function (connection) {
                return (connection[nodeId] != undefined && connection[nodeId].length > 0);
            })[0] != null;

            return (nodeHasDefaultLinkFrom == true || (nodeHasYesLink == true || nodeHasNoLink == true));

        }).length == endNodes.length;


        var hasOnlyOneStartNode = rule.nodes.filter(function (node) { return node.type.toLowerCase() == "start" }) == 1;

        return allNodesAreConnected && allEndNodesConnected && hasOnlyOneStartNode;
    }

    function removeShapeConnections(shape) {
        var connections = shape.connectors.filter(function (ctr) { return ctr.connections.length > 0 })[0];
        if (connections != undefined) {
            var _connections = connections.connections;
            _connections.forEach(function (_connection, index) {
                index++;
                setTimeout(function () { diagram.remove(_connection); }, 30 * index);
            });
        }
    }

    function generateTooltip(name, desc) {


        var tableHtml = "<table class='nodeTooltip'>" +
        "<tr>" +
            "<td>Name</td>" +
            "<td>{0}</td>" +
        "</tr>" +
        "<tr>" +
            "<td>Description</td>" +
            "<td >{1}</td>" +
        "</tr>" +
    "</table>";

      
        return String.format(tableHtml, name, desc == null ? "" : desc);
    }

    function generateDecisionTooltip(data) {

        var tableHtml = "<table class='nodeTooltip'>" +
        "<tr>" +
            "<td>Expression</td>" +
            "<td >{0}</td>" +
        "</tr>" +
    "</table>";

        return String.format(tableHtml, data.expression == null ? "" : data.expression);
    }

    function extendTreeViewFolderForPopup(folderId, source) {
        var extendButton = $("a[data-expand-button='rule'][data-expand=" + folderId + "]:last");

        extendButton.parents('li:first').find('ul:first').toggle();
        extendButton.parents('li:first').find('span').toggle();
        setTimeout(function () { toogleMenuTreViewScroll() }, 200);
    }


    function toogleMenuTreViewScroll() {
        $("#scrollable_treeview").removeClass("scrolly").removeClass("scrollx");

        if ($("#scrollable_treeview").height() > 340) {
            $("#scrollable_treeview").addClass("scrolly");
        } else {
            $("#scrollable_treeview").removeClass("scrolly");
        }

        if ($("#scrollable_treeview").width() > 194) {
            $("#scrollable_treeview").addClass("scrollx");
        } else {
            $("#scrollable_treeview").removeClass("scrollx");
        }
    }



    function requestFullScreen() {


        var element = $("#fullScreenBox")[0];
        if (element.requestFullscreen) {
            element.requestFullscreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
        } else if (element.msRequestFullscreen) {
            element.msRequestFullscreen();
        }

        $("#diagram_container").width(screen.width);
        $("#diagram_container").height(screen.height);
        $("#diagram").height(screen.height);

        setTimeout(function () { diagram.bringIntoView(diagram.shapes); }, 100);
    }

    function fsEnd() {

        var fsElement = document.fullscreenElement ||
	                    document.webkitFullscreenElement ||
	                    document.mozFullScreenElement ||
	                    document.msFullscreenElement;

        if (fsElement == undefined) {
            $("#diagram_container").css("width", "auto");
            $("#diagram_container").css("height", "auto");
            $("#diagram").css("height", "");
            setTimeout(function () { diagram.bringIntoView(diagram.shapes); }, 100);
        }
    }

    document.addEventListener("fullscreenchange", fsEnd)
    document.addEventListener("webkitfullscreenchange", fsEnd);
    document.addEventListener("mozfullscreenchange", fsEnd);
    document.addEventListener("MSFullscreenChange", fsEnd);

    //#endregion

    $('[data-toggle="tooltip"]').tooltip();

    //Disable copy paste
    $(document).unbind("keydown").unbind("keypress");
    $("#diagram,circle,text,svg,rect,path,g").unbind("keydown").unbind("keypress");
    $(document).bind("keydown", function (event) {
     
        if ($rootScope.activePage != "rule" && $scope.currentStep != 2)
            return;


        if (event.target.nodeName == "INPUT" || event.target.nodeName == "TEXTAREA")
            return;

        if (event.keyCode == 8 || event.keyCode == 46) {
            var selectedShape = diagram.select()[0];
            if (selectedShape != undefined && selectedShape != null) {
                showDeletePopup();
                $("div.k-animation-container").hide();
                event.preventDefault();
            }
        }

      
    });



});




app.controller('notificationsController', function ($rootScope, $scope, $routeParams, $route, $location, dataService, $interval, $filter, $timeout) {


    if (sessionStorage.getItem("isLoggedin") == "false" || sessionStorage.getItem("isLoggedin") == undefined) {
        $location.path("/login");
        return;
    }

    $rootScope.activePage = "notifications";
    document.title = getFormattedPageTitle("Notifications");

    var lastViewedPage = 0;
    var size = 20;
    $scope.scrollDisabled = false;
    $scope.loading = true;
    var lastScrollPosition = { x: 0, y: 0 };

    $scope.notifications = [];

    $rootScope.selectedItem = undefined;

    dataService.getNotifications(lastViewedPage, size, function (data) {

        $rootScope.lastNewNotificationsCount = 0;

        data = formatNotificationDates(data);
        $scope.notifications = data;

        $timeout(function () {
            dataService.countCotifications(function (count) {
                setNotificationCount(count);
            }, function () { });

        }, 500);

        $scope.loading = false;

    }, function (error) { $rootScope.showNotifyError(error); });



    $scope.loadMoreNotification = function () {

        if ($scope.scrollDisabled == true)
            return;

        $scope.scrollDisabled = true;

        lastViewedPage++;
        dataService.getNotifications(lastViewedPage, size, function (data) {
            data = formatNotificationDates(data);
            data.forEach(function (n) { $scope.notifications.push(n); });
            $scope.scrollDisabled = data.length == 0;
        }, function (error) { $rootScope.showNotifyError( error); });
    }

    if ($rootScope.notificationService != null) {
        $interval.cancel($rootScope.notificationService);
    }


    $rootScope.notificationService = $interval(function () {

        if ($rootScope.activePage != "notifications")
            return;

        dataService.getNotifications(0, size, function (data) {
            var newNotifications = [];
            if (data.length > 0) {
                data.forEach(function (nn) {
                    var alreadyExists = $scope.notifications.filter(function (n) { return n.id == nn.id })[0] != undefined;
                    if (alreadyExists == false) {
                        newNotifications.push(nn);
                    }
                });
            }

            newNotifications = formatNotificationDates(newNotifications);

            newNotifications.forEach(function (nn, index) {
                $scope.notifications.splice(index, 0, nn);
            });


            if (newNotifications.length > 0 && lastScrollPosition.x > 0 && astScrollPosition.y > 0) {
                setTimeout(function () { window.scrollTo(lastScrollPosition.x, (lastScrollPosition.y + (51 * newNotifications.length))); }, 10);
            }

        }, function () { });

    }, 1000 * 2);



    if ($rootScope.notificationTimeformatService != null) {
        $interval.cancel($rootScope.notificationTimeformatService);
    }


    $rootScope.notificationTimeformatService = $interval(function () {

        if ($rootScope.activePage != "notifications")
            return;

        $scope.notifications = formatNotificationDates($scope.notifications);
    }, 1000 * 60);



    function formatNotificationDates(notifications) {
        notifications.forEach(function (n) {
            n.notificationDateFormatted = $filter("timeago")(n.notificationDate, false);
        });

        return notifications;
    }

    $scope.clearAllNotifications = function () {
        dataService.deleteNotifications(function () {
            $scope.notifications = [];
            setNotificationCount(0);
        }, function () { });
    }


    function setNotificationCount(count) {
        var notificationMenu = $rootScope.menuItems.filter(function (m) { return m.id == 3 })[0];
        if (notificationMenu)
            notificationMenu.count = count;
    }


    setTimeout(function () {

        $(window).on('scroll', function () {

            if ($rootScope.activePage == "notifications") {
                var doc = document.documentElement;
                var left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
                var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);

                lastScrollPosition = { x: left, y: top };

                //infinite Scroll
                if ($(window).scrollTop() > ($(document).height() - $(window).height() - 50)) {
                    $scope.loadMoreNotification();
                }
            }

        }).scroll();

    }, 500);

});


app.controller('monitoringController', function ($rootScope, $scope, $routeParams, $route, $location, dataService, $interval, $filter, $timeout) {

    if (sessionStorage.getItem("isLoggedin") == "false" || sessionStorage.getItem("isLoggedin") == undefined) {
        $location.path("/login");
        return;
    }
    document.title = getFormattedPageTitle("Monitoring");

    var getYesterdayDate = function () {
        var d = new Date();
        d.setDate(d.getDate() - 1);
        d.setHours(23, 59, 0, 0);
        return d;
    }
    var getTomorrowDate = function () {
        var d = new Date();
        d.setDate(d.getDate() + 1);
        d.setHours(0, 0, 0, 0);
        return d;
    }

    var formatDate = function (dateToFormat) {

        var selectedMonth = dateToFormat.getMonth() + 1;
        var selectedDay = dateToFormat.getDate();

        var formattedDate = dateToFormat.getFullYear().toString() + (selectedMonth < 10 ? ("0" + selectedMonth) : selectedMonth.toString()) + (selectedDay < 10 ? ("0" + selectedDay) : selectedDay.toString());
        return formattedDate;
    }



    $rootScope.selectedItem = undefined;
    $rootScope.activePage = "monitoring";
    $scope.activeTab = "today";

    var pageSize = 20;

    $scope.scrollDisabled = false;
    $scope.loading = false;
    var lastScrollPosition = { x: 0, y: 0 };

    $scope.operations = [];
    $scope.schedules = [];
    $scope.histories = [];


    $scope.filterScheduleFromDate = getTomorrowDate();
    $scope.filterScheduleToDate = getTomorrowDate();

    $scope.filterHistoryFromDate = getYesterdayDate();
    $scope.filterHistoryToDate = getYesterdayDate();


    var scheduleFromDate = formatDate(getTomorrowDate());
    var scheduleToDate   = formatDate(getTomorrowDate());

    var historyFromDate = formatDate(getYesterdayDate());
    var historyToDate = formatDate(getYesterdayDate());

    var lastViewedPage = 0;

    $scope.showFilterPopup = function () {

        if ($scope.activeTab == "schedule" && $scope.filterScheduleFromDate != null && $scope.filterScheduleToDate != null) {
            $("#fromDatePicker").data("DateTimePicker").date($scope.filterScheduleFromDate);
            $("#toDatePicker").data("DateTimePicker").date($scope.filterScheduleToDate);

            $("#toDatePicker").data("DateTimePicker").minDate($scope.filterScheduleFromDate);
            fromDate = formatDate($scope.filterScheduleFromDate);
            toDate = formatDate($scope.filterScheduleToDate);
        }

        if ($scope.activeTab == "history" && $scope.filterHistoryFromDate != null && $scope.filterHistoryToDate != null) {
            $("#fromDatePicker").data("DateTimePicker").date($scope.filterHistoryFromDate);
            $("#toDatePicker").data("DateTimePicker").date($scope.filterHistoryToDate);

            fromDate = formatDate($scope.filterHistoryFromDate);
            toDate = formatDate($scope.filterHistoryToDate);
        }


        if ($("#fromDatePicker").val().length > 0 && $("#toDatePicker").val().length > 0) {
            $("#applyFilterButton").removeAttr("disabled");
        } else {
            $("#applyFilterButton").attr("disabled", "disabled");
        }

        //show popup
        $("#applyFilterPopup").modal("show");
    }


    //Kill today timer on route changed.
    var todayTimerService = $rootScope.$on('$locationChangeSuccess', function () {
        $interval.cancel(todayDataTimer);
        todayTimerService();
    });


    //Update today operations on every 10 seconds
    var todayDataTimer = null;
    var enableTodayDataTimer = true;
    var startTodayDataTimer = function () {

        if (todayDataTimer != null)
            $interval.cancel(todayDataTimer);
         
        todayDataTimer = $interval(function (e) {

            if (enableTodayDataTimer == false ||  $rootScope.activePage != "monitoring" || ( $rootScope.activePage == "monitoring" && $scope.activeTab != "today")) {
                return;
            } else {

                dataService.getMonitoringToday(function (data) {
                    $scope.operations = data;
                    $scope.loading = false;
                }, function () { });
            }
             
        }, 1000 * 10);
    }



    $scope.loading = true;
    dataService.getMonitoringToday(function (data) {
        $scope.operations = data;
        $scope.loading = false;
    }, function (error) { $rootScope.showNotifyError( error); });


    startTodayDataTimer();


    $scope.loadMoreSchedule = function () {

        if ($scope.scrollDisabled == true)
            return;

        $scope.scrollDisabled = true;
        lastViewedPage++;

        dataService.getMonitoringSchedule(scheduleFromDate, scheduleToDate, lastViewedPage, pageSize, function (data) {
            $scope.schedules = $scope.schedules.concat(data);
            $scope.scrollDisabled = data.length == 0;
        }, function (error) { $rootScope.showNotifyError( error); });
    }

    $scope.loadMoreHistory = function () {

        if ($scope.scrollDisabled == true)
            return;

        $scope.scrollDisabled = true;
        lastViewedPage++;

        dataService.getMonitoringHistory(historyFromDate, historyToDate, lastViewedPage, pageSize, function (data) {
            $scope.histories = $scope.histories.concat(data);
            $scope.scrollDisabled = data.length == 0;
        }, function (error) { $rootScope.showNotifyError(error); });
    }

    var getMinDateForSchedule = function () {
        return getTomorrowDate();
    }

    var getMaxDateForHistory = function () {
        return getYesterdayDate();
    }

  

    var fullTextSearch = function (searchTerm, item, filterType) {

        if (searchTerm == "")
            return true;

        var formattedNextRunDate = item.nextRunDate != null ? moment(item.nextRunDate).format("MMMM DD, YYYY HH:MM") : "";
        var formattedFirstRunDate = item.schedule.firstRunDate != null ? moment(item.nextRunDate).format("MMMM DD, YYYY HH:MM") : "";
        var formattedStartDate = item.startDate != null ? moment(item.startDate).format("MMMM DD, YYYY HH:MM") : "";
        var formattedEndDate = item.endDate != null ? moment(item.endDate).format("MMMM DD, YYYY HH:MM") : "";

        var searchQuery = "";

        if (filterType == "schedule") {
            searchQuery = item.modelName.toLowerCase() + item.status.toLowerCase() + formattedNextRunDate.toLowerCase() + formattedFirstRunDate.toLowerCase() + item.schedule.interval.toString() + (item.schedule.period != null ? item.schedule.period.toString().toLowerCase() : "");


        } else {
            searchQuery = item.modelName.toLowerCase() + item.status.toLowerCase() + (item.summary != null ? item.summary.toString().toLowerCase() : "") + formattedStartDate.toLowerCase() + formattedEndDate.toLowerCase();
        }

        if (!searchTerm || (searchQuery.toLowerCase().indexOf(searchTerm) != -1)) {
            return true;
        }

        return false;
    }

    $scope.scheduleFullTextSearch = function (item) {
        var searchTerm = $scope.scheduleSearchTerm == undefined ? "" : $scope.scheduleSearchTerm;
        return fullTextSearch(searchTerm.toLowerCase(), item, "schedule");
    }

    $scope.todayFullTextSearch = function (item) {
        var searchTerm = $scope.todaySearchTerm == undefined ? "" : $scope.todaySearchTerm;
        return fullTextSearch(searchTerm.toLowerCase(), item, "today");
    }

    $scope.historyFullTextSearch = function (item) {
        var searchTerm = $scope.historySearchTerm == undefined ? "" : $scope.historySearchTerm;

        return fullTextSearch(searchTerm.toLowerCase(), item, "history");
    }

    $scope.clearFilterResult = function (tab) {
        if (tab == "schedule") {
            $scope.scheduleSearchTerm = "";
        } else if (tab == "today") {
            $scope.todaySearchTerm = "";
        } else {
            $scope.historySearchTerm = "";
        }
    }


    $scope.changeTab = function (tab) {


        enableTodayDataTimer = false;

        if (todayDataTimer != null) {
            $interval.cancel(todayDataTimer);
            todayDataTimer = null;
        }
         

        lastViewedPage = 0;
        $scope.scrollDisabled = false;

        $scope.activeTab = tab;



        if (tab == "schedule" || tab == "history") {
            $("#fromDatePicker").data("DateTimePicker").minDate(false);
            $("#toDatePicker").data("DateTimePicker").minDate(false);
            $("#fromDatePicker").data("DateTimePicker").maxDate(false);
            $("#toDatePicker").data("DateTimePicker").maxDate(false);
        }


        if (tab == "schedule") {

            $("#fromDatePicker").data("DateTimePicker").minDate(getMinDateForSchedule());
            $("#toDatePicker").data("DateTimePicker").minDate(getMinDateForSchedule());

            if (scheduleFromDate == null && scheduleToDate == null) {
                $("#fromDatePicker").data("DateTimePicker").clear();
                $("#toDatePicker").data("DateTimePicker").clear();

                $("#applyFilterPopup").modal("show");
            } else {

                dataService.getMonitoringSchedule(scheduleFromDate, scheduleToDate, lastViewedPage, pageSize, function (data) {
                    $scope.schedules = data;
                    $scope.loading = false;
                }, function (error) { $rootScope.showNotifyError(error); });
            }

        } else if (tab == "history") {

            $("#fromDatePicker").data("DateTimePicker").maxDate(getMaxDateForHistory());
            $("#toDatePicker").data("DateTimePicker").maxDate(getMaxDateForHistory());

            if (historyFromDate == null && historyToDate == null) {
                $("#fromDatePicker").data("DateTimePicker").clear();
                $("#toDatePicker").data("DateTimePicker").clear();

                $("#applyFilterPopup").modal("show");
            } else {
                dataService.getMonitoringHistory(historyFromDate, historyToDate, lastViewedPage, pageSize, function (data) {
                    $scope.histories = data;
                    $scope.loading = false;
                }, function (error) { $rootScope.showNotifyError( error); });
            }

        } else { //today

            $scope.loading = true;
        
            dataService.getMonitoringToday(function (data) {
                $scope.operations = data;
                $scope.loading = false;
            }, function (error) { $rootScope.showNotifyError(error); });

            enableTodayDataTimer = true;
            startTodayDataTimer();

        }

    }

    $scope.applyFilter = function () {

        $scope.loading = true;

        if ($scope.activeTab == "history") {
            historyFromDate = fromDate;
            historyToDate = toDate;

            $scope.filterHistoryFromDate = getDateFromFormattedDate(fromDate);
            $scope.filterHistoryToDate = getDateFromFormattedDate(toDate);

            dataService.getMonitoringHistory(historyFromDate, historyToDate, 0, pageSize, function (data) {
                $scope.histories = data;
                $scope.loading = false;
            }, function (error) { $rootScope.showNotifyError( error); });

        } else {

            scheduleFromDate = fromDate;
            scheduleToDate = toDate;

            $scope.filterScheduleFromDate = getDateFromFormattedDate(fromDate);
            $scope.filterScheduleToDate = getDateFromFormattedDate(toDate);

            dataService.getMonitoringSchedule(scheduleFromDate, scheduleToDate, 0, pageSize, function (data) {
                $scope.schedules = data;
                $scope.loading = false;
            }, function (error) { $rootScope.showNotifyError( error); });
        }

        $("#applyFilterPopup").modal("hide");

    }

  

    var getDateFromFormattedDate = function (formattedDate) {

        if (formattedDate == undefined) {
            return null;
        }

        var year = parseInt(formattedDate.substring(0, 4));
        var month = parseInt(formattedDate.substring(4, 6)) - 1;
        var day = parseInt(formattedDate.substring(6, 8));

        return new Date(year, month, day);
    }

    var fromDate = undefined;
    var toDate = undefined;

    $("#applyFilterPopup").on('hidden.bs.modal', function () {
        $("#fromDatePicker,#toDatePicker").data("DateTimePicker").clear();
        $("#fromDatePicker,#toDatePicker").val("");

        $("#toDatePicker").data("DateTimePicker").minDate(false);
        fromDate = undefined;
        toDate = undefined;
    })

    $("#fromDatePicker,#toDatePicker").datetimepicker({ useCurrent: false, viewMode: "days", format: "LL" });
    $("#fromDatePicker,#toDatePicker").on("dp.change", function (date) {

        var selectedDate = new Date(date.date);
        var controlId = $(date.currentTarget).attr("id");

        var formattedDate = formatDate(selectedDate);
        if (controlId == "fromDatePicker") {
            fromDate = formattedDate;

            if (date.oldDate != null) {
                var minDate = selectedDate;
                minDate.setHours(0, 0, 0);
                $("#toDatePicker").data("DateTimePicker").minDate(minDate);
            }

        } else {

            if (date.oldDate != null) {
  
                toDate = formattedDate;
                var maxDate = selectedDate;
                maxDate.setHours(0, 0, 0);
                $("#fromDatePicker").data("DateTimePicker").maxDate(maxDate);
            }
        }


        if ($("#fromDatePicker").val().length > 0 && $("#toDatePicker").val().length > 0) {
            $("#applyFilterButton").removeAttr("disabled");
        } else {
            $("#applyFilterButton").attr("disabled", "disabled");
        }

    });

    $("#fromDatePicker").click(function () { $("#fromDatePicker").data("DateTimePicker").show(); });
    $("#toDatePicker").click(function () { $("#toDatePicker").data("DateTimePicker").show(); });
    $("#fromDatePicker,#toDatePicker").keydown(function (event) {
        event.preventDefault();
    });

     $scope.changeDetailTab = function (tab) {
         $scope.activeDetailTab = tab;
     }

     $("#detailPopup,#liveRuleMonitoringPopup").on('hidden.bs.modal', function () {
         //Start the today data timer
         enableTodayDataTimer = true;
         $rootScope.activePage = "monitoring";

         //Destroy diagram for live monitoring popup
         if (diagram != null)
             diagram.destroy();

         clearExecutingAnimationIntervals();
     });
     
     $("#areYouSureCommandPopup,#rescheduleCommandPopup").on('hidden.bs.modal', function () {

         enableTodayDataTimer = true;
         $rootScope.activePage = "monitoring";

     });
     

    $scope.areYouSureData = undefined;
    $scope.currentAction = null;
    $scope.takingAction = false;
    $scope.takeMonitoringAction = function (obj, action) {


        if ($scope.takingAction == true)
            return;

        if (action == "none") {
            return;
        }

        if (action == "details") {

            //Stop the today data timer
            enableTodayDataTimer = false;

            $scope.activeDetailTab = "results";

            $scope.logDetail = null;

            dataService.getMonitoringDetail(obj.id, function (data) {

                if (data.errorDetail != null) {
                    data.errorDetail = data.errorDetail.replaceAll("\r\n\t", "<br />");
                    data.errorDetail = data.errorDetail.replaceAll("\n", "<br />");
                }
          

                data.queryDetail = hljs.fixMarkup(data.queryDetail);
                $scope.logDetail = data;

                $("#sqlQueryCodeBox").html(data.queryDetail);

                //Format sql
                setTimeout(function () {
                    $('pre code').each(function (i, block) {
                        hljs.highlightBlock(block);
                    });
                }, 500);

                  $("#detailPopup").modal("show");

            }, function () { });

            return;
        }

        $scope.areYouSureData = obj;
        $scope.currentAction = action;

        switch (action) {
            case "restart":
            case "pause":
            case "resume":
            case "start": {

                $scope.takingAction = true;

                dataService.takeMonitoringAction(obj.id, action, function () {
                    
                }, function (error) { $rootScope.showNotifyError(error); });

                $timeout(function () {
                    $scope.takingAction = false;
                    $scope.changeTab("today");
                    window.scrollTo(0, 0);
                }, 1000);

               
                break;
            }
            case "skip":
            case "unschedule":
            case "cancel": {

                //Stop the today data timer
                enableTodayDataTimer = false;

                $("#areYouSureCommandPopup").modal("show");
                break;
            }
            case "reschedule": {

                //Stop the today data timer
                enableTodayDataTimer = false;

                $("#scheduleDateTimePicker").data("DateTimePicker").clear();
                $scope.schedulePostData.period = undefined;
                $scope.schedulePostData.interval = "NONE";
                $scope.schedulePostData.firstRunDate = undefined;
                $scope.repeatEveryData = false;
                $scope.notifyError = undefined;
                $("#rescheduleCommandPopup").modal("show");
                break;
            }
            case "live": {
                initRuleLiveMonitoringPopupAndShow(obj.modelId,obj.id, obj.status);
                break;
            }
        }

    }

    $scope.takeActionAfterBeingSure = function () {

        if ($scope.takingAction == true)
            return;


        $scope.processing = true;


        $scope.takingAction = true;

        dataService.takeMonitoringAction($scope.areYouSureData.id, $scope.currentAction, function () {

            $scope.processing = false;

            $("#areYouSureCommandPopup").modal("hide");

            $timeout(function () {
                if ($scope.currentAction == "skip" || $scope.currentAction == "unschedule") {

                    if ($scope.activeTab == "schedule") {
                        $scope.changeTab("schedule");
                    } else {
                        $scope.changeTab("today");
                    }

                } else {
                    $scope.changeTab("today");
                }

                window.scrollTo(0, 0);
                $scope.takingAction = false;
            }, 1000);

        }, function (error) {
            $("#areYouSureCommandPopup").modal("hide");
            $scope.processing = false;

            $rootScope.showNotifyError(error);
        });

    }


    //#region Reschedule Popup

    $scope.schedulePostData = {};
    $scope.scheduleProcessing = false;
    $scope.saveSchedule = function () {

        $scope.notifyError = undefined;
        $scope.scheduleProcessing = true;

        dataService.monitoringReschedule($scope.areYouSureData.id, $scope.schedulePostData, function () {
            $scope.scheduleProcessing = false;
            $("#rescheduleCommandPopup").modal("hide");

            $scope.takingAction = true;
            $timeout(function () {

                if ($scope.activeTab == "schedule") {
                    $scope.changeTab("schedule");
                } else {
                    $scope.changeTab("today");
                }
                window.scrollTo(0, 0);
                $scope.takingAction = false;
            }, 1000);

        }, function (error) { $scope.scheduleProcessing = false;  $scope.notifyError = error; $timeout(function () { $scope.notifyError = undefined; }, 8 * 1000); });

    }



    $("#rescheduleCommandPopup").on('hidden.bs.modal', function () {
        $("#scheduleDateTimePicker,#scheduleTimePicker").val("");
        $scope.schedulePostData.firstRunDate = undefined;
    })

   
    $("#scheduleDateTimePicker").datetimepicker({
        inline: true,
        sideBySide: true,
        minDate: new Date()
    });

    $("#scheduleDateTimePicker").on("dp.change", function (date) {
        if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
            $scope.$apply(function () { $scope.schedulePostData.firstRunDate = new Date(date.date); });
        }
    });

    //#endregion


    //#region Live Monitoring
    var diagram;
    var lineWidth = 2;
    var defaultColor = "#2e3f50";
    var yesColor = "#46cc25";
    var noColor = "#e5252a";
    var jobColor = "#46cc25";
    var ruleColor = "#f39d38";
    var connectorsDefaults = [
                    { name: "top", width: 1, height: 1, fill: "transparent", stroke: { color: "transparent" } },
                    { name: "right", width: 1, height: 1, fill: "transparent", stroke: { color: "transparent" } },
                    { name: "bottom", width: 1, height: 1, fill: "transparent", stroke: { color: "transparent" } },
                    { name: "left", width: 1, height: 1, fill: "transparent", stroke: { color: "transparent" } },
                    { name: "auto", width: 20, height: 20, fill: "transparent", stroke: { color: "#999" }, hover: { fill: "#ccc", stroke: "#ededed" } }
    ];

   
    $scope.zoomToDiagram = function (zoomValue) {
        zoom(zoomValue);
    }

    $scope.resetViewport = function () {
        centerDiagram();
    }

    var browser = getBrowser();

    function initRuleDiagram() {
        $("#liveMonitoringDiagram").kendoDiagram({

            pannable: { key: "none" },
            layout: { subtype: "right", type: "tree", horizontalSeparation: 1, grid: { componentSpacingX: 1, componentSpacingY: 1, offsetX: 1, offsetY: 1 } },

            connectionDefaults: {
                selectable:false,
                stroke: { color: defaultColor, width: lineWidth }, type: "cascading", startCap: "", endCap: { type: "ArrowEnd", fill: defaultColor }, editable: { resize: false, drag : false, remove:false, select:false, connect:false}
            },
            editable: false,
            selectable: false
        });

        diagram = $("#liveMonitoringDiagram").getKendoDiagram();
    }

    function drawRuleDiagram(dataModel) {

        //generate nodes
        dataModel.nodes.forEach(function (shape) {

            var editableOptions = { connect: false, drag:false, remove:false , selectable:false};

            var dataItem = { id: shape.id, x: shape.x, y: shape.y, type: shape.type };

            if (shape.type.toLocaleLowerCase() == "job") {
                dataItem.jobId = shape.jobId;
                dataItem.name = shape.name;
                dataItem.description = shape.description;
            } else if (shape.type.toLocaleLowerCase() == "rule") {
                dataItem.ruleId = shape.ruleId;
                dataItem.name = shape.name;
                dataItem.description = shape.description;
            } else if (shape.type.toLocaleLowerCase() == "decision") {
                dataItem.expression = shape.expression;
            }
            else if (shape.type.toLocaleLowerCase() == "comment") {
                dataItem.comment = shape.comment;
                editableOptions.connect = false;
            } else if (shape.type.toLocaleLowerCase() == "end") {
                editableOptions.connect = false;
            }

            var newCreatedShape = new kendo.dataviz.diagram.Shape({
                editable: editableOptions,
                dataItem: dataItem,
                x: shape.x, y: shape.y, visual: drawRuleShape,

                connectors: connectorsDefaults
            });

            diagram.addShape(newCreatedShape);

        });

        //Generate default links
        drawConnections(dataModel.links, "default");
        //Generate true links
        drawConnections(dataModel.trueLinks, "yes");
        //Generate false links
        drawConnections(dataModel.falseLinks, "no");

    }

    function drawConnections(sourceLinksObject, linkType) {

        //Convert object map to array
        var sourceLinks = [];
        var props = Object.keys(sourceLinksObject);
        props.forEach(function (prop) {
            var cnn = new Object();
            cnn[prop] = sourceLinksObject[prop];

            sourceLinks.push(cnn);
        });

        sourceLinks.forEach(function (connection) {

            var shapeId = Object.keys(connection)[0];
            var connectToShapes = connection[shapeId.toString()];
            var fromShape = getShapeByDataItem("id", shapeId.toString());

            connectToShapes.forEach(function (connectShapeId) {
                var connectionOptions = { width: lineWidth };

                if (linkType == "default") {

                } else if (linkType == "yes") {
                    connectionOptions = { stroke: { color: yesColor, width: lineWidth }, endCap: { type: "ArrowEnd", fill: yesColor }, content: { text: "YES", color: yesColor, fontFamily: "Lato", fontWeight: "700", fontSize: 12 }, dataItem: { decisionType: true } };
                } else {
                    connectionOptions = { stroke: { color: noColor, width: lineWidth }, endCap: { type: "ArrowEnd", fill: noColor }, content: { text: "NO", color: noColor, fontFamily: "Lato", fontWeight: "700", fontSize: 12 }, dataItem: { decisionType: false }};
                }

                var connectShape = getShapeByDataItem("id", connectShapeId.toString());
                diagram.connect(fromShape, connectShape, connectionOptions);



            });

        });
    }

    function getShapeByDataItem(property, value) {
        return diagram.shapes.filter(function (shape) {
            return (shape.dataItem[property] == value);
        })[0];
    }

    function drawRuleShape(options) {

        //Draw rule diagram shapes based on its dataItem
        var dataviz = kendo.dataviz;
        var g = new dataviz.diagram.Group();
        var dataItem = options.dataItem;

        g.x = options.dataItem.x;
        g.y = options.dataItem.y;

        if (dataItem.type.toLowerCase() == "start") {

            g.append(new dataviz.diagram.Circle({ width: 42, stroke: { width: lineWidth, color: defaultColor }, fill: "#fff" }));

            var textBlockY = 2;
            if (browser.name == "Firefox" || browser.name == "Opera" || browser.name == "Safari") {
                var textBlockY = 6;
            }

            g.append(new dataviz.diagram.TextBlock({ x: 12, y: textBlockY, text: "S", fontSize: 26, fill: defaultColor, fontWeight: "800" }));

        } else if (dataItem.type.toLowerCase() == "end") {

            g.append(new dataviz.diagram.Circle({ width: 39, stroke: { width: 5, color: defaultColor }, fill: "#fff" }));

            var textBlockY = 0;
            if (browser.name == "Firefox" || browser.name == "Opera" || browser.name == "Safari") {
                var textBlockY = 4;
            }

            g.append(new dataviz.diagram.TextBlock({ x: 9, y: textBlockY, text: "E", fontSize: 26, fill: defaultColor, fontWeight: "800" }));

        }
        else if (dataItem.type.toLowerCase() == "comment") {

            if (dataItem.comment == undefined || dataItem.comment == null) {
                dataItem.comment = "";
            }

            //calculate rect height based on text char count
            var rectHeight = 45;
            for (var i = 0; i < Math.round(dataItem.comment.trim().length / 22) ; i++) {
                var text = dataItem.comment.substr(i * 22, 22);
                if (text.length > 0) {
                    if (i > 0)
                        rectHeight += (i * 2) + 8;
                }
            }

            var rect = new dataviz.diagram.Rectangle({ width: 150, height: rectHeight, fill: "#f4f2e4", stroke: { width: 0.5, color: "#bcad8d", dashType: "dot" } });
            g.append(rect);

            for (var i = 0; i < dataItem.comment.trim().length / 22 ; i++) {
                var text = dataItem.comment.substr(i * 22, 22);
                if (text.length > 0) {
                    g.append(new dataviz.diagram.TextBlock({ x: 8, y: (i * 15) + 8, fontFamily: "Lato", text: text, fontSize: 11, fill: "#666666" }));

                }
            }

        }
        else if (dataItem.type.toLowerCase() == "decision") {

            g.append(new dataviz.diagram.Rectangle({ width: 57.98, height: 57.98, stroke: { width: lineWidth, color: defaultColor }, fill: "#fff" }));
            g.append(new dataviz.diagram.TextBlock({ x: 15, y: 2, text: "+", fontSize: 48, fill: defaultColor }));

            g.rotate(45, { x: 18, y: 51 });
        }
        else if (dataItem.type.toLowerCase() == "job") {
            g.append(new dataviz.diagram.Rectangle({ width: 142, height: 82, stroke: { width: lineWidth, color: defaultColor }, fill: "#fff" }));

            g.append(new dataviz.diagram.Rectangle({ width: 142, height: 20, fill: jobColor, stroke: { width: lineWidth, color: defaultColor } }));

            g.append(new dataviz.diagram.TextBlock({ x: 56, y: 3, text: "JOB", fontSize: 12, lineHeight: 12, fill: "#fff", fontFamily: "Lato", fontWeight: "700" }));

            if (dataItem.name.length > 16)
                dataItem.name = dataItem.name.substr(0, 16).trim() + "...";

            var textBlockXPosition = 70 - (dataItem.name.length) * 3;

            g.append(new dataviz.diagram.TextBlock({ x: textBlockXPosition, y: 40, text: dataItem.name, fontSize: 12, fill: defaultColor, fontWeight: "700", fontFamily: "Lato" }));

        } else {

            g.append(new dataviz.diagram.Rectangle({ width: 142, height: 82, stroke: { width: lineWidth, color: defaultColor }, fill: "#fff" }));

            g.append(new dataviz.diagram.Rectangle({ width: 142, height: 20, fill: ruleColor, stroke: { width: lineWidth, color: defaultColor } }));

            g.append(new dataviz.diagram.TextBlock({ x: 52, y: 3, text: "RULE", fontSize: 12, lineHeight: 12, fill: "#fff", fontFamily: "Lato", fontWeight: "700" }));


            if (dataItem.name.length > 16)
                dataItem.name = dataItem.name.substr(0, 16).trim() + "...";

            var textBlockXPosition = 70 - (dataItem.name.length) * 3;

            g.append(new dataviz.diagram.TextBlock({ x: textBlockXPosition, y: 40, text: dataItem.name, fontSize: 12, fill: defaultColor, fontWeight: "700", fontFamily: "Lato" }));

        }

        return g;
    }

    function centerDiagram() {
        //Center the diagram
        diagram.zoom(1, { x: 0, y: 0 });
        diagram.pan(new kendo.dataviz.diagram.Point(0, 0));
        diagram.bringIntoView(diagram.shapes);
    }

    function zoom(zoomValue) {
        //Zoom in/out to diagram
        var currentZoom = diagram.zoom();
        diagram.zoom(currentZoom + zoomValue);
        diagram.bringIntoView(diagram.shapes);
    }


    //Draw rule
    function initDiagramAndDrawRule(rule, operationId, finalStatus, updateRealtime) {

        //init diagram
        initRuleDiagram();

        //Draw Rule 
        drawRuleDiagram(rule);

        setTimeout(function () { centerDiagram(); }, 10);

        getLiveFinalStatesAndDrawShapeNodes(operationId, updateRealtime);
    }

    var currentLiveProcess = null;

    //Get live monitoring states and reDraw nodes of the rule on the diagram until live monitoring popup is closed.
    function getLiveFinalStatesAndDrawShapeNodes(operationId,updateRealtime) {

        $scope.notifyError = undefined;

        if ($rootScope.activePage == "monitoring_live") {
            dataService.getLiveMonitoringStates(operationId, function (states) {

                reDrawRuleShapeNodes(states);

                if (updateRealtime == true) {
                    if (currentLiveProcess != null) {
                        $timeout.cancel(currentLiveProcess);
                    }
                    currentLiveProcess = $timeout(function () { getLiveFinalStatesAndDrawShapeNodes(operationId,true); }, 1000);
                }


            }, function (error) { $scope.notifyError = error; });
        }

    }

    function generateErrorTooltip(errorDetail) {

        var tableHtml = "<table class='nodeTooltip live_monitor'>" +
        "<tr>" +
            "<td>{0}</td>" +
        "</tr>" +
    "</table>";

        return String.format(tableHtml, errorDetail);
    }

    var intervalList = [];
    function reDrawRuleShapeNodes(finalStatus) {

        var states = finalStatus.states;
        var errors = finalStatus.errors;

        for (var nodeId in states) {

            //Clear executing animation intervals
            var existingInterval = intervalList.filter(function (a) { return a.nodeId == nodeId })[0];
            if (existingInterval) {
                clearInterval(existingInterval.interval);
                intervalList = intervalList.filter(function (a) { return a.nodeId != nodeId });
            }


            var state = states[nodeId];
            var error = errors[nodeId];
            var shape = getShapeByDataItem("id", nodeId);
            if (shape) {

                switch (state) {
                    case "Executing": {
                        var interval = setInterval( setExecutingAnimationToShape, 500, nodeId);
                        intervalList.push({ nodeId: nodeId, interval: interval });
                        break;
                    }
                    case "Finished": {

                        var existingInterval = intervalList.filter(function (a) { return a.nodeId == nodeId })[0];
                        if (existingInterval) {
                            clearInterval(existingInterval.interval);
                            intervalList = intervalList.filter(function (a) { return a.nodeId != nodeId });
                        }

                        shape.shapeVisual.children[0].redraw({
                            stroke: {
                                width: lineWidth + 2,
                                color: "#28F828"
                            }
                        });
                        shape.shapeVisual.children[1].redraw({
                            stroke: {
                                color: "#28F828"
                            }
                        });

                        break;
                    }
                    case "Waiting": {
                        shape.shapeVisual.children[0].redraw({
                            stroke: {
                                width: lineWidth + 2,
                                color: "yellow"
                            }
                        });
                        shape.shapeVisual.children[1].redraw({
                            stroke: {
                                color: "yellow"
                            }
                        });
                        break;
                    }
                    case "Error": {

                        shape.shapeVisual.children[0].redraw({
                            stroke: {
                                width: lineWidth + 2,
                                color: "red"
                            }
                        });
                        shape.shapeVisual.children[1].redraw({
                            stroke: {
                                color: "red"
                            }
                        });

                        shape.shapeVisual.children[0].drawingElement.options.tooltip = {
                            content: generateErrorTooltip(error),
                            position: "bottom",
                            showAfter: 200,
                            shared: "false"
                        };

                        shape.shapeVisual.children[1].drawingElement.options.tooltip = {
                            content: generateErrorTooltip(error),
                            position: "bottom",
                            showAfter: 200,
                            shared: "false"
                        };


                        shape.redraw();

                        break;
                    }
                }

               
            }
        }

    }

    function clearExecutingAnimationIntervals() {

        intervalList.forEach(function (i) {
            clearInterval(i.interval);
        });
    }

    function setExecutingAnimationToShape(nodeId) {

        var shape = getShapeByDataItem("id", nodeId);

        if (shape.dataItem.type == "end") {
            if (shape.shapeVisual.children[0].options.stroke.width == 5) {
                shape.shapeVisual.children[0].redraw({
                    stroke: {
                        width: 7,
                        color: "black"
                    }
                });
            } else {
                shape.shapeVisual.children[0].redraw({
                    stroke: {
                        width: 5,
                        color: "black"
                    }
                });
            }
        } else {
            if (shape.shapeVisual.children[0].options.stroke.width == 2) {

                shape.shapeVisual.children[0].redraw({
                    stroke: {
                        width: lineWidth + 4,
                        color: "#28F828"
                    }
                });

                shape.shapeVisual.children[1].redraw({
                    stroke: {
                        width: lineWidth ,
                        color: "#28F828"
                    }
                });

            } else {
                shape.shapeVisual.children[0].redraw({
                    stroke: {
                        width: lineWidth,
                        color: "black"
                    }
                });


                shape.shapeVisual.children[1].redraw({
                    stroke: {
                        width: lineWidth,
                        color: "black"
                    }
                });
            }
        }

    }

    function initRuleLiveMonitoringPopupAndShow(ruleId,operationId, operationStatus) {

        dataService.getMonitoringDetail(operationId, function (data) {

            //Eğer today tabı ise durdurulacak
            enableTodayDataTimer = false;

            //Bunu handle et.
            $rootScope.activePage = "monitoring_live";

            setTimeout(function () { initDiagramAndDrawRule(JSON.parse(data.ruleDetail), operationId, data.finalStatus, operationStatus == "Executing"); }, 300);
  
           $("#liveRuleMonitoringPopup").modal("show");
       
        }, function (error) { $rootScope.showNotifyError(error); });

    }

    //#endregion

    //Handle auto-refresh for every tab
    setTimeout(function () {

        $(window).on('scroll', function () {

            if ($rootScope.activePage == "monitoring") {

                if ($scope.loading == true)
                    return;

                var doc = document.documentElement;
                var left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
                var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);

                lastScrollPosition = { x: left, y: top };

                //infinite Scroll
                if ($(window).scrollTop() > ($(document).height() - $(window).height() - 50)) {

                    if ($scope.activeTab == "schedule") {
                        $scope.loadMoreSchedule();
                    } else if ($scope.activeTab == "history") {
                        $scope.loadMoreHistory();
                    }
                }
            }

        }).scroll();

    }, 500);


    $("div.modal").on('hidden.bs.modal', function () {
        $scope.notifyError = undefined;
    })


});


app.controller('settingsController', function ($rootScope, $scope, $routeParams, $route, $location, dataService) {
    $rootScope.activePage = "settings";

    document.title = getFormattedPageTitle("Settings");

    $scope.activeTab            = "profile";
    $scope.notificationSettings = [];
    $scope.showNesPasswordInfo = false;

    if (sessionStorage.getItem("isLoggedin") == "false" || sessionStorage.getItem("isLoggedin") == undefined) {
        $location.path("/login");
        return;
    }

    dataService.getProfile(null,function (profile) {
        $scope.profile = profile;
    }, function () { });


    $scope.changeTab = function (tab) {
        $scope.activeTab = tab;

        if (tab == "notifications") {
            dataService.listSettings(function (settings) {
                $scope.notificationSettings = settings.groupBy("settingGroup");
      
                $scope.notificationSettings.forEach(function (setting) {
                    setting.values.forEach(function (input) { if (input.type == "Boolean") { input.value = input.value == "true" ? true : false; } });
                });

            }, function (error) { $rootScope.showNotifyError(error); });
        }
    }

    $scope.validateProfileInput = function (profile) {
        if($scope.loading){
            return "disabled";
        }

        if (profile == undefined)
            return "disabled";

        if ((profile.email == "" || profile.email == undefined)) {
            return "disabled";
        }


        if (profile.email != "" && profile.email != undefined && profile.oldPassword != undefined && profile.oldPassword != ''  && ( profile.newPassword == undefined || profile.newPassword == '' || profile.retypedNewPassword == undefined || profile.retypedNewPassword == '')) {
            return "disabled";
        }


        if (profile.email != "" && profile.email != undefined && profile.newPassword != undefined && profile.newPassword != '' && (profile.oldPassword == undefined || profile.oldPassword == '' || profile.retypedNewPassword == undefined || profile.retypedNewPassword == '')) {
            return "disabled";
        }


        if (profile.email != "" && profile.email != undefined && profile.retypedNewPassword != undefined && profile.retypedNewPassword != '' && (profile.oldPassword == undefined || profile.oldPassword == '' || profile.newPassword == undefined || profile.newPassword == '')) {
            return "disabled";
        }
      
        if (profile.email != "" && profile.email != undefined && profile.retypedNewPassword != undefined && profile.retypedNewPassword != '' && profile.newPassword != undefined && profile.newPassword != '' && profile.retypedNewPassword !=  profile.newPassword) {
            return "disabled";
        }

        return "";
    }

    $scope.saveProfile = function (profile) {
        $scope.errorMessage = ""
        $scope.updated = false;
        $scope.buttonClicked = true;
       

        if (profile.email == '' || profile.email == undefined)
            return;

        if (profile.email != "" && profile.email != undefined && profile.oldPassword != undefined && profile.oldPassword != '' && (profile.newPassword == undefined || profile.newPassword == '' || profile.retypedNewPassword == undefined || profile.retypedNewPassword == '')) {
            return;
        }

         if (profile.email != "" && profile.email != undefined && profile.newPassword != undefined && profile.newPassword != '' && (profile.oldPassword == undefined || profile.oldPassword == '' || profile.retypedNewPassword == undefined || profile.retypedNewPassword == '')) {
            return;
        }

         if (profile.email != "" && profile.email != undefined && profile.retypedNewPassword != undefined && profile.retypedNewPassword != '' && (profile.oldPassword == undefined || profile.oldPassword == '' || profile.newPassword == undefined || profile.newPassword == '')) {
             return;
         }

         var updateData = { email: profile.email, oldPassword: profile.oldPassword == undefined ? null : profile.oldPassword, newPassword: profile.newPassword == undefined ? null : profile.newPassword };

        $scope.loading = true;
        dataService.updateProfile(updateData, function () {
            $scope.loading = false;
            $scope.updated = true;
            $scope.buttonClicked = false;

            //If password is changed
            if (updateData.newPassword != null) {
                 $scope.showNesPasswordInfo = true;

                 setTimeout(function () {
                     sessionStorage.removeItem("authKey");
                     sessionStorage.removeItem("username");
                     sessionStorage.setItem("isLoggedin", false);
                     sessionStorage.removeItem("users");
                     sessionStorage.removeItem("user");
                     $location.path("/login");
                 }, 1000 * 2);
            }

       

        }, function (error) {
            $rootScope.showNotifyError(error);
            $scope.loading = false;
        });

    }

    $scope.saveNotificationSettings = function () {
        
        $scope.settingsSaved = false;

        var settingObj = {};
        $scope.notificationSettings.forEach(function (setting) {
            setting.values.forEach(function (input) {
              
                settingObj[input.id.toString()] = input.value.toString();
            });
        });

        $scope.loading = true;
        dataService.saveSettings({ values: settingObj }, function () {
            $scope.loading = false;
            $scope.settingsSaved = true;

        }, function (error) {
            $rootScope.showNotifyError( error);
            $scope.loading = false;
        });

    }

    $scope.resetNotificationSettings = function () {

        $scope.settingsSaved    = false;
        $scope.loading          = true;

        dataService.resetSettings(function () {
            $scope.loading = false;
  
            $scope.changeTab("notifications");
        }, function (error) {
            $rootScope.showNotifyError(error);
            $scope.loading = false;
        });

    }

});



app.controller('helpController', function ($rootScope, $scope, $routeParams, $route, $location, dataService) {
    $rootScope.activePage = "help";
    document.title = getFormattedPageTitle("Help");
});



app.controller('reportsController', function ($rootScope, $scope, $routeParams, $route, $location, dataService, $q) {
    $rootScope.activePage = "reports";
    document.title        = getFormattedPageTitle("Reports");
    $scope.pageHeader = "Reports";

    var loggedInUser = window.getLogginedUser();
    if (loggedInUser && loggedInUser != null) {
        $scope.reports = getReports().filter(function (report) { return report.roles.indexOf(loggedInUser.userType) != -1; });
    }

    $scope.reportNames    = [];
    $scope.disableNexButton = true;
    $scope.scrollDisabled = false;

    $scope.allRoles = [];
    $scope.allUsers = [];
    $scope.loading = false;
    $scope.reportResult = [];

    dataService.getRoleList(function (_roles) {
        $scope.allRoles = _roles;
    });

    dataService.getUserList(function (_users) {
        $scope.allUsers = _users;
    });


    var selectedReportType   = null;
    var selectedReportName   = null;
    $scope.reportFilters        = { };

    clearReportFilter();

    function clearReportFilter() {
        $scope.reportFilters      = {};
        $scope.reportFilters.page = 0;
        $scope.reportFilters.size = 100;
        selectedReportType        = null;
        selectedName              = null;
        rwxValue                  = 0;
        $scope.scrollDisabled = false;
    }

    function reportTypeSelected() {
        $scope.reportFilters.reportType = $scope.reportName;
        selectedReportType              = $scope.reports.filter(function (report) { return report.typeValue == $scope.reportType; })[0];
        var reportTypeText              = selectedReportType.typeText;
        selectedReportName              = selectedReportType.names.filter(function (name) { return name.value == $scope.reportName })[0];
        var reportNameText              = selectedReportName.text;
        $scope.pageHeader               = reportTypeText + " - " + reportNameText;
    }


    function reportFiltersSelected() {

        if ($scope.reportFilters.roleId && $scope.reportFilters.roleId.originalObject) {
            $scope.reportFilters.roleId = $scope.reportFilters.roleId.originalObject.id;
        }

        if ($scope.reportFilters.userId && $scope.reportFilters.userId.originalObject) {
            $scope.reportFilters.userId = $scope.reportFilters.userId.originalObject.id;
        }

        $scope.loading            = true;
        $scope.reportResult       = [];
        $scope.reportFilters.page = 0;
        $scope.scrollDisabled = false;

        dataService.getReport(selectedReportName.endpoint, $scope.reportFilters, function (_reportResult) {
            $scope.reportResult = _reportResult;
            $scope.loading = false;
        }, function (error) { $scope.loading == false; $rootScope.showNotifyError(error); });
    }


    $scope.loadMoreNotification = function () {

        if ($scope.scrollDisabled == true)
            return;

        $scope.scrollDisabled = true;

        $scope.reportFilters.page += 1;
        dataService.getReport(selectedReportName.endpoint, $scope.reportFilters, function (_reportResult) {
            _reportResult.forEach(function (n) { $scope.reportResult.push(n); });
            $scope.scrollDisabled = _reportResult.length == 0;
        }, function (error) { $scope.loading == false; $rootScope.showNotifyError(error); });

    }


    $scope.currentStep = 1;
    $scope.nextStep = function () {
        var cs = $scope.currentStep;
        cs++;
        if (cs > 3) {
            cs = 3;
        }

        $scope.currentStep = cs;

        if (cs == 2) {
            reportTypeSelected();
        } else if (cs == 3) {
            reportFiltersSelected();
        }
    }

    $scope.prevStep = function () {
        var cs = $scope.currentStep;
        cs--;
        if (cs < 1) {
            cs = 1;
        }

        if (cs == 3) {
            cs = 2;
        }

        $scope.currentStep = cs;
    }
    function getReports() {

        return [{

            typeValue: "Authorization",
            typeText: "Authorization Reports",
            roles : ["Admin"],
            names: [
                { text: "Role & User", value: "AuthorizationRoleUser", endpoint: "report_roleuser" },
                { text: "User & Role", value: "AuthorizationUserRole", endpoint: "report_userrole" },
                { text: "Role & Object", value: "AuthorizationRoleObject", endpoint: "report_roleobject" },
                { text: "User & Object", value: "AuthorizationUserObject", endpoint: "report_userobject" }
            ]

        },
        {
            typeValue: "Audit",
            typeText: "Audit Reports",
            names: [],
            roles: ["Admin"],
        },
        {
            typeValue: "Meta",
            typeText: "ICC Meta Reports",
            names: [],
            roles: ["Admin","Normal"],

        },
        {
            typeValue: "Operational",
            typeText: "Operational reports",
            names: [],
            roles: ["Admin", "Normal"],
        }];

    }
    $scope.populateReportNames = function (reportType) {
        var report = $scope.reports.filter(function (_report) { return _report.typeValue == reportType })[0];
        if (report) {
            $scope.reportNames = report.names;
        } else {
            $scope.reportNames = [];
        }
        disableNexButton(true);
        $scope.reportName = "";
    }

    function disableNexButton(disable) {
        $scope.disableNexButton = disable;
    }

    $scope.saveReportName = function (report) {
        $scope.reportName = report;
        clearReportFilter();
        if (report != "") {
            disableNexButton(false);
        } else {
            disableNexButton(true);
        }
    }

    $scope.clearFilters = function () {
        $scope.reportFilters.page = 0;
        $scope.reportFilters.size = 100;
        $scope.reportFilters.size = 100;

        if ($scope.reportFilters.userId) {
            $scope.reportFilters.userId = null;
        }

        if ($scope.reportFilters.roleId) {
            $scope.reportFilters.roleId = null;
        }

        $("#autoCompleteTextBox").val("");

        $scope.reportFilters.rights = 0;
        clearRightsValues();
        $scope.reportFilters.includePassive = false;

    }


    var rwxValue = 0;
    function clearRightsValues() {
        rwxValue = 0;
        $("#rightsTable input[type=checkbox]").prop("checked", false);
    }

    $scope.calculateRWXValue = function (value) {
        $scope.reportFilters.rights = getRWXValue(value);
    }

    function getRWXValue(value) {
        rwxValue = rwxValue + value;
        if (rwxValue < 0)
            return 7 + rwxValue;
        return rwxValue;
    }


    setTimeout(function () {

        $(window).on('scroll', function () {

            if ($rootScope.activePage == "reports" && $scope.reportResult.length > 0) {
                var doc = document.documentElement;
                var left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
                var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);

                //infinite Scroll
                if ($(window).scrollTop() > ($(document).height() - $(window).height() - 50)) {
                    $scope.loadMoreNotification();
                }
            }

        }).scroll();

    }, 500);


});

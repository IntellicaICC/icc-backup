package net.intellica.icc.engine.util;

import net.intellica.icc.template.exception.UnknownErrorException;

public class ThreadUtil {

	public static String getThreadId() {
		return "[Thread=" + new Long(Thread.currentThread().getId()).toString() + "]";
	}

	public static void waitFor(Integer ms) {
		try {
			Thread.sleep(ms);
		}
		catch (InterruptedException e) {
			throw new UnknownErrorException(e);
		}
	}

}

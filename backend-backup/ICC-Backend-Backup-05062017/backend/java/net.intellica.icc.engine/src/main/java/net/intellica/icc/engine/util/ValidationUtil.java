package net.intellica.icc.engine.util;

import net.intellica.icc.engine.ignite.IgniteWrapper;
import net.intellica.icc.template.exception.NotFoundException;
import net.intellica.icc.template.exception.ValidationException;
import net.intellica.icc.template.model.GlobalVariable;
import net.intellica.icc.template.model.InstanceKey;
import net.intellica.icc.template.model.WorkItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

@Component
public class ValidationUtil {

	private static final String INVALID_WORK = "Work is required!";
	private static final String INVALID_PARAMETERS = "Parameters are required!";
	private static final String NO_INSTANCE = "Instance id %s already exists!";
	private static final String INSTANCE_EXISTS = "Instance id %s does not exist!";
	private static final String INVALID_KEY = "Invalid key!";

	private static final Integer CODE_INVALID_WORK = 101;
	private static final Integer CODE_INVALID_PARAMETERS = 102;
	private static final Integer CODE_NO_INSTANCE = 103;
	private static final Integer CODE_INSTANCE_EXISTS = 104;
	private static final Integer CODE_INVALID_KEY = 105;

	@Autowired IgniteWrapper ignite;

	public void validateWork(WorkItem work) throws ValidationException {
		if (work == null)
			throw new ValidationException(CODE_INVALID_WORK, INVALID_WORK);
		if (CollectionUtils.isEmpty(work.getParameters()))
			throw new ValidationException(CODE_INVALID_PARAMETERS, INVALID_PARAMETERS);
		String ruleInstanceId = work.getParameters().get(GlobalVariable.RuleInstanceID.toString());
		String jobInstanceId = work.getParameters().get(GlobalVariable.JobInstanceID.toString());
		InstanceKey key = new InstanceKey(ruleInstanceId, jobInstanceId);
		if (ignite.getWork(key) != null)
			throw new ValidationException(CODE_NO_INSTANCE, NO_INSTANCE);
	}

	public void validateKeyExists(InstanceKey key, Boolean flag) throws ValidationException {
		if (flag && ignite.getWork(key) == null)
			throw new NotFoundException(String.format(INSTANCE_EXISTS, key));
		else if (!flag && ignite.getWork(key) != null)
			throw new ValidationException(CODE_NO_INSTANCE, String.format(NO_INSTANCE, key));
	}

	public void validateKey(InstanceKey key) throws ValidationException {
		if (key == null || (StringUtils.isEmpty(key.getMainInstanceId()) && StringUtils.isEmpty(key.getSubInstanceId())))
			throw new ValidationException(CODE_INVALID_KEY, INVALID_KEY);
	}

}

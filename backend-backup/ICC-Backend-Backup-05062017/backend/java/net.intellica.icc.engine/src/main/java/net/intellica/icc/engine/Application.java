package net.intellica.icc.engine;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import net.intellica.icc.engine.ignite.IgniteWrapper;
import org.apache.ignite.Ignite;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.FileSystemXmlApplicationContext;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext springContext = SpringApplication.run(Application.class, args);
		springContext.registerShutdownHook();
	}

	@Bean
	public IgniteWrapper igniteWrapper() {
		IgniteWrapper result = new IgniteWrapper();
		ConfigurableApplicationContext igniteContext = new FileSystemXmlApplicationContext("/ignite.xml");
		igniteContext.registerShutdownHook();
		Ignite ignite = (Ignite)igniteContext.getBean("ignite");
		result.setIgnite(ignite);
		return result;
	}

	@Bean
	public ScheduledExecutorService executorService() {
		return Executors.newScheduledThreadPool(5);
	}

}

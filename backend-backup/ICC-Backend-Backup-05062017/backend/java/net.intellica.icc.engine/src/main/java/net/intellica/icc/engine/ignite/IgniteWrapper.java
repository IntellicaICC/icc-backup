package net.intellica.icc.engine.ignite;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import net.intellica.icc.engine.config.EngineConfig;
import net.intellica.icc.engine.util.ExceptionHandler;
import net.intellica.icc.engine.util.ThreadUtil;
import net.intellica.icc.template.model.Constants;
import net.intellica.icc.template.model.GlobalVariable;
import net.intellica.icc.template.model.InstanceKey;
import net.intellica.icc.template.model.WorkItem;
import net.intellica.icc.template.model.WorkItemResult;
import net.intellica.icc.template.model.WorkItemStatus;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCompute;
import org.apache.ignite.IgniteException;
import org.apache.ignite.compute.ComputeTaskFuture;
import org.apache.ignite.lang.IgniteFutureCancelledException;
import org.apache.ignite.lang.IgniteFutureTimeoutException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class IgniteWrapper {

	private static final Integer BASE_TIMEOUT = 5000;
	private static final Integer CHECK_DURATION = 1000;

	@Autowired private ExceptionHandler exceptionHandler;
	@Autowired private EngineConfig engineConfig;
	@Autowired private ScheduledExecutorService executorService;

	private Ignite ignite;
	private Map<InstanceKey, ComputeTaskFuture<WorkItemResult>> futures;

	public IgniteWrapper() {
		futures = new ConcurrentHashMap<>();
	}

	public IgniteWrapper(Ignite ignite) {
		this.ignite = ignite;
	}

	public Ignite getIgnite() {
		return ignite;
	}

	public void setIgnite(Ignite ignite) {
		this.ignite = ignite;
	}

	public synchronized WorkItemStatus getStatus(InstanceKey key) {
		WorkItemStatus result = null;
		ComputeTaskFuture<WorkItemResult> future = futures.get(key);
		if (future.isDone()) {
			if (future.isCancelled())
				result = WorkItemStatus.Canceled;
			else {
				try {
					result = future.get(BASE_TIMEOUT).getStatus();
				}
				catch (IgniteException e) {
					exceptionHandler.handleQuietly(e);
					result = WorkItemStatus.Error;
				}
			}
		}
		else
			result = WorkItemStatus.Executing;
		return result;
	}

	public WorkItemResult getResult(InstanceKey key, Boolean wait, Integer timeout) {
		WorkItemResult result = null;
		ComputeTaskFuture<WorkItemResult> future = futures.get(key);
		if (future.isDone()) {
			if (future.isCancelled()) {
				result = generateCancelResult();
			}
			else {
				try {
					result = future.get(BASE_TIMEOUT);
				}
				catch (IgniteException e) {
					result = generateErrorResult(e);
				}
			}
		}
		else {
			if (wait) {
				try {
					if (timeout <= 0) {
						ThreadUtil.waitFor((int)(new Random().nextFloat() * 1000));
						result = future.get(BASE_TIMEOUT);
					}
					else {
						Duration jobTimeout = Duration.ofSeconds(timeout);
						Duration totalSeconds = Duration.ZERO;
						while (totalSeconds.compareTo(jobTimeout) < 0) {
							Instant start = Instant.now();
							if (future.isDone()) {
								result = future.get(BASE_TIMEOUT);
								break;
							}
							ThreadUtil.waitFor(CHECK_DURATION);
							totalSeconds = totalSeconds.plus(Duration.between(start, Instant.now()));
						}
						if (totalSeconds.compareTo(jobTimeout) >= 0)
							throw new IgniteFutureTimeoutException("Job timeout reached!");
					}
				}
				catch (IgniteFutureCancelledException e) {
					exceptionHandler.handleQuietly(e);
					result = generateCancelResult();
				}
				catch (IgniteFutureTimeoutException e) {
					// return null
				}
				catch (IgniteException e) {
					exceptionHandler.handleQuietly(e);
					result = generateErrorResult(e);
				}
			}
			else {
				// return null
			}
		}
		return result;
	}

	private WorkItemResult generateErrorResult(IgniteException e) {
		WorkItemResult result = new WorkItemResult();
		result.setError(e);
		return result;
	}

	private WorkItemResult generateCancelResult() {
		WorkItemResult result = new WorkItemResult();
		result.setCanceled(true);
		return result;
	}

	public synchronized ComputeTaskFuture<WorkItemResult> getWork(InstanceKey key) {
		return futures.get(key);
	}

	public synchronized List<ComputeTaskFuture<WorkItemResult>> getAllSubWork(String mainInstanceId) {
		return futures.keySet().stream()
			.filter(k -> mainInstanceId.equals(k.getMainInstanceId()))
			.map(k -> futures.get(k))
			.collect(Collectors.toList());
	}

	public synchronized void removeWork(InstanceKey key) {
		if (futures.get(key) != null)
			futures.remove(key);
	}

	private synchronized void addExecution(InstanceKey key, ComputeTaskFuture<WorkItemResult> future) {
		futures.put(key, future);
		executorService.schedule(() -> futures.remove(key), 24, TimeUnit.HOURS);
	}

	public void process(WorkItem work) {
		String templateName = work.getParameters().get(GlobalVariable.TemplateName.toString());
		String ruleInstanceId = work.getParameters().get(GlobalVariable.RuleInstanceID.toString());
		String jobInstanceId = work.getParameters().get(GlobalVariable.JobInstanceID.toString());
		putMailParamaters(work);
		InstanceKey key = new InstanceKey(ruleInstanceId, jobInstanceId);
		IgniteCompute compute = ignite.compute().withAsync();
		compute.execute(templateName, work);
		addExecution(key, compute.future());
	}

	public List<String> getCacheNames() {
		return ignite.cacheNames().stream()
			.filter(cn -> !StringUtils.isEmpty(cn))
			.collect(Collectors.toList());
	}

	public void clearCache(String cache) {
		if (!StringUtils.isEmpty(cache) && ignite.cache(cache) != null) {
			try {
				ignite.cache(cache).removeAll();
				ignite.destroyCache(cache);
			}
			catch (Exception e) {
				exceptionHandler.handleQuietly(e);
			}
		}
	}

	private void putMailParamaters(WorkItem work) {
		work.getMailParameters().put(Constants.Email.HOST, engineConfig.getEmail_host());
		work.getMailParameters().put(Constants.Email.PROTOCOL, engineConfig.getEmail_protocol());
		work.getMailParameters().put(Constants.Email.USERNAME, engineConfig.getEmail_username());
		work.getMailParameters().put(Constants.Email.PASSWORD, engineConfig.getEmail_password());
	}

}

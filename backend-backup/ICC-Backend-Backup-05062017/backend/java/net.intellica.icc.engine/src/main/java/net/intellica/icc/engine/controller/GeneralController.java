package net.intellica.icc.engine.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GeneralController {

	private static final String VERSION = "0.2";
	private static final String BUILD = "309";

	@RequestMapping(value = "/version", method = RequestMethod.GET)
	public ResponseEntity<String> version() {
		String version = "ICC Engine - version " + VERSION + " build " + BUILD;
		return new ResponseEntity<>(version, HttpStatus.OK);
	}

}

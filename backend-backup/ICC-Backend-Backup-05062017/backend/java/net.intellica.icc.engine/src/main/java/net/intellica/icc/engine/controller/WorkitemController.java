package net.intellica.icc.engine.controller;

import net.intellica.icc.engine.ignite.IgniteWrapper;
import net.intellica.icc.template.model.InstanceKey;
import net.intellica.icc.template.model.WorkItem;
import net.intellica.icc.template.model.WorkItemResult;
import net.intellica.icc.template.model.WorkItemStatus;
import org.apache.ignite.lang.IgniteFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/workitems")
public class WorkitemController {

	@Autowired private IgniteWrapper ignite;
	@Autowired private net.intellica.icc.engine.util.ExceptionHandler exceptionHandler;

	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public void handle(HttpMessageNotReadableException e) {
		e.printStackTrace();
	}

	@RequestMapping(value = { "/status", "/status/" }, method = RequestMethod.POST)
	public ResponseEntity<WorkItemStatus> getStatus(@RequestBody InstanceKey key) {
		exceptionHandler.debug("Status request for key=" + key);
		return new ResponseEntity<>(ignite.getStatus(key), HttpStatus.OK);
	}

	@RequestMapping(value = { "/result", "/result/" }, method = RequestMethod.POST)
	public ResponseEntity<WorkItemResult> getResult(
		@RequestBody InstanceKey key,
		@RequestParam(value = "wait", required = false, defaultValue = "false") Boolean wait,
		@RequestParam(value = "timeout", required = false, defaultValue = "0") Integer timeout//
	) {
		exceptionHandler.debug("Result request for key=" + key);
		return new ResponseEntity<>(ignite.getResult(key, wait, timeout), HttpStatus.OK);
	}

	@RequestMapping(value = { "/cancel", "/cancel/" }, method = RequestMethod.POST)
	public ResponseEntity<String> cancel(@RequestBody InstanceKey key) {
		ResponseEntity<String> result = new ResponseEntity<>(HttpStatus.OK);
		exceptionHandler.debug("Cancel request for key=" + key);
		// rule
		if (StringUtils.isEmpty(key.getSubInstanceId()))
			ignite.getAllSubWork(key.getMainInstanceId()).forEach(this::cancelFuture);
		// job
		else
			cancelFuture(ignite.getWork(key));
		return result;
	}

	private void cancelFuture(IgniteFuture<?> future) {
		if (!future.isDone()) {
			try {
				future.cancel();
			}
			catch (Exception e) {
				exceptionHandler.handleQuietly(e);
			}
		}
	}

	@RequestMapping(value = { "", "/" }, method = RequestMethod.POST)
	public ResponseEntity<String> run(@RequestBody WorkItem work) {
		ResponseEntity<String> result;
		ignite.process(work);
		result = new ResponseEntity<>(HttpStatus.ACCEPTED);
		return result;
	}

}

package net.intellica.icc.engine.util;

import net.intellica.icc.template.exception.ForbiddenException;
import net.intellica.icc.template.exception.GridException;
import net.intellica.icc.template.exception.NotFoundException;
import net.intellica.icc.template.exception.NotImplementedException;
import net.intellica.icc.template.exception.TechnicalException;
import net.intellica.icc.template.exception.ValidationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ignite.IgniteCheckedException;
import org.apache.ignite.IgniteException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class ExceptionHandler {

	private static final String GRID_ERROR = "Grid error!";
	private static final String TECHNICAL_ERROR = "Technical error!";

	private static final Log LOG = LogFactory.getLog(ExceptionHandler.class);

	public void handle(Throwable t) throws Exception {
		Exception exception = selectException(t);
		LOG.error(exception.getMessage(), exception);
		throw exception;
	}

	public void handleQuietly(Throwable t) {
		Exception exception = selectException(t);
		LOG.error(exception.getMessage(), exception);
	}

	public Object handleResponse(Exception e) {
		Exception exception = selectException(e);
		HttpStatus status = selectStatus(exception);
		LOG.error(exception.getMessage(), exception);
		return new ResponseEntity<>(exception.getMessage(), status);
	}

	public void info(String message) {
		LOG.info(message);
	}

	public void warning(String message) {
		LOG.warn(message);
	}

	public void error(Throwable t) {
		LOG.error(t.getMessage(), t);
	}

	public void debug(String message) {
		LOG.debug(message);
	}

	private Exception selectException(Throwable t) {
		Exception result;
		try {
			throw t;
		}
		catch (IgniteException | IgniteCheckedException e) {
			result = new GridException(GRID_ERROR, t);
		}
		catch (ValidationException e) {
			result = e;
		}
		catch (Throwable e) {
			result = new TechnicalException(TECHNICAL_ERROR, t);
		}
		return result;
	}

	private HttpStatus selectStatus(Exception exception) {
		HttpStatus result = HttpStatus.INTERNAL_SERVER_ERROR;
		if (exception instanceof NotFoundException)
			result = HttpStatus.NOT_FOUND;
		else if (exception instanceof ForbiddenException)
			result = HttpStatus.FORBIDDEN;
		else if (exception instanceof ValidationException)
			result = HttpStatus.BAD_REQUEST;
		else if (exception instanceof NotImplementedException)
			result = HttpStatus.NOT_IMPLEMENTED;
		else if (exception instanceof GridException)
			result = HttpStatus.OK;
		return result;
	}

}

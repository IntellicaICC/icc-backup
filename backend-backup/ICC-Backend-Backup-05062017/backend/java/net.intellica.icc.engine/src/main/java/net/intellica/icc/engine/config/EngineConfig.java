package net.intellica.icc.engine.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "icc.engine")
public class EngineConfig {

	private String email_host;
	private String email_protocol;
	private String email_username;
	private String email_password;

	public String getEmail_host() {
		return email_host;
	}

	public void setEmail_host(String email_host) {
		this.email_host = email_host;
	}

	public String getEmail_protocol() {
		return email_protocol;
	}

	public void setEmail_protocol(String email_protocol) {
		this.email_protocol = email_protocol;
	}

	public String getEmail_username() {
		return email_username;
	}

	public void setEmail_username(String email_username) {
		this.email_username = email_username;
	}

	public String getEmail_password() {
		return email_password;
	}

	public void setEmail_password(String email_password) {
		this.email_password = email_password;
	}

}

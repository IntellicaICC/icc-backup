package net.intellica.icc.engine.aop;

import net.intellica.icc.engine.util.ExceptionHandler;
import net.intellica.icc.engine.util.ValidationUtil;
import net.intellica.icc.template.model.InstanceKey;
import net.intellica.icc.template.model.WorkItem;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Aspect
@Component
public class ControllerAspect {

	@Autowired private ValidationUtil validationUtil;
	@Autowired private ExceptionHandler exceptionHandler;

	@Around("execution(* net.intellica.icc.engine.controller.WorkitemController.get(..))")
	public Object getWorkStatus(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			InstanceKey key = (InstanceKey)pjp.getArgs()[0];
			validationUtil.validateKey(key);
			validationUtil.validateKeyExists(key, true);
			result = pjp.proceed();
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e);
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.engine.controller.WorkitemController.getResult(..))")
	public Object getWorkResult(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			InstanceKey key = (InstanceKey)pjp.getArgs()[0];
			validationUtil.validateKey(key);
			validationUtil.validateKeyExists(key, true);
			result = pjp.proceed();
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e);
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.engine.controller.WorkitemController.cancel(..))")
	public Object cancelWork(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			InstanceKey key = (InstanceKey)pjp.getArgs()[0];
			validationUtil.validateKey(key);
			// rule'un tum islerini cancel ederken bos gelir
			if (!StringUtils.isEmpty(key.getSubInstanceId()))
				validationUtil.validateKeyExists(key, true);
			result = pjp.proceed();
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e);
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.engine.controller.WorkitemController.run(..))")
	public Object postWork(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			WorkItem work = (WorkItem)pjp.getArgs()[0];
			validationUtil.validateWork(work);
			result = pjp.proceed();
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e);
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.engine.controller.CacheController.clear(..))")
	public Object clearCache(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			result = pjp.proceed();
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e);
		}
		return result;
	}

}

package net.intellica.icc.engine.controller;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import net.intellica.icc.engine.ignite.IgniteWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/caches")
public class CacheController {

	@Autowired private IgniteWrapper ignite;
	@Autowired private net.intellica.icc.engine.util.ExceptionHandler exceptionHandler;

	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public void handle(HttpMessageNotReadableException e) {
		e.printStackTrace();
	}

	@RequestMapping(value = { "", "/" }, method = RequestMethod.GET)
	public ResponseEntity<List<String>> list() {
		return new ResponseEntity<>(ignite.getCacheNames(), HttpStatus.OK);
	}

	@RequestMapping(value = { "/clear", "/clear/" }, method = RequestMethod.POST)
	public ResponseEntity<String> clear(@RequestBody String id) {
		ResponseEntity<String> result;
		exceptionHandler.debug("Clear request for prefix=" + id);
		Set<String> caches = ignite.getCacheNames().stream()
			.filter(c -> !StringUtils.isEmpty(c) && c.startsWith(id))
			.collect(Collectors.toSet());
		if (caches.size() > 0) {
			caches.forEach(c -> ignite.clearCache(c));
			result = new ResponseEntity<>(HttpStatus.OK);
		}
		else
			result = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		return result;
	}

}

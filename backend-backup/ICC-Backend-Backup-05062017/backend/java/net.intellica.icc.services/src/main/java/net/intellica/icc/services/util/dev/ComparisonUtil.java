package net.intellica.icc.services.util.dev;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.intellica.icc.template.exception.CompareException;
import org.springframework.stereotype.Component;

@Component
public class ComparisonUtil {

	public static final Integer EQUAL = 0;

	public Integer compare(Object o1, Object o2) {
		Integer result;
		try {
			ObjectMapper mapper = new ObjectMapper();
			String json1 = mapper.writeValueAsString(o1);
			String json2 = mapper.writeValueAsString(o2);
			result = json1.compareTo(json2);
		}
		catch (Exception e) {
			throw new CompareException(e);
		}
		return result;
	}

}

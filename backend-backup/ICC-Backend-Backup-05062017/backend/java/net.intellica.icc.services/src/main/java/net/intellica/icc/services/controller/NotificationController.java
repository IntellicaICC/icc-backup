package net.intellica.icc.services.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import net.intellica.icc.services.model.Notification;
import net.intellica.icc.services.model.Setting;
import net.intellica.icc.services.model.SettingDefinition;
import net.intellica.icc.services.model.SettingGroup;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.service.NotificationService;
import net.intellica.icc.services.service.SettingDefinitionService;
import net.intellica.icc.services.service.UserService;
import net.intellica.icc.services.util.other.EmailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/notification")
public class NotificationController {

	@Autowired private ApplicationContext context;
	@Autowired private NotificationService notificationService;
	@Autowired private UserService userService;
	@Autowired private ProfileController profileController;
	@Autowired private SettingDefinitionService definitionService;
	@Autowired private EmailUtil emailUtil;

	@RequestMapping(value = { "/{id}", "/{id}/" }, method = RequestMethod.GET)
	public Object get(@PathVariable String id) {
		return notificationService.get(id);
	}

	@RequestMapping(value = { "", "/" }, method = RequestMethod.DELETE)
	public Object deleteAll() {
		String userId = context.getBean(User.class).getId();
		notificationService.deleteAll(userId);
		return null;
	}

	@RequestMapping(value = { "/list", "/list/" }, method = RequestMethod.GET)
	public Object list(
		@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
		@RequestParam(value = "size", required = false, defaultValue = "20") Integer size//
	) {
		String userId = context.getBean(User.class).getId();
		return notificationService.list(userId, page, size);
	}

	@RequestMapping(value = { "/peek", "/peek/" }, method = RequestMethod.GET)
	public Object peek() {
		String userId = context.getBean(User.class).getId();
		return notificationService.list(userId, 0, 5);
	}

	@RequestMapping(value = { "/count", "/count/" }, method = RequestMethod.GET)
	public Object count() {
		String userId = context.getBean(User.class).getId();
		return notificationService.countUnread(userId);
	}

	public void saveAll(List<Notification> notifications) {
		Date now = new Date();
		List<SettingDefinition> defaults = definitionService.list();
		for (Notification notification : notifications) {
			List<Setting> userSettings = profileController.listSettingsOfUser(notification.getTargetUser()).stream()
				.filter(s -> s.getDefinition().getEventType() == notification.getEventType())
				.collect(Collectors.toList());
			Optional<Setting> nOptional = userSettings.stream()
				.filter(s -> s.getDefinition().getSettingGroup() == SettingGroup.Notifications)
				.filter(s -> s.getDefinition().getEventType() == notification.getEventType())
				.findFirst();
			Boolean notify = true;
			if (nOptional.isPresent()) {
				try {
					notify = Boolean.parseBoolean(nOptional.get().getValue());
				}
				catch (Exception e) {
				}
			}
			else {
				Optional<SettingDefinition> dOptional = defaults.stream()
					.filter(d -> d.getSettingGroup() == SettingGroup.Notifications)
					.filter(d -> d.getEventType() == notification.getEventType())
					.findFirst();
				if (dOptional.isPresent()) {
					try {
						notify = Boolean.parseBoolean(dOptional.get().getDefaultValue());
					}
					catch (Exception e) {
					}
				}
			}
			if (!notify) {
				notification.setRead(true);
				notification.setReadDate(now);
			}
			Optional<Setting> eOptional = userSettings.stream()
				.filter(s -> s.getDefinition().getSettingGroup() == SettingGroup.Email)
				.filter(s -> s.getDefinition().getEventType() == notification.getEventType())
				.findFirst();
			Boolean sendEmail = true;
			if (eOptional.isPresent()) {
				try {
					sendEmail = Boolean.parseBoolean(eOptional.get().getValue());
				}
				catch (Exception e) {
				}
			}
			else {
				Optional<SettingDefinition> dOptional = defaults.stream()
					.filter(d -> d.getSettingGroup() == SettingGroup.Email)
					.filter(d -> d.getEventType() == notification.getEventType())
					.findFirst();
				if (dOptional.isPresent()) {
					try {
						sendEmail = Boolean.parseBoolean(dOptional.get().getDefaultValue());
					}
					catch (Exception e) {
					}
				}
			}
			if (sendEmail) {
				String email = userService.get(notification.getTargetUser(), false).getEmail();
				emailUtil.sendEmailAsync(email, notification.getSubject(), notification.getText());
			}
		}
		notificationService.saveAll(notifications);
	}

}

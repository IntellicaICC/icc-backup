package net.intellica.icc.services.model.sd;

import com.fasterxml.jackson.databind.util.StdConverter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateStringConverter extends StdConverter<String, Date> {

	@Override
	public Date convert(String value) {
		Date result = null;
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
			format.setTimeZone(TimeZone.getDefault());
			result = format.parse(value);
		}
		catch (Exception e) {
		}
		return result;
	}

}

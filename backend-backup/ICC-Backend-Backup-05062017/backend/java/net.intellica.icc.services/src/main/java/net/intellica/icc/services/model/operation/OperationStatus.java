package net.intellica.icc.services.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.Arrays;

public enum OperationStatus {

	/**
	 * Job/Rule is not yet triggered.
	 */
	Idle,

	/**
	 * Job/Rule is executing.
	 */
	Executing,

	/**
	 * Job/Rule has executed successfully.
	 */
	Finished,

	/**
	 * Job/Rule is about to pause.
	 */
	Pausing,

	/**
	 * Job/Rule is paused, waiting for the resume command.
	 */
	Paused,

	/**
	 * Job/Rule is trying to cancel its execution.
	 */
	Canceling,

	/**
	 * Job/Rule execution is canceled by the user. 
	 */
	Canceled,

	/**
	 * Job/Rule is skipped before it can execute.
	 */
	Skipped,

	/**
	 * Job/Rule has encountered an error and stopped.
	 */
	Error;

	@JsonCreator
	public static OperationStatus fromString(String string) {
		return Arrays.asList(OperationStatus.values()).stream().filter(s -> s.toString().equals(string)).findFirst().orElse(null);
	}

}

package net.intellica.icc.services.util.other;

import net.intellica.icc.services.model.operation.OperationLiveStatus;
import net.intellica.icc.services.model.rule.Graph;
import net.intellica.icc.services.model.rule.NodeStatus;
import net.intellica.icc.template.exception.TechnicalException;
import org.springframework.stereotype.Component;

@Component
public class ConversionUtil {

	public OperationLiveStatus convertDetailToLiveStatus(String ruleDetail) {
		OperationLiveStatus result = new OperationLiveStatus();
		try {
			Graph graph = Graph.fromString(ruleDetail);
			graph.getNodes().forEach(n -> result.getStates().put(n.getId(), NodeStatus.Idle));
		}
		catch (Exception e) {
			throw new TechnicalException("Can not convert rule graph into live status!");
		}
		return result;
	}

}

package net.intellica.icc.services.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import net.intellica.icc.services.model.Template;
import net.intellica.icc.services.model.VariableDefinition;
import net.intellica.icc.services.service.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/template")
public class TemplateController {

	@Autowired private TemplateService templateService;

	@RequestMapping(value = { "/{id}", "/{id}/" }, method = RequestMethod.GET)
	public Object get(@PathVariable String id) {
		return templateService.get(id, false);
	}

	@RequestMapping(value = { "/{id}/variables", "/{id}/variables/" }, method = RequestMethod.GET)
	public Object listVariables(@PathVariable String id) {
		Set<VariableDefinition> result = null;
		Template template = templateService.get(id, true, false);
		if (template != null)
			result = template.getVariables();
		return result;
	}

	@RequestMapping(value = { "/save", "/save/" }, method = RequestMethod.POST)
	public Object save(@RequestBody Template template) {
		return templateService.save(template).getId();
	}

	@RequestMapping(value = { "/{id}", "/{id}/" }, method = RequestMethod.DELETE)
	public Object delete(@PathVariable String id) {
		templateService.delete(id);
		return null;
	}

	@RequestMapping(value = { "/list", "/list/" }, method = RequestMethod.GET)
	public Object list(@RequestParam(value = "name", required = false) String name) {
		List<Template> result = new ArrayList<>();
		if (StringUtils.isEmpty(name))
			result = templateService.listAll();
		else
			result = templateService.listByName(name);
		return result;
	}

}

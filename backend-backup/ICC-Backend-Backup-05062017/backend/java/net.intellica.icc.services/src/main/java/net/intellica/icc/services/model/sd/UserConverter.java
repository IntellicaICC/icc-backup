package net.intellica.icc.services.model.sd;

import com.fasterxml.jackson.databind.util.StdConverter;
import net.intellica.icc.services.model.User;

public class UserConverter extends StdConverter<String, User> {

	@Override
	public User convert(String value) {
		User result = new User();
		result.setId(value);
		return result;
	}

}

package net.intellica.icc.services.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import net.intellica.icc.services.model.RepeatInterval;
import net.intellica.icc.services.model.operation.Operation;
import net.intellica.icc.services.model.operation.OperationLog;
import net.intellica.icc.services.model.operation.OperationStatus;
import net.intellica.icc.services.model.operation.OperationSummary;
import net.intellica.icc.services.model.operation.OperationType;
import net.intellica.icc.services.repo.OperationRepository;
import net.intellica.icc.services.util.aop.PreSaveUtil;
import net.intellica.icc.services.util.audit.HistoryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class OperationService {

	@Autowired private OperationRepository repository;
	@Autowired private OperationLogService logService;
	@Autowired private PreSaveUtil preSaveUtil;
	@Autowired private HistoryUtil historyUtil;

	public OperationService() {
	}

	public Operation get(String id, Boolean includeHistory) {
		return get(id, false, includeHistory);
	}

	public Operation get(String id, Boolean fetchResults, Boolean includeHistory) {
		Operation result = repository.findOne(id);
		if (includeHistory && result == null)
			result = historyUtil.getLastRevision(Operation.class, id);
		if (fetchResults && result != null && result.getDetail() != null)
			result.getDetail().getResults().size();
		fillLogs(result);
		return result;
	}

	public Operation get(String triggerId, OperationType type, Date nextRunDate) {
		Operation result = repository.findByTriggerIdAndTypeAndNextRunDate(triggerId, type, nextRunDate);
		fillLogs(result);
		return result;
	}

	public Operation getByInstanceId(String instanceId) {
		Operation result = repository.findByInstanceId(instanceId);
		fillLogs(result);
		return result;
	}

	public String getMaxInstanceId(String triggerId, OperationType type) {
		String result = triggerId;
		Operation maxOperation = repository.findFirstByTriggerIdAndTypeAndInstanceIdNotNullOrderByInstanceIdDesc(triggerId, type);
		if (maxOperation != null && !StringUtils.isEmpty(maxOperation.getInstanceId()))
			result = maxOperation.getInstanceId();
		return result;
	}

	public Operation save(Operation operation) {
		preSaveUtil.fill(operation);
		return repository.save(operation);
	}

	public List<Operation> saveAll(List<Operation> operations) {
		List<Operation> result = new ArrayList<>();
		preSaveUtil.fillAllOperations(operations);
		Iterable<Operation> saved = repository.save(operations);
		List<OperationLog> logs = new ArrayList<>();
		Date now = new Date();
		for (Operation operation : saved) {
			OperationLog log = new OperationLog();
			log.setOperationId(operation.getId());
			log.setOperationDate(now);
			log.setStatus(OperationStatus.Idle);
			logs.add(log);
		}
		logService.saveAll(logs);
		saved.forEach(result::add);
		return result;
	}

	public void deleteAll(Collection<Operation> operations) {
		repository.delete(operations);
	}

	public List<Operation> list(Date from, Date to) {
		List<Operation> result = repository.findAllByNextRunDateBetweenOrderByNextRunDateDesc(from, to);
		result.forEach(this::fillLogs);
		return result;
	}

	public List<Operation> list(Date from, Date to, Integer page, Integer size) {
		List<Operation> result = repository.findAllByNextRunDateBetweenOrderByNextRunDateDesc(from, to, new PageRequest(page, size));
		result.forEach(this::fillLogs);
		return result;
	}

	public List<Operation> list(String triggerId, Date from) {
		List<Operation> result = repository.findAllByTriggerIdAndNextRunDateGreaterThan(triggerId, from);
		result.forEach(this::fillLogs);
		return result;
	}

	public List<Operation> list(Date at) {
		List<Operation> result = repository.findAllByNextRunDate(at);
		result.forEach(this::fillLogs);
		return result;
	}

	public List<Operation> list(String modelId, OperationType type, Boolean skip, OperationSummary summary) {
		List<Operation> result = repository.findAllByModelIdAndTypeAndSkipNextRunAndSummary(modelId, type, skip, summary);
		result.forEach(this::fillLogs);
		return result;
	}

	public List<Operation> list(String triggerId) {
		List<Operation> result = repository.findAllByTriggerIdAndScheduleIntervalNot(triggerId, RepeatInterval.NONE);
		result.forEach(this::fillLogs);
		return result;
	}

	public List<Operation> list(OperationType type, Boolean skip, OperationSummary summary, Date to) {
		List<Operation> result = repository.findAllByTypeAndSkipNextRunAndSummaryAndNextRunDateLessThan(type, skip, summary, to);
		result.forEach(this::fillLogs);
		return result;
	}

	public List<String> listDistinctScheduleJobTriggers(OperationType type) {
		return repository.findDistinctTriggerIdByTypeAndScheduleIntervalNot(type, RepeatInterval.NONE).stream()
			.map(o -> o.getTriggerId())
			.collect(Collectors.toList());
	}

	public Date getLastScheduleTriggerRunDate(String triggerId, OperationType type) {
		return repository.findTopByTriggerIdAndTypeAndScheduleIntervalNotOrderByNextRunDateDesc(triggerId, type, RepeatInterval.NONE).getNextRunDate();
	}

	public List<Operation> listByUser(String userId, OperationType type, Boolean skip, OperationSummary summary) {
		List<Operation> result = repository.findAllByUserIdAndTypeAndSkipNextRunAndSummary(userId, type, skip, summary);
		result.forEach(this::fillLogs);
		return result;
	}

	public List<Operation> listUsersScheduled(String userId, OperationType type) {
		List<Operation> result = repository.findDistinctTriggerIdByUserIdAndTypeAndScheduleIntervalNot(userId, type, RepeatInterval.NONE);
		result.forEach(this::fillLogs);
		return result;
	}

	private void fillLogs(Operation operation) {
		if (operation != null) {
			operation.setStartLog(logService.getStart(operation.getId()));
			operation.setLastLog(logService.getLast(operation.getId()));
		}
	}

}

package net.intellica.icc.services.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.Arrays;

public enum Command {

	None,
	Start,
	Skip,
	Pause,
	Resume,
	Cancel,
	Restart,
	Reschedule,
	Unschedule,
	Details,
	Live,
	Run,
	Schedule;

	@JsonCreator
	public static Command fromString(String string) {
		return Arrays.asList(Command.values()).stream().filter(s -> s.toString().equals(string)).findFirst().orElse(null);
	}

}

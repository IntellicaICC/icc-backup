package net.intellica.icc.services.util.aop;

import java.util.Collection;
import net.intellica.icc.services.model.Rule;
import org.springframework.stereotype.Component;

@Component
public class RefineUtil {

	public void refineRuleFields(Collection<Rule> collection) {
		collection.forEach(this::refineRuleFields);
	}

	public void refineRuleFields(Rule rule) {
		rule.setDetail(null);
		rule.setImage(null);
	}

}

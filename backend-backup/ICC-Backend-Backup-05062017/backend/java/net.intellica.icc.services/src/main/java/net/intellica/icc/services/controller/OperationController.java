package net.intellica.icc.services.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import net.intellica.icc.services.model.Job;
import net.intellica.icc.services.model.Rule;
import net.intellica.icc.services.model.Schedule;
import net.intellica.icc.services.model.operation.Command;
import net.intellica.icc.services.model.operation.CommandLog;
import net.intellica.icc.services.model.operation.InternalOperation;
import net.intellica.icc.services.model.operation.Operation;
import net.intellica.icc.services.model.operation.OperationDetail;
import net.intellica.icc.services.model.operation.OperationLiveStatus;
import net.intellica.icc.services.model.operation.OperationStatus;
import net.intellica.icc.services.model.operation.OperationType;
import net.intellica.icc.services.model.work.WorkflowResult;
import net.intellica.icc.services.quartz.QuartzManager;
import net.intellica.icc.services.service.CommandLogService;
import net.intellica.icc.services.service.JobService;
import net.intellica.icc.services.service.OperationService;
import net.intellica.icc.services.service.RuleService;
import net.intellica.icc.services.util.dev.DateTimeUtil;
import net.intellica.icc.services.util.other.OperationUtil;
import net.intellica.icc.services.util.other.ServiceConstants;
import net.intellica.icc.template.exception.NotFoundException;
import net.intellica.icc.template.model.WorkItemResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/operation")
public class OperationController {

	@Autowired private QuartzManager quartz;
	@Autowired private OperationService operationService;
	@Autowired private DateTimeUtil dateTimeUtil;
	@Autowired private JobService jobService;
	@Autowired private RuleService ruleService;
	@Autowired private CommandLogService commandLogService;
	@Autowired private OperationLogController operationLogController;
	@Autowired private OperationUtil operationUtil;

	@RequestMapping(value = { "/{id}", "/{id}/" }, method = RequestMethod.GET)
	public Object get(@PathVariable String id) {
		return operationService.get(id, false);
	}

	@RequestMapping(value = { "/{id}/detail", "/{id}/detail/" }, method = RequestMethod.GET)
	public Object getDetail(@PathVariable String id) {
		Operation operation = operationService.get(id, true, false);
		if (operation == null)
			throw new NotFoundException();
		return operation.getDetail();
	}

	@RequestMapping(value = { "/schedule", "/schedule/" }, method = RequestMethod.GET)
	public Object listSchedule(
		@RequestParam(value = "from", required = true) @DateTimeFormat(pattern = ServiceConstants.Operation.DATETIME_FORMAT) Date from,
		@RequestParam(value = "to", required = true) @DateTimeFormat(pattern = ServiceConstants.Operation.DATETIME_FORMAT) Date to,
		@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
		@RequestParam(value = "size", required = false, defaultValue = "20") Integer size//
	) {
		return operationService.list(from, to, page, size);
	}

	@RequestMapping(value = { "/today", "/today/" }, method = RequestMethod.GET)
	public Object listToday() {
		return operationService.list(dateTimeUtil.getStartOfToday(), dateTimeUtil.getEndOfToday());
	}

	@RequestMapping(value = { "/history", "/history/" }, method = RequestMethod.GET)
	public Object listHistory(
		@RequestParam(value = "from", required = true) @DateTimeFormat(pattern = ServiceConstants.Operation.DATETIME_FORMAT) Date from,
		@RequestParam(value = "to", required = true) @DateTimeFormat(pattern = ServiceConstants.Operation.DATETIME_FORMAT) Date to,
		@RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
		@RequestParam(value = "size", required = false, defaultValue = "20") Integer size//
	) {
		return operationService.list(from, to, page, size);
	}

	@RequestMapping(value = { "/{id}/start", "/{id}/start/" }, method = RequestMethod.GET)
	public Object start(@PathVariable String id) throws Exception {
		Operation operation = operationService.get(id, false);
		saveCommand(operation, Command.Start);
		startCommon(operation);
		return null;
	}

	@RequestMapping(value = { "/{id}/skip", "/{id}/skip/" }, method = RequestMethod.GET)
	public Object skip(@PathVariable String id) {
		Operation operation = operationService.get(id, false);
		saveCommand(operation, Command.Skip);
		operation.setSkipNextRun(true);
		operationService.save(operation);
		operationLogController.save(id, OperationStatus.Skipped);
		return null;
	}

	@RequestMapping(value = { "/{id}/pause", "/{id}/pause/" }, method = RequestMethod.GET)
	public Object pause(@PathVariable String id) {
		return null;
	}

	@RequestMapping(value = { "/{id}/resume", "/{id}/resume/" }, method = RequestMethod.GET)
	public Object resume(@PathVariable String id) {
		return null;
	}

	@RequestMapping(value = { "/{id}/cancel", "/{id}/cancel/" }, method = RequestMethod.GET)
	public Object cancel(@PathVariable String id) throws Exception {
		Operation operation = operationService.get(id, false);
		saveCommand(operation, Command.Cancel);
		operationLogController.save(id, OperationStatus.Canceling);
		if (!quartz.cancel(operation.getInstanceId(), operation.getType())) {
			if (operation.getType() == OperationType.Job) {
				WorkItemResult workResult = new WorkItemResult();
				workResult.setCanceled(true);
				operationUtil.save(id, workResult);
				operationLogController.save(id, OperationStatus.Canceled);
			}
			else if (operation.getType() == OperationType.Rule) {
				WorkflowResult workResult = new WorkflowResult();
				workResult.setCanceled(true);
				operationUtil.save(id, workResult);
				operationLogController.save(id, OperationStatus.Canceled);
			}
		}
		return null;
	}

	@RequestMapping(value = { "/{id}/restart", "/{id}/restart/" }, method = RequestMethod.GET)
	public Object restart(@PathVariable String id) throws Exception {
		Operation operation = operationService.get(id, false);
		saveCommand(operation, Command.Restart);
		startCommon(operation);
		return null;
	}

	@RequestMapping(value = { "/{id}/reschedule", "/{id}/reschedule/" }, method = RequestMethod.POST)
	public Object reschedule(@PathVariable String id, @RequestBody Schedule schedule) throws Exception {
		Operation operation = operationService.get(id, false);
		saveCommand(operation, Command.Reschedule);
		unscheduleCommon(operation);
		if (operation.getType() == OperationType.Job) {
			Job job = jobService.get(operation.getModelId(), false);
			schedule(job, schedule);
		}
		else if (operation.getType() == OperationType.Rule) {
			Rule rule = ruleService.get(operation.getModelId(), false);
			schedule(rule, schedule);
		}
		return null;
	}

	@RequestMapping(value = { "/{id}/unschedule", "/{id}/unschedule/" }, method = RequestMethod.GET)
	public Object unschedule(@PathVariable String id) throws Exception {
		Operation operation = operationService.get(id, false);
		saveCommand(operation, Command.Unschedule);
		unscheduleCommon(operation);
		return null;
	}

	@RequestMapping(value = { "/internal", "/internal/" }, method = RequestMethod.GET)
	public Object internal(@RequestParam(value = "name", required = true) String name) throws Exception {
		InternalOperation internalOperation = InternalOperation.valueOf(name.trim().toUpperCase(Locale.US));
		quartz.runInternal(internalOperation);
		return null;
	}

	public Operation run(Job job) throws Exception {
		return schedule(job, new Schedule(dateTimeUtil.getMinimumOperationDate())).get(0);
	}

	public List<Operation> schedule(Job job, Schedule schedule) throws Exception {
		String triggerId = quartz.schedule(job, schedule);
		return scheduleCommon(job.getId(), job.getProps().getName(), schedule, triggerId, OperationType.Job);
	}

	public Operation run(Rule rule) throws Exception {
		return schedule(rule, new Schedule(dateTimeUtil.getMinimumOperationDate())).get(0);
	}

	public List<Operation> schedule(Rule rule, Schedule schedule) throws Exception {
		String triggerId = quartz.schedule(rule, schedule);
		return scheduleCommon(rule.getId(), rule.getProps().getName(), schedule, triggerId, OperationType.Rule);
	}

	private List<Operation> scheduleCommon(String modelId, String modelName, Schedule schedule, String triggerId, OperationType type) throws Exception {
		List<Operation> result = new ArrayList<>();
		List<Date> dates = quartz.generateRunDates(triggerId, type, new Date(), 2);
		if (dates.size() > 0)
			result.addAll(saveExtension(modelId, modelName, schedule, triggerId, type, dates));
		return result;
	}

	public List<Operation> saveExtension(String modelId, String modelName, Schedule schedule, String triggerId, OperationType type, List<Date> dates) {
		List<Operation> result = new ArrayList<>();
		List<Operation> operations = new ArrayList<>();
		Operation baseOperation = new Operation();
		baseOperation.setType(type);
		baseOperation.setModelId(modelId);
		baseOperation.setModelName(modelName);
		baseOperation.setSchedule(schedule);
		baseOperation.setTriggerId(triggerId);
		Boolean firstOperation = true;
		for (Date runDate : dates) {
			Operation operation = new Operation(baseOperation);
			operation.setNextRunDate(runDate);
			operation.setSkipNextRun(false);
			OperationDetail detail = new OperationDetail();
			operation.setDetail(detail);
			if (firstOperation) {
				result.addAll(operationService.saveAll(Arrays.asList(operation)));
				firstOperation = false;
			}
			else
				operations.add(operation);
		}
		result.addAll(operationService.saveAll(operations));
		return result;
	}

	@RequestMapping(value = { "/{id}/live", "/{id}/live/" }, method = RequestMethod.GET)
	public Object live(@PathVariable String id) throws Exception {
		Object result = null;
		Operation operation = operationService.get(id, false);
		if (operation.getType() == OperationType.Rule) {
			if (operation.getStatus() == OperationStatus.Executing)
				result = quartz.liveStatus(operation.getInstanceId());
			else {
				String statusString = operation.getDetail().getFinalStatus();
				if (!StringUtils.isEmpty(statusString))
					result = OperationLiveStatus.fromString(statusString);
			}
		}
		return result;
	}

	public void save(String id, OperationType type, String name) {
		List<Operation> operations = operationService.list(id, type, false, null);
		List<Operation> toUpdate = operations.stream()
			.filter(o -> o.getNextRunDate().after(dateTimeUtil.getMinimumOperationDate()))
			.filter(o -> !o.getModelName().equals(name))
			.collect(Collectors.toList());
		if (toUpdate.size() > 0) {
			toUpdate.forEach(o -> o.setModelName(name));
			toUpdate.forEach(operationService::save);
		}
	}

	public void saveCommand(Operation operation, Command command) {
		CommandLog commandLog = new CommandLog(operation);
		commandLog.setCommand(command);
		commandLogService.save(commandLog);
	}

	private void startCommon(Operation operation) throws Exception {
		if (operation.getType() == OperationType.Job) {
			Job job = jobService.get(operation.getModelId(), false);
			run(job);
		}
		else if (operation.getType() == OperationType.Rule) {
			Rule rule = ruleService.get(operation.getModelId(), false);
			run(rule);
		}
	}

	private void unscheduleCommon(Operation operation) throws Exception {
		quartz.unschedule(operation.getTriggerId(), operation.getType());
		List<Operation> toDelete = operationService.list(operation.getTriggerId(), new Date());
		toDelete.forEach(o -> operationLogController.deleteByOperation(o.getId()));
		operationService.deleteAll(toDelete);
	}

}

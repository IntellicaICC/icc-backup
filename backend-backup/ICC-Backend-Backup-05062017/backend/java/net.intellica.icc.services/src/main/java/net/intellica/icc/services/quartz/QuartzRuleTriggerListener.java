package net.intellica.icc.services.quartz;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import net.intellica.icc.services.model.Rule;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.model.operation.Operation;
import net.intellica.icc.services.model.operation.OperationType;
import net.intellica.icc.services.model.operation.SkipReason;
import net.intellica.icc.services.security.AuthorizationUtil;
import net.intellica.icc.services.service.OperationService;
import net.intellica.icc.services.service.RuleService;
import net.intellica.icc.services.service.UserService;
import net.intellica.icc.services.util.other.ServiceConstants;
import net.intellica.icc.template.model.GlobalVariable;
import org.quartz.JobExecutionContext;
import org.quartz.Trigger;
import org.quartz.Trigger.CompletedExecutionInstruction;
import org.quartz.TriggerListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class QuartzRuleTriggerListener implements TriggerListener {

	@Autowired private OperationService operationService;
	@Autowired private RuleService ruleService;
	@Autowired private AuthorizationUtil authorizationUtil;
	@Autowired private UserService userService;

	@Override
	public String getName() {
		return "QuartzRuleTriggerListener";
	}

	@Override
	public void triggerFired(Trigger trigger, JobExecutionContext context) {
		trigger.getJobDataMap().keySet().forEach(k -> context.put(k, trigger.getJobDataMap().get(k)));
	}

	@Override
	public boolean vetoJobExecution(Trigger trigger, JobExecutionContext context) {
		Boolean result = true;
		// TODO repeat eden islerde getStartTime dogru zamani veriyor mu?
		Operation operation = operationService.get(trigger.getKey().getName(), OperationType.Rule, trigger.getStartTime());
		Map<String, String> parameters = (Map<String, String>)context.get(QuartzParameter.PARAMETERS);
		parameters.put(GlobalVariable.OperationID.toString(), operation.getId());
		if (!trigger.getJobDataMap().containsKey(QuartzParameter.SKIP_REASON)) {
			result = operation.getSkipNextRun();
			if (result)
				trigger.getJobDataMap().put(QuartzParameter.SKIP_REASON, SkipReason.User);
			// TODO rule silindiyse?
			if (!result)
				result = checkInactive(trigger, operation);
			if (!result)
				result = checkCollision(trigger, operation);
			if (!result)
				result = checkUnauthorized(trigger, operation);
		}
		return result;
	}

	@Override
	public void triggerMisfired(Trigger trigger) {
		trigger.getJobDataMap().put(QuartzParameter.SKIP_REASON, SkipReason.Misfire);
	}

	@Override
	public void triggerComplete(Trigger trigger, JobExecutionContext context, CompletedExecutionInstruction triggerInstructionCode) {
		// en son tetiklenen metot
	}

	private synchronized Boolean checkCollision(Trigger trigger, Operation operation) {
		Boolean result = false;
		List<Operation> allOperations = operationService.list(operation.getModelId(), OperationType.Rule, false, null);
		Predicate<Operation> executionControl = o -> !o.getId().equals(operation.getId()) &&
			ServiceConstants.Operation.COLLISION_STATES.contains(o.getStatus());
		List<Operation> executingOperations = allOperations.stream().filter(executionControl).collect(Collectors.toList());
		result = executingOperations.size() > 0;
		if (result)
			trigger.getJobDataMap().put(QuartzParameter.SKIP_REASON, SkipReason.Collision);
		return result;
	}

	private Boolean checkInactive(Trigger trigger, Operation operation) {
		Boolean result;
		Rule rule = ruleService.get(operation.getModelId(), false);
		result = !rule.getProps().isActive();
		if (result)
			trigger.getJobDataMap().put(QuartzParameter.SKIP_REASON, SkipReason.Inactive);
		return result;
	}

	private Boolean checkUnauthorized(Trigger trigger, Operation operation) {
		Boolean result;
		User user = userService.get(operation.getUserId(), true, false);
		result = !authorizationUtil.canExecuteOperation(operation, user, false);
		if (result)
			trigger.getJobDataMap().put(QuartzParameter.SKIP_REASON, SkipReason.Unauthorized);
		return result;
	}

}

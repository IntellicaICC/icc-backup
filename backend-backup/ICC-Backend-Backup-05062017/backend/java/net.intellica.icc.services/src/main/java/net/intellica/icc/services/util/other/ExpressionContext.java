package net.intellica.icc.services.util.other;

import org.springframework.expression.ParserContext;

public class ExpressionContext implements ParserContext {

	@Override
	public boolean isTemplate() {
		return true;
	}

	@Override
	public String getExpressionPrefix() {
		return ServiceConstants.Expression.EXPRESSION_PREFIX;
	}

	@Override
	public String getExpressionSuffix() {
		return ServiceConstants.Expression.EXPRESSION_SUFFIX;
	}

}

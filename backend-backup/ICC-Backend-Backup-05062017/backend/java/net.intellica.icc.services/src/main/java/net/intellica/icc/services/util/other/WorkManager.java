package net.intellica.icc.services.util.other;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URI;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import net.intellica.icc.services.config.ServicesConfig;
import net.intellica.icc.services.util.config.EngineUtil;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import net.intellica.icc.services.util.thread.ContextUtil;
import net.intellica.icc.template.exception.CommunicationException;
import net.intellica.icc.template.exception.TimeoutException;
import net.intellica.icc.template.model.GlobalVariable;
import net.intellica.icc.template.model.InstanceKey;
import net.intellica.icc.template.model.WorkItem;
import net.intellica.icc.template.model.WorkItemResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class WorkManager {

	private ContextUtil contextUtil;
	private WorkItem work;
	private InstanceKey key;
	private Boolean canceled;

	public WorkManager(ContextUtil contextUtil, WorkItem work) {
		this.contextUtil = contextUtil;
		this.work = work;
		String ruleInstanceId = work.getParameters().get(GlobalVariable.RuleInstanceID.toString());
		String jobInstanceId = work.getParameters().get(GlobalVariable.JobInstanceID.toString());
		key = new InstanceKey(ruleInstanceId, jobInstanceId);
		canceled = false;
	}

	public WorkItemResult runSync() {
		WorkItemResult result = new WorkItemResult();
		try {
			RestTemplate rest = new RestTemplate();
			String runUrl = contextUtil.call(EngineUtil.class).getRunUrl();
			ResponseEntity<String> runResponse = rest.postForEntity(new URI(runUrl), work, String.class);
			if (runResponse.getStatusCode() == HttpStatus.ACCEPTED) {
				String ruleInstanceId = work.getParameters().get(GlobalVariable.RuleInstanceID.toString());
				String jobInstanceId = work.getParameters().get(GlobalVariable.JobInstanceID.toString());
				InstanceKey key = new InstanceKey(ruleInstanceId, jobInstanceId);
				// servis tarafinda sonuc bekleme
				String resultUrl = contextUtil.call(EngineUtil.class).getResultUrl();
				Map<String, Object> urlVariables = new HashMap<>();
				urlVariables.put("wait", "true");
				urlVariables.put("timeout", 0);
				Duration jobTimeout = Duration.ofSeconds(contextUtil.call(ServicesConfig.class).getJob_timeout());
				Duration totalSeconds = Duration.ZERO;
				while (totalSeconds.compareTo(jobTimeout) < 0 && !canceled) {
					Instant start = Instant.now();
					ResponseEntity<WorkItemResult> resultResponse = rest.postForEntity(resultUrl, key, WorkItemResult.class, urlVariables);
					WorkItemResult engineResult = resultResponse.getBody();
					if (resultResponse.getStatusCode() == HttpStatus.OK) {
						if (engineResult != null) {
							castResultTypes(engineResult);
							result = engineResult;
							break;
						}
					}
					else if (resultResponse.getStatusCode() == HttpStatus.NOT_FOUND)
						throw new CommunicationException("Engine could not find the work item!");
					else
						throw new CommunicationException("Engine encountered an unknown error while returning job result!");
					totalSeconds = totalSeconds.plus(Duration.between(start, Instant.now()));
				}
				if (canceled)
					result.setCanceled(true);
				else if (totalSeconds.compareTo(jobTimeout) >= 0)
					result.setError(new TimeoutException("Job timeout!"));
			}
			else
				throw new CommunicationException("Engine did not accept the work item!");
		}
		catch (Exception e) {
			if (e instanceof CommunicationException)
				result.setError(e);
			else
				result.setError(new CommunicationException(e));
		}
		return result;
	}

	public void cancel() {
		canceled = true;
		RestTemplate rest = new RestTemplate();
		try {
			String cancelUrl = contextUtil.call(EngineUtil.class).getCancelUrl();
			ResponseEntity<String> cancelResponse = rest.postForEntity(new URI(cancelUrl), key, String.class);
			if (cancelResponse.getStatusCode() != HttpStatus.OK)
				throw new CommunicationException("Could not cancel operation with instance id=" + key);
		}
		catch (Exception e) {
			contextUtil.call(SimpleExceptionHandler.class).warning(e.getMessage());
		}
	}

	private void castResultTypes(WorkItemResult workResult) {
		ObjectMapper mapper = new ObjectMapper();
		for (String key : workResult.getResults().keySet()) {
			try {
				Object rawValue = workResult.getResults().get(key);
				if (rawValue != null) {
					Object actualValue = mapper.readValue(rawValue.toString(), Class.forName(workResult.getResultTypes().get(key)));
					workResult.getResults().put(key, actualValue.toString());
				}
			}
			catch (Exception e) {
				// bu sonuc oldugu gibi kalsin
			}
		}
	}

}

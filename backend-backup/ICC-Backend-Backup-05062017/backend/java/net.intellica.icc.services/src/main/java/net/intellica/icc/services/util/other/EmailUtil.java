package net.intellica.icc.services.util.other;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class EmailUtil implements AutoCloseable {

	@Autowired private JavaMailSender mailSender;

	private final ExecutorService executor;

	public EmailUtil() {
		executor = Executors.newSingleThreadExecutor();
	}

	public void sendEmail(String to, String subject, String text) {
		mailSender.send(createSimpleMail(to, subject, text));
	}

	public void sendEmailAsync(String to, String subject, String text) {
		executor.submit(() -> sendEmail(to, subject, text));
	}

	private SimpleMailMessage createSimpleMail(String to, String subject, String text) {
		SimpleMailMessage result = new SimpleMailMessage();
		result.setFrom("noreply@icc-mailer");
		result.setTo(to);
		result.setSubject(subject);
		result.setText(text);
		return result;
	}

	@Override
	public void close() throws Exception {
		executor.shutdownNow();
	}

}

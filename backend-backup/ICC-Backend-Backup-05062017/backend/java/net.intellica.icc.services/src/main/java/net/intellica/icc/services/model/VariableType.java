package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.Arrays;

public enum VariableType {

	Standard,
	Connection,
	SQL,
	Boolean,
	List,
	Dataset;

	@JsonCreator
	public static VariableType fromString(String string) {
		return Arrays.asList(VariableType.values()).stream().filter(s -> s.toString().equals(string)).findFirst().orElse(null);
	}

}

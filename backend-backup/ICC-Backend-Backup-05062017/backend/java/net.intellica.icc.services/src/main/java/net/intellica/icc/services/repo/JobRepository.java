package net.intellica.icc.services.repo;

import java.util.List;
import net.intellica.icc.services.model.Job;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JobRepository extends PagingAndSortingRepository<Job, String> {

	List<Job> findAllByPropsName(String name);

	List<Job> findAllByFolderIdOrderByPropsNameAsc(String id);

	List<Job> findDistinctIdBy();

}

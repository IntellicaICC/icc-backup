package net.intellica.icc.services.model;

public enum NotificationType {

	Operational,
	Custom,
	Other
	
}

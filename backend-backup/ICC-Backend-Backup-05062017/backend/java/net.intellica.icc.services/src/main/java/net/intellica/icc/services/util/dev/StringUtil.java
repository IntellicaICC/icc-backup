package net.intellica.icc.services.util.dev;

import java.util.Locale;

public class StringUtil {

	public static String toUpper(Enum<?> e) {
		return e.toString().toUpperCase(Locale.ENGLISH);
	}

	public static String[] toUpper(Enum<?>... e) {
		String[] result = new String[e.length];
		for (int i = 0; i < result.length; i++) {
			result[i] = e[i].toString().toUpperCase(Locale.ENGLISH);
		}
		return result;
	}

}

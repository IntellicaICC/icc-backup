package net.intellica.icc.services.model.work;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.Arrays;

public enum WorkflowResultStatus {

	Executing,
	Finished,
	Error,
	Canceled;

	@JsonCreator
	public static WorkflowResultStatus fromString(String string) {
		return Arrays.asList(WorkflowResultStatus.values()).stream().filter(s -> s.toString().equals(string)).findFirst().orElse(null);
	}

}

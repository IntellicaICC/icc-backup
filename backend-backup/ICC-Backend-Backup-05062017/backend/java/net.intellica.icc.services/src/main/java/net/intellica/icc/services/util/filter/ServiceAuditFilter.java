package net.intellica.icc.services.util.filter;

import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;
import java.util.Set;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.intellica.icc.services.model.ServiceCategory;
import net.intellica.icc.services.model.ServiceLog;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.service.ServiceLogService;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class ServiceAuditFilter extends OncePerRequestFilter {

	@Autowired private ApplicationContext context;
	@Autowired private ServiceLogService serviceLogService;
	@Autowired private SimpleExceptionHandler exceptionHandler;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		if (context != null) {
			User currentUser = context.getBean(User.class);
			try {
				ServiceLog serviceLog = new ServiceLog();
				ServiceCategory category = Arrays.asList(ServiceCategory.values()).stream()
					.filter(c -> request.getRequestURI().startsWith("/" + c.toString().toLowerCase(Locale.US)))
					.findAny()
					.orElse(ServiceCategory.Other);
				serviceLog.setCategory(category);
				serviceLog.setLocalIp(request.getLocalAddr());
				serviceLog.setRemoteIp(request.getRemoteAddr());
				serviceLog.setLocalPort(request.getLocalPort());
				serviceLog.setRemotePort(request.getRemotePort());
				serviceLog.setUri(request.getRequestURI());
				serviceLog.setMethod(request.getMethod());
				serviceLog.setParameters(readParameters(request));
				serviceLogService.save(serviceLog, currentUser);
				// servis loga kayit atilamazsa (exception) isleme devam edilemez
				filterChain.doFilter(request, response);
			}
			catch (Exception e) {
				exceptionHandler.handleQuietly(e, currentUser);
			}
		}
		else
			logger.fatal("Autowired user service in audit filter is null!");
	}

	private String readParameters(HttpServletRequest request) {
		String result = "";
		if (!CollectionUtils.isEmpty(request.getParameterMap())) {
			StringBuilder builder = new StringBuilder("");
			Boolean firstKey = true;
			for (String key : (Set<String>)request.getParameterMap().keySet()) {
				if (firstKey)
					firstKey = false;
				else
					builder.append(",").append(" ");
				String[] values = (String[])request.getParameterMap().get(key);
				String value = "\"\"";
				if (values.length > 0)
					value = "\"" + values[0] + "\"";
				builder.append(key).append("=").append(value);
			}
			result = builder.toString();
		}
		return result;
	}

}

package net.intellica.icc.services.model.rule;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StartNode extends Node {

	private static final long serialVersionUID = 1L;

	public StartNode() {
		super();
		type = NodeType.Start;
	}

	@Override
	protected void doWork() {
		// do nothing
	}

}

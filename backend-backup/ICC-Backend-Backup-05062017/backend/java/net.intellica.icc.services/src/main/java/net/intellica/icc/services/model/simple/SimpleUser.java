package net.intellica.icc.services.model.simple;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Set;
import net.intellica.icc.services.model.LoginType;
import net.intellica.icc.services.model.Role;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.model.UserType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SimpleUser extends User {

	@Override
	@JsonIgnore
	public LoginType getLoginType() {
		return super.getLoginType();
	}

	@Override
	@JsonIgnore
	public String getPassword() {
		return super.getPassword();
	}

	@Override
	@JsonIgnore
	public Set<Role> getRoles() {
		return super.getRoles();
	}

	@Override
	@JsonIgnore
	public UserType getUserType() {
		return super.getUserType();
	}

}

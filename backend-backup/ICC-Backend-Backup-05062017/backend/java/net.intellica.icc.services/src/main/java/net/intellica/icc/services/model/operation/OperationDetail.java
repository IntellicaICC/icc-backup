package net.intellica.icc.services.model.operation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import net.intellica.icc.services.model.sd.FinalStatusConverter;
import net.intellica.icc.services.model.sd.OperationResultConverter;
import org.hibernate.envers.NotAudited;

@Embeddable
@JsonIgnoreProperties(ignoreUnknown = true)
public class OperationDetail {

	private String ruleDetail;
	private String errorDetail;
	private String queryDetail;
	private String finalStatus;
	private List<OperationResult> results;

	public OperationDetail() {
		results = new ArrayList<>();
	}

	@Lob
	@Column(name = "RULE_DETAIL", nullable = true)
	public String getRuleDetail() {
		return ruleDetail;
	}

	public void setRuleDetail(String ruleDetail) {
		this.ruleDetail = ruleDetail;
	}

	@Lob
	@Column(name = "ERROR_DETAIL", nullable = true)
	public String getErrorDetail() {
		return errorDetail;
	}

	public void setErrorDetail(String errorDetail) {
		this.errorDetail = errorDetail;
	}

	@Lob
	@Column(name = "QUERY_DETAIL", nullable = true)
	public String getQueryDetail() {
		return queryDetail;
	}

	public void setQueryDetail(String queryDetail) {
		this.queryDetail = queryDetail;
	}

	@Lob
	@Column(name = "FINAL_STATUS", nullable = true)
	@JsonSerialize(converter = FinalStatusConverter.class)
	public String getFinalStatus() {
		return finalStatus;
	}

	public void setFinalStatus(String finalStatus) {
		this.finalStatus = finalStatus;
	}

	// operation result tablosuna yazilmis (bitmis) operasyonlar zaten silinemez, o yuzden @NotAudited 
	@NotAudited
	@JsonProperty
	@OneToMany(mappedBy = "operation", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonSerialize(converter = OperationResultConverter.class)
	public List<OperationResult> getResults() {
		return results;
	}

	@JsonIgnore
	public void setResults(List<OperationResult> results) {
		this.results = results;
	}

}

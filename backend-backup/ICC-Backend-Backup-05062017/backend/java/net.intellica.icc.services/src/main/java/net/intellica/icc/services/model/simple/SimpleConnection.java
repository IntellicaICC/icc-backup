package net.intellica.icc.services.model.simple;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import net.intellica.icc.services.model.Connection;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SimpleConnection extends Connection {

	@Override
	@JsonIgnore
	public String getDatabaseUrl() {
		return super.getDatabaseUrl();
	}

	@Override
	@JsonIgnore
	public String getDatabaseUser() {
		return super.getDatabaseUser();
	}

	@Override
	@JsonIgnore
	public String getDatabasePassword() {
		return super.getDatabasePassword();
	}

}

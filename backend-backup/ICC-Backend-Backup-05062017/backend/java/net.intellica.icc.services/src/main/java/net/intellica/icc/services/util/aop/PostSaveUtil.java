package net.intellica.icc.services.util.aop;

import net.intellica.icc.services.model.Folder;
import net.intellica.icc.services.model.Job;
import net.intellica.icc.services.model.LocalParameter;
import net.intellica.icc.services.model.Rule;
import org.springframework.stereotype.Component;

@Component
public class PostSaveUtil {

	public void folderUpdated(Folder folder) {
		// TODO PostSaveUtil.folderUpdated(Folder)
	}

	public void jobUpdated(Job job) {
		// TODO PostSaveUtil.jobUpdated(Job)
	}

	public void ruleUpdated(Rule rule) {
		// TODO PostSaveUtil.ruleUpdated(Rule)
	}

	public void localParameterUpdated(LocalParameter parameter) {
		// TODO PostSaveUtil.localParameterUpdated(LocalParameter)
	}

}

package net.intellica.icc.services.model.report;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorizationUserObjectFilter extends ReportFilter {

	private String userId;
	private Byte rights;
	private Boolean includePassive;

	public AuthorizationUserObjectFilter() {
		reportType = ReportType.AuthorizationUserObject;
		rights = 0;
		includePassive = false;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String roleId) {
		this.userId = roleId;
	}

	public Byte getRights() {
		return rights;
	}

	public void setRights(Byte rights) {
		this.rights = rights;
	}

	public Boolean getIncludePassive() {
		return includePassive;
	}

	public void setIncludePassive(Boolean includePassive) {
		this.includePassive = includePassive;
	}

}

package net.intellica.icc.services.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.Arrays;

public enum OperationType {

	Job,
	Rule;

	@JsonCreator
	public static OperationType fromString(String string) {
		return Arrays.asList(OperationType.values()).stream().filter(s -> s.toString().equals(string)).findFirst().orElse(null);
	}

}

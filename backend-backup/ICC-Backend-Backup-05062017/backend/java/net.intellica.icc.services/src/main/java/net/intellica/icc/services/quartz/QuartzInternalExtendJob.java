package net.intellica.icc.services.quartz;

import java.util.Date;
import java.util.List;
import net.intellica.icc.services.controller.OperationController;
import net.intellica.icc.services.model.operation.Operation;
import net.intellica.icc.services.model.operation.OperationType;
import net.intellica.icc.services.service.OperationService;
import net.intellica.icc.services.util.dev.DateTimeUtil;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class QuartzInternalExtendJob implements Job {

	@Autowired private OperationController operationController;
	@Autowired private OperationService operationService;
	@Autowired private QuartzManager manager;
	@Autowired private DateTimeUtil dateTimeUtil;
	@Autowired private SimpleExceptionHandler exceptionHandler;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			checkAndUpdate(OperationType.Job);
			checkAndUpdate(OperationType.Rule);
		}
		catch (Exception e) {
			exceptionHandler.handleQuietly(e, null);
		}
	}

	public void checkAndUpdate(OperationType type) throws Exception {
		List<String> triggerIds = operationService.listDistinctScheduleJobTriggers(type);
		for (String triggerId : triggerIds) {
			Date lastDate = operationService.getLastScheduleTriggerRunDate(triggerId, type);
			if (manager.triggerExists(triggerId, type) && dateTimeUtil.needsExtension(lastDate, 1)) {
				Operation operation = operationService.get(triggerId, type, lastDate);
				List<Date> dates = manager.generateRunDates(triggerId, type, lastDate, 2);
				dates.removeIf(d -> d.equals(lastDate));
				if (dates.size() > 0)
					operationController.saveExtension(operation.getModelId(), operation.getModelName(), operation.getSchedule(), triggerId, type, dates);
			}
		}
	}

}

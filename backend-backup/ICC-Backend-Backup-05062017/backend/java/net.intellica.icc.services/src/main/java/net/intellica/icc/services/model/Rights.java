package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;
import net.intellica.icc.services.util.other.ServiceConstants;
import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "ICC_RIGHTS")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Rights {

	public static final Byte READ_RIGHT = 0b1;
	public static final Byte WRITE_RIGHT = 0b10;
	public static final Byte EXECUTE_RIGHT = 0b100;

	private String id;
	private String owner;
	private Map<String, Byte> roleRights;
	private Map<String, Byte> userRights;
	private Boolean inheritsFromParent;

	public Rights() {
		roleRights = new HashMap<>();
		userRights = new HashMap<>();
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "OWNER_ID", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "ICC_RIGHTS_ROLE")
	@MapKeyColumn(name = "ROLE_ID")
	@Column(name = "RIGHTS")
	public Map<String, Byte> getRoleRights() {
		return roleRights;
	}

	public void setRoleRights(Map<String, Byte> roleRights) {
		this.roleRights = roleRights;
	}

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "ICC_RIGHTS_USER")
	@MapKeyColumn(name = "USER_ID")
	@Column(name = "RIGHTS")
	public Map<String, Byte> getUserRights() {
		return userRights;
	}

	public void setUserRights(Map<String, Byte> userRights) {
		this.userRights = userRights;
	}

	@Column(name = "INHERIT", nullable = false)
	public Boolean getInheritsFromParent() {
		return inheritsFromParent;
	}

	public void setInheritsFromParent(Boolean inheritsFromParent) {
		this.inheritsFromParent = inheritsFromParent;
	}

}

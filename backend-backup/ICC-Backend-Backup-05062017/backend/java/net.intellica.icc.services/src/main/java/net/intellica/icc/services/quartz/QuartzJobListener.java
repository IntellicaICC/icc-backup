package net.intellica.icc.services.quartz;

import java.util.Map;
import net.intellica.icc.services.controller.OperationLogController;
import net.intellica.icc.services.event.EventType;
import net.intellica.icc.services.model.Job;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.model.operation.Command;
import net.intellica.icc.services.model.operation.CommandLog;
import net.intellica.icc.services.model.operation.Operation;
import net.intellica.icc.services.model.operation.OperationStatus;
import net.intellica.icc.services.model.operation.OperationSummary;
import net.intellica.icc.services.model.operation.OperationType;
import net.intellica.icc.services.model.operation.SkipReason;
import net.intellica.icc.services.service.CommandLogService;
import net.intellica.icc.services.service.JobService;
import net.intellica.icc.services.service.OperationService;
import net.intellica.icc.services.service.UserService;
import net.intellica.icc.services.util.handler.EventHandler;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import net.intellica.icc.services.util.other.OperationUtil;
import net.intellica.icc.template.model.GlobalVariable;
import net.intellica.icc.template.model.WorkItem;
import net.intellica.icc.template.model.WorkItemResult;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class QuartzJobListener implements JobListener {

	@Autowired private OperationService operationService;
	@Autowired private JobService jobService;
	@Autowired private UserService userService;
	@Autowired private EventHandler eventHandler;
	@Autowired private CommandLogService commandLogService;
	@Autowired private SimpleExceptionHandler simpleExceptionHandler;
	@Autowired private QuartzRuntimeUtil quartzRuntimeUtil;
	@Autowired private OperationUtil operationUtil;
	@Autowired private OperationLogController operationLogController;

	public QuartzJobListener() {
	}

	@Override
	public String getName() {
		return "QuartzJobListener";
	}

	@Override
	public void jobToBeExecuted(JobExecutionContext context) {
		Map<String, String> parameters = (Map<String, String>)context.get(QuartzParameter.PARAMETERS);
//		CrossThreadUtil.put(GlobalVariable.UserID.toString(), parameters.get(GlobalVariable.UserID.toString()));
//		CrossThreadUtil.put(GlobalVariable.Username.toString(), parameters.get(GlobalVariable.Username.toString()));
		String jobId = parameters.get(GlobalVariable.JobID.toString());
		Job job = jobService.get(jobId, false);
		WorkItem work = quartzRuntimeUtil.fillParameters(parameters, null, job, OperationType.Job, null, null, null);
		context.put(QuartzParameter.WORK, work);
		String instanceId = work.getParameters().get(GlobalVariable.JobInstanceID.toString());
		((QuartzJob)context.getJobInstance()).setInstanceId(instanceId);
		String operationId = work.getParameters().get(GlobalVariable.OperationID.toString());
		markOperationStart(operationId, instanceId);
		User eventUser = userService.get(work.getParameters().get(GlobalVariable.UserID.toString()), false);
		eventHandler.generateEvent(EventType.Start, job, operationId, eventUser, eventUser);
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext context) {
		if (context.getTrigger().getJobDataMap().containsKey(QuartzParameter.SKIP_REASON)) {
			Map<String, String> parameters = (Map<String, String>)context.get(QuartzParameter.PARAMETERS);
			User eventUser = userService.get(parameters.get(GlobalVariable.UserID.toString()), false);
			String operationId = parameters.get(GlobalVariable.OperationID.toString());
			Operation operation = operationService.get(operationId, false);
			SkipReason reason = (SkipReason)context.getTrigger().getJobDataMap().get(QuartzParameter.SKIP_REASON);
			switch (reason) {
				case Collision:
					operation.getDetail().setErrorDetail("Operation skipped execution since it is already executing!");
					eventHandler.skipDueToCollision(operation, eventUser);
					break;
				case Inactive:
					operation.getDetail().setErrorDetail("Operation skipped execution since it is not active!");
					eventHandler.skipPassive(operation, eventUser);
					break;
				case Misfire:
					operation.getDetail().setErrorDetail("Operation missed its scheduled run time!");
					eventHandler.skipMissed(operation, eventUser);
					break;
				case Unauthorized:
					operation.getDetail().setErrorDetail("Operation skipped execution since user lost rights!");
					eventHandler.skipUnauthorized(operation, eventUser);
					break;
				case User:
					CommandLog skipCommand = commandLogService.getLast(operationId, Command.Skip);
					if (skipCommand != null) {
						User skipUser = userService.get(skipCommand.getUserId(), false);
						eventHandler.skipManual(operation, eventUser, skipUser);
					}
					break;
				default:
					break;
			}
			operation.setSummary(OperationSummary.Error);
			operationService.save(operation);
			operationLogController.save(operationId, OperationStatus.Skipped);
		}
	}

	@Override
	public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
		WorkItem work = (WorkItem)context.get(QuartzParameter.WORK);
		String operationId = work.getParameters().get(GlobalVariable.OperationID.toString());
		User user = userService.get(work.getParameters().get(GlobalVariable.UserID.toString()), false);
		Job job = jobService.get(work.getParameters().get(GlobalVariable.JobID.toString()), false);
		if (jobException != null) {
			// engine/quartz hatasi
			simpleExceptionHandler.handleQuietly(jobException, user);
			WorkItemResult workResult = new WorkItemResult();
			workResult.setError(jobException);
			markOperationError(operationId, workResult);
			eventHandler.generateEvent(jobException, job, operationId, null, user);
		}
		else {
			WorkItemResult workResult = (WorkItemResult)context.getResult();
			if (workResult.isCanceled()) {
				markOperationCancel(operationId, workResult);
				CommandLog cancelCommand = commandLogService.getLast(operationId, Command.Cancel);
				User cancelUser = userService.get(cancelCommand.getUserId(), false);
				eventHandler.generateEvent(EventType.Cancel, job, operationId, cancelUser, user);
			}
			else if (workResult.isSuccessful()) {
				markOperationSuccess(operationId, workResult);
				eventHandler.generateEvent(EventType.Finish, job, operationId, null, user);
			}
			else {
				// template job hatasi
				markOperationError(operationId, workResult);
				eventHandler.generateEvent(workResult.getError(), job, operationId, null, user);
			}
		}
	}

	private void markOperationStart(String operationId, String instanceId) {
		Operation operation = operationService.get(operationId, false);
		operation.setInstanceId(instanceId);
		operationService.save(operation);
		operationLogController.save(operationId, OperationStatus.Executing);
	}

	private void markOperationSuccess(String operationId, WorkItemResult workResult) {
		operationUtil.save(operationId, workResult);
		operationLogController.save(operationId, OperationStatus.Finished);
	}

	private void markOperationCancel(String operationId, WorkItemResult workResult) {
		operationUtil.save(operationId, workResult);
		operationLogController.save(operationId, OperationStatus.Canceled);
	}

	private void markOperationError(String operationId, WorkItemResult workResult) {
		operationUtil.save(operationId, workResult);
		operationLogController.save(operationId, OperationStatus.Error);
	}

}

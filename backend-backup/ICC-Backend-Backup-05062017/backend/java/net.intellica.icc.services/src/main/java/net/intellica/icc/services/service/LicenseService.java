package net.intellica.icc.services.service;

import java.util.Iterator;
import net.intellica.icc.services.model.License;
import net.intellica.icc.services.repo.LicenseRepository;
import net.intellica.icc.services.util.aop.PreSaveUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LicenseService {

	@Autowired private LicenseRepository repository;
	@Autowired private PreSaveUtil preSaveUtil;

	public LicenseService() {
	}

	public License get() {
		License result = null;
		Iterator<License> iterator = repository.findAll().iterator();
		if (iterator.hasNext())
			result = iterator.next();
		return result;
	}

	public License save(License license) {
		repository.deleteAll();
		preSaveUtil.fill(license);
		return repository.save(license);
	}

	public void delete() {
		repository.deleteAll();
	}

}

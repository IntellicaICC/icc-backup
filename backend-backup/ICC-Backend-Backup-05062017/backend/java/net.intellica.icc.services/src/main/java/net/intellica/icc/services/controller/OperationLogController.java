package net.intellica.icc.services.controller;

import java.util.Date;
import java.util.List;
import net.intellica.icc.services.model.operation.OperationLog;
import net.intellica.icc.services.model.operation.OperationStatus;
import net.intellica.icc.services.service.OperationLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OperationLogController {

	@Autowired private OperationLogService operationLogService;

	public void save(String operationId, OperationStatus status) {
		OperationLog log = new OperationLog();
		log.setOperationId(operationId);
		log.setOperationDate(new Date());
		log.setStatus(status);
		operationLogService.save(log);
	}

	public void deleteByOperation(String operationId) {
		List<OperationLog> toDelete = operationLogService.list(operationId);
		operationLogService.deleteAll(toDelete);
	}

}

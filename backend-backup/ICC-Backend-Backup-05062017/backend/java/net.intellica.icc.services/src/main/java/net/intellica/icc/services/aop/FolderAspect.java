package net.intellica.icc.services.aop;

import java.util.Collection;
import net.intellica.icc.services.model.Folder;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.security.AuthorizationUtil;
import net.intellica.icc.services.service.FolderService;
import net.intellica.icc.services.util.aop.FillUtil;
import net.intellica.icc.services.util.aop.PreSaveUtil;
import net.intellica.icc.services.util.aop.SimplifyUtil;
import net.intellica.icc.services.util.aop.ValidationUtil;
import net.intellica.icc.services.util.handler.ResponseHandler;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import net.intellica.icc.template.exception.NotFoundException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Aspect
@Component
public class FolderAspect {

	@Autowired private ApplicationContext context;
	@Autowired private AuthorizationUtil authorizationUtil;
	@Autowired private ValidationUtil validationUtil;
	@Autowired private ResponseHandler responseHandler;
	@Autowired private SimpleExceptionHandler exceptionHandler;
	@Autowired private PreSaveUtil preSaveUtil;
	@Autowired private SimplifyUtil simplifyUtil;
	@Autowired private FillUtil fillUtil;
	@Autowired private FolderService folderService;

	@Around("execution(* net.intellica.icc.services.controller.FolderController.get(..))")
	public Object getFolder(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			validationUtil.validateId((String)pjp.getArgs()[0]);
			result = pjp.proceed();
			if (result == null)
				throw new NotFoundException();
			else {
				Folder folder = (Folder)result;
				authorizationUtil.canViewFolder(folder, true);
				fillUtil.fillFolderFields(folder);
				if (authorizationUtil.isNormalUser())
					folder.setRights(simplifyUtil.simplify(folder.getRights()));
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.FolderController.getParents(..))")
	public Object getParentFolders(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String id = (String)pjp.getArgs()[0];
			validationUtil.validateId(id);
			Folder folder = folderService.get(id, false);
			if (folder == null)
				throw new NotFoundException();
			else {
				authorizationUtil.canViewFolder(folder, true);
				result = pjp.proceed();
				Collection<Folder> collection = (Collection<Folder>)result;
				for (Folder parent : collection)
					fillUtil.setChildFolderField(parent, true);
				if (authorizationUtil.isNormalUser()) {
					for (Folder parent : collection)
						parent.setRights(simplifyUtil.simplify(parent.getRights()));
				}
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.FolderController.explore(..))")
	public Object exploreFolders(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String id = (String)pjp.getArgs()[0];
			Folder folder = null;
			if (!StringUtils.isEmpty(id)) {
				validationUtil.validateId(id);
				folder = folderService.get(id, false);
				if (folder == null)
					throw new NotFoundException();
			}
			result = pjp.proceed();
			Collection<Folder> collection = (Collection<Folder>)result;
			if (authorizationUtil.isNormalUser()) {
				Collection<Folder> refined = authorizationUtil.refineFolders(collection);
				fillUtil.fillFolderFields(refined);
				refined.forEach(f -> f.setRights(simplifyUtil.simplify(f.getRights())));
				result = refined;
			}
			else if (authorizationUtil.isAdminUser())
				fillUtil.fillFolderFields(collection);
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.FolderController.delete(..))")
	public Object deleteFolder(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			if (authorizationUtil.isOperatorUser())
				authorizationUtil.throwDeleteException();
			validationUtil.validateId((String)pjp.getArgs()[0]);
			String id = (String)pjp.getArgs()[0];
			Folder toDelete = folderService.get(id, false);
			if (toDelete == null)
				throw new NotFoundException();
			else {
				validationUtil.validateDelete(toDelete);
				if (authorizationUtil.isNormalUser())
					authorizationUtil.canDeleteFolder(toDelete, true);
				result = pjp.proceed();
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.FolderController.save(..))")
	public Object saveFolder(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			Folder folder = (Folder)pjp.getArgs()[0];
			Folder dbFolder = StringUtils.isEmpty(folder.getId()) ? null : folderService.get(folder.getId(), false);
			authorizationUtil.canSaveFolder(folder, dbFolder, true);
			validationUtil.validateSave(folder);
			preSaveUtil.fill(folder, dbFolder);
			result = pjp.proceed();
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

}

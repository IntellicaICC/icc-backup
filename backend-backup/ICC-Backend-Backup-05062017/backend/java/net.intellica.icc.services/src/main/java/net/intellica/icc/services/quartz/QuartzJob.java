package net.intellica.icc.services.quartz;

import net.intellica.icc.services.util.other.WorkManager;
import net.intellica.icc.services.util.thread.ContextUtil;
import net.intellica.icc.template.model.WorkItem;
import net.intellica.icc.template.model.WorkItemResult;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class QuartzJob implements Job {

	@Autowired private ContextUtil contextUtil;
	private String instanceId;
	private WorkManager workManager;

	public QuartzJob() {
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		workManager = new WorkManager(contextUtil, (WorkItem)context.get(QuartzParameter.WORK));
		WorkItemResult workResult = workManager.runSync();
		context.setResult(workResult);
	}

	public void cancel() {
		workManager.cancel();
	}

}

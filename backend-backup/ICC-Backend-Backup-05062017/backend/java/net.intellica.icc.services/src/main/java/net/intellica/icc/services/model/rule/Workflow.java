package net.intellica.icc.services.model.rule;

import java.io.Closeable;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import net.intellica.icc.services.config.ServicesConfig;
import net.intellica.icc.services.model.operation.OperationLiveStatus;
import net.intellica.icc.services.model.operation.OperationRuleLog;
import net.intellica.icc.services.model.work.WorkflowResult;
import net.intellica.icc.services.util.dev.TimeUtil;
import net.intellica.icc.services.util.thread.ContextUtil;
import net.intellica.icc.services.util.thread.ThreadUtil;
import net.intellica.icc.template.model.QueryLog;

public class Workflow implements Closeable {

	private static final Integer CONTROL_PERIOD_MS = 1000;
	private final Integer timeout;

	private ContextUtil contextUtil;

	private ExecutorService executor;
	private final Map<Node, AtomicInteger> chokeMap;
	private Map<String, String> globalVariables;
	private Map<String, Object> startInput;
	private Map<String, Object> endOutput;
	private List<QueryLog> queryLogs;
	private List<OperationRuleLog> ruleLogs;
	private Map<NodeStatus, Set<Integer>> nodeStates;
	private List<String> instanceChain;
	private Boolean paused;
	private Boolean canceled;
	private Boolean hasError;
	private AtomicReference<Instant> lastStatusChange;
	private Graph graph;
	private WorkflowResult workflowResult;
	private String lastDataset;

	public Workflow(ContextUtil contextUtil) {
		this.contextUtil = contextUtil;
		timeout = contextUtil.call(ServicesConfig.class).getRule_timeout();
		executor = Executors.newCachedThreadPool();
		chokeMap = new ConcurrentHashMap<>();
		globalVariables = new ConcurrentHashMap<>();
		startInput = new ConcurrentHashMap<>();
		endOutput = new ConcurrentHashMap<>();
		queryLogs = new CopyOnWriteArrayList<>();
		ruleLogs = new CopyOnWriteArrayList<>();
		nodeStates = new ConcurrentHashMap<>();
		Arrays.asList(NodeStatus.values()).forEach(s -> nodeStates.put(s, ConcurrentHashMap.newKeySet()));
		paused = false;
		canceled = false;
		hasError = false;
		lastStatusChange = new AtomicReference<>(Instant.now());
		instanceChain = new ArrayList<>();
	}

	private ExecutorService getExecutor() {
		return executor;
	}

	private Map<Node, AtomicInteger> getChokeMap() {
		return chokeMap;
	}

	Map<String, String> getGlobalVariables() {
		return globalVariables;
	}

	Map<String, Object> getStartInput() {
		return startInput;
	}

	Map<String, Object> getEndOutput() {
		return endOutput;
	}

	public Graph getGraph() {
		return graph;
	}

	public void setGraph(Graph graph) {
		this.graph = graph;
		graph.getNodes().forEach(n -> n.setWorkflow(this));
		graph.getNodes().forEach(n -> nodeStates.get(NodeStatus.Idle).add(n.getId()));
	}

	synchronized void addQueryLogs(List<QueryLog> logs) {
		queryLogs.addAll(logs);
	}

	synchronized void addRuleLogs(List<OperationRuleLog> logs) {
		ruleLogs.addAll(logs);
	}

	ContextUtil getContextUtil() {
		return contextUtil;
	}

	/**
	 * Determine overall rule status based on nodes
	 * @return
	 */
	public WorkflowStatus getStatus() {
		WorkflowStatus result = WorkflowStatus.Idle;
		Integer executingCount = nodeStates.get(NodeStatus.Executing).size();
		Integer waitingCount = nodeStates.get(NodeStatus.Waiting).size();
		Boolean started = !nodeStates.get(NodeStatus.Idle).contains(graph.getStartNode().getId());
		if (started)
			result = WorkflowStatus.Executing;
		Set<Integer> endNodes = new HashSet<>();
		graph.getNodes().stream().filter(n -> n instanceof EndNode).map(n -> n.getId()).forEach(id -> endNodes.add(id));
		Set<Integer> finishedEndNodes = new HashSet<>();
		endNodes.stream().filter(en -> nodeStates.get(NodeStatus.Finished).contains(en)).forEach(en -> finishedEndNodes.add(en));
		Boolean allEndNodesFinished = endNodes.equals(finishedEndNodes);
		if (allEndNodesFinished)
			result = WorkflowStatus.Finished;
		else {
			Integer pathCount = ((ThreadPoolExecutor)getExecutor()).getActiveCount();
			if (paused) {
				result = WorkflowStatus.Pausing;
				if (executingCount == 0 && waitingCount == pathCount)
					result = WorkflowStatus.Paused;
			}
			else if (canceled) {
				result = WorkflowStatus.Canceling;
				if (executingCount == 0)
					result = WorkflowStatus.Canceled;
			}
			else if (hasError)
				result = WorkflowStatus.Error;
			else {
				if (executingCount == 0) {
					if (endNodes.size() == 1) {
						if (nodeStates.get(NodeStatus.Finished).contains(endNodes.iterator().next()))
							result = WorkflowStatus.Finished;
					}
					else if (finishedEndNodes.size() > 0) {
						if (pathCount == 0)
							result = WorkflowStatus.Finished;
					}
				}
			}
		}
		System.out.println(simpleToString() + ThreadUtil.getThreadId() + " " + TimeUtil.getDateTime() + " Status=" + result + ", S=" + nodeStates);
		return result;
	}

	public WorkflowResult getWorkflowResult() {
		if (getStatus() == WorkflowStatus.Finished && workflowResult == null)
			createResult();
		return workflowResult;
	}

	private void createResult() {
		workflowResult = new WorkflowResult();
		workflowResult.getResults().putAll(endOutput);
		workflowResult.getQueryLogs().addAll(queryLogs);
		workflowResult.getRuleLogs().addAll(ruleLogs);
	}

	public Boolean isPaused() {
		return paused;
	}

	public Boolean isCanceled() {
		return canceled;
	}

	public Boolean hasError() {
		return hasError;
	}

	public Boolean isFinished() {
		return isCanceled() || hasError() || WorkflowStatus.Finished == getStatus();
	}

	public WorkflowResult startSync(Map<String, String> globalVariables) {
		System.out.println(simpleToString() + ThreadUtil.getThreadId() + " " + TimeUtil.getDateTime() + " Started workflow sync-run.");
		getGlobalVariables().putAll(globalVariables);
		graph.getStartNode().getInput().putAll(getStartInput());
		graph.getStartNode().setLastDataset(getLastDataset());
		execute(graph.getStartNode());
		for (WorkflowStatus s = getStatus(); s != WorkflowStatus.Finished; s = getStatus()) {
			ThreadUtil.waitFor(CONTROL_PERIOD_MS);
			if (s == WorkflowStatus.Canceled || s == WorkflowStatus.Error)
				break;
			checkTimeout();
		}
		if (getStatus() == WorkflowStatus.Finished)
			createResult();
		System.out.println(simpleToString() + ThreadUtil.getThreadId() + " " + TimeUtil.getDateTime() + " Finished workflow sync-run.");
		return workflowResult;
	}

	private void checkTimeout() {
		Duration timeWithoutAction = Duration.between(lastStatusChange.get(), Instant.now());
		if (timeWithoutAction.getSeconds() >= timeout)
			error(new TimeoutException("Workflow timeout!"));
	}

	public void pause() {
		if (!paused) {
			// TODO workflow pause
			System.out.println(simpleToString() + "Workflow is paused!");
			graph.getNodes().forEach(Node::pause);
			paused = true;
		}
	}

	public void resume() {
		if (paused) {
			// TODO workflow resume
			System.out.println(simpleToString() + "Workflow is resumed!");// TODO
			graph.getNodes().forEach(Node::resume);
			paused = false;
		}
	}

	public synchronized void cancel() {
		if (!canceled) {
			System.out.println(simpleToString() + "Workflow is canceled!");
			workflowResult = new WorkflowResult();
			workflowResult.setCanceled(true);
			graph.getNodes().forEach(Node::cancel);
			canceled = true;
		}
	}

	synchronized void error(Exception e) {
		if (!hasError && !canceled) {
			System.out.println(simpleToString() + "Workflow got an error!");
			workflowResult = new WorkflowResult();
			workflowResult.setError(e);
			graph.getNodes().forEach(Node::cancel);
			hasError = true;
		}
	}

	void execute(Node source, List<Node> targets) {
		for (Node target : targets) {
			// tum parametreleri kopyala
			target.getInput().putAll(source.getOutput());
			target.setLastDataset(source.getLastDataset());
			execute(target);
		}
	}

	private void execute(Node node) {
		if (node.getIncoming().size() > 1) {
			AtomicInteger countdown = getChokeMap().get(node);
			if (countdown == null) {
				countdown = new AtomicInteger(node.getIncoming().size() - 1);
				getChokeMap().put(node, countdown);
			}
			else {
				Integer count = countdown.decrementAndGet();
				if (count == 0) {
					// tum incoming nodelar tamamlandi
					getChokeMap().remove(countdown);
					doExecuteNode(node);
				}
				else
					node.markForExecution();
			}
		}
		else
			doExecuteNode(node);
	}

	private void doExecuteNode(Node node) {
		getExecutor().submit(node);
	}

	void bypass(Node source, List<Node> targets) {
		for (Node target : targets) {
			// tum parametreleri kopyala
			target.getInput().putAll(source.getOutput());
			bypass(target);
		}
	}

	private void bypass(Node node) {
		if (node.getIncoming().size() > 1) {
			AtomicInteger countdown = getChokeMap().get(node);
			if (countdown == null) {
				countdown = new AtomicInteger(node.getIncoming().size() - 1);
				getChokeMap().put(node, countdown);
			}
			else {
				Integer count = countdown.decrementAndGet();
				if (count == 0) {
					// tum incoming nodelar tamamlandi
					getChokeMap().remove(countdown);
					if (node.isToBeExecuted())
						doExecuteNode(node);
					else
						doBypassNode(node);
				}
			}
		}
		else
			doBypassNode(node);
	}

	private void doBypassNode(Node node) {
		getExecutor().submit(() -> node.bypass());
	}

	synchronized void setNodeStatus(Node node, NodeStatus nodeStatus) {
		nodeStates.get(node.getStatus()).remove(node.getId());
		nodeStates.get(nodeStatus).add(node.getId());
		lastStatusChange.set(Instant.now());
		System.out.println(simpleToString() + ThreadUtil.getThreadId() + " " + TimeUtil.getDateTime() + " Node status changed! " + node + ", new status=" + nodeStatus);
	}

	@Override
	public void close() throws IOException {
		getExecutor().shutdown();
	}

	private String simpleToString() {
		return toString().substring(toString().lastIndexOf(".") + 1) + " ";
	}

	public OperationLiveStatus getLiveStatus() {
		OperationLiveStatus result = new OperationLiveStatus();
		graph.getNodes().forEach(n -> result.getStates().put(n.getId(), n.getStatus()));
		for (Integer id : nodeStates.get(NodeStatus.Error)) {
			Exception nodeError = graph.getNode(id).getNodeError();
			String errorText = nodeError != null ? nodeError.getMessage() : "Error!";
			result.getErrors().put(id, errorText);
		}
		return result;
	}

	public List<String> getInstanceChain() {
		return instanceChain;
	}

	public String getLastDataset() {
		return lastDataset;
	}

	public void setLastDataset(String lastDataset) {
		this.lastDataset = lastDataset;
	}

}

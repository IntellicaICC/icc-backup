package net.intellica.icc.services.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.intellica.icc.services.model.operation.OperationLog;
import net.intellica.icc.services.model.operation.OperationStatus;
import net.intellica.icc.services.repo.OperationLogRepository;
import net.intellica.icc.services.util.aop.PreSaveUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OperationLogService {

	@Autowired private OperationLogRepository repository;
	@Autowired private PreSaveUtil preSaveUtil;

	public OperationLogService() {
	}

	public OperationLog get(String id) {
		return repository.findOne(id);
	}

	public OperationLog getStart(String operationId) {
		return repository.findFirstByOperationIdAndStatusOrderByOperationDateAsc(operationId, OperationStatus.Executing);
	}

	public OperationLog getLast(String operationId) {
		return repository.findFirstByOperationIdOrderByOperationDateDesc(operationId);
	}

	public OperationLog save(OperationLog operationLog) {
		preSaveUtil.fill(operationLog);
		return repository.save(operationLog);
	}

	public List<OperationLog> saveAll(List<OperationLog> logs) {
		List<OperationLog> result = new ArrayList<>();
		preSaveUtil.fillAllOperationLogs(logs);
		repository.save(logs).forEach(result::add);
		return result;
	}

	public List<OperationLog> list(String operationId) {
		return repository.findAllByOperationIdOrderByOperationDateDesc(operationId);
	}

	public void deleteAll(Collection<OperationLog> logs) {
		repository.delete(logs);
	}

}

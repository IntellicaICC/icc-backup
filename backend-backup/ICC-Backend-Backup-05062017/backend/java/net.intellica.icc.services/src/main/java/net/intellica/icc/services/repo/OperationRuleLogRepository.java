package net.intellica.icc.services.repo;

import java.util.List;
import net.intellica.icc.services.model.operation.OperationRuleLog;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OperationRuleLogRepository extends PagingAndSortingRepository<OperationRuleLog, String> {

	public List<OperationRuleLog> findAllByOperationIdOrderByRunDateDesc(String operationId);

}

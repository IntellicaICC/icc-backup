package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import net.intellica.icc.services.util.other.ServiceConstants;
import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "ICC_ROLE", uniqueConstraints = @UniqueConstraint(columnNames = "NAME") )
@JsonIgnoreProperties(ignoreUnknown = true)
public class Role {

	private String id;
	private CommonProperties props;
	private Set<User> users;

	public Role() {
		props = new CommonProperties();
		users = new HashSet<>();
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Embedded
	public CommonProperties getProps() {
		return props;
	}

	public void setProps(CommonProperties props) {
		this.props = props;
	}

	//@formatter:off
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
		name = "ICC_ROLEUSER", 
		joinColumns = { @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID") }, 
		inverseJoinColumns = { @JoinColumn(name = "USER_ID", referencedColumnName = "ID") }
	)
	//@formatter:on
	@JsonIgnore
	public Set<User> getUsers() {
		return users;
	}

	@JsonProperty
	public void setUsers(Set<User> users) {
		this.users = users;
	}

}

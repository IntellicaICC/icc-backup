package net.intellica.icc.services.quartz;

import java.util.Map;
import net.intellica.icc.services.model.rule.Graph;
import net.intellica.icc.services.model.rule.Workflow;
import net.intellica.icc.services.model.work.WorkflowResult;
import net.intellica.icc.services.util.thread.ContextUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class QuartzRule implements Job {

	@Autowired private ContextUtil contextUtil;
	@Autowired private QuartzRuntimeUtil quartzRuntimeUtil;
	private String instanceId;
	private Workflow workflow;

	public QuartzRule() {
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public Workflow getWorkflow() {
		return workflow;
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		WorkflowResult result = new WorkflowResult();
		Map<String, String> parameters = (Map<String, String>)context.get(QuartzParameter.PARAMETERS);
		try {
			Graph graph = Graph.fromString((String)context.get(QuartzParameter.RULE_DETAIL));
			try (Workflow workflow = new Workflow(contextUtil)) {
				this.workflow = workflow;
				workflow.setGraph(graph);
				workflow.getInstanceChain().add(instanceId);
				workflow.startSync(parameters);
				result = workflow.getWorkflowResult();
			}
		}
		catch (Exception e) {
			result.setError(e);
		}
		finally {
			result.setFinalStatus(workflow.getLiveStatus());
			quartzRuntimeUtil.clearCache(instanceId);
		}
		context.setResult(result);
	}

	public void cancel() {
		workflow.cancel();
	}

}

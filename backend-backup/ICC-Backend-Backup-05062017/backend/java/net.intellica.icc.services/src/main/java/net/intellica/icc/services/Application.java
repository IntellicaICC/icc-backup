package net.intellica.icc.services;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import net.intellica.icc.services.config.ServicesConfig;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.security.DetailsService;
import net.intellica.icc.services.service.UserService;
import net.intellica.icc.services.util.handler.OtherExceptionHandler;
import net.intellica.icc.services.util.other.ServiceConstants;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Scope;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@SpringBootApplication
@EnableTransactionManagement
@EnableWebSecurity
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class Application extends SpringBootServletInitializer {

	@Autowired private ServicesConfig servicesConfig;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(Application.class);
	}

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
//		Arrays.asList(context.getBeanDefinitionNames()).forEach(System.out::println);
		context.registerShutdownHook();
	}

	@Bean
	public PlatformTransactionManager transactionManager(LocalContainerEntityManagerFactoryBean factory) {
		JpaTransactionManager result = new JpaTransactionManager();
		result.setEntityManagerFactory(factory.getObject());
		return result;
	}

	@Bean
	public UserDetailsService detailsService() {
		return new DetailsService();
	}

	@Bean
	public CorsFilter corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("GET");
		config.addAllowedMethod("POST");
		config.addAllowedMethod("HEAD");
		config.addAllowedMethod("DELETE");
		config.addAllowedMethod("OPTIONS");
		source.registerCorsConfiguration("/**", config);
		return new CorsFilter(source);
	}

	@Bean
	public ScheduledExecutorService executor() {
		return Executors.newScheduledThreadPool(servicesConfig.getQuartz_thread_pool_count());
	}

	@Bean
	public Scheduler scheduler() throws SchedulerException {
		StdSchedulerFactory factory = new StdSchedulerFactory("quartz.properties");
		return factory.getScheduler();
	}

	@Bean
	public ErrorAttributes errorAttributes() {
		return new OtherExceptionHandler();
	}

	@Bean
	@Scope(WebApplicationContext.SCOPE_REQUEST)
	public User currentUser(UserService userService) {
		User result = userService.getCurrentUser();
		if (result == null) {
			result = new User();
			result.getProps().setName(ServiceConstants.User.UNKNOWN_USER);
		}
		return result;
	}

}

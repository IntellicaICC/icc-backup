package net.intellica.icc.services.model.rule;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import net.intellica.icc.services.model.Rule;
import net.intellica.icc.services.model.operation.OperationRuleLog;
import net.intellica.icc.services.model.work.WorkflowResult;
import net.intellica.icc.services.quartz.QuartzCompileTimeUtil;
import net.intellica.icc.services.service.RuleService;
import net.intellica.icc.services.util.dev.HashUtil;
import net.intellica.icc.services.util.other.IdGenerator;
import net.intellica.icc.template.exception.InactiveException;
import net.intellica.icc.template.model.GlobalVariable;
import org.springframework.util.ObjectUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RuleNode extends Node {

	private static final long serialVersionUID = 1L;

	private String ruleId;
	private String name;
	private String description;

	private Workflow subWorkflow;

	public RuleNode() {
		super();
		type = NodeType.Rule;
	}

	public String getRuleId() {
		return ruleId;
	}

	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonIgnore
	public Workflow getSubWorkflow() {
		return subWorkflow;
	}

	public void setSubWorkflow(Workflow subWorkflow) {
		this.subWorkflow = subWorkflow;
	}

	@Override
	protected void doWork() {
		WorkflowResult workResult = new WorkflowResult();
		try {
			Map<String, String> parameters = new HashMap<>();
			Rule rule = getWorkflow().getContextUtil().call(RuleService.class).get(ruleId, false);
			workResult.setRuleDetail(rule.getDetail());
			if (!rule.getProps().isActive())
				throw new InactiveException(String.format("Rule '%s' is inactive!", rule.getProps().getName()));
			String triggerId = getWorkflow().getGlobalVariables().get(GlobalVariable.RuleTriggerID.toString());
			String userId = getWorkflow().getGlobalVariables().get(GlobalVariable.UserID.toString());
			String userName = getWorkflow().getGlobalVariables().get(GlobalVariable.Username.toString());
			String parentInstanceId = getWorkflow().getGlobalVariables().get(GlobalVariable.RuleInstanceID.toString());
			String instanceId = getWorkflow().getContextUtil().call(IdGenerator.class).generateSubRuleInstanceId(parentInstanceId);
			parameters.put(GlobalVariable.RuleInstanceID.toString(), instanceId);
			getWorkflow().getContextUtil().call(QuartzCompileTimeUtil.class).fillParameters(parameters, rule, triggerId, userId, userName);
			Graph graph = Graph.fromString(rule.getDetail());
			getWorkflow().addRuleLogs(Arrays.asList(createRuleLog(parameters, rule)));
			try (Workflow workflow = new Workflow(getWorkflow().getContextUtil())) {
				subWorkflow = workflow;
				workflow.setGraph(graph);
				workflow.getInstanceChain().addAll(getWorkflow().getInstanceChain());
				workflow.getInstanceChain().add(instanceId);
				workflow.getStartInput().putAll(getInput());
				workflow.setLastDataset(getLastDataset());
				workflow.startSync(parameters);
				workResult = workflow.getWorkflowResult();
				setLastDataset(workflow.getLastDataset());
			}
		}
		catch (Exception e) {
			workResult.setError(e);
		}
		if (workResult.isSuccessful()) {
			getOutput().putAll(workResult.getResults());
			getWorkflow().addQueryLogs(workResult.getQueryLogs());
			getWorkflow().addRuleLogs(workResult.getRuleLogs());
		}
		else if (workResult.getError() != null)
			setNodeError(workResult.getError());
	}

	private OperationRuleLog createRuleLog(Map<String, String> parameters, Rule rule) {
		OperationRuleLog result = new OperationRuleLog();
		result.setOperationId(parameters.get(GlobalVariable.OperationID.toString()));
		result.setRunDate(new Date());
		result.setRuleId(rule.getId());
		result.setRuleName(rule.getProps().getName());
		result.setRuleDetail(rule.getDetail());
		return result;
	}

	@Override
	public void cancel() {
		super.cancel();
		if (getStatus() == NodeStatus.Executing && subWorkflow != null)
			subWorkflow.cancel();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj) &&
			ObjectUtils.nullSafeEquals(getRuleId(), ((RuleNode)obj).getRuleId()) &&
			ObjectUtils.nullSafeEquals(getName(), ((RuleNode)obj).getName()) &&
			ObjectUtils.nullSafeEquals(getDescription(), ((RuleNode)obj).getDescription());
	}

	@Override
	public int hashCode() {
		return HashUtil.generate(super.hashCode(), ruleId, name, description);
	}

}

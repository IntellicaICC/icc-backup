package net.intellica.icc.services.aop;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import net.intellica.icc.services.controller.ProfileController;
import net.intellica.icc.services.model.Setting;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.model.simple.SimpleUser;
import net.intellica.icc.services.security.AuthorizationUtil;
import net.intellica.icc.services.security.PasswordGenerator;
import net.intellica.icc.services.service.UserService;
import net.intellica.icc.services.util.aop.PreSaveUtil;
import net.intellica.icc.services.util.aop.SimplifyUtil;
import net.intellica.icc.services.util.aop.ValidationUtil;
import net.intellica.icc.services.util.handler.ResponseHandler;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import net.intellica.icc.services.util.other.EmailUtil;
import net.intellica.icc.template.exception.NotFoundException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Aspect
@Component
public class UserAspect {

	@Autowired private ApplicationContext context;
	@Autowired private AuthorizationUtil authorizationUtil;
	@Autowired private ValidationUtil validationUtil;
	@Autowired private ResponseHandler responseHandler;
	@Autowired private SimpleExceptionHandler exceptionHandler;
	@Autowired private PreSaveUtil preSaveUtil;
	@Autowired private SimplifyUtil simplifyUtil;
	@Autowired private UserService userService;
	@Autowired private ProfileController profileController;
	@Autowired private EmailUtil emailUtil;
	@Autowired private ScheduledExecutorService executor;// TODO sart mi?

	@Around("execution(* net.intellica.icc.services.controller.UserController.get(..))")
	public Object getUser(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			validationUtil.validateId((String)pjp.getArgs()[0]);
			result = pjp.proceed();
			if (result == null)
				throw new NotFoundException();
			else {
				if (!authorizationUtil.isAdminUser())
					result = simplifyUtil.simplify((User)result);
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.UserController.list(..))")
	public Object listUsers(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			result = pjp.proceed();
			if (result == null)
				throw new NotFoundException();
			else {
				if (!authorizationUtil.isAdminUser()) {
					Collection<User> collection = (Collection<User>)result;
					List<SimpleUser> simples = collection.stream().map(c -> simplifyUtil.simplify(c)).collect(Collectors.toList());
					result = simples;
				}
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.UserController.delete(..))")
	public Object deleteUser(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			if (!authorizationUtil.isAdminUser())
				authorizationUtil.throwDeleteException();
			validationUtil.validateId((String)pjp.getArgs()[0]);
			String id = (String)pjp.getArgs()[0];
			User toDelete = userService.get(id, false);
			if (toDelete == null)
				throw new NotFoundException();
			else {
				validationUtil.validateDelete(toDelete);
				result = pjp.proceed();
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.UserController.save(..))")
	public Object saveUser(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			if (!authorizationUtil.isAdminUser())
				authorizationUtil.throwSaveException();
			User user = (User)pjp.getArgs()[0];
			validationUtil.validateSave(user);
			User dbUser = StringUtils.isEmpty(user.getId()) ? null : userService.get(user.getId(), false);
			User currentUser = context.getBean(User.class);
			String generated = PasswordGenerator.generate();
			preSaveUtil.fill(user, dbUser, generated);
			result = pjp.proceed();
			if (dbUser == null) {
				executor.schedule(() -> checkSettings(currentUser.getId()), 250, TimeUnit.MILLISECONDS);
				executor.schedule(() -> sendEmail(user.getEmail(), user.getProps().getName(), generated), 500, TimeUnit.MILLISECONDS);
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	private void checkSettings(String userId) {
		List<Setting> settings = profileController.listSettingsOfUser(userId);
		if (settings.isEmpty())
			profileController.resetSettings(userId);
	}

	private void sendEmail(String to, String username, String password) {
		String subject = "ICC Login Information";
		String text = String.format("Username=%s\nPassword=%s", username, password);
		emailUtil.sendEmail(to, subject, text);
	}

}

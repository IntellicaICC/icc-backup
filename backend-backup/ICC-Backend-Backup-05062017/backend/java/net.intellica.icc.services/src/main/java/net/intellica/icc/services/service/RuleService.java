package net.intellica.icc.services.service;

import java.util.ArrayList;
import java.util.List;
import net.intellica.icc.services.model.Rule;
import net.intellica.icc.services.repo.RuleRepository;
import net.intellica.icc.services.util.audit.HistoryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RuleService {

	@Autowired private RuleRepository repository;
	@Autowired private HistoryUtil historyUtil;

	public RuleService() {
	}

	public Rule get(String id, Boolean includeHistory) {
		Rule result = repository.findOne(id);
		if (includeHistory && result == null)
			result = historyUtil.getLastRevision(Rule.class, id);
		return result;
	}

	public Rule save(Rule rule) {
		return repository.save(rule);
	}

	public void delete(String id) {
		repository.delete(id);
	}

	public List<Rule> listAll() {
		List<Rule> result = new ArrayList<>();
		repository.findAll().forEach(result::add);
		return result;
	}

	public List<Rule> listByName(String name) {
		return repository.findAllByPropsName(name);
	}

	public List<Rule> listByFolderId(String id) {
		return repository.findAllByFolderIdOrderByPropsNameAsc(id);
	}

	public List<Rule> listAllIds() {
		List<Rule> result = new ArrayList<>();
		repository.findDistinctIdBy().forEach(result::add);
		return result;
	}

}

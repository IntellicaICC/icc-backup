package net.intellica.icc.services.aop;

import java.util.Collection;
import net.intellica.icc.services.model.Folder;
import net.intellica.icc.services.model.LocalParameter;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.security.AuthorizationUtil;
import net.intellica.icc.services.service.FolderService;
import net.intellica.icc.services.service.LocalParameterService;
import net.intellica.icc.services.util.aop.FillUtil;
import net.intellica.icc.services.util.aop.PreSaveUtil;
import net.intellica.icc.services.util.aop.SimplifyUtil;
import net.intellica.icc.services.util.aop.ValidationUtil;
import net.intellica.icc.services.util.handler.ResponseHandler;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import net.intellica.icc.template.exception.NotFoundException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Aspect
@Component
public class LocalParameterAspect {

	@Autowired private ApplicationContext context;
	@Autowired private AuthorizationUtil authorizationUtil;
	@Autowired private ValidationUtil validationUtil;
	@Autowired private ResponseHandler responseHandler;
	@Autowired private SimpleExceptionHandler exceptionHandler;
	@Autowired private PreSaveUtil preSaveUtil;
	@Autowired private SimplifyUtil simplifyUtil;
	@Autowired private FillUtil fillUtil;
	@Autowired private FolderService folderService;
	@Autowired private LocalParameterService localParameterService;

	@Around("execution(* net.intellica.icc.services.controller.LocalParameterController.get(..))")
	public Object getLocalParameter(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			validationUtil.validateId((String)pjp.getArgs()[0]);
			result = pjp.proceed();
			if (result == null)
				throw new NotFoundException();
			else {
				LocalParameter parameter = (LocalParameter)result;
				authorizationUtil.canViewLocalParameter(parameter, true);
				fillUtil.fillLocalParameterFields(parameter);
				if (authorizationUtil.isNormalUser())
					parameter.setRights(simplifyUtil.simplify(parameter.getRights()));
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.LocalParameterController.explore(..))")
	public Object exploreLocalParameters(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String id = (String)pjp.getArgs()[0];
			Folder folder = null;
			if (!StringUtils.isEmpty(id)) {
				validationUtil.validateId(id);
				folder = folderService.get(id, false);
				if (folder == null)
					throw new NotFoundException();
			}
			result = pjp.proceed();
			Collection<LocalParameter> collection = (Collection<LocalParameter>)result;
			if (authorizationUtil.isNormalUser()) {
				Collection<LocalParameter> refined = authorizationUtil.refineParameters(collection);
				fillUtil.fillLocalParameterFields(refined);
				refined.forEach(p -> p.setRights(simplifyUtil.simplify(p.getRights())));
				result = refined;
			}
			else
				fillUtil.fillLocalParameterFields(collection);
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.LocalParameterController.delete(..))")
	public Object deleteLocalParameter(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			if (authorizationUtil.isOperatorUser())
				authorizationUtil.throwDeleteException();
			validationUtil.validateId((String)pjp.getArgs()[0]);
			String id = (String)pjp.getArgs()[0];
			LocalParameter toDelete = localParameterService.get(id, false);
			if (toDelete == null)
				throw new NotFoundException();
			else {
				validationUtil.validateDelete(toDelete);
				if (authorizationUtil.isNormalUser())
					authorizationUtil.canDeleteLocalParameter(toDelete, true);
				result = pjp.proceed();
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.LocalParameterController.save(..))")
	public Object saveLocalParameter(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			LocalParameter parameter = (LocalParameter)pjp.getArgs()[0];
			LocalParameter dbParameter = StringUtils.isEmpty(parameter.getId()) ? null : localParameterService.get(parameter.getId(), false);
			authorizationUtil.canSaveLocalParameter(parameter, dbParameter, true);
			validationUtil.validateSave(parameter);
			preSaveUtil.fill(parameter, dbParameter);
			result = pjp.proceed();
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

}

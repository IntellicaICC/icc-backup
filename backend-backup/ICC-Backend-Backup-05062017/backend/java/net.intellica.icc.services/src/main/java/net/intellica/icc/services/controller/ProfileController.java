package net.intellica.icc.services.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import net.intellica.icc.services.model.ProfileUpdate;
import net.intellica.icc.services.model.Setting;
import net.intellica.icc.services.model.SettingDefinition;
import net.intellica.icc.services.model.SettingsUpdate;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.service.SettingDefinitionService;
import net.intellica.icc.services.service.SettingService;
import net.intellica.icc.services.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/profile")
public class ProfileController {

	@Autowired private ApplicationContext context;
	@Autowired private UserService userService;
	@Autowired private SettingService settingService;
	@Autowired private SettingDefinitionService settingDefinitionService;

	@RequestMapping(value = { "", "/" }, method = RequestMethod.GET)
	public Object get() {
		return context.getBean(User.class);
	}

	@RequestMapping(value = { "/save", "/save/" }, method = RequestMethod.POST)
	public Object saveProfile(@RequestBody ProfileUpdate update) {
		User currentUser = context.getBean(User.class);
		if (!StringUtils.isEmpty(update.getNewPassword()))
			currentUser.setPassword(update.getNewPassword());
		currentUser.setEmail(update.getEmail());
		userService.save(currentUser);
		return null;
	}

	@RequestMapping(value = { "/settings", "/settings/" }, method = RequestMethod.GET)
	public Object listSettings() {
		return listSettingsOfUser(context.getBean(User.class).getId());
	}

	public List<Setting> listSettingsOfUser(String userId) {
		return settingService.list(userId);
	}

	@RequestMapping(value = { "/settings/save", "/settings/save/" }, method = RequestMethod.POST)
	public Object saveSettings(@RequestBody SettingsUpdate update) {
		List<Setting> settings = settingService.list(context.getBean(User.class).getId());
		List<Setting> changedSettings = new ArrayList<>();
		for (String id : update.getValues().keySet()) {
			Optional<Setting> optional = settings.stream().filter(s -> Objects.equals(id, s.getId())).findFirst();
			if (optional.isPresent()) {
				Setting setting = optional.get();
				setting.setValue(update.getValues().get(id));
				changedSettings.add(setting);
			} // setting yoksa ignore
		}
		settingService.saveAll(changedSettings);
		return null;
	}

	@RequestMapping(value = { "/settings/reset", "/settings/reset/" }, method = RequestMethod.GET)
	public Object resetSettings() {
		resetSettings(context.getBean(User.class).getId());
		return null;
	}

	public void resetSettings(String userId) {
		settingService.deleteAll(userId);
		List<Setting> settings = new ArrayList<>();
		List<SettingDefinition> definitions = settingDefinitionService.list();
		for (SettingDefinition definition : definitions) {
			Setting setting = new Setting();
			setting.setDefinition(definition);
			setting.setUserId(userId);
			setting.setValue(definition.getDefaultValue());
			settings.add(setting);
		}
		settingService.saveAll(settings);
	}

}

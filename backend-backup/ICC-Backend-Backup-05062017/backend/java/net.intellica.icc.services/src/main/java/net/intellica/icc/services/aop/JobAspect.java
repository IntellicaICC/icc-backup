package net.intellica.icc.services.aop;

import java.util.Collection;
import net.intellica.icc.services.model.Folder;
import net.intellica.icc.services.model.Job;
import net.intellica.icc.services.model.Schedule;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.model.operation.OperationType;
import net.intellica.icc.services.security.AuthorizationUtil;
import net.intellica.icc.services.service.FolderService;
import net.intellica.icc.services.service.JobService;
import net.intellica.icc.services.util.aop.FillUtil;
import net.intellica.icc.services.util.aop.PreSaveUtil;
import net.intellica.icc.services.util.aop.SimplifyUtil;
import net.intellica.icc.services.util.aop.ValidationUtil;
import net.intellica.icc.services.util.dev.DateTimeUtil;
import net.intellica.icc.services.util.handler.ResponseHandler;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import net.intellica.icc.template.exception.NotFoundException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Aspect
@Component
public class JobAspect {

	@Autowired private ApplicationContext context;
	@Autowired private AuthorizationUtil authorizationUtil;
	@Autowired private ValidationUtil validationUtil;
	@Autowired private ResponseHandler responseHandler;
	@Autowired private SimpleExceptionHandler exceptionHandler;
	@Autowired private PreSaveUtil preSaveUtil;
	@Autowired private SimplifyUtil simplifyUtil;
	@Autowired private FillUtil fillUtil;
	@Autowired private JobService jobService;
	@Autowired private FolderService folderService;
	@Autowired private DateTimeUtil dateTimeUtil;

	@Around("execution(* net.intellica.icc.services.controller.JobController.get(..))")
	public Object getJob(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			validationUtil.validateId((String)pjp.getArgs()[0]);
			result = pjp.proceed();
			if (result == null)
				throw new NotFoundException();
			else {
				Job job = (Job)result;
				authorizationUtil.canViewJob(job, true);
				fillUtil.fillJobFields(job);
				if (authorizationUtil.isNormalUser())
					job.setRights(simplifyUtil.simplify(job.getRights()));
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.JobController.explore(..))")
	public Object exploreJobs(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String id = (String)pjp.getArgs()[0];
			Folder folder = null;
			if (!StringUtils.isEmpty(id)) {
				validationUtil.validateId(id);
				folder = folderService.get(id, false);
				if (folder == null)
					throw new NotFoundException();
			}
			result = pjp.proceed();
			Collection<Job> collection = (Collection<Job>)result;
			if (authorizationUtil.isNormalUser()) {
				Collection<Job> refined = authorizationUtil.refineJobs(collection);
				fillUtil.fillJobFields(refined);
				refined.forEach(j -> j.setRights(simplifyUtil.simplify(j.getRights())));
				result = refined;
			}
			else
				fillUtil.fillJobFields(collection);
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.JobController.listVariables(..))")
	public Object listJobVariables(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String id = (String)pjp.getArgs()[0];
			validationUtil.validateId(id);
			Job job = jobService.get(id, false);
			if (job == null)
				throw new NotFoundException();
			else {
				authorizationUtil.canViewJob(job, true);
				result = pjp.proceed();
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.JobController.delete(..))")
	public Object deleteJob(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			if (authorizationUtil.isOperatorUser())
				authorizationUtil.throwDeleteException();
			validationUtil.validateId((String)pjp.getArgs()[0]);
			String id = (String)pjp.getArgs()[0];
			Job toDelete = jobService.get(id, false);
			if (toDelete == null)
				throw new NotFoundException();
			else {
				validationUtil.validateDelete(toDelete);
				if (authorizationUtil.isNormalUser())
					authorizationUtil.canDeleteJob(toDelete, true);
				result = pjp.proceed();
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.JobController.save(..))")
	public Object saveJob(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			Job job = (Job)pjp.getArgs()[0];
			Job dbJob = StringUtils.isEmpty(job.getId()) ? null : jobService.get(job.getId(), false);
			authorizationUtil.canSaveJob(job, dbJob, true);
			validationUtil.validateSave(job);
			preSaveUtil.fill(job, dbJob);
			result = pjp.proceed();
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.JobController.run(..))")
	public Object runJob(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String id = (String)pjp.getArgs()[0];
			validationUtil.validateId(id);
			Job toRun = jobService.get(id, false);
			if (toRun == null)
				throw new NotFoundException();
			else {
				authorizationUtil.canExecuteJob(toRun, true);
				validationUtil.validateRun(toRun);
				validationUtil.validateOperationDate(OperationType.Job, id, dateTimeUtil.getMinimumOperationDate());
				result = pjp.proceed();
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.JobController.schedule(..))")
	public Object scheduleJob(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String id = (String)pjp.getArgs()[0];
			Schedule schedule = (Schedule)pjp.getArgs()[1];
			validationUtil.validateId(id);
			validationUtil.validateSchedule(schedule);
			schedule.setFirstRunDate(dateTimeUtil.adjustFirstRunDate(schedule.getFirstRunDate()));
			Job toRun = jobService.get(id, false);
			if (toRun == null)
				throw new NotFoundException();
			else {
				authorizationUtil.canExecuteJob(toRun, true);
				validationUtil.validateSchedule(toRun);
				validationUtil.validateOperationDate(OperationType.Job, id, schedule.getFirstRunDate());
				result = pjp.proceed();
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

}

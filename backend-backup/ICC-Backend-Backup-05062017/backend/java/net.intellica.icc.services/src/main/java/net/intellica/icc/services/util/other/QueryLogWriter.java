package net.intellica.icc.services.util.other;

import java.util.List;
import java.util.stream.IntStream;
import net.intellica.icc.template.model.QueryLog;
import org.springframework.stereotype.Component;

@Component
public class QueryLogWriter {

	private static final String MAIN_SEPARATOR_FORMAT = "--- %s (%s) @ %s ---";

	public String write(List<QueryLog> queryLogs) {
		StringBuilder builder = new StringBuilder();
		String n = "\n";
		String last = "";
		Integer lastLength = 0;
		for (QueryLog q : queryLogs) {
			String current = String.format(MAIN_SEPARATOR_FORMAT, q.getJobName(), q.getTemplateName(), q.getConnectionName());
			if (!current.equals(last)) {
				builder.append(n);
				builder.append(current + n);
				builder.append(n);
				last = current;
				lastLength = last.length();
			}
			builder.append(q.getQuery() + n);
			String separator = IntStream.range(0, lastLength).mapToObj(i -> new Integer(i).toString()).reduce("", (acc, i) -> acc + "-");
			builder.append(separator + n);
		}
		return builder.toString();

	}

}

package net.intellica.icc.services.service;

import java.util.ArrayList;
import java.util.List;
import net.intellica.icc.services.model.Folder;
import net.intellica.icc.services.repo.FolderRepository;
import net.intellica.icc.services.util.audit.HistoryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FolderService {

	@Autowired private FolderRepository repository;
	@Autowired private HistoryUtil historyUtil;

	public FolderService() {
	}

	public Folder get(String id, Boolean includeHistory) {
		return get(id, false, false, false, false, includeHistory);
	}

	public Folder get(String id, Boolean fetchFolders, Boolean fetchJobs, Boolean fetchRules, Boolean fetchParameters, Boolean includeHistory) {
		Folder result = repository.findOne(id);
		if (includeHistory && result == null)
			result = historyUtil.getLastRevision(Folder.class, id);
		if (result != null) {
			if (fetchFolders)
				result.getFolders().size();
			if (fetchJobs)
				result.getJobs().size();
			if (fetchRules)
				result.getRules().size();
			if (fetchParameters)
				result.getParameters().size();
		}
		return result;
	}

	public Folder save(Folder folder) {
		return repository.save(folder);
	}

	public void delete(String id) {
		repository.delete(id);
	}

	public List<Folder> listAll() {
		List<Folder> result = new ArrayList<>();
		repository.findAll().forEach(result::add);
		return result;
	}

	public List<Folder> listByName(String name) {
		return repository.findAllByPropsName(name);
	}

	public List<Folder> listByParentFolderId(String id) {
		return repository.findAllByParentFolderIdOrderByPropsNameAsc(id);
	}

	public List<Folder> listAllParents(String id) {
		List<Folder> result = new ArrayList<>();
		Folder current = get(id, false);
		if (current != null) {
			for (Folder folder = current.getParentFolder(); folder != null; folder = folder.getParentFolder()) {
				result.add(folder);
			}
		}
		return result;
	}

	public Long countByParentFolderId(String id) {
		return repository.countByParentFolderId(id);
	}

}

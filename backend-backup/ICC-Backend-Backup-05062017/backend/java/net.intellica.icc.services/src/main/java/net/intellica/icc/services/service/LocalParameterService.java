package net.intellica.icc.services.service;

import java.util.ArrayList;
import java.util.List;
import net.intellica.icc.services.model.Folder;
import net.intellica.icc.services.model.LocalParameter;
import net.intellica.icc.services.repo.LocalParameterRepository;
import net.intellica.icc.services.util.audit.HistoryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LocalParameterService {

	@Autowired private LocalParameterRepository repository;
	@Autowired private HistoryUtil historyUtil;

	public LocalParameterService() {
	}

	public List<LocalParameter> list(Folder folder) {
		return repository.findAllByFolderIdOrderByPropsNameAsc(folder.getId());
	}

	public LocalParameter get(String id, Boolean includeHistory) {
		LocalParameter result = repository.findOne(id);
		if (includeHistory && result == null)
			result = historyUtil.getLastRevision(LocalParameter.class, id);
		return result;
	}

	public LocalParameter save(LocalParameter parameter) {
		return repository.save(parameter);
	}

	public void delete(String id) {
		repository.delete(id);
	}

	public List<LocalParameter> listAll() {
		List<LocalParameter> result = new ArrayList<>();
		repository.findAll().forEach(result::add);
		return result;
	}

	public List<LocalParameter> listByName(String name) {
		return repository.findAllByPropsName(name);
	}

	public List<LocalParameter> listByFolderId(String id) {
		return repository.findAllByFolderIdOrderByPropsNameAsc(id);
	}

}

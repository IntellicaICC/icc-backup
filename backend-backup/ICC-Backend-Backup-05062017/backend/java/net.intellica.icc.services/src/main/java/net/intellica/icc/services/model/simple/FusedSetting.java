package net.intellica.icc.services.model.simple;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import net.intellica.icc.services.model.Setting;
import net.intellica.icc.services.model.SettingDefinition;
import net.intellica.icc.services.model.SettingGroup;
import net.intellica.icc.services.model.SettingType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FusedSetting extends Setting {

	private String name;
	private String description;
	private Integer position;
	private SettingType type;
	private SettingGroup settingGroup;

	@Override
	@JsonIgnore
	public SettingDefinition getDefinition() {
		return super.getDefinition();
	}

	@Override
	@JsonIgnore
	public String getUserId() {
		return super.getUserId();
	}

	@Override
	@JsonIgnore
	public Date getModificationDate() {
		return super.getModificationDate();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public SettingType getType() {
		return type;
	}

	public void setType(SettingType type) {
		this.type = type;
	}

	public SettingGroup getSettingGroup() {
		return settingGroup;
	}

	public void setSettingGroup(SettingGroup settingGroup) {
		this.settingGroup = settingGroup;
	}

}

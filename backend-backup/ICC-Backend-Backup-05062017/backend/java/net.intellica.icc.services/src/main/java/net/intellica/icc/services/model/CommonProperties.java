package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.intellica.icc.services.util.other.ServiceConstants;

@Embeddable
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommonProperties {

	private Boolean active;
	private String createdBy;
	private Date creationDate;
	private String description;
	private String modifiedBy;
	private Date modificationDate;
	private String name;

	public CommonProperties() {
		active = true;
		description = "";
		name = "";
	}

	@Column(name = "ACTIVE", nullable = false)
	public Boolean isActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Column(name = "CREATED_BY", nullable = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "CREATION_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date date) {
		this.creationDate = date;
	}

	@Column(name = "DESCRIPTION", nullable = true, length = ServiceConstants.Model.LONG_STRING_LENGTH)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "MODIFIED_BY", nullable = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Column(name = "MODIFICATION_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date date) {
		this.modificationDate = date;
	}

	@Column(name = "NAME", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

package net.intellica.icc.services.controller;

import net.intellica.icc.services.license.LicenseValidator;
import net.intellica.icc.services.model.License;
import net.intellica.icc.services.service.LicenseService;
import net.intellica.icc.template.exception.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/license")
public class LicenseController {

	@Autowired private LicenseValidator validator;
	@Autowired private LicenseService licenseService;

	@RequestMapping(value = { "", "/" }, method = RequestMethod.GET)
	public Object get() {
		return validator.getMessage();
	}

	@RequestMapping(value = { "/add", "/add/" }, method = RequestMethod.POST)
	public Object add(@RequestBody License license) throws Exception {
		validator.validateLicense(license);
		if (validator.isValid())
			licenseService.save(license);
		else
			throw new Exception(Messages.License.INVALID_LICENSE);
		return Messages.License.THANKS;
	}

}

package net.intellica.icc.services.util.other;

import net.intellica.icc.services.event.ErrorEvent;
import net.intellica.icc.services.model.operation.Operation;
import net.intellica.icc.services.model.operation.OperationQueryLog;
import net.intellica.icc.services.model.operation.OperationResult;
import net.intellica.icc.services.model.operation.OperationSummary;
import net.intellica.icc.services.model.work.WorkflowResult;
import net.intellica.icc.services.service.OperationQueryLogService;
import net.intellica.icc.services.service.OperationRuleLogService;
import net.intellica.icc.services.service.OperationService;
import net.intellica.icc.template.model.QueryLog;
import net.intellica.icc.template.model.WorkItemResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OperationUtil {

	@Autowired private OperationService operationService;
	@Autowired private OperationQueryLogService queryLogService;
	@Autowired private OperationRuleLogService ruleLogService;
	@Autowired private QueryLogWriter queryLogWriter;

	public void save(String operationId, WorkItemResult workResult) {
		Operation operation = operationService.get(operationId, false);
		operation.setSummary(workResult.isSuccessful() ? OperationSummary.Success : OperationSummary.Error);
		workResult.getQueryLogs().forEach(q -> saveQueryLog(operationId, q));
		operation.getDetail().setQueryDetail(queryLogWriter.write(workResult.getQueryLogs()));
		if (workResult.getError() != null)
			operation.getDetail().setErrorDetail(new ErrorEvent(workResult.getError(), null).getErrorLog().getMessage());
		for (String key : workResult.getResults().keySet()) {
			OperationResult operationResult = new OperationResult();
			operationResult.setOperation(operation);
			operationResult.setName(key);
			operationResult.setValue(represent(workResult.getResults().get(key)));
			operation.getDetail().getResults().add(operationResult);
		}
		operationService.save(operation);
	}

	public void save(String operationId, WorkflowResult workResult) {
		Operation operation = operationService.get(operationId, false);
		operation.setSummary(workResult.isSuccessful() ? OperationSummary.Success : OperationSummary.Error);
		operation.getDetail().setQueryDetail(queryLogWriter.write(workResult.getQueryLogs()));
		workResult.getQueryLogs().forEach(q -> saveQueryLog(operationId, q));
		if (workResult.getError() != null)
			operation.getDetail().setErrorDetail(new ErrorEvent(workResult.getError(), null).getErrorLog().getMessage());
		for (String key : workResult.getResults().keySet()) {
			OperationResult operationResult = new OperationResult();
			operationResult.setOperation(operation);
			operationResult.setName(key);
			operationResult.setValue(represent(workResult.getResults().get(key)));
			operation.getDetail().getResults().add(operationResult);
		}
		operation.getDetail().setFinalStatus(workResult.getFinalStatus().toString());
		operationService.save(operation);
		ruleLogService.saveAll(workResult.getRuleLogs());
	}

	private void saveQueryLog(String operationId, QueryLog queryLog) {
		OperationQueryLog log = new OperationQueryLog();
		log.setOperationId(operationId);
		log.setJobId(queryLog.getJobId());
		log.setJobName(queryLog.getJobName());
		log.setTemplateName(queryLog.getTemplateName());
		log.setQueryDate(queryLog.getTimestamp());
		log.setConnectionId(queryLog.getConnectionId());
		log.setConnectionName(queryLog.getConnectionName());
		log.setQuery(queryLog.getQuery());
		queryLogService.save(log);
	}

	private String represent(Object value) {
		return value == null ? "" : value.toString();
	}

}

package net.intellica.icc.services.model.simple;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import net.intellica.icc.services.model.operation.Operation;
import net.intellica.icc.services.model.operation.OperationDetail;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SimpleOperation extends Operation {

	@Override
	@JsonIgnore
	public OperationDetail getDetail() {
		return super.getDetail();
	}

}

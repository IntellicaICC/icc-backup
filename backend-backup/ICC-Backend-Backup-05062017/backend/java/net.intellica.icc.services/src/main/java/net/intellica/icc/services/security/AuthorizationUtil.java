package net.intellica.icc.services.security;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import net.intellica.icc.services.model.Connection;
import net.intellica.icc.services.model.Folder;
import net.intellica.icc.services.model.GlobalParameter;
import net.intellica.icc.services.model.Job;
import net.intellica.icc.services.model.LocalParameter;
import net.intellica.icc.services.model.Notification;
import net.intellica.icc.services.model.Rights;
import net.intellica.icc.services.model.Role;
import net.intellica.icc.services.model.Rule;
import net.intellica.icc.services.model.Template;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.model.UserType;
import net.intellica.icc.services.model.operation.Operation;
import net.intellica.icc.services.model.operation.OperationType;
import net.intellica.icc.services.model.report.ReportFilter;
import net.intellica.icc.services.service.FolderService;
import net.intellica.icc.services.service.JobService;
import net.intellica.icc.services.service.RuleService;
import net.intellica.icc.services.util.dev.CollectionUtil;
import net.intellica.icc.template.exception.Messages.Generic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class AuthorizationUtil {

	@Autowired private ApplicationContext context;
	@Autowired private FolderService folderService;
	@Autowired private JobService jobService;
	@Autowired private RuleService ruleService;

	public Boolean isAdminUser() {
		return context.getBean(User.class).getUserType() == UserType.Admin;
	}

	public Boolean isNormalUser() {
		return context.getBean(User.class).getUserType() == UserType.Normal;
	}

	public Boolean isOperatorUser() {
		return context.getBean(User.class).getUserType() == UserType.Operator;
	}

	public Boolean isAdminUser(User user) {
		return user.getUserType() == UserType.Admin;
	}

	public Boolean isNormalUser(User user) {
		return user.getUserType() == UserType.Normal;
	}

	public Boolean isOperatorUser(User user) {
		return user.getUserType() == UserType.Operator;
	}

	public Boolean isOwner(Rights rights, User user) {
		return user.getId().equals(rights.getOwner());
	}

	public Set<String> extractUserRoles(User user) {
		return user.getRoles().stream()
			.filter(r -> r.getProps().isActive())
			.map(r -> r.getId())
			.collect(Collectors.toSet());
	}

	private Boolean commonReadAdvice(Rights objectRights, Rights actualRights, User user, Set<String> userRoleIds, Boolean throwException) {
		Boolean result = true;
		if (!isOwner(objectRights, user) && !hasRight(actualRights, user.getId(), userRoleIds, Rights.READ_RIGHT)) {
			if (throwException)
				throwReadException();
			else
				result = false;
		}
		return result;
	}

	private Boolean commonSaveAdvice(Rights objectRights, Rights actualRights, User user, Set<String> userRoleIds, Boolean throwException) {
		Boolean result = true;
		if (!isOwner(objectRights, user) && !hasRight(actualRights, user.getId(), userRoleIds, Rights.WRITE_RIGHT)) {
			if (throwException)
				throwSaveException();
			else
				result = false;
		}
		return result;
	}

	private Boolean commonDeleteAdvice(Rights objectRights, Rights actualRights, User user, Set<String> userRoleIds, Boolean throwException) {
		Boolean result = true;
		if (!isOwner(objectRights, user) && !hasRight(actualRights, user.getId(), userRoleIds, Rights.WRITE_RIGHT)) {
			if (throwException)
				throwDeleteException();
			else
				result = false;
		}
		return result;
	}

	private Boolean commonExecuteAdvice(Rights objectRights, Rights actualRights, User user, Set<String> userRoleIds, Boolean throwException) {
		Boolean result = true;
		if (!isOwner(objectRights, user) && !hasRight(actualRights, user.getId(), userRoleIds, Rights.EXECUTE_RIGHT)) {
			if (throwException)
				throwExecuteException();
			else
				result = false;
		}
		return result;
	}

	public void throwReadException() {
		throwException("access");
	}

	public void throwSaveException() {
		throwException("modify");
	}

	public void throwDeleteException() {
		throwException("delete");
	}

	public void throwExecuteException() {
		throwException("execute");
	}

	private void throwException(String operation) {
		throw new AccessDeniedException(String.format(Generic.ACCESS_DENIED, operation));
	}

	public Boolean hasRight(Rights rights, String userId, Set<String> userRoleIds, Byte request) {
		Boolean result = false;
		if (rights != null) {
			Set<String> commonRoleIds = CollectionUtil.intersect(rights.getRoleRights().keySet(), userRoleIds);
			for (String roleId : commonRoleIds) {
				if (evaluateRight(rights.getRoleRights().get(roleId), request)) {
					result = true;
					break;
				}
			}
			if (!result) {
				Byte b = rights.getUserRights().get(userId);
				if (b != null && (b & request) > 0)
					result = true;
			}
		}
		return result;
	}

	public Boolean evaluateRight(Byte rights, Byte request) {
		Boolean result = false;
		if (rights != null && request != null)
			result = (rights & request) == request;
		return result;
	}

	public Collection<Folder> refineFolders(Collection<Folder> collection) {
		return refineFolders(collection, context.getBean(User.class));
	}

	public Collection<Folder> refineFolders(Collection<Folder> collection, User user) {
		return collection.stream().filter(o -> canViewFolder(o, user, false)).collect(Collectors.toList());
	}

	public Collection<Job> refineJobs(Collection<Job> collection) {
		return refineJobs(collection, context.getBean(User.class));
	}

	public Collection<Job> refineJobs(Collection<Job> collection, User user) {
		return collection.stream().filter(o -> canViewJob(o, user, false)).collect(Collectors.toList());
	}

	public Collection<Rule> refineRules(Collection<Rule> collection) {
		return refineRules(collection, context.getBean(User.class));
	}

	public Collection<Rule> refineRules(Collection<Rule> collection, User user) {
		return collection.stream().filter(o -> canViewRule(o, user, false)).collect(Collectors.toList());
	}

	public Collection<LocalParameter> refineParameters(Collection<LocalParameter> collection) {
		return refineParameters(collection, context.getBean(User.class));
	}

	public Collection<LocalParameter> refineParameters(Collection<LocalParameter> collection, User user) {
		return collection.stream().filter(o -> canViewLocalParameter(o, user, false)).collect(Collectors.toList());
	}

	public Collection<Operation> refineOperations(Collection<Operation> collection) {
		return refineOperations(collection, context.getBean(User.class));
	}

	public Collection<Operation> refineOperations(Collection<Operation> collection, User user) {
		return collection.stream().filter(o -> canViewOperation(o, user, false)).collect(Collectors.toList());
	}

	public Rights findRights(Folder folder) {
		Rights result = null;
		for (Folder f = folder; f != null; f = f.getParentFolder()) {
			if (!f.getRights().getInheritsFromParent()) {
				result = f.getRights();
				break;
			}
		}
		return result;
	}

	public Rights findRights(Job job) {
		Rights result = null;
		if (!job.getRights().getInheritsFromParent())
			result = job.getRights();
		if (result == null)
			result = findRights(job.getFolder());
		return result;
	}

	public Rights findRights(Rule rule) {
		Rights result = null;
		if (!rule.getRights().getInheritsFromParent())
			result = rule.getRights();
		if (result == null)
			result = findRights(rule.getFolder());
		return result;
	}

	public Rights findRights(LocalParameter parameter) {
		Rights result = null;
		if (!parameter.getRights().getInheritsFromParent())
			result = parameter.getRights();
		if (result == null)
			result = findRights(parameter.getFolder());
		return result;
	}

	public Boolean canViewFolder(Folder folder, Boolean throwException) {
		return canViewFolder(folder, context.getBean(User.class), throwException);
	}

	public Boolean canViewFolder(Folder folder, User user, Boolean throwException) {
		return canViewFolder(folder, user, extractUserRoles(user), throwException);
	}

	public Boolean canViewFolder(Folder folder, User user, Set<String> userRoleIds, Boolean throwException) {
		Boolean result = isAdminUser(user);
		if (!result && isNormalUser(user)) {
			if (folder != null)
				result = commonReadAdvice(folder.getRights(), findRights(folder), user, userRoleIds, false);
			if (!result)
				result = hasChildWithReadRight(folder, user);
		}
		if (!result && throwException)
			throwReadException();
		return result;
	}

	public Boolean canSaveFolder(Folder folder, Folder dbFolder, Boolean throwException) {
		return canSaveFolder(folder, dbFolder, context.getBean(User.class), throwException);
	}

	public Boolean canSaveFolder(Folder folder, Folder dbFolder, User user, Boolean throwException) {
		return canSaveFolder(folder, dbFolder, user, extractUserRoles(user), throwException);
	}

	public Boolean canSaveFolder(Folder folder, Folder dbFolder, User user, Set<String> userRoleIds, Boolean throwException) {
		Boolean result = isAdminUser(user);
		if (!result && isNormalUser(user)) {
			if (StringUtils.isEmpty(folder.getId())) {
				if (folder.getParentFolder() == null)
					result = false;
				else {
					Folder parentFolder = folderService.get(folder.getParentFolder().getId(), false);
					result = commonSaveAdvice(parentFolder.getRights(), findRights(parentFolder), user, userRoleIds, throwException);
				}
			}
			else
				result = commonSaveAdvice(dbFolder.getRights(), findRights(dbFolder), user, userRoleIds, throwException);
		}
		if (!result && throwException)
			throwSaveException();
		return result;
	}

	public Boolean canDeleteFolder(Folder folder, Boolean throwException) {
		return canDeleteFolder(folder, context.getBean(User.class), throwException);
	}

	public Boolean canDeleteFolder(Folder folder, User user, Boolean throwException) {
		return canDeleteFolder(folder, user, extractUserRoles(user), throwException);
	}

	public Boolean canDeleteFolder(Folder folder, User user, Set<String> userRoleIds, Boolean throwException) {
		Boolean result = isAdminUser(user);
		if (!result && isNormalUser(user))
			result = commonDeleteAdvice(folder.getRights(), findRights(folder), user, userRoleIds, throwException);
		if (!result && throwException)
			throwDeleteException();
		return result;
	}

	public Boolean canViewJob(Job job, Boolean throwException) {
		return canViewJob(job, context.getBean(User.class), throwException);
	}

	public Boolean canViewJob(Job job, User user, Boolean throwException) {
		return canViewJob(job, user, extractUserRoles(user), throwException);
	}

	public Boolean canViewJob(Job job, User user, Set<String> userRoleIds, Boolean throwException) {
		Boolean result = isAdminUser(user);
		if (!result && isNormalUser(user))
			result = commonReadAdvice(job.getRights(), findRights(job), user, userRoleIds, throwException);
		if (!result && throwException)
			throwReadException();
		return result;
	}

	public Boolean canSaveJob(Job job, Job dbJob, Boolean throwException) {
		return canSaveJob(job, dbJob, context.getBean(User.class), throwException);
	}

	public Boolean canSaveJob(Job job, Job dbJob, User user, Boolean throwException) {
		return canSaveJob(job, dbJob, context.getBean(User.class), extractUserRoles(user), throwException);
	}

	public Boolean canSaveJob(Job job, Job dbJob, User user, Set<String> userRoleIds, Boolean throwException) {
		Boolean result = isAdminUser(user);
		if (!result && isNormalUser(user)) {
			if (StringUtils.isEmpty(job.getId())) {
				if (job.getFolder() == null)
					result = false;
				else {
					Folder parentFolder = folderService.get(job.getFolder().getId(), false);
					result = commonSaveAdvice(parentFolder.getRights(), findRights(parentFolder), user, userRoleIds, throwException);
				}
			}
			else
				result = commonSaveAdvice(dbJob.getRights(), findRights(dbJob), user, userRoleIds, throwException);
		}
		if (!result && throwException)
			throwSaveException();
		return result;
	}

	public Boolean canDeleteJob(Job job, Boolean throwException) {
		return canDeleteJob(job, context.getBean(User.class), throwException);
	}

	public Boolean canDeleteJob(Job job, User user, Boolean throwException) {
		return canDeleteJob(job, user, extractUserRoles(user), throwException);
	}

	public Boolean canDeleteJob(Job job, User user, Set<String> userRoleIds, Boolean throwException) {
		Boolean result = isAdminUser(user);
		if (!result && isNormalUser(user))
			result = commonDeleteAdvice(job.getRights(), findRights(job), user, userRoleIds, throwException);
		if (!result && throwException)
			throwDeleteException();
		return result;
	}

	public Boolean canExecuteOperation(Operation operation, Boolean throwException) {
		return canExecuteOperation(operation, context.getBean(User.class), throwException);
	}

	public Boolean canExecuteOperation(Operation operation, User user, Boolean throwException) {
		Boolean result = !isNormalUser(user);
		if (!result) {
			if (operation.getType() == OperationType.Job) {
				Job job = jobService.get(operation.getModelId(), false);
				result = canExecuteJob(job, user, throwException);
			}
			else if (operation.getType() == OperationType.Rule) {
				Rule rule = ruleService.get(operation.getModelId(), false);
				result = canExecuteRule(rule, user, throwException);
			}
		}
		return result;
	}

	public Boolean canExecuteJob(Job job, Boolean throwException) {
		return canExecuteJob(job, context.getBean(User.class), throwException);
	}

	public Boolean canExecuteJob(Job job, User user, Boolean throwException) {
		return canExecuteJob(job, user, extractUserRoles(user), throwException);
	}

	public Boolean canExecuteJob(Job job, User user, Set<String> userRoleIds, Boolean throwException) {
		Boolean result = !isNormalUser(user);
		if (!result)
			result = commonExecuteAdvice(job.getRights(), findRights(job), user, userRoleIds, throwException);
		return result;
	}

	public Boolean canViewRule(Rule rule, Boolean throwException) {
		return canViewRule(rule, context.getBean(User.class), throwException);
	}

	public Boolean canViewRule(Rule rule, User user, Boolean throwException) {
		return canViewRule(rule, user, extractUserRoles(user), throwException);
	}

	public Boolean canViewRule(Rule rule, User user, Set<String> userRoleIds, Boolean throwException) {
		Boolean result = isAdminUser(user);
		if (!result && isNormalUser(user))
			result = commonReadAdvice(rule.getRights(), findRights(rule), user, userRoleIds, throwException);
		if (!result && throwException)
			throwReadException();
		return result;
	}

	public Boolean canSaveRule(Rule rule, Rule dbRule, Boolean throwException) {
		return canSaveRule(rule, dbRule, context.getBean(User.class), throwException);
	}

	public Boolean canSaveRule(Rule rule, Rule dbRule, User user, Boolean throwException) {
		return canSaveRule(rule, dbRule, context.getBean(User.class), extractUserRoles(user), throwException);
	}

	public Boolean canSaveRule(Rule rule, Rule dbRule, User user, Set<String> userRoleIds, Boolean throwException) {
		Boolean result = isAdminUser(user);
		if (!result && isNormalUser(user)) {
			if (StringUtils.isEmpty(rule.getId())) {
				if (rule.getFolder() == null)
					result = false;
				else {
					Folder parentFolder = folderService.get(rule.getFolder().getId(), false);
					result = commonSaveAdvice(parentFolder.getRights(), findRights(parentFolder), user, userRoleIds, throwException);
				}
			}
			else
				result = commonSaveAdvice(dbRule.getRights(), findRights(dbRule), user, userRoleIds, throwException);
		}
		if (!result && throwException)
			throwSaveException();
		return result;
	}

	public Boolean canDeleteRule(Rule rule, Boolean throwException) {
		return canDeleteRule(rule, context.getBean(User.class), throwException);
	}

	public Boolean canDeleteRule(Rule rule, User user, Boolean throwException) {
		return canDeleteRule(rule, user, extractUserRoles(user), throwException);
	}

	public Boolean canDeleteRule(Rule rule, User user, Set<String> userRoleIds, Boolean throwException) {
		Boolean result = isAdminUser(user);
		if (!result && isNormalUser(user))
			result = commonDeleteAdvice(rule.getRights(), findRights(rule), user, userRoleIds, throwException);
		if (!result && throwException)
			throwDeleteException();
		return result;
	}

	public Boolean canExecuteRule(Rule rule, Boolean throwException) {
		return canExecuteRule(rule, context.getBean(User.class), throwException);
	}

	public Boolean canExecuteRule(Rule rule, User user, Boolean throwException) {
		return canExecuteRule(rule, user, extractUserRoles(user), throwException);
	}

	public Boolean canExecuteRule(Rule rule, User user, Set<String> userRoleIds, Boolean throwException) {
		Boolean result = !isNormalUser(user);
		if (!result)
			result = commonExecuteAdvice(rule.getRights(), findRights(rule), user, userRoleIds, throwException);
		return result;
	}

	public Boolean canViewLocalParameter(LocalParameter parameter, Boolean throwException) {
		return canViewLocalParameter(parameter, context.getBean(User.class), throwException);
	}

	public Boolean canViewLocalParameter(LocalParameter parameter, User user, Boolean throwException) {
		return canViewLocalParameter(parameter, user, extractUserRoles(user), throwException);
	}

	public Boolean canViewLocalParameter(LocalParameter parameter, User user, Set<String> userRoleIds, Boolean throwException) {
		Boolean result = isAdminUser(user);
		if (!result && isNormalUser(user))
			result = commonReadAdvice(parameter.getRights(), findRights(parameter), user, userRoleIds, throwException);
		if (!result && throwException)
			throwReadException();
		return result;
	}

	public Boolean canSaveLocalParameter(LocalParameter parameter, LocalParameter dbParameter, Boolean throwException) {
		return canSaveLocalParameter(parameter, dbParameter, context.getBean(User.class), throwException);
	}

	public Boolean canSaveLocalParameter(LocalParameter parameter, LocalParameter dbParameter, User user, Boolean throwException) {
		return canSaveLocalParameter(parameter, dbParameter, context.getBean(User.class), extractUserRoles(user), throwException);
	}

	public Boolean canSaveLocalParameter(LocalParameter parameter, LocalParameter dbParameter, User user, Set<String> userRoleIds, Boolean throwException) {
		Boolean result = isAdminUser(user);
		if (!result && isNormalUser(user)) {
			if (StringUtils.isEmpty(parameter.getId())) {
				if (parameter.getFolder() == null)
					result = false;
				else {
					Folder parentFolder = folderService.get(parameter.getFolder().getId(), false);
					result = commonSaveAdvice(parentFolder.getRights(), findRights(parentFolder), user, userRoleIds, throwException);
				}
			}
			else
				result = commonSaveAdvice(dbParameter.getRights(), findRights(dbParameter), user, userRoleIds, throwException);
		}
		if (!result && throwException)
			throwSaveException();
		return result;
	}

	public Boolean canDeleteLocalParameter(LocalParameter parameter, Boolean throwException) {
		return canDeleteLocalParameter(parameter, context.getBean(User.class), throwException);
	}

	public Boolean canDeleteLocalParameter(LocalParameter parameter, User user, Boolean throwException) {
		return canDeleteLocalParameter(parameter, user, extractUserRoles(user), throwException);
	}

	public Boolean canDeleteLocalParameter(LocalParameter parameter, User user, Set<String> userRoleIds, Boolean throwException) {
		Boolean result = isAdminUser(user);
		if (!result && isNormalUser(user))
			result = commonDeleteAdvice(parameter.getRights(), findRights(parameter), user, userRoleIds, throwException);
		if (!result && throwException)
			throwDeleteException();
		return result;
	}

	public Boolean canExecuteLocalParameter(LocalParameter parameter, Boolean throwException) {
		return canExecuteLocalParameter(parameter, context.getBean(User.class), throwException);
	}

	public Boolean canExecuteLocalParameter(LocalParameter parameter, User user, Boolean throwException) {
		return canExecuteLocalParameter(parameter, user, extractUserRoles(user), throwException);
	}

	public Boolean canExecuteLocalParameter(LocalParameter parameter, User user, Set<String> userRoleIds, Boolean throwException) {
		return commonExecuteAdvice(parameter.getRights(), findRights(parameter), user, userRoleIds, throwException);
	}

	public Boolean canViewNotification(Notification notification, Boolean throwException) {
		return canViewNotification(notification, context.getBean(User.class), throwException);
	}

	public Boolean canViewNotification(Notification notification, User user, Boolean throwException) {
		Boolean result = user.getId().equals(notification.getTargetUser());
		if (!result && throwException)
			throwReadException();
		return result;
	}

	public Boolean canViewGlobalParameter(GlobalParameter parameter, Boolean throwException) {
		return canViewGlobalParameter(parameter, context.getBean(User.class), throwException);
	}

	public Boolean canViewGlobalParameter(GlobalParameter parameter, User user, Boolean throwException) {
		Boolean result = !isOperatorUser(user);
		if (!result && throwException)
			throwReadException();
		return result;
	}

	public Boolean canSaveGlobalParameter(GlobalParameter parameter, GlobalParameter dbParameter, Boolean throwException) {
		return canSaveGlobalParameter(parameter, dbParameter, context.getBean(User.class), throwException);
	}

	public Boolean canSaveGlobalParameter(GlobalParameter parameter, GlobalParameter dbParameter, User user, Boolean throwException) {
		Boolean result = !isOperatorUser(user);
		if (!result && throwException)
			throwSaveException();
		return result;
	}

	public Boolean canViewOperation(Operation operation, Boolean throwException) {
		return canViewOperation(operation, context.getBean(User.class), throwException);
	}

	public Boolean canViewOperation(Operation operation, User user, Boolean throwException) {
		Boolean result = !isNormalUser(user) || user.getId().equals(operation.getUserId());
		if (!result) {
			if (operation.getType() == OperationType.Job) {
				Job job = jobService.get(operation.getModelId(), true);
				result = canViewJob(job, user, throwException);
			}
			else if (operation.getType() == OperationType.Rule) {
				Rule rule = ruleService.get(operation.getModelId(), true);
				result = canViewRule(rule, user, throwException);
			}
		}
		if (!result && throwException)
			throwReadException();
		return result;
	}

	public Boolean canViewRole(Role role, Boolean throwException) {
		return canViewRole(role, context.getBean(User.class), throwException);
	}

	public Boolean canViewRole(Role role, User user, Boolean throwException) {
		Boolean result = isAdminUser(user);
		if (!result && throwException)
			throwReadException();
		return result;
	}

	public Boolean canSaveRole(Role role, Role dbRole, Boolean throwException) {
		return canSaveRole(role, dbRole, context.getBean(User.class), throwException);
	}

	public Boolean canSaveRole(Role role, Role dbRole, User user, Boolean throwException) {
		Boolean result = isAdminUser(user);
		if (!result && throwException)
			throwSaveException();
		return result;
	}

	public Boolean canDeleteRole(Role role, Boolean throwException) {
		return canDeleteRole(role, context.getBean(User.class), throwException);
	}

	public Boolean canDeleteRole(Role role, User user, Boolean throwException) {
		Boolean result = isAdminUser(user);
		if (!result && throwException)
			throwDeleteException();
		return result;
	}

	public Boolean canViewTemplate(Template template, Boolean throwException) {
		return canViewTemplate(template, context.getBean(User.class), throwException);
	}

	public Boolean canViewTemplate(Template template, User user, Boolean throwException) {
		Boolean result = !isOperatorUser(user);
		if (!result && throwException)
			throwReadException();
		return result;
	}

	public Boolean canSaveConnection(Connection connection, Connection dbConnection, Boolean throwException) {
		return canSaveConnection(connection, dbConnection, context.getBean(User.class), throwException);
	}

	public Boolean canSaveConnection(Connection connection, Connection dbConnection, User user, Boolean throwException) {
		Boolean result = isAdminUser(user);
		if (!result && throwException)
			throwSaveException();
		return result;
	}

	public Boolean hasChildFolderWithReadRight(Folder folder) {
		return hasChildFolderWithReadRight(folder, context.getBean(User.class));
	}

	public Boolean hasChildFolderWithReadRight(Folder folder, User user) {
		Boolean result = false;
		try {
			Folder dbFolder = folderService.get(folder.getId(), true, false, false, false, false);
			for (Folder f : dbFolder.getFolders())
				result = result || commonReadAdvice(f.getRights(), findRights(f), user, extractUserRoles(user), false);
			for (Folder f : dbFolder.getFolders())
				result = result || hasChildWithReadRight(f, user);
		}
		catch (Throwable t) {
			// hata ignore
		}
		return result;
	}

	public Boolean canViewReport(ReportFilter filter, Boolean throwException) {
		return canViewReport(filter, context.getBean(User.class), throwException);
	}

	public Boolean canViewReport(ReportFilter filter, User user, Boolean throwException) {
		Boolean result;
		switch (filter.getReportType()) {
			case AuthorizationRoleObject:
			case AuthorizationRoleUser:
			case AuthorizationUserObject:
			case AuthorizationUserRole:
				result = isAdminUser(user);
				break;
			default:
				result = false;
				break;
		}
		if (!result && throwException)
			throwReadException();
		return result;
	}

	private Boolean hasChildWithReadRight(Folder folder) {
		return hasChildWithReadRight(folder, context.getBean(User.class));
	}

	private Boolean hasChildWithReadRight(Folder folder, User user) {
		Boolean result = false;
		try {
			Folder dbFolder = folderService.get(folder.getId(), true, true, true, true, false);
			for (Job job : dbFolder.getJobs())
				result = result || commonReadAdvice(job.getRights(), findRights(job), user, extractUserRoles(user), false);
			for (Rule rule : dbFolder.getRules())
				result = result || commonReadAdvice(rule.getRights(), findRights(rule), user, extractUserRoles(user), false);
			for (LocalParameter parameter : dbFolder.getParameters())
				result = result || commonReadAdvice(parameter.getRights(), findRights(parameter), user, extractUserRoles(user), false);
			for (Folder f : dbFolder.getFolders())
				result = result || commonReadAdvice(f.getRights(), findRights(f), user, extractUserRoles(user), false);
			for (Folder f : dbFolder.getFolders())
				result = result || hasChildWithReadRight(f, user);
		}
		catch (Throwable t) {
			// hata ignore
		}
		return result;
	}

}

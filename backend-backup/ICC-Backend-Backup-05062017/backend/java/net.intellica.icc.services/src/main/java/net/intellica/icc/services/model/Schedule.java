package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.intellica.icc.services.util.other.ServiceConstants;

@Embeddable
@JsonIgnoreProperties(ignoreUnknown = true)
public class Schedule {

	private Date firstRunDate;
	private RepeatInterval interval;
	private Integer period;

	public Schedule() {
		interval = RepeatInterval.NONE;
	}

	public Schedule(Date firstRunDate) {
		this(firstRunDate, RepeatInterval.NONE, null);
	}

	public Schedule(Date firstRunDate, RepeatInterval interval, Integer period) {
		this.firstRunDate = firstRunDate;
		this.interval = interval;
		this.period = period;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FIRST_RUN_DATE", nullable = false)
	public Date getFirstRunDate() {
		return firstRunDate;
	}

	public void setFirstRunDate(Date firstRunDate) {
		this.firstRunDate = firstRunDate;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "INTERVAL", nullable = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public RepeatInterval getInterval() {
		return interval;
	}

	public void setInterval(RepeatInterval interval) {
		this.interval = interval;
	}

	@Column(name = "PERIOD", nullable = true)
	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}

}

package net.intellica.icc.services.model.operation;

import com.fasterxml.jackson.annotation.JsonIgnoreType;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.intellica.icc.services.util.other.ServiceConstants;

@Entity
@Table(name = "ICC_OPERATION_QUERY_LOG")
@JsonIgnoreType
public class OperationQueryLog {

	private String id;
	private String operationId;
	private String jobId;
	private String jobName;
	private String templateName;
	private Date queryDate;
	private String connectionId;
	private String connectionName;
	private String query;

	public OperationQueryLog() {
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "OPERATION_ID", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getOperationId() {
		return operationId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	@Column(name = "JOB_ID", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	@Column(name = "JOB_NAME", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	@Column(name = "TEMPLATE_NAME", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "QUERY_DATE", nullable = false)
	public Date getQueryDate() {
		return queryDate;
	}

	public void setQueryDate(Date queryDate) {
		this.queryDate = queryDate;
	}

	@Column(name = "CONNECTION_ID", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	@Column(name = "CONNECTION_NAME", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getConnectionName() {
		return connectionName;
	}

	public void setConnectionName(String connectionName) {
		this.connectionName = connectionName;
	}

	@Lob
	@Column(name = "QUERY", nullable = false)
	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

}

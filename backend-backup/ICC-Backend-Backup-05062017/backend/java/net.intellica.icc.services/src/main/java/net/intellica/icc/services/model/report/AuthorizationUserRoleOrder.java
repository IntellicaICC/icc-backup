package net.intellica.icc.services.model.report;

import java.util.Comparator;

public class AuthorizationUserRoleOrder implements Comparator<AuthorizationUserRoleReport> {

	@Override
	public int compare(AuthorizationUserRoleReport r1, AuthorizationUserRoleReport r2) {
		Integer result = r1.getUserName().compareTo(r2.getUserName());
		if (result == 0)
			result = r1.getRoleName().compareTo(r2.getRoleName());
		return result;
	}

}

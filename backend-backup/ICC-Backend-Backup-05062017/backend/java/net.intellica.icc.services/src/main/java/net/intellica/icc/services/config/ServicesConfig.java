package net.intellica.icc.services.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "icc.services")
public class ServicesConfig {

	private static final Integer MIN_THREAD_POOL_COUNT = 10;
	private static final Integer MAX_THREAD_POOL_COUNT = 1000;
	private static final Integer MIN_JOB_TIMEOUT = 300;
	private static final Integer MAX_JOB_TIMEOUT = 7200;
	private static final Integer MIN_RULE_TIMEOUT = 600;
	private static final Integer MAX_RULE_TIMEOUT = 36000;
	private static final Integer MIN_LOCK_FAILURES = 0;
	private static final Integer MAX_LOCK_FAILURES = 100;

	private Boolean setup_mode;
	private Integer min_notification_size;
	private Integer max_notification_size;
	private Boolean quartz_use_weekly_calendar;
	private Integer quartz_thread_pool_count;
	private String personal_pw;
	private Integer job_timeout;
	private Integer rule_timeout;
	private Integer audit_lock_failures;

	public Boolean getSetup_mode() {
		return setup_mode;
	}

	public void setSetup_mode(Boolean template_editing_enabled) {
		this.setup_mode = template_editing_enabled;
	}

	public Integer getMin_notification_size() {
		return min_notification_size;
	}

	public void setMin_notification_size(Integer min_notification_size) {
		this.min_notification_size = min_notification_size;
	}

	public Integer getMax_notification_size() {
		return max_notification_size;
	}

	public void setMax_notification_size(Integer max_notification_size) {
		this.max_notification_size = max_notification_size;
	}

	public Boolean getQuartz_use_weekly_calendar() {
		return quartz_use_weekly_calendar;
	}

	public void setQuartz_use_weekly_calendar(Boolean quartz_use_weekly_calendar) {
		this.quartz_use_weekly_calendar = quartz_use_weekly_calendar;
	}

	public Integer getQuartz_thread_pool_count() {
		return quartz_thread_pool_count;
	}

	public void setQuartz_thread_pool_count(Integer quartz_thread_pool_count) {
		if (quartz_thread_pool_count < MIN_THREAD_POOL_COUNT)
			quartz_thread_pool_count = MIN_THREAD_POOL_COUNT;
		else if (quartz_thread_pool_count > MAX_THREAD_POOL_COUNT)
			quartz_thread_pool_count = MAX_THREAD_POOL_COUNT;
		this.quartz_thread_pool_count = quartz_thread_pool_count;
	}

	public String getPersonal_pw() {
		return personal_pw;
	}

	public void setPersonal_pw(String personal_pw) {
		this.personal_pw = personal_pw;
	}

	public Integer getJob_timeout() {
		return job_timeout;
	}

	public void setJob_timeout(Integer job_timeout) {
		if (job_timeout < MIN_JOB_TIMEOUT)
			job_timeout = MIN_JOB_TIMEOUT;
		else if (job_timeout > MAX_JOB_TIMEOUT)
			job_timeout = MAX_JOB_TIMEOUT;
		this.job_timeout = job_timeout;
	}

	public Integer getRule_timeout() {
		return rule_timeout;
	}

	public void setRule_timeout(Integer rule_timeout) {
		if (rule_timeout < MIN_RULE_TIMEOUT)
			rule_timeout = MIN_RULE_TIMEOUT;
		else if (rule_timeout > MAX_RULE_TIMEOUT)
			rule_timeout = MAX_RULE_TIMEOUT;
		this.rule_timeout = rule_timeout;
	}

	public Integer getAudit_lock_failures() {
		return audit_lock_failures;
	}

	public void setAudit_lock_failures(Integer audit_lock_failures) {
		if (audit_lock_failures < MIN_LOCK_FAILURES)
			audit_lock_failures = MIN_LOCK_FAILURES;
		else if (audit_lock_failures > MAX_LOCK_FAILURES)
			audit_lock_failures = MAX_LOCK_FAILURES;
		this.audit_lock_failures = audit_lock_failures;
	}

}

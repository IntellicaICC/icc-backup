package net.intellica.icc.services.controller;

import net.intellica.icc.services.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/service")
public class TestController {

	@Autowired private ApplicationContext context;

	@RequestMapping(value = { "/test", "/test/" }, method = RequestMethod.GET)
	public Object test() {
		// TEMP
		User currentUser = (User)context.getBean(User.class);
		return currentUser == null ? null : currentUser.getProps().getName();
	}

}

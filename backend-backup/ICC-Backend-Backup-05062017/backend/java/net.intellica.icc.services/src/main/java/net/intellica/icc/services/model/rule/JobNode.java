package net.intellica.icc.services.model.rule;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.HashMap;
import java.util.Map;
import net.intellica.icc.services.model.Job;
import net.intellica.icc.services.model.operation.OperationType;
import net.intellica.icc.services.quartz.QuartzCompileTimeUtil;
import net.intellica.icc.services.quartz.QuartzRuntimeUtil;
import net.intellica.icc.services.service.JobService;
import net.intellica.icc.services.util.dev.HashUtil;
import net.intellica.icc.services.util.other.WorkManager;
import net.intellica.icc.template.exception.InactiveException;
import net.intellica.icc.template.model.GlobalVariable;
import net.intellica.icc.template.model.WorkItem;
import net.intellica.icc.template.model.WorkItemResult;
import org.springframework.util.ObjectUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JobNode extends Node {

	private static final long serialVersionUID = 1L;

	private String jobId;
	private String name;
	private String description;

	private WorkManager workManager;

	public JobNode() {
		super();
		type = NodeType.Job;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonIgnore
	public WorkManager getWorkManager() {
		return workManager;
	}

	public void setWorkManager(WorkManager workManager) {
		this.workManager = workManager;
	}

	@Override
	protected void doWork() {
		WorkItemResult workResult = new WorkItemResult();
		try {
			Map<String, String> parameters = new HashMap<>();
			Job job = getWorkflow().getContextUtil().call(JobService.class).get(jobId, false);
			if (!job.getProps().isActive())
				throw new InactiveException(String.format("Job '%s' is inactive!", job.getProps().getName()));
			String triggerId = getWorkflow().getGlobalVariables().get(GlobalVariable.RuleTriggerID.toString());
			String userId = getWorkflow().getGlobalVariables().get(GlobalVariable.UserID.toString());
			String userName = getWorkflow().getGlobalVariables().get(GlobalVariable.Username.toString());
			String ruleInstanceId = getWorkflow().getGlobalVariables().get(GlobalVariable.RuleInstanceID.toString());
			getWorkflow().getContextUtil().call(QuartzCompileTimeUtil.class).fillParameters(parameters, job, triggerId, userId, userName);
			WorkItem work = getWorkflow().getContextUtil().call(QuartzRuntimeUtil.class).fillParameters(parameters, getInput(), job, OperationType.Rule, ruleInstanceId, getWorkflow().getInstanceChain(), getLastDataset());
			workManager = new WorkManager(getWorkflow().getContextUtil(), work);
			workResult = workManager.runSync();
			if (job.getTemplate().getHasDataset())
				setLastDataset(job.getId());
		}
		catch (Exception e) {
			workResult.setError(e);
		}
		if (workResult.isSuccessful()) {
			getOutput().putAll(workResult.getResults());
			getWorkflow().addQueryLogs(workResult.getQueryLogs());
		}
		else if (workResult.getError() != null)
			setNodeError(workResult.getError());
	}

	@Override
	public void cancel() {
		super.cancel();
		if (getStatus() == NodeStatus.Executing && workManager != null)
			workManager.cancel();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj) &&
			ObjectUtils.nullSafeEquals(getJobId(), ((JobNode)obj).getJobId()) &&
			ObjectUtils.nullSafeEquals(getName(), ((JobNode)obj).getName()) &&
			ObjectUtils.nullSafeEquals(getDescription(), ((JobNode)obj).getDescription());
	}

	@Override
	public int hashCode() {
		return HashUtil.generate(super.hashCode(), jobId, name, description);
	}

}

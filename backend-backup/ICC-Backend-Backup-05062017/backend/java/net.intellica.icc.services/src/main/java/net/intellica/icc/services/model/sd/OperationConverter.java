package net.intellica.icc.services.model.sd;

import com.fasterxml.jackson.databind.util.StdConverter;
import net.intellica.icc.services.model.operation.Operation;

public class OperationConverter extends StdConverter<String, Operation> {

	@Override
	public Operation convert(String value) {
		Operation result = new Operation();
		result.setId(value);
		return result;
	}

}

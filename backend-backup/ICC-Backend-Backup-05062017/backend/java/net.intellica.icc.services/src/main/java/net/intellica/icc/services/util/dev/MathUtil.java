package net.intellica.icc.services.util.dev;

import java.time.LocalDate;
import org.springframework.stereotype.Component;

@Component
public class MathUtil {

	private static final String[] DIGITS = {
		"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
		"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
		"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
	};

	public String convert(Long number) {
		String result = "";
		Integer radix = DIGITS.length;
		while (number >= radix) {
			Long r = number % radix;
			number = number / radix;
			result = DIGITS[(int)(long)r] + result;
		}
		result = DIGITS[(int)(long)number] + result;
		return result;
	}

	public Integer clamp(Integer value, Integer min, Integer max) {
		Integer result = value;
		if (min > max)
			throw new IllegalArgumentException();
		if (result < min)
			result = min;
		else if (result > max)
			result = max;
		return result;
	}

	public LocalDate clamp(LocalDate value, LocalDate min, LocalDate max) {
		LocalDate result = value;
		if (min.isAfter(max))
			throw new IllegalArgumentException();
		if (result.isBefore(min))
			result = min;
		else if (result.isAfter(max))
			result = max;
		return result;
	}

}

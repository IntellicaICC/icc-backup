package net.intellica.icc.services.service;

import java.util.List;
import net.intellica.icc.services.model.LoginLog;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.repo.LoginLogRepository;
import net.intellica.icc.services.util.aop.PreSaveUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class LoginLogService {

	@Autowired private LoginLogRepository repository;
	@Autowired private PreSaveUtil preSaveUtil;

	public LoginLogService() {
	}

	public LoginLog get(String id) {
		return repository.findOne(id);
	}

	public LoginLog save(LoginLog loginLog, User user) {
		preSaveUtil.fill(loginLog, user);
		return repository.save(loginLog);
	}

	public List<LoginLog> list(String userId, Integer page, Integer size) {
		return repository.findAllByUserIdOrderByAttemptDateDesc(userId, new PageRequest(page, size));
	}

}

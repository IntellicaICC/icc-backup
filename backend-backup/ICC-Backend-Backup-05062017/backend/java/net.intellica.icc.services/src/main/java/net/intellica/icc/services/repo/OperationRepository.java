package net.intellica.icc.services.repo;

import java.util.Date;
import java.util.List;
import net.intellica.icc.services.model.RepeatInterval;
import net.intellica.icc.services.model.operation.Operation;
import net.intellica.icc.services.model.operation.OperationSummary;
import net.intellica.icc.services.model.operation.OperationType;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OperationRepository extends PagingAndSortingRepository<Operation, String> {

	Operation findByInstanceId(String instanceId);

	List<Operation> findAllByNextRunDateBetweenOrderByNextRunDateDesc(Date from, Date to);

	List<Operation> findAllByNextRunDateBetweenOrderByNextRunDateDesc(Date from, Date to, Pageable pageable);

	List<Operation> findAllByTriggerIdAndNextRunDateGreaterThan(String triggerId, Date date);

	Operation findByTriggerIdAndTypeAndNextRunDate(String triggerId, OperationType type, Date nextRunDate);

	Operation findFirstByTriggerIdAndTypeAndInstanceIdNotNullOrderByInstanceIdDesc(String triggerId, OperationType type);

	List<Operation> findAllByNextRunDate(Date at);

	List<Operation> findAllByModelIdAndTypeAndSkipNextRunAndSummary(String modelId, OperationType type, Boolean skip, OperationSummary summary);

	List<Operation> findDistinctTriggerIdByTypeAndScheduleIntervalNot(OperationType type, RepeatInterval interval);

	List<Operation> findAllByTriggerIdAndScheduleIntervalNot(String triggerId, RepeatInterval interval);

	Operation findTopByTriggerIdAndTypeAndScheduleIntervalNotOrderByNextRunDateDesc(String triggerId, OperationType type, RepeatInterval interval);

	List<Operation> findAllByTypeAndSkipNextRunAndSummaryAndNextRunDateLessThan(OperationType type, Boolean skip, OperationSummary summary, Date to);

	List<Operation> findAllByUserIdAndTypeAndSkipNextRunAndSummary(String userId, OperationType type, Boolean skip, OperationSummary summary);

	List<Operation> findDistinctTriggerIdByUserIdAndTypeAndScheduleIntervalNot(String userId, OperationType type, RepeatInterval interval);

}

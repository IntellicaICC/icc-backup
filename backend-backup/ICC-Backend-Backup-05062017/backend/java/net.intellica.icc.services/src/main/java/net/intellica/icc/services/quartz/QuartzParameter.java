package net.intellica.icc.services.quartz;

public abstract class QuartzParameter {

	public static final String WORK = "work";
	public static final String PARAMETERS = "parameters";
	public static final String RULE_DETAIL = "ruleDetail";
	public static final String SKIP_REASON = "skipReason";
	public static final String INTERNAL_GROUP = "internalGroup";
	public static final String CALENDAR_WEEKDAYS = "Weekdays";
	public static final String CALENDAR_DEFAULT = "Default";

}

package net.intellica.icc.services.util.config;

import net.intellica.icc.services.config.BackupEngineConfig;
import net.intellica.icc.services.config.EngineConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EngineUtil {

	@Autowired private EngineConfig engineConfig;
	@Autowired private BackupEngineConfig backupEngineConfig;

	public String getStatusUrl() {
		// TODO engine kontrol, calismiyorsa backup
		return engineConfig.getStatus_url();
	}

	public String getResultUrl() {
		// TODO engine kontrol, calismiyorsa backup
		return engineConfig.getResult_url();
	}

	public String getCancelUrl() {
		// TODO engine kontrol, calismiyorsa backup
		return engineConfig.getCancel_url();
	}

	public String getRunUrl() {
		// TODO engine kontrol, calismiyorsa backup
		return engineConfig.getRun_url();
	}

	public String getVersionUrl() {
		// TODO engine kontrol, calismiyorsa backup
		return engineConfig.getVersion_url();
	}

	public String getClearUrl() {
		// TODO engine kontrol, calismiyorsa backup
		return engineConfig.getClear_url();
	}

}

package net.intellica.icc.services.aop;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.AttributeConverter;
import net.intellica.icc.services.model.Connection;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.model.simple.SimpleConnection;
import net.intellica.icc.services.security.AuthorizationUtil;
import net.intellica.icc.services.service.ConnectionService;
import net.intellica.icc.services.util.aop.PreSaveUtil;
import net.intellica.icc.services.util.aop.SimplifyUtil;
import net.intellica.icc.services.util.aop.ValidationUtil;
import net.intellica.icc.services.util.handler.ResponseHandler;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import net.intellica.icc.template.exception.NotFoundException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Aspect
@Component
public class ConnectionAspect {

	@Autowired private ApplicationContext context;
	@Autowired private AuthorizationUtil authorizationUtil;
	@Autowired private ValidationUtil validationUtil;
	@Autowired private ResponseHandler responseHandler;
	@Autowired private SimpleExceptionHandler exceptionHandler;
	@Autowired private PreSaveUtil preSaveUtil;
	@Autowired private SimplifyUtil simplifyUtil;
	@Autowired private AttributeConverter<String, String> ac;
	@Autowired private ConnectionService connectionService;

	@Around("execution(* net.intellica.icc.services.controller.ConnectionController.get(..))")
	public Object getConnection(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			validationUtil.validateId((String)pjp.getArgs()[0]);
			result = pjp.proceed();
			if (result == null)
				throw new NotFoundException();
			else {
				if (!authorizationUtil.isAdminUser())
					result = simplifyUtil.simplify((Connection)result);
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.ConnectionController.list(..))")
	public Object listConnections(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			if (authorizationUtil.isOperatorUser())
				authorizationUtil.throwReadException();
			result = pjp.proceed();
			if (result == null)
				throw new NotFoundException();
			else {
				if (!authorizationUtil.isAdminUser()) {
					Collection<Connection> collection = (Collection<Connection>)result;
					List<SimpleConnection> simples = collection.stream().map(c -> simplifyUtil.simplify(c)).collect(Collectors.toList());
					result = simples;
				}
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.ConnectionController.delete(..))")
	public Object deleteConnection(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			if (!authorizationUtil.isAdminUser())
				authorizationUtil.throwDeleteException();
			validationUtil.validateId((String)pjp.getArgs()[0]);
			String id = (String)pjp.getArgs()[0];
			Connection toDelete = connectionService.get(id, false);
			if (toDelete == null)
				throw new NotFoundException();
			else {
				validationUtil.validateDelete(toDelete);
				result = pjp.proceed();
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.ConnectionController.save(..))")
	public Object saveConnection(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			Connection connection = (Connection)pjp.getArgs()[0];
			Connection dbConnection = StringUtils.isEmpty(connection.getId()) ? null : connectionService.get(connection.getId(), false);
			authorizationUtil.canSaveConnection(connection, dbConnection, true);
			validationUtil.validateSave(connection);
			preSaveUtil.fill(connection, dbConnection, ac);
			result = pjp.proceed();
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

}

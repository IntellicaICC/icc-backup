package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.intellica.icc.services.util.other.ServiceConstants;

@Entity
@Table(name = "ICC_ERROR_LOG")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorLog {

	private String id;
	private ErrorType type;
	private String errorClass;
	private String message;
	private Date errorDate;
	private String userId;
	private String username;

	public ErrorLog() {
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "ERROR_TYPE", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public ErrorType getType() {
		return type;
	}

	public void setType(ErrorType type) {
		this.type = type;
	}

	@Column(name = "ERROR_CLASS", nullable = false, length = ServiceConstants.Model.LONG_STRING_LENGTH)
	public String getErrorClass() {
		return errorClass;
	}

	public void setErrorClass(String errorClass) {
		this.errorClass = errorClass;
	}

	@Lob
	@Column(name = "MESSAGE", nullable = true)
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Column(name = "ERROR_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getErrorDate() {
		return errorDate;
	}

	public void setErrorDate(Date errorDate) {
		this.errorDate = errorDate;
	}

	@Column(name = "USER_ID", nullable = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "USERNAME", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}

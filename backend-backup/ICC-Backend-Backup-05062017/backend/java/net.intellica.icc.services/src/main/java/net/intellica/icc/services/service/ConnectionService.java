package net.intellica.icc.services.service;

import java.util.ArrayList;
import java.util.List;
import net.intellica.icc.services.model.Connection;
import net.intellica.icc.services.repo.ConnectionRepository;
import net.intellica.icc.services.util.audit.HistoryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConnectionService {

	@Autowired private ConnectionRepository repository;
	@Autowired private HistoryUtil historyUtil;

	public ConnectionService() {
	}

	public Connection get(String id, Boolean includeHistory) {
		Connection result = repository.findOne(id);
		if (includeHistory && result == null)
			result = historyUtil.getLastRevision(Connection.class, id);
		return result;
	}

	public Connection save(Connection connection) {
		return repository.save(connection);
	}

	public void delete(String id) {
		repository.delete(id);
	}

	public List<Connection> listAll() {
		List<Connection> result = new ArrayList<>();
		repository.findAllByOrderByPropsNameAsc().forEach(result::add);
		return result;
	}

	public List<Connection> listByName(String name) {
		return repository.findAllByPropsName(name);
	}

}

package net.intellica.icc.services.model.report;

import java.util.Comparator;

public class AuthorizationRoleUserOrder implements Comparator<AuthorizationRoleUserReport> {

	@Override
	public int compare(AuthorizationRoleUserReport r1, AuthorizationRoleUserReport r2) {
		Integer result = r1.getRoleName().compareTo(r2.getRoleName());
		if (result == 0)
			result = r1.getUserName().compareTo(r2.getUserName());
		return result;
	}

}

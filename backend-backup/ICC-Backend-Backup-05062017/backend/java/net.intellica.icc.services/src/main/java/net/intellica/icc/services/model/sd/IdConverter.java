package net.intellica.icc.services.model.sd;

import com.fasterxml.jackson.databind.util.StdConverter;
import net.intellica.icc.services.model.Connection;
import net.intellica.icc.services.model.Folder;
import net.intellica.icc.services.model.GlobalParameter;
import net.intellica.icc.services.model.Job;
import net.intellica.icc.services.model.LocalParameter;
import net.intellica.icc.services.model.Rights;
import net.intellica.icc.services.model.Role;
import net.intellica.icc.services.model.Rule;
import net.intellica.icc.services.model.Template;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.model.Variable;
import net.intellica.icc.services.model.VariableDefinition;
import net.intellica.icc.services.model.operation.Operation;

public class IdConverter extends StdConverter<Object, String> {

	@Override
	public String convert(Object value) {
		String result = null;
		if (value instanceof Connection)
			result = ((Connection)value).getId();
		else if (value instanceof Folder)
			result = ((Folder)value).getId();
		else if (value instanceof GlobalParameter)
			result = ((GlobalParameter)value).getId();
		else if (value instanceof Job)
			result = ((Job)value).getId();
		else if (value instanceof LocalParameter)
			result = ((LocalParameter)value).getId();
		else if (value instanceof Rights)
			result = ((Rights)value).getId();
		else if (value instanceof Role)
			result = ((Role)value).getId();
		else if (value instanceof Rule)
			result = ((Rule)value).getId();
		else if (value instanceof Template)
			result = ((Template)value).getId();
		else if (value instanceof User)
			result = ((User)value).getId();
		else if (value instanceof Variable)
			result = ((Variable)value).getId();
		else if (value instanceof VariableDefinition)
			result = ((VariableDefinition)value).getId();
		else if (value instanceof Operation)
			result = ((Operation)value).getId();
		return result;
	}

}

package net.intellica.icc.services.model.sd;

import com.fasterxml.jackson.databind.util.StdConverter;
import net.intellica.icc.services.model.Template;

public class TemplateConverter extends StdConverter<String, Template> {

	@Override
	public Template convert(String value) {
		Template result = new Template();
		result.setId(value);
		return result;
	}

}

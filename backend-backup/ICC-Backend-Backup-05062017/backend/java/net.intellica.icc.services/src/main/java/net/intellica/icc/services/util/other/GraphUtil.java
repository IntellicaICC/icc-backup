package net.intellica.icc.services.util.other;

import net.intellica.icc.services.model.Job;
import net.intellica.icc.services.model.Rule;
import net.intellica.icc.services.model.rule.Graph;
import net.intellica.icc.services.model.rule.JobNode;
import net.intellica.icc.services.model.rule.Node;
import net.intellica.icc.services.model.rule.NodeType;
import net.intellica.icc.services.model.rule.RuleNode;
import net.intellica.icc.services.service.JobService;
import net.intellica.icc.services.service.RuleService;
import net.intellica.icc.services.util.dev.QuickCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GraphUtil {

	@Autowired private JobService jobService;
	@Autowired private RuleService ruleService;

	public String fillNames(String detail) {
		String result = detail;
		try {
			// node isim ve aciklamalari kaydetme (cunku degisebilir)
			Graph graph = Graph.fromString(detail);
			QuickCache<Job> jobCache = new QuickCache<>();
			jobCache.setGetter(id -> jobService.get(id, false));
			QuickCache<Rule> ruleCache = new QuickCache<>();
			ruleCache.setGetter(id -> ruleService.get(id, false));
			for (Node node : graph.getNodes()) {
				if (node.getType() == NodeType.Job) {
					Job job = jobCache.get(((JobNode)node).getJobId());
					((JobNode)node).setName(job.getProps().getName());
					((JobNode)node).setDescription(job.getProps().getDescription());
				}
				else if (node.getType() == NodeType.Rule) {
					Rule nestedRule = ruleCache.get(((RuleNode)node).getRuleId());
					((RuleNode)node).setName(nestedRule.getProps().getName());
					((RuleNode)node).setDescription(nestedRule.getProps().getDescription());
				}
			}
			result = graph.toString();
		}
		catch (Exception e) {
		}
		return result;
	}

}

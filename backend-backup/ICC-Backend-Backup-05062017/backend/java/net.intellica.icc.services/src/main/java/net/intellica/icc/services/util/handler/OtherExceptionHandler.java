package net.intellica.icc.services.util.handler;

import java.util.LinkedHashMap;
import java.util.Map;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.web.context.request.RequestAttributes;

public class OtherExceptionHandler implements ErrorAttributes {

	private static final String ERROR_ATTRIBUTE = OtherExceptionHandler.class.getName() + ".ERROR";

	@Override
	public Map<String, Object> getErrorAttributes(RequestAttributes requestAttributes, boolean includeStackTrace) {
		Map<String, Object> result = new LinkedHashMap<String, Object>();
		result.put("object", null);
		result.put("errorCode", "ICC-???");
		String errorMessage = getAttribute(requestAttributes, "javax.servlet.error.message");
		result.put("errorMessage", errorMessage);
		return result;
	}

	@Override
	public Throwable getError(RequestAttributes requestAttributes) {
		Throwable exception = getAttribute(requestAttributes, ERROR_ATTRIBUTE);
		if (exception == null) {
			exception = getAttribute(requestAttributes, "javax.servlet.error.exception");
		}
		return exception;
	}

	@SuppressWarnings("unchecked")
	private <T> T getAttribute(RequestAttributes requestAttributes, String name) {
		return (T)requestAttributes.getAttribute(name, RequestAttributes.SCOPE_REQUEST);
	}

}

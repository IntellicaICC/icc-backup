package net.intellica.icc.services.model.operation;

import com.fasterxml.jackson.annotation.JsonIgnoreType;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.intellica.icc.services.util.other.ServiceConstants;

@Entity
@Table(name = "ICC_OPERATION_RULE_LOG")
@JsonIgnoreType
public class OperationRuleLog {

	private String id;
	private String operationId;
	private Date runDate;
	private String ruleId;
	private String ruleName;
	private String ruleDetail;

	public OperationRuleLog() {
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "OPERATION_ID", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getOperationId() {
		return operationId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	@Column(name = "RUN_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getRunDate() {
		return runDate;
	}

	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}

	@Column(name = "RULE_ID", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getRuleId() {
		return ruleId;
	}

	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}

	@Column(name = "RULE_NAME", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

	@Lob
	@Column(name = "RULE_DETAIL", nullable = true)
	public String getRuleDetail() {
		return ruleDetail;
	}

	public void setRuleDetail(String ruleDetail) {
		this.ruleDetail = ruleDetail;
	}

}

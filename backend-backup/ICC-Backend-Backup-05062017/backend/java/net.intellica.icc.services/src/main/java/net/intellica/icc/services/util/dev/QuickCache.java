package net.intellica.icc.services.util.dev;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import org.springframework.stereotype.Component;

@Component
public class QuickCache<T> {

	private Function<String, T> getter;
	private Map<String, T> cache;

	public QuickCache() {
		cache = new HashMap<>();
	}

	public void setGetter(Function<String, T> getter) {
		this.getter = getter;
	}

	public T get(String id) {
		T result = cache.get(id);
		if (result == null) {
			result = getter.apply(id);
			if (result != null)
				cache.put(id, result);
		}
		return result;
	}

}

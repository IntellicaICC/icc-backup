package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.Arrays;

public enum RepeatInterval {

	NONE,
	MINUTES,
	HOURS,
	DAYS,
	WEEKS,
	MONTHS;

	@JsonCreator
	public static RepeatInterval fromString(String string) {
		return Arrays.asList(RepeatInterval.values()).stream().filter(s -> s.toString().equals(string)).findFirst().orElse(null);
	}

}

package net.intellica.icc.services.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.intellica.icc.services.model.operation.OperationRuleLog;
import net.intellica.icc.services.repo.OperationRuleLogRepository;
import net.intellica.icc.services.util.aop.PreSaveUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OperationRuleLogService {

	@Autowired private OperationRuleLogRepository repository;
	@Autowired private PreSaveUtil preSaveUtil;

	public OperationRuleLogService() {
	}

	public OperationRuleLog get(String id) {
		return repository.findOne(id);
	}

	public OperationRuleLog save(OperationRuleLog operationLog) {
		preSaveUtil.fill(operationLog);
		return repository.save(operationLog);
	}

	public List<OperationRuleLog> saveAll(List<OperationRuleLog> logs) {
		List<OperationRuleLog> result = new ArrayList<>();
		preSaveUtil.fillAllOperationRuleLogs(logs);
		repository.save(logs).forEach(result::add);
		return result;
	}

	public List<OperationRuleLog> list(String operationId) {
		return repository.findAllByOperationIdOrderByRunDateDesc(operationId);
	}

	public void deleteAll(Collection<OperationRuleLog> logs) {
		repository.delete(logs);
	}

}

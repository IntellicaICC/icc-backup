package net.intellica.icc.services.util.aop;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.persistence.AttributeConverter;
import net.intellica.icc.services.model.Connection;
import net.intellica.icc.services.model.ErrorLog;
import net.intellica.icc.services.model.Folder;
import net.intellica.icc.services.model.GlobalParameter;
import net.intellica.icc.services.model.Job;
import net.intellica.icc.services.model.License;
import net.intellica.icc.services.model.LocalParameter;
import net.intellica.icc.services.model.LoginLog;
import net.intellica.icc.services.model.Notification;
import net.intellica.icc.services.model.Rights;
import net.intellica.icc.services.model.Role;
import net.intellica.icc.services.model.Rule;
import net.intellica.icc.services.model.ServiceLog;
import net.intellica.icc.services.model.Setting;
import net.intellica.icc.services.model.SettingDefinition;
import net.intellica.icc.services.model.Template;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.model.UserType;
import net.intellica.icc.services.model.Variable;
import net.intellica.icc.services.model.VariableDefinition;
import net.intellica.icc.services.model.operation.CommandLog;
import net.intellica.icc.services.model.operation.Operation;
import net.intellica.icc.services.model.operation.OperationLog;
import net.intellica.icc.services.model.operation.OperationQueryLog;
import net.intellica.icc.services.model.operation.OperationRuleLog;
import net.intellica.icc.services.model.rule.Graph;
import net.intellica.icc.services.model.rule.JobNode;
import net.intellica.icc.services.model.rule.Node;
import net.intellica.icc.services.model.rule.NodeType;
import net.intellica.icc.services.model.rule.RuleNode;
import net.intellica.icc.services.security.BcryptUtil;
import net.intellica.icc.services.util.other.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

@Component
public class PreSaveUtil {

	@Autowired private IdGenerator idGenerator;
	@Autowired private BcryptUtil bcryptUtil;
	@Autowired private ApplicationContext context;

	public void fill(Folder folder, Folder dbFolder) {
		User currentUser = context.getBean(User.class);
		Date now = new Date();
		if (folder.getRights() == null)
			folder.setRights(new Rights());
		if (dbFolder == null) {// new creation
			folder.setId(idGenerator.generateDatabaseId());
			folder.getProps().setCreatedBy(currentUser.getId());
			folder.getProps().setCreationDate(now);
			folder.getRights().setId(idGenerator.generateDatabaseId());
			if (currentUser.getUserType() == UserType.Admin) {
				if (StringUtils.isEmpty(folder.getRights().getOwner()))
					folder.getRights().setOwner(currentUser.getId());
				setInheritsFromParent(folder.getRights());
			}
			else {
				folder.getRights().setInheritsFromParent(true);
				folder.getRights().setOwner(currentUser.getId());
				folder.getRights().setRoleRights(new HashMap<>());
				folder.getRights().setRoleRights(new HashMap<>());
			}
		}
		else {// patch existing
			folder.getProps().setCreatedBy(dbFolder.getProps().getCreatedBy());
			folder.getProps().setCreationDate(dbFolder.getProps().getCreationDate());
			folder.getRights().setId(dbFolder.getRights().getId());
			if (currentUser.getUserType() == UserType.Admin) {
				if (StringUtils.isEmpty(folder.getRights().getOwner()))
					folder.getRights().setOwner(dbFolder.getRights().getOwner());
				setInheritsFromParent(folder.getRights());
			}
			else
				folder.setRights(dbFolder.getRights());
		}
		folder.getProps().setModifiedBy(currentUser.getId());
		folder.getProps().setModificationDate(now);
	}

	public void fill(Job job, Job dbJob) {
		User currentUser = context.getBean(User.class);
		Date now = new Date();
		if (job.getRights() == null)
			job.setRights(new Rights());
		if (dbJob == null) {
			// new creation
			job.setId(idGenerator.generateDatabaseId());
			job.getProps().setCreatedBy(currentUser.getId());
			job.getProps().setCreationDate(now);
			job.getRights().setId(idGenerator.generateDatabaseId());
			if (currentUser.getUserType() == UserType.Admin) {
				if (StringUtils.isEmpty(job.getRights().getOwner()))
					job.getRights().setOwner(currentUser.getId());
				setInheritsFromParent(job.getRights());
			}
			else {
				job.getRights().setInheritsFromParent(true);
				job.getRights().setOwner(currentUser.getId());
				job.getRights().setRoleRights(new HashMap<>());
				job.getRights().setRoleRights(new HashMap<>());
			}
			for (Variable v : job.getVariables()) {
				v.setJob(job);
				v.setId(idGenerator.generateDatabaseId());
			}
		}
		else {
			// patch existing
			job.getProps().setCreatedBy(dbJob.getProps().getCreatedBy());
			job.getProps().setCreationDate(dbJob.getProps().getCreationDate());
			job.getRights().setId(dbJob.getRights().getId());
			if (currentUser.getUserType() == UserType.Admin) {
				if (StringUtils.isEmpty(job.getRights().getOwner()))
					job.getRights().setOwner(dbJob.getRights().getOwner());
				setInheritsFromParent(job.getRights());
			}
			else
				job.setRights(dbJob.getRights());
			job.setTemplate(dbJob.getTemplate());
			if (CollectionUtils.isEmpty(job.getVariables()))
				job.setVariables(dbJob.getVariables());
			else {
				for (Variable v : job.getVariables()) {
					v.setJob(job);
					for (Variable dbv : dbJob.getVariables()) {
						if (dbv.getDefinition().getId().equals(v.getDefinition().getId())) {
							v.setId(dbv.getId());
							break;
						}
					}
				}
			}
		}
		job.getProps().setModifiedBy(currentUser.getId());
		job.getProps().setModificationDate(now);
	}

	public void fill(Rule rule, Rule dbRule) {
		User currentUser = context.getBean(User.class);
		Date now = new Date();
		if (rule.getRights() == null)
			rule.setRights(new Rights());
		if (dbRule == null) {// new creation
			rule.setId(idGenerator.generateDatabaseId());
			rule.getProps().setCreatedBy(currentUser.getId());
			rule.getProps().setCreationDate(now);
			rule.getRights().setId(idGenerator.generateDatabaseId());
			if (currentUser.getUserType() == UserType.Admin) {
				if (StringUtils.isEmpty(rule.getRights().getOwner()))
					rule.getRights().setOwner(currentUser.getId());
				setInheritsFromParent(rule.getRights());
			}
			else {
				rule.getRights().setInheritsFromParent(true);
				rule.getRights().setOwner(currentUser.getId());
				rule.getRights().setRoleRights(new HashMap<>());
				rule.getRights().setRoleRights(new HashMap<>());
			}
		}
		else {// patch existing
			rule.getProps().setCreatedBy(dbRule.getProps().getCreatedBy());
			rule.getProps().setCreationDate(dbRule.getProps().getCreationDate());
			rule.getRights().setId(dbRule.getRights().getId());
			if (currentUser.getUserType() == UserType.Admin) {
				if (StringUtils.isEmpty(rule.getRights().getOwner()))
					rule.getRights().setOwner(dbRule.getRights().getOwner());
				setInheritsFromParent(rule.getRights());
			}
			else
				rule.setRights(dbRule.getRights());
		}
		rule.getProps().setModifiedBy(currentUser.getId());
		rule.getProps().setModificationDate(now);
		try {
			// node isim ve aciklamalari kaydetme (cunku degisebilir)
			Graph graph = Graph.fromString(rule.getDetail());
			for (Node node : graph.getNodes()) {
				if (node.getType() == NodeType.Job) {
					((JobNode)node).setName("");
					((JobNode)node).setDescription("");
				}
				else if (node.getType() == NodeType.Rule) {
					((RuleNode)node).setName("");
					((RuleNode)node).setDescription("");
				}
			}
			rule.setDetail(graph.toString());
		}
		catch (Exception e) {
		}
	}

	public void fill(LocalParameter parameter, LocalParameter dbParameter) {
		User currentUser = context.getBean(User.class);
		Date now = new Date();
		if (parameter.getRights() == null)
			parameter.setRights(new Rights());
		if (dbParameter == null) {// new creation
			parameter.setId(idGenerator.generateDatabaseId());
			parameter.getProps().setCreatedBy(currentUser.getId());
			parameter.getProps().setCreationDate(now);
			parameter.getRights().setId(idGenerator.generateDatabaseId());
			if (currentUser.getUserType() == UserType.Admin) {
				if (StringUtils.isEmpty(parameter.getRights().getOwner()))
					parameter.getRights().setOwner(currentUser.getId());
				setInheritsFromParent(parameter.getRights());
			}
			else {
				parameter.getRights().setInheritsFromParent(true);
				parameter.getRights().setOwner(currentUser.getId());
				parameter.getRights().setRoleRights(new HashMap<>());
				parameter.getRights().setRoleRights(new HashMap<>());
			}
		}
		else {// patch existing
			parameter.getProps().setCreatedBy(dbParameter.getProps().getCreatedBy());
			parameter.getProps().setCreationDate(dbParameter.getProps().getCreationDate());
			parameter.getRights().setId(dbParameter.getRights().getId());
			if (currentUser.getUserType() == UserType.Admin) {
				if (StringUtils.isEmpty(parameter.getRights().getOwner()))
					parameter.getRights().setOwner(dbParameter.getRights().getOwner());
				setInheritsFromParent(parameter.getRights());
			}
			else
				parameter.setRights(dbParameter.getRights());
		}
		parameter.getProps().setModifiedBy(currentUser.getId());
		parameter.getProps().setModificationDate(now);
	}

	public void fill(GlobalParameter parameter, GlobalParameter dbParameter) {
		User currentUser = context.getBean(User.class);
		Date now = new Date();
		if (dbParameter == null) {// new creation
			parameter.setId(idGenerator.generateDatabaseId());
			parameter.getProps().setCreatedBy(currentUser.getId());
			parameter.getProps().setCreationDate(now);
		}
		else {// patch existing
			parameter.getProps().setCreatedBy(dbParameter.getProps().getCreatedBy());
			parameter.getProps().setCreationDate(dbParameter.getProps().getCreationDate());
		}
		parameter.getProps().setModifiedBy(currentUser.getId());
		parameter.getProps().setModificationDate(now);
	}

	public void fill(Connection connection, Connection dbConnection, AttributeConverter<String, String> ac) {
		User currentUser = context.getBean(User.class);
		Date now = new Date();
		if (dbConnection == null) {// new creation
			connection.setId(idGenerator.generateDatabaseId());
			connection.getProps().setCreatedBy(currentUser.getId());
			connection.getProps().setCreationDate(now);
		}
		else {// patch existing
			connection.getProps().setCreatedBy(dbConnection.getProps().getCreatedBy());
			connection.getProps().setCreationDate(dbConnection.getProps().getCreationDate());
		}
		connection.getProps().setModifiedBy(currentUser.getId());
		connection.getProps().setModificationDate(now);
		if (!StringUtils.isEmpty(connection.getDatabasePassword())) {
			connection.setDatabasePassword(ac.convertToDatabaseColumn(connection.getDatabasePassword()));
		}
	}

	public void fill(Role role, Role dbRole) {
		User currentUser = context.getBean(User.class);
		Date now = new Date();
		if (dbRole == null) {// new creation
			role.setId(idGenerator.generateDatabaseId());
			role.getProps().setCreatedBy(currentUser.getId());
			role.getProps().setCreationDate(now);
		}
		else {// patch existing
			role.getProps().setCreatedBy(dbRole.getProps().getCreatedBy());
			role.getProps().setCreationDate(dbRole.getProps().getCreationDate());
		}
		role.getProps().setModifiedBy(currentUser.getId());
		role.getProps().setModificationDate(now);
	}

	public void fill(User user, User dbUser, String generated) {
		User currentUser = context.getBean(User.class);
		Date now = new Date();
		if (dbUser == null) {// new creation
			user.setId(idGenerator.generateDatabaseId());
			user.getProps().setCreatedBy(currentUser.getId());
			user.getProps().setCreationDate(now);
			user.setPassword(bcryptUtil.getEncoder().encode(generated));
		}
		else {// patch existing
			user.getProps().setCreatedBy(dbUser.getProps().getCreatedBy());
			user.getProps().setCreationDate(dbUser.getProps().getCreationDate());
			user.setPassword(dbUser.getPassword());
		}
		user.getProps().setModifiedBy(currentUser.getId());
		user.getProps().setModificationDate(now);
	}

	public void fill(Template template, Template dbTemplate) {
		User currentUser = context.getBean(User.class);
		Date now = new Date();
		if (dbTemplate == null) {// new creation
			template.setId(idGenerator.generateDatabaseId());
//			template.getProps().setCreatedBy(currentUser.getId());
//			template.getProps().setCreationDate(now);
			for (VariableDefinition v : template.getVariables()) {
				v.setTemplate(template);
				v.setId(idGenerator.generateDatabaseId());
				v.getProps().setCreatedBy(currentUser.getId());
				v.getProps().setCreationDate(now);
			}
		}
		else {// patch existing
//			template.getProps().setCreatedBy(dbTemplate.getProps().getCreatedBy());
//			template.getProps().setCreationDate(dbTemplate.getProps().getCreationDate());
			for (VariableDefinition v : template.getVariables()) {
				v.setTemplate(template);
				for (VariableDefinition dbv : dbTemplate.getVariables()) {
					if (dbv.getTemplate().getId().equals(v.getTemplate().getId())) {
						v.setId(dbv.getId());
						v.getProps().setCreatedBy(dbv.getProps().getCreatedBy());
						v.getProps().setCreationDate(dbv.getProps().getCreationDate());
						break;
					}
				}
			}
		}
//		template.getProps().setModifiedBy(currentUser.getId());
//		template.getProps().setModificationDate(now);
		for (VariableDefinition v : template.getVariables()) {
			v.getProps().setModifiedBy(currentUser.getId());
			v.getProps().setModificationDate(now);
		}
	}

	public void fill(ErrorLog errorLog, User user) {
		Date now = new Date();
		errorLog.setId(idGenerator.generateDatabaseId());
		errorLog.setErrorDate(now);
		errorLog.setUserId(user.getId());
		errorLog.setUsername(user.getProps().getName());
	}

	public void fill(Operation operation) {
		if (StringUtils.isEmpty(operation.getId()))
			operation.setId(idGenerator.generateDatabaseId());
		if (operation.getDetail() != null) {
			operation.getDetail().getResults().stream()
				.filter(r -> StringUtils.isEmpty(r.getId()))
				.forEach(r -> r.setId(idGenerator.generateDatabaseId()));
		}
	}

	public void fillAllOperations(List<Operation> operations) {
		User currentUser = context.getBean(User.class);
		Date now = new Date();
		operations.forEach(o -> o.setId(idGenerator.generateDatabaseId()));
		operations.forEach(o -> o.setUserId(currentUser.getId()));
		operations.forEach(o -> o.setUserName(currentUser.getProps().getName()));
		operations.forEach(o -> o.setSetDate(now));
	}

	public void fill(OperationLog log) {
		log.setId(idGenerator.generateDatabaseId());
	}

	public void fillAllOperationLogs(List<OperationLog> logs) {
		logs.forEach(this::fill);
	}

	public void fillAllNotifications(List<Notification> notifications) {
		notifications.forEach(this::fill);
	}

	public void fill(Notification notification) {
		if (StringUtils.isEmpty(notification.getId())) {
			notification.setId(idGenerator.generateDatabaseId());
			notification.setNotificationDate(new Date());
		}
	}

	public void fill(CommandLog commandLog) {
		User currentUser = context.getBean(User.class);
		Date now = new Date();
		commandLog.setId(idGenerator.generateDatabaseId());
		commandLog.setCommandDate(now);
		commandLog.setUserId(currentUser.getId());
		commandLog.setUserName(currentUser.getProps().getName());
	}

	public void fill(License license) {
		license.setId(idGenerator.generateDatabaseId());
	}

	public void fill(SettingDefinition definition, SettingDefinition dbDefinition) {
		if (dbDefinition == null)
			definition.setId(idGenerator.generateDatabaseId());
	}

	public void fill(Setting setting) {
		fill(setting, new Date());
	}

	public void fill(Setting setting, Date date) {
		if (StringUtils.isEmpty(setting.getId()))
			setting.setId(idGenerator.generateDatabaseId());
		setting.setModificationDate(date);
	}

	public void fill(Collection<Setting> settings) {
		Date now = new Date();
		settings.forEach(s -> fill(s, now));
	}

	public void fill(OperationRuleLog ruleLog) {
		ruleLog.setId(idGenerator.generateDatabaseId());
	}

	public void fillAllOperationRuleLogs(List<OperationRuleLog> ruleLogs) {
		ruleLogs.forEach(this::fill);
	}

	public void fill(OperationQueryLog queryLog) {
		queryLog.setId(idGenerator.generateDatabaseId());
	}

	public void fillAllOperationQueryLogs(List<OperationQueryLog> queryLogs) {
		queryLogs.forEach(this::fill);
	}

	private void setInheritsFromParent(Rights rights) {
		if (rights.getInheritsFromParent() == null)
			rights.setInheritsFromParent(true);
		if (rights.getInheritsFromParent()) {
			rights.setRoleRights(new HashMap<>());
			rights.setUserRights(new HashMap<>());
		}
	}

	public void fill(ServiceLog serviceLog) {
		User currentUser = context.getBean(User.class);
		Date now = new Date();
		serviceLog.setId(idGenerator.generateDatabaseId());
		serviceLog.setCallDate(now);
		serviceLog.setUserId(currentUser.getId());
		serviceLog.setUsername(currentUser.getProps().getName());
	}

	public void fill(LoginLog loginLog, User user) {
		Date now = new Date();
		loginLog.setId(idGenerator.generateDatabaseId());
		loginLog.setAttemptDate(now);
		loginLog.setUserId(user.getId());
		loginLog.setUserName(user.getProps().getName());
	}

}

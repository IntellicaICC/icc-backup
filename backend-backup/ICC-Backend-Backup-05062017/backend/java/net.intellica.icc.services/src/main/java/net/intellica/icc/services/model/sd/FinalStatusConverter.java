package net.intellica.icc.services.model.sd;

import com.fasterxml.jackson.databind.util.StdConverter;
import net.intellica.icc.services.model.operation.OperationLiveStatus;

public class FinalStatusConverter extends StdConverter<String, OperationLiveStatus> {

	@Override
	public OperationLiveStatus convert(String value) {
		OperationLiveStatus result = new OperationLiveStatus();
		try {
			result = OperationLiveStatus.fromString(value);
		}
		catch (Exception e) {
		}
		return result;
	}

}

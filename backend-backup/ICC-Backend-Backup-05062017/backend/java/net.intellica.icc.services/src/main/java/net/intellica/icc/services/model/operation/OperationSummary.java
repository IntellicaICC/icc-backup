package net.intellica.icc.services.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.Arrays;

public enum OperationSummary {

	Success,
	Error;

	@JsonCreator
	public static OperationSummary fromString(String string) {
		return Arrays.asList(OperationSummary.values()).stream().filter(s -> s.toString().equals(string)).findFirst().orElse(null);
	}

}

package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import net.intellica.icc.services.util.other.ServiceConstants;
import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "ICC_USER", uniqueConstraints = @UniqueConstraint(columnNames = "NAME") )
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

	private String id;
	private CommonProperties props;
	private String email;
	private String firstName;
	private String lastName;
	private String password;
	private LoginType loginType;
	private UserType userType;
	private Boolean locked;
	private Set<Role> roles;

	public User() {
		props = new CommonProperties();
		email = "";
		firstName = "";
		lastName = "";
		password = "";
		loginType = LoginType.Default;
		userType = UserType.Normal;
		locked = false;
		roles = new HashSet<>();
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Embedded
	public CommonProperties getProps() {
		return props;
	}

	public void setProps(CommonProperties props) {
		this.props = props;
	}

	@Column(name = "EMAIL", nullable = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "FIRST_NAME", nullable = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "LAST_NAME", nullable = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "PASSWORD", nullable = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonProperty
	public void setPassword(String password) {
		this.password = password;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "LOGIN_TYPE", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public LoginType getLoginType() {
		return loginType;
	}

	public void setLoginType(LoginType loginType) {
		this.loginType = loginType;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "USER_TYPE", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	@Column(name = "LOCKED", nullable = false)
	public Boolean isLocked() {
		return locked;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}

	@ManyToMany(mappedBy = "users", fetch = FetchType.EAGER)
	@JsonIgnore
	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Transient
	@JsonIgnore
	public String getFullName() {
		return firstName + " " + lastName;
	}

}

package net.intellica.icc.services.security;

import java.util.Arrays;
import java.util.Collection;
import javax.persistence.AttributeConverter;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.util.dev.StringUtil;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class SecurityDetails implements UserDetails {

	private static final long serialVersionUID = 1L;

	private Boolean active;
	private String username;
	private String password;
	private String authority;
	private Boolean locked;

	public SecurityDetails(User user, AttributeConverter<String, String> ac) {
		active = user.getProps().isActive();
		username = user.getProps().getName();
		password = user.getPassword();
		authority = "ROLE_" + StringUtil.toUpper(user.getUserType());
		locked = user.isLocked();
	}

	@Override
	public boolean isEnabled() {
		return active;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return !locked;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Arrays.asList(new SimpleGrantedAuthority(authority));
	}

}

package net.intellica.icc.services.model.simple;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import net.intellica.icc.services.model.CommonProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SimpleProperties extends CommonProperties {

	@Override
	@JsonIgnore
	public String getDescription() {
		return super.getDescription();
	}

	@Override
	@JsonIgnore
	public String getCreatedBy() {
		return super.getCreatedBy();
	}

	@Override
	@JsonIgnore
	public Date getCreationDate() {
		return super.getCreationDate();
	}

	@Override
	@JsonIgnore
	public String getModifiedBy() {
		return super.getModifiedBy();
	}

	@Override
	@JsonIgnore
	public Date getModificationDate() {
		return super.getModificationDate();
	}

}

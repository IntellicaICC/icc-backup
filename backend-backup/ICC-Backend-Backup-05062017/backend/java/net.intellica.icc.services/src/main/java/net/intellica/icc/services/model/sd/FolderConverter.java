package net.intellica.icc.services.model.sd;

import com.fasterxml.jackson.databind.util.StdConverter;
import net.intellica.icc.services.model.Folder;

public class FolderConverter extends StdConverter<String, Folder> {

	@Override
	public Folder convert(String value) {
		Folder result = new Folder();
		result.setId(value);
		return result;
	}

}

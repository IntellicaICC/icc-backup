package net.intellica.icc.services.service;

import java.util.ArrayList;
import java.util.List;
import net.intellica.icc.services.model.GlobalParameter;
import net.intellica.icc.services.repo.GlobalParameterRepository;
import net.intellica.icc.services.util.audit.HistoryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GlobalParameterService {

	@Autowired private GlobalParameterRepository repository;
	@Autowired private HistoryUtil historyUtil;

	public GlobalParameterService() {
	}

	public GlobalParameter get(String id, Boolean includeHistory) {
		GlobalParameter result = repository.findOne(id);
		if (includeHistory && result == null)
			result = historyUtil.getLastRevision(GlobalParameter.class, id);
		return result;
	}

	public GlobalParameter save(GlobalParameter parameter) {
		return repository.save(parameter);
	}

	public void delete(String id) {
		repository.delete(id);
	}

	public List<GlobalParameter> listAll() {
		List<GlobalParameter> result = new ArrayList<>();
		repository.findAllByOrderByPropsNameAsc().forEach(result::add);
		return result;
	}

	public List<GlobalParameter> listByName(String name) {
		return repository.findAllByPropsName(name);
	}

}

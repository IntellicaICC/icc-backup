package net.intellica.icc.services.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.intellica.icc.services.model.operation.OperationQueryLog;
import net.intellica.icc.services.repo.OperationQueryLogRepository;
import net.intellica.icc.services.util.aop.PreSaveUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OperationQueryLogService {

	@Autowired private OperationQueryLogRepository repository;
	@Autowired private PreSaveUtil preSaveUtil;

	public OperationQueryLogService() {
	}

	public OperationQueryLog get(String id) {
		return repository.findOne(id);
	}

	public OperationQueryLog save(OperationQueryLog operationLog) {
		preSaveUtil.fill(operationLog);
		return repository.save(operationLog);
	}

	public List<OperationQueryLog> saveAll(List<OperationQueryLog> logs) {
		List<OperationQueryLog> result = new ArrayList<>();
		preSaveUtil.fillAllOperationQueryLogs(logs);
		repository.save(logs).forEach(result::add);
		return result;
	}

	public List<OperationQueryLog> list(String operationId) {
		return repository.findAllByOperationIdOrderByQueryDateDesc(operationId);
	}

	public void deleteAll(Collection<OperationQueryLog> logs) {
		repository.delete(logs);
	}

}

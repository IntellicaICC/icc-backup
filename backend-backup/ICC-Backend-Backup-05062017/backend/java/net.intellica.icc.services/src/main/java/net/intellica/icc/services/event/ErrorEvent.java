package net.intellica.icc.services.event;

import java.io.PrintWriter;
import java.io.StringWriter;
import net.intellica.icc.services.model.ErrorLog;
import net.intellica.icc.services.model.ErrorType;
import net.intellica.icc.services.model.User;
import net.intellica.icc.template.exception.DatabaseException;
import net.intellica.icc.template.exception.TechnicalException;
import net.intellica.icc.template.exception.ValidationException;

public class ErrorEvent {

	private ErrorLog errorLog;
	private User user;

	public ErrorEvent(Exception exception, User user) {
		errorLog = new ErrorLog();
		errorLog.setErrorClass(exception.getClass().getSimpleName());
		try (StringWriter sw = new StringWriter()) {
			try (PrintWriter pw = new PrintWriter(sw)) {
				exception.printStackTrace(pw);
				errorLog.setMessage(sw.toString());
			}
		}
		catch (Exception e) {
		}
		errorLog.setType(selectType(exception));
		this.user = user;
	}

	private ErrorType selectType(Exception exception) {
		ErrorType result = ErrorType.Technical;
		if (exception instanceof DatabaseException)
			result = ErrorType.Database;
		else if (exception instanceof ValidationException)
			result = ErrorType.Validation;
		else if (exception instanceof TechnicalException)
			result = ErrorType.Technical;
		return result;
	}

	public ErrorLog getErrorLog() {
		return errorLog;
	}

	public User getUser() {
		return user;
	}

}

package net.intellica.icc.services.service;

import java.util.Date;
import java.util.List;
import net.intellica.icc.services.model.ErrorLog;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.repo.ErrorLogRepository;
import net.intellica.icc.services.util.aop.PreSaveUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class ErrorLogService {

	@Autowired private ErrorLogRepository repository;
	@Autowired private PreSaveUtil preSaveUtil;

	public ErrorLogService() {
	}

	public ErrorLog get(String id) {
		return repository.findOne(id);
	}

	public ErrorLog save(ErrorLog errorLog, User user) {
		preSaveUtil.fill(errorLog, user);
		return repository.save(errorLog);
	}

	public List<ErrorLog> list(Date date, Integer page, Integer size) {
		return repository.findAllByErrorDateOrderByErrorDateDesc(date, new PageRequest(page, size));
	}

}

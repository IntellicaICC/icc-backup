package net.intellica.icc.services.controller;

import net.intellica.icc.services.model.Credentials;
import net.intellica.icc.services.security.BcryptUtil;
import net.intellica.icc.services.util.config.EngineUtil;
import net.intellica.icc.template.exception.ForbiddenException;
import net.intellica.icc.template.exception.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class GeneralController {

	private static final String VERSION = "0.2";
	private static final String BUILD = "309";

	@Autowired private EngineUtil engineUtil;
	@Autowired private UserDetailsService detailsService;
	@Autowired private BcryptUtil bcryptUtil;

	@RequestMapping({ "/version", "/version/" })
	public Object version() {
		String result = "ICC Services - version " + VERSION + " build " + BUILD;
		try {
			RestTemplate restTemplate = new RestTemplate();
			String engineVersion = restTemplate.getForObject(engineUtil.getVersionUrl(), String.class);
			result += "\r\n" + engineVersion;
		}
		catch (Exception e) {
			result += "\r\nICC Engine is offline.";
		}
		return result;
	}

	@RequestMapping(value = { "/login", "/login/" }, method = RequestMethod.POST)
	public Object login(@RequestBody Credentials credentials) {
		try {
			if (!StringUtils.isEmpty(credentials.getUsername())) {
				UserDetails details = detailsService.loadUserByUsername(credentials.getUsername());
				if (!details.isEnabled())
					throw new ForbiddenException(Messages.Login.PASSIVE);
				else if (!details.isAccountNonLocked())
					throw new ForbiddenException(Messages.Login.LOCKED);
				else if (!bcryptUtil.getEncoder().matches(credentials.getPassword(), details.getPassword()))
					throw new ForbiddenException(Messages.Login.INVALID);
			}
			else
				throw new ForbiddenException(Messages.Login.INVALID);
		}
		catch (UsernameNotFoundException e) {
			throw new ForbiddenException(Messages.Login.INVALID);
		}
		return null;
	}

}

package net.intellica.icc.services.util.dev;

import java.util.Stack;
import net.intellica.icc.template.exception.DuplicateNodeException;

public class NodeStack extends Stack<Integer> {

	private static final long serialVersionUID = 1L;

	@Override
	public Integer push(Integer item) {
		if (contains(item))
			throw new DuplicateNodeException();
		return super.push(item);
	}

}

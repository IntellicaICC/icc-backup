package net.intellica.icc.services.aop;

import net.intellica.icc.services.model.SampleQuery;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.util.aop.ValidationUtil;
import net.intellica.icc.services.util.handler.ResponseHandler;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class UtilityAspect {

	@Autowired private ApplicationContext context;
	@Autowired private ValidationUtil validationUtil;
	@Autowired private ResponseHandler responseHandler;
	@Autowired private SimpleExceptionHandler exceptionHandler;

	@Around("execution(* net.intellica.icc.services.controller.UtilityController.query(..))")
	public Object query(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			SampleQuery query = (SampleQuery)pjp.getArgs()[0];
			validationUtil.validateQuery(query);
			result = pjp.proceed();
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

}

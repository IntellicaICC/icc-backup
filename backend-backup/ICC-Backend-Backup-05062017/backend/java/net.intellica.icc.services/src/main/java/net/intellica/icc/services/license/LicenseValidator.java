package net.intellica.icc.services.license;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import me.sniggle.pgp.crypt.MessageEncryptor;
import me.sniggle.pgp.crypt.PGPWrapperFactory;
import net.intellica.icc.services.config.ServicesConfig;
import net.intellica.icc.services.model.License;
import net.intellica.icc.services.service.LicenseService;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class LicenseValidator {

	@Value("#{ systemProperties['user.dir'] }") private String userDir;
	@Value("#{ systemProperties['file.separator'] }") private String fileSeparator;

	private static final String PGP_HEADER = "-----BEGIN PGP MESSAGE-----\nVersion: GnuPG v2\n\n";
	private static final String PGP_FOOTER = "\n-----END PGP MESSAGE-----\n";
	private static final String LICENSE_FORMAT = "ICC is licensed to [\\w ]+.";
	private static final String UNLICENSED = "ICC is not licensed.";

	private final String password;
	private Boolean matches;
	private String message;

	@Autowired private SimpleExceptionHandler exceptionHandler;

	@Autowired
	public LicenseValidator(ServicesConfig servicesConfig, LicenseService licenseService) {
		password = servicesConfig.getPersonal_pw();
		matches = false;
		message = UNLICENSED;
		try {
			License license = licenseService.get();
			if (license != null)
				validateLicense(license);
		}
		catch (Exception e) {
		}
	}

	public void validateLicense(License license) throws Exception {
		PGPWrapperFactory.init();
		MessageEncryptor encryptor = PGPWrapperFactory.getEncyptor();
		InputStream privateStream = createKeyStream();
		InputStream licenseStream = createLicenseStream(formPgp(license.getLicense()));
		ByteArrayOutputStream outputStream = createOutputStream();
		if (encryptor.decrypt(password, privateStream, licenseStream, outputStream)) {
			String temp = new String(outputStream.toByteArray(), java.nio.charset.StandardCharsets.UTF_8);
			matches = temp.matches(LICENSE_FORMAT);
			if (matches)
				message = temp;
		}
		if (!isValid())
			exceptionHandler.warning("No valid license found!");
	}

	public Boolean isValid() {
		return matches;
	}

	public String getMessage() {
		return message;
	}

	private String formPgp(String text) {
		return PGP_HEADER + text + PGP_FOOTER;
	}

	private InputStream createKeyStream() throws FileNotFoundException {
		return new FileInputStream("private.key");
	}

	private InputStream createLicenseStream(String string) {
		return new ByteArrayInputStream(string.getBytes(java.nio.charset.StandardCharsets.US_ASCII));
	}

	private ByteArrayOutputStream createOutputStream() {
		return new ByteArrayOutputStream(256);
	}

	// TEMP gecici
	public void reset() {
		matches = false;
		message = UNLICENSED;
	}

}

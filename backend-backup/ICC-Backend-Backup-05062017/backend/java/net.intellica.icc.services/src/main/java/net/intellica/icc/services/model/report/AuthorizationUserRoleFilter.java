package net.intellica.icc.services.model.report;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorizationUserRoleFilter extends ReportFilter {

	private String userId;
	private Boolean includePassive;

	public AuthorizationUserRoleFilter() {
		reportType = ReportType.AuthorizationUserRole;
		includePassive = false;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Boolean getIncludePassive() {
		return includePassive;
	}

	public void setIncludePassive(Boolean includePassive) {
		this.includePassive = includePassive;
	}

}

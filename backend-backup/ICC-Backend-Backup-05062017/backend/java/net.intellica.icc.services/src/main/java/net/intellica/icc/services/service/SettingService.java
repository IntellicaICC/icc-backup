package net.intellica.icc.services.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.intellica.icc.services.model.Setting;
import net.intellica.icc.services.model.SettingGroup;
import net.intellica.icc.services.repo.SettingRepository;
import net.intellica.icc.services.util.aop.PreSaveUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SettingService {

	@Autowired private SettingRepository repository;
	@Autowired private PreSaveUtil preSaveUtil;

	public SettingService() {
	}

	public Setting get(String id) {
		return repository.findOne(id);
	}

	public Setting save(Setting setting) {
		preSaveUtil.fill(setting);
		return repository.save(setting);
	}

	public List<Setting> saveAll(Collection<Setting> settings) {
		List<Setting> result = new ArrayList<>();
		preSaveUtil.fill(settings);
		repository.save(settings).forEach(result::add);
		return result;
	}

	public void delete(String id) {
		repository.delete(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteAll(String userId) {
		repository.deleteAllByUserId(userId);
	}

	public List<Setting> listAll() {
		List<Setting> result = new ArrayList<>();
		repository.findAll().forEach(result::add);
		return result;
	}

	public List<Setting> list(String userId) {
		return repository.findAllByUserIdOrderByDefinitionSettingGroupAscDefinitionPositionAsc(userId);
	}

	public List<Setting> list(String userId, SettingGroup group) {
		return repository.findAllByUserIdAndDefinitionSettingGroupOrderByDefinitionPositionAsc(userId, group);
	}

}

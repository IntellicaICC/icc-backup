package net.intellica.icc.services.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "icc.engine.backup")
public class BackupEngineConfig {

	private String status_url;
	private String result_url;
	private String cancel_url;
	private String run_url;
	private String version_url;
	private String clear_url;

	public String getStatus_url() {
		return status_url;
	}

	public void setStatus_url(String status_url) {
		this.status_url = status_url;
	}

	public String getResult_url() {
		return result_url;
	}

	public void setResult_url(String result_url) {
		this.result_url = result_url;
	}

	public String getCancel_url() {
		return cancel_url;
	}

	public void setCancel_url(String cancel_url) {
		this.cancel_url = cancel_url;
	}

	public String getRun_url() {
		return run_url;
	}

	public void setRun_url(String run_url) {
		this.run_url = run_url;
	}

	public String getVersion_url() {
		return version_url;
	}

	public void setVersion_url(String version_url) {
		this.version_url = version_url;
	}

	public String getClear_url() {
		return clear_url;
	}

	public void setClear_url(String clear_url) {
		this.clear_url = clear_url;
	}

}

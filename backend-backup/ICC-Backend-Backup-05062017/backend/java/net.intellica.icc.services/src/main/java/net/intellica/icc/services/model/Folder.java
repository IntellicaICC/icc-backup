package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import net.intellica.icc.services.model.sd.FolderConverter;
import net.intellica.icc.services.model.sd.IdConverter;
import net.intellica.icc.services.util.other.ServiceConstants;
import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "ICC_FOLDER")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Folder {

	private String id;
	private CommonProperties props;
	private Set<Folder> folders;
	private Set<Job> jobs;
	private Set<Rule> rules;
	private Set<LocalParameter> parameters;
	private Folder parentFolder;
	private Rights rights;
	private Boolean hasChildFolders;

	public Folder() {
		props = new CommonProperties();
		folders = new HashSet<>();
		jobs = new HashSet<>();
		rules = new HashSet<>();
		parameters = new HashSet<>();
		rights = new Rights();
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Embedded
	public CommonProperties getProps() {
		return props;
	}

	public void setProps(CommonProperties props) {
		this.props = props;
	}

	@OneToMany(mappedBy = "parentFolder", fetch = FetchType.LAZY, cascade = { CascadeType.REMOVE, CascadeType.REFRESH }, orphanRemoval = true)
	@JsonIgnore
	public Set<Folder> getFolders() {
		return folders;
	}

	public void setFolders(Set<Folder> folders) {
		this.folders = folders;
	}

	@OneToMany(mappedBy = "folder", fetch = FetchType.LAZY, cascade = { CascadeType.REMOVE, CascadeType.REFRESH }, orphanRemoval = true)
	@JsonIgnore
	public Set<Job> getJobs() {
		return jobs;
	}

	public void setJobs(Set<Job> jobs) {
		this.jobs = jobs;
	}

	@OneToMany(mappedBy = "folder", fetch = FetchType.LAZY, cascade = { CascadeType.REMOVE, CascadeType.REFRESH }, orphanRemoval = true)
	@JsonIgnore
	public Set<Rule> getRules() {
		return rules;
	}

	public void setRules(Set<Rule> rules) {
		this.rules = rules;
	}

	@OneToMany(mappedBy = "folder", fetch = FetchType.LAZY, cascade = { CascadeType.REMOVE, CascadeType.REFRESH }, orphanRemoval = true)
	@JsonIgnore
	public Set<LocalParameter> getParameters() {
		return parameters;
	}

	public void setParameters(Set<LocalParameter> parameters) {
		this.parameters = parameters;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PARENT_FOLDER_ID", nullable = true)
	@JsonSerialize(converter = IdConverter.class)
	@JsonDeserialize(converter = FolderConverter.class)
	public Folder getParentFolder() {
		return parentFolder;
	}

	public void setParentFolder(Folder parentFolder) {
		this.parentFolder = parentFolder;
	}

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "RIGHTS_ID", nullable = false)
	public Rights getRights() {
		return rights;
	}

	public void setRights(Rights rights) {
		this.rights = rights;
	}

	@Transient
	@JsonProperty
	public Boolean getHasChildFolders() {
		return hasChildFolders;
	}

	@JsonIgnore
	public void setHasChildFolders(Boolean hasChildFolders) {
		this.hasChildFolders = hasChildFolders;
	}

}

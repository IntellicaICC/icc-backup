package net.intellica.icc.services.model.operation;

import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import net.intellica.icc.services.model.sd.IdConverter;
import net.intellica.icc.services.model.sd.OperationConverter;
import net.intellica.icc.services.util.other.ServiceConstants;

@Entity
@Table(name = "ICC_OPERATION_RESULT")
@JsonIgnoreType
public class OperationResult {

	private String id;
	private String name;
	private String value;
	private Operation operation;

	public OperationResult() {
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "NAME", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Lob
	@Column(name = "VALUE", nullable = true)
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "OPERATION_ID", nullable = false)
	@JsonSerialize(converter = IdConverter.class)
	@JsonDeserialize(converter = OperationConverter.class)
	public Operation getOperation() {
		return operation;
	}

	public void setOperation(Operation operation) {
		this.operation = operation;
	}

}

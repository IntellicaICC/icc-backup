package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import net.intellica.icc.services.util.other.ServiceConstants;
import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "ICC_CONNECTION", uniqueConstraints = @UniqueConstraint(columnNames = "NAME") )
@JsonIgnoreProperties(ignoreUnknown = true)
public class Connection {

	private String id;
	private CommonProperties props;
	private String databasePassword;
	private String databaseUrl;
	private String databaseUser;

	public Connection() {
		props = new CommonProperties();
		databasePassword = "";
		databaseUrl = "";
		databaseUser = "";
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Embedded
	public CommonProperties getProps() {
		return props;
	}

	public void setProps(CommonProperties props) {
		this.props = props;
	}

	@Column(name = "PASSWORD", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	@JsonIgnore
	public String getDatabasePassword() {
		return databasePassword;
	}

	@JsonProperty
	public void setDatabasePassword(String databasePassword) {
		this.databasePassword = databasePassword;
	}

	@Column(name = "URL", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getDatabaseUrl() {
		return databaseUrl;
	}

	public void setDatabaseUrl(String databaseUrl) {
		this.databaseUrl = databaseUrl;
	}

	@Column(name = "USERNAME", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getDatabaseUser() {
		return databaseUser;
	}

	public void setDatabaseUser(String databaseUser) {
		this.databaseUser = databaseUser;
	}

}

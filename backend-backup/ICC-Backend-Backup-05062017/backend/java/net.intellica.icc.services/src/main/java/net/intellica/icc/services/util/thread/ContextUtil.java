package net.intellica.icc.services.util.thread;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ContextUtil {

	@Autowired private ApplicationContext context;

	public <T> T call(Class<T> clazz) {
		return context.getBean(clazz);
	}

}

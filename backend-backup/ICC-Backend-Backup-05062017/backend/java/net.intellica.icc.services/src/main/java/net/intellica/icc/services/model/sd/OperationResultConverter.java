package net.intellica.icc.services.model.sd;

import com.fasterxml.jackson.databind.util.StdConverter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.intellica.icc.services.model.operation.OperationResult;

public class OperationResultConverter extends StdConverter<List<OperationResult>, Map<String, String>> {

	@Override
	public Map<String, String> convert(List<OperationResult> results) {
		Map<String, String> result = new HashMap<>();
		results.forEach(r -> result.put(r.getName(), r.getValue()));
		return result;
	}

}

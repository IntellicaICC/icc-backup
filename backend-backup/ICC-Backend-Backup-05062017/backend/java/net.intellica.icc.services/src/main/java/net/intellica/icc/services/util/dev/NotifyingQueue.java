package net.intellica.icc.services.util.dev;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;
import net.intellica.icc.services.event.QueueEvent;
import net.intellica.icc.services.event.QueueEvent.EventType;
import net.intellica.icc.services.event.QueueEventListener;

public class NotifyingQueue<T> implements Queue<T> {

	private List<T> list;
	private List<QueueEventListener> listeners;

	public NotifyingQueue() {
		list = new ArrayList<>();
		listeners = new ArrayList<>();
	}

	@Override
	public synchronized int size() {
		return list.size();
	}

	@Override
	public synchronized boolean isEmpty() {
		return list.isEmpty();
	}

	@Override
	public synchronized boolean contains(Object o) {
		return list.contains(o);
	}

	@Override
	public synchronized Iterator<T> iterator() {
		return list.iterator();
	}

	@Override
	public synchronized Object[] toArray() {
		return list.toArray();
	}

	@Override
	public synchronized <E> E[] toArray(E[] a) {
		return list.toArray(a);
	}

	@Override
	public synchronized boolean remove(Object o) {
		return list.remove(o);
	}

	@Override
	public synchronized boolean containsAll(Collection<?> c) {
		return list.containsAll(c);
	}

	@Override
	public synchronized boolean addAll(Collection<? extends T> c) {
		return list.addAll(c);
	}

	@Override
	public synchronized boolean removeAll(Collection<?> c) {
		return list.removeAll(c);
	}

	@Override
	public synchronized boolean retainAll(Collection<?> c) {
		return list.retainAll(c);
	}

	@Override
	public synchronized void clear() {
		list.clear();
	}

	@Override
	public synchronized boolean add(T e) {
		list.add(e);
		listeners.forEach(l -> l.queueEvent(new QueueEvent(EventType.Add, e)));
		return true;
	}

	@Override
	public synchronized boolean offer(T e) {
		return add(e);
	}

	@Override
	public synchronized T remove() {
		if (list.isEmpty())
			throw new NoSuchElementException();
		return list.remove(0);
	}

	@Override
	public synchronized T poll() {
		T result = null;
		if (!list.isEmpty())
			result = list.remove(0);
		return result;
	}

	@Override
	public synchronized T element() {
		if (list.isEmpty())
			throw new NoSuchElementException();
		return list.get(0);
	}

	@Override
	public synchronized T peek() {
		T result = null;
		if (!list.isEmpty())
			result = list.get(0);
		return result;
	}

	public synchronized void addListener(QueueEventListener listener) {
		listeners.add(listener);
	}

	public synchronized void removeListener(QueueEventListener listener) {
		listeners.remove(listener);
	}

}

package net.intellica.icc.services.service;

import java.util.ArrayList;
import java.util.List;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.repo.UserRepository;
import net.intellica.icc.services.util.audit.HistoryUtil;
import net.intellica.icc.template.model.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class UserService {

	@Autowired private UserRepository repository;
	@Autowired private HistoryUtil historyUtil;

	public UserService() {
	}

	public User get(String id, Boolean includeHistory) {
		return get(id, false, includeHistory);
	}

	public User get(String id, Boolean fetchRoles, Boolean includeHistory) {
		User result;
		if (Constants.User.ANONYMOUS_USER_ID.equals(id))
			result = getAnonymousUser();
		else {
			result = repository.findOne(id);
			if (includeHistory && result == null)
				result = historyUtil.getLastRevision(User.class, id);
			if (fetchRoles && result != null)
				result.getRoles().size();
		}
		return result;
	}

	public User save(User user) {
		return repository.save(user);
	}

	public void delete(String id) {
		repository.delete(id);
	}

	public List<User> listAll() {
		List<User> result = new ArrayList<>();
		repository.findAllByOrderByPropsNameAsc().forEach(result::add);
		return result;
	}

	public User firstByName(String name) {
		return repository.findFirstByPropsName(name);
	}

	public User getCurrentUser() {
		User result = null;
		SecurityContext contextHolder = SecurityContextHolder.getContext();
		if (contextHolder != null) {
			Authentication auth = contextHolder.getAuthentication();
			if (auth != null && !StringUtils.isEmpty(auth.getName())) {
				if (net.intellica.icc.template.model.Constants.User.ANONYMOUS_USER_ID.equals(auth.getName()))
					result = getAnonymousUser();
				else {
					result = firstByName(auth.getName());
					result = get(result.getId(), true, false);
				}
			}
		}
		return result;
	}

	private User getAnonymousUser() {
		User result = new User();
		result.setId(net.intellica.icc.template.model.Constants.User.ANONYMOUS_USER_ID);
		result.getProps().setName(net.intellica.icc.template.model.Constants.User.ANONYMOUS_USER_NAME);
		return result;
	}

}

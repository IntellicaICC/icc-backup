package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.Arrays;

public enum ErrorType {

	Validation,
	Technical,
	Database;

	@JsonCreator
	public static ErrorType fromString(String string) {
		return Arrays.asList(ErrorType.values()).stream().filter(s -> s.toString().equals(string)).findFirst().orElse(null);
	}

}

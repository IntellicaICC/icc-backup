package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import net.intellica.icc.services.event.EventType;
import net.intellica.icc.services.util.other.ServiceConstants;

@Entity
@Table(name = "ICC_SETTINGDEF", uniqueConstraints = { @UniqueConstraint(columnNames = { "SETTING_GROUP", "EVENT_TYPE" }) })
@JsonIgnoreProperties(ignoreUnknown = true)
public class SettingDefinition {

	private String id;
	private String name;
	private String description;
	private Integer position;
	private SettingType type;
	private SettingGroup settingGroup;
	private EventType eventType;
	private String defaultValue;

	public SettingDefinition() {
		name = "";
		description = "";
		defaultValue = "";
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "NAME", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "DESCRIPTION", nullable = true, length = ServiceConstants.Model.LONG_STRING_LENGTH)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "POSITION", nullable = false)
	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "TYPE", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public SettingType getType() {
		return type;
	}

	public void setType(SettingType type) {
		this.type = type;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "SETTING_GROUP", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public SettingGroup getSettingGroup() {
		return settingGroup;
	}

	public void setSettingGroup(SettingGroup group) {
		this.settingGroup = group;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "EVENT_TYPE", nullable = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	@Column(name = "DEFAULT_VALUE", nullable = true, length = ServiceConstants.Model.LONG_STRING_LENGTH)
	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

}

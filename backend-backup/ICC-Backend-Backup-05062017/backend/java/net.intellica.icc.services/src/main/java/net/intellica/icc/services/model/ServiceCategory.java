package net.intellica.icc.services.model;

public enum ServiceCategory {

	Connection,
	Folder,
	General,
	GlobalParameter,
	Job,
	License,
	LocalParameter,
	Notification,
	Operation,
	Profile,
	Report,
	Role,
	Rule,
	Setting,
	Template,
	User,
	Utility,
	Other

}

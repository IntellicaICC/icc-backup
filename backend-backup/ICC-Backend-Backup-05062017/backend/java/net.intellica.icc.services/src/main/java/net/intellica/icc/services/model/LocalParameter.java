package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import net.intellica.icc.services.model.sd.FolderConverter;
import net.intellica.icc.services.model.sd.IdConverter;
import net.intellica.icc.services.util.other.ServiceConstants;
import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "ICC_PARAMETER_LOCAL")
@JsonIgnoreProperties(ignoreUnknown = true)
public class LocalParameter {

	private String id;
	private CommonProperties props;
	private String value;
	private Rights rights;
	private Folder folder;

	public LocalParameter() {
		props = new CommonProperties();
		value = "";
		rights = new Rights();
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Embedded
	public CommonProperties getProps() {
		return props;
	}

	public void setProps(CommonProperties props) {
		this.props = props;
	}

	@Lob
	@Column(name = "VALUE", nullable = true)
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value == null ? "" : value;
	}

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "RIGHTS_ID", nullable = false)
	public Rights getRights() {
		return rights;
	}

	public void setRights(Rights rights) {
		this.rights = rights;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FOLDER_ID", nullable = true)
	@JsonSerialize(converter = IdConverter.class)
	@JsonDeserialize(converter = FolderConverter.class)
	public Folder getFolder() {
		return folder;
	}

	public void setFolder(Folder folder) {
		this.folder = folder;
	}

}

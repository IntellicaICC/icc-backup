package net.intellica.icc.services.model.rule;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import net.intellica.icc.services.util.dev.HashUtil;
import net.intellica.icc.services.util.other.ExpressionUtil;
import net.intellica.icc.template.model.GlobalVariable;
import org.springframework.util.ObjectUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DecisionNode extends Node {

	private static final long serialVersionUID = 1L;

	private String expression;
	private Boolean evaluatedValue;
	private List<Node> trueOutgoing;
	private List<Node> falseOutgoing;

	public DecisionNode() {
		super();
		expression = "";
		trueOutgoing = new ArrayList<>();
		falseOutgoing = new ArrayList<>();
		type = NodeType.Decision;
	}

	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression == null ? "" : expression;
	}

	@Override
	public void setGraph(Graph graph) {
		super.setGraph(graph);
		// set true outgoing nodes
		trueOutgoing.clear();
		trueOutgoing.addAll(graph.getTrueLinks().get(getId()).stream().map(linkId -> graph.getNode(linkId)).collect(Collectors.toList()));
		// set false outgoing nodes
		falseOutgoing.clear();
		falseOutgoing.addAll(graph.getFalseLinks().get(getId()).stream().map(linkId -> graph.getNode(linkId)).collect(Collectors.toList()));
	}

	@Override
	public List<Node> getOutgoing() {
		return evaluatedValue == null ? new ArrayList<>() : (evaluatedValue ? getTrueOutgoing() : getFalseOutgoing());
	}

	private List<Node> getNotOutgoing() {
		return evaluatedValue == null ? new ArrayList<>() : (evaluatedValue ? getFalseOutgoing() : getTrueOutgoing());
	}

	@JsonIgnore
	public List<Node> getTrueOutgoing() {
		return trueOutgoing;
	}

	@JsonIgnore
	public List<Node> getFalseOutgoing() {
		return falseOutgoing;
	}

	@Override
	public void run() {
		super.run();
		if (!workflowHasError() && !isCanceled() && !nodeHasError())
			getWorkflow().bypass(this, getNotOutgoing());
	}

	@Override
	protected void doWork() {
		try {
			String contextName = getWorkflow().getGlobalVariables().get(GlobalVariable.RuleInstanceID.toString());
			String folderId = getWorkflow().getGlobalVariables().get(GlobalVariable.FolderID.toString());
			Object context = getWorkflow().getContextUtil().call(ExpressionUtil.class).prepareContext(contextName, folderId, getInput());
			evaluatedValue = (Boolean)getWorkflow().getContextUtil().call(ExpressionUtil.class).evaluate(context, expression, Boolean.class);
		}
		catch (Exception e) {
			setNodeError(e);
		}
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj) &&
			ObjectUtils.nullSafeEquals(getExpression(), ((DecisionNode)obj).getExpression());
	}

	@Override
	public int hashCode() {
		return HashUtil.generate(super.hashCode(), expression);
	}

}

package net.intellica.icc.services.service;

import net.intellica.icc.services.model.ServiceLog;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.repo.ServiceLogRepository;
import net.intellica.icc.services.util.aop.PreSaveUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceLogService {

	@Autowired private ServiceLogRepository repository;
	@Autowired private PreSaveUtil preSaveUtil;

	public ServiceLogService() {
	}

	public ServiceLog get(String id) {
		return repository.findOne(id);
	}

	public ServiceLog save(ServiceLog serviceLog, User user) {
		preSaveUtil.fill(serviceLog);
		return repository.save(serviceLog);
	}

}

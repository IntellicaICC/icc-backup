package net.intellica.icc.services.controller;

import java.util.List;
import net.intellica.icc.services.model.Connection;
import net.intellica.icc.services.service.ConnectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/connection")
public class ConnectionController {

	@Autowired private ConnectionService connectionService;

	@RequestMapping(value = { "/{id}", "/{id}/" }, method = RequestMethod.GET)
	public Object get(@PathVariable String id) {
		return connectionService.get(id, false);
	}

	@RequestMapping(value = { "/save", "/save/" }, method = RequestMethod.POST)
	public Object save(@RequestBody Connection connection) {
		return connectionService.save(connection).getId();
	}

	@RequestMapping(value = { "/{id}", "/{id}/" }, method = RequestMethod.DELETE)
	public Object delete(@PathVariable String id) {
		connectionService.delete(id);
		return null;
	}

	@RequestMapping(value = { "/list", "/list/" }, method = RequestMethod.GET)
	public Object list(@RequestParam(value = "name", required = false) String name) {
		List<Connection> result;
		if (StringUtils.isEmpty(name))
			result = connectionService.listAll();
		else
			result = connectionService.listByName(name);
		return result;
	}

}

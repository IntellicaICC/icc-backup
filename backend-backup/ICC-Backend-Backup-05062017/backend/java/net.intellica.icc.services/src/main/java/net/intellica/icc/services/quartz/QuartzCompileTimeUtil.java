package net.intellica.icc.services.quartz;

import java.util.Map;
import net.intellica.icc.services.model.Job;
import net.intellica.icc.services.model.Rule;
import net.intellica.icc.services.model.User;
import net.intellica.icc.template.model.GlobalVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class QuartzCompileTimeUtil {

	@Autowired private ApplicationContext context;

	public void fillParameters(Map<String, String> parameters, Job job, String triggerId) {
		User currentUser = context.getBean(User.class);
		fillParameters(parameters, job, triggerId, currentUser.getId(), currentUser.getProps().getName());
	}

	public void fillParameters(Map<String, String> parameters, Job job, String triggerId, String userId, String userName) {
		// operasyon basladiginda degismeyecek job parametreleri
		parameters.put(GlobalVariable.UserID.toString(), userId);
		parameters.put(GlobalVariable.Username.toString(), userName);
		parameters.put(GlobalVariable.TemplateID.toString(), job.getTemplate().getId());
		parameters.put(GlobalVariable.TemplateName.toString(), job.getTemplate().getName());
		parameters.put(GlobalVariable.JobID.toString(), job.getId());
		parameters.put(GlobalVariable.JobTriggerID.toString(), triggerId);
	}

	public void fillParameters(Map<String, String> parameters, Rule rule, String triggerId) {
		User currentUser = context.getBean(User.class);
		fillParameters(parameters, rule, triggerId, currentUser.getId(), currentUser.getProps().getName());
	}

	public void fillParameters(Map<String, String> parameters, Rule rule, String triggerId, String userId, String userName) {
		// operasyon basladiginda degismeyecek rule parametreleri
		parameters.put(GlobalVariable.UserID.toString(), userId);
		parameters.put(GlobalVariable.Username.toString(), userName);
		parameters.put(GlobalVariable.RuleID.toString(), rule.getId());
		parameters.put(GlobalVariable.RuleTriggerID.toString(), triggerId);
	}

}

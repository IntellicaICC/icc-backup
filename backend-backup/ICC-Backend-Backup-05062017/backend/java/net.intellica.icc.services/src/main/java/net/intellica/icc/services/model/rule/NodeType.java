package net.intellica.icc.services.model.rule;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.Arrays;

public enum NodeType {

	Start,
	Job,
	Rule,
	Decision,
	Comment,
	End;

	@JsonCreator
	public static NodeType fromString(String string) {
		return Arrays.asList(NodeType.values()).stream().filter(s -> s.toString().equals(string)).findFirst().orElse(null);
	}

}

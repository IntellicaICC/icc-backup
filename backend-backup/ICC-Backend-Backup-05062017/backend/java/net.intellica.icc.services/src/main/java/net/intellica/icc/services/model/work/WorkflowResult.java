package net.intellica.icc.services.model.work;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.intellica.icc.services.model.operation.OperationLiveStatus;
import net.intellica.icc.services.model.operation.OperationRuleLog;
import net.intellica.icc.template.model.QueryLog;

public class WorkflowResult {

	private String ruleDetail;
	private Boolean canceled;
	private Map<String, Object> results;
	private Exception error;
	private List<QueryLog> queryLogs;
	private OperationLiveStatus finalStatus;
	private List<OperationRuleLog> ruleLogs;

	public WorkflowResult() {
		canceled = false;
		results = new HashMap<>();
		queryLogs = new ArrayList<>();
		ruleLogs = new ArrayList<>();
	}

	public String getRuleDetail() {
		return ruleDetail;
	}

	public void setRuleDetail(String ruleDetail) {
		this.ruleDetail = ruleDetail;
	}

	public Boolean isSuccessful() {
		return error == null && !canceled;
	}

	public Boolean isCanceled() {
		return canceled;
	}

	public void setCanceled(Boolean canceled) {
		this.canceled = canceled;
	}

	public Map<String, Object> getResults() {
		return results;
	}

	public void setResults(Map<String, Object> results) {
		this.results = results;
	}

	public Exception getError() {
		return error;
	}

	public void setError(Exception error) {
		this.error = error;
	}

	public List<QueryLog> getQueryLogs() {
		return queryLogs;
	}

	public void setQueryLogs(List<QueryLog> queryLogs) {
		this.queryLogs = queryLogs;
	}

	public OperationLiveStatus getFinalStatus() {
		return finalStatus;
	}

	public void setFinalStatus(OperationLiveStatus finalStatus) {
		this.finalStatus = finalStatus;
	}

	public List<OperationRuleLog> getRuleLogs() {
		return ruleLogs;
	}

	public void setRuleLogs(List<OperationRuleLog> ruleLogs) {
		this.ruleLogs = ruleLogs;
	}

	public WorkflowResultStatus getStatus() {
		WorkflowResultStatus result;
		if (canceled)
			result = WorkflowResultStatus.Canceled;
		else if (isSuccessful())
			result = WorkflowResultStatus.Finished;
		else
			result = WorkflowResultStatus.Error;
		return result;
	}

}

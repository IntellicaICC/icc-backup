package net.intellica.icc.services.repo;

import java.util.List;
import net.intellica.icc.services.model.LoginLog;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginLogRepository extends PagingAndSortingRepository<LoginLog, String> {

	List<LoginLog> findAllByUserIdOrderByAttemptDateDesc(String userId, Pageable pageable);

}

package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.Arrays;

public enum JobPriority {

	Low,
	Normal,
	High;

	@JsonCreator
	public static JobPriority fromString(String string) {
		return Arrays.asList(JobPriority.values()).stream().filter(s -> s.toString().equals(string)).findFirst().orElse(null);
	}

}

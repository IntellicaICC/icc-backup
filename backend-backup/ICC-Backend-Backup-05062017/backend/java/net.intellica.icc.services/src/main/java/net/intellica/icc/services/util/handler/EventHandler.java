package net.intellica.icc.services.util.handler;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import net.intellica.icc.services.controller.NotificationController;
import net.intellica.icc.services.event.EventType;
import net.intellica.icc.services.model.Job;
import net.intellica.icc.services.model.Notification;
import net.intellica.icc.services.model.NotificationSource;
import net.intellica.icc.services.model.NotificationType;
import net.intellica.icc.services.model.Rule;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.model.UserType;
import net.intellica.icc.services.model.operation.Operation;
import net.intellica.icc.services.model.operation.OperationType;
import net.intellica.icc.services.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EventHandler {

	private static final Integer MAX_NOTIFICATION_TEXT = 2000;
	private static final String SUBJECT_CANCEL = "%s '%s' is cancelled!";
	private static final String SUBJECT_ERROR = "%s '%s' finished with error!";
	private static final String SUBJECT_FINISH = "%s '%s' finished successfully.";
	private static final String SUBJECT_START = "%s '%s' started.";
	private static final String SUBJECT_SKIP = "%s '%s' skipped execution.";
	private static final String TEXT_CANCEL = "%s '%s' is cancelled by user %s.";
	private static final String TEXT_ERROR = "%s '%s' finished with error:%s";
	private static final String TEXT_FINISH = "%s '%s' finished successfully.";
	private static final String TEXT_START = "%s '%s' is started by user %s.";
	private static final String TEXT_SKIP = "%s '%s' is skipped by user %s.";
	private static final String TEXT_SKIP_COLLISION = "%s '%s' skipped execution since it is already executing.";
	private static final String TEXT_SKIP_INACTIVE = "%s '%s' skipped execution since it is not active.";
	private static final String TEXT_SKIP_MISFIRE = "%s '%s' skipped execution since it missed its scheduled run time.";
	private static final String TEXT_SKIP_MANUAL = "%s '%s' is skipped by user %s.";
	private static final String TEXT_SKIP_UNAUTHORIZED = "%s '%s' skipped execution since user lost execute right.";

	@Autowired NotificationController notificationController;
	@Autowired UserService userService;

	public void skipDueToCollision(Operation operation, User targetUser) {
		NotificationSource source = NotificationSource.Other;
		if (operation.getType() == OperationType.Job)
			source = NotificationSource.Job;
		else if (operation.getType() == OperationType.Rule)
			source = NotificationSource.Rule;
		else
			source = NotificationSource.Other;
		List<Notification> notifications = new ArrayList<>();
		if (targetUser != null && targetUser.getProps().isActive())
			notifications.add(new Notification(
				NotificationType.Operational,
				source,
				operation.getId(),
				targetUser.getId(),
				String.format(SUBJECT_SKIP, source, operation.getModelName()),
				String.format(TEXT_SKIP_COLLISION, source, operation.getModelName()),
				EventType.Skip//
			));
		// operator kullanicilara uyari
		List<User> operators = userService.listAll().stream()
			.filter(u -> UserType.Operator.equals(u.getUserType()))
			.filter(u -> u.getProps().isActive())
			.collect(Collectors.toList());
		if (targetUser != null)
			operators.removeIf(o -> o.getId().equals(targetUser.getId()));
		for (User operator : operators)
			notifications.add(new Notification(
				NotificationType.Operational,
				source,
				operation.getId(),
				operator.getId(),
				String.format(SUBJECT_SKIP, source, operation.getModelName()),
				String.format(TEXT_SKIP_COLLISION, source, operation.getModelName()),
				EventType.Skip//
			));
		notificationController.saveAll(notifications);
	}

	public void skipPassive(Operation operation, User targetUser) {
		NotificationSource source = NotificationSource.Other;
		if (operation.getType() == OperationType.Job)
			source = NotificationSource.Job;
		else if (operation.getType() == OperationType.Rule)
			source = NotificationSource.Rule;
		else
			source = NotificationSource.Other;
		List<Notification> notifications = new ArrayList<>();
		if (targetUser != null && targetUser.getProps().isActive())
			notifications.add(new Notification(
				NotificationType.Operational,
				source,
				operation.getId(),
				targetUser.getId(),
				String.format(SUBJECT_SKIP, source, operation.getModelName()),
				String.format(TEXT_SKIP_INACTIVE, source, operation.getModelName()),
				EventType.Skip//
			));
		// operator kullanicilara uyari
		List<User> operators = userService.listAll().stream()
			.filter(u -> UserType.Operator.equals(u.getUserType()))
			.filter(u -> u.getProps().isActive())
			.collect(Collectors.toList());
		if (targetUser != null)
			operators.removeIf(o -> o.getId().equals(targetUser.getId()));
		for (User operator : operators)
			notifications.add(new Notification(
				NotificationType.Operational,
				source,
				operation.getId(),
				operator.getId(),
				String.format(SUBJECT_SKIP, source, operation.getModelName()),
				String.format(TEXT_SKIP_INACTIVE, source, operation.getModelName()),
				EventType.Skip//
			));
		notificationController.saveAll(notifications);
	}

	public void skipMissed(Operation operation, User targetUser) {
		NotificationSource source = NotificationSource.Other;
		if (operation.getType() == OperationType.Job)
			source = NotificationSource.Job;
		else if (operation.getType() == OperationType.Rule)
			source = NotificationSource.Rule;
		else
			source = NotificationSource.Other;
		List<Notification> notifications = new ArrayList<>();
		if (targetUser != null && targetUser.getProps().isActive())
			notifications.add(new Notification(
				NotificationType.Operational,
				source,
				operation.getId(),
				targetUser.getId(),
				String.format(SUBJECT_SKIP, source, operation.getModelName()),
				String.format(TEXT_SKIP_MISFIRE, source, operation.getModelName()),
				EventType.Skip//
			));
		// operator kullanicilara uyari
		List<User> operators = userService.listAll().stream()
			.filter(u -> UserType.Operator.equals(u.getUserType()))
			.filter(u -> u.getProps().isActive())
			.collect(Collectors.toList());
		if (targetUser != null)
			operators.removeIf(o -> o.getId().equals(targetUser.getId()));
		for (User operator : operators)
			notifications.add(new Notification(
				NotificationType.Operational,
				source,
				operation.getId(),
				operator.getId(),
				String.format(SUBJECT_SKIP, source, operation.getModelName()),
				String.format(TEXT_SKIP_MISFIRE, source, operation.getModelName()),
				EventType.Skip//
			));
		notificationController.saveAll(notifications);
	}

	public void skipManual(Operation operation, User targetUser, User sourceUser) {
		NotificationSource source = NotificationSource.Other;
		if (operation.getType() == OperationType.Job)
			source = NotificationSource.Job;
		else if (operation.getType() == OperationType.Rule)
			source = NotificationSource.Rule;
		else
			source = NotificationSource.Other;
		List<Notification> notifications = new ArrayList<>();
		if (targetUser != null && targetUser.getProps().isActive())
			notifications.add(new Notification(
				NotificationType.Operational,
				source,
				operation.getId(),
				targetUser.getId(),
				String.format(SUBJECT_SKIP, source, operation.getModelName()),
				String.format(TEXT_SKIP_MANUAL, source, operation.getModelName(), sourceUser.getFullName()),
				EventType.Skip//
			));
		// operator kullanicilara uyari
		List<User> operators = userService.listAll().stream()
			.filter(u -> UserType.Operator.equals(u.getUserType()))
			.filter(u -> u.getProps().isActive())
			.collect(Collectors.toList());
		if (targetUser != null)
			operators.removeIf(o -> o.getId().equals(targetUser.getId()));
		for (User operator : operators)
			notifications.add(new Notification(
				NotificationType.Operational,
				source,
				operation.getId(),
				operator.getId(),
				String.format(SUBJECT_SKIP, source, operation.getModelName()),
				String.format(TEXT_SKIP_MANUAL, source, operation.getModelName(), sourceUser.getFullName()),
				EventType.Skip//
			));
		notificationController.saveAll(notifications);
	}

	public void skipUnauthorized(Operation operation, User targetUser) {
		NotificationSource source = NotificationSource.Other;
		if (operation.getType() == OperationType.Job)
			source = NotificationSource.Job;
		else if (operation.getType() == OperationType.Rule)
			source = NotificationSource.Rule;
		else
			source = NotificationSource.Other;
		List<Notification> notifications = new ArrayList<>();
		if (targetUser != null && targetUser.getProps().isActive())
			notifications.add(new Notification(
				NotificationType.Operational,
				source,
				operation.getId(),
				targetUser.getId(),
				String.format(SUBJECT_SKIP, source, operation.getModelName()),
				String.format(TEXT_SKIP_UNAUTHORIZED, source, operation.getModelName()),
				EventType.Skip//
			));
		// operator kullanicilara uyari
		List<User> operators = userService.listAll().stream()
			.filter(u -> UserType.Operator.equals(u.getUserType()))
			.filter(u -> u.getProps().isActive())
			.collect(Collectors.toList());
		if (targetUser != null)
			operators.removeIf(o -> o.getId().equals(targetUser.getId()));
		for (User operator : operators)
			notifications.add(new Notification(
				NotificationType.Operational,
				source,
				operation.getId(),
				operator.getId(),
				String.format(SUBJECT_SKIP, source, operation.getModelName()),
				String.format(TEXT_SKIP_UNAUTHORIZED, source, operation.getModelName()),
				EventType.Skip//
			));
		notificationController.saveAll(notifications);
	}

	public void generateEvent(EventType type, Job job, String operationId, User sourceUser, User targetUser) {
		List<Notification> notifications = new ArrayList<>();
		if (targetUser != null && targetUser.getProps().isActive())
			notifications.add(createNotification(type, job, operationId, sourceUser, targetUser));
		// operator kullanicilara uyari
		List<User> operators = userService.listAll().stream()
			.filter(u -> UserType.Operator.equals(u.getUserType()))
			.filter(u -> u.getProps().isActive())
			.collect(Collectors.toList());
		if (targetUser != null)
			operators.removeIf(o -> o.getId().equals(targetUser.getId()));
		for (User operator : operators)
			notifications.add(createNotification(type, job, operationId, sourceUser, operator));
		notificationController.saveAll(notifications);
	}

	public void generateEvent(Exception exception, Job job, String operationId, User sourceUser, User targetUser) {
		List<Notification> notifications = new ArrayList<>();
		if (targetUser != null && targetUser.getProps().isActive())
			notifications.add(createNotification(EventType.Error, job, operationId, targetUser, exception));
		// operator kullanicilara uyari
		List<User> operators = userService.listAll().stream()
			.filter(u -> UserType.Operator.equals(u.getUserType()))
			.filter(u -> u.getProps().isActive())
			.collect(Collectors.toList());
		if (targetUser != null)
			operators.removeIf(o -> o.getId().equals(targetUser.getId()));
		for (User operator : operators)
			notifications.add(createNotification(EventType.Error, job, operationId, operator, exception));
		notificationController.saveAll(notifications);
	}

	public void generateEvent(EventType type, Rule rule, String operationId, User sourceUser, User targetUser) {
		List<Notification> notifications = new ArrayList<>();
		if (targetUser != null && targetUser.getProps().isActive())
			notifications.add(createNotification(type, rule, operationId, sourceUser, targetUser));
		// operator kullanicilara uyari
		List<User> operators = userService.listAll().stream()
			.filter(u -> UserType.Operator.equals(u.getUserType()))
			.filter(u -> u.getProps().isActive())
			.collect(Collectors.toList());
		if (targetUser != null)
			operators.removeIf(o -> o.getId().equals(targetUser.getId()));
		for (User operator : operators)
			notifications.add(createNotification(type, rule, operationId, sourceUser, operator));
		notificationController.saveAll(notifications);
	}

	public void generateEvent(Exception exception, Rule rule, String operationId, User sourceUser, User targetUser) {
		List<Notification> notifications = new ArrayList<>();
		if (targetUser != null && targetUser.getProps().isActive())
			notifications.add(createNotification(EventType.Error, rule, operationId, targetUser, exception));
		// operator kullanicilara uyari
		List<User> operators = userService.listAll().stream()
			.filter(u -> UserType.Operator.equals(u.getUserType()))
			.filter(u -> u.getProps().isActive())
			.collect(Collectors.toList());
		if (targetUser != null)
			operators.removeIf(o -> o.getId().equals(targetUser.getId()));
		for (User operator : operators)
			notifications.add(createNotification(EventType.Error, rule, operationId, operator, exception));
		notificationController.saveAll(notifications);
	}

	private Notification createNotification(EventType eventType, Job job, String operationId, User sourceUser, User targetUser) {
		Notification result = createNotification(targetUser, operationId, NotificationSource.Job, NotificationType.Operational, eventType);
		fillText(result, eventType, job, sourceUser);
		return result;
	}

	private Notification createNotification(EventType error, Job job, String operationId, User targetUser, Exception exception) {
		Notification result = createNotification(targetUser, operationId, NotificationSource.Job, NotificationType.Operational, EventType.Error);
		fillText(result, job, exception);
		return result;
	}

	private Notification createNotification(EventType eventType, Rule rule, String operationId, User sourceUser, User targetUser) {
		Notification result = createNotification(targetUser, operationId, NotificationSource.Rule, NotificationType.Operational, eventType);
		fillText(result, eventType, rule, sourceUser);
		return result;
	}

	private Notification createNotification(EventType error, Rule rule, String operationId, User targetUser, Exception exception) {
		Notification result = createNotification(targetUser, operationId, NotificationSource.Rule, NotificationType.Operational, EventType.Error);
		fillText(result, rule, exception);
		return result;
	}

	private Notification createNotification(User targetUser, String operationId, NotificationSource source, NotificationType type, EventType eventType) {
		Notification result = new Notification();
		result.setSource(source);
		result.setSourceId(operationId);
		result.setTargetUser(targetUser.getId());
		result.setType(type);
		result.setEventType(eventType);
		return result;
	}

	private void fillText(Notification notification, EventType eventType, Job job, User sourceUser) {
		String userFullName = sourceUser == null ? "" : sourceUser.getFullName();
		fillText(notification, eventType, NotificationSource.Job, job.getProps().getName(), userFullName, null);
	}

	private void fillText(Notification notification, Job job, Exception exception) {
		fillText(notification, EventType.Error, NotificationSource.Job, job.getProps().getName(), null, exception);
	}

	private void fillText(Notification notification, EventType eventType, Rule rule, User sourceUser) {
		String userFullName = sourceUser == null ? "" : sourceUser.getFullName();
		fillText(notification, eventType, NotificationSource.Rule, rule.getProps().getName(), userFullName, null);
	}

	private void fillText(Notification notification, Rule rule, Exception exception) {
		fillText(notification, EventType.Error, NotificationSource.Rule, rule.getProps().getName(), null, exception);
	}

	private void fillText(Notification notification, EventType eventType, NotificationSource source, String name, String userFullName, Exception exception) {
		switch (eventType) {
			case Cancel:
				notification.setSubject(String.format(SUBJECT_CANCEL, source, name));
				notification.setText(String.format(TEXT_CANCEL, source, name, userFullName));
				break;
			case Error:
				notification.setSubject(String.format(SUBJECT_ERROR, source, name));
				// TODO exception yerine error code
				String message = exception.getMessage();
				if (message == null)
					message = "";
				message = message.substring(0, Math.min(message.length(), MAX_NOTIFICATION_TEXT));
				notification.setText(String.format(TEXT_ERROR, source, name, message));
				break;
			case Finish:
				notification.setSubject(String.format(SUBJECT_FINISH, source, name));
				notification.setText(String.format(TEXT_FINISH, source, name));
				break;
			case Start:
				notification.setSubject(String.format(SUBJECT_START, source, name));
				notification.setText(String.format(TEXT_START, source, name, userFullName));
				break;
			case Skip:
				notification.setSubject(String.format(SUBJECT_SKIP, source, name));
				notification.setText(String.format(TEXT_SKIP, source, name, userFullName));
				break;
			default:
				break;
		}
	}

}

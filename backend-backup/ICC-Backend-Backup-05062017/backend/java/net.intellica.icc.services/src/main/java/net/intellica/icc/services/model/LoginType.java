package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.Arrays;

public enum LoginType {

	Default,
	LDAP;

	@JsonCreator
	public static LoginType fromString(String string) {
		return Arrays.asList(LoginType.values()).stream().filter(s -> s.toString().equals(string)).findFirst().orElse(null);
	}

}

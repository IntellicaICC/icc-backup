package net.intellica.icc.services.repo;

import net.intellica.icc.services.model.ServiceLog;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceLogRepository extends PagingAndSortingRepository<ServiceLog, String> {

}

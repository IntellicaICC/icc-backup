package net.intellica.icc.services.repo;

import java.util.List;
import net.intellica.icc.services.model.Notification;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationRepository extends PagingAndSortingRepository<Notification, String> {

	List<Notification> findAllByOrderByNotificationDateDesc(Pageable pageable);

	List<Notification> findAllByTargetUserOrderByNotificationDateDesc(String id, Pageable pageable);

	Integer countAllByTargetUserAndRead(String id, Boolean read);

	void deleteAllByTargetUser(String id);

}

package net.intellica.icc.services.quartz;

import java.util.List;
import java.util.Map;
import javax.persistence.AttributeConverter;
import net.intellica.icc.services.model.Connection;
import net.intellica.icc.services.model.Job;
import net.intellica.icc.services.model.Rule;
import net.intellica.icc.services.model.Variable;
import net.intellica.icc.services.model.operation.OperationType;
import net.intellica.icc.services.service.ConnectionService;
import net.intellica.icc.services.util.config.EngineUtil;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import net.intellica.icc.services.util.other.ExpressionUtil;
import net.intellica.icc.services.util.other.IdGenerator;
import net.intellica.icc.template.exception.CommunicationException;
import net.intellica.icc.template.exception.ExpressionException;
import net.intellica.icc.template.exception.InactiveException;
import net.intellica.icc.template.exception.TemplateException;
import net.intellica.icc.template.model.DataLink;
import net.intellica.icc.template.model.GlobalVariable;
import net.intellica.icc.template.model.WorkItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

@Component
public class QuartzRuntimeUtil {

	@Autowired private IdGenerator idGenerator;
	@Autowired private ExpressionUtil expressionUtil;
	@Autowired private AttributeConverter<String, String> attributeConverter;
	@Autowired private ConnectionService connectionService;
	@Autowired private EngineUtil engineUtil;
	@Autowired private SimpleExceptionHandler simpleExceptionHandler;

	public WorkItem fillParameters(Map<String, String> parameters, Map<String, Object> input, Job job, OperationType type, String ruleInstanceId, List<String> instanceChain, String lastDataset) {
		WorkItem result = new WorkItem();
		result.getParameters().putAll(parameters);
		ruleInstanceId = ruleInstanceId == null ? "" : ruleInstanceId;
		result.getParameters().put(GlobalVariable.RuleInstanceID.toString(), ruleInstanceId);
		String jobInstanceId;
		String contextName;
		if (OperationType.Job == type) {
			// yeni job operasyonu
			jobInstanceId = idGenerator.generateOperationalInstanceId(parameters.get(GlobalVariable.JobTriggerID.toString()), type);
			contextName = jobInstanceId;
		}
		else {
			// rule icinden cagirilan job (tek basina operasyon degil)
			jobInstanceId = idGenerator.generateRandomInstanceId();
			contextName = ruleInstanceId;
		}
		result.getParameters().put(GlobalVariable.JobInstanceID.toString(), jobInstanceId);
		result.getParameters().put(GlobalVariable.JobName.toString(), job.getProps().getName());
		if (job.getFolder() == null) {
			parameters.put(GlobalVariable.FolderID.toString(), "");
			parameters.put(GlobalVariable.FolderName.toString(), "");
		}
		else {
			parameters.put(GlobalVariable.FolderID.toString(), job.getFolder().getId());
			parameters.put(GlobalVariable.FolderName.toString(), job.getFolder().getProps().getName());
		}
		String folderId = job.getFolder() == null ? null : job.getFolder().getId();
		Object context = expressionUtil.prepareContext(contextName, folderId, input);
		AttributeConverter<String, String> ac = (AttributeConverter<String, String>)attributeConverter;
		for (Variable v : job.getVariables()) {
			Object evaluatedValue;
			switch (v.getDefinition().getType()) {
				case Connection:
					Connection connection = connectionService.get(v.getValue(), false);
					if (connection != null) {
						if (!connection.getProps().isActive())
							throw new InactiveException(String.format("Inactive connection (%s)!", connection.getProps().getName()));
						DataLink link = new DataLink();
						link.setName(connection.getProps().getName());
						link.setUrl(connection.getDatabaseUrl());
						link.setUsername(connection.getDatabaseUser());
						link.setPassword(ac.convertToEntityAttribute(connection.getDatabasePassword()));
						result.getLinks().put(v.getValue(), link);
					}
					else if (connectionService.get(v.getValue(), true) != null)
						throw new TemplateException(String.format("Deleted connection (id=%s)!", v.getValue()));
					else
						throw new TemplateException(String.format("Invalid connection (id=%s)!", v.getValue()));
					evaluatedValue = v.getValue();
					break;
				case SQL:
				case Standard:
					evaluatedValue = tryEvaluate(context, v.getValue());
					break;
				case Dataset:
				case List:
				case Boolean:
				default:
					evaluatedValue = v.getValue();
					break;
			}
			evaluatedValue = evaluatedValue == null ? "" : evaluatedValue;
			result.getArguments().put(v.getDefinition().getProps().getName(), String.valueOf(evaluatedValue));
		}
		if (instanceChain != null)
			result.getInstanceChain().addAll(instanceChain);
		result.setLastDataset(lastDataset);
		return result;
	}

	public void fillParameters(Map<String, String> parameters, Rule rule) {
		String instanceId = idGenerator.generateOperationalInstanceId(parameters.get(GlobalVariable.RuleTriggerID.toString()), OperationType.Rule);
		parameters.put(GlobalVariable.RuleInstanceID.toString(), instanceId);
		if (rule.getFolder() == null) {
			parameters.put(GlobalVariable.FolderID.toString(), "");
			parameters.put(GlobalVariable.FolderName.toString(), "");
		}
		else {
			parameters.put(GlobalVariable.FolderID.toString(), rule.getFolder().getId());
			parameters.put(GlobalVariable.FolderName.toString(), rule.getFolder().getProps().getName());
		}
		parameters.put(GlobalVariable.RuleName.toString(), rule.getProps().getName());
	}

	public void clearCache(String ruleInstanceId) {
		try {
			RestTemplate rest = new RestTemplate();
			try {
				String clearUrl = engineUtil.getClearUrl();
				ResponseEntity<String> clearResponse = rest.postForEntity(clearUrl, ruleInstanceId, String.class);
				if (clearResponse.getStatusCode() != HttpStatus.OK)
					throw new CommunicationException("Could not clear cache! rid=" + ruleInstanceId);
			}
			catch (Exception e) {
				simpleExceptionHandler.warning(e.getMessage());
			}
		}
		catch (CommunicationException e) {
			throw e;
		}
		catch (Exception e) {
			throw new CommunicationException(e);
		}
	}

	private Object tryEvaluate(Object context, String value) {
		Object result;
		try {
			if (StringUtils.isEmpty(value))
				result = "";
			else
				result = expressionUtil.evaluate(context, value, null);
		}
		catch (ExpressionException e) {
			result = value;
		}
		return result;
	}

}

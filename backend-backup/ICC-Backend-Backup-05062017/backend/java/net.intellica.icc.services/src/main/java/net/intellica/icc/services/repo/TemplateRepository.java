package net.intellica.icc.services.repo;

import java.util.List;
import net.intellica.icc.services.model.Template;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TemplateRepository extends PagingAndSortingRepository<Template, String> {

	List<Template> findAllByName(String name);

}

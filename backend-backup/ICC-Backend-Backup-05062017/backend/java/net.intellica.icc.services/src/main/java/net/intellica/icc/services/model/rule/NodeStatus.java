package net.intellica.icc.services.model.rule;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.Arrays;

public enum NodeStatus {

	/**
	 * Node is not yet triggered.
	 */
	Idle,

	/**
	 * Node is executing.
	 */
	Executing,

	/**
	 * Node has executed successfully.
	 */
	Finished,

	/**
	 * Workflow is paused, node is waiting for the resume command.
	 */
	Waiting,

	/**
	 * Node has encountered an error.
	 */
	Error;

	@JsonCreator
	public static NodeStatus fromString(String string) {
		return Arrays.asList(NodeStatus.values()).stream().filter(s -> s.toString().equals(string)).findFirst().orElse(null);
	}

}

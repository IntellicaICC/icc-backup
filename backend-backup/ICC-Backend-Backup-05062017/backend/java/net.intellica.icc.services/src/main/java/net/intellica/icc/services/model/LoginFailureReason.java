package net.intellica.icc.services.model;

public enum LoginFailureReason {

	UserNotFound,
	PasswordMismatch,
	UserPassive,
	AccountLocked,
	Other

}

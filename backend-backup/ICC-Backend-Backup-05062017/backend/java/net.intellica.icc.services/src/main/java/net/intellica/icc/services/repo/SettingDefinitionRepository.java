package net.intellica.icc.services.repo;

import net.intellica.icc.services.model.SettingDefinition;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SettingDefinitionRepository extends CrudRepository<SettingDefinition, String> {

}

package net.intellica.icc.services.quartz;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.intellica.icc.services.config.ServicesConfig;
import net.intellica.icc.services.model.Job;
import net.intellica.icc.services.model.JobPriority;
import net.intellica.icc.services.model.Rule;
import net.intellica.icc.services.model.Schedule;
import net.intellica.icc.services.model.operation.InternalOperation;
import net.intellica.icc.services.model.operation.OperationLiveStatus;
import net.intellica.icc.services.model.operation.OperationType;
import net.intellica.icc.services.model.rule.Workflow;
import net.intellica.icc.services.util.other.IdGenerator;
import org.quartz.CalendarIntervalScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.ScheduleBuilder;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.TriggerUtils;
import org.quartz.impl.calendar.BaseCalendar;
import org.quartz.impl.calendar.WeeklyCalendar;
import org.quartz.impl.matchers.GroupMatcher;
import org.quartz.spi.OperableTrigger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class QuartzManager {

	private Boolean useWeeklyCalendar;
	@Autowired private IdGenerator idGenerator;
	@Autowired private QuartzCompileTimeUtil quartzCompileTimeUtil;
	private final Scheduler scheduler;

	@Autowired
	public QuartzManager(
		ServicesConfig servicesConfig,
		Scheduler scheduler,
		QuartzJobFactory jobFactory,
		QuartzEventListener generalListener,
		QuartzJobListener jobListener,
		QuartzRuleListener ruleListener,
		QuartzJobTriggerListener jobTriggerListener,
		QuartzRuleTriggerListener ruleTriggerListener//
	) throws Exception {
		useWeeklyCalendar = servicesConfig.getQuartz_use_weekly_calendar();
		this.scheduler = scheduler;
		if (scheduler.getCalendar(QuartzParameter.CALENDAR_DEFAULT) == null)
			scheduler.addCalendar(QuartzParameter.CALENDAR_DEFAULT, new BaseCalendar(), false, false);
		if (scheduler.getCalendar(QuartzParameter.CALENDAR_WEEKDAYS) == null)
			scheduler.addCalendar(QuartzParameter.CALENDAR_WEEKDAYS, new WeeklyCalendar(), false, false);
		scheduler.setJobFactory(jobFactory);
		scheduler.getListenerManager().addSchedulerListener(generalListener);
		scheduler.getListenerManager().addJobListener(jobListener, GroupMatcher.jobGroupEquals(OperationType.Job.toString()));
		scheduler.getListenerManager().addTriggerListener(jobTriggerListener, GroupMatcher.triggerGroupEquals(OperationType.Job.toString()));
		scheduler.getListenerManager().addJobListener(ruleListener, GroupMatcher.jobGroupEquals(OperationType.Rule.toString()));
		scheduler.getListenerManager().addTriggerListener(ruleTriggerListener, GroupMatcher.triggerGroupEquals(OperationType.Rule.toString()));
		scheduler.startDelayed(1);
	}

	public String schedule(Job job, Schedule schedule) throws SchedulerException {
		String result;
		JobDataMap map = new JobDataMap();
		String triggerId = idGenerator.generateTriggerId(job.getId());
		// fill parameters
		Map<String, String> parameters = new HashMap<>();
		quartzCompileTimeUtil.fillParameters(parameters, job, triggerId);
		map.put(QuartzParameter.PARAMETERS, parameters);
		JobKey jobKey = new JobKey(triggerId, OperationType.Job.toString());
		TriggerKey triggerKey = new TriggerKey(triggerId, OperationType.Job.toString());
		// create job
		JobDetail jobDetail = JobBuilder
			.newJob(QuartzJob.class)
			.withIdentity(jobKey)
			.build();
		// create trigger
		TriggerBuilder<Trigger> triggerBuilder = TriggerBuilder
			.newTrigger()
			.startAt(schedule.getFirstRunDate());
		setTriggerSchedule(schedule, triggerBuilder);
		if (useWeeklyCalendar)
			triggerBuilder = triggerBuilder.modifiedByCalendar(QuartzParameter.CALENDAR_WEEKDAYS);
		else
			triggerBuilder = triggerBuilder.modifiedByCalendar(QuartzParameter.CALENDAR_DEFAULT);
		Trigger trigger = triggerBuilder
			.withIdentity(triggerKey)
			.withPriority(job.getPriority().ordinal())
			.usingJobData(map)
			.build();
		result = triggerId;
		scheduler.scheduleJob(jobDetail, trigger);
		return result;
	}

	public String schedule(Rule rule, Schedule schedule) throws SchedulerException {
		String result;
		JobDataMap map = new JobDataMap();
		String triggerId = idGenerator.generateTriggerId(rule.getId());
		// fill parameters
		Map<String, String> parameters = new HashMap<>();
		quartzCompileTimeUtil.fillParameters(parameters, rule, triggerId);
		map.put(QuartzParameter.PARAMETERS, parameters);
		JobKey jobKey = new JobKey(triggerId, OperationType.Rule.toString());
		TriggerKey triggerKey = new TriggerKey(triggerId, OperationType.Rule.toString());
		// create job
		JobDetail jobDetail = JobBuilder
			.newJob(QuartzRule.class)
			.withIdentity(jobKey)
			.build();
		// create trigger
		TriggerBuilder<Trigger> triggerBuilder = TriggerBuilder
			.newTrigger()
			.startAt(schedule.getFirstRunDate());
		setTriggerSchedule(schedule, triggerBuilder);
		if (useWeeklyCalendar)
			triggerBuilder = triggerBuilder.modifiedByCalendar(QuartzParameter.CALENDAR_WEEKDAYS);
		else
			triggerBuilder = triggerBuilder.modifiedByCalendar(QuartzParameter.CALENDAR_DEFAULT);
		Trigger trigger = triggerBuilder
			.withIdentity(triggerKey)
			.usingJobData(map)
			.build();
		result = triggerId;
		scheduler.scheduleJob(jobDetail, trigger);
		return result;
	}

	public Boolean triggerExists(String triggerId, OperationType type) throws Exception {
		TriggerKey key = new TriggerKey(triggerId, type.toString());
		return scheduler.checkExists(key);
	}

	public void unschedule(String triggerId, OperationType type) throws Exception {
		if (triggerExists(triggerId, type))
			scheduler.unscheduleJob(new TriggerKey(triggerId, type.toString()));
	}

	public Boolean cancel(String instanceId, OperationType type) throws Exception {
		Boolean result = false;
		for (JobExecutionContext j : scheduler.getCurrentlyExecutingJobs()) {
			if (type == OperationType.Job &&
				j.getJobInstance() instanceof QuartzJob//
			) {
				QuartzJob quartzJob = (QuartzJob)j.getJobInstance();
				if (quartzJob.getInstanceId().equals(instanceId)) {
					quartzJob.cancel();
					result = true;
				}
			}
			else if (type == OperationType.Rule &&
				j.getJobInstance() instanceof QuartzRule//
			) {
				QuartzRule quartzRule = (QuartzRule)j.getJobInstance();
				if (quartzRule.getInstanceId().equals(instanceId)) {
					quartzRule.cancel();
					result = true;
				}
			}
		}
		// TODO scheduler uzerinden hatali sonlanan instance bulunabilir mi?
		return result;
	}

	public OperationLiveStatus liveStatus(String instanceId) throws Exception {
		OperationLiveStatus result = null;
		for (JobExecutionContext j : scheduler.getCurrentlyExecutingJobs()) {
			if (j.getJobInstance() instanceof QuartzRule) {
				QuartzRule quartzRule = (QuartzRule)j.getJobInstance();
				if (quartzRule.getInstanceId().equals(instanceId)) {
					Workflow workflow = quartzRule.getWorkflow();
					result = workflow.getLiveStatus();
					break;
				}
			}
		}
		return result;
	}

	public List<Date> generateRunDates(String triggerId, OperationType type, Date startDate, Integer extensionMonths) throws Exception {
		List<Date> result = new ArrayList<>();
		TriggerKey key = new TriggerKey(triggerId, type.toString());
		Trigger trigger = scheduler.getTrigger(key);
		if (trigger != null) {
			Date endDate = Date.from(ZonedDateTime.now().plusMonths(extensionMonths).toInstant());
			result = TriggerUtils.computeFireTimesBetween((OperableTrigger)trigger, scheduler.getCalendar(trigger.getCalendarName()), startDate, endDate);
		}
		return result;
	}

	public void runInternal(InternalOperation internalOperation) throws Exception {
		JobDetail job = scheduler.getJobDetail(new JobKey(internalOperation.toString(), QuartzParameter.INTERNAL_GROUP));
		Trigger trigger = TriggerBuilder
			.newTrigger()
			.startNow()
			.withPriority(JobPriority.Low.ordinal())
			.forJob(job)
			.build();
		scheduler.scheduleJob(trigger);
	}

	public Date getNextRunDate(String triggerId, OperationType type) throws Exception {
		Date result = null;
		TriggerKey key = new TriggerKey(triggerId, type.toString());
		Trigger trigger = scheduler.getTrigger(key);
		if (trigger != null)
			result = trigger.getNextFireTime();
		return result;
	}

	private void setTriggerSchedule(Schedule schedule, TriggerBuilder<Trigger> triggerBuilder) {
		ScheduleBuilder<? extends Trigger> sb = null;
		switch (schedule.getInterval()) {
			case MINUTES:
				sb = SimpleScheduleBuilder.repeatMinutelyForever(schedule.getPeriod());
				triggerBuilder.withSchedule(sb);
				break;
			case HOURS:
				sb = SimpleScheduleBuilder.repeatHourlyForever(schedule.getPeriod());
				triggerBuilder.withSchedule(sb);
				break;
			case DAYS:
				sb = CalendarIntervalScheduleBuilder.calendarIntervalSchedule().withIntervalInDays(schedule.getPeriod());
				triggerBuilder.withSchedule(sb);
				break;
			case WEEKS:
				sb = CalendarIntervalScheduleBuilder.calendarIntervalSchedule().withIntervalInWeeks(schedule.getPeriod());
				triggerBuilder.withSchedule(sb);
				break;
			case MONTHS:
				sb = CalendarIntervalScheduleBuilder.calendarIntervalSchedule().withIntervalInMonths(schedule.getPeriod());
				triggerBuilder.withSchedule(sb);
				break;
			case NONE:
			default:
				break;
		}
	}

}

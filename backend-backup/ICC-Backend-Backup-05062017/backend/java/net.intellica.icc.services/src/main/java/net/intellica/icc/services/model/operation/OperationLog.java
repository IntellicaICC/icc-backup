package net.intellica.icc.services.model.operation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.intellica.icc.services.util.other.ServiceConstants;

@Entity
@Table(name = "ICC_OPERATION_LOG")
@JsonIgnoreProperties(ignoreUnknown = true)
public class OperationLog {

	private String id;
	private String operationId;
	private Date operationDate;
	private OperationStatus status;

	public OperationLog() {
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "OPERATION_ID", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getOperationId() {
		return operationId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	@Column(name = "OPERATION_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOperationDate() {
		return operationDate;
	}

	public void setOperationDate(Date operationDate) {
		this.operationDate = operationDate;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "STATUS", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public OperationStatus getStatus() {
		return status;
	}

	public void setStatus(OperationStatus status) {
		this.status = status;
	}

}

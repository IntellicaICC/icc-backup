package net.intellica.icc.services.controller;

import net.intellica.icc.services.model.SampleQuery;
import net.intellica.icc.services.util.db.SqlExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/utility")
public class UtilityController {

	@Autowired private SqlExecutor executor;

	@RequestMapping(value = { "/query", "/query/" }, method = RequestMethod.POST)
	public Object query(@RequestBody SampleQuery query) {
		return executor.sampleQuery(query);
	}

}

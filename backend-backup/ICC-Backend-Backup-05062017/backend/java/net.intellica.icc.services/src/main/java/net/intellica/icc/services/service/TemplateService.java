package net.intellica.icc.services.service;

import java.util.ArrayList;
import java.util.List;
import net.intellica.icc.services.model.Template;
import net.intellica.icc.services.repo.TemplateRepository;
import net.intellica.icc.services.util.audit.HistoryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TemplateService {

	@Autowired private TemplateRepository repository;
	@Autowired private HistoryUtil historyUtil;

	public TemplateService() {
	}

	public Template get(String id, Boolean includeHistory) {
		return get(id, false, includeHistory);
	}

	public Template get(String id, Boolean fetchDefinitions, Boolean includeHistory) {
		Template result = repository.findOne(id);
		if (includeHistory && result == null)
			result = historyUtil.getLastRevision(Template.class, id);
		if (result != null) {
			if (fetchDefinitions)
				result.getVariables().size();
		}
		return result;
	}

	public Template save(Template template) {
		return repository.save(template);
	}

	public void delete(String id) {
		repository.delete(id);
	}

	public List<Template> listAll() {
		List<Template> result = new ArrayList<>();
		repository.findAll().forEach(result::add);
		result.sort((c1, c2) -> c1.getName().compareTo(c2.getName()));
		return result;
	}

	public List<Template> listByName(String name) {
		return repository.findAllByName(name);
	}

}

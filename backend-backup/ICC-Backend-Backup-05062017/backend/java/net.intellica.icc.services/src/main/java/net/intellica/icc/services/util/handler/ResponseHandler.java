package net.intellica.icc.services.util.handler;

import net.intellica.icc.services.model.ResponseWrapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class ResponseHandler {

	public Object generate(Object object) {
		ResponseWrapper wrapper = new ResponseWrapper();
		wrapper.setObject(object);
		return new ResponseEntity<>(wrapper, HttpStatus.OK);
	}

}

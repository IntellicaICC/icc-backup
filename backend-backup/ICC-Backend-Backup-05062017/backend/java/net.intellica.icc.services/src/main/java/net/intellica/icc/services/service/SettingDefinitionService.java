package net.intellica.icc.services.service;

import java.util.ArrayList;
import java.util.List;
import net.intellica.icc.services.model.SettingDefinition;
import net.intellica.icc.services.repo.SettingDefinitionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SettingDefinitionService {

	@Autowired private SettingDefinitionRepository repository;

	public SettingDefinitionService() {
	}

	public SettingDefinition get(String id) {
		return repository.findOne(id);
	}

	public SettingDefinition save(SettingDefinition definition) {
		return repository.save(definition);
	}

	public void delete(String id) {
		repository.delete(id);
	}

	public List<SettingDefinition> list() {
		List<SettingDefinition> result = new ArrayList<>();
		repository.findAll().forEach(result::add);
		return result;
	}

}

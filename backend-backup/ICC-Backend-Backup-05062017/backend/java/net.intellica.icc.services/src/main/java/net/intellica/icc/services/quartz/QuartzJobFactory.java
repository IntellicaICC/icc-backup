package net.intellica.icc.services.quartz;

import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.spi.JobFactory;
import org.quartz.spi.TriggerFiredBundle;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class QuartzJobFactory implements JobFactory {

	@Autowired private ApplicationContext applicationContext;

	@Override
	public Job newJob(TriggerFiredBundle bundle, Scheduler scheduler) throws SchedulerException {
		Job result;
		JobDetail jobDetail = bundle.getJobDetail();
		Class<? extends Job> jobClass = jobDetail.getJobClass();
		try {
			try {
				result = applicationContext.getBean(jobClass);
			}
			catch (BeansException e) {
				result = jobClass.newInstance();
			}
		}
		catch (Exception e1) {
			throw new SchedulerException("Problem instantiating class '" + jobDetail.getJobClass().getName() + "'", e1);
		}
		return result;
	}

}

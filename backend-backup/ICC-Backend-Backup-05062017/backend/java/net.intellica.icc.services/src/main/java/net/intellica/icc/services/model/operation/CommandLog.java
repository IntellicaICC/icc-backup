package net.intellica.icc.services.model.operation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.intellica.icc.services.util.other.ServiceConstants;

@Entity
@Table(name = "ICC_COMMAND_LOG")
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommandLog {

	private String id;
	private OperationType type;
	private String modelId;
	private String modelName;
	private String operationId;
	private String triggerId;
	private String instanceId;
	private Command command;
	private String userId;
	private String userName;
	private Date commandDate;

	public CommandLog() {
	}

	public CommandLog(Operation operation) {
		type = operation.getType();
		modelId = operation.getModelId();
		modelName = operation.getModelName();
		operationId = operation.getId();
		triggerId = operation.getTriggerId();
		instanceId = operation.getInstanceId();
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "TYPE", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public OperationType getType() {
		return type;
	}

	public void setType(OperationType type) {
		this.type = type;
	}

	@Column(name = "MODEL_ID", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	@Column(name = "MODEL_NAME", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	@Column(name = "OPERATION_ID", nullable = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getOperationId() {
		return operationId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	@Column(name = "TRIGGER_ID", nullable = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getTriggerId() {
		return triggerId;
	}

	public void setTriggerId(String triggerId) {
		this.triggerId = triggerId;
	}

	@Column(name = "INSTANCE_ID", nullable = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "COMMAND", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public Command getCommand() {
		return command;
	}

	public void setCommand(Command command) {
		this.command = command;
	}

	@Column(name = "USER_ID", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "USER_NAME", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "COMMAND_DATE", nullable = false)
	public Date getCommandDate() {
		return commandDate;
	}

	public void setCommandDate(Date commandDate) {
		this.commandDate = commandDate;
	}

}

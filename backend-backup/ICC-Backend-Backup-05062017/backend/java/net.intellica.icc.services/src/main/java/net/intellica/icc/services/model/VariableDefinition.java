package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import net.intellica.icc.services.model.sd.IdConverter;
import net.intellica.icc.services.model.sd.TemplateConverter;
import net.intellica.icc.services.util.other.ServiceConstants;
import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "ICC_VARIABLEDEF")
@JsonIgnoreProperties(ignoreUnknown = true)
public class VariableDefinition {

	private String id;
	private CommonProperties props;
	private Integer position;
	private String defaultValue;
	private Boolean required;
	private VariableType type;
	private Template template;
	private Set<Variable> variables;

	public VariableDefinition() {
		props = new CommonProperties();
		position = 1;
		defaultValue = "";
		required = false;
		type = VariableType.Standard;
		variables = new HashSet<>();
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Embedded
	public CommonProperties getProps() {
		return props;
	}

	public void setProps(CommonProperties props) {
		this.props = props;
	}

	@Column(name = "POSITION", nullable = false)
	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	@Lob
	@Column(name = "DEFAULT_VALUE", nullable = true)
	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	@Column(name = "REQUIRED", nullable = false)
	public Boolean getRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "TYPE", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public VariableType getType() {
		return type;
	}

	public void setType(VariableType type) {
		this.type = type;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "TEMPLATE_ID", nullable = false)
	@JsonSerialize(converter = IdConverter.class)
	@JsonDeserialize(converter = TemplateConverter.class)
	public Template getTemplate() {
		return template;
	}

	public void setTemplate(Template template) {
		this.template = template;
	}

	@OneToMany(mappedBy = "definition", fetch = FetchType.LAZY)
	@JsonIgnore
	public Set<Variable> getVariables() {
		return variables;
	}

	public void setVariables(Set<Variable> variables) {
		this.variables = variables;
	}

}

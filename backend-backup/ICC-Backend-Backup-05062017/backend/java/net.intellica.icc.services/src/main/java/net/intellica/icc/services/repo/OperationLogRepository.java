package net.intellica.icc.services.repo;

import java.util.List;
import net.intellica.icc.services.model.operation.OperationLog;
import net.intellica.icc.services.model.operation.OperationStatus;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OperationLogRepository extends PagingAndSortingRepository<OperationLog, String> {

	List<OperationLog> findAllByOperationIdOrderByOperationDateDesc(String operationId);

	OperationLog findFirstByOperationIdOrderByOperationDateDesc(String operationId);

	OperationLog findFirstByOperationIdAndStatusOrderByOperationDateAsc(String operationId, OperationStatus status);

}

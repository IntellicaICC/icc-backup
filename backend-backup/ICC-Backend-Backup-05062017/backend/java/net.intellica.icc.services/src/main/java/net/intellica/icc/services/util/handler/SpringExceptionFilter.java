//package net.intellica.icc.services.util.handler;
//
//import org.springframework.web.filter.OncePerRequestFilter;

//@Component
//@WebFilter("/*")
//public class SpringExceptionFilter extends OncePerRequestFilter {

//	@Autowired private SimpleExceptionHandler exceptionHandler;
//
//	@Override
//	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
//		try {
//			filterChain.doFilter(request, response);
//		}
//		catch (Exception e) {
//			if (exceptionHandler != null)
//				exceptionHandler.handleResponse(e);
//			else
//				System.out.println(e.getMessage() + ", exceptionHandler is null!");
//		}
//	}
//
//}

package net.intellica.icc.services.controller;

import net.intellica.icc.services.model.LocalParameter;
import net.intellica.icc.services.service.LocalParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/localparameter")
public class LocalParameterController {

	@Autowired private LocalParameterService parameterService;

	@RequestMapping(value = { "/{id}", "/{id}/" }, method = RequestMethod.GET)
	public Object get(@PathVariable String id) {
		return parameterService.get(id, false);
	}

	@RequestMapping(value = { "/save", "/save/" }, method = RequestMethod.POST)
	public Object save(@RequestBody LocalParameter parameter) {
		return parameterService.save(parameter).getId();
	}

	@RequestMapping(value = { "/{id}", "/{id}/" }, method = RequestMethod.DELETE)
	public Object delete(@PathVariable String id) {
		parameterService.delete(id);
		return null;
	}

	@RequestMapping(value = { "/explore", "/explore/" }, method = RequestMethod.GET)
	public Object explore(@RequestParam(value = "id", required = false) String id) {
		return parameterService.listByFolderId(id);
	}

}

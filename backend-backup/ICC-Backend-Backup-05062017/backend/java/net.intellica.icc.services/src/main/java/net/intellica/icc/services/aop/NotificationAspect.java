package net.intellica.icc.services.aop;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import net.intellica.icc.services.config.ServicesConfig;
import net.intellica.icc.services.model.Notification;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.security.AuthorizationUtil;
import net.intellica.icc.services.service.NotificationService;
import net.intellica.icc.services.util.aop.ValidationUtil;
import net.intellica.icc.services.util.dev.MathUtil;
import net.intellica.icc.services.util.handler.ResponseHandler;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import net.intellica.icc.template.exception.NotFoundException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class NotificationAspect {

	@Autowired private ApplicationContext context;
	@Autowired private AuthorizationUtil authorizationUtil;
	@Autowired private ValidationUtil validationUtil;
	@Autowired private ResponseHandler responseHandler;
	@Autowired private SimpleExceptionHandler exceptionHandler;
	@Autowired private ScheduledExecutorService executor;
	@Autowired private MathUtil mathUtil;
	@Autowired private NotificationService notificationService;

	private Integer minPageSize;
	private Integer maxPageSize;

	@Autowired
	public NotificationAspect(ServicesConfig servicesConfig) {
		minPageSize = servicesConfig.getMin_notification_size();
		maxPageSize = servicesConfig.getMax_notification_size();
	}

	@Around("execution(* net.intellica.icc.services.controller.NotificationController.get(..))")
	public Object getNotification(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			validationUtil.validateId((String)pjp.getArgs()[0]);
			result = pjp.proceed();
			if (result == null)
				throw new NotFoundException();
			else {
				Notification notification = (Notification)result;
				authorizationUtil.canViewNotification(notification, true);
				executor.schedule(() -> saveNotificationsAsRead(notification), 250, TimeUnit.MILLISECONDS);
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.NotificationController.list(..))")
	public Object listNotifications(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		Integer page = (Integer)pjp.getArgs()[0];
		Integer size = (Integer)pjp.getArgs()[1];
		try {
			validationUtil.validateLicense();
			page = mathUtil.clamp(page, 0, Integer.MAX_VALUE);
			size = mathUtil.clamp(size, minPageSize, maxPageSize);
			result = pjp.proceed(new Object[] { page, size });
			if (result == null)
				throw new NotFoundException();
			else {
				List<Notification> list = (List<Notification>)result;
				List<Notification> unread = list.stream().filter(n -> !n.isRead()).collect(Collectors.toList());
				executor.schedule(() -> saveNotificationsAsRead(unread.toArray(new Notification[0])), 250, TimeUnit.MILLISECONDS);
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.NotificationController.peek(..))")
	public Object peekNotifications(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			result = pjp.proceed();
			if (result == null)
				throw new NotFoundException();
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.NotificationController.count(..))")
	public Object countNotifications(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			result = pjp.proceed();
			if (result == null)
				throw new NotFoundException();
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.NotificationController.deleteAll(..))")
	public Object deleteNotifications(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			result = pjp.proceed();
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	private void saveNotificationsAsRead(Notification... notifications) {
		List<Notification> list = Arrays.asList(notifications);
		list.forEach(n -> {
			n.setRead(true);
			n.setReadDate(new Date());
		});
		notificationService.saveAll(list);
	}

}

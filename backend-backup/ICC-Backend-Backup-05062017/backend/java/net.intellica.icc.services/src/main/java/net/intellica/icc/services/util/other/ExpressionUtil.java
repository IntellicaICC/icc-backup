package net.intellica.icc.services.util.other;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import net.intellica.icc.services.model.Folder;
import net.intellica.icc.services.model.GlobalParameter;
import net.intellica.icc.services.model.LocalParameter;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.security.AuthorizationUtil;
import net.intellica.icc.services.service.FolderService;
import net.intellica.icc.services.service.GlobalParameterService;
import net.intellica.icc.services.service.LocalParameterService;
import net.intellica.icc.services.service.UserService;
import net.intellica.icc.template.exception.ExpressionException;
import net.intellica.icc.template.exception.TechnicalException;
import net.intellica.icc.template.model.GlobalVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.EvaluationException;
import org.springframework.expression.Expression;
import org.springframework.expression.ParseException;
import org.springframework.expression.ParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

@Component
public class ExpressionUtil {

	private static final int TRIM_LENGTH = 17;

	@Autowired private FolderService folderService;
	@Autowired private GlobalParameterService globalParameterService;
	@Autowired private LocalParameterService localParameterService;
	@Autowired private AuthorizationUtil authorizationUtil;
	@Autowired private UserService userService;

	private final SpelExpressionParser parser;
	private final Map<String, AtomicInteger> contexts;

	public ExpressionUtil() {
		parser = new SpelExpressionParser();
		contexts = new ConcurrentHashMap<>();
	}

	public Object prepareContext(String contextName, String folderId, Map<String, Object> extraParameters) {
		Object result;
		User user = null;
		if (extraParameters == null)
			extraParameters = new HashMap<>();
		if (extraParameters.containsKey(GlobalVariable.UserID))
			user = userService.get((String)extraParameters.get(GlobalVariable.UserID), false);
		Map<String, Object> variables = new HashMap<>();// context'deki tum variable'lari alma komutu olmadigindan
		for (GlobalParameter gp : globalParameterService.listAll())
			if (gp.getProps().isActive())
				variables.put(gp.getProps().getName(), gp.getValue());
		result = createRootObject(contextName, variables);
		if (!StringUtils.isEmpty(folderId)) {
			Folder folder = folderService.get(folderId, false);
			LinkedList<Folder> allFolders = new LinkedList<>();
			for (Folder f = folder; f != null; f = f.getParentFolder())
				allFolders.addFirst(f);
			for (Folder f : allFolders) {
				Map<String, Object> folderContext = new HashMap<>(variables);
				Boolean contextChanged = false;
				for (LocalParameter lp : localParameterService.list(f)) {
					// calistiran kullanicinin bu parametreyi execute yetkisi varmi?
					if (lp.getProps().isActive() && authorizationUtil.canExecuteLocalParameter(lp, user, false)) {
						String rawValue = lp.getValue();
						Object evaluatedValue;
						try {
							evaluatedValue = evaluate(result, rawValue, null);
						}
						catch (ExpressionException e) {
							evaluatedValue = rawValue;
						}
						folderContext.put(lp.getProps().getName(), evaluatedValue);
						contextChanged = true;
					}
				}
				if (contextChanged) {
					variables.putAll(folderContext);
					result = createRootObject(contextName, variables);
				}
			}
		}
		else {// top folder
			Map<String, Object> folderContext = new HashMap<>(variables);
			Boolean contextChanged = false;
			for (LocalParameter lp : localParameterService.listByFolderId(null)) {
				if (lp.getProps().isActive()) {
					String rawValue = lp.getValue();
					Object evaluatedValue;
					try {
						evaluatedValue = evaluate(result, rawValue, null);
					}
					catch (ExpressionException e) {
						evaluatedValue = rawValue;
					}
					folderContext.put(lp.getProps().getName(), evaluatedValue);
					contextChanged = true;
				}
			}
			if (contextChanged) {
				variables.putAll(folderContext);
				result = createRootObject(contextName, variables);
			}
		}
		if (!CollectionUtils.isEmpty(extraParameters)) {
			variables.putAll(extraParameters);
			result = createRootObject(contextName, variables);
		}
		return result;
	}

	private Object createRootObject(String contextName, Map<String, Object> variables) {
		Object result = new Object();
		try {
			ClassPool classPool = ClassPool.getDefault();
			CtClass ctClass = classPool.makeClass(getFullName(contextName));
			for (String variable : variables.keySet())
				ctClass.addField(CtField.make("public Object " + variable + ";", ctClass));
			Class<Object> clazz = ctClass.toClass();
			result = clazz.newInstance();
			for (String variable : variables.keySet())
				clazz.getField(variable).set(result, variables.get(variable));
		}
		catch (Exception e) {
			throw new TechnicalException("Unable to create root object!", e);
		}
		return result;
	}

	public <T> Object evaluate(Object rootObject, String expression, Class<T> clazz) {
		Object result = null;
		try {
			ParserContext parserContext = new ExpressionContext();
			Expression parsedExpression = parser.parseExpression(expression, parserContext);
			if (rootObject == null) {
				if (clazz == null)
					result = parsedExpression.getValue();
				else {
					try {
						result = parsedExpression.getValue(clazz);
					}
					catch (Exception e) {
						result = parsedExpression.getValue(String.class);
						result = parser.parseExpression((String)result).getValue(clazz);
					}
				}
			}
			else {
				if (clazz == null)
					result = parsedExpression.getValue(rootObject);
				else {
					try {
						result = parsedExpression.getValue(rootObject, clazz);
					}
					catch (Exception e) {
						result = parsedExpression.getValue(rootObject, String.class);
						result = parser.parseExpression((String)result).getValue(clazz);
					}
				}
			}
		}
		catch (ParseException e) {
			String trimmedExpression = expression.length() > TRIM_LENGTH ? expression.substring(0, TRIM_LENGTH) + "..." : expression;
			String parseMessage = String.format("Unable to parse expression! (%s)", trimmedExpression);
			throw new ExpressionException(parseMessage, e);
		}
		catch (EvaluationException e) {
			throw new ExpressionException("Unable to evaluate expression!", e);
		}
		return result;
	}

	private String getFullName(String context) {
		return "RC_" + context + "_" + getVersion(context);
	}

	private synchronized Integer getVersion(String context) {
		Integer result;
		if (!contexts.containsKey(context))
			contexts.put(context, new AtomicInteger(1));
		result = contexts.get(context).getAndIncrement();
		return result;
	}

}

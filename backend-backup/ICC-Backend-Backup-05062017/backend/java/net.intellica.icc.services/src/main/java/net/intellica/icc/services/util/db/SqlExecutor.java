package net.intellica.icc.services.util.db;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.persistence.AttributeConverter;
import javax.sql.DataSource;
import net.intellica.icc.services.model.Connection;
import net.intellica.icc.services.model.SampleQuery;
import net.intellica.icc.services.model.SampleResult;
import net.intellica.icc.services.service.ConnectionService;
import net.intellica.icc.services.util.aop.ValidationUtil;
import net.intellica.icc.services.util.other.ExpressionUtil;
import net.intellica.icc.template.exception.DatabaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

@Component
public class SqlExecutor {

	@Autowired private ConnectionService connectionService;
	@Autowired private ExpressionUtil expressionUtil;
	@Autowired private JdbcUtil jdbcUtil;
	@Autowired private ValidationUtil validationUtil;
	@Autowired private AttributeConverter<String, String> ac;

	public SqlExecutor() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		}
		catch (Exception e) {
			throw new DatabaseException("Can not find Oracle JDBC driver!");
		}
		try {
			Class.forName("org.apache.hive.jdbc.HiveDriver");
		}
		catch (Exception e) {
			throw new DatabaseException("Can not find Hive JDBC driver!");
		}
		try {
			Class.forName("com.teradata.jdbc.TeraDriver");
		}
		catch (Exception e) {
			throw new DatabaseException("Can not find Teradata JDBC driver!");
		}
	}

	public SampleResult sampleQuery(SampleQuery query) {
		SampleResult result = new SampleResult();
		Connection connection = connectionService.get(query.getConnectionId(), false);
		DataSource dataSource = getDataSource(connection, false);
		JdbcTemplate jdbc = new JdbcTemplate(dataSource);
		String contextName = String.valueOf(System.nanoTime() + new Random().nextInt(1000));
		Object context = expressionUtil.prepareContext(contextName, query.getFolderId(), null);
		String sql = (String)expressionUtil.evaluate(context, query.getSql(), String.class);
		validationUtil.validateSelect(sql);
		jdbc.query(sql, new ResultSetExtractor<Object>() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				ResultSetMetaData metaData = rs.getMetaData();
				for (int i = 0; i < metaData.getColumnCount(); i++)
					result.getColumns().add(metaData.getColumnName(i + 1));
				List<Object[]> rowList = new ArrayList<>();
				Integer count = 0;
				while (count < 10 && rs.next()) {
					Object[] row = new Object[metaData.getColumnCount()];
					for (int i = 0; i < metaData.getColumnCount(); i++) {
						row[i] = jdbcUtil.represent(rs, i + 1);
					}
					rowList.add(row);
					count++;
				}
				result.setRows(rowList.toArray(new Object[rowList.size()][metaData.getColumnCount()]));
				return null;
			}
		});
		return result;
	}

	public Boolean testConnection(Connection connection) {
		Boolean result = false;
		try {
			DataSource dataSource = getDataSource(connection, true);
			dataSource.getConnection();
			result = true;
		}
		catch (Exception e) {
		}
		return result;
	}

	private DataSource getDataSource(Connection connection, Boolean isTest) {
		DriverManagerDataSource result = new DriverManagerDataSource();
		result.setUrl(connection.getDatabaseUrl());
		result.setUsername(connection.getDatabaseUser());
		String password = isTest ? connection.getDatabasePassword() : ac.convertToEntityAttribute(connection.getDatabasePassword());
		result.setPassword(password);
		return result;
	}

}

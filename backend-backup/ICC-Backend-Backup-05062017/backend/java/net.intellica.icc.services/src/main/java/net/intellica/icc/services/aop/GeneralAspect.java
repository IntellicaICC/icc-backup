package net.intellica.icc.services.aop;

import net.intellica.icc.services.model.User;
import net.intellica.icc.services.security.AuditUtil;
import net.intellica.icc.services.util.aop.ValidationUtil;
import net.intellica.icc.services.util.handler.ResponseHandler;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class GeneralAspect {

	@Autowired private ApplicationContext context;
	@Autowired private ValidationUtil validationUtil;
	@Autowired private ResponseHandler responseHandler;
	@Autowired private SimpleExceptionHandler exceptionHandler;
	@Autowired private AuditUtil auditUtil;

	@Around("execution(* net.intellica.icc.services.controller.GeneralController.login(..))")
	public Object login(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			result = pjp.proceed();
			auditUtil.logSuccessiveLogin(pjp);
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			auditUtil.logFailedLogin(pjp, e.getMessage());
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.GeneralController.version(..))")
	public Object version(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			result = pjp.proceed();
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

}

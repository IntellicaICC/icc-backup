package net.intellica.icc.services.model.rule;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import net.intellica.icc.services.model.sd.NodeDeserializer;
import net.intellica.icc.services.util.dev.HashUtil;
import net.intellica.icc.services.util.thread.ThreadUtil;
import net.intellica.icc.template.exception.CanceledException;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(using = NodeDeserializer.class)
public abstract class Node implements Runnable, Serializable {

	private static final long serialVersionUID = 1L;
	private static final Integer CONTROL_PERIOD_MS = 1000;

	// ui-related
	private Integer id;
	private Integer x;
	private Integer y;
	protected NodeType type;

	// engine-related
	private List<Node> incoming;
	private List<Node> outgoing;
	private Graph graph;
	private NodeStatus status;
	private Workflow workflow;
	private Map<String, Object> input;
	private Map<String, Object> output;
	private Boolean toBeExecuted;
	private Boolean paused;
	private Boolean canceled;
	private Exception nodeError;
	private String lastDataset;

	public Node() {
		incoming = new ArrayList<>();
		outgoing = new ArrayList<>();
		input = new HashMap<>();
		output = new HashMap<>();
		toBeExecuted = false;
		paused = false;
		canceled = false;
		status = NodeStatus.Idle;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public final NodeType getType() {
		return type;
	}

	@JsonIgnore
	public Graph getGraph() {
		return graph;
	}

	@JsonIgnore
	public void setGraph(Graph graph) {
		this.graph = graph;
		// set incoming nodes
		incoming.clear();
		for (Integer fromId : graph.getLinks().keySet()) {
			Set<Integer> inLinks = graph.getLinks().get(fromId);
			if (!CollectionUtils.isEmpty(inLinks) && inLinks.contains(id))
				incoming.add(graph.getNode(fromId));
		}
		for (Integer fromId : graph.getTrueLinks().keySet()) {
			Set<Integer> trueLinks = graph.getTrueLinks().get(fromId);
			if (!CollectionUtils.isEmpty(trueLinks) && trueLinks.contains(id))
				incoming.add(graph.getNode(fromId));
		}
		for (Integer fromId : graph.getFalseLinks().keySet()) {
			Set<Integer> falseLinks = graph.getFalseLinks().get(fromId);
			if (!CollectionUtils.isEmpty(falseLinks) && falseLinks.contains(id))
				incoming.add(graph.getNode(fromId));
		}
		// set outgoing nodes
		outgoing.clear();
		Set<Integer> outLinks = graph.getLinks().get(id);
		if (!CollectionUtils.isEmpty(outLinks))
			outgoing.addAll(outLinks.stream().map(linkId -> graph.getNode(linkId)).collect(Collectors.toSet()));
	}

	@JsonIgnore
	public NodeStatus getStatus() {
		return status;
	}

	@JsonIgnore
	protected void setStatus(NodeStatus status) {
		workflow.setNodeStatus(this, status);
		this.status = status;
	}

	@JsonIgnore
	public Workflow getWorkflow() {
		return workflow;
	}

	@JsonIgnore
	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}

	@JsonIgnore
	public List<Node> getIncoming() {
		return incoming;
	}

	@JsonIgnore
	public List<Node> getOutgoing() {
		return outgoing;
	}

	@JsonIgnore
	public Map<String, Object> getInput() {
		return input;
	}

	@JsonIgnore
	public Map<String, Object> getOutput() {
		return output;
	}

	@JsonIgnore
	public Boolean isToBeExecuted() {
		return toBeExecuted;
	}

	@JsonIgnore
	public void markForExecution() {
		toBeExecuted = true;
	}

	@JsonIgnore
	public Boolean isPaused() {
		return paused;
	}

	@JsonIgnore
	public Boolean workflowHasError() {
		return getWorkflow().hasError();
	}

	@JsonIgnore
	public Boolean isCanceled() {
		return canceled;
	}

	@JsonIgnore
	public Exception getNodeError() {
		return nodeError;
	}

	protected void setNodeError(Exception e) {
		nodeError = e;
	}

	@JsonIgnore
	public String getLastDataset() {
		return lastDataset;
	}

	public void setLastDataset(String lastDataset) {
		this.lastDataset = lastDataset;
	}

	@JsonIgnore
	public Boolean nodeHasError() {
		return nodeError != null;
	}

	@Override
	public void run() {
		checkPause();
		if (!workflowHasError() && !isCanceled()) {
			setStatus(NodeStatus.Executing);
			doWork();
			if (nodeHasError()) {
				setStatus(NodeStatus.Error);
				getWorkflow().error(getNodeError());
			}
			else if (workflowHasError() || isCanceled()) {
				setStatus(NodeStatus.Error);
				setNodeError(new CanceledException("Canceled!"));
			}
			else {
				setStatus(NodeStatus.Finished);
				createOutput();
				getWorkflow().execute(this, getOutgoing());
			}
		}
	}

	public void bypass() {
		checkPause();
		if (!workflowHasError() && !isCanceled()) {
			createOutput();
			getWorkflow().bypass(this, getOutgoing());
		}
	}

	public void cancel() {
		canceled = true;
	}

	public void pause() {
		paused = true;
	}

	public void resume() {
		paused = false;
	}

	protected void createOutput() {
		Map<String, Object> finalOutput = new HashMap<>();
		finalOutput.putAll(getInput());
		finalOutput.putAll(getOutput());
		getOutput().putAll(finalOutput);
	}

	// node'lar tarafindan yurutulen is
	protected abstract void doWork();

	private void checkPause() {
		while (isPaused()) {
			if (status != NodeStatus.Waiting)
				setStatus(NodeStatus.Waiting);
			ThreadUtil.waitFor(CONTROL_PERIOD_MS);
		}
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "." + id;
	}

	@Override
	public boolean equals(Object obj) {
		Boolean result = false;
		if (obj instanceof Node) {
			Node node = (Node)obj;
			result = ObjectUtils.nullSafeEquals(type, node.type) &&
				ObjectUtils.nullSafeEquals(id, node.id) &&
				ObjectUtils.nullSafeEquals(x, node.x) &&
				ObjectUtils.nullSafeEquals(y, node.y);
		}
		return result;
	}

	@Override
	public int hashCode() {
		return HashUtil.generate(type, id, x, y);
	}

}

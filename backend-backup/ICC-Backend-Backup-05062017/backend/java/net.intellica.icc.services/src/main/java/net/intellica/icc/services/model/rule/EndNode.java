package net.intellica.icc.services.model.rule;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EndNode extends Node {

	private static final long serialVersionUID = 1L;

	public EndNode() {
		super();
		type = NodeType.End;
	}

	@Override
	protected void doWork() {
		// do nothing
	}

	@Override
	protected void createOutput() {
		super.createOutput();
		getWorkflow().getEndOutput().putAll(getOutput());
		getWorkflow().setLastDataset(getLastDataset());
	}

}

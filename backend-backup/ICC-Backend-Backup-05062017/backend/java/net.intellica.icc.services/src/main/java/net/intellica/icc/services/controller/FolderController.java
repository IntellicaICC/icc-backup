package net.intellica.icc.services.controller;

import java.util.List;
import net.intellica.icc.services.model.Folder;
import net.intellica.icc.services.model.Job;
import net.intellica.icc.services.model.LocalParameter;
import net.intellica.icc.services.model.Rule;
import net.intellica.icc.services.service.FolderService;
import net.intellica.icc.services.service.JobService;
import net.intellica.icc.services.service.LocalParameterService;
import net.intellica.icc.services.service.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/folder")
public class FolderController {

	@Autowired private FolderService folderService;
	@Autowired private JobService jobService;
	@Autowired private RuleService ruleService;
	@Autowired private LocalParameterService parameterService;

	@RequestMapping(value = { "/{id}", "/{id}/" }, method = RequestMethod.GET)
	public Object get(@PathVariable String id) {
		return folderService.get(id, false);
	}

	@RequestMapping(value = { "/{id}/parents", "/{id}/parents/" }, method = RequestMethod.GET)
	public Object getParents(@PathVariable String id) {
		return folderService.listAllParents(id);
	}

	@RequestMapping(value = { "/save", "/save/" }, method = RequestMethod.POST)
	public Object save(@RequestBody Folder folder) {
		Boolean initialActive = null;
		Folder dbFolder = folderService.get(folder.getId(), false);
		if (dbFolder != null)
			initialActive = dbFolder.getProps().isActive();
		Folder result = folderService.save(folder);
		Boolean newActive = result.getProps().isActive();
		// aktiflik degisti, alt objeleri de degistir
		if (initialActive != null && initialActive != newActive)
			updateRecursively(result, newActive);
		return result.getId();
	}

	@RequestMapping(value = { "/{id}", "/{id}/" }, method = RequestMethod.DELETE)
	public Object delete(@PathVariable String id) {
		folderService.delete(id);
		return null;
	}

	@RequestMapping(value = { "/explore", "/explore/" }, method = RequestMethod.GET)
	public Object explore(@RequestParam(value = "id", required = false) String id) {
		return folderService.listByParentFolderId(id);
	}

	private void updateRecursively(Folder folder, Boolean newActive) {
		List<Job> folderJobs = jobService.listByFolderId(folder.getId());
		for (Job job : folderJobs) {
			if (job.getProps().isActive() != newActive) {
				job.getProps().setActive(newActive);
				jobService.save(job);
			}
		}
		List<Rule> folderRules = ruleService.listByFolderId(folder.getId());
		for (Rule rule : folderRules) {
			if (rule.getProps().isActive() != newActive) {
				rule.getProps().setActive(newActive);
				ruleService.save(rule);
			}
		}
		List<LocalParameter> folderParameters = parameterService.listByFolderId(folder.getId());
		for (LocalParameter parameter : folderParameters) {
			if (parameter.getProps().isActive() != newActive) {
				parameter.getProps().setActive(newActive);
				parameterService.save(parameter);
			}
		}
		List<Folder> folderSubFolders = folderService.listByParentFolderId(folder.getId());
		for (Folder subFolder : folderSubFolders) {
			if (subFolder.getProps().isActive() != newActive) {
				subFolder.getProps().setActive(newActive);
				folderService.save(subFolder);
			}
			updateRecursively(subFolder, newActive);
		}
	}

}

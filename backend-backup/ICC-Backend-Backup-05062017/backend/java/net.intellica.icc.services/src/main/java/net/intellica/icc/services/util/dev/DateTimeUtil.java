package net.intellica.icc.services.util.dev;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import org.springframework.stereotype.Component;

@Component
public class DateTimeUtil {

	public Date adjustFirstRunDate(Date date) {
		LocalDateTime ldt = dateToLocalDateTime(date);
		LocalDateTime minDate = getMinimumOperationDateInternal();
		LocalDateTime maxDate = getMaximumOperationDateInternal();
		return localDateTimeToDate(clamp(ldt, minDate, maxDate));
	}

	public Date getMinimumOperationDate() {
		return localDateTimeToDate(getMinimumOperationDateInternal());
	}

	public Date getMaximumOperationDate() {
		return localDateTimeToDate(getMaximumOperationDateInternal());
	}

	public Date getStartOfToday() {
		return localDateTimeToDate(LocalDate.now().atStartOfDay());
	}

	public Date getEndOfToday() {
		return localDateTimeToDate(LocalDateTime.of(LocalDate.now(), LocalTime.of(23, 59, 59)));
	}

	public Date getStartOfTomorrow() {
		return localDateTimeToDate(LocalDate.now().plusDays(1).atStartOfDay());
	}

	public Date getEndOfTomorrow() {
		return localDateTimeToDate(LocalDateTime.of(LocalDate.now().plusDays(1), LocalTime.of(23, 59, 59)));
	}

	public Date getStartOfYesterday() {
		return localDateTimeToDate(LocalDate.now().minusDays(1).atStartOfDay());
	}

	public Date getEndOfYesterday() {
		return localDateTimeToDate(LocalDateTime.of(LocalDate.now().minusDays(1), LocalTime.of(23, 59, 59)));
	}

	public Boolean needsExtension(Date date, Integer minimumMonths) {
		return dateToLocalDateTime(date).isBefore(LocalDateTime.now().plusMonths(minimumMonths));
	}

	public Date getMinPlus(Integer minutes) {
		return localDateTimeToDate(getMinimumOperationDateInternal().plusMinutes(minutes));
	}

	private LocalDateTime getMinimumOperationDateInternal() {
		return trim(LocalDateTime.now().plusMinutes(1));
	}

	private LocalDateTime getMaximumOperationDateInternal() {
		return LocalDate.now().plusDays(30).atTime(23, 59);
	}

	private LocalDateTime clamp(LocalDateTime ldt, LocalDateTime begin, LocalDateTime end) {
		LocalDateTime result = ldt;
		if (result.isBefore(begin))
			result = begin;
		else if (result.isAfter(end))
			result = end;
		result = trim(result);
		return result;
	}

	private LocalDateTime trim(LocalDateTime ldt) {
		return LocalDateTime.of(ldt.toLocalDate(), LocalTime.of(ldt.getHour(), ldt.getMinute()));
	}

	private Date localDateTimeToDate(LocalDateTime date) {
		return Date.from(date.atZone(ZoneId.systemDefault()).toInstant());
	}

	private LocalDateTime dateToLocalDateTime(Date date) {
		return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
	}

}

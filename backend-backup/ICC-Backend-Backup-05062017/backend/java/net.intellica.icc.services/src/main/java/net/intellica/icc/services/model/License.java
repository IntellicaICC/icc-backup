package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import net.intellica.icc.services.util.other.ServiceConstants;

@Entity
@Table(name = "ICC_LICENSE")
@JsonIgnoreProperties(ignoreUnknown = true)
public class License {

	private String id;
	private String license;

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	@JsonIgnore
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "LICENSE", nullable = false, length = ServiceConstants.Model.LONG_STRING_LENGTH)
	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

}

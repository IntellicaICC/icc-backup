package net.intellica.icc.services.model.report;

public enum ReportType {

	AuthorizationRoleObject,
	AuthorizationRoleUser,
	AuthorizationUserObject,
	AuthorizationUserRole

}

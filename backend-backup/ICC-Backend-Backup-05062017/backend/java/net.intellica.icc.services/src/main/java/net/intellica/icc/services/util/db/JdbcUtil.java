package net.intellica.icc.services.util.db;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Types;
import java.text.DecimalFormat;
import java.util.Locale;
import org.springframework.stereotype.Component;

@Component
public class JdbcUtil {

	private DecimalFormat decimalFormat;

	public JdbcUtil() {
		decimalFormat = new DecimalFormat();
	}

	public String represent(ResultSet rs, Integer index) {
		String result = "";
		try {
			ResultSetMetaData metaData = rs.getMetaData();
			if (rs.getObject(index) != null) {
				switch (metaData.getColumnType(index)) {
					case Types.FLOAT:
						result = decimalFormat.format(rs.getFloat(index));
					case Types.REAL:
					case Types.DECIMAL:
						result = decimalFormat.format(rs.getDouble(index));
						break;
					case Types.DATE:
					case Types.TIMESTAMP:
					case Types.TIME:
					case Types.TIME_WITH_TIMEZONE:
					case Types.TIMESTAMP_WITH_TIMEZONE:
						result = String.format(Locale.US, "%1$tB %1$td, %1$tY %1$tI:%1$tM %1$Tp", rs.getDate(index));
						break;
					default:
						result = rs.getString(index);
						break;
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace(System.err);
		}
		return result;
	}

}

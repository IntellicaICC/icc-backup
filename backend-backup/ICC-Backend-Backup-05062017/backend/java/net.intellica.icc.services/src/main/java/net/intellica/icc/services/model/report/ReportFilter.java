package net.intellica.icc.services.model.report;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import net.intellica.icc.services.util.other.ServiceConstants;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReportFilter {

	protected ReportType reportType;
	private Integer page;
	private Integer size;

	public ReportFilter() {
		page = 0;
		size = ServiceConstants.Report.DEFAULT_SIZE;
	}

	@JsonIgnore
	public ReportType getReportType() {
		return reportType;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		if (page != null) {
			this.page = page;
			if (this.page < 0)
				this.page = 0;
		}
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		if (size != null) {
			this.size = size;
			if (this.size < ServiceConstants.Report.MIN_SIZE)
				this.size = ServiceConstants.Report.MIN_SIZE;
			if (this.size > ServiceConstants.Report.MAX_SIZE)
				this.size = ServiceConstants.Report.MAX_SIZE;
		}
	}

}

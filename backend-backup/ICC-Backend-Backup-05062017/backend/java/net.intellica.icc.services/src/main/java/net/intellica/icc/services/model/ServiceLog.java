package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.intellica.icc.services.util.other.ServiceConstants;

@Entity
@Table(name = "ICC_SERVICE_LOG")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceLog {

	private String id;
	private ServiceCategory category;
	private String remoteIp;
	private Integer remotePort;
	private String localIp;
	private Integer localPort;
	private String uri;
	private String method;
	private Date callDate;
	private String userId;
	private String username;
	private String parameters;

	public ServiceLog() {
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "CATEGORY", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public ServiceCategory getCategory() {
		return category;
	}

	public void setCategory(ServiceCategory category) {
		this.category = category;
	}

	@Column(name = "REMOTE_IP", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getRemoteIp() {
		return remoteIp;
	}

	public void setRemoteIp(String remoteIp) {
		this.remoteIp = remoteIp;
	}

	@Column(name = "REMOTE_PORT", nullable = false)
	public Integer getRemotePort() {
		return remotePort;
	}

	public void setRemotePort(Integer remotePort) {
		this.remotePort = remotePort;
	}

	@Column(name = "LOCAL_IP", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getLocalIp() {
		return localIp;
	}

	public void setLocalIp(String localIp) {
		this.localIp = localIp;
	}

	@Column(name = "LOCAL_PORT", nullable = false)
	public Integer getLocalPort() {
		return localPort;
	}

	public void setLocalPort(Integer localPort) {
		this.localPort = localPort;
	}

	@Column(name = "URI", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	@Column(name = "METHOD", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	@Column(name = "CALL_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCallDate() {
		return callDate;
	}

	public void setCallDate(Date callDate) {
		this.callDate = callDate;
	}

	@Column(name = "USER_ID", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "USERNAME", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Lob
	@Column(name = "PARAMETERS", nullable = true)
	public String getParameters() {
		return parameters;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

	// TODO donulen HTTP status kodu

}

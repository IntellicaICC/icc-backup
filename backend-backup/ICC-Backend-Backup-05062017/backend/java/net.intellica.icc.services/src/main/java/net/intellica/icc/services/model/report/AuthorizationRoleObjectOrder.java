package net.intellica.icc.services.model.report;

import java.util.Comparator;

public class AuthorizationRoleObjectOrder implements Comparator<AuthorizationRoleObjectReport> {

	@Override
	public int compare(AuthorizationRoleObjectReport r1, AuthorizationRoleObjectReport r2) {
		Integer result = r1.getRoleName().compareTo(r2.getRoleName());
		if (result == 0)
			result = r1.getObjectName().compareTo(r2.getObjectName());
		if (result == 0)
			result = r1.getObjectType().compareTo(r2.getObjectType());
		return result;
	}

}

package net.intellica.icc.services.model.report;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorizationRoleUserFilter extends ReportFilter {

	private String roleId;
	private Boolean includePassive;

	public AuthorizationRoleUserFilter() {
		reportType = ReportType.AuthorizationRoleUser;
		includePassive = false;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public Boolean getIncludePassive() {
		return includePassive;
	}

	public void setIncludePassive(Boolean includePassive) {
		this.includePassive = includePassive;
	}

}

package net.intellica.icc.services.aop;

import java.util.Collection;
import net.intellica.icc.services.model.Folder;
import net.intellica.icc.services.model.Rule;
import net.intellica.icc.services.model.Schedule;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.model.operation.OperationType;
import net.intellica.icc.services.security.AuthorizationUtil;
import net.intellica.icc.services.service.FolderService;
import net.intellica.icc.services.service.RuleService;
import net.intellica.icc.services.util.aop.FillUtil;
import net.intellica.icc.services.util.aop.PreSaveUtil;
import net.intellica.icc.services.util.aop.RefineUtil;
import net.intellica.icc.services.util.aop.SimplifyUtil;
import net.intellica.icc.services.util.aop.ValidationUtil;
import net.intellica.icc.services.util.dev.DateTimeUtil;
import net.intellica.icc.services.util.handler.ResponseHandler;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import net.intellica.icc.template.exception.NotFoundException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Aspect
@Component
public class RuleAspect {

	@Autowired private ApplicationContext context;
	@Autowired private AuthorizationUtil authorizationUtil;
	@Autowired private ValidationUtil validationUtil;
	@Autowired private ResponseHandler responseHandler;
	@Autowired private SimpleExceptionHandler exceptionHandler;
	@Autowired private PreSaveUtil preSaveUtil;
	@Autowired private SimplifyUtil simplifyUtil;
	@Autowired private FillUtil fillUtil;
	@Autowired private RefineUtil refineUtil;
	@Autowired private RuleService ruleService;
	@Autowired private FolderService folderService;
	@Autowired private DateTimeUtil dateTimeUtil;

	@Around("execution(* net.intellica.icc.services.controller.RuleController.get(..))")
	public Object getRule(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			validationUtil.validateId((String)pjp.getArgs()[0]);
			result = pjp.proceed();
			if (result == null)
				throw new NotFoundException();
			else {
				Rule rule = (Rule)result;
				authorizationUtil.canViewRule(rule, true);
				fillUtil.fillRuleFields(rule);
				if (authorizationUtil.isNormalUser())
					rule.setRights(simplifyUtil.simplify(rule.getRights()));
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.RuleController.explore(..))")
	public Object exploreRules(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String id = (String)pjp.getArgs()[0];
			Folder folder = null;
			if (!StringUtils.isEmpty(id)) {
				validationUtil.validateId(id);
				folder = folderService.get(id, false);
				if (folder == null)
					throw new NotFoundException();
			}
			result = pjp.proceed();
			Collection<Rule> collection = (Collection<Rule>)result;
			if (authorizationUtil.isNormalUser()) {
				Collection<Rule> refined = authorizationUtil.refineRules(collection);
				fillUtil.fillRuleFields(refined);
				refineUtil.refineRuleFields(refined);
				refined.forEach(r -> r.setRights(simplifyUtil.simplify(r.getRights())));
				result = refined;
			}
			else {
				fillUtil.fillRuleFields(collection);
				refineUtil.refineRuleFields(collection);
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.RuleController.delete(..))")
	public Object deleteRule(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			if (authorizationUtil.isOperatorUser())
				authorizationUtil.throwDeleteException();
			validationUtil.validateId((String)pjp.getArgs()[0]);
			String id = (String)pjp.getArgs()[0];
			Rule toDelete = ruleService.get(id, false);
			if (toDelete == null)
				throw new NotFoundException();
			else {
				validationUtil.validateDelete(toDelete);
				if (authorizationUtil.isNormalUser())
					authorizationUtil.canDeleteRule(toDelete, true);
				result = pjp.proceed();
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.RuleController.save(..))")
	public Object saveRule(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			Rule rule = (Rule)pjp.getArgs()[0];
			Rule dbRule = StringUtils.isEmpty(rule.getId()) ? null : ruleService.get(rule.getId(), false);
			authorizationUtil.canSaveRule(rule, dbRule, true);
			validationUtil.validateSave(rule);
			preSaveUtil.fill(rule, dbRule);
			result = pjp.proceed();
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.RuleController.run(..))")
	public Object runRule(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String id = (String)pjp.getArgs()[0];
			validationUtil.validateId(id);
			Rule toRun = ruleService.get(id, false);
			if (toRun == null)
				throw new NotFoundException();
			else {
				authorizationUtil.canExecuteRule(toRun, true);
				validationUtil.validateRun(toRun);
				validationUtil.validateOperationDate(OperationType.Rule, id, dateTimeUtil.getMinimumOperationDate());
				result = pjp.proceed();
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.RuleController.schedule(..))")
	public Object scheduleRule(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String id = (String)pjp.getArgs()[0];
			Schedule schedule = (Schedule)pjp.getArgs()[1];
			validationUtil.validateId(id);
			validationUtil.validateSchedule(schedule);
			schedule.setFirstRunDate(dateTimeUtil.adjustFirstRunDate(schedule.getFirstRunDate()));
			Rule toRun = ruleService.get(id, false);
			if (toRun == null)
				throw new NotFoundException();
			else {
				authorizationUtil.canExecuteRule(toRun, true);
				validationUtil.validateSchedule(toRun);
				validationUtil.validateOperationDate(OperationType.Rule, id, schedule.getFirstRunDate());
				result = pjp.proceed();
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

}

package net.intellica.icc.services.aop;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import net.intellica.icc.services.config.ServicesConfig;
import net.intellica.icc.services.model.Schedule;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.model.operation.Command;
import net.intellica.icc.services.model.operation.Operation;
import net.intellica.icc.services.model.simple.SimpleOperation;
import net.intellica.icc.services.security.AuthorizationUtil;
import net.intellica.icc.services.service.OperationService;
import net.intellica.icc.services.util.aop.SimplifyUtil;
import net.intellica.icc.services.util.aop.ValidationUtil;
import net.intellica.icc.services.util.dev.DateTimeUtil;
import net.intellica.icc.services.util.dev.MathUtil;
import net.intellica.icc.services.util.handler.ResponseHandler;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import net.intellica.icc.template.exception.NotFoundException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class OperationAspect {

	@Autowired private ApplicationContext context;
	@Autowired private AuthorizationUtil authorizationUtil;
	@Autowired private ValidationUtil validationUtil;
	@Autowired private ResponseHandler responseHandler;
	@Autowired private SimpleExceptionHandler exceptionHandler;
	@Autowired private SimplifyUtil simplifyUtil;
	@Autowired private OperationService operationService;
	@Autowired private MathUtil mathUtil;
	@Autowired private DateTimeUtil dateTimeUtil;

	private static final Integer MAX_DAYS = 30;
	private Integer minPageSize;
	private Integer maxPageSize;

	@Autowired
	public OperationAspect(ServicesConfig servicesConfig) {
		minPageSize = servicesConfig.getMin_notification_size();
		maxPageSize = servicesConfig.getMax_notification_size();
	}

	@Around("execution(* net.intellica.icc.services.controller.OperationController.get(..))")
	public Object getOperation(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String operationId = (String)pjp.getArgs()[0];
			validationUtil.validateId(operationId);
			result = pjp.proceed();
			if (result == null)
				throw new NotFoundException();
			else {
				Operation operation = (Operation)result;
				authorizationUtil.canViewOperation(operation, true);
				result = simplifyUtil.simplify((Operation)result);
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.OperationController.getDetail(..))")
	public Object getOperationDetail(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String id = (String)pjp.getArgs()[0];
			validationUtil.validateId(id);
			result = pjp.proceed();
			if (result == null)
				throw new NotFoundException();
			else {
				Operation operation = operationService.get(id, false);
				authorizationUtil.canViewOperation(operation, true);
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.OperationController.listSchedule(..))")
	public Object listOperationSchedule(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			Date from = (Date)pjp.getArgs()[0];
			Date to = (Date)pjp.getArgs()[1];
			validationUtil.validateOperationDates(from, to);
			LocalDate now = LocalDate.now();
			LocalDate minLocal = now.plusDays(1);
			LocalDate maxLocal = now.plusDays(MAX_DAYS);
			LocalDate fromLocal = from.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			LocalDate toLocal = to.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			fromLocal = mathUtil.clamp(fromLocal, minLocal, maxLocal);
			toLocal = mathUtil.clamp(toLocal, fromLocal, maxLocal);
			Date newFrom = Date.from(fromLocal.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
			Date newTo = Date.from(toLocal.atTime(23, 59, 59).atZone(ZoneId.systemDefault()).toInstant());

			Integer page = (Integer)pjp.getArgs()[2];
			Integer size = (Integer)pjp.getArgs()[3];
			page = mathUtil.clamp(page, 0, Integer.MAX_VALUE);
			size = mathUtil.clamp(size, minPageSize, maxPageSize);

			result = pjp.proceed(new Object[] { newFrom, newTo, page, size });
			if (result == null)
				throw new NotFoundException();
			else {
				Collection<Operation> collection = (Collection<Operation>)result;
				Collection<Operation> finalCollection;
				if (authorizationUtil.isNormalUser()) {
					Collection<Operation> refined = authorizationUtil.refineOperations(collection);
					finalCollection = refined;
				}
				else
					finalCollection = collection;
				List<SimpleOperation> simples = finalCollection.stream().map(c -> simplifyUtil.simplify(c)).collect(Collectors.toList());
				result = simples;
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.OperationController.listToday(..))")
	public Object listOperationToday(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			result = pjp.proceed();
			if (result == null)
				throw new NotFoundException();
			else {
				Collection<Operation> collection = (Collection<Operation>)result;
				Collection<Operation> finalCollection;
				if (authorizationUtil.isNormalUser()) {
					Collection<Operation> refined = authorizationUtil.refineOperations(collection);
					finalCollection = refined;
				}
				else
					finalCollection = collection;
				List<SimpleOperation> simples = finalCollection.stream().map(c -> simplifyUtil.simplify(c)).collect(Collectors.toList());
				result = simples;
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.OperationController.listHistory(..))")
	public Object listOperationHistory(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			Date from = (Date)pjp.getArgs()[0];
			Date to = (Date)pjp.getArgs()[1];
			validationUtil.validateOperationDates(from, to);
			LocalDate now = LocalDate.now();
			LocalDate minLocal = now.minusDays(MAX_DAYS);
			LocalDate maxLocal = now.minusDays(1);
			LocalDate fromLocal = from.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			LocalDate toLocal = to.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			fromLocal = mathUtil.clamp(fromLocal, minLocal, maxLocal);
			toLocal = mathUtil.clamp(toLocal, fromLocal, maxLocal);
			Date newFrom = Date.from(fromLocal.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
			Date newTo = Date.from(toLocal.atTime(23, 59, 59).atZone(ZoneId.systemDefault()).toInstant());

			Integer page = (Integer)pjp.getArgs()[2];
			Integer size = (Integer)pjp.getArgs()[3];
			page = mathUtil.clamp(page, 0, Integer.MAX_VALUE);
			size = mathUtil.clamp(size, minPageSize, maxPageSize);

			result = pjp.proceed(new Object[] { newFrom, newTo, page, size });
			if (result == null)
				throw new NotFoundException();
			else {
				Collection<Operation> collection = (Collection<Operation>)result;
				Collection<Operation> finalCollection;
				if (authorizationUtil.isNormalUser()) {
					Collection<Operation> refined = authorizationUtil.refineOperations(collection);
					finalCollection = refined;
				}
				else
					finalCollection = collection;
				List<SimpleOperation> simples = finalCollection.stream().map(c -> simplifyUtil.simplify(c)).collect(Collectors.toList());
				result = simples;
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.OperationController.start(..))")
	public Object operationStart(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String id = (String)pjp.getArgs()[0];
			validationUtil.validateId(id);
			Operation operation = operationService.get(id, false);
			if (operation == null)
				throw new NotFoundException();
			else {
				validationUtil.validateOperationModel(operation);
				authorizationUtil.canExecuteOperation(operation, true);
				validationUtil.validateOperationCommand(operation, Command.Start);
				validationUtil.validateOperationDate(operation, dateTimeUtil.getMinimumOperationDate());
				result = pjp.proceed();
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.OperationController.skip(..))")
	public Object operationSkip(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String id = (String)pjp.getArgs()[0];
			validationUtil.validateId(id);
			Operation operation = operationService.get(id, false);
			if (operation == null)
				throw new NotFoundException();
			else {
				validationUtil.validateOperationModel(operation);
				authorizationUtil.canExecuteOperation(operation, true);
				validationUtil.validateOperationCommand(operation, Command.Skip);
				result = pjp.proceed();
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.OperationController.pause(..))")
	public Object operationPause(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String id = (String)pjp.getArgs()[0];
			validationUtil.validateId(id);
			Operation operation = operationService.get(id, false);
			if (operation == null)
				throw new NotFoundException();
			else {
				validationUtil.validateOperationModel(operation);
				authorizationUtil.canExecuteOperation(operation, true);
				validationUtil.validateOperationCommand(operation, Command.Pause);
				result = pjp.proceed();
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.OperationController.resume(..))")
	public Object operationResume(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String id = (String)pjp.getArgs()[0];
			validationUtil.validateId(id);
			Operation operation = operationService.get(id, false);
			if (operation == null)
				throw new NotFoundException();
			else {
				validationUtil.validateOperationModel(operation);
				authorizationUtil.canExecuteOperation(operation, true);
				validationUtil.validateOperationCommand(operation, Command.Resume);
				result = pjp.proceed();
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.OperationController.cancel(..))")
	public Object operationCancel(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String id = (String)pjp.getArgs()[0];
			validationUtil.validateId(id);
			Operation operation = operationService.get(id, false);
			if (operation == null)
				throw new NotFoundException();
			else {
				validationUtil.validateOperationModel(operation);
				authorizationUtil.canExecuteOperation(operation, true);
				validationUtil.validateOperationCommand(operation, Command.Cancel);
				result = pjp.proceed();
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.OperationController.restart(..))")
	public Object operationRestart(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String id = (String)pjp.getArgs()[0];
			validationUtil.validateId(id);
			Operation operation = operationService.get(id, false);
			if (operation == null)
				throw new NotFoundException();
			else {
				validationUtil.validateOperationModel(operation);
				authorizationUtil.canExecuteOperation(operation, true);
				validationUtil.validateOperationCommand(operation, Command.Restart);
				validationUtil.validateOperationDate(operation, dateTimeUtil.getMinimumOperationDate());
				result = pjp.proceed();
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.OperationController.reschedule(..))")
	public Object operationReschedule(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String id = (String)pjp.getArgs()[0];
			Operation operation = operationService.get(id, false);
			if (operation == null)
				throw new NotFoundException();
			else {
				validationUtil.validateOperationModel(operation);
				authorizationUtil.canExecuteOperation(operation, true);
				validationUtil.validateOperationCommand(operation, Command.Reschedule);
				Schedule schedule = (Schedule)pjp.getArgs()[1];
				validationUtil.validateId(id);
				validationUtil.validateSchedule(schedule);
				schedule.setFirstRunDate(dateTimeUtil.adjustFirstRunDate(schedule.getFirstRunDate()));
				result = pjp.proceed();
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.OperationController.unschedule(..))")
	public Object operationUnschedule(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String id = (String)pjp.getArgs()[0];
			validationUtil.validateId(id);
			Operation operation = operationService.get(id, false);
			if (operation == null)
				throw new NotFoundException();
			else {
				validationUtil.validateOperationModel(operation);
				authorizationUtil.canExecuteOperation(operation, true);
				validationUtil.validateOperationCommand(operation, Command.Unschedule);
				result = pjp.proceed();
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.OperationController.live(..))")
	public Object operationLive(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			String operationId = (String)pjp.getArgs()[0];
			validationUtil.validateId(operationId);
			result = pjp.proceed();
			if (result == null)
				throw new NotFoundException();
			else if (authorizationUtil.isNormalUser()) {
				Operation operation = operationService.get(operationId, false);
				authorizationUtil.canViewOperation(operation, true);
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.OperationController.internal(..))")
	public Object internalOperation(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			if (!authorizationUtil.isAdminUser())
				authorizationUtil.throwExecuteException();
			String name = (String)pjp.getArgs()[0];
			validationUtil.validateInternalOperationName(name);
			result = pjp.proceed();
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

}

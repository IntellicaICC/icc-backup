package net.intellica.icc.services.model.sd;

import com.fasterxml.jackson.databind.util.StdConverter;
import net.intellica.icc.services.model.VariableDefinition;

public class VariableDefinitionConverter extends StdConverter<String, VariableDefinition> {

	@Override
	public VariableDefinition convert(String value) {
		VariableDefinition result = new VariableDefinition();
		result.setId(value);
		return result;
	}

}

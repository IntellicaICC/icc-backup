package net.intellica.icc.services.repo;

import java.util.List;
import net.intellica.icc.services.model.Rule;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RuleRepository extends PagingAndSortingRepository<Rule, String> {

	List<Rule> findAllByPropsName(String name);

	List<Rule> findAllByFolderIdOrderByPropsNameAsc(String id);

	List<Rule> findDistinctIdBy();

}

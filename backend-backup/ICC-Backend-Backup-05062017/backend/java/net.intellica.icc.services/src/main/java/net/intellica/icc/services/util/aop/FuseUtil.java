package net.intellica.icc.services.util.aop;

import net.intellica.icc.services.model.Setting;
import net.intellica.icc.services.model.simple.FusedSetting;
import org.springframework.stereotype.Component;

@Component
public class FuseUtil {

	public FusedSetting fuse(Setting setting) {
		FusedSetting result = new FusedSetting();
		result.setId(setting.getId());
		result.setValue(setting.getValue());
		result.setName(setting.getDefinition().getName());
		result.setDescription(setting.getDefinition().getDescription());
		result.setPosition(setting.getDefinition().getPosition());
		result.setType(setting.getDefinition().getType());
		result.setSettingGroup(setting.getDefinition().getSettingGroup());
		return result;
	}

}

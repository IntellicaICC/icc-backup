package net.intellica.icc.services.controller;

import net.intellica.icc.services.event.ErrorEvent;
import net.intellica.icc.services.service.ErrorLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ErrorLogController {

	@Autowired private ErrorLogService errorLogService;

	public Object save(ErrorEvent event) {
		return errorLogService.save(event.getErrorLog(), event.getUser()).getId();
	}

}

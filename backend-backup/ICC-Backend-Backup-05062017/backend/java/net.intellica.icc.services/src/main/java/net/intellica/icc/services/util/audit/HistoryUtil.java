package net.intellica.icc.services.util.audit;

import java.util.List;
import net.intellica.icc.services.model.Folder;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.criteria.internal.IdentifierEqAuditExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
public class HistoryUtil {

	private AuditReader reader;

	@Autowired
	public HistoryUtil(LocalContainerEntityManagerFactoryBean factory) {
		reader = AuditReaderFactory.get(factory.getObject().createEntityManager());
	}

	public <T> T getLastRevision(Class<T> clazz, String id) {
		T result = null;
		List<T> allRevisions = (List<T>)reader
			.createQuery()
			.forRevisionsOfEntity(clazz, true, false)
			.add(new IdentifierEqAuditExpression(id, true))
			.getResultList();
		if (!CollectionUtils.isEmpty(allRevisions))
			result = allRevisions.get(allRevisions.size() - 1);
		return result;
	}

	public void fetchFolders(Folder first) {
		for (Folder f = first; f != null; f = f.getParentFolder()) {
			// fetch folder from db
		}
	}

}

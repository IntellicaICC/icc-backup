package net.intellica.icc.services.util.dev;

import java.sql.Timestamp;
import java.util.Date;

public class TimeUtil {

	public static String getDateTime() {
		return new Timestamp(new Date().getTime()).toString();
	}

}

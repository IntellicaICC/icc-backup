package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import net.intellica.icc.services.util.other.ServiceConstants;
import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "ICC_TEMPLATE")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Template {

	private String id;
	// TODO active, description, name CommonProperties'e tasinacak
	private Boolean active;
	private String description;
	private String name;
	private Boolean hasDataset;
	private Set<VariableDefinition> variables;
	private Set<Job> jobs;

	public Template() {
		active = true;
		description = "";
		name = "";
		hasDataset = false;
		variables = new HashSet<>();
		jobs = new HashSet<>();
	}

	@Id
	@Column(name = "ID", nullable = true, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "ACTIVE", nullable = false)
	public Boolean isActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Column(name = "DESCRIPTION", nullable = false, length = ServiceConstants.Model.LONG_STRING_LENGTH)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "NAME", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "HAS_DATASET", nullable = false)
	public Boolean getHasDataset() {
		return hasDataset;
	}

	public void setHasDataset(Boolean hasDataset) {
		this.hasDataset = hasDataset;
	}

	@OneToMany(mappedBy = "template", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonIgnore
	public Set<VariableDefinition> getVariables() {
		return variables;
	}

	@JsonProperty
	public void setVariables(Set<VariableDefinition> variables) {
		this.variables = variables;
	}

	@OneToMany(mappedBy = "template", fetch = FetchType.LAZY)
	@JsonIgnore
	public Set<Job> getJobs() {
		return jobs;
	}

	public void setJobs(Set<Job> jobs) {
		this.jobs = jobs;
	}

}

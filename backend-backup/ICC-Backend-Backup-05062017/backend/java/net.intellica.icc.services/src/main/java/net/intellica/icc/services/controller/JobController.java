package net.intellica.icc.services.controller;

import java.util.List;
import net.intellica.icc.services.model.Job;
import net.intellica.icc.services.model.Schedule;
import net.intellica.icc.services.model.operation.Command;
import net.intellica.icc.services.model.operation.Operation;
import net.intellica.icc.services.model.operation.OperationType;
import net.intellica.icc.services.service.JobService;
import net.intellica.icc.template.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/job")
public class JobController {

	@Autowired private JobService jobService;
	@Autowired private OperationController operationController;

	@RequestMapping(value = { "/{id}", "/{id}/" }, method = RequestMethod.GET)
	public Object get(@PathVariable String id) {
		return jobService.get(id, false);
	}

	@RequestMapping(value = { "/{id}/variables", "/{id}/variables/" }, method = RequestMethod.GET)
	public Object listVariables(@PathVariable String id) {
		Job job = jobService.get(id, false);
		if (job == null)
			throw new NotFoundException();
		return job.getVariables();
	}

	@RequestMapping(value = { "/save", "/save/" }, method = RequestMethod.POST)
	public Object save(@RequestBody Job job) {
		String result = jobService.save(job).getId();
		operationController.save(job.getId(), OperationType.Job, job.getProps().getName());
		return result;
	}

	@RequestMapping(value = { "/{id}", "/{id}/" }, method = RequestMethod.DELETE)
	public Object delete(@PathVariable String id) {
		jobService.delete(id);
		// TODO delete from operations
		return null;
	}

	@RequestMapping(value = { "/run/{id}", "/run/{id}/" }, method = RequestMethod.GET)
	public Object run(@PathVariable String id) throws Exception {
		Operation operation = operationController.run(jobService.get(id, false));
		operationController.saveCommand(operation, Command.Run);
		return null;
	}

	@RequestMapping(value = { "/schedule/{id}", "/schedule/{id}/" }, method = RequestMethod.POST)
	public Object schedule(@PathVariable String id, @RequestBody Schedule schedule) throws Exception {
		List<Operation> list = operationController.schedule(jobService.get(id, false), schedule);
		operationController.saveCommand(list.get(0), Command.Schedule);
		return null;
	}

	@RequestMapping(value = { "/explore", "/explore/" }, method = RequestMethod.GET)
	public Object explore(@RequestParam(value = "id", required = false) String id) {
		return jobService.listByFolderId(id);
	}

}

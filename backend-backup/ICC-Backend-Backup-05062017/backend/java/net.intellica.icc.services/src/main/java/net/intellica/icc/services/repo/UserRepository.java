package net.intellica.icc.services.repo;

import java.util.List;
import net.intellica.icc.services.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, String> {

	List<User> findAllByPropsName(String name);

	User findFirstByPropsName(String name);

	List<User> findAllByOrderByPropsNameAsc();

}

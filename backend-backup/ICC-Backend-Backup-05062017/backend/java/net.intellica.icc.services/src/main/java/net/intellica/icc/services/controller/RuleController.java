package net.intellica.icc.services.controller;

import java.util.List;
import net.intellica.icc.services.model.Rule;
import net.intellica.icc.services.model.Schedule;
import net.intellica.icc.services.model.operation.Command;
import net.intellica.icc.services.model.operation.Operation;
import net.intellica.icc.services.model.operation.OperationType;
import net.intellica.icc.services.service.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rule")
public class RuleController {

	@Autowired private RuleService ruleService;
	@Autowired private OperationController operationController;

	@RequestMapping(value = { "/{id}", "/{id}/" }, method = RequestMethod.GET)
	public Object get(@PathVariable String id) {
		return ruleService.get(id, false);
	}

	@RequestMapping(value = { "/save", "/save/" }, method = RequestMethod.POST)
	public Object save(@RequestBody Rule rule) {
		String result = ruleService.save(rule).getId();
		operationController.save(rule.getId(), OperationType.Rule, rule.getProps().getName());
		return result;
	}

	@RequestMapping(value = { "/{id}", "/{id}/" }, method = RequestMethod.DELETE)
	public Object delete(@PathVariable String id) {
		ruleService.delete(id);
		// TODO delete from operations
		return null;
	}

	@RequestMapping(value = { "/run/{id}", "/run/{id}/" }, method = RequestMethod.GET)
	public Object run(@PathVariable String id) throws Exception {
		Operation operation = operationController.run(ruleService.get(id, false));
		operationController.saveCommand(operation, Command.Run);
		return null;
	}

	@RequestMapping(value = { "/schedule/{id}", "/schedule/{id}/" }, method = RequestMethod.POST)
	public Object schedule(@PathVariable String id, @RequestBody Schedule schedule) throws Exception {
		List<Operation> list = operationController.schedule(ruleService.get(id, false), schedule);
		operationController.saveCommand(list.get(0), Command.Schedule);
		return null;
	}

	@RequestMapping(value = { "/explore", "/explore/" }, method = RequestMethod.GET)
	public Object explore(@RequestParam(value = "id", required = false) String id) {
		return ruleService.listByFolderId(id);
	}

}

package net.intellica.icc.services.controller;

import java.util.List;
import net.intellica.icc.services.model.Role;
import net.intellica.icc.services.service.RoleService;
import net.intellica.icc.template.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/role")
public class RoleController {

	@Autowired private RoleService roleService;

	@RequestMapping(value = { "/{id}", "/{id}/" }, method = RequestMethod.GET)
	public Object get(@PathVariable String id) {
		return roleService.get(id, false);
	}

	@RequestMapping(value = { "/{id}/users", "/{id}/users/" }, method = RequestMethod.GET)
	public Object getUsers(@PathVariable String id) {
		Role role = roleService.get(id, false);
		if (role == null)
			throw new NotFoundException();
		return role.getUsers();
	}

	@RequestMapping(value = { "/save", "/save/" }, method = RequestMethod.POST)
	public Object save(@RequestBody Role role) {
		return roleService.save(role).getId();
	}

	@RequestMapping(value = { "/{id}", "/{id}/" }, method = RequestMethod.DELETE)
	public Object delete(@PathVariable String id) {
		roleService.delete(id);
		return null;
	}

	@RequestMapping(value = { "/list", "/list/" }, method = RequestMethod.GET)
	public Object list(@RequestParam(value = "name", required = false) String name) {
		List<Role> result;
		if (StringUtils.isEmpty(name))
			result = roleService.listAll();
		else
			result = roleService.listByName(name);
		return result;
	}

}

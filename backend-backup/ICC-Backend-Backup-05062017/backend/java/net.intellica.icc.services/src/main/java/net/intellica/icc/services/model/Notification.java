package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.intellica.icc.services.event.EventType;
import net.intellica.icc.services.util.other.ServiceConstants;

@Entity
@Table(name = "ICC_NOTIFICATION")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Notification {

	private String id;
	private NotificationType type;
	private NotificationSource source;
	private String sourceId;
	private String targetUser;
	private Date notificationDate;
	private Boolean read;
	private Date readDate;
	private String subject;
	private String text;
	private EventType eventType;

	public Notification() {
		read = false;
	}

	public Notification(
		NotificationType notificationType,
		NotificationSource source,
		String sourceId,
		String targetUserId,
		String subject,
		String text,
		EventType eventType//
	) {
		this();
		this.type = notificationType;
		this.source = source;
		this.sourceId = sourceId;
		this.targetUser = targetUserId;
		this.subject = subject;
		this.text = text;
		this.eventType = eventType;
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "TYPE", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public NotificationType getType() {
		return type;
	}

	public void setType(NotificationType type) {
		this.type = type;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "SOURCE", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public NotificationSource getSource() {
		return source;
	}

	public void setSource(NotificationSource source) {
		this.source = source;
	}

	@Column(name = "SOURCE_ID", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	@Column(name = "TARGET_USER_ID", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getTargetUser() {
		return targetUser;
	}

	public void setTargetUser(String targetUser) {
		this.targetUser = targetUser;
	}

	@Column(name = "NOTIFICATION_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getNotificationDate() {
		return notificationDate;
	}

	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}

	@Column(name = "READ", nullable = false)
	public Boolean isRead() {
		return read;
	}

	public void setRead(Boolean read) {
		this.read = read;
	}

	@Column(name = "READ_DATE", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getReadDate() {
		return readDate;
	}

	public void setReadDate(Date readDate) {
		this.readDate = readDate;
	}

	@Column(name = "SUBJECT", nullable = false, length = ServiceConstants.Model.LONG_STRING_LENGTH)
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Column(name = "TEXT", nullable = false, length = ServiceConstants.Model.LONG_STRING_LENGTH)
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "EVENT_TYPE", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

}

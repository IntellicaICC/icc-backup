package net.intellica.icc.services.repo;

import java.util.List;
import net.intellica.icc.services.model.GlobalParameter;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GlobalParameterRepository extends PagingAndSortingRepository<GlobalParameter, String> {

	List<GlobalParameter> findAllByPropsName(String name);

	List<GlobalParameter> findAllByOrderByPropsNameAsc();

}

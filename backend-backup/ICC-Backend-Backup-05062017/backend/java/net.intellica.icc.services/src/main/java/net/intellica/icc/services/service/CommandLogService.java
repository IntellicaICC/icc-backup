package net.intellica.icc.services.service;

import net.intellica.icc.services.model.operation.Command;
import net.intellica.icc.services.model.operation.CommandLog;
import net.intellica.icc.services.repo.CommandLogRepository;
import net.intellica.icc.services.util.aop.PreSaveUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommandLogService {

	@Autowired private CommandLogRepository repository;
	@Autowired private PreSaveUtil preSaveUtil;

	public CommandLogService() {
	}

	public CommandLog get(String id) {
		return repository.findOne(id);
	}

	public CommandLog getLast(String operationId, Command command) {
		return repository.findFirstByOperationIdAndCommandOrderByCommandDateDesc(operationId, command);
	}

	public CommandLog save(CommandLog commandLog) {
		preSaveUtil.fill(commandLog);
		return repository.save(commandLog);
	}

}

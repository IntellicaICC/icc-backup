package net.intellica.icc.services.util.aop;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import net.intellica.icc.services.config.ServicesConfig;
import net.intellica.icc.services.license.LicenseValidator;
import net.intellica.icc.services.model.CommonProperties;
import net.intellica.icc.services.model.Connection;
import net.intellica.icc.services.model.Folder;
import net.intellica.icc.services.model.GlobalParameter;
import net.intellica.icc.services.model.Job;
import net.intellica.icc.services.model.License;
import net.intellica.icc.services.model.LocalParameter;
import net.intellica.icc.services.model.ProfileUpdate;
import net.intellica.icc.services.model.RepeatInterval;
import net.intellica.icc.services.model.Rights;
import net.intellica.icc.services.model.Role;
import net.intellica.icc.services.model.Rule;
import net.intellica.icc.services.model.SampleQuery;
import net.intellica.icc.services.model.Schedule;
import net.intellica.icc.services.model.Setting;
import net.intellica.icc.services.model.SettingDefinition;
import net.intellica.icc.services.model.Template;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.model.Variable;
import net.intellica.icc.services.model.VariableDefinition;
import net.intellica.icc.services.model.VariableType;
import net.intellica.icc.services.model.operation.Command;
import net.intellica.icc.services.model.operation.InternalOperation;
import net.intellica.icc.services.model.operation.Operation;
import net.intellica.icc.services.model.operation.OperationType;
import net.intellica.icc.services.model.report.AuthorizationRoleObjectFilter;
import net.intellica.icc.services.model.report.AuthorizationRoleUserFilter;
import net.intellica.icc.services.model.report.AuthorizationUserObjectFilter;
import net.intellica.icc.services.model.report.AuthorizationUserRoleFilter;
import net.intellica.icc.services.model.rule.DecisionNode;
import net.intellica.icc.services.model.rule.Graph;
import net.intellica.icc.services.model.rule.JobNode;
import net.intellica.icc.services.model.rule.Node;
import net.intellica.icc.services.model.rule.NodeType;
import net.intellica.icc.services.model.rule.RuleNode;
import net.intellica.icc.services.quartz.QuartzManager;
import net.intellica.icc.services.security.AuthorizationUtil;
import net.intellica.icc.services.security.BcryptUtil;
import net.intellica.icc.services.service.ConnectionService;
import net.intellica.icc.services.service.FolderService;
import net.intellica.icc.services.service.GlobalParameterService;
import net.intellica.icc.services.service.JobService;
import net.intellica.icc.services.service.LocalParameterService;
import net.intellica.icc.services.service.OperationService;
import net.intellica.icc.services.service.RoleService;
import net.intellica.icc.services.service.RuleService;
import net.intellica.icc.services.service.SettingDefinitionService;
import net.intellica.icc.services.service.SettingService;
import net.intellica.icc.services.service.TemplateService;
import net.intellica.icc.services.service.UserService;
import net.intellica.icc.services.util.db.SqlExecutor;
import net.intellica.icc.services.util.dev.DateTimeUtil;
import net.intellica.icc.services.util.dev.QuickCache;
import net.intellica.icc.services.util.other.ServiceConstants;
import net.intellica.icc.template.exception.Messages;
import net.intellica.icc.template.exception.ParseException;
import net.intellica.icc.template.exception.ValidationException;
import net.intellica.icc.template.model.GlobalVariable;
import net.intellica.icc.template.model.TemplateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class ValidationUtil {

	private static final Integer MIN_PASSWORD_LENGTH = 8;
	private static final Integer MAX_PASSWORD_LENGTH = 32;
	private static final String SELECT = "SELECT";

	private static final String PASSWORD_REGEX = "[\\w]*";
	private static final String NAME_REGEX = "[\\w -]+";
	private static final String PARAMETER_NAME_REGEX = "[a-zA-Z][\\w]*";
	private static final String PERSON_NAME_REGEX = "[a-zA-Z\\. ]+";
	private static final String PARAMETRIC_REGEX = Pattern.quote(ServiceConstants.Expression.EXPRESSION_PREFIX) + "(\\w+?)" + Pattern.quote(ServiceConstants.Expression.EXPRESSION_SUFFIX);
	private static final String FROM_REGEX = "\\bFROM\\b";
	private static final String EMAIL_REGEX = ".+@.+";// baska turlu valide etmek zor

	private static final String USER_OWNER_DELETE = "This user is the owner of %s, hence can not be deleted!";
	private static final String RULE_IN_RULE_DELETE = "%s used in this rule is deleted!";
	private static final String NO_SUCH_RULE = "A rule used in this rule does not exist!";
	private static final String RULE_NODE_WITHOUT_ID = "A rule node doesn't have a rule id!";
	private static final String JOB_IN_RULE_DELETE = "%s used in this rule is deleted!";
	private static final String NO_SUCH_JOB = "A job used in this rule does not exist!";
	private static final String JOB_NODE_WITHOUT_ID = "A job node doesn't have a job id!";
	private static final String FILTER_INVALID_RIGHTS = "The rights given in filter are invalid!";
	private static final String FILTER_NO_USER = "The user given in filter does not exist!";
	private static final String FILTER_NO_ROLE = "The role given in filter does not exist!";
	private static final String TEMPLATE_DELETED = "This template has been deleted!";
	private static final String USER_DELETED = "This user has been deleted!";
	private static final String ROLE_DELETED = "This role has been deleted!";
	private static final String CONNECTION_DELETED = "This connection has been deleted!";
	private static final String PARAMETER_DELETED = "This parameter has been deleted!";
	private static final String FOLDER_DELETED = "This folder has been deleted!";
	private static final String RULE_DELETED = "This rule has been deleted!";
	private static final String JOB_DELETED = "This job has been deleted!";
	private static final String NO_REFERENCE = "The reference '%s' does not exist in any context!";
	private static final String ROLE_TO_START_DELETE = "This role grants rights to %s that is about to start at any moment, hence can not be deleted!";
	private static final String ROLE_EXECUTING_DELETE = "This role grants rights to %s that is currently executing, hence can not be deleted!";
	private static final String PARAMETER_RENAME = "This parameter is used in %s, hence can not be renamed!";
	private static final String PARAMETER_DELETE = "This parameter is used in %s, hence can not be deleted!";
	private static final String PARSE_GRAPH = "Unable to parse graph of rule '%s'";
	private static final String NAME_CHARACTERS = "Name can be at most %d characters!";
	private static final String INTERNAL_OPERATION_NAME = "'%s' is not a valid operation name!";
	private static final String TEMPLATE_DELETE = "This template is used by %s, hence can not be deleted!";
	private static final String USER_RULES_DELETE = "This user has %d rules scheduled for execution, hence can not be deleted!";
	private static final String USER_JOBS_DELETE = "This user has %d jobs scheduled for execution, hence can not be deleted!";
	private static final String USER_JOBS_RULES_DELETE = "This user has %d jobs and %d rules scheduled for execution, hence can not be deleted!";
	private static final String USER_DELETE = "This user is used by rights of %s, hence can not be deleted!";
	private static final String ROLE_DELETE = "This role is used by rights of %s, hence can not be deleted!";
	private static final String CONNECTION_DELETE = "This connection is used in %s, hence can not be deleted!";
	private static final String RULE_INSIDE_RULE_DELETE = "This rule is used inside %s, hence can not be deleted!";
	private static final String JOB_INSIDE_RULE_DELETE = "This job is used inside %s, hence can not be deleted!";
	private static final String FOLDER_INSIDE_JOB_DELETE = "%s in this folder is used inside %s, hence can not be deleted!";
	private static final String FOLDER_TO_START_WITHIN_RULE_DELETE = "%s in this folder is about to start at any moment within a rule, hence can not be deleted!";
	private static final String FOLDER_WITHIN_RULE_DELETE = "%s in this folder is used within an executing rule, hence can not be deleted!";
	private static final String FOLDER_TO_START_WITHIN_JOB_DELETE = "%s in this folder is about to start at any moment within a job, hence can not be deleted!";
	private static final String FOLDER_WITHIN_JOB_DELETE = "%s in this folder is used within an executing job, hence can not be deleted!";
	private static final String FOLDER_TO_START_DELETE = "%s in this folder is about to start at any moment, hence can not be deleted!";
	private static final String FOLDER_EXECUTING_DELETE = "%s in this folder is currently executing, hence can not be deleted!";
	private static final String NAME_PERSON = "Name can only consist of alphabetic characters, space and period!";
	private static final String NAME_FORBIDDEN = "The use of this name is forbidden!";
	private static final String NAME_PARAMETER = "Name can only consist of alphanumeric characters, with no whitespace!";
	private static final String NAME_EXPLORER = "Name can only consist of alphanumeric characters, hyphen and underscore!";
	private static final String NAME_EMPTY = "Name can not be left empty!";
	private static final String PROPERTIES_NULL = "Object properties can not be null!";
	private static final String SAVE_NULL = "Can not save null object!";
	private static final String OPERATION_NAME = "Operation name is required!";
	private static final String SETTING_DELETE = "This setting definition is used, hence can not be deleted!";
	private static final String RULE_TO_START_DELETE = "This rule is about to start at any moment, hence can not be deleted!";
	private static final String RULE_EXECUTING_DELETE = "This rule is currently executing, hence can not be deleted!";
	private static final String JOB_TO_START_DELETE = "This job is about to start at any moment, hence can not be deleted!";
	private static final String JOB_EXECUTING_DELETE = "This job is currently executing, hence can not be deleted!";
	private static final String DUPLICATE_SETTING = "There is already another setting definition with the same name!";
	private static final String DUPLICATE_TEMPLATE = "There is already another template with the same name!";
	private static final String USER_HAS_OPERATIONS = "This user has queued operations that won't work if converted into a normal user!";
	private static final String OWN_USER = "Alteration of one's own user is prohibited!";
	private static final String INVALID_USER_TYPE = "Invalid user type!";
	private static final String INVALID_LOGIN_TYPE = "Invalid login type!";
	private static final String DUPLICATE_EMAIL = "There is already another user with the same email address!";
	private static final String INVALID_EMAIL = "Invalid email address!";
	private static final String DUPLICATE_USER = "There is already another user with the same username!";
	private static final String DUPLICATE_ROLE = "There is already another role with the same name!";
	private static final String CONNECTION_ABOUT_TO_START_INSIDE = "This connection is used in %s that is about to start at any moment, hence can not be changed!";
	private static final String CONNECTION_EXECUTING_INSIDE = "This connection is used in %s that is currently executing, hence can not be changed!";
	private static final String CONNECTION_USER_EMPTY = "Connection user can not be left empty!";
	private static final String CONNECTION_PASS_EMPTY = "Connection password can not be left empty!";
	private static final String CONNECTION_URL_EMPTY = "Connection URL can not be left empty!";
	private static final String DUPLICATE_CONNECTION = "There is already another connection with the same name!";
	private static final String DUPLICATE_GLOBAL_PARAMETER = "There is already another global parameter with the same name!";
	private static final String DUPLICATE_LOCAL_PARAMETER = "There is already another local parameter with the same name in this folder!";
	private static final String TEMPLATE_RESULT_WITH_SAME_NAME = "There is a template result with the same name!";
	private static final String GLOBAL_PARAMETER_WITH_SAME_NAME = "There is a global parameter with the same name!";
	private static final String SYSTEM_VARIABLE_WITH_SAME_NAME = "There is a system variable with the same name!";
	private static final String PARAMETER_VALUE = "Parameter value can not be left empty!";
	private static final String NODE_MISSES_OUTGOING = "A %s node misses an outgoing link!";
	private static final String NODE_MISSES_INCOMING = "A %s node misses an incoming link!";
	private static final String INVALID_GRAPH = "Invalid rule graph!";
	private static final String RULE_LOOP = "Rules can not contain loops!";
	private static final String START_NO_OUTGOING = "A start node misses an outgoing link!";
	private static final String START_INCOMING = "A start node can not have an incoming link!";
	private static final String END_OUTGOING = "An end node can not have an outgoing link!";
	private static final String END_NO_INCOMING = "An end node misses an incoming link!";
	private static final String DECISION_TO_THE_SAME_NODE = "A decision node can not have a 'true' and 'false' outgoing link to the same node!";
	private static final String DECISION_REGULAR_LINK = "A decision node can not have a regular link!";
	private static final String DECISION_NOFALSE_OUTGOING = "A decision node misses a 'false' outgoing link!";
	private static final String DECISION_NO_TRUE_OUTGOING = "A decision node misses a 'true' outgoing link!";
	private static final String DECISION_NO_INCOMING_LINK = "A decision node misses an incoming link!";
	private static final String COMMENT_NODE = "Comment node can not be linked to other nodes!";
	private static final String RULE_INSIDE_ITSELF = "Nesting a rule inside itself is prohibited!";
	private static final String RULE_END_NODE = "Rule flows must terminate with an end node!";
	private static final String RULE_START_NODE = "The rule must start with a start node!";
	private static final String RULE_TO_START_INSIDE_CHANGE = "This rule is about to start at any moment inside %s, hence can not be changed!";
	private static final String RULE_EXECUTING_INSIDE_CHANGE = "This rule is currently executing inside %s, hence can not be changed!";
	private static final String RULE_TO_START_CHANGE = "This rule is about to start at any moment, hence can not be changed!";
	private static final String RULE_EXECUTING_CHANGE = "This rule is currently executing, hence can not be changed!";
	private static final String RULE_ALREADY_DELETED = "This rule was deleted before save could be completed!";
	private static final String DUPLICATE_RULE = "There is already another rule with the same name in this folder!";
	private static final String VARIABLE_INVALID_SQL = "The sql statement given in variable '%s' is not a valid select statement!";
	private static final String VARIABLE_DATASET_NOT_FOUND = "Can not find dataset of variable '%s'!";
	private static final String VARIABLE_OWN_JOB = "Can not select own job for dataset variable '%s'!";
	private static final String VARIABLE_NO_DATASET = "A dataset needs to be selected for variable '%s'!";
	private static final String VARIABLE_CONNECTION = "Can not find connection of variable '%s'!";
	private static final String VARIABLE_PARSE_BOOLEAN = "Can not parse variable '%s' as a boolean value!";
	private static final String VARIABLE_REQUIRED = "The variable '%s' is required!";
	private static final String JOB_MISSES_VALUE = "Job misses a variable value for '%s'!";
	private static final String JOB_VARIABLES_MISMATCH = "Job variables and template variables mismatch!";
	private static final String JOB_WITHOUT_VARIABLES = "Job can not be saved without variables!";
	private static final String JOB_WITHOUT_TEMPLATE = "Job can not be saved without a template!";
	private static final String TEMPLATE_CHANGED = "Job template can not be changed once it's been created!";
	private static final String JOB_TO_START_CHANGE = "This job is about to start at any moment, hence can not be changed!";
	private static final String JOB_TO_START_INSIDE_CHANGE = "This job is about to start at any moment inside %s, hence can not be changed!";
	private static final String JOB_EXECUTING_CHANGE = "This job is currently executing, hence can not be changed!";
	private static final String JOB_EXECUTING_INSIDE_CHANGE = "This job is currently executing inside %s, hence can not be changed!";
	private static final String JOB_ALREADY_DELETED = "This job was deleted before save could be completed!";
	private static final String DUPLICATE_JOB = "There is already another job with the same name in this folder!";
	private static final String DUPLICATE_FOLDER = "There is already another folder with the same name in this folder!";
	private static final String UNLICENSED = "A valid license is required to use ICC services.";
	private static final String INVALID_ID = "Invalid id! (%s)";
	private static final String INVALID_DATES = "Invalid dates! (%s)";
	private static final String INVALID_COMMAND = "Invalid command! (%s)";
	private static final String INVALID_LICENSE = "Invalid license!";
	private static final String LICENSE_ALREADY_EXISTS = "A valid ICC license already exists!";
	private static final String SCHEDULE_TOO_OFTEN = "Minimum repeat period is 10 minutes!";
	private static final String OPERATION_COLLISION = "An operation is already scheduled to run at this date and time!";
	private static final String INVALID_CONNECTION = "Connection can not be established!";
	private static final String INVALID_OLD_PASSWORD = "Invalid old password!";
	private static final String PASSWORD_TOO_SHORT = "Password must be at least " + MIN_PASSWORD_LENGTH + " characters!";
	private static final String PASSWORD_TOO_LONG = "Password must be at most " + MAX_PASSWORD_LENGTH + " characters!";
	private static final String INVALID_NEW_PASSWORD = "Password may only contain letters, numbers and '_' character!";
	private static final String INVALID_SQL_SELECT = "Only select statements are allowed!";
	private static final String QUERY_INACTIVE_CONNECTION = "Connection is inactive!";
	private static final String QUERY_NO_CONNECTION = "Connection with the given id could not be found!";
	private static final String QUERY_EMPTY_SQL = "A query is required!";
	private static final String QUERY_EMPTY_CONNECTION = "A connection id is required!";
	private static final String INACTIVE_JOB_RUN = "Only active jobs can be run!";
	private static final String INACTIVE_RULE_RUN = "Only active rules can be run!";
	private static final String INACTIVE_JOB_SCHEDULE = "Only active jobs can be scheduled!";
	private static final String INACTIVE_RULE_SCHEDULE = "Only active rules can be scheduled!";
	private static final String SCHEDULE_DATE_INTERVAL = "Schedule date must be between %1$tB %1$td, %1$tY %1$tI:%1$tM %1$Tp and %2$tB %2$td, %2$tY %2$tI:%2$tM %2$Tp!";

	private static final Integer CODE_UNLICENSED = 1;

	private static final Predicate<String> IS_NOT_SELECT = sql -> sql == null ||
		sql.length() < SELECT.length() ||
		!sql.toUpperCase().startsWith(SELECT) ||
		!Pattern.compile(FROM_REGEX, Pattern.CASE_INSENSITIVE).matcher(sql).find();

	@Autowired private ApplicationContext context;
	@Autowired private LicenseValidator validator;
	@Autowired private DateTimeUtil dateTimeUtil;
	@Autowired private SqlExecutor sqlExecutor;
	@Autowired private OperationService operationService;
	@Autowired private ConnectionService connectionService;
	@Autowired private BcryptUtil bcryptUtil;
	@Autowired private FolderService folderService;
	@Autowired private JobService jobService;
	@Autowired private RuleService ruleService;
	@Autowired private LocalParameterService localParameterService;
	@Autowired private GlobalParameterService globalParameterService;
	@Autowired private RoleService roleService;
	@Autowired private UserService userService;
	@Autowired private TemplateService templateService;
	@Autowired private SettingDefinitionService settingDefinitionService;
	@Autowired private SettingService settingService;
	@Autowired private ServicesConfig servicesConfig;
	@Autowired private AuthorizationUtil authorizationUtil;
	@Autowired private QuartzManager quartz;

	public void validateId(String id) {
		try {
			Long longId = Long.valueOf(id);
			if (longId <= 0)
				throw new ValidationException(null, String.format(INVALID_ID, id));
		}
		catch (Exception e) {
			throw new ValidationException(null, String.format(INVALID_ID, id));
		}
	}

	public void validateSchedule(Schedule schedule) {
		if (schedule.getInterval() == RepeatInterval.MINUTES && schedule.getPeriod() < 10)
			throw new ValidationException(null, SCHEDULE_TOO_OFTEN);
		Date minDate = dateTimeUtil.getMinimumOperationDate();
		Date maxDate = dateTimeUtil.getMaximumOperationDate();
		if (schedule.getFirstRunDate().before(minDate) || schedule.getFirstRunDate().after(maxDate))
			throw new ValidationException(null, String.format(Locale.US, SCHEDULE_DATE_INTERVAL, minDate, maxDate));
	}

	public void validateSave(Folder folder) {
		validateNonNull(folder);
		validateProperties(folder.getProps());
		validateNameCommon(folder.getProps().getName());
		validateNameExplorer(folder.getProps().getName());
		// bulundugu klasorde ayni isimde (case-sensitive) baska folder varmi?
		List<Folder> list = folderService.listByParentFolderId(folder.getParentFolder() == null ? null : folder.getParentFolder().getId());
		Predicate<Folder> predicate = f -> f.getProps().getName().equals(folder.getProps().getName()) && !f.getId().equals(folder.getId());
		if (list.stream().anyMatch(predicate))
			throw new ValidationException(null, DUPLICATE_FOLDER);
		// silinmis folder (baskasi tarafindan) tekrar kaydedilemez
		if (!isStringEmpty(folder.getId()) &&
			folderService.get(folder.getId(), false) == null &&
			folderService.get(folder.getId(), true) != null//
		)
			throw new ValidationException(null, FOLDER_DELETED);
	}

	public void validateSave(Job job) {
		validateNonNull(job);
		validateProperties(job.getProps());
		validateNameCommon(job.getProps().getName());
		validateNameExplorer(job.getProps().getName());
		// bulundugu klasorde ayni isimde (case-sensitive) baska job varmi ?
		List<Job> folderJobs = jobService.listByFolderId(job.getFolder() == null ? null : job.getFolder().getId());
		Predicate<Job> jobNameExists = j -> j.getProps().getName().equals(job.getProps().getName()) && !j.getId().equals(job.getId());
		if (folderJobs.stream().anyMatch(jobNameExists))
			throw new ValidationException(null, DUPLICATE_JOB);
		Job dbJob = null;
		// silinmis job (baskasi tarafindan) tekrar kaydedilemez
		if (!isStringEmpty(job.getId()) &&
			(dbJob = jobService.get(job.getId(), false)) == null &&
			jobService.get(job.getId(), true) != null//
		)
			throw new ValidationException(null, JOB_DELETED);
		Boolean variablesChanged = false;
		if (!isStringEmpty(job.getId())) {
			List<Operation> operationJobs = operationService.list(job.getId(), OperationType.Job, false, null);
			// zaten calisiyorsa degistirilemez
			Predicate<Operation> jobExecuting = o -> ServiceConstants.Operation.COLLISION_STATES.contains(o.getStatus());
			if (operationJobs.stream().anyMatch(jobExecuting))
				throw new ValidationException(null, JOB_EXECUTING_CHANGE);
			// bir sonraki job ise degistirilemez
			Predicate<Operation> isNextJob = o -> dateTimeUtil.getMinimumOperationDate().equals(o.getNextRunDate());
			if (operationJobs.stream().anyMatch(isNextJob))
				throw new ValidationException(null, JOB_TO_START_CHANGE);
			List<Operation> operationRules = operationService.list(OperationType.Rule, false, null, dateTimeUtil.getMinPlus(1));
			// zaten calisan bir rule icindeyse degistirilemez
			Predicate<Operation> ruleExecuting = o -> ServiceConstants.Operation.COLLISION_STATES.contains(o.getStatus()) &&
				isJobInRule(job.getId(), o.getModelId());
			Optional<Operation> executingRule = operationRules.stream().filter(ruleExecuting).findFirst();
			if (executingRule.isPresent())
				throw new ValidationException(null, String.format(JOB_EXECUTING_INSIDE_CHANGE, chooseIdentifier(executingRule.get(), false, false)));
			// bir sonraki rule ise degistirilemez
			Predicate<Operation> isNextRule = o -> dateTimeUtil.getMinimumOperationDate().equals(o.getNextRunDate()) &&
				isJobInRule(job.getId(), o.getModelId());
			Optional<Operation> nextRule = operationRules.stream().filter(isNextRule).findFirst();
			if (nextRule.isPresent())
				throw new ValidationException(null, String.format(JOB_TO_START_INSIDE_CHANGE, chooseIdentifier(nextRule.get(), false, false)));
			// template degistirilemez
			if (!job.getTemplate().getId().equals(dbJob.getTemplate().getId()))
				throw new ValidationException(null, TEMPLATE_CHANGED);
			if (isCollectionEmpty(job.getVariables()))
				variablesChanged = false;
		}
		else {
			// templatesiz kaydedilemez
			if (job.getTemplate() == null)
				throw new ValidationException(null, JOB_WITHOUT_TEMPLATE);
			variablesChanged = true;
		}
		// variable kontrolleri ve definition ile uyum
		if (variablesChanged) {
			Template template = templateService.get(job.getTemplate().getId(), true, false);
			// variable'lar bos olamaz
			if (isCollectionEmpty(job.getVariables()))
				throw new ValidationException(null, JOB_WITHOUT_VARIABLES);
			// template'de tanimlananlar ile farkli sayida olamaz
			if (job.getVariables().size() != template.getVariables().size())
				throw new ValidationException(null, JOB_VARIABLES_MISMATCH);
			// template degiskenlerini sirala
			List<VariableDefinition> variableList = new ArrayList<>(template.getVariables());
			variableList.sort((v1, v2) -> v1.getPosition().compareTo(v2.getPosition()));
			// template'de tanimlananlar disinda bir variable olamaz
			for (VariableDefinition definition : variableList) {
				if (job.getVariables().stream().noneMatch(v -> v.getDefinition() != null && definition.getId().equals(v.getDefinition().getId())))
					throw new ValidationException(null, String.format(JOB_MISSES_VALUE, definition.getProps().getName()));
			}
			// variable tipine gore gerekli kontroller
			Long datasetCount = template.getVariables().stream().filter(v -> v.getType() == VariableType.Dataset).count();
			for (VariableDefinition definition : variableList) {
				Variable variable = job.getVariables().stream().filter(v -> definition.getId().equals(v.getDefinition().getId())).findFirst().get();
				if (definition.getRequired() && isStringEmpty(variable.getValue()))
					throw new ValidationException(null, String.format(VARIABLE_REQUIRED, definition.getProps().getName()));
				if (!isStringEmpty(variable.getValue())) {
					switch (definition.getType()) {
						case Boolean:
							if (!Boolean.TRUE.toString().equalsIgnoreCase(variable.getValue()) &&
								!Boolean.FALSE.toString().equalsIgnoreCase(variable.getValue())//
							)
								throw new ValidationException(null, String.format(VARIABLE_PARSE_BOOLEAN, definition.getProps().getName()));
							break;
						case Connection:
							if (connectionService.listAll().stream().noneMatch(c -> c.getId().equals(variable.getValue())))
								throw new ValidationException(null, String.format(VARIABLE_CONNECTION, definition.getProps().getName()));
							break;
						case Dataset:
							if (isStringEmpty(variable.getValue())) {
								// birden fazla dataset secilmeliyse bos gecilemez
								if (datasetCount > 1)
									throw new ValidationException(null, String.format(VARIABLE_NO_DATASET, definition.getProps().getName()));
							}
							else {
								// jobun kendisi dataset olarak secilemez
								if (!isStringEmpty(job.getId()) && job.getId().equals(variable.getValue()))
									throw new ValidationException(null, String.format(VARIABLE_OWN_JOB, definition.getProps().getName()));
								// boyle bir dataset var mi?
								if (jobService.listAll().stream().noneMatch(j -> j.getId().equals(variable.getValue())))
									throw new ValidationException(null, String.format(VARIABLE_DATASET_NOT_FOUND, definition.getProps().getName()));
							}
							break;
						case List:
							// no validation
							break;
						case SQL:
							if (IS_NOT_SELECT.test(variable.getValue()))
								throw new ValidationException(null, String.format(VARIABLE_INVALID_SQL, definition.getProps().getName()));
							validateParametricUsage(variable.getValue(), job.getFolder());
							break;
						case Standard:
							validateParametricUsage(variable.getValue(), job.getFolder());
							break;
						default:
							break;
					}
				}
			}
		}
	}

	public void validateSave(Rule rule) {
		validateNonNull(rule);
		validateProperties(rule.getProps());
		validateNameCommon(rule.getProps().getName());
		validateNameExplorer(rule.getProps().getName());
		// bulundugu klasorde ayni isimde (case-sensitive) baska rule varmi ?
		List<Rule> list = ruleService.listByFolderId(rule.getFolder() == null ? null : rule.getFolder().getId());
		Predicate<Rule> predicate = r -> r.getProps().getName().equals(rule.getProps().getName()) && !r.getId().equals(rule.getId());
		if (list.stream().anyMatch(predicate))
			throw new ValidationException(null, DUPLICATE_RULE);
		// silinmis rule (baskasi tarafindan) tekrar kaydedilemez
		if (!isStringEmpty(rule.getId()) &&
			ruleService.get(rule.getId(), false) == null &&
			ruleService.get(rule.getId(), true) != null//
		)
			throw new ValidationException(null, RULE_DELETED);
		if (!isStringEmpty(rule.getId())) {
			List<Operation> operationRules = operationService.list(rule.getId(), OperationType.Rule, false, null);
			// zaten calisiyorsa degistirilemez
			Predicate<Operation> ruleExecuting = o -> ServiceConstants.Operation.COLLISION_STATES.contains(o.getStatus());
			if (operationRules.stream().anyMatch(ruleExecuting))
				throw new ValidationException(null, RULE_EXECUTING_CHANGE);
			// bir sonraki rule ise degistirilemez
			Predicate<Operation> isNextRule = o -> dateTimeUtil.getMinimumOperationDate().equals(o.getNextRunDate());
			if (operationRules.stream().anyMatch(isNextRule))
				throw new ValidationException(null, RULE_TO_START_CHANGE);
			List<Operation> allOperationRules = operationService.list(OperationType.Rule, false, null, dateTimeUtil.getMinPlus(1));
			// zaten calisan bir rule icindeyse degistirilemez
			Predicate<Operation> ruleExecutingInsideAnother = o -> ServiceConstants.Operation.COLLISION_STATES.contains(o.getStatus()) &&
				isRuleInRule(rule.getId(), o.getModelId());
			Optional<Operation> ruleExecutingInside = allOperationRules.stream().filter(ruleExecutingInsideAnother).findFirst();
			if (ruleExecutingInside.isPresent())
				throw new ValidationException(null, String.format(RULE_EXECUTING_INSIDE_CHANGE, chooseIdentifier(ruleExecutingInside.get(), true, false)));
			// bir sonraki rule icinde ise degistirilemez
			Predicate<Operation> isNextInsideAnotherRule = o -> dateTimeUtil.getMinimumOperationDate().equals(o.getNextRunDate()) &&
				isRuleInRule(rule.getId(), o.getModelId());
			Optional<Operation> ruleNextInside = allOperationRules.stream().filter(isNextInsideAnotherRule).findFirst();
			if (ruleNextInside.isPresent())
				throw new ValidationException(null, String.format(RULE_TO_START_INSIDE_CHANGE, chooseIdentifier(ruleNextInside.get(), true, false)));
		}
		validateRuleGraph(rule);
	}

	private void validateRuleGraph(Rule rule) {
		try {
			Graph graph = Graph.fromString(rule.getDetail());
			// 1 start node bulunmak zorunda
			Predicate<Node> isStart = n -> n.getType() == NodeType.Start;
			if (graph.getNodes().stream().filter(isStart).count() != 1L)
				throw new ValidationException(null, RULE_START_NODE);
			// 1+ end node bulunmak zorunda
			Predicate<Node> isEnd = n -> n.getType() == NodeType.End;
			if (graph.getNodes().stream().filter(isEnd).count() < 1)
				throw new ValidationException(null, RULE_END_NODE);
			// rule icinde kendisi yer alamaz
			if (isRuleInRule(rule.getId(), rule))
				throw new ValidationException(null, RULE_INSIDE_ITSELF);
			// link kontrolleri
			Set<Integer> allOutgoing = new HashSet<>();
			graph.getLinks().keySet().forEach(allOutgoing::add);
			graph.getTrueLinks().keySet().forEach(allOutgoing::add);
			graph.getFalseLinks().keySet().forEach(allOutgoing::add);
			Set<Integer> trueOutgoing = new HashSet<>();
			graph.getTrueLinks().keySet().forEach(trueOutgoing::add);
			Set<Integer> falseOutgoing = new HashSet<>();
			graph.getFalseLinks().keySet().forEach(falseOutgoing::add);
			Set<Integer> normalOutgoing = new HashSet<>();
			graph.getLinks().keySet().forEach(normalOutgoing::add);
			Set<Integer> allIncoming = new HashSet<>();
			graph.getLinks().values().forEach(allIncoming::addAll);
			graph.getTrueLinks().values().forEach(allIncoming::addAll);
			graph.getFalseLinks().values().forEach(allIncoming::addAll);
			// tek node uzerinden kontroller
			for (Node node : graph.getNodes()) {
				switch (node.getType()) {
					case Comment:
						if (allOutgoing.contains(node.getId()) || allIncoming.contains(node.getId()))
							throw new ValidationException(null, COMMENT_NODE);
						break;
					case Decision:
						if (!allIncoming.contains(node.getId()))
							throw new ValidationException(null, DECISION_NO_INCOMING_LINK);
						if (!trueOutgoing.contains(node.getId()))
							throw new ValidationException(null, DECISION_NO_TRUE_OUTGOING);
						if (!falseOutgoing.contains(node.getId()))
							throw new ValidationException(null, DECISION_NOFALSE_OUTGOING);
						if (normalOutgoing.contains(node.getId()))
							throw new ValidationException(null, DECISION_REGULAR_LINK);
						DecisionNode decisionNode = (DecisionNode)node;
						Set<Integer> trueIds = decisionNode.getTrueOutgoing().stream()
							.map(n -> n.getId())
							.collect(Collectors.toSet());
						Set<Integer> falseIds = decisionNode.getFalseOutgoing().stream()
							.map(n -> n.getId())
							.collect(Collectors.toSet());
						for (Integer trueId : trueIds) {
							if (falseIds.contains(trueId))
								throw new ValidationException(null, DECISION_TO_THE_SAME_NODE);
						}
						break;
					case End:
						if (!allIncoming.contains(node.getId()))
							throw new ValidationException(null, END_NO_INCOMING);
						if (allOutgoing.contains(node.getId()))
							throw new ValidationException(null, END_OUTGOING);
						break;
					case Job:
						if (!allIncoming.contains(node.getId()))
							throw new ValidationException(null, String.format(NODE_MISSES_INCOMING, node.getType().toString().toLowerCase()));
						if (!allOutgoing.contains(node.getId()))
							throw new ValidationException(null, String.format(NODE_MISSES_OUTGOING, node.getType().toString().toLowerCase()));
						JobNode jobNode = (JobNode)node;
						if (isStringEmpty(jobNode.getJobId()))
							throw new ValidationException(null, JOB_NODE_WITHOUT_ID);
						if (jobService.get(jobNode.getJobId(), false) == null) {
							Job dbJob = jobService.get(jobNode.getJobId(), true);
							if (dbJob == null)
								throw new ValidationException(null, NO_SUCH_JOB);
							else
								throw new ValidationException(null, String.format(JOB_IN_RULE_DELETE, chooseIdentifier(dbJob, false, true)));
						}
						break;
					case Rule:
						if (!allIncoming.contains(node.getId()))
							throw new ValidationException(null, String.format(NODE_MISSES_INCOMING, node.getType().toString().toLowerCase()));
						if (!allOutgoing.contains(node.getId()))
							throw new ValidationException(null, String.format(NODE_MISSES_OUTGOING, node.getType().toString().toLowerCase()));
						RuleNode ruleNode = (RuleNode)node;
						if (isStringEmpty(ruleNode.getRuleId()))
							throw new ValidationException(null, RULE_NODE_WITHOUT_ID);
						if (ruleService.get(ruleNode.getRuleId(), false) == null) {
							Rule dbRule = ruleService.get(ruleNode.getRuleId(), true);
							if (dbRule == null)
								throw new ValidationException(null, NO_SUCH_RULE);
							else
								throw new ValidationException(null, String.format(RULE_IN_RULE_DELETE, chooseIdentifier(dbRule, false, true)));
						}
						break;
					case Start:
						if (allIncoming.contains(node.getId()))
							throw new ValidationException(null, START_INCOMING);
						if (!allOutgoing.contains(node.getId()))
							throw new ValidationException(null, START_NO_OUTGOING);
						break;
					default:
						break;
				}
			}
			// rule loop iceremez
			if (graph.isCyclic())
				throw new ValidationException(null, RULE_LOOP);
		}
		catch (Exception e) {
			throw new ValidationException(null, INVALID_GRAPH);
		}
	}

	public void validateSave(LocalParameter parameter) {
		validateNonNull(parameter);
		validateProperties(parameter.getProps());
		validateNameCommon(parameter.getProps().getName());
		validateNameParameter(parameter.getProps().getName());
		// parametre degeri bos gecilemez
		if (isStringEmpty(parameter.getValue()))
			throw new ValidationException(null, PARAMETER_VALUE);
		// sistem sabiti kullanilamaz
		if (Stream.of(GlobalVariable.values()).anyMatch(v -> v.toString().equals(parameter.getProps().getName())))
			throw new ValidationException(null, SYSTEM_VARIABLE_WITH_SAME_NAME);
		// global parametre kullanilamaz
		List<GlobalParameter> gpList = globalParameterService.listByName(parameter.getProps().getName());
		if (gpList.size() > 0)
			throw new ValidationException(null, GLOBAL_PARAMETER_WITH_SAME_NAME);
		// template sonucu kullanilamaz
		if (Stream.of(TemplateResult.values()).anyMatch(v -> v.toString().equals(parameter.getProps().getName())))
			throw new ValidationException(null, TEMPLATE_RESULT_WITH_SAME_NAME);
		// bulundugu klasorde ayni isimde (case-sensitive) baska lokal parametre varmi?
		List<LocalParameter> siblings = localParameterService.listByFolderId(parameter.getFolder() == null ? null : parameter.getFolder().getId());
		Predicate<LocalParameter> hasSameName = p -> p.getProps().getName().equals(parameter.getProps().getName()) && !p.getId().equals(parameter.getId());
		if (siblings.stream().anyMatch(hasSameName))
			throw new ValidationException(null, DUPLICATE_LOCAL_PARAMETER);
		List<LocalParameter> allLocalParameters = localParameterService.listAll();
		LocalParameter dbParameter = null;
		// silinmis parametre (baskasi tarafindan) tekrar kaydedilemez
		if (!isStringEmpty(parameter.getId()) &&
			(dbParameter = localParameterService.get(parameter.getId(), false)) == null &&
			localParameterService.get(parameter.getId(), true) != null//
		)
			throw new ValidationException(null, PARAMETER_DELETED);
		if (!isStringEmpty(parameter.getId())) {
			if (!Objects.equals(parameter.getProps().getName(), dbParameter.getProps().getName())) {
				// alt folderdaki baska lokal parametre icinde kullaniliyorsa ismi degistirilemez
				List<LocalParameter> possibleLocalParameters = new ArrayList<>();
				for (LocalParameter localParameter : allLocalParameters) {
					if (isParameterInLocalParameter(dbParameter.getProps().getName(), localParameter))
						possibleLocalParameters.add(localParameter);
				}
				if (parameter.getFolder() == null) {
					Predicate<LocalParameter> isInFolder = p -> p.getFolder() != null;
					Optional<LocalParameter> inFolderParameter = possibleLocalParameters.stream().filter(isInFolder).findFirst();
					if (inFolderParameter.isPresent())
						throw new ValidationException(null, String.format(PARAMETER_RENAME, chooseIdentifier(inFolderParameter.get(), true, false)));
				}
				else {
					Predicate<LocalParameter> isUnderThisFolder = p -> p.getFolder() != null &&
						!parameter.getFolder().getId().equals(p.getFolder().getId()) &&
						isParameterInFolder(p, parameter.getFolder());
					Optional<LocalParameter> underFolderParameter = possibleLocalParameters.stream().filter(isUnderThisFolder).findFirst();
					if (underFolderParameter.isPresent())
						throw new ValidationException(null, String.format(PARAMETER_RENAME, chooseIdentifier(underFolderParameter.get(), true, false)));
				}
				// job icinde kullaniliyorsa ismi degistirilemez
				List<Job> allJobs = jobService.listAll();
				for (Job job : allJobs) {
					if (isParameterInJob(dbParameter.getProps().getName(), job.getId()))
						throw new ValidationException(null, String.format(PARAMETER_RENAME, chooseIdentifier(job, false, false)));
				}
				// rule icinde kullaniliyorsa ismi degistirilemez
				List<Rule> allRules = ruleService.listAll();
				for (Rule rule : allRules) {
					if (isParameterInRule(dbParameter.getProps().getName(), rule.getId()))
						throw new ValidationException(null, String.format(PARAMETER_RENAME, chooseIdentifier(rule, false, false)));
				}
			}
		}
		validateParametricUsage(parameter.getValue(), parameter.getFolder());
	}

	public void validateSave(GlobalParameter parameter) {
		validateNonNull(parameter);
		validateProperties(parameter.getProps());
		validateNameCommon(parameter.getProps().getName());
		validateNameParameter(parameter.getProps().getName());
		// parametre degeri bos gecilemez
		if (isStringEmpty(parameter.getValue()))
			throw new ValidationException(null, PARAMETER_VALUE);
		// sistem sabiti kullanilamaz
		if (Stream.of(GlobalVariable.values()).anyMatch(v -> v.toString().equals(parameter.getProps().getName())))
			throw new ValidationException(null, SYSTEM_VARIABLE_WITH_SAME_NAME);
		// template sonucu kullanilamaz
		if (Stream.of(TemplateResult.values()).anyMatch(v -> v.toString().equals(parameter.getProps().getName())))
			throw new ValidationException(null, TEMPLATE_RESULT_WITH_SAME_NAME);
		// baska global variable ismi kullanilamaz
		List<GlobalParameter> allGlobalParameters = globalParameterService.listAll();
		Predicate<GlobalParameter> hasSameName = p -> p.getProps().getName().equals(parameter.getProps().getName()) &&
			!p.getId().equals(parameter.getId());
		Optional<GlobalParameter> sameNameParameter = allGlobalParameters.stream().filter(hasSameName).findAny();
		if (sameNameParameter.isPresent())
			throw new ValidationException(null, DUPLICATE_GLOBAL_PARAMETER);
		GlobalParameter dbParameter = null;
		if (!isStringEmpty(parameter.getId()) &&
			(dbParameter = globalParameterService.get(parameter.getId(), false)) == null &&
			globalParameterService.get(parameter.getId(), true) != null//
		)
			throw new ValidationException(null, PARAMETER_DELETED);
		if (!isStringEmpty(parameter.getId())) {
			if (!Objects.equals(parameter.getProps().getName(), dbParameter.getProps().getName())) {
				// lokal parametre icinde kullaniliyorsa ismi degistirilemez
				for (LocalParameter localParameter : localParameterService.listAll()) {
					if (isParameterInLocalParameter(dbParameter.getProps().getName(), localParameter))
						throw new ValidationException(null, String.format(PARAMETER_RENAME, chooseIdentifier(localParameter, false, false)));
				}
				// job icinde kullaniliyorsa ismi degistirilemez
				List<Job> allJobs = jobService.listAll();
				for (Job job : allJobs) {
					if (isParameterInJob(dbParameter.getProps().getName(), job.getId()))
						throw new ValidationException(null, String.format(PARAMETER_RENAME, chooseIdentifier(job, false, false)));
				}
				// rule icinde kullaniliyorsa ismi degistirilemez
				List<Rule> allRules = ruleService.listAll();
				for (Rule rule : allRules) {
					if (isParameterInRule(dbParameter.getProps().getName(), rule.getId()))
						throw new ValidationException(null, String.format(PARAMETER_RENAME, chooseIdentifier(rule, false, false)));
				}
			}
		}
		// context'de olmayan bir degisken kullanilamaz
		Pattern parametricPattern = Pattern.compile(PARAMETRIC_REGEX);
		Matcher matcher = parametricPattern.matcher(parameter.getValue());
		while (matcher.find()) {
			String reference = matcher.group(1);
			if (Stream.of(GlobalVariable.values()).noneMatch(v -> v.toString().equals(reference)) ||
				Stream.of(TemplateResult.values()).noneMatch(v -> v.toString().equals(reference))//
			)
				throw new ValidationException(null, String.format(NO_REFERENCE, reference));
		}
	}

	public void validateSave(Connection connection) {
		validateNonNull(connection);
		validateProperties(connection.getProps());
		validateNameCommon(connection.getProps().getName());
		validateNameExplorer(connection.getProps().getName());
		// baska connection ismi kullanilamaz
		List<Connection> list = connectionService.listByName(connection.getProps().getName());
		if (list.size() > 0 && !list.get(0).getId().equals(connection.getId()))
			throw new ValidationException(null, DUPLICATE_CONNECTION);
		if (isStringEmpty(connection.getDatabaseUrl()))
			throw new ValidationException(null, CONNECTION_URL_EMPTY);
		if (isStringEmpty(connection.getDatabaseUser()))
			throw new ValidationException(null, CONNECTION_USER_EMPTY);
		if (isStringEmpty(connection.getDatabasePassword()))
			throw new ValidationException(null, CONNECTION_PASS_EMPTY);
		// baglantiyi test et
		if (!sqlExecutor.testConnection(connection))
			throw new ValidationException(null, INVALID_CONNECTION);
		// silinmis connection (baskasi tarafindan) tekrar kaydedilemez
		if (!isStringEmpty(connection.getId()) &&
			connectionService.get(connection.getId(), false) == null &&
			connectionService.get(connection.getId(), true) != null//
		)
			throw new ValidationException(null, CONNECTION_DELETED);
		if (!isStringEmpty(connection.getId())) {
			// calisan/siradaki operasyonlar icinde kullaniliyorsa degistirilemez
			List<Operation> jobOperations = operationService.list(OperationType.Job, false, null, dateTimeUtil.getMinPlus(1));
			Predicate<Operation> isJobExecuting = o -> ServiceConstants.Operation.COLLISION_STATES.contains(o.getStatus()) &&
				isConnectionInJob(connection.getId(), o.getModelId());
			Optional<Operation> executingJob = jobOperations.stream().filter(isJobExecuting).findFirst();
			if (executingJob.isPresent())
				throw new ValidationException(null, String.format(CONNECTION_EXECUTING_INSIDE, chooseIdentifier(executingJob.get(), false, false)));
			Predicate<Operation> isJobNext = o -> dateTimeUtil.getMinimumOperationDate().equals(o.getNextRunDate()) &&
				isConnectionInJob(connection.getId(), o.getModelId());
			Optional<Operation> nextJob = jobOperations.stream().filter(isJobNext).findFirst();
			if (nextJob.isPresent())
				throw new ValidationException(null, String.format(CONNECTION_ABOUT_TO_START_INSIDE, chooseIdentifier(nextJob.get(), false, false)));
			List<Operation> ruleOperations = operationService.list(OperationType.Rule, false, null, dateTimeUtil.getMinPlus(1));
			Predicate<Operation> isRuleExecuting = o -> ServiceConstants.Operation.COLLISION_STATES.contains(o.getStatus()) &&
				isConnectionInRule(connection.getId(), o.getModelId());
			Optional<Operation> executingRule = ruleOperations.stream().filter(isRuleExecuting).findFirst();
			if (executingRule.isPresent())
				throw new ValidationException(null, String.format(CONNECTION_EXECUTING_INSIDE, chooseIdentifier(executingRule.get(), false, false)));
			Predicate<Operation> isRuleNext = o -> dateTimeUtil.getMinimumOperationDate().equals(o.getNextRunDate()) &&
				isConnectionInRule(connection.getId(), o.getModelId());
			Optional<Operation> nextRule = ruleOperations.stream().filter(isRuleNext).findFirst();
			if (nextRule.isPresent())
				throw new ValidationException(null, String.format(CONNECTION_ABOUT_TO_START_INSIDE, chooseIdentifier(nextRule.get(), false, false)));
		}
	}

	public void validateSave(License license) {
		if (license == null || StringUtils.isEmpty(license.getLicense()))
			throw new ValidationException(null, INVALID_LICENSE);
		if (validator.isValid())
			throw new ValidationException(null, LICENSE_ALREADY_EXISTS);
	}

	public void validateSave(Role role) {
		validateNonNull(role);
		validateProperties(role.getProps());
		validateNameCommon(role.getProps().getName());
		validateNameExplorer(role.getProps().getName());
		// baska rol ismi kullanilamaz
		List<Role> list = roleService.listByName(role.getProps().getName());
		if (list.size() > 0 && !list.get(0).getId().equals(role.getId()))
			throw new ValidationException(null, DUPLICATE_ROLE);
		// silinmis rol (baskasi tarafindan) tekrar kaydedilemez
		if (!isStringEmpty(role.getId()) &&
			roleService.get(role.getId(), false) == null &&
			roleService.get(role.getId(), false) != null//
		)
			throw new ValidationException(null, ROLE_DELETED);
	}

	public void validateSave(User user) throws Exception {
		validateNonNull(user);
		validateProperties(user.getProps());
		validateNameCommon(user.getProps().getName());
		// baska user ismi kullanilamaz
		User duplicateUser = userService.firstByName(user.getProps().getName());
		if (duplicateUser != null && !duplicateUser.getId().equals(user.getId()))
			throw new ValidationException(null, DUPLICATE_USER);
		// first name bos olamaz
		validateNamePerson(user.getFirstName());
		// last name bos olamaz
		validateNamePerson(user.getLastName());
		validateEmail(user);
		// baska kullanicinin emaili girilemez
		List<User> allUsers = userService.listAll();
		if (allUsers.stream().anyMatch(u -> u.getEmail().trim().equalsIgnoreCase(user.getEmail().trim()) && !u.getId().equals(user.getId())))
			throw new ValidationException(null, DUPLICATE_EMAIL);
		// login tipi belirtilmeli
		if (user.getLoginType() == null)
			throw new ValidationException(null, INVALID_LOGIN_TYPE);
		// user tipi belirtilmeli
		if (user.getLoginType() == null)
			throw new ValidationException(null, INVALID_USER_TYPE);
		// kendi kullanici duzenlenemez
		if (context.getBean(User.class).getId().equals(user.getId()))
			throw new ValidationException(null, OWN_USER);
		User dbUser = null;
		// silinmis user (baskasi tarafindan) tekrar kaydedilemez
		if (!isStringEmpty(user.getId()) &&
			(dbUser = userService.get(user.getId(), false)) == null &&
			userService.get(user.getId(), true) != null//
		)
			throw new ValidationException(null, USER_DELETED);
		// operasyonel isler etkileniyorsa user tipi degistirilemez
		if (!isStringEmpty(user.getId())) {
			if (!dbUser.getUserType().equals(user.getUserType())) {
				Boolean needsControl = false;
				switch (dbUser.getUserType()) {
					case Admin:
						switch (user.getUserType()) {
							case Normal:
								needsControl = true;
								break;
							// operator olacaksa kontrole gerek yok
							default:
								break;
						}
						break;
					// normal kullanicidan donusumde kontrole gerek yok
					case Operator:
						switch (user.getUserType()) {
							// admin olacaksa kontrole gerek yok
							case Normal:
								needsControl = true;
								break;
							default:
								break;
						}
						break;
					default:
						break;
				}
				if (needsControl) {
					List<Operation> userJobOperations = operationService.listUsersScheduled(user.getId(), OperationType.Job);
					for (Operation jobOperation : userJobOperations) {
						Date nextRunDate = quartz.getNextRunDate(jobOperation.getTriggerId(), OperationType.Job);
						if (nextRunDate != null)
							throw new ValidationException(null, USER_HAS_OPERATIONS);
					}
					List<Operation> userRuleOperations = operationService.listUsersScheduled(user.getId(), OperationType.Rule);
					for (Operation ruleOperation : userRuleOperations) {
						Date nextRunDate = quartz.getNextRunDate(ruleOperation.getTriggerId(), OperationType.Rule);
						if (nextRunDate != null)
							throw new ValidationException(null, USER_HAS_OPERATIONS);
					}
				}
			}
		}
	}

	public void validateSave(Template template) {
		if (!servicesConfig.getSetup_mode())
			throw new ValidationException(null, Messages.Validation.ERROR_SETUP);
		validateNonNull(template);
		validateNameParameter(template.getName());
		// baska template ismi kullanilamaz
		List<Template> list = templateService.listByName(template.getName());
		if (list.size() > 0 && !list.get(0).getId().equals(template.getId()))
			throw new ValidationException(null, DUPLICATE_TEMPLATE);
		// silinmis template (baskasi tarafindan) tekrar kaydedilemez
		if (!isStringEmpty(template.getId()) &&
			templateService.get(template.getId(), false) == null &&
			templateService.get(template.getId(), false) != null//
		)
			throw new ValidationException(null, TEMPLATE_DELETED);
	}

	public void validateSave(ProfileUpdate update) {
		User user = context.getBean(User.class);
		if (!StringUtils.isEmpty(update.getNewPassword())) {
			if (!bcryptUtil.getEncoder().matches(update.getOldPassword(), user.getPassword()))
				throw new ValidationException(null, INVALID_OLD_PASSWORD);
			if (update.getNewPassword().length() < MIN_PASSWORD_LENGTH)
				throw new ValidationException(null, PASSWORD_TOO_SHORT);
			if (update.getNewPassword().length() > MAX_PASSWORD_LENGTH)
				throw new ValidationException(null, PASSWORD_TOO_LONG);
			if (!update.getNewPassword().matches(PASSWORD_REGEX))
				throw new ValidationException(null, INVALID_NEW_PASSWORD);
		}
		validateEmail(user);
	}

	public void validateSave(SettingDefinition definition) {
		if (!servicesConfig.getSetup_mode())
			throw new ValidationException(null, Messages.Validation.ERROR_SETUP);
		validateNonNull(definition);
		// ayni grup icinde ayni isim kullanilamaz
		List<SettingDefinition> list = settingDefinitionService.list();
		Predicate<SettingDefinition> predicate = s -> s.getSettingGroup() == definition.getSettingGroup() && s.getName().equals(definition.getName()) && !s.getId().equals(definition.getId());
		if (list.stream().anyMatch(predicate))
			throw new ValidationException(null, DUPLICATE_SETTING);
	}

	public void validateDelete(Folder folder) {
		List<LocalParameter> allFolderParameters = localParameterService.listAll().stream()
			.filter(p -> isParameterInFolder(p, folder))
			.collect(Collectors.toList());
		// folder icindeki bir job/rule calisiyorsa/calisacaksa silinemez
		Predicate<Operation> isExecuting = o -> ServiceConstants.Operation.COLLISION_STATES.contains(o.getStatus());
		List<Operation> jobOperations = operationService.list(OperationType.Job, false, null, dateTimeUtil.getMinPlus(1));
		for (Operation jobOperation : jobOperations) {
			Boolean jobExecuting = isExecuting.test(jobOperation);
			if (isJobInFolder(jobOperation.getModelId(), folder)) {
				if (jobExecuting)
					throw new ValidationException(null, String.format(FOLDER_EXECUTING_DELETE, chooseIdentifier(jobOperation, false, true)));
				else
					throw new ValidationException(null, String.format(FOLDER_TO_START_DELETE, chooseIdentifier(jobOperation, false, true)));
			}
			Optional<LocalParameter> folderParameter = allFolderParameters.stream().filter(p -> isParameterInJob(p.getProps().getName(), jobOperation.getModelId())).findFirst();
			if (folderParameter.isPresent()) {
				if (jobExecuting)
					throw new ValidationException(null, String.format(FOLDER_WITHIN_JOB_DELETE, chooseIdentifier(folderParameter.get(), false, true)));
				else
					throw new ValidationException(null, String.format(FOLDER_TO_START_WITHIN_JOB_DELETE, chooseIdentifier(folderParameter.get(), false, true)));
			}
		}
		List<Operation> ruleOperations = operationService.list(OperationType.Rule, false, null, dateTimeUtil.getMinPlus(1));
		for (Operation ruleOperation : ruleOperations) {
			Boolean ruleExecuting = isExecuting.test(ruleOperation);
			if (isRuleInFolder(ruleOperation.getModelId(), folder)) {
				if (ruleExecuting)
					throw new ValidationException(null, String.format(FOLDER_EXECUTING_DELETE, chooseIdentifier(ruleOperation, false, true)));
				else
					throw new ValidationException(null, String.format(FOLDER_TO_START_DELETE, chooseIdentifier(ruleOperation, false, true)));
			}
			Optional<LocalParameter> folderParameter = allFolderParameters.stream().filter(p -> isParameterInRule(p.getProps().getName(), ruleOperation.getModelId())).findFirst();
			if (folderParameter.isPresent()) {
				if (ruleExecuting)
					throw new ValidationException(null, String.format(FOLDER_WITHIN_RULE_DELETE, chooseIdentifier(folderParameter.get(), false, true)));
				else
					throw new ValidationException(null, String.format(FOLDER_TO_START_WITHIN_RULE_DELETE, chooseIdentifier(folderParameter.get(), false, true)));
			}
		}
		// herhangi bir obje bu folder disindaki bir yerde kullaniliyorsa silinemez
		List<Job> allJobs = jobService.listAll();
		List<Job> allFolderJobs = allJobs.stream()
			.filter(j -> isJobInFolder(j, folder))
			.collect(Collectors.toList());
		List<Job> allOtherJobs = allJobs.stream()
			.filter(j -> !allFolderJobs.contains(j))
			.collect(Collectors.toList());
		List<Rule> allRules = ruleService.listAll();
		List<Rule> allFolderRules = allRules.stream()
			.filter(r -> isRuleInFolder(r, folder))
			.collect(Collectors.toList());
		List<Rule> allOtherRules = allRules.stream()
			.filter(r -> !allFolderRules.contains(r))
			.collect(Collectors.toList());
		for (LocalParameter localParameter : allFolderParameters) {
			for (Job job : allOtherJobs) {
				if (isParameterInJob(localParameter.getProps().getName(), job.getId()))
					throw new ValidationException(null, String.format(FOLDER_INSIDE_JOB_DELETE, chooseIdentifier(localParameter, false, true), chooseIdentifier(job, true, false)));
			}
			for (Rule rule : allOtherRules) {
				if (isParameterInRule(localParameter.getProps().getName(), rule.getId()))
					throw new ValidationException(null, String.format(FOLDER_INSIDE_JOB_DELETE, chooseIdentifier(localParameter, false, true), chooseIdentifier(rule, true, false)));
			}
		}
		for (Job job : allFolderJobs) {
			for (Rule rule : allOtherRules) {
				if (isJobInRule(job.getId(), rule))
					throw new ValidationException(null, String.format(FOLDER_INSIDE_JOB_DELETE, chooseIdentifier(job, false, true), chooseIdentifier(rule, true, false)));
			}
		}
		for (Rule rule : allFolderRules) {
			for (Rule otherRule : allOtherRules) {
				if (isRuleInRule(rule.getId(), otherRule))
					throw new ValidationException(null, String.format(FOLDER_INSIDE_JOB_DELETE, chooseIdentifier(rule, false, true), chooseIdentifier(rule, true, false)));
			}
		}
	}

	public void validateDelete(Job job) {
		// calisiyorsa veya siradaki ise silinemez
		List<Operation> jobList = operationService.list(job.getId(), OperationType.Job, false, null);
		Predicate<Operation> isExecuting = o -> ServiceConstants.Operation.COLLISION_STATES.contains(o.getStatus());
		if (jobList.stream().anyMatch(isExecuting))
			throw new ValidationException(null, JOB_EXECUTING_DELETE);
		Predicate<Operation> isNext = o -> dateTimeUtil.getMinimumOperationDate().equals(o.getNextRunDate());
		if (jobList.stream().anyMatch(isNext))
			throw new ValidationException(null, JOB_TO_START_DELETE);
		// bir rule icinde kullaniliyorsa silinemez
		for (Rule rule : ruleService.listAll()) {
			if (isJobInRule(job.getId(), rule))
				throw new ValidationException(null, String.format(JOB_INSIDE_RULE_DELETE, chooseIdentifier(rule, false, false)));
		}
	}

	public void validateDelete(Rule rule) {
		// calisiyorsa veya siradaki ise silinemez
		List<Operation> ruleList = operationService.list(rule.getId(), OperationType.Rule, false, null);
		Predicate<Operation> isExecuting = o -> ServiceConstants.Operation.COLLISION_STATES.contains(o.getStatus());
		if (ruleList.stream().anyMatch(isExecuting))
			throw new ValidationException(null, RULE_EXECUTING_DELETE);
		Predicate<Operation> isNext = o -> dateTimeUtil.getMinimumOperationDate().equals(o.getNextRunDate());
		if (ruleList.stream().anyMatch(isNext))
			throw new ValidationException(null, RULE_TO_START_DELETE);
		// bir rule icinde kullaniliyorsa silinemez
		for (Rule anotherRule : ruleService.listAll()) {
			if (isRuleInRule(rule.getId(), anotherRule))
				throw new ValidationException(null, String.format(RULE_INSIDE_RULE_DELETE, chooseIdentifier(anotherRule, true, false)));
		}
	}

	public void validateDelete(LocalParameter parameter) {
		// alt folderdaki baska lokal parametre icinde kullaniliyorsa silinemez
		List<LocalParameter> possibleLocalParameters = new ArrayList<>();
		for (LocalParameter localParameter : localParameterService.listAll()) {
			if (isParameterInLocalParameter(parameter.getProps().getName(), localParameter))
				possibleLocalParameters.add(localParameter);
		}
		for (LocalParameter localParameter : possibleLocalParameters) {
			if (parameter.getFolder() == null || isParameterInFolder(localParameter, parameter.getFolder()))
				throw new ValidationException(null, String.format(PARAMETER_DELETE, chooseIdentifier(localParameter, true, false)));
		}
		// job icinde kullaniliyorsa silinemez
		List<Job> allJobs = jobService.listAll();
		for (Job job : allJobs) {
			if (isParameterInJob(parameter.getProps().getName(), job.getId()))
				throw new ValidationException(null, String.format(PARAMETER_DELETE, chooseIdentifier(job, false, false)));
		}
		// rule icinde kullaniliyorsa silinemez
		List<Rule> allRules = ruleService.listAll();
		for (Rule rule : allRules) {
			if (isParameterInRule(parameter.getProps().getName(), rule.getId()))
				throw new ValidationException(null, String.format(PARAMETER_DELETE, chooseIdentifier(rule, false, false)));
		}
	}

	public void validateDelete(GlobalParameter parameter) {
		// lokal parametre icinde kullaniliyorsa silinemez
		List<LocalParameter> allLocalParameters = localParameterService.listAll();
		for (LocalParameter localParameter : allLocalParameters) {
			if (isParameterInLocalParameter(parameter.getProps().getName(), localParameter))
				throw new ValidationException(null, String.format(PARAMETER_DELETE, chooseIdentifier(localParameter, false, false)));
		}
		// job icinde kullaniliyorsa silinemez
		List<Job> allJobs = jobService.listAll();
		for (Job job : allJobs) {
			if (isParameterInJob(parameter.getProps().getName(), job.getId()))
				throw new ValidationException(null, String.format(PARAMETER_DELETE, chooseIdentifier(job, false, false)));
		}
		// rule icinde kullaniliyorsa silinemez
		List<Rule> allRules = ruleService.listAll();
		for (Rule rule : allRules) {
			if (isParameterInRule(parameter.getProps().getName(), rule.getId()))
				throw new ValidationException(null, String.format(PARAMETER_DELETE, chooseIdentifier(rule, false, false)));
		}
	}

	public void validateDelete(Connection connection) {
		List<Job> jobs = jobService.listAll();
		for (Job job : jobs) {
			if (isConnectionInJob(connection.getId(), job.getId()))
				throw new ValidationException(null, String.format(CONNECTION_DELETE, chooseIdentifier(job, false, false)));
		}
		List<Rule> rules = ruleService.listAll();
		for (Rule rule : rules) {
			if (isConnectionInRule(connection.getId(), rule.getId()))
				throw new ValidationException(null, String.format(CONNECTION_DELETE, chooseIdentifier(rule, false, false)));
		}
	}

	public void validateDelete(Role role) {
		// explorer obje haklarina bak
		Predicate<Rights> predicate = r -> r.getInheritsFromParent() == false && r.getRoleRights().containsKey(role.getId());
		for (Folder folder : folderService.listAll()) {
			if (predicate.test(folder.getRights()))
				throw new ValidationException(null, String.format(ROLE_DELETE, chooseIdentifier(folder, false, false)));
		}
		for (Job job : jobService.listAll()) {
			if (predicate.test(job.getRights()))
				throw new ValidationException(null, String.format(ROLE_DELETE, chooseIdentifier(job, false, false)));
		}
		for (Rule rule : ruleService.listAll()) {
			if (predicate.test(rule.getRights()))
				throw new ValidationException(null, String.format(ROLE_DELETE, chooseIdentifier(rule, false, false)));
		}
		for (LocalParameter parameter : localParameterService.listAll()) {
			if (predicate.test(parameter.getRights()))
				throw new ValidationException(null, String.format(ROLE_DELETE, chooseIdentifier(parameter, false, false)));
		}
		// rolun silinmesi ile execute hakki kaybolan operasyonlar varsa rol silinemez
		Predicate<Operation> isExecuting = o -> ServiceConstants.Operation.COLLISION_STATES.contains(o.getStatus());
		List<Operation> jobOperations = operationService.list(OperationType.Job, false, null, dateTimeUtil.getMinPlus(1));
		QuickCache<Job> jobCache = new QuickCache<>();
		jobCache.setGetter(id -> jobService.get(id, false));
		// TODO REFACTOR service.get referanslari cache.get ile degistirilecek
		for (Operation jobOperation : jobOperations) {
			Job job = jobCache.get(jobOperation.getId());
			User user = userService.get(jobOperation.getUserId(), true, false);
			// operasyonu yuruten kullanici normal ise kontrol edilir
			if (authorizationUtil.isNormalUser(user)) {
				Set<String> userRoleIds = authorizationUtil.extractUserRoles(user);
				userRoleIds.remove(role.getId());
				if (!authorizationUtil.hasRight(job.getRights(), user.getId(), userRoleIds, Rights.EXECUTE_RIGHT)) {
					if (isExecuting.test(jobOperation))
						throw new ValidationException(null, String.format(ROLE_EXECUTING_DELETE, chooseIdentifier(jobOperation, false, true)));
					else
						throw new ValidationException(null, String.format(ROLE_TO_START_DELETE, chooseIdentifier(jobOperation, false, true)));
				}
			}
		}
		List<Operation> ruleOperations = operationService.list(OperationType.Rule, false, null, dateTimeUtil.getMinPlus(1));
		QuickCache<Rule> ruleCache = new QuickCache<>();
		ruleCache.setGetter(id -> ruleService.get(id, false));
		for (Operation ruleOperation : ruleOperations) {
			Rule rule = ruleCache.get(ruleOperation.getId());
			User user = userService.get(ruleOperation.getUserId(), true, false);
			// operasyonu yuruten kullanici normal ise kontrol edilir
			if (authorizationUtil.isNormalUser(user)) {
				Set<String> userRoleIds = authorizationUtil.extractUserRoles(user);
				userRoleIds.remove(role.getId());
				if (!authorizationUtil.hasRight(rule.getRights(), user.getId(), userRoleIds, Rights.EXECUTE_RIGHT)) {
					if (isExecuting.test(ruleOperation))
						throw new ValidationException(null, String.format(ROLE_EXECUTING_DELETE, chooseIdentifier(ruleOperation, false, true)));
					else
						throw new ValidationException(null, String.format(ROLE_TO_START_DELETE, chooseIdentifier(ruleOperation, false, true)));
				}
			}
		}
	}

	public void validateDelete(User user) {
		List<Folder> allFolders = folderService.listAll();
		List<Job> allJobs = jobService.listAll();
		List<Rule> allRules = ruleService.listAll();
		List<LocalParameter> allParameters = localParameterService.listAll();
		// ownerlara bak
		Predicate<Rights> isOwner = r -> user.getId().equals(r.getOwner());
		for (Folder folder : allFolders) {
			if (isOwner.test(folder.getRights()))
				throw new ValidationException(null, String.format(USER_OWNER_DELETE, chooseIdentifier(folder, false, false)));
		}
		for (Job job : allJobs) {
			if (isOwner.test(job.getRights()))
				throw new ValidationException(null, String.format(USER_OWNER_DELETE, chooseIdentifier(job, false, false)));
		}
		for (Rule rule : allRules) {
			if (isOwner.test(rule.getRights()))
				throw new ValidationException(null, String.format(USER_OWNER_DELETE, chooseIdentifier(rule, false, false)));
		}
		for (LocalParameter parameter : allParameters) {
			if (isOwner.test(parameter.getRights()))
				throw new ValidationException(null, String.format(USER_OWNER_DELETE, chooseIdentifier(parameter, false, false)));
		}
		// explorer obje haklarina bak
		Predicate<Rights> hasRightsOnObject = r -> r.getInheritsFromParent() == false && r.getUserRights().containsKey(user.getId());
		for (Folder folder : allFolders) {
			if (hasRightsOnObject.test(folder.getRights()))
				throw new ValidationException(null, String.format(USER_DELETE, chooseIdentifier(folder, false, false)));
		}
		for (Job job : allJobs) {
			if (hasRightsOnObject.test(job.getRights()))
				throw new ValidationException(null, String.format(USER_DELETE, chooseIdentifier(job, false, false)));
		}
		for (Rule rule : allRules) {
			if (hasRightsOnObject.test(rule.getRights()))
				throw new ValidationException(null, String.format(USER_DELETE, chooseIdentifier(rule, false, false)));
		}
		for (LocalParameter parameter : allParameters) {
			if (hasRightsOnObject.test(parameter.getRights()))
				throw new ValidationException(null, String.format(USER_DELETE, chooseIdentifier(parameter, false, false)));
		}
		// kullanicinin baslattigi tum operasyonlara bak
		List<Operation> jobOperations = operationService.listByUser(user.getId(), OperationType.Job, false, null);
		List<Operation> ruleOperations = operationService.listByUser(user.getId(), OperationType.Rule, false, null);
		if (jobOperations.size() > 0 && ruleOperations.size() > 0)
			throw new ValidationException(null, String.format(USER_JOBS_RULES_DELETE, jobOperations.size(), ruleOperations.size()));
		else if (jobOperations.size() > 0)
			throw new ValidationException(null, String.format(USER_JOBS_DELETE, jobOperations.size()));
		else if (ruleOperations.size() > 0)
			throw new ValidationException(null, String.format(USER_RULES_DELETE, ruleOperations.size()));
	}

	public void validateDelete(Template template) {
		if (!servicesConfig.getSetup_mode())
			throw new ValidationException(null, Messages.Validation.ERROR_SETUP);
		// bunu kullanan job varsa silinemez
		for (Job job : jobService.listAll()) {
			if (job.getTemplate().getId().equals(template.getId()))
				throw new ValidationException(null, String.format(TEMPLATE_DELETE, chooseIdentifier(job, false, false)));
		}
	}

	public void validateDelete(SettingDefinition toDelete) {
		if (!servicesConfig.getSetup_mode())
			throw new ValidationException(null, Messages.Validation.ERROR_SETUP);
		// bunu kullanan setting varsa silinemez
		for (Setting setting : settingService.listAll()) {
			if (setting.getDefinition().getId().equals(toDelete.getId()))
				throw new ValidationException(null, SETTING_DELETE);
		}
	}

	public void validateOperationDates(Date from, Date to) {
		if (from != null && to != null) {
			if (to.before(from)) {
				SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
				String dateString = "from=" + format.format(from) + ", to=" + format.format(to);
				throw new ValidationException(null, String.format(INVALID_DATES, dateString));
			}
		}
	}

	public void validateOperationCommand(Operation operation, Command command) {
		if (!operation.getAvailableCommands().contains(command))
			throw new ValidationException(null, String.format(INVALID_COMMAND, command));
	}

	public void validateLicense() {
		if (!validator.isValid())
			throw new ValidationException(CODE_UNLICENSED, UNLICENSED);
	}

	public void validateOperationDate(Operation operation, Date date) {
		Predicate<Operation> control = o -> o.getType() == operation.getType() && o.getModelId().equals(operation.getModelId());
		List<Operation> operations = operationService.list(date);
		if (operations.stream().filter(control).findAny().isPresent())
			throw new ValidationException(null, OPERATION_COLLISION);
	}

	public void validateOperationDate(OperationType type, String modelId, Date date) {
		Predicate<Operation> control = o -> o.getType() == type && o.getModelId().equals(modelId);
		List<Operation> operations = operationService.list(date);
		if (operations.stream().filter(control).findAny().isPresent())
			throw new ValidationException(null, OPERATION_COLLISION);
	}

	public void validateSelect(String sql) {
		if (IS_NOT_SELECT.test(sql))
			throw new ValidationException(null, INVALID_SQL_SELECT);
	}

	public void validateQuery(SampleQuery query) {
		if (StringUtils.isEmpty(query.getConnectionId()))
			throw new ValidationException(null, QUERY_EMPTY_CONNECTION);
		if (StringUtils.isEmpty(query.getSql()))
			throw new ValidationException(null, QUERY_EMPTY_SQL);
		Connection connection = connectionService.get(query.getConnectionId(), false);
		if (connection == null)
			throw new ValidationException(null, QUERY_NO_CONNECTION);
		else if (!connection.getProps().isActive())
			throw new ValidationException(null, QUERY_INACTIVE_CONNECTION);
	}

	public void validateRun(Job job) {
		if (!job.getProps().isActive())
			throw new ValidationException(null, INACTIVE_JOB_RUN);
	}

	public void validateRun(Rule rule) {
		if (!rule.getProps().isActive())
			throw new ValidationException(null, INACTIVE_RULE_RUN);
	}

	public void validateSchedule(Job job) {
		if (!job.getProps().isActive())
			throw new ValidationException(null, INACTIVE_JOB_SCHEDULE);
	}

	public void validateSchedule(Rule rule) {
		if (!rule.getProps().isActive())
			throw new ValidationException(null, INACTIVE_RULE_SCHEDULE);
	}

	public void validateInternalOperationName(String name) {
		if (StringUtils.isEmpty(name))
			throw new ValidationException(null, OPERATION_NAME);
		Set<String> validNames = Arrays.asList(InternalOperation.values()).stream()
			.map(n -> n.toString())
			.collect(Collectors.toSet());
		if (!validNames.contains(name.toUpperCase()))
			throw new ValidationException(null, String.format(INTERNAL_OPERATION_NAME, name));
	}

	public void validateOperationModel(Operation operation) {
		if (operation.getType() == OperationType.Job) {
			Job job = jobService.get(operation.getModelId(), false);
			if (job == null)
				throw new ValidationException(null, JOB_DELETED);
		}
		else if (operation.getType() == OperationType.Rule) {
			Rule rule = ruleService.get(operation.getModelId(), false);
			if (rule == null)
				throw new ValidationException(null, RULE_DELETED);
		}
	}

	public void validateFilter(AuthorizationRoleUserFilter filter) {
		// rol kontrolu
		validateFilterRole(filter.getRoleId());
	}

	public void validateFilter(AuthorizationUserRoleFilter filter) {
		// user kontrolu
		validateFilterUser(filter.getUserId());
	}

	public void validateFilter(AuthorizationRoleObjectFilter filter) {
		// rol kontrolu
		validateFilterRole(filter.getRoleId());
		// haklar duzgun mu?
		validateFilterRights(filter.getRights());
	}

	public void validateFilter(AuthorizationUserObjectFilter filter) {
		// user kontrolu
		validateFilterUser(filter.getUserId());
		// haklar duzgun mu?
		validateFilterRights(filter.getRights());
	}

	private void validateFilterRole(String roleId) {
		if (!isStringEmpty(roleId)) {
			Role role = roleService.get(roleId, false);
			if (role == null)
				throw new ValidationException(null, FILTER_NO_ROLE);
		}
	}

	private void validateFilterUser(String userId) {
		if (!isStringEmpty(userId)) {
			User user = userService.get(userId, false);
			if (user == null)
				throw new ValidationException(null, FILTER_NO_USER);
		}
	}

	private void validateFilterRights(Byte rights) {
		Boolean rightsValid = rights == 0 ||
			(rights & Rights.READ_RIGHT) == Rights.READ_RIGHT ||
			(rights & Rights.WRITE_RIGHT) == Rights.WRITE_RIGHT ||
			(rights & Rights.EXECUTE_RIGHT) == Rights.EXECUTE_RIGHT;
		if (!rightsValid)
			throw new ValidationException(null, FILTER_INVALID_RIGHTS);
	}

	private Boolean isStringEmpty(String s) {
		return StringUtils.isEmpty(s);
	}

	private Boolean isStringEmptyWS(String s) {
		return StringUtils.isEmpty(s) || StringUtils.isEmpty(s.trim());
	}

	private void validateNonNull(Object object) {
		if (object == null)
			throw new ValidationException(null, SAVE_NULL);
	}

	private void validateProperties(CommonProperties props) {
		if (props == null)
			throw new ValidationException(null, PROPERTIES_NULL);
	}

	private void validateNameCommon(String name) {
		if (isStringEmptyWS(name))
			throw new ValidationException(null, NAME_EMPTY);
		if (name.length() > ServiceConstants.Model.SHORT_STRING_LENGTH)
			throw new ValidationException(null, String.format(NAME_CHARACTERS, ServiceConstants.Model.SHORT_STRING_LENGTH));
	}

	private void validateNameExplorer(String name) {
		Pattern pattern = Pattern.compile(NAME_REGEX, Pattern.UNICODE_CHARACTER_CLASS);
		if (!pattern.matcher(name).matches())
			throw new ValidationException(null, NAME_EXPLORER);
	}

	private void validateNameParameter(String name) {
		if (!name.matches(PARAMETER_NAME_REGEX))
			throw new ValidationException(null, NAME_PARAMETER);
		// java reserved words kullanilamaz
		if (ServiceConstants.Expression.JAVA_RESERVED_WORDS.contains(name))
			throw new ValidationException(null, NAME_FORBIDDEN);
	}

	private void validateNamePerson(String name) {
		if (isStringEmpty(name) || !name.matches(PERSON_NAME_REGEX))
			throw new ValidationException(null, NAME_PERSON);
	}

	private Boolean isJobInRule(String jobId, String ruleId) {
		return isJobInRule(jobId, ruleService.get(ruleId, false));
	}

	private Boolean isJobInRule(String jobId, Rule rule) {
		Boolean result = false;
		try {
			Graph graph = Graph.fromString(rule.getDetail());
			// rule derinligine inmeden once joblara bak
			for (Node node : graph.getNodes()) {
				if (node.getType() == NodeType.Job && jobId.equals(((JobNode)node).getJobId())) {
					result = true;
					break;
				}
			}
			// rulelara bak (recursive)
			if (!result) {
				for (Node node : graph.getNodes()) {
					if (node.getType() == NodeType.Rule) {
						RuleNode ruleNode = (RuleNode)node;
						Rule nestedRule = ruleService.get(ruleNode.getRuleId(), false);
						if (isJobInRule(jobId, nestedRule)) {
							result = true;
							break;
						}
					}
				}
			}
		}
		catch (Exception e) {
			throw new ParseException(String.format(PARSE_GRAPH, rule.getProps().getName()));
		}
		return result;
	}

	private Boolean isRuleInRule(String ruleId, String parentRuleId) {
		return isRuleInRule(ruleId, ruleService.get(parentRuleId, false));
	}

	private Boolean isRuleInRule(String ruleId, Rule parentRule) {
		Boolean result = false;
		try {
			Graph graph = Graph.fromString(parentRule.getDetail());
			// rule derinligine inmeden once rule node idleri kontrol et
			for (Node node : graph.getNodes()) {
				if (node.getType() == NodeType.Rule) {
					RuleNode ruleNode = (RuleNode)node;
					if (ruleId.equals(ruleNode.getRuleId())) {
						result = true;
						break;
					}
				}
			}
			// nested rulelara bak (recursive)
			for (Node node : graph.getNodes()) {
				if (node.getType() == NodeType.Rule) {
					RuleNode ruleNode = (RuleNode)node;
					Rule nestedRule = ruleService.get(ruleNode.getRuleId(), false);
					if (isRuleInRule(ruleId, nestedRule)) {
						result = true;
						break;
					}
				}
			}
		}
		catch (Exception e) {
			throw new ParseException(String.format(PARSE_GRAPH, parentRule.getProps().getName()));
		}
		return result;
	}

	private Boolean isParameterInJob(String name, String jobId) {
		Boolean result = false;
		Job job = jobService.get(jobId, false);
		if (job != null) {
			Pattern pattern = Pattern.compile(PARAMETRIC_REGEX);
			List<Variable> possibleVariables = job.getVariables().stream()
				.filter(v -> ServiceConstants.Job.PARAMETRIC_TYPES.contains(v.getDefinition().getType()))
				.collect(Collectors.toList());
			for (Variable variable : possibleVariables) {
				Matcher matcher = pattern.matcher(variable.getValue());
				if (matcher.find()) {
					String parameter = matcher.group(1);
					if (parameter.equals(name)) {
						result = true;
						break;
					}
				}
			}
		}
		return result;
	}

	private Boolean isParameterInRule(String name, String ruleId) {
		Boolean result = false;
		Rule rule = ruleService.get(ruleId, false);
		if (rule != null) {
			Pattern pattern = Pattern.compile(PARAMETRIC_REGEX);
			try {
				Graph graph = Graph.fromString(rule.getDetail());
				// once decision nodelara bak
				for (Node node : graph.getNodes()) {
					if (node.getType() == NodeType.Decision) {
						Matcher matcher = pattern.matcher(((DecisionNode)node).getExpression());
						if (matcher.find()) {
							String parameter = matcher.group(1);
							if (parameter.equals(name)) {
								result = true;
								break;
							}
						}
					}
				}
				// rule derinligine inmeden once joblara bak
				for (Node node : graph.getNodes()) {
					if (node.getType() == NodeType.Job && isParameterInJob(name, ((JobNode)node).getJobId())) {
						result = true;
						break;
					}
				}
				// rulelara bak (recursive)
				if (!result) {
					for (Node node : graph.getNodes()) {
						if (node.getType() == NodeType.Rule && isParameterInRule(name, ((RuleNode)node).getRuleId())) {
							result = true;
							break;
						}
					}
				}
			}
			catch (Exception e) {
				throw new ParseException(String.format(PARSE_GRAPH, rule.getProps().getName()));
			}
		}
		return result;
	}

	private Boolean isConnectionInJob(String connectionId, String jobId) {
		Boolean result = false;
		Job job = jobService.get(jobId, false);
		if (job != null) {
			Predicate<Variable> predicate = v -> VariableType.Connection == v.getDefinition().getType() && connectionId.equals(v.getValue());
			if (job.getVariables().stream().anyMatch(predicate))
				result = true;
		}
		return result;
	}

	private Boolean isConnectionInRule(String connectionId, String ruleId) {
		Boolean result = false;
		Rule rule = ruleService.get(ruleId, false);
		if (rule != null) {
			try {
				Graph graph = Graph.fromString(rule.getDetail());
				// rule derinligine inmeden once joblara bak
				for (Node node : graph.getNodes()) {
					if (node.getType() == NodeType.Job && isConnectionInJob(connectionId, ((JobNode)node).getJobId())) {
						result = true;
						break;
					}
				}
				// rulelara bak (recursive)
				if (!result) {
					for (Node node : graph.getNodes()) {
						if (node.getType() == NodeType.Rule && isConnectionInRule(connectionId, ((RuleNode)node).getRuleId())) {
							result = true;
							break;
						}
					}
				}
			}
			catch (Exception e) {
				throw new ParseException(String.format(PARSE_GRAPH, rule.getProps().getName()));
			}
		}
		return result;
	}

	private Boolean isCollectionEmpty(Collection<?> collection) {
		return collection == null || collection.size() == 0;
	}

	private Boolean isParameterInLocalParameter(String name, LocalParameter parameter) {
		String actualName = ServiceConstants.Expression.EXPRESSION_PREFIX + name + ServiceConstants.Expression.EXPRESSION_SUFFIX;
		return parameter.getValue().contains(actualName);
	}

	private Boolean isJobInFolder(String jobId, Folder folder) {
		Boolean result = false;
		Job job = jobService.get(jobId, false);
		if (job != null)
			result = isJobInFolder(job, folder);
		return result;
	}

	private Boolean isJobInFolder(Job job, Folder folder) {
		return isInFolderCommon(job.getFolder(), folder);
	}

	private Boolean isRuleInFolder(String ruleId, Folder folder) {
		Boolean result = false;
		Rule rule = ruleService.get(ruleId, false);
		if (rule != null)
			result = isRuleInFolder(rule, folder);
		return result;
	}

	private Boolean isRuleInFolder(Rule rule, Folder folder) {
		return isInFolderCommon(rule.getFolder(), folder);
	}

	private Boolean isParameterInFolder(String parameterId, Folder folder) {
		Boolean result = false;
		LocalParameter parameter = localParameterService.get(parameterId, false);
		if (parameter != null)
			result = isParameterInFolder(parameter, folder);
		return result;
	}

	private Boolean isParameterInFolder(LocalParameter parameter, Folder folder) {
		return isInFolderCommon(parameter.getFolder(), folder);
	}

	private Boolean isInFolderCommon(Folder parent, Folder searched) {
		Boolean result = false;
		for (Folder f = parent; f != null; f = f.getParentFolder()) {
			if (f.getId().equals(searched.getId())) {
				result = true;
				break;
			}
		}
		return result;
	}

	private String chooseIdentifier(GlobalParameter parameter, Boolean another) {
		return (another ? "another" : "the") + " global parameter '" + parameter.getProps().getName() + "'";
	}

	private String chooseIdentifier(Folder folder, Boolean another, Boolean capital) {
		String result = (another ? (capital ? "Another" : "another") : (capital ? "A" : "a")) + " folder";
		if (authorizationUtil.canViewFolder(folder, false))
			result = (another ? (capital ? "Another" : "another") : (capital ? "The" : "the")) + " folder '" + folder.getProps().getName() + "'";
		return result;
	}

	private String chooseIdentifier(LocalParameter parameter, Boolean another, Boolean capital) {
		String result = (another ? (capital ? "Another" : "another") : (capital ? "A" : "a")) + " local parameter";
		if (authorizationUtil.canViewLocalParameter(parameter, false))
			result = (another ? (capital ? "Another" : "another") : (capital ? "The" : "the")) + " local parameter '" + parameter.getProps().getName() + "'";
		return result;
	}

	private String chooseIdentifier(Job job, Boolean another, Boolean capital) {
		String result = (another ? (capital ? "Another" : "another") : (capital ? "A" : "a")) + " job";
		if (authorizationUtil.canViewJob(job, false))
			result = (another ? (capital ? "Another" : "another") : (capital ? "The" : "the")) + " job '" + job.getProps().getName() + "'";
		return result;
	}

	private String chooseIdentifier(Rule rule, Boolean another, Boolean capital) {
		String result = (another ? (capital ? "Another" : "another") : (capital ? "A" : "a")) + " rule";
		if (authorizationUtil.canViewRule(rule, false))
			result = (another ? (capital ? "Another" : "another") : (capital ? "The" : "the")) + " rule '" + rule.getProps().getName() + "'";
		return result;
	}

	private String chooseIdentifier(Operation operation, Boolean another, Boolean capital) {
		String result;
		switch (operation.getType()) {
			case Job:
				Job job = jobService.get(operation.getModelId(), false);
				result = chooseIdentifier(job, another, false);
				break;
			case Rule:
				Rule rule = ruleService.get(operation.getModelId(), false);
				result = chooseIdentifier(rule, another, false);
				break;
			default:
				result = capital ? "An operation" : "an operation";
				break;
		}
		return result;
	}

	private void validateParametricUsage(String value, Folder folder) {
		// context'de olmayan bir degisken kullanilamaz
		Pattern parametricPattern = Pattern.compile(PARAMETRIC_REGEX);
		Matcher matcher = parametricPattern.matcher(value);
		if (matcher.find()) {
			Set<String> availableLocalParameters = new HashSet<>();
			if (folder != null) {
				folder = folderService.get(folder.getId(), false);
				LinkedList<Folder> parentFolders = new LinkedList<>();
				for (Folder f = folder.getParentFolder(); f != null; f = f.getParentFolder())
					parentFolders.addFirst(f);
				for (Folder parentFolder : parentFolders)
					localParameterService.list(parentFolder).stream()
						.filter(lp -> authorizationUtil.canViewLocalParameter(lp, false))
						.map(lp -> lp.getProps().getName())
						.forEach(availableLocalParameters::add);
			}
			do {
				String reference = matcher.group(1);
				if (Stream.of(GlobalVariable.values()).noneMatch(v -> v.toString().equals(reference)) &&
					Stream.of(TemplateResult.values()).noneMatch(v -> v.toString().equals(reference)) &&
					globalParameterService.listAll().stream().noneMatch(gp -> gp.getProps().getName().equals(reference)) &&
					availableLocalParameters.stream().noneMatch(v -> v.equals(reference))//
				)
					throw new ValidationException(null, String.format(NO_REFERENCE, reference));
			}
			while (matcher.find());
		}
	}

	private void validateEmail(User user) {
		// email adresi gecerli mi?
		if (isStringEmpty(user.getEmail()) || !Pattern.matches(EMAIL_REGEX, user.getEmail()))
			throw new ValidationException(null, INVALID_EMAIL);
	}

}

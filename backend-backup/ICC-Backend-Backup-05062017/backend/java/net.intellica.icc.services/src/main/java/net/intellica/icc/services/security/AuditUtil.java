package net.intellica.icc.services.security;

import java.util.List;
import net.intellica.icc.services.config.ServicesConfig;
import net.intellica.icc.services.model.Credentials;
import net.intellica.icc.services.model.LoginFailureReason;
import net.intellica.icc.services.model.LoginLog;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.service.LoginLogService;
import net.intellica.icc.services.service.UserService;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import net.intellica.icc.template.exception.Messages;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AuditUtil {

	@Autowired private LoginLogService loginLogService;
	@Autowired private SimpleExceptionHandler exceptionHandler;
	@Autowired private UserService userService;
	@Autowired private ServicesConfig servicesConfig;

	public void logSuccessiveLogin(ProceedingJoinPoint pjp) {
		try {
			Credentials credentials = (Credentials)pjp.getArgs()[0];
			User user = userService.firstByName(credentials.getUsername());
			loginLogService.save(new LoginLog(), user);
		}
		catch (Exception e) {
			exceptionHandler.warning("Could not save successive login log!");
		}
	}

	public void logFailedLogin(ProceedingJoinPoint pjp, String reason) {
		try {
			Credentials credentials = (Credentials)pjp.getArgs()[0];
			User user = userService.firstByName(credentials.getUsername());
			if (user == null) {
				user = new User();
				user.getProps().setName(credentials.getUsername());
			}
			LoginFailureReason failureReason;
			switch (reason) {
				case Messages.Login.EMPTY:
					failureReason = LoginFailureReason.UserNotFound;
					break;
				case Messages.Login.INVALID:
					// check and lock account (if necessary)
					user = userService.get(user.getId(), false);
					Integer lockLimit = servicesConfig.getAudit_lock_failures();
					List<LoginLog> userLogins = loginLogService.list(user.getId(), 0, lockLimit);
					if (userLogins.size() == lockLimit &&
						userLogins.stream().allMatch(l -> LoginFailureReason.PasswordMismatch == l.getFailureReason())//
					) {
						user.setLocked(true);
						userService.save(user);
					}
					failureReason = LoginFailureReason.PasswordMismatch;
					break;
				case Messages.Login.LOCKED:
					failureReason = LoginFailureReason.AccountLocked;
					break;
				case Messages.Login.PASSIVE:
					failureReason = LoginFailureReason.UserPassive;
					break;
				default:
					failureReason = LoginFailureReason.Other;
					break;
			}
			LoginLog log = new LoginLog();
			log.setFailureReason(failureReason);
			loginLogService.save(log, user);
		}
		catch (Exception e) {
			exceptionHandler.warning("Could not save failed login log!");
		}
	}

	private String getServiceName(ProceedingJoinPoint pjp) {
		return pjp.getSignature().getDeclaringType().getSimpleName() + "." + pjp.getSignature().getName();
	}

}

package net.intellica.icc.services.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class BcryptUtil {

	private BCryptPasswordEncoder encoder;

	public BcryptUtil() {
		encoder = new BCryptPasswordEncoder(10);
	}

	public BCryptPasswordEncoder getEncoder() {
		return encoder;
	}

}

package net.intellica.icc.services.controller;

import java.util.List;
import net.intellica.icc.services.model.GlobalParameter;
import net.intellica.icc.services.service.GlobalParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/globalparameter")
public class GlobalParameterController {

	@Autowired private GlobalParameterService parameterService;

	@RequestMapping(value = { "/{id}", "/{id}/" }, method = RequestMethod.GET)
	public Object get(@PathVariable String id) {
		return parameterService.get(id, false);
	}

	@RequestMapping(value = { "/save", "/save/" }, method = RequestMethod.POST)
	public Object save(@RequestBody GlobalParameter parameter) {
		// TODO kullanildigi yerler guncellenmeli
		return parameterService.save(parameter).getId();
	}

	@RequestMapping(value = { "/{id}", "/{id}/" }, method = RequestMethod.DELETE)
	public Object delete(@PathVariable String id) {
		parameterService.delete(id);
		return null;
	}

	@RequestMapping(value = { "/list", "/list/" }, method = RequestMethod.GET)
	public Object list(@RequestParam(value = "name", required = false) String name) {
		List<GlobalParameter> result;
		if (StringUtils.isEmpty(name))
			result = parameterService.listAll();
		else
			result = parameterService.listByName(name);
		return result;
	}

}

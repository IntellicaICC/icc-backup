package net.intellica.icc.services.security;

import javax.persistence.AttributeConverter;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class DetailsService implements UserDetailsService {

	@Autowired private UserService userService;
	@Autowired private AttributeConverter<String, String> attributeConverter;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserDetails result;
		User user = userService.firstByName(username);
		if (user == null)
			throw new UsernameNotFoundException("User does not exist!");
		result = new SecurityDetails(user, attributeConverter);
		return result;
	}

}

package net.intellica.icc.services.security;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

public class CorsEntryPoint extends BasicAuthenticationEntryPoint {

	private static final String HEADER_ORIGIN = "Origin";

	public CorsEntryPoint() {
		super();
	}

	public CorsEntryPoint(String realmName) {
		setRealmName(realmName);
	}

	@Override
	public void commence(final HttpServletRequest request, final HttpServletResponse response, final AuthenticationException authException) throws IOException, ServletException {
		if (hasOrigin(request) && isPreflight(request)) {
			response.setStatus(HttpServletResponse.SC_OK);
			response.setHeader("Access-Control-Allow-Origin", request.getHeader(HEADER_ORIGIN));
			response.setHeader("Access-Control-Allow-Methods", "GET, HEAD, POST, DELETE, OPTIONS");
			response.setHeader("Access-Control-Allow-Credentials", Boolean.TRUE.toString());
			if (request.getHeader("Access-Control-Request-Headers") != null)
				response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
		}
		else {
			super.commence(request, response, authException);
		}
	}

	private boolean isPreflight(HttpServletRequest request) {
		return HttpMethod.OPTIONS.toString().equalsIgnoreCase(request.getMethod());
	}

	private Boolean hasOrigin(HttpServletRequest request) {
		return request.getHeader(HEADER_ORIGIN) != null;
	}

}

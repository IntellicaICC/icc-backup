package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.Arrays;

public enum SettingGroup {

	Notifications,
	Email;

	@JsonCreator
	public static SettingGroup fromString(String string) {
		return Arrays.asList(SettingGroup.values()).stream().filter(s -> s.toString().equals(string)).findFirst().orElse(null);
	}

}

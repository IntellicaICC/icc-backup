package net.intellica.icc.services.controller;

import java.util.ArrayList;
import java.util.List;
import net.intellica.icc.services.model.Folder;
import net.intellica.icc.services.model.Job;
import net.intellica.icc.services.model.LocalParameter;
import net.intellica.icc.services.model.ObjectType;
import net.intellica.icc.services.model.Rights;
import net.intellica.icc.services.model.Role;
import net.intellica.icc.services.model.Rule;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.model.report.AuthorizationRoleObjectFilter;
import net.intellica.icc.services.model.report.AuthorizationRoleObjectOrder;
import net.intellica.icc.services.model.report.AuthorizationRoleObjectReport;
import net.intellica.icc.services.model.report.AuthorizationRoleUserFilter;
import net.intellica.icc.services.model.report.AuthorizationRoleUserOrder;
import net.intellica.icc.services.model.report.AuthorizationRoleUserReport;
import net.intellica.icc.services.model.report.AuthorizationUserObjectFilter;
import net.intellica.icc.services.model.report.AuthorizationUserObjectOrder;
import net.intellica.icc.services.model.report.AuthorizationUserObjectReport;
import net.intellica.icc.services.model.report.AuthorizationUserRoleFilter;
import net.intellica.icc.services.model.report.AuthorizationUserRoleOrder;
import net.intellica.icc.services.model.report.AuthorizationUserRoleReport;
import net.intellica.icc.services.security.AuthorizationUtil;
import net.intellica.icc.services.service.FolderService;
import net.intellica.icc.services.service.JobService;
import net.intellica.icc.services.service.LocalParameterService;
import net.intellica.icc.services.service.RoleService;
import net.intellica.icc.services.service.RuleService;
import net.intellica.icc.services.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/report")
public class ReportController {

	@Autowired private RoleService roleService;
	@Autowired private UserService userService;
	@Autowired private FolderService folderService;
	@Autowired private JobService jobService;
	@Autowired private RuleService ruleService;
	@Autowired private LocalParameterService parameterService;
	@Autowired private AuthorizationUtil authorizationUtil;

	@RequestMapping(value = { "/authorization/roleuser", "/authorization/roleuser/" }, method = RequestMethod.POST)
	public Object authorizationRoleUser(@RequestBody AuthorizationRoleUserFilter filter) {
		List<AuthorizationRoleUserReport> result = new ArrayList<>();
		List<Role> roles = new ArrayList<>();
		if (StringUtils.isEmpty(filter.getRoleId())) {
			for (Role role : roleService.listAll())
				roles.add(roleService.get(role.getId(), true));
		}
		else {
			Role role = roleService.get(filter.getRoleId(), false);
			roles.add(role);
		}
		for (Role role : roles) {
			for (User user : role.getUsers()) {
				if (user.getProps().isActive() || filter.getIncludePassive()) {
					AuthorizationRoleUserReport row = new AuthorizationRoleUserReport();
					row.setRoleName(role.getProps().getName());
					row.setRoleStatus(role.getProps().isActive());
					row.setUserName(user.getProps().getName());
					row.setUserStatus(user.getProps().isActive());
					result.add(row);
				}
			}
		}
		result.sort(new AuthorizationRoleUserOrder());
		return result;
	}

	@RequestMapping(value = { "/authorization/userrole", "/authorization/userrole/" }, method = RequestMethod.POST)
	public Object authorizationUserRole(@RequestBody AuthorizationUserRoleFilter filter) {
		List<AuthorizationUserRoleReport> result = new ArrayList<>();
		List<User> users = new ArrayList<>();
		if (StringUtils.isEmpty(filter.getUserId())) {
			for (User user : userService.listAll())
				users.add(userService.get(user.getId(), true));
		}
		else {
			User user = userService.get(filter.getUserId(), false);
			users.add(user);
		}
		for (User user : users) {
			for (Role role : user.getRoles()) {
				if (role.getProps().isActive() || filter.getIncludePassive()) {
					AuthorizationUserRoleReport row = new AuthorizationUserRoleReport();
					row.setUserName(user.getProps().getName());
					row.setUserStatus(user.getProps().isActive());
					row.setRoleName(role.getProps().getName());
					row.setRoleStatus(role.getProps().isActive());
					result.add(row);
				}
			}
		}
		result.sort(new AuthorizationUserRoleOrder());
		return result;
	}

	@RequestMapping(value = { "/authorization/roleobject", "/authorization/roleobject/" }, method = RequestMethod.POST)
	public Object authorizationRoleObject(@RequestBody AuthorizationRoleObjectFilter filter) {
		List<AuthorizationRoleObjectReport> result = new ArrayList<>();
		List<Role> roles = new ArrayList<>();
		if (StringUtils.isEmpty(filter.getRoleId())) {
			for (Role role : roleService.listAll())
				roles.add(roleService.get(role.getId(), true));
		}
		else {
			Role role = roleService.get(filter.getRoleId(), false);
			roles.add(role);
		}
		for (Folder folder : folderService.listAll()) {
			if (folder.getProps().isActive() || filter.getIncludePassive()) {
				for (Role role : roles) {
					Rights rights = authorizationUtil.findRights(folder);
					if (rights != null) {
						Boolean doReport = false;
						if (filter.getRights() == null || filter.getRights() == 0) {
							if (rights.getRoleRights().containsKey(role.getId()))
								doReport = true;
						}
						else {
							if (authorizationUtil.evaluateRight(rights.getRoleRights().get(role.getId()), filter.getRights()))
								doReport = true;
						}
						if (doReport) {
							AuthorizationRoleObjectReport row = new AuthorizationRoleObjectReport();
							row.setRoleName(role.getProps().getName());
							row.setRoleStatus(role.getProps().isActive());
							row.setObjectName(folder.getProps().getName());
							row.setObjectStatus(folder.getProps().isActive());
							row.setObjectType(ObjectType.Folder);
							row.setRights(rights.getRoleRights().get(role.getId()));
							result.add(row);
						}
					}
				}
			}
		}
		for (Job job : jobService.listAll()) {
			if (job.getProps().isActive() || filter.getIncludePassive()) {
				for (Role role : roles) {
					Rights rights = authorizationUtil.findRights(job);
					if (rights != null) {
						Boolean doReport = false;
						if (filter.getRights() == null || filter.getRights() == 0) {
							if (rights.getRoleRights().containsKey(role.getId()))
								doReport = true;
						}
						else {
							if (authorizationUtil.evaluateRight(rights.getRoleRights().get(role.getId()), filter.getRights()))
								doReport = true;
						}
						if (doReport) {
							AuthorizationRoleObjectReport row = new AuthorizationRoleObjectReport();
							row.setRoleName(role.getProps().getName());
							row.setRoleStatus(role.getProps().isActive());
							row.setObjectName(job.getProps().getName());
							row.setObjectStatus(job.getProps().isActive());
							row.setObjectType(ObjectType.Job);
							row.setRights(rights.getRoleRights().get(role.getId()));
							result.add(row);
						}
					}
				}
			}
		}
		for (Rule rule : ruleService.listAll()) {
			if (rule.getProps().isActive() || filter.getIncludePassive()) {
				for (Role role : roles) {
					Rights rights = authorizationUtil.findRights(rule);
					if (rights != null) {
						Boolean doReport = false;
						if (filter.getRights() == null || filter.getRights() == 0) {
							if (rights.getRoleRights().containsKey(role.getId()))
								doReport = true;
						}
						else {
							if (authorizationUtil.evaluateRight(rights.getRoleRights().get(role.getId()), filter.getRights()))
								doReport = true;
						}
						if (doReport) {
							AuthorizationRoleObjectReport row = new AuthorizationRoleObjectReport();
							row.setRoleName(role.getProps().getName());
							row.setRoleStatus(role.getProps().isActive());
							row.setObjectName(rule.getProps().getName());
							row.setObjectStatus(rule.getProps().isActive());
							row.setObjectType(ObjectType.Rule);
							row.setRights(rights.getRoleRights().get(role.getId()));
							result.add(row);
						}
					}
				}
			}
		}
		for (LocalParameter parameter : parameterService.listAll()) {
			if (parameter.getProps().isActive() || filter.getIncludePassive()) {
				for (Role role : roles) {
					Rights rights = authorizationUtil.findRights(parameter);
					if (rights != null) {
						Boolean doReport = false;
						if (filter.getRights() == null || filter.getRights() == 0) {
							if (rights.getRoleRights().containsKey(role.getId()))
								doReport = true;
						}
						else {
							if (authorizationUtil.evaluateRight(rights.getRoleRights().get(role.getId()), filter.getRights()))
								doReport = true;
						}
						if (doReport) {
							AuthorizationRoleObjectReport row = new AuthorizationRoleObjectReport();
							row.setRoleName(role.getProps().getName());
							row.setRoleStatus(role.getProps().isActive());
							row.setObjectName(parameter.getProps().getName());
							row.setObjectStatus(parameter.getProps().isActive());
							row.setObjectType(ObjectType.Parameter);
							row.setRights(rights.getRoleRights().get(role.getId()));
							result.add(row);
						}
					}
				}
			}
		}
		result.sort(new AuthorizationRoleObjectOrder());
		return result;
	}

	@RequestMapping(value = { "/authorization/userobject", "/authorization/userobject/" }, method = RequestMethod.POST)
	public Object authorizationUserObject(@RequestBody AuthorizationUserObjectFilter filter) {
		List<AuthorizationUserObjectReport> result = new ArrayList<>();
		List<User> users = new ArrayList<>();
		if (StringUtils.isEmpty(filter.getUserId())) {
			for (User user : userService.listAll())
				users.add(userService.get(user.getId(), true));
		}
		else {
			User user = userService.get(filter.getUserId(), false);
			users.add(user);
		}
		for (Folder folder : folderService.listAll()) {
			if (folder.getProps().isActive() || filter.getIncludePassive()) {
				for (User user : users) {
					Rights rights = authorizationUtil.findRights(folder);
					if (rights != null) {
						Boolean doReport = false;
						Role foundRole = null;
						// tum haklari sorgula
						if (filter.getRights() == null || filter.getRights() == 0) {
							if (rights.getUserRights().containsKey(user.getId()))
								doReport = true;
							if (!doReport) {
								for (Role role : user.getRoles()) {
									if (rights.getRoleRights().containsKey(role.getId())) {
										foundRole = role;
										doReport = true;
										break;
									}
								}
							}
						}
						// belirli haklari sorgula
						else {
							if (authorizationUtil.evaluateRight(rights.getUserRights().get(user.getId()), filter.getRights()))
								doReport = true;
							if (!doReport) {
								for (Role role : user.getRoles()) {
									if (authorizationUtil.evaluateRight(rights.getRoleRights().get(role.getId()), filter.getRights())) {
										foundRole = role;
										doReport = true;
										break;
									}
								}
							}
						}
						if (doReport) {
							AuthorizationUserObjectReport row = new AuthorizationUserObjectReport();
							row.setUserName(user.getProps().getName());
							row.setUserStatus(user.getProps().isActive());
							if (foundRole != null)
								row.setRoleName(foundRole.getProps().getName());
							row.setObjectName(folder.getProps().getName());
							row.setObjectType(ObjectType.Folder);
							row.setObjectStatus(folder.getProps().isActive());
							if (foundRole == null)
								row.setRights(rights.getUserRights().get(user.getId()));
							else
								row.setRights(rights.getRoleRights().get(foundRole.getId()));
							result.add(row);
						}
					}
				}
			}
		}
		for (Job job : jobService.listAll()) {
			if (job.getProps().isActive() || filter.getIncludePassive()) {
				for (User user : users) {
					Rights rights = authorizationUtil.findRights(job);
					if (rights != null) {
						Boolean doReport = false;
						Role foundRole = null;
						// tum haklari sorgula
						if (filter.getRights() == null || filter.getRights() == 0) {
							if (rights.getUserRights().containsKey(user.getId()))
								doReport = true;
							if (!doReport) {
								for (Role role : user.getRoles()) {
									if (rights.getRoleRights().containsKey(role.getId())) {
										foundRole = role;
										doReport = true;
										break;
									}
								}
							}
						}
						// belirli haklari sorgula
						else {
							if (authorizationUtil.evaluateRight(rights.getUserRights().get(user.getId()), filter.getRights()))
								doReport = true;
							if (!doReport) {
								for (Role role : user.getRoles()) {
									if (authorizationUtil.evaluateRight(rights.getRoleRights().get(role.getId()), filter.getRights())) {
										foundRole = role;
										doReport = true;
										break;
									}
								}
							}
						}
						if (doReport) {
							AuthorizationUserObjectReport row = new AuthorizationUserObjectReport();
							row.setUserName(user.getProps().getName());
							row.setUserStatus(user.getProps().isActive());
							if (foundRole != null)
								row.setRoleName(foundRole.getProps().getName());
							row.setObjectName(job.getProps().getName());
							row.setObjectType(ObjectType.Job);
							row.setObjectStatus(job.getProps().isActive());
							if (foundRole == null)
								row.setRights(rights.getUserRights().get(user.getId()));
							else
								row.setRights(rights.getRoleRights().get(foundRole.getId()));
							result.add(row);
						}
					}
				}
			}
		}
		for (Rule rule : ruleService.listAll()) {
			if (rule.getProps().isActive() || filter.getIncludePassive()) {
				for (User user : users) {
					Rights rights = authorizationUtil.findRights(rule);
					if (rights != null) {
						Boolean doReport = false;
						Role foundRole = null;
						// tum haklari sorgula
						if (filter.getRights() == null || filter.getRights() == 0) {
							if (rights.getUserRights().containsKey(user.getId()))
								doReport = true;
							if (!doReport) {
								for (Role role : user.getRoles()) {
									if (rights.getRoleRights().containsKey(role.getId())) {
										foundRole = role;
										doReport = true;
										break;
									}
								}
							}
						}
						// belirli haklari sorgula
						else {
							if (authorizationUtil.evaluateRight(rights.getUserRights().get(user.getId()), filter.getRights()))
								doReport = true;
							if (!doReport) {
								for (Role role : user.getRoles()) {
									if (authorizationUtil.evaluateRight(rights.getRoleRights().get(role.getId()), filter.getRights())) {
										foundRole = role;
										doReport = true;
										break;
									}
								}
							}
						}
						if (doReport) {
							AuthorizationUserObjectReport row = new AuthorizationUserObjectReport();
							row.setUserName(user.getProps().getName());
							row.setUserStatus(user.getProps().isActive());
							if (foundRole != null)
								row.setRoleName(foundRole.getProps().getName());
							row.setObjectName(rule.getProps().getName());
							row.setObjectType(ObjectType.Rule);
							row.setObjectStatus(rule.getProps().isActive());
							if (foundRole == null)
								row.setRights(rights.getUserRights().get(user.getId()));
							else
								row.setRights(rights.getRoleRights().get(foundRole.getId()));
							result.add(row);
						}
					}
				}
			}
		}
		for (LocalParameter parameter : parameterService.listAll()) {
			if (parameter.getProps().isActive() || filter.getIncludePassive()) {
				for (User user : users) {
					Rights rights = authorizationUtil.findRights(parameter);
					if (rights != null) {
						Boolean doReport = false;
						Role foundRole = null;
						// tum haklari sorgula
						if (filter.getRights() == null || filter.getRights() == 0) {
							if (rights.getUserRights().containsKey(user.getId()))
								doReport = true;
							if (!doReport) {
								for (Role role : user.getRoles()) {
									if (rights.getRoleRights().containsKey(role.getId())) {
										foundRole = role;
										doReport = true;
										break;
									}
								}
							}
						}
						// belirli haklari sorgula
						else {
							if (authorizationUtil.evaluateRight(rights.getUserRights().get(user.getId()), filter.getRights()))
								doReport = true;
							if (!doReport) {
								for (Role role : user.getRoles()) {
									if (authorizationUtil.evaluateRight(rights.getRoleRights().get(role.getId()), filter.getRights())) {
										foundRole = role;
										doReport = true;
										break;
									}
								}
							}
						}
						if (doReport) {
							AuthorizationUserObjectReport row = new AuthorizationUserObjectReport();
							row.setUserName(user.getProps().getName());
							row.setUserStatus(user.getProps().isActive());
							if (foundRole != null)
								row.setRoleName(foundRole.getProps().getName());
							row.setObjectName(parameter.getProps().getName());
							row.setObjectType(ObjectType.Parameter);
							row.setObjectStatus(parameter.getProps().isActive());
							if (foundRole == null)
								row.setRights(rights.getUserRights().get(user.getId()));
							else
								row.setRights(rights.getRoleRights().get(foundRole.getId()));
							result.add(row);
						}
					}
				}
			}
		}
		result.sort(new AuthorizationUserObjectOrder());
		return result;
	}

}

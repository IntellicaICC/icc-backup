package net.intellica.icc.services.repo;

import java.util.Date;
import java.util.List;
import net.intellica.icc.services.model.ErrorLog;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ErrorLogRepository extends PagingAndSortingRepository<ErrorLog, String> {

	List<ErrorLog> findAllByErrorDateOrderByErrorDateDesc(Date errorDate, Pageable pageable);

}

package net.intellica.icc.services.model.report;

import java.util.Comparator;

public class AuthorizationUserObjectOrder implements Comparator<AuthorizationUserObjectReport> {

	@Override
	public int compare(AuthorizationUserObjectReport r1, AuthorizationUserObjectReport r2) {
		Integer result = r1.getUserName().compareTo(r2.getUserName());
		if (result == 0)
			result = r1.getObjectName().compareTo(r2.getObjectName());
		if (result == 0)
			result = r1.getObjectType().compareTo(r2.getObjectType());
		return result;
	}

}

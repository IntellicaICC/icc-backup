package net.intellica.icc.services.model;

public enum ObjectType {

	Folder,
	Job,
	Rule,
	Parameter

}

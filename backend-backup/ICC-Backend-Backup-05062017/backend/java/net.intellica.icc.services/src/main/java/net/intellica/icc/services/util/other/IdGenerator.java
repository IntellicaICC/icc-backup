package net.intellica.icc.services.util.other;

import java.time.Instant;
import java.util.Random;
import net.intellica.icc.services.model.operation.OperationType;
import net.intellica.icc.services.service.OperationService;
import net.intellica.icc.services.util.dev.MathUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IdGenerator {

	private static final String SEPARATOR = "_";

	@Autowired private MathUtil math;
	@Autowired private OperationService operationService;

	public String generateDatabaseId() {
		return generateLong().toString();
	}

	public String generateTriggerId(String id) {
		return id + SEPARATOR + math.convert((long)Instant.now().getNano());
	}

	public synchronized String generateOperationalInstanceId(String id, OperationType type) {
		String result;
		try {
			String maxInstanceId = operationService.getMaxInstanceId(id, type);
			String[] ids = maxInstanceId.split(SEPARATOR);
			Integer nextInstanceNo = Integer.valueOf(ids[2]) + 1;
			result = ids[0] + SEPARATOR + ids[1] + SEPARATOR + nextInstanceNo;
		}
		catch (Exception e) {
			result = id + SEPARATOR + 1;
		}
		return result;
	}

	public String generateRandomInstanceId() {
		return new Long(generateLong() + Instant.now().getNano()).toString();
	}

	public String generateSubRuleInstanceId(String parentInstanceId) {
		return parentInstanceId + SEPARATOR + generateRandomInstanceId();
	}

	private Long generateLong() {
		return new Long(System.nanoTime() + new Random().nextInt(100));
	}

}

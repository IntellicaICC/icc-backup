package net.intellica.icc.services.security;

import net.intellica.icc.services.model.UserType;
import net.intellica.icc.services.util.dev.StringUtil;
import net.intellica.icc.services.util.filter.ServiceAuditFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CorsFilter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	public static final String ROLE_ADMIN = StringUtil.toUpper(UserType.Admin);
	public static final String ROLE_NORMAL = StringUtil.toUpper(UserType.Normal);
	public static final String ROLE_OPERATOR = StringUtil.toUpper(UserType.Operator);

	@Autowired private CorsFilter corsFilter;
	@Autowired private ServiceAuditFilter auditFilter;
	@Autowired private UserDetailsService detailsService;
	@Autowired private BcryptUtil bcryptUtil;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
			.userDetailsService(detailsService)
			.passwordEncoder(bcryptUtil.getEncoder());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.addFilterBefore(corsFilter, CsrfFilter.class)
			.addFilterAfter(auditFilter, BasicAuthenticationFilter.class)
			.authorizeRequests()
			.antMatchers("/service/**").permitAll()
			.antMatchers("/login").permitAll()
			.antMatchers("/license/**").permitAll()
			.antMatchers("/version").permitAll()
			.antMatchers("/operation/**").authenticated()
			.antMatchers("/profile/**").authenticated()
			.antMatchers("/notification/**").authenticated()
			.antMatchers("/user/**").authenticated()
			.antMatchers("/connection/**").authenticated()
			.antMatchers("/role/**").hasAnyRole(ROLE_ADMIN)
			.anyRequest().hasAnyRole(ROLE_ADMIN, ROLE_NORMAL).and()
			.csrf().disable()
			.formLogin().disable()
			.logout().disable()
			.httpBasic().realmName("ICC Service").and()
			.requiresChannel().anyRequest().requiresSecure();
	}

}

package net.intellica.icc.services.repo;

import net.intellica.icc.services.model.operation.Command;
import net.intellica.icc.services.model.operation.CommandLog;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommandLogRepository extends PagingAndSortingRepository<CommandLog, String> {

	CommandLog findFirstByOperationIdAndCommandOrderByCommandDateDesc(String operationId, Command command);

}

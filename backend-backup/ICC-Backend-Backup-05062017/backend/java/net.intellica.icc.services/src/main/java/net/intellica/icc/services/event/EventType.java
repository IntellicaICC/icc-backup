package net.intellica.icc.services.event;

public enum EventType {
	
	Start,
	Finish,
	Error,
	Cancel,
	Skip
	
}
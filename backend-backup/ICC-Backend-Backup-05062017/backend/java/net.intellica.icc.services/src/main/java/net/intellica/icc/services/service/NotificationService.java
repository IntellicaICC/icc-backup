package net.intellica.icc.services.service;

import java.util.ArrayList;
import java.util.List;
import net.intellica.icc.services.model.Notification;
import net.intellica.icc.services.repo.NotificationRepository;
import net.intellica.icc.services.util.aop.PreSaveUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NotificationService {

	@Autowired private NotificationRepository repository;
	@Autowired private PreSaveUtil preSaveUtil;

	public NotificationService() {
	}

	public Notification get(String id) {
		return repository.findOne(id);
	}

	public Notification save(Notification notification) {
		preSaveUtil.fill(notification);
		return repository.save(notification);
	}

	public List<Notification> saveAll(List<Notification> notifications) {
		List<Notification> result = new ArrayList<>();
		preSaveUtil.fillAllNotifications(notifications);
		repository.save(notifications).forEach(result::add);
		return result;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteAll(String userId) {
		repository.deleteAllByTargetUser(userId);
	}

	public List<Notification> list(String userId, Integer page, Integer size) {
		return repository.findAllByTargetUserOrderByNotificationDateDesc(userId, new PageRequest(page, size));
	}

	public Integer countUnread(String userId) {
		return repository.countAllByTargetUserAndRead(userId, false);
	}

}

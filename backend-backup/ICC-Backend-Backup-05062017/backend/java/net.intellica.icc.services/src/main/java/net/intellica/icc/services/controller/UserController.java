package net.intellica.icc.services.controller;

import java.util.ArrayList;
import java.util.List;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.service.SettingService;
import net.intellica.icc.services.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired private UserService userService;
	@Autowired private SettingService settingService;

	@RequestMapping(value = { "/{id}", "/{id}/" }, method = RequestMethod.GET)
	public Object get(@PathVariable String id) {
		return userService.get(id, false);
	}

	@RequestMapping(value = { "/save", "/save/" }, method = RequestMethod.POST)
	public Object save(@RequestBody User user) {
		String result;
		result = userService.save(user).getId();
		return result;
	}

	@RequestMapping(value = { "/{id}", "/{id}/" }, method = RequestMethod.DELETE)
	public Object delete(@PathVariable String id) {
		userService.delete(id);
		settingService.deleteAll(id);
		return null;
	}

	@RequestMapping(value = { "/list", "/list/" }, method = RequestMethod.GET)
	public Object list(@RequestParam(value = "name", required = false) String name) {
		List<User> result = new ArrayList<>();
		if (StringUtils.isEmpty(name))
			result = userService.listAll();
		else
			result.add(userService.firstByName(name));
		return result;
	}

}

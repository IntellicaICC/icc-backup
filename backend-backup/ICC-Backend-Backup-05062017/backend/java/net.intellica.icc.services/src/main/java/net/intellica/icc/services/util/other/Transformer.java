package net.intellica.icc.services.util.other;

public interface Transformer<T, R> {

	R transform(T t);

}

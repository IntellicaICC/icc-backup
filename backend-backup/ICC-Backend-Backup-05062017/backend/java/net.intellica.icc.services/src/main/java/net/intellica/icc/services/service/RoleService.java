package net.intellica.icc.services.service;

import java.util.ArrayList;
import java.util.List;
import net.intellica.icc.services.model.Role;
import net.intellica.icc.services.repo.RoleRepository;
import net.intellica.icc.services.util.audit.HistoryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

	@Autowired private RoleRepository repository;
	@Autowired private HistoryUtil historyUtil;

	public RoleService() {
	}

	public Role get(String id, Boolean includeHistory) {
		Role result = repository.findOne(id);
		if (includeHistory && result == null)
			result = historyUtil.getLastRevision(Role.class, id);
		return result;
	}

	public Role save(Role role) {
		return repository.save(role);
	}

	public void delete(String id) {
		repository.delete(id);
	}

	public List<Role> listAll() {
		List<Role> result = new ArrayList<>();
		repository.findAllByOrderByPropsNameAsc().forEach(result::add);
		return result;
	}

	public List<Role> listByName(String name) {
		return repository.findAllByPropsName(name);
	}

}

package net.intellica.icc.services.event;

import java.util.EventListener;

public interface QueueEventListener extends EventListener {

	public void queueEvent(QueueEvent event);

}

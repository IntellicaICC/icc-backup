package net.intellica.icc.services.util.aop;

import java.util.Collection;
import net.intellica.icc.services.model.Folder;
import net.intellica.icc.services.model.Job;
import net.intellica.icc.services.model.LocalParameter;
import net.intellica.icc.services.model.Rights;
import net.intellica.icc.services.model.Rule;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.security.AuthorizationUtil;
import net.intellica.icc.services.service.FolderService;
import net.intellica.icc.services.util.other.GraphUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class FillUtil {

	@Autowired private ApplicationContext context;
	@Autowired private AuthorizationUtil authorizationUtil;
	@Autowired private FolderService folderService;
	@Autowired private GraphUtil graphUtil;

	private void fillRights(Rights rights, Rights parentRights) {
		if (rights.getInheritsFromParent() && parentRights != null) {
			rights.setRoleRights(parentRights.getRoleRights());
			rights.setUserRights(parentRights.getUserRights());
		}
	}

	public void fillFolderFields(Collection<Folder> collection) {
		collection.forEach(this::fillFolderFields);
	}

	public void fillFolderFields(Folder folder) {
		setChildFolderField(folder);
		fillRights(folder.getRights(), authorizationUtil.findRights(folder));
	}

	public void setChildFolderField(Folder folder) {
		setChildFolderField(folder, null);
	}

	public void setChildFolderField(Folder folder, Boolean hasChildFolders) {
		setChildFolderField(folder, hasChildFolders, context.getBean(User.class));
	}

	public void setChildFolderField(Folder folder, Boolean hasChildFolders, User user) {
		if (hasChildFolders != null)
			folder.setHasChildFolders(hasChildFolders);
		else {
			if (authorizationUtil.isAdminUser())
				folder.setHasChildFolders(folderService.countByParentFolderId(folder.getId()) > 0);
			else
				folder.setHasChildFolders(authorizationUtil.hasChildFolderWithReadRight(folder, user));
		}
	}

	public void fillLocalParameterFields(Collection<LocalParameter> collection) {
		collection.forEach(this::fillLocalParameterFields);
	}

	public void fillLocalParameterFields(LocalParameter parameter) {
		fillRights(parameter.getRights(), authorizationUtil.findRights(parameter));
	}

	public void fillJobFields(Collection<Job> collection) {
		collection.forEach(this::fillJobFields);
	}

	public void fillJobFields(Job job) {
		fillRights(job.getRights(), authorizationUtil.findRights(job));
	}

	public void fillRuleFields(Collection<Rule> collection) {
		for (Rule rule : collection)
			fillRights(rule.getRights(), authorizationUtil.findRights(rule));
	}

	public void fillRuleFields(Rule rule) {
		fillRights(rule.getRights(), authorizationUtil.findRights(rule));
		fillNames(rule);
	}

	public void fillNames(Rule rule) {
		rule.setDetail(graphUtil.fillNames(rule.getDetail()));
	}

}

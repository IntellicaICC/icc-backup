package net.intellica.icc.services.model.operation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import net.intellica.icc.services.model.Schedule;
import net.intellica.icc.services.util.other.ServiceConstants;
import org.hibernate.envers.Audited;

@Entity
@Audited // TODO kimin sildigi bilinmiyor
@Table(name = "ICC_OPERATION")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Operation {

	private String id;
	private String userId;
	private String userName;
	private Date setDate;
	private OperationType type;
	private String modelId;
	private String modelName;
	private Schedule schedule;
	private String triggerId;
	private String instanceId;
	private Date nextRunDate;
	private Boolean skipNextRun;
	private OperationSummary summary;
	private OperationDetail detail;
	private OperationLog lastLog;
	private OperationLog startLog;
	// TODO private String rightsId;

	public Operation() {
		detail = new OperationDetail();
	}

	public Operation(Operation base) {
		this();
		this.userId = base.userId;
		this.userName = base.userName;
		this.setDate = base.setDate;
		this.type = base.type;
		this.modelId = base.modelId;
		this.modelName = base.modelName;
		this.schedule = base.schedule;
		this.triggerId = base.triggerId;
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "USER_ID", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	@JsonIgnore
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "USER_NAME", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	@JsonIgnore
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "SET_DATE", nullable = false)
	@JsonIgnore
	public Date getSetDate() {
		return setDate;
	}

	public void setSetDate(Date setDate) {
		this.setDate = setDate;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "TYPE", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public OperationType getType() {
		return type;
	}

	public void setType(OperationType type) {
		this.type = type;
	}

	@Column(name = "MODEL_ID", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	@Column(name = "MODEL_NAME", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	@Embedded
	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	@Column(name = "TRIGGER_ID", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	@JsonIgnore
	public String getTriggerId() {
		return triggerId;
	}

	public void setTriggerId(String triggerId) {
		this.triggerId = triggerId;
	}

	@Column(name = "INSTANCE_ID", nullable = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	@JsonIgnore
	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "NEXT_RUN_DATE", nullable = false)
	public Date getNextRunDate() {
		return nextRunDate;
	}

	public void setNextRunDate(Date nextRunDate) {
		this.nextRunDate = nextRunDate;
	}

	@Column(name = "SKIP", nullable = false)
	public Boolean getSkipNextRun() {
		return skipNextRun;
	}

	public void setSkipNextRun(Boolean skipNextRun) {
		this.skipNextRun = skipNextRun;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "SUMMARY", nullable = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public OperationSummary getSummary() {
		return summary;
	}

	public void setSummary(OperationSummary summary) {
		this.summary = summary;
	}

	@Embedded
	public OperationDetail getDetail() {
		return detail;
	}

	public void setDetail(OperationDetail detail) {
		this.detail = detail;
	}

	@Transient
	@JsonIgnore
	public OperationLog getStartLog() {
		return startLog;
	}

	public void setStartLog(OperationLog startLog) {
		this.startLog = startLog;
	}

	@Transient
	@JsonIgnore
	public OperationLog getLastLog() {
		return lastLog;
	}

	public void setLastLog(OperationLog lastLog) {
		this.lastLog = lastLog;
	}

	@Transient
	public OperationStatus getStatus() {
		return lastLog.getStatus();
	}

	@Transient
	public Date getStartDate() {
		return startLog == null ? nextRunDate : startLog.getOperationDate();
	}

	@Transient
	public Date getEndDate() {
		return summary != null ? lastLog.getOperationDate() : null;
	}

	@Transient
	public List<Command> getAvailableCommands() {
		List<Command> result = new ArrayList<>();
		OperationStatus status = lastLog == null ? null : lastLog.getStatus();
		// TODO status null olamaz
		switch (status) {
			case Idle:
				result.add(Command.Start);
				result.add(Command.Skip);
				result.add(Command.Reschedule);
				result.add(Command.Unschedule);
				break;
			case Executing:
//				result.add(Command.Pause);
				result.add(Command.Cancel);
				if (type == OperationType.Rule)
					result.add(Command.Live);
				break;
			case Paused:
//				result = Arrays.asList(Command.Resume);
				result.add(Command.Cancel);
				if (type == OperationType.Rule)
					result.add(Command.Live);
				break;
			case Finished:
			case Error:
				result.add(Command.Restart);
				result.add(Command.Details);
				if (type == OperationType.Rule)
					result.add(Command.Live);
				break;
			case Skipped:
				result.add(Command.Start);
				result.add(Command.Details);
				if (type == OperationType.Rule)
					result.add(Command.Live);
				break;
			case Canceled:
				result.add(Command.Start);
				break;
			case Canceling:
			case Pausing:
				result.add(Command.None);
				break;
			default:
				break;
		}
		return result;
	}

}

package net.intellica.icc.services.model.report;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import net.intellica.icc.services.model.ObjectType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorizationRoleObjectReport {

	private String roleName;
	private String objectName;
	private ObjectType objectType;
	private Byte rights;
	private Boolean roleStatus;
	private Boolean objectStatus;

	public AuthorizationRoleObjectReport() {
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public ObjectType getObjectType() {
		return objectType;
	}

	public void setObjectType(ObjectType objectType) {
		this.objectType = objectType;
	}

	public Byte getRights() {
		return rights;
	}

	public void setRights(Byte rights) {
		this.rights = rights;
	}

	public Boolean getRoleStatus() {
		return roleStatus;
	}

	public void setRoleStatus(Boolean roleStatus) {
		this.roleStatus = roleStatus;
	}

	public Boolean getObjectStatus() {
		return objectStatus;
	}

	public void setObjectStatus(Boolean objectStatus) {
		this.objectStatus = objectStatus;
	}

}

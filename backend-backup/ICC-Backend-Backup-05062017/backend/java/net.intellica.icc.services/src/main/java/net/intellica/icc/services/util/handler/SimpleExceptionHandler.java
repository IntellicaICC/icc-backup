package net.intellica.icc.services.util.handler;

import net.intellica.icc.services.controller.ErrorLogController;
import net.intellica.icc.services.event.ErrorEvent;
import net.intellica.icc.services.model.ResponseWrapper;
import net.intellica.icc.services.model.User;
import net.intellica.icc.template.exception.DatabaseException;
import net.intellica.icc.template.exception.ForbiddenException;
import net.intellica.icc.template.exception.Messages;
import net.intellica.icc.template.exception.NotFoundException;
import net.intellica.icc.template.exception.NotImplementedException;
import net.intellica.icc.template.exception.SimpleCodedException;
import net.intellica.icc.template.exception.TechnicalException;
import net.intellica.icc.template.exception.ValidationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class SimpleExceptionHandler {

	private static final Log LOG = LogFactory.getLog(SimpleExceptionHandler.class);

	@Autowired private ErrorLogController errorLogController;

	public void handle(Throwable t, User user) throws Exception {
		Exception exception = selectException(t);
		errorLogController.save(new ErrorEvent(exception, user));
		error(t);
		throw exception;
	}

	public void handleQuietly(Throwable t, User user) {
		Exception exception = selectException(t);
		errorLogController.save(new ErrorEvent(exception, user));
		error(t);
	}

	public Object handleResponse(Throwable t, User user) {
		Exception exception = selectException(t);
		HttpStatus status = selectStatus(exception);
		errorLogController.save(new ErrorEvent(exception, user));
		error(exception);
		ResponseWrapper response = new ResponseWrapper();
		if (exception instanceof SimpleCodedException)
			response.setErrorCode(((SimpleCodedException)exception).getCode());
		response.setErrorMessage(exception.getMessage());
		return new ResponseEntity<>(response, status);
	}

	public void debug(String message) {
		LOG.debug(message);
	}

	public void info(String message) {
		LOG.info(message);
	}

	public void warning(String message) {
		LOG.warn(message);
	}

	public void error(Throwable t) {
		LOG.error(t.getMessage(), t);
	}

	private Exception selectException(Throwable t) {
		Exception result;
		try {
			throw t;
		}
		catch (DataAccessException e) {
			if (StringUtils.isEmpty(e.getMessage()))
				result = new DatabaseException(Messages.Generic.DATABASE, e);
			else
				result = new DatabaseException(e.getMessage(), e);
		}
		catch (ValidationException e) {
			if (StringUtils.isEmpty(e.getMessage()))
				result = new ValidationException(Messages.Generic.CODE_VALIDATION, Messages.Generic.VALIDATION, e.getCause());
			else
				result = e;
		}
		catch (AccessDeniedException e) {
			if (StringUtils.isEmpty(e.getMessage()))
				result = new ForbiddenException(String.format(Messages.Generic.ACCESS_DENIED, "access"), e);
			else
				result = new ForbiddenException(e.getMessage(), e);
		}
		catch (ForbiddenException e) {
			if (StringUtils.isEmpty(e.getMessage()))
				result = new ForbiddenException(Messages.Generic.FORBIDDEN, e.getCause());
			else
				result = e;
		}
		catch (TechnicalException e) {
			if (StringUtils.isEmpty(e.getMessage()))
				result = new TechnicalException(Messages.Generic.TECHNICAL, e.getCause());
			else
				result = e;
		}
		catch (NotFoundException e) {
			if (StringUtils.isEmpty(e.getMessage()))
				result = new NotFoundException(Messages.Generic.NOT_FOUND, e.getCause());
			else
				result = e;
		}
		catch (Throwable e) {
			if (StringUtils.isEmpty(e.getMessage()))
				result = new TechnicalException(Messages.Generic.TECHNICAL, e);
			else
				result = new TechnicalException(e.getMessage(), e);
		}
		return result;
	}

	private HttpStatus selectStatus(Exception exception) {
		HttpStatus result = HttpStatus.INTERNAL_SERVER_ERROR;
		if (exception instanceof NotFoundException)
			result = HttpStatus.NOT_FOUND;
		else if (exception instanceof ForbiddenException)
			result = HttpStatus.FORBIDDEN;
		else if (exception instanceof ValidationException)
			result = HttpStatus.BAD_REQUEST;
		else if (exception instanceof NotImplementedException)
			result = HttpStatus.NOT_IMPLEMENTED;
		return result;
	}

}

package net.intellica.icc.services.model.sd;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import java.io.IOException;
import java.util.Iterator;
import net.intellica.icc.services.model.rule.CommentNode;
import net.intellica.icc.services.model.rule.DecisionNode;
import net.intellica.icc.services.model.rule.EndNode;
import net.intellica.icc.services.model.rule.JobNode;
import net.intellica.icc.services.model.rule.Node;
import net.intellica.icc.services.model.rule.NodeType;
import net.intellica.icc.services.model.rule.RuleNode;
import net.intellica.icc.services.model.rule.StartNode;
import org.springframework.util.StringUtils;

public class NodeDeserializer extends JsonDeserializer<Node> {

	@Override
	public Node deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		Node result = null;
		try {
			TreeNode tree = p.readValueAsTree();
			if (tree instanceof JsonNode) {
				JsonNode json = (JsonNode)tree;
				String nodeTypeString = determineType(json);
				NodeType nodeType = NodeType.valueOf(StringUtils.capitalize(nodeTypeString.toLowerCase()));
				switch (nodeType) {
					case Start:
						result = new StartNode();
						break;
					case Job:
						JobNode jobNode = new JobNode();
						jobNode.setJobId(json.get("jobId").asText());
						jobNode.setName(json.get("name").asText());
						jobNode.setDescription(json.get("description").asText());
						result = jobNode;
						break;
					case Rule:
						RuleNode ruleNode = new RuleNode();
						ruleNode.setRuleId(json.get("ruleId").asText());
						ruleNode.setName(json.get("name").asText());
						ruleNode.setDescription(json.get("description").asText());
						result = ruleNode;
						break;
					case Decision:
						DecisionNode decisionNode = new DecisionNode();
						decisionNode.setExpression(json.get("expression").asText());
						result = decisionNode;
						break;
					case Comment:
						CommentNode commentNode = new CommentNode();
						commentNode.setComment(json.get("comment").asText());
						result = commentNode;
						break;
					case End:
						result = new EndNode();
						break;
					default:
						throw new InvalidFormatException("Unsupported node type", nodeTypeString, Node.class);
				}
				result.setId(Integer.parseInt(json.get("id").asText()));
				result.setX(Integer.parseInt(json.get("x").asText()));
				result.setY(Integer.parseInt(json.get("y").asText()));
			}
		}
		catch (NumberFormatException e) {
			throw new InvalidFormatException("Non-integer coordinates", null, Node.class);
		}
		catch (Exception e) {
			throw new InvalidFormatException("Unsupported format", null, Node.class);
		}
		return result;
	}

	private String determineType(JsonNode node) {
		String result = "";
		Iterator<String> fieldNames = node.fieldNames();
		while (fieldNames.hasNext()) {
			String field = (String)fieldNames.next();
			if ("type".equals(field)) {
				result = node.get(field).asText();
				break;
			}
		}
		return result;
	}

}

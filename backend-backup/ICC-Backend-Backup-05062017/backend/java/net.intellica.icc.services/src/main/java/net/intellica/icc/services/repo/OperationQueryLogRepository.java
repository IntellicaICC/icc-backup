package net.intellica.icc.services.repo;

import java.util.List;
import net.intellica.icc.services.model.operation.OperationQueryLog;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OperationQueryLogRepository extends PagingAndSortingRepository<OperationQueryLog, String> {

	public List<OperationQueryLog> findAllByOperationIdOrderByQueryDateDesc(String operationId);

}

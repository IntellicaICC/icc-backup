package net.intellica.icc.services.repo;

import java.util.List;
import net.intellica.icc.services.model.Connection;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConnectionRepository extends PagingAndSortingRepository<Connection, String> {

	List<Connection> findAllByPropsName(String name);

	List<Connection> findAllByOrderByPropsNameAsc();

}

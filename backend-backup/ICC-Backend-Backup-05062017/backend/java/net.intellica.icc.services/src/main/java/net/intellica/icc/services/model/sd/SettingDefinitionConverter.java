package net.intellica.icc.services.model.sd;

import com.fasterxml.jackson.databind.util.StdConverter;
import net.intellica.icc.services.model.SettingDefinition;

public class SettingDefinitionConverter extends StdConverter<String, SettingDefinition> {

	@Override
	public SettingDefinition convert(String value) {
		SettingDefinition result = new SettingDefinition();
		result.setId(value);
		return result;
	}

}

package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.intellica.icc.services.model.sd.IdConverter;
import net.intellica.icc.services.model.sd.SettingDefinitionConverter;
import net.intellica.icc.services.util.other.ServiceConstants;

@Entity
@Table(name = "ICC_SETTING")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Setting {

	private String id;
	private SettingDefinition definition;
	private String userId;
	private String value;
	private Date modificationDate;

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "DEFINITION_ID", nullable = false)
	@JsonSerialize(converter = IdConverter.class)
	@JsonDeserialize(converter = SettingDefinitionConverter.class)
	public SettingDefinition getDefinition() {
		return definition;
	}

	public void setDefinition(SettingDefinition definition) {
		this.definition = definition;
	}

	@Column(name = "USER_ID", nullable = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "VALUE", nullable = true, length = ServiceConstants.Model.LONG_STRING_LENGTH)
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Column(name = "MODIFICATION_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date date) {
		this.modificationDate = date;
	}

}

package net.intellica.icc.services.event;

public class QueueEvent {

	public static enum EventType {
		Add,
		Poll
	}

	private final EventType eventType;
	private final Object object;

	public QueueEvent(EventType eventType, Object object) {
		this.eventType = eventType;
		this.object = object;
	}

	public EventType getEventType() {
		return eventType;
	}

	public Object getObject() {
		return object;
	}

}

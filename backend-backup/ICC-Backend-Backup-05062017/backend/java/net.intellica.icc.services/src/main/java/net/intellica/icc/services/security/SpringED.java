package net.intellica.icc.services.security;

import javax.persistence.AttributeConverter;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.stereotype.Component;

@Component
public class SpringED implements AttributeConverter<String, String> {

	// TODO bu sekilde hardcoded tutulamaz
	private static final String key = "7ac670080b1dbe301cf0ddcd9dd0f2e6";
	private static final String salt = "02313c1b99876db2";

	@Override
	public String convertToDatabaseColumn(String attribute) {
		return Encryptors.text(key, salt).encrypt(attribute);
	}

	@Override
	public String convertToEntityAttribute(String dbData) {
		return Encryptors.text(key, salt).decrypt(dbData);
	}

}

package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import net.intellica.icc.services.model.sd.IdConverter;
import net.intellica.icc.services.model.sd.JobConverter;
import net.intellica.icc.services.model.sd.VariableDefinitionConverter;
import net.intellica.icc.services.util.other.ServiceConstants;
import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "ICC_VARIABLE")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Variable {

	private String id;
	private VariableDefinition definition;
	private String value;
	private Job job;

	public Variable() {
		definition = new VariableDefinition();
		value = "";
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "DEFINITION_ID", nullable = false)
	@JsonSerialize(converter = IdConverter.class)
	@JsonDeserialize(converter = VariableDefinitionConverter.class)
	public VariableDefinition getDefinition() {
		return definition;
	}

	public void setDefinition(VariableDefinition definition) {
		this.definition = definition;
	}

	@Lob
	@Column(name = "VALUE", nullable = true)
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value == null ? "" : value;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "JOB_ID", nullable = false)
	@JsonSerialize(converter = IdConverter.class)
	@JsonDeserialize(converter = JobConverter.class)
	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

}

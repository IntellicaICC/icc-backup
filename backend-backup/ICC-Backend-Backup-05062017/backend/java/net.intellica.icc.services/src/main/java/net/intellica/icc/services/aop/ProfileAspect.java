package net.intellica.icc.services.aop;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import net.intellica.icc.services.model.ProfileUpdate;
import net.intellica.icc.services.model.Setting;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.model.simple.FusedSetting;
import net.intellica.icc.services.security.BcryptUtil;
import net.intellica.icc.services.util.aop.FuseUtil;
import net.intellica.icc.services.util.aop.ValidationUtil;
import net.intellica.icc.services.util.handler.ResponseHandler;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import net.intellica.icc.template.exception.NotFoundException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Aspect
@Component
public class ProfileAspect {

	@Autowired private ApplicationContext context;
	@Autowired private ValidationUtil validationUtil;
	@Autowired private ResponseHandler responseHandler;
	@Autowired private SimpleExceptionHandler exceptionHandler;
	@Autowired private BcryptUtil bcryptUtil;
	@Autowired private FuseUtil fuseUtil;

	@Around("execution(* net.intellica.icc.services.controller.ProfileController.get(..))")
	public Object getProfile(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			result = pjp.proceed();
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.ProfileController.saveProfile(..))")
	public Object saveProfile(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			ProfileUpdate update = (ProfileUpdate)pjp.getArgs()[0];
			validationUtil.validateSave(update);
			if (!StringUtils.isEmpty(update.getNewPassword()))
				update.setNewPassword(bcryptUtil.getEncoder().encode(update.getNewPassword()));
			result = pjp.proceed();
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.ProfileController.listSettings(..))")
	public Object listProfileSettings(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			result = pjp.proceed();
			if (result == null)
				throw new NotFoundException();
			else {
				Collection<Setting> collection = (Collection<Setting>)result;
				List<FusedSetting> fused = collection.stream().map(c -> fuseUtil.fuse(c)).collect(Collectors.toList());
				result = fused;
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.ProfileController.saveSettings(..))")
	public Object saveProfileSettings(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			result = pjp.proceed();
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.ProfileController.resetSettings(..))")
	public Object resetProfileSettings(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			result = pjp.proceed();
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

}

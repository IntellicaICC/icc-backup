package net.intellica.icc.services.aop;

import net.intellica.icc.services.model.SettingDefinition;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.security.AuthorizationUtil;
import net.intellica.icc.services.service.SettingDefinitionService;
import net.intellica.icc.services.util.aop.PreSaveUtil;
import net.intellica.icc.services.util.aop.ValidationUtil;
import net.intellica.icc.services.util.handler.ResponseHandler;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import net.intellica.icc.template.exception.NotFoundException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Aspect
@Component
public class SettingAspect {

	@Autowired private ApplicationContext context;
	@Autowired private AuthorizationUtil authorizationUtil;
	@Autowired private ValidationUtil validationUtil;
	@Autowired private ResponseHandler responseHandler;
	@Autowired private SimpleExceptionHandler exceptionHandler;
	@Autowired private PreSaveUtil preSaveUtil;
	@Autowired private SettingDefinitionService settingDefinitionService;

	@Around("execution(* net.intellica.icc.services.controller.SettingController.list(..))")
	public Object listSettings(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			if (!authorizationUtil.isAdminUser())
				authorizationUtil.throwReadException();
			result = pjp.proceed();
			if (result == null)
				throw new NotFoundException();
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.SettingController.delete(..))")
	public Object deleteSetting(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			if (!authorizationUtil.isAdminUser())
				authorizationUtil.throwDeleteException();
			validationUtil.validateId((String)pjp.getArgs()[0]);
			String id = (String)pjp.getArgs()[0];
			SettingDefinition toDelete = settingDefinitionService.get(id);
			if (toDelete == null)
				throw new NotFoundException();
			else {
				validationUtil.validateDelete(toDelete);
				result = pjp.proceed();
			}
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.SettingController.get(..))")
	public Object getSetting(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			if (!authorizationUtil.isAdminUser())
				authorizationUtil.throwReadException();
			validationUtil.validateId((String)pjp.getArgs()[0]);
			result = pjp.proceed();
			if (result == null)
				throw new NotFoundException();
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.SettingController.save(..))")
	public Object saveSetting(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			if (!authorizationUtil.isAdminUser())
				authorizationUtil.throwSaveException();
			SettingDefinition definition = (SettingDefinition)pjp.getArgs()[0];
			validationUtil.validateSave(definition);
			SettingDefinition dbDefinition = StringUtils.isEmpty(definition.getId()) ? null : settingDefinitionService.get(definition.getId());
			preSaveUtil.fill(definition, dbDefinition);
			result = pjp.proceed();
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

}

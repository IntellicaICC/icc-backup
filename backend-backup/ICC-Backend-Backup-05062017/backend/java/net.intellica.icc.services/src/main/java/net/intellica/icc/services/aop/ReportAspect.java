package net.intellica.icc.services.aop;

import java.util.List;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.model.report.AuthorizationRoleObjectFilter;
import net.intellica.icc.services.model.report.AuthorizationRoleUserFilter;
import net.intellica.icc.services.model.report.AuthorizationUserObjectFilter;
import net.intellica.icc.services.model.report.AuthorizationUserRoleFilter;
import net.intellica.icc.services.security.AuthorizationUtil;
import net.intellica.icc.services.util.aop.ValidationUtil;
import net.intellica.icc.services.util.dev.PagingUtil;
import net.intellica.icc.services.util.handler.ResponseHandler;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ReportAspect {

	@Autowired private ApplicationContext context;
	@Autowired private AuthorizationUtil authorizationUtil;
	@Autowired private ValidationUtil validationUtil;
	@Autowired private ResponseHandler responseHandler;
	@Autowired private SimpleExceptionHandler exceptionHandler;
	@Autowired private PagingUtil pagingUtil;

	@Around("execution(* net.intellica.icc.services.controller.ReportController.authorizationRoleUser(..))")
	public Object reportAuthorizationRoleUser(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			AuthorizationRoleUserFilter filter = (AuthorizationRoleUserFilter)pjp.getArgs()[0];
			authorizationUtil.canViewReport(filter, true);
			validationUtil.validateFilter(filter);
			result = pjp.proceed();
			List<?> list = (List<?>)result;
			result = pagingUtil.getPaged(list, filter.getPage(), filter.getSize());
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.ReportController.authorizationUserRole(..))")
	public Object reportAuthorizationUserRole(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			AuthorizationUserRoleFilter filter = (AuthorizationUserRoleFilter)pjp.getArgs()[0];
			authorizationUtil.canViewReport(filter, true);
			validationUtil.validateFilter(filter);
			result = pjp.proceed();
			List<?> list = (List<?>)result;
			result = pagingUtil.getPaged(list, filter.getPage(), filter.getSize());
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.ReportController.authorizationRoleObject(..))")
	public Object reportAuthorizationRoleObject(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			AuthorizationRoleObjectFilter filter = (AuthorizationRoleObjectFilter)pjp.getArgs()[0];
			authorizationUtil.canViewReport(filter, true);
			validationUtil.validateFilter(filter);
			result = pjp.proceed();
			List<?> list = (List<?>)result;
			result = pagingUtil.getPaged(list, filter.getPage(), filter.getSize());
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

	@Around("execution(* net.intellica.icc.services.controller.ReportController.authorizationUserObject(..))")
	public Object reportAuthorizationUserObject(ProceedingJoinPoint pjp) throws Throwable {
		Object result;
		try {
			validationUtil.validateLicense();
			AuthorizationUserObjectFilter filter = (AuthorizationUserObjectFilter)pjp.getArgs()[0];
			authorizationUtil.canViewReport(filter, true);
			validationUtil.validateFilter(filter);
			result = pjp.proceed();
			List<?> list = (List<?>)result;
			result = pagingUtil.getPaged(list, filter.getPage(), filter.getSize());
			result = responseHandler.generate(result);
		}
		catch (Exception e) {
			result = exceptionHandler.handleResponse(e, context.getBean(User.class));
		}
		return result;
	}

}

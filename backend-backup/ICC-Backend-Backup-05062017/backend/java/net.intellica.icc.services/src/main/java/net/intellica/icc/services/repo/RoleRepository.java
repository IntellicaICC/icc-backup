package net.intellica.icc.services.repo;

import java.util.List;
import net.intellica.icc.services.model.Role;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends PagingAndSortingRepository<Role, String> {

	List<Role> findAllByPropsName(String name);

	List<Role> findAllByOrderByPropsNameAsc();

}

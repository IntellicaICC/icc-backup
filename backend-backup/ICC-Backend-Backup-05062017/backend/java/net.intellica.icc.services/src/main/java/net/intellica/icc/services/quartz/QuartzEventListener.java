package net.intellica.icc.services.quartz;

import java.util.Set;
import net.intellica.icc.services.model.JobPriority;
import net.intellica.icc.services.model.operation.InternalOperation;
import net.intellica.icc.services.util.dev.DateTimeUtil;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import org.quartz.CalendarIntervalScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerListener;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class QuartzEventListener implements SchedulerListener {

	@Autowired private SimpleExceptionHandler simpleExceptionHandler;
	@Autowired private DateTimeUtil dateTimeUtil;

	@Override
	public void jobScheduled(Trigger trigger) {
	}

	@Override
	public void jobUnscheduled(TriggerKey triggerKey) {
	}

	@Override
	public void triggerFinalized(Trigger trigger) {
	}

	@Override
	public void triggerPaused(TriggerKey triggerKey) {
	}

	@Override
	public void triggersPaused(String triggerGroup) {
	}

	@Override
	public void triggerResumed(TriggerKey triggerKey) {
	}

	@Override
	public void triggersResumed(String triggerGroup) {
	}

	@Override
	public void jobAdded(JobDetail jobDetail) {
	}

	@Override
	public void jobDeleted(JobKey jobKey) {
	}

	@Override
	public void jobPaused(JobKey jobKey) {
	}

	@Override
	public void jobsPaused(String jobGroup) {
	}

	@Override
	public void jobResumed(JobKey jobKey) {
	}

	@Override
	public void jobsResumed(String jobGroup) {
	}

	@Override
	public void schedulerError(String msg, SchedulerException cause) {
		simpleExceptionHandler.handleQuietly(cause, null); // TODO exception alan kullanici?
	}

	@Override
	public void schedulerInStandbyMode() {
	}

	@Override
	public void schedulerStarted() {
		try {
			Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
			Set<JobKey> jobKeys = scheduler.getJobKeys(GroupMatcher.jobGroupEquals(QuartzParameter.INTERNAL_GROUP));
			if (jobKeys.size() == 0) {
				createExtendJob(scheduler);
			}
		}
		catch (SchedulerException e) {
			simpleExceptionHandler.handleQuietly(e, null);
		}
	}

	private void createExtendJob(Scheduler scheduler) throws SchedulerException {
		// create keys
		JobKey jobKey = new JobKey(InternalOperation.EXTEND.toString(), QuartzParameter.INTERNAL_GROUP);
		TriggerKey triggerKey = new TriggerKey(InternalOperation.EXTEND.toString(), QuartzParameter.INTERNAL_GROUP);
		// create job
		JobDetail jobDetail = JobBuilder
			.newJob(QuartzInternalExtendJob.class)
			.withIdentity(jobKey)
			.build();
		// create trigger
		Trigger trigger = TriggerBuilder
			.newTrigger()
			.startAt(dateTimeUtil.getStartOfTomorrow())
			.withSchedule(CalendarIntervalScheduleBuilder.calendarIntervalSchedule().withIntervalInDays(1))
			.withIdentity(triggerKey)
			.withPriority(JobPriority.Low.ordinal())
			.build();
		scheduler.scheduleJob(jobDetail, trigger);
	}

	@Override
	public void schedulerStarting() {
	}

	@Override
	public void schedulerShutdown() {
	}

	@Override
	public void schedulerShuttingdown() {
	}

	@Override
	public void schedulingDataCleared() {
	}

}

package net.intellica.icc.services.model.report;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorizationRoleObjectFilter extends ReportFilter {

	private String roleId;
	private Byte rights;
	private Boolean includePassive;

	public AuthorizationRoleObjectFilter() {
		reportType = ReportType.AuthorizationRoleObject;
		rights = 0;
		includePassive = false;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public Byte getRights() {
		return rights;
	}

	public void setRights(Byte rights) {
		this.rights = rights;
	}

	public Boolean getIncludePassive() {
		return includePassive;
	}

	public void setIncludePassive(Boolean includePassive) {
		this.includePassive = includePassive;
	}

}

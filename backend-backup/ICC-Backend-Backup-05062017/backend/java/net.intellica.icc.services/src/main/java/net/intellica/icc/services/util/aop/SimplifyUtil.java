package net.intellica.icc.services.util.aop;

import net.intellica.icc.services.model.Connection;
import net.intellica.icc.services.model.Rights;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.model.operation.Operation;
import net.intellica.icc.services.model.simple.SimpleConnection;
import net.intellica.icc.services.model.simple.SimpleOperation;
import net.intellica.icc.services.model.simple.SimpleProperties;
import net.intellica.icc.services.model.simple.SimpleRights;
import net.intellica.icc.services.model.simple.SimpleUser;
import org.springframework.stereotype.Component;

@Component
public class SimplifyUtil {

	public SimpleConnection simplify(Connection connection) {
		SimpleConnection result = new SimpleConnection();
		result.setId(connection.getId());
		SimpleProperties properties = new SimpleProperties();
		properties.setName(connection.getProps().getName());
		result.setProps(properties);
		return result;
	}

	public SimpleUser simplify(User user) {
		SimpleUser result = new SimpleUser();
		result.setId(user.getId());
		result.setEmail(user.getEmail());
		result.setFirstName(user.getFirstName());
		result.setLastName(user.getLastName());
		SimpleProperties properties = new SimpleProperties();
		properties.setName(user.getProps().getName());
		result.setProps(properties);
		return result;
	}

	public SimpleRights simplify(Rights rights) {
		SimpleRights result = new SimpleRights();
		result.setId(rights.getId());
		result.setOwner(rights.getOwner());
		return result;
	}

	// detail payload yaratmasin diye ayri gonderiliyor
	public SimpleOperation simplify(Operation operation) {
		SimpleOperation result = new SimpleOperation();
		result.setId(operation.getId());
		result.setType(operation.getType());
		result.setModelId(operation.getModelId());
		result.setModelName(operation.getModelName());
		result.setSchedule(operation.getSchedule());
		result.setNextRunDate(operation.getNextRunDate());
		result.setSkipNextRun(operation.getSkipNextRun());
		result.setSummary(operation.getSummary());
		result.setStartLog(operation.getStartLog());
		result.setLastLog(operation.getLastLog());
		return result;
	}

}

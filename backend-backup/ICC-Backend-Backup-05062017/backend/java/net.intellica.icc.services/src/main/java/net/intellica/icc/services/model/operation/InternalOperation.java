package net.intellica.icc.services.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.Arrays;

public enum InternalOperation {

	EXTEND;

	@JsonCreator
	public static InternalOperation fromString(String string) {
		return Arrays.asList(InternalOperation.values()).stream().filter(s -> s.toString().equals(string)).findFirst().orElse(null);
	}

}

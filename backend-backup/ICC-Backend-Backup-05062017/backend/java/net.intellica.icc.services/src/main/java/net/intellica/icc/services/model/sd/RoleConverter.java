package net.intellica.icc.services.model.sd;

import com.fasterxml.jackson.databind.util.StdConverter;
import net.intellica.icc.services.model.Role;

public class RoleConverter extends StdConverter<String, Role> {

	@Override
	public Role convert(String value) {
		Role result = new Role();
		result.setId(value);
		return result;
	}

}

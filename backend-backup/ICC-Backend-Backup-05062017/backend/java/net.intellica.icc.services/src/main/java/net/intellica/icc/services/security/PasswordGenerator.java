package net.intellica.icc.services.security;

import java.util.Random;

public class PasswordGenerator {

	private static final Integer LENGTH = 8;
	private static final Integer LOWER_CASE_A = (int)'a';
	private static final Integer LOWER_CASE_Z = (int)'z';
	private static final Integer UPPER_CASE_A = (int)'A';
	private static final Integer UPPER_CASE_Z = (int)'Z';
	private static final Integer NUMBER_0 = (int)'0';
	private static final Integer NUMBER_9 = (int)'9';

	public static String generate() {
		String result = "";
		Long seed = System.nanoTime();
		for (int i = 0; i < LENGTH; i++) {
			Random random = new Random(seed);
			int category = random.nextInt(3);
			switch (category) {
				case 0:
					result += "" + (char)(random.nextInt(LOWER_CASE_Z - LOWER_CASE_A) + LOWER_CASE_A);
					break;
				case 1:
					result += "" + (char)(random.nextInt(UPPER_CASE_Z - UPPER_CASE_A) + UPPER_CASE_A);
					break;
				case 2:
					result += "" + (char)(random.nextInt(NUMBER_9 - NUMBER_0) + NUMBER_0);
					break;
				default:
					break;
			}
			seed = random.nextLong();
		}
		return result;
	}

}

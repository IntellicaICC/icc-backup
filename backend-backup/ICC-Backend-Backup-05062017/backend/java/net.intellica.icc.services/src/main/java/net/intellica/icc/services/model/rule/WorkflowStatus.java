package net.intellica.icc.services.model.rule;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.Arrays;

public enum WorkflowStatus {

	/**
	 * Workflow is not yet triggered.
	 */
	Idle,

	/**
	 * Workflow is executing.
	 */
	Executing,

	/**
	 * Workflow has executed successfully.
	 */
	Finished,

	/**
	 * Workflow is about to pause
	 */
	Pausing,

	/**
	 * Workflow is paused, waiting for the resume command.
	 */
	Paused,

	/**
	 * Workflow has encountered an error and halted execution.
	 */
	Error,

	/**
	 * Workflow is about to cancel all running jobs
	 */
	Canceling,

	/**
	 * Workflow execution is canceled by the user. 
	 */
	Canceled;

	@JsonCreator
	public static WorkflowStatus fromString(String string) {
		return Arrays.asList(WorkflowStatus.values()).stream().filter(s -> s.toString().equals(string)).findFirst().orElse(null);
	}

}

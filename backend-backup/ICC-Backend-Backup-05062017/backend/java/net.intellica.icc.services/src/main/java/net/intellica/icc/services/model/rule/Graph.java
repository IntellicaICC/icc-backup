package net.intellica.icc.services.model.rule;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.intellica.icc.services.util.dev.NodeStack;
import org.springframework.util.ObjectUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Graph {

	private Set<Node> nodes;
	private Map<Integer, Set<Integer>> links;
	private Map<Integer, Set<Integer>> trueLinks;
	private Map<Integer, Set<Integer>> falseLinks;

	public Graph() {
		nodes = new HashSet<>();
		links = new HashMap<>();
		trueLinks = new HashMap<>();
		falseLinks = new HashMap<>();
	}

	public Set<Node> getNodes() {
		return nodes;
	}

	public void setNodes(Set<Node> nodes) {
		this.nodes = nodes;
	}

	public Map<Integer, Set<Integer>> getLinks() {
		return links;
	}

	public void setLinks(Map<Integer, Set<Integer>> links) {
		this.links = links;
	}

	public Map<Integer, Set<Integer>> getTrueLinks() {
		return trueLinks;
	}

	public void setTrueLinks(Map<Integer, Set<Integer>> trueLinks) {
		this.trueLinks = trueLinks;
	}

	public Map<Integer, Set<Integer>> getFalseLinks() {
		return falseLinks;
	}

	public void setFalseLinks(Map<Integer, Set<Integer>> falseLinks) {
		this.falseLinks = falseLinks;
	}

	@JsonIgnore
	public Node getNode(Integer id) {
		return nodes.stream().filter(n -> id == n.getId()).findFirst().orElse(null);
	}

	@JsonIgnore
	public StartNode getStartNode() {
		return (StartNode)nodes.stream().filter(n -> n instanceof StartNode).findFirst().get();
	}

	public static Graph fromString(String value) throws Exception {
		Graph result = new ObjectMapper().readValue(value, Graph.class);
		result.nodes.forEach(n -> n.setGraph(result));
		return result;
	}

	@JsonIgnore
	public Boolean isCyclic() {
		Boolean result = false;
		try {
			NodeStack stack = new NodeStack();
			traverse(stack, getStartNode());
		}
		catch (Exception e) {
			result = true;
		}
		return result;
	}

	private void traverse(NodeStack stack, Node node) {
		stack.push(node.getId());
		if (node.getType() == NodeType.Decision) {
			DecisionNode decisionNode = (DecisionNode)node;
			for (Node trueOutNode : decisionNode.getTrueOutgoing())
				traverse(stack, trueOutNode);
			for (Node falseOutNode : decisionNode.getFalseOutgoing())
				traverse(stack, falseOutNode);
		}
		else {
			for (Node outNode : node.getOutgoing())
				traverse(stack, outNode);
		}
		stack.pop();
	}

	@Override
	public String toString() {
		String result = "";
		try {
			ObjectMapper mapper = new ObjectMapper();
			result = mapper.writeValueAsString(this);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		Boolean result = false;
		if (obj instanceof Graph) {
			Graph graph = (Graph)obj;
			result = ObjectUtils.nullSafeEquals(getNodes(), graph.getNodes()) &&
				ObjectUtils.nullSafeEquals(getLinks(), graph.getLinks()) &&
				ObjectUtils.nullSafeEquals(getTrueLinks(), graph.getTrueLinks()) &&
				ObjectUtils.nullSafeEquals(getFalseLinks(), graph.getFalseLinks());
		}
		return result;
	}

}
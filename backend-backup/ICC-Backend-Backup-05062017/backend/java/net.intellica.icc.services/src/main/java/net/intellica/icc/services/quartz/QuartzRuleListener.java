package net.intellica.icc.services.quartz;

import java.util.Map;
import net.intellica.icc.services.controller.OperationLogController;
import net.intellica.icc.services.event.EventType;
import net.intellica.icc.services.model.Rule;
import net.intellica.icc.services.model.User;
import net.intellica.icc.services.model.operation.Command;
import net.intellica.icc.services.model.operation.CommandLog;
import net.intellica.icc.services.model.operation.Operation;
import net.intellica.icc.services.model.operation.OperationStatus;
import net.intellica.icc.services.model.operation.OperationSummary;
import net.intellica.icc.services.model.operation.SkipReason;
import net.intellica.icc.services.model.work.WorkflowResult;
import net.intellica.icc.services.service.CommandLogService;
import net.intellica.icc.services.service.OperationService;
import net.intellica.icc.services.service.RuleService;
import net.intellica.icc.services.service.UserService;
import net.intellica.icc.services.util.handler.EventHandler;
import net.intellica.icc.services.util.handler.SimpleExceptionHandler;
import net.intellica.icc.services.util.other.ConversionUtil;
import net.intellica.icc.services.util.other.GraphUtil;
import net.intellica.icc.services.util.other.OperationUtil;
import net.intellica.icc.template.model.GlobalVariable;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class QuartzRuleListener implements JobListener {

	@Autowired private OperationService operationService;
	@Autowired private RuleService ruleService;
	@Autowired private UserService userService;
	@Autowired private EventHandler eventHandler;
	@Autowired private CommandLogService commandLogService;
	@Autowired private ConversionUtil conversionUtil;
	@Autowired private OperationLogController operationLogController;
	@Autowired private QuartzRuntimeUtil quartzRuntimeTimeUtil;
	@Autowired private SimpleExceptionHandler simpleExceptionHandler;
	@Autowired private OperationUtil operationUtil;
	@Autowired private GraphUtil graphUtil;

	public QuartzRuleListener() {
	}

	@Override
	public String getName() {
		return "QuartzRuleListener";
	}

	@Override
	public void jobToBeExecuted(JobExecutionContext context) {
		Map<String, String> parameters = (Map<String, String>)context.get(QuartzParameter.PARAMETERS);
//		CrossThreadUtil.put(GlobalVariable.UserID.toString(), parameters.get(GlobalVariable.UserID.toString()));
//		CrossThreadUtil.put(GlobalVariable.Username.toString(), parameters.get(GlobalVariable.Username.toString()));
		String ruleId = parameters.get(GlobalVariable.RuleID.toString());
		Rule rule = ruleService.get(ruleId, false);
		context.put(QuartzParameter.RULE_DETAIL, rule.getDetail());
		quartzRuntimeTimeUtil.fillParameters(parameters, rule);
		String instanceId = parameters.get(GlobalVariable.RuleInstanceID.toString());
		((QuartzRule)context.getJobInstance()).setInstanceId(instanceId);
		String operationId = parameters.get(GlobalVariable.OperationID.toString());
		markOperationStart(operationId, instanceId, rule.getDetail());
		User eventUser = userService.get(parameters.get(GlobalVariable.UserID.toString()), false);
		eventHandler.generateEvent(EventType.Start, rule, operationId, eventUser, eventUser);
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext context) {
		if (context.getTrigger().getJobDataMap().containsKey(QuartzParameter.SKIP_REASON)) {
			Map<String, String> parameters = (Map<String, String>)context.get(QuartzParameter.PARAMETERS);
			User eventUser = userService.get(parameters.get(GlobalVariable.UserID.toString()), false);
			String operationId = parameters.get(GlobalVariable.OperationID.toString());
			Operation operation = operationService.get(operationId, false);
			Rule rule = ruleService.get(operation.getModelId(), false);
			SkipReason reason = (SkipReason)context.getTrigger().getJobDataMap().get(QuartzParameter.SKIP_REASON);
			switch (reason) {
				case Collision:
					operation.getDetail().setErrorDetail("Skipped execution since already executing!");
					eventHandler.skipDueToCollision(operation, eventUser);
					break;
				case Inactive:
					operation.getDetail().setErrorDetail("Skipped execution since not active!");
					eventHandler.skipPassive(operation, eventUser);
					break;
				case Misfire:
					operation.getDetail().setErrorDetail("Missed the scheduled run time!");
					eventHandler.skipMissed(operation, eventUser);
					break;
				case Unauthorized:
					operation.getDetail().setErrorDetail("Operation skipped execution since user lost rights!");
					eventHandler.skipUnauthorized(operation, eventUser);
					break;
				case User:
					CommandLog skipCommand = commandLogService.getLast(operationId, Command.Skip);
					if (skipCommand != null) {
						User skipUser = userService.get(skipCommand.getUserId(), false);
						eventHandler.skipManual(operation, eventUser, skipUser);
					}
					break;
				default:
					break;
			}
			fillRuleDetail(operation, rule.getDetail());
			operation.getDetail().setFinalStatus(conversionUtil.convertDetailToLiveStatus(rule.getDetail()).toString());
			operation.setSummary(OperationSummary.Error);
			operationService.save(operation);
			operationLogController.save(operationId, OperationStatus.Skipped);
		}
	}

	@Override
	public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
		Map<String, String> parameters = (Map<String, String>)context.get(QuartzParameter.PARAMETERS);
		String operationId = parameters.get(GlobalVariable.OperationID.toString());
		User user = userService.get(parameters.get(GlobalVariable.UserID.toString()), false);
		Rule rule = ruleService.get(parameters.get(GlobalVariable.RuleID.toString()), false);
		if (jobException != null) {
			// engine/quartz hatasi
			simpleExceptionHandler.handleQuietly(jobException, user);
			WorkflowResult workResult = new WorkflowResult();
			workResult.setRuleDetail((String)context.get(QuartzParameter.RULE_DETAIL));
			workResult.setError(jobException);
			markOperationError(operationId, workResult);
			eventHandler.generateEvent(jobException, rule, operationId, null, user);
		}
		else {
			WorkflowResult workResult = (WorkflowResult)context.getResult();
			workResult.setRuleDetail((String)context.get(QuartzParameter.RULE_DETAIL));
			if (workResult.isCanceled()) {
				markOperationCancel(operationId, workResult);
				CommandLog cancelCommand = commandLogService.getLast(operationId, Command.Cancel);
				User cancelUser = userService.get(cancelCommand.getUserId(), false);
				eventHandler.generateEvent(EventType.Cancel, rule, operationId, cancelUser, user);
			}
			else if (workResult.isSuccessful()) {
				markOperationSuccess(operationId, workResult);
				eventHandler.generateEvent(EventType.Finish, rule, operationId, null, user);
			}
			else {
				// herhangi bir rule hatasi
				markOperationError(operationId, workResult);
				eventHandler.generateEvent(workResult.getError(), rule, operationId, null, user);
			}
		}
	}

	private void markOperationStart(String operationId, String instanceId, String detail) {
		Operation operation = operationService.get(operationId, false);
		operation.setInstanceId(instanceId);
		fillRuleDetail(operation, detail);
		operationService.save(operation);
		operationLogController.save(operationId, OperationStatus.Executing);
	}

	private void markOperationSuccess(String operationId, WorkflowResult workResult) {
		operationUtil.save(operationId, workResult);
		operationLogController.save(operationId, OperationStatus.Finished);
	}

	private void markOperationCancel(String operationId, WorkflowResult workResult) {
		operationUtil.save(operationId, workResult);
		operationLogController.save(operationId, OperationStatus.Canceled);
	}

	private void markOperationError(String operationId, WorkflowResult workResult) {
		operationUtil.save(operationId, workResult);
		operationLogController.save(operationId, OperationStatus.Error);
	}

	private void fillRuleDetail(Operation operation, String detail) {
		// job ve rule isimleri operasyon detayi icerisinde saklanir ve bir daha degismez
		operation.getDetail().setRuleDetail(graphUtil.fillNames(detail));
	}

}

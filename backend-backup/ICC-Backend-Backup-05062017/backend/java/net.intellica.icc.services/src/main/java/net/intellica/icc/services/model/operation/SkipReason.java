package net.intellica.icc.services.model.operation;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.Arrays;

public enum SkipReason {

	/**
	 * Operation missed its scheduled run time
	 */
	Misfire,

	/**
	 * Operation was inactive at its scheduled run time
	 */
	Inactive,

	/**
	 * Operation job/rule was already executing at its scheduled run time
	 */
	Collision,

	/**
	 * User manually skipped operation
	 */
	User,

	/**
	 * Execution rights have been revoked for the user on this object
	 */
	Unauthorized;

	@JsonCreator
	public static SkipReason fromString(String string) {
		return Arrays.asList(SkipReason.values()).stream().filter(s -> s.toString().equals(string)).findFirst().orElse(null);
	}

}

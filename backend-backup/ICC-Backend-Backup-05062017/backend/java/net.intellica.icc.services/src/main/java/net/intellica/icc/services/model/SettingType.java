package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.Arrays;

public enum SettingType {

	Standard,
	Boolean;

	@JsonCreator
	public static SettingType fromString(String string) {
		return Arrays.asList(SettingType.values()).stream().filter(s -> s.toString().equals(string)).findFirst().orElse(null);
	}

}

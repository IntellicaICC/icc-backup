package net.intellica.icc.services.model.operation;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.Map;
import net.intellica.icc.services.model.rule.NodeStatus;

public class OperationLiveStatus {

	private Map<Integer, NodeStatus> states;
	private Map<Integer, String> errors;

	public OperationLiveStatus() {
		states = new HashMap<>();
		errors = new HashMap<>();
	}

	public Map<Integer, NodeStatus> getStates() {
		return states;
	}

	public void setStates(Map<Integer, NodeStatus> states) {
		this.states = states;
	}

	public Map<Integer, String> getErrors() {
		return errors;
	}

	public void setErrors(Map<Integer, String> errors) {
		this.errors = errors;
	}

	public static OperationLiveStatus fromString(String value) throws Exception {
		return new ObjectMapper().readValue(value, OperationLiveStatus.class);
	}

	@Override
	public String toString() {
		String result = "";
		try {
			ObjectMapper mapper = new ObjectMapper();
			result = mapper.writeValueAsString(this);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}

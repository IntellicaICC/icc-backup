package net.intellica.icc.services.repo;

import java.util.List;
import net.intellica.icc.services.model.LocalParameter;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocalParameterRepository extends PagingAndSortingRepository<LocalParameter, String> {

	List<LocalParameter> findAllByPropsName(String name);

	List<LocalParameter> findAllByFolderIdOrderByPropsNameAsc(String id);

}

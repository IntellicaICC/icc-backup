package net.intellica.icc.services.model.simple;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Map;
import net.intellica.icc.services.model.Rights;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SimpleRights extends Rights {

	@Override
	@JsonIgnore
	public Map<String, Byte> getRoleRights() {
		return null;
	}

	@Override
	@JsonIgnore
	public Map<String, Byte> getUserRights() {
		return null;
	}

	@Override
	@JsonIgnore
	public Boolean getInheritsFromParent() {
		return null;
	}

}

package net.intellica.icc.services.model.report;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.Date;
import net.intellica.icc.services.model.sd.DateStringConverter;
import net.intellica.icc.services.util.other.ServiceConstants;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AuditLoginFilter {

	private String userId;
	private Date fromDate;
	private Date toDate;
	private Integer page;
	private Integer size;

	public AuditLoginFilter() {
		page = 0;
		size = ServiceConstants.Report.DEFAULT_SIZE;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@JsonDeserialize(converter = DateStringConverter.class)
	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	@JsonDeserialize(converter = DateStringConverter.class)
	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

}

package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.Arrays;

public enum NotificationSource {

	Job,
	Rule,
	Other;

	@JsonCreator
	public static NotificationSource fromString(String string) {
		return Arrays.asList(NotificationSource.values()).stream().filter(s -> s.toString().equals(string)).findFirst().orElse(null);
	}

}

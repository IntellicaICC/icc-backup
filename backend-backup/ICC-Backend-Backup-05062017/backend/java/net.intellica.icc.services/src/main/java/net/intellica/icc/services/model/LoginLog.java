package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import net.intellica.icc.services.util.other.ServiceConstants;

@Entity
@Table(name = "ICC_LOGIN_LOG")
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginLog {

	private String id;
	private String userId;
	private String userName;
	private Date attemptDate;
	private LoginFailureReason failureReason;

	public LoginLog() {
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "USER_ID", nullable = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "USERNAME", nullable = false, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "ATTEMPT_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getAttemptDate() {
		return attemptDate;
	}

	public void setAttemptDate(Date attemptDate) {
		this.attemptDate = attemptDate;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "FAILURE_REASON", nullable = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public LoginFailureReason getFailureReason() {
		return failureReason;
	}

	public void setFailureReason(LoginFailureReason failureReason) {
		this.failureReason = failureReason;
	}

}

package net.intellica.icc.services.model.rule;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import net.intellica.icc.services.util.dev.HashUtil;
import org.springframework.util.ObjectUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CommentNode extends Node {

	private static final long serialVersionUID = 1L;

	private String comment;

	public CommentNode() {
		type = NodeType.Decision;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	protected void doWork() {
		// no work
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj) &&
			ObjectUtils.nullSafeEquals(getComment(), ((CommentNode)obj).getComment());
	}

	@Override
	public int hashCode() {
		return HashUtil.generate(super.hashCode(), comment);
	}

}

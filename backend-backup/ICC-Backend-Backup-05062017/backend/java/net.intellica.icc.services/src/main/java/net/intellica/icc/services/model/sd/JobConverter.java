package net.intellica.icc.services.model.sd;

import com.fasterxml.jackson.databind.util.StdConverter;
import net.intellica.icc.services.model.Job;

public class JobConverter extends StdConverter<String, Job> {

	@Override
	public Job convert(String value) {
		Job result = new Job();
		result.setId(value);
		return result;
	}

}

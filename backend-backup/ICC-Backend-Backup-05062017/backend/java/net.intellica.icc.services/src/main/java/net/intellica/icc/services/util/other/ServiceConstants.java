package net.intellica.icc.services.util.other;

import java.util.Arrays;
import java.util.List;
import net.intellica.icc.services.model.VariableType;
import net.intellica.icc.services.model.operation.OperationStatus;

public abstract class ServiceConstants {

	public static abstract class Operation {

		public static final List<OperationStatus> COLLISION_STATES = Arrays.asList(
			OperationStatus.Executing,
			OperationStatus.Paused,
			OperationStatus.Pausing//
		);

		public static final String DATETIME_FORMAT = "yyyyMMdd";

	}

	public static abstract class Report {

		public static final Integer DEFAULT_SIZE = 100;
		public static final Integer MIN_SIZE = 20;
		public static final Integer MAX_SIZE = 1000;

	}

	public static abstract class Model {

		public static final int SHORT_STRING_LENGTH = 255;
		public static final int LONG_STRING_LENGTH = 2000;

	}

	public static abstract class Job {

		public static final List<VariableType> PARAMETRIC_TYPES = Arrays.asList(
			VariableType.Standard,
			VariableType.List,
			VariableType.SQL//
		);

	}

	public static abstract class Expression {

		public static final String EXPRESSION_PREFIX = "{";
		public static final String EXPRESSION_SUFFIX = "}";
		public static final List<String> JAVA_RESERVED_WORDS = Arrays.asList(
			"abstract",
			"assert",
			"boolean",
			"break",
			"byte",
			"case",
			"catch",
			"char",
			"class",
			"const",
			"continue",
			"default",
			"do",
			"double",
			"else",
			"enum",
			"extends",
			"false",
			"final",
			"finally",
			"float",
			"for",
			"goto",
			"if",
			"implements",
			"import",
			"instanceof",
			"int",
			"interface",
			"long",
			"native",
			"new",
			"null",
			"package",
			"private",
			"protected",
			"public",
			"return",
			"short",
			"static",
			"strictfp",
			"super",
			"switch",
			"synchronized",
			"this",
			"throw",
			"throws",
			"transient",
			"true",
			"try",
			"void",
			"volatile",
			"while"//
		);

	}

	public static abstract class Audit {

		public static final String LOGIN_URL = "/login";
		public static final String PROFILE_URL = "/profile/save";
		public static final String CONNECTION_URL = "/connection/save";

	}

	public static abstract class User {

		public static final String UNKNOWN_USER = "unknown";

	}

}

package net.intellica.icc.services.service;

import java.util.ArrayList;
import java.util.List;
import net.intellica.icc.services.model.Job;
import net.intellica.icc.services.repo.JobRepository;
import net.intellica.icc.services.util.audit.HistoryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JobService {

	@Autowired private JobRepository repository;
	@Autowired private HistoryUtil historyUtil;

	public JobService() {
	}

	public Job get(String id, Boolean includeHistory) {
		Job result = repository.findOne(id);
		if (includeHistory && result == null)
			result = historyUtil.getLastRevision(Job.class, id);
		return result;
	}

	public Job save(Job job) {
		return repository.save(job);
	}

	public void delete(String id) {
		repository.delete(id);
	}

	public List<Job> listAll() {
		List<Job> result = new ArrayList<>();
		repository.findAll().forEach(result::add);
		return result;
	}

	public List<Job> listByName(String name) {
		return repository.findAllByPropsName(name);
	}

	public List<Job> listByFolderId(String id) {
		return repository.findAllByFolderIdOrderByPropsNameAsc(id);
	}

	public List<Job> listAllIds() {
		List<Job> result = new ArrayList<>();
		repository.findDistinctIdBy().forEach(result::add);
		return result;
	}

}

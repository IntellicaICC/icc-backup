package net.intellica.icc.services.repo;

import java.util.List;
import net.intellica.icc.services.model.Folder;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FolderRepository extends PagingAndSortingRepository<Folder, String> {

	List<Folder> findAllByPropsName(String name);

	List<Folder> findAllByParentFolderIdOrderByPropsNameAsc(String id);

	Long countByParentFolderId(String id);

}

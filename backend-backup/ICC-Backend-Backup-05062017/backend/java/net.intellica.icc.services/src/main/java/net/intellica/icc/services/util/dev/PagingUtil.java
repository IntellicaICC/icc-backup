package net.intellica.icc.services.util.dev;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class PagingUtil {

	public <T> List<T> getPaged(List<T> list, Integer page, Integer size) {
		List<T> result = new ArrayList<>();
		Integer begin = page * size;
		Integer end = (page + 1) * size - 1;
		for (int i = 0; i < list.size(); i++) {
			if (i >= begin && i <= end)
				result.add(list.get(i));
		}
		return result;
	}

}

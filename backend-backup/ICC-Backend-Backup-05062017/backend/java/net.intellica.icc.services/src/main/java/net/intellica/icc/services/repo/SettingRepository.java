package net.intellica.icc.services.repo;

import java.util.List;
import net.intellica.icc.services.model.Setting;
import net.intellica.icc.services.model.SettingGroup;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SettingRepository extends CrudRepository<Setting, String> {

	List<Setting> findAllByUserIdOrderByDefinitionSettingGroupAscDefinitionPositionAsc(String userId);

	List<Setting> findAllByUserIdAndDefinitionSettingGroupOrderByDefinitionPositionAsc(String userId, SettingGroup group);

	void deleteAllByUserId(String userId);

}

package net.intellica.icc.services.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import net.intellica.icc.services.model.sd.FolderConverter;
import net.intellica.icc.services.model.sd.IdConverter;
import net.intellica.icc.services.util.other.ServiceConstants;
import org.hibernate.envers.Audited;

@Entity
@Audited
@Table(name = "ICC_RULE")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Rule {

	private String id;
	private CommonProperties props;
	private String detail;
	private String image;
	private Folder folder;
	private Rights rights;

	public Rule() {
		props = new CommonProperties();
		rights = new Rights();
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true, length = ServiceConstants.Model.SHORT_STRING_LENGTH)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Embedded
	public CommonProperties getProps() {
		return props;
	}

	public void setProps(CommonProperties props) {
		this.props = props;
	}

	@Lob
	@Column(name = "DETAIL", nullable = false)
	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	@Lob
	@Column(name = "IMAGE", nullable = true)
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FOLDER_ID", nullable = true)
	@JsonSerialize(converter = IdConverter.class)
	@JsonDeserialize(converter = FolderConverter.class)
	public Folder getFolder() {
		return folder;
	}

	public void setFolder(Folder folder) {
		this.folder = folder;
	}

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "RIGHTS_ID", nullable = false)
	public Rights getRights() {
		return rights;
	}

	public void setRights(Rights rights) {
		this.rights = rights;
	}

}

package net.intellica.icc.template.model;

public enum GlobalVariable {

	FolderID,
	FolderName,
	JobID,
	JobInstanceID,
	JobName,
	JobTriggerID,
	OperationID,
	RuleID,
	RuleInstanceID,
	RuleName,
	RuleTriggerID,
	TemplateID,
	TemplateName,
	UserID,
	Username

}

package net.intellica.icc.template.model;

public enum TemplateResult {

	AggregateValue,
	CompareCount,
	DifferentCount,
	DuplicateColumns,
	DuplicateCount,
	DuplicateExists,
	ExceedCount,
	FileExists,
	FileOverwritten,
	MatchCount,
	MaxAbsDifference,
	MaxDifference,
	MinAbsDifference,
	MinDifference,
	NullColumns,
	NullCount,
	NullExists,
	ReadCount,
	SameCount,
	TableCreated,
	TableDropped,
	TableExists,
	ThresholdExceeded,
	WriteCount

}

package net.intellica.icc.template.util;

import java.math.BigInteger;

public abstract class HashUtil {

	private static final Integer[] PRIMES = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541 };

	public static Integer generate(Object... values) {
		Integer result = 1;

		if (values == null || values.length > PRIMES.length)
			throw new IllegalArgumentException();

		BigInteger hash = BigInteger.ONE;
		for (Integer i = 0; i < values.length; i++) {
			Integer value = (values[i] == null ? 0 : (values[i] instanceof Integer ? (Integer)values[i] : values[i].hashCode()));
			hash = hash.multiply(BigInteger.valueOf(PRIMES[i])).add(BigInteger.valueOf(value));
		}
		result = hash.mod(BigInteger.valueOf(Integer.MAX_VALUE)).intValue();

		return result;
	}

}

package net.intellica.icc.template.exception;

import java.io.PrintWriter;

public abstract class SimpleRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SimpleRuntimeException() {
		super();
	}

	public SimpleRuntimeException(String message) {
		super(message);
	}

	public SimpleRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public SimpleRuntimeException(Throwable cause) {
		super(cause);
	}

	@Override
	public void printStackTrace(PrintWriter s) {
		s.print(this);
	}

}

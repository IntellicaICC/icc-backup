package net.intellica.icc.template.model;

@FunctionalInterface
public interface QueryLoggerFunction {

	public void log(String connection, String query);

}

package net.intellica.icc.template.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import net.intellica.icc.template.util.HashUtil;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@jsonId")
public class InstanceKey implements Serializable {

	private static final long serialVersionUID = 1L;

	private String mainInstanceId;
	private String subInstanceId;

	public InstanceKey() {
	}

	public InstanceKey(String mainInstanceId, String subInstanceId) {
		this.mainInstanceId = mainInstanceId == null ? "" : mainInstanceId;
		this.subInstanceId = subInstanceId == null ? "" : subInstanceId;
	}

	public String getMainInstanceId() {
		return mainInstanceId;
	}

	public void setMainInstanceId(String mainInstanceId) {
		this.mainInstanceId = mainInstanceId;
	}

	public String getSubInstanceId() {
		return subInstanceId;
	}

	public void setSubInstanceId(String subInstanceId) {
		this.subInstanceId = subInstanceId;
	}

	@Override
	public String toString() {
		return mainInstanceId + subInstanceId;
	}

	@Override
	public boolean equals(Object obj) {
		Boolean result = super.equals(obj);
		if (!result && obj instanceof InstanceKey) {
			InstanceKey key = (InstanceKey)obj;
			result = mainInstanceId.equals(key.mainInstanceId) &&
				subInstanceId.equals(key.subInstanceId);
		}
		return result;
	}

	@Override
	public int hashCode() {
		return HashUtil.generate(mainInstanceId, subInstanceId);
	}

}

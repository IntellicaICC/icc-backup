package net.intellica.icc.template.exception;

public class GridException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public GridException() {
		super();
	}

	public GridException(String message) {
		super(message);
	}

	public GridException(String message, Throwable cause) {
		super(message, cause);
	}

	public GridException(Throwable cause) {
		super(cause);
	}

}

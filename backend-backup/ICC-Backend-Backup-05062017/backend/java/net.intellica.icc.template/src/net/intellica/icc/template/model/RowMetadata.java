package net.intellica.icc.template.model;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.sql.JDBCType;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RowMetadata implements Externalizable {

	private List<String> columns;
	private List<JDBCType> types;
	private List<Integer> lengths;

	RowMetadata() {
		columns = new ArrayList<>();
		types = new ArrayList<>();
		lengths = new ArrayList<>();
	}

	RowMetadata(ResultSetMetaData metaData) throws SQLException {
		this();
		for (int i = 0; i < metaData.getColumnCount(); i++) {
			columns.add(metaData.getColumnName(i + 1));
			JDBCType type = JDBCType.valueOf(metaData.getColumnType(i + 1));
			types.add(type);
			switch (type) {
				case BIT:
				case TINYINT:
				case SMALLINT:
				case INTEGER:
				case BIGINT:
				case FLOAT:
				case REAL:
				case DOUBLE:
				case NUMERIC:
				case DECIMAL:
					lengths.add(metaData.getPrecision(i + 1));
					break;
				default:
					lengths.add(metaData.getColumnDisplaySize(i + 1));
					break;
			}
		}
	}

	RowMetadata(RowMetadata metaData) {
		columns = new ArrayList<>(metaData.columns);
		types = new ArrayList<>(metaData.types);
		lengths = new ArrayList<>(metaData.lengths);
	}

	RowMetadata addColumn(String name, JDBCType type, Integer length) {
		columns.add(name);
		types.add(type);
		lengths.add(length);
		return this;
	}

	public List<String> getColumns() {
		return new ArrayList<>(columns);
	}

	public List<JDBCType> getTypes() {
		return new ArrayList<>(types);
	}

	public List<Integer> getLengths() {
		return new ArrayList<>(lengths);
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeObject(columns);
		out.writeObject(types);
		out.writeObject(lengths);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		columns = (List)in.readObject();
		types = (List)in.readObject();
		lengths = (List)in.readObject();
	}

}

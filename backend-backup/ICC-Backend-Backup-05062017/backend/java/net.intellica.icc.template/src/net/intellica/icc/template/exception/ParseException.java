package net.intellica.icc.template.exception;

public class ParseException extends SimpleRuntimeException {

	private static final long serialVersionUID = 1L;

	public ParseException() {
		super();
	}

	public ParseException(String message) {
		super(message);
	}

	public ParseException(String message, Throwable cause) {
		super(message, cause);
	}

	public ParseException(Throwable cause) {
		super(cause);
	}

}

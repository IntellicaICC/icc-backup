package net.intellica.icc.template.model;

import java.sql.JDBCType;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import javax.cache.Cache;
import javax.cache.Cache.Entry;
import net.intellica.icc.template.exception.CanceledException;
import net.intellica.icc.template.exception.DatabaseException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteAtomicLong;
import org.apache.ignite.IgniteAtomicReference;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.IgniteException;
import org.apache.ignite.IgniteQueue;
import org.apache.ignite.cache.CachePeekMode;
import org.apache.ignite.cache.query.ScanQuery;
import org.apache.ignite.compute.ComputeJob;
import org.apache.ignite.configuration.CollectionConfiguration;
import org.apache.ignite.lang.IgniteBiPredicate;
import org.apache.ignite.lang.IgniteFutureCancelledException;
import org.apache.ignite.resources.IgniteInstanceResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

public abstract class TemplateJob implements ComputeJob {

	// CONSTANTS
	private static final long serialVersionUID = 1L;
	private static final String COLUMN_REGEX = "(\\w+)";

	// INFRASTRUCTURE
	@IgniteInstanceResource private Ignite ignite;
	private Boolean canceled;

	// TEMPLATE-RELATED
	private final Log LOG;
	private Map<String, String> variables;
	private Map<String, String> parameters;
	private Map<String, DatabaseUtil> databaseUtils;
	private Map<String, String> connectionNames;
	private String datasetName;
	private String resultsetName;
	private String querysetName;
	private String ruleInstanceId;
	private String jobInstanceId;
	private JavaMailSenderImpl mailSender;
	private List<String> instanceChain;
	private String lastDataset;

	// UTILITY
	public static final List<JDBCType> JDBC_NUMERICS = Arrays.asList(new JDBCType[] {
		JDBCType.BIT,
		JDBCType.TINYINT,
		JDBCType.SMALLINT,
		JDBCType.INTEGER,
		JDBCType.BIGINT,
		JDBCType.FLOAT,
		JDBCType.REAL,
		JDBCType.DOUBLE,
		JDBCType.NUMERIC,
		JDBCType.DECIMAL// 
	});

	public static final List<JDBCType> JDBC_CHARS = Arrays.asList(new JDBCType[] {
		JDBCType.CHAR,
		JDBCType.LONGNVARCHAR,
		JDBCType.LONGVARCHAR,
		JDBCType.NCHAR,
		JDBCType.NVARCHAR,
		JDBCType.VARCHAR//
	});

	public static final List<JDBCType> JDBC_DATES = Arrays.asList(new JDBCType[] {
		JDBCType.DATE,
		JDBCType.TIME,
		JDBCType.TIME_WITH_TIMEZONE,
		JDBCType.TIMESTAMP,
		JDBCType.TIMESTAMP_WITH_TIMEZONE//
	});

	public TemplateJob(Object arg) {
		LOG = LogFactory.getLog(getClass());
		variables = new HashMap<>();
		parameters = new HashMap<>();
		connectionNames = new HashMap<>();
		databaseUtils = new HashMap<>();
		canceled = false;
		WorkItem work = (WorkItem)arg;
		if (!CollectionUtils.isEmpty(work.getLinks())) {
			for (String id : work.getLinks().keySet()) {
				connectionNames.put(id, work.getLinks().get(id).getName());
				DriverManagerDataSource dataSource = new DriverManagerDataSource();
				dataSource.setUrl(work.getLinks().get(id).getUrl());
				dataSource.setUsername(work.getLinks().get(id).getUsername());
				dataSource.setPassword(work.getLinks().get(id).getPassword());
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				jdbcTemplate.setFetchSize(100);
				if (dataSource.getUrl().startsWith("jdbc:oracle:"))
					databaseUtils.put(id, new OracleUtil(jdbcTemplate));
				else if (dataSource.getUrl().startsWith("jdbc:hive2:"))
					databaseUtils.put(id, new HiveUtil(jdbcTemplate));
				else if (dataSource.getUrl().startsWith("jdbc:teradata:"))
					databaseUtils.put(id, new TeradataUtil(jdbcTemplate));
				else
					throw new DatabaseException("The JDBC url given in connection '" + connectionNames.get(id) + "' is not supported!");
			}
		}
		if (!CollectionUtils.isEmpty(work.getArguments()))
			for (String variable : work.getArguments().keySet()) {
				setVariable(variable, work.getArguments().get(variable));
			}
		if (!CollectionUtils.isEmpty(work.getParameters()))
			for (String parameter : work.getParameters().keySet())
				setParameter(parameter, work.getParameters().get(parameter));
		ruleInstanceId = getParameter(GlobalVariable.RuleInstanceID.toString());
		jobInstanceId = getParameter(GlobalVariable.JobInstanceID.toString());
		datasetName = getParameter(GlobalVariable.JobID.toString());
		Long extension = System.nanoTime();
		resultsetName = datasetName + InternalVariable.CACHE_SEPARATOR + InternalVariable.RESULTSET_PREFIX + extension;
		querysetName = datasetName + InternalVariable.CACHE_SEPARATOR + InternalVariable.QUERYSET_PREFIX + extension;
		mailSender = new JavaMailSenderImpl();
		mailSender.setHost(work.getMailParameters().get(Constants.Email.HOST));
		mailSender.setProtocol(work.getMailParameters().get(Constants.Email.PROTOCOL));
		mailSender.setUsername(work.getMailParameters().get(Constants.Email.USERNAME));
		mailSender.setPassword(work.getMailParameters().get(Constants.Email.PASSWORD));
		instanceChain = work.getInstanceChain();
		lastDataset = work.getLastDataset();
	}

	protected final String getVariable(String name) {
		return variables.get(name);
	}

	private void setVariable(String name, String value) {
		variables.put(name, value);
	}

	protected final String getParameter(String name) {
		return parameters.get(name);
	}

	private void setParameter(String name, String value) {
		parameters.put(name, value);
	}

	protected final void createCache() {
		createCache(datasetName);
	}

	protected final void createResultCache() {
		createCache(resultsetName);
	}

	protected final void createQueryCache() {
		clearQueryCache();
		CollectionConfiguration configuration = new CollectionConfiguration();
		configuration.setCollocated(true);
		String gridName = getCacheName(querysetName);
		ignite.queue(gridName, 0, configuration);
	}

	protected final void createCache(String name) {
		if (!StringUtils.isEmpty(name)) {
			String gridName = getCacheName(name);
			if (ignite.cache(gridName) != null)
				clearCache(gridName);
//			ignite.getOrCreateCache(gridName);
			ignite.createCache(gridName);
		}
	}

	protected final Object getFromCache(Object key) {
		return getFromCache(datasetName, key);
	}

	protected final Object getFromResultCache(Object key) {
		return getFromCache(resultsetName, key);
	}

	protected final Object getFromCache(String cache, Object key) {
		Object result = null;
		if (!StringUtils.isEmpty(cache)) {
			String gridCache = getCacheName(cache);
			IgniteCache<Object, Object> cacheObject = ignite.cache(gridCache);
			if (cacheObject == null)
				cacheObject = findCache(cache);
			if (cacheObject != null)
				result = cacheObject.get(key);
		}
		return result;
	}

	protected final Object getSampleFromCache() {
		return getSampleFromCache(datasetName);
	}

	protected final Object getSampleFromCache(String cache) {
		Object result = null;
		if (!StringUtils.isEmpty(cache)) {
			String gridCache = getCacheName(cache);
			IgniteCache<Object, Object> cacheObject = ignite.cache(gridCache);
			if (cacheObject == null)
				cacheObject = findCache(cache);
			if (cacheObject != null) {
				Iterator<Entry<Object, Object>> iterator = cacheObject.iterator();
				if (iterator.hasNext())
					result = iterator.next().getValue();
			}
		}
		return result;
	}

	protected final Boolean cacheExists(String cache) {
		Boolean result = false;
		if (!StringUtils.isEmpty(cache)) {
			String gridCache = getCacheName(cache);
			IgniteCache<Object, Object> cacheObject = ignite.cache(gridCache);
			if (cacheObject == null)
				cacheObject = findCache(cache);
			result = cacheObject != null;
		}
		return result;
	}

	protected final Boolean cacheKeyExists(String key) {
		return cacheKeyExists(datasetName, key);
	}

	protected final Boolean cacheKeyExists(String cache, String key) {
		Boolean result = false;
		if (!StringUtils.isEmpty(cache)) {
			String gridCache = getCacheName(cache);
			IgniteCache<Object, Object> cacheObject = ignite.cache(gridCache);
			if (cacheObject == null)
				cacheObject = findCache(cache);
			if (cacheObject != null)
				result = cacheObject.containsKey(key);
		}
		return result;
	}

	protected final void putIntoCache(Object key, Object value) {
		putIntoCache(datasetName, key, value);
	}

	protected final void putIntoResultCache(Object key, Object value) {
		putIntoCache(resultsetName, key, value);
	}

	protected final void putIntoQueryCache(QueryLog log) {
		String gridName = getCacheName(querysetName);
		IgniteQueue<Object> queue = ignite.queue(gridName, 0, null);
		if (queue != null)
			queue.add(log);
	}

	protected final void putIntoCache(String cache, Object key, Object value) {
		if (!StringUtils.isEmpty(cache)) {
			String gridCache = getCacheName(cache);
			IgniteCache<Object, Object> cacheObject = ignite.cache(gridCache);
			if (cacheObject != null)
				cacheObject.put(key, value);
		}
	}

	protected final void removeFromCache(Object key) {
		removeFromCache(datasetName, key);
	}

	protected final void removeFromResultCache(Object key) {
		removeFromCache(resultsetName, key);
	}

	protected final void removeFromCache(String cache, Object key) {
		if (!StringUtils.isEmpty(cache)) {
			String gridCache = getCacheName(cache);
			IgniteCache<Object, Object> cacheObject = ignite.cache(gridCache);
			if (cacheObject != null) {
				ignite.cache(gridCache).remove(key);
			}
		}
	}

	protected final void createGridLong(String name, Long initialValue) {
		if (!StringUtils.isEmpty(name))
			ignite.atomicLong(getVariableName(name), initialValue, true);
	}

	protected final Long getGridLong(String name) {
		Long result = null;
		if (!StringUtils.isEmpty(name)) {
			String gridName = getVariableName(name);
			IgniteAtomicLong atomicLong = ignite.atomicLong(gridName, 0, false);
			if (atomicLong == null)
				atomicLong = findLongVariable(name);
			if (atomicLong != null)
				result = atomicLong.get();
		}
		return result;
	}

	protected final Long getAndIncrementGridLong(String name) {
		Long result = null;
		if (!StringUtils.isEmpty(name)) {
			IgniteAtomicLong atomicLong = ignite.atomicLong(getVariableName(name), 0, false);
			if (atomicLong != null)
				result = atomicLong.getAndIncrement();
		}
		return result;
	}

	protected final Long incrementAndGetGridLong(String name) {
		Long result = null;
		if (!StringUtils.isEmpty(name)) {
			IgniteAtomicLong atomicLong = ignite.atomicLong(getVariableName(name), 0, false);
			if (atomicLong != null)
				result = atomicLong.incrementAndGet();
		}
		return result;
	}

	protected final void removeGridLong(String name) {
		if (!StringUtils.isEmpty(name)) {
			IgniteAtomicLong atomicLong = ignite.atomicLong(getVariableName(name), 0, false);
			if (atomicLong != null)
				atomicLong.close();
		}
	}

	protected final void createGridVariable(String name, Object initialValue) {
		if (!StringUtils.isEmpty(name))
			ignite.atomicReference(getVariableName(name), initialValue, true);
	}

	protected final Object getGridVariable(String name) {
		Object result = null;
		if (!StringUtils.isEmpty(name)) {
			String gridName = getVariableName(name);
			IgniteAtomicReference<Object> atomicReference = ignite.atomicReference(gridName, null, false);
			if (atomicReference == null)
				atomicReference = findReferenceVariable(name);
			if (atomicReference != null)
				result = atomicReference.get();
		}
		return result;
	}

	protected final synchronized void setGridVariable(String name, Function<Object, Object> setter) {
		if (!StringUtils.isEmpty(name)) {
			IgniteAtomicReference<Object> atomicReference = ignite.atomicReference(getVariableName(name), null, false);
			if (atomicReference != null)
				atomicReference.set(setter.apply(atomicReference.get()));
		}
	}

	protected final void removeGridVariable(String name) {
		if (!StringUtils.isEmpty(name)) {
			IgniteAtomicReference<Object> atomicReference = ignite.atomicReference(getVariableName(name), null, false);
			if (atomicReference != null)
				atomicReference.close();
		}
	}

	protected final Iterator<Entry<Object, Object>> iterateCache() {
		return iterateCache(datasetName);
	}

	protected final Iterator<Entry<Object, Object>> iterateResultCache() {
		return iterateCache(resultsetName);
	}

	protected final Iterator<Entry<Object, Object>> iterateCache(String cache) {
		Iterator<Entry<Object, Object>> result = new EmptyIterator();
		if (!StringUtils.isEmpty(cache)) {
			String gridCache = getCacheName(cache);
			IgniteCache<Object, Object> cacheObject = ignite.cache(gridCache);
			if (cacheObject == null)
				cacheObject = findCache(cache);
			if (cacheObject != null)
				result = cacheObject.iterator();
		}
		return result;
	}

	protected final Iterable<Entry<Object, Object>> queryCache(IgniteBiPredicate<Object, Object> predicate) {
		return queryCache(datasetName, predicate);
	}

	protected final Iterable<Entry<Object, Object>> queryResultCache(IgniteBiPredicate<Object, Object> predicate) {
		return queryCache(resultsetName, predicate);
	}

	protected final Iterable<Entry<Object, Object>> queryCache(String cache, IgniteBiPredicate<Object, Object> predicate) {
		Iterable<Entry<Object, Object>> result = new ArrayList<>();
		if (!StringUtils.isEmpty(cache)) {
			String gridCache = getCacheName(cache);
			IgniteCache<Object, Object> cacheObject = ignite.cache(gridCache);
			if (cacheObject == null)
				cacheObject = findCache(cache);
			if (cacheObject != null) {
//				IgniteBiPredicate<Object, Object> ignitePredicate = (k, v) -> predicate.test(k, v);
				result = cacheObject.query(new ScanQuery<>(predicate));
			}
		}
		return result;
	}

	final List<QueryLog> getQueryCache() {
		List<QueryLog> result = new ArrayList<>();
		List<? super Object> list = new ArrayList<>();
		String gridName = getCacheName(querysetName);
		ignite.queue(gridName, 0, null).drainTo(list);
		list.forEach(q -> result.add((QueryLog)q));
		return result;
	}

	protected final void localCacheRun(CacheProcessor processor) {
		localCacheRun(datasetName, processor);
	}

	protected final void localCacheRun(String cache, CacheProcessor processor) {
		if (!StringUtils.isEmpty(cache)) {
			String gridCache = getCacheName(cache);
			IgniteCache<Object, Object> cacheObject = ignite.cache(gridCache);
			if (cacheObject == null)
				cacheObject = findCache(cache);
			if (cacheObject != null && processor != null) {
				IgniteCache<Object, Object> foundCache = cacheObject;
				ignite.compute().broadcast(() -> processor.process(foundCache.localEntries()));
			}
		}
	}

	protected final void clearCache() {
		clearCache(datasetName);
	}

	protected final void clearResultCache() {
		clearCache(resultsetName);
	}

	protected final void clearQueryCache() {
		String gridName = getCacheName(querysetName);
		IgniteQueue<Object> queue = ignite.queue(gridName, 0, null);
		if (queue != null)
			ignite.queue(gridName, 0, null).close();
	}

	protected final void clearCache(String cache) {
		if (!StringUtils.isEmpty(cache)) {
			String gridCache = getCacheName(cache);
			IgniteCache<Object, Object> cacheObject = ignite.cache(gridCache);
			if (cacheObject != null) {
				// TODO memory leak
				cacheObject.removeAll();
				cacheObject.destroy();
//				ignite.destroyCache(gridCache);
//				ignite.createCache(gridCache);
//				ignite.destroyCache(gridCache);
			}
		}
	}

	protected final Integer countCache() {
		return countCache(datasetName);
	}

	protected final Integer countResultCache() {
		return countCache(resultsetName);
	}

	protected final Integer countCache(String cache) {
		Integer result = 0;
		if (!StringUtils.isEmpty(cache)) {
			String gridCache = getCacheName(cache);
			IgniteCache<Object, Object> cacheObject = ignite.cache(gridCache);
			if (cacheObject == null)
				cacheObject = findCache(cache);
			if (cacheObject != null)
				result = cacheObject.size(CachePeekMode.PRIMARY);
		}
		return result;
	}

	protected final List<Row> query(String connection, String query) {
		return databaseUtils.get(connection).query(connection, this::logQuery, this::checkCanceled, query);
	}

	protected final <T> T queryScalar(String connection, String query, Class<T> clazz) {
		return databaseUtils.get(connection).queryScalar(connection, this::logQuery, query, clazz);
	}

	protected final void execute(String connection, String query) {
		databaseUtils.get(connection).execute(connection, this::logQuery, query);
	}

	protected final void executePrepared(String connection, String query, Object... args) {
		databaseUtils.get(connection).executePrepared(connection, this::logQuery, query, args);
	}

	protected final Long queryCount(String connection, String table) {
		return databaseUtils.get(connection).queryCount(connection, this::logQuery, table);
	}

	protected final void iterateQuery(String connection, String query, Consumer<Row> rowHandler) {
		databaseUtils.get(connection).iterateQuery(connection, this::logQuery, this::checkCanceled, query, rowHandler);
	}

	protected final void dropTable(String connection, String table) {
		databaseUtils.get(connection).dropTable(connection, this::logQuery, table);
	}

	protected final void createTable(String connection, String table, Row row) {
		databaseUtils.get(connection).createTable(connection, this::logQuery, table, row);
	}

	protected final void insertToTable(String connection, String table, Row row) {
		databaseUtils.get(connection).insertToTable(connection, this::logQuery, table, row);
	}

	protected final void batchInsertToTable(String connection, String table, Row... rows) {
		databaseUtils.get(connection).batchInsertToTable(connection, this::logQuery, this::checkCanceled, table, rows);
	}

	protected final void validateTableName(String connection, String table) {
		databaseUtils.get(connection).validateTableName(table);
	}

	protected final void validateQualifiedTableName(String connection, String table) {
		databaseUtils.get(connection).validateQualifiedTableName(table);
	}

	protected final void validateColumnName(String connection, String column) {
		databaseUtils.get(connection).validateColumnName(column);
	}

	protected final String getDatasetName() {
		return datasetName;
	}

	protected final String getResultsetName() {
		return resultsetName;
	}

	protected final String getQuerysetName() {
		return querysetName;
	}

	protected final void waitFor(Integer ms) {
		try {
			Thread.sleep(ms);
		}
		catch (Exception e) {
		}
	}

	private void logQuery(String connection, String query) {
		logDebugMessage(query.replaceAll("[\\r\\n]+", " "));
		QueryLog log = new QueryLog();
		log.setJobId(getParameter(GlobalVariable.JobID.toString()));
		log.setJobName(getParameter(GlobalVariable.JobName.toString()));
		log.setTemplateName(getParameter(GlobalVariable.TemplateName.toString()));
		log.setTimestamp(new Date());
		log.setConnectionId(connection);
		log.setConnectionName(connectionNames.get(connection));
		log.setQuery(query);
		putIntoQueryCache(log);
	}

	protected final void logInfoMessage(String message) {
		LOG.info(createLogMessage(message));
	}

	protected final void logInfoMessage(String message, Object... args) {
		LOG.info(createLogMessage(String.format(message, args)));
	}

	protected final void logInfoMessage(String message, Throwable t) {
		LOG.info(createLogMessage(message), t);
	}

	protected final void logInfoMessage(String message, Throwable t, Object... args) {
		LOG.info(createLogMessage(String.format(message, args)), t);
	}

	protected final void logDebugMessage(String message) {
		LOG.debug(createLogMessage(message));
	}

	protected final void logDebugMessage(String message, Object... args) {
		LOG.debug(createLogMessage(String.format(message, args)));
	}

	protected final void logDebugMessage(String message, Throwable t) {
		LOG.debug(createLogMessage(message), t);
	}

	protected final void logDebugMessage(String message, Throwable t, Object... args) {
		LOG.debug(createLogMessage(String.format(message, args)), t);
	}

	protected final void logWarningMessage(String message) {
		LOG.warn(createLogMessage(message));
	}

	protected final void logWarningMessage(String message, Object... args) {
		LOG.warn(createLogMessage(String.format(message, args)));
	}

	protected final void logWarningMessage(String message, Throwable t) {
		LOG.warn(createLogMessage(message), t);
	}

	protected final void logWarningMessage(String message, Throwable t, Object... args) {
		LOG.warn(createLogMessage(String.format(message, args)), t);
	}

	protected final void logErrorMessage(String message) {
		LOG.error(createLogMessage(message));
	}

	protected final void logErrorMessage(String message, Object... args) {
		LOG.error(createLogMessage(String.format(message, args)));
	}

	protected final void logErrorMessage(String message, Throwable t) {
		LOG.error(createLogMessage(message), t);
	}

	protected final void logErrorMessage(String message, Throwable t, Object... args) {
		LOG.error(createLogMessage(String.format(message, args)), t);
	}

	protected final void logFatalMessage(String message) {
		LOG.fatal(createLogMessage(message));
	}

	protected final void logFatalMessage(String message, Object... args) {
		LOG.fatal(createLogMessage(String.format(message, args)));
	}

	protected final void logFatalMessage(String message, Throwable t) {
		LOG.fatal(createLogMessage(message), t);
	}

	protected final void logFatalMessage(String message, Throwable t, Object... args) {
		LOG.fatal(createLogMessage(String.format(message, args)), t);
	}

	protected final void logTraceMessage(String message) {
		LOG.trace(createLogMessage(message));
	}

	protected final void logTraceMessage(String message, Object... args) {
		LOG.trace(createLogMessage(String.format(message, args)));
	}

	protected final void logTraceMessage(String message, Throwable t) {
		LOG.trace(createLogMessage(message), t);
	}

	protected final void logTraceMessage(String message, Throwable t, Object... args) {
		LOG.trace(createLogMessage(String.format(message, args)), t);
	}

	private String createLogMessage(String message) {
		String result = "Template %s [jobId=%s]: %s";
		String templateName = getParameter(GlobalVariable.TemplateName.toString());
		String jobId = getParameter(GlobalVariable.JobID.toString());
		result = String.format(result, templateName, jobId, message);
		return result;
	}

	@Override
	public final Object execute() throws IgniteException {
		WorkItemResult result = new WorkItemResult();
		IgniteException exception = null;
		logInfoMessage("Started execution.");
		Instant start = Instant.now();
		try {
			validatialization();
			createCache();
			createResultCache();
			createQueryCache();
			run();
			setResults();
			mapResults(result.getResults());
			mapResultTypes(result.getResultTypes());
			result.setQueryLogs(getQueryCache());
		}
		catch (CanceledException e) {
			postCancel();
			exception = new IgniteFutureCancelledException("Job is cancelled!");
		}
		catch (Exception e) {
			logErrorMessage("An error occurred while executing job!", e);
			exception = new IgniteException(e);
		}
		finally {
			if (!result.isSuccessful())
				clearCache();
			clearResultCache();
			clearQueryCache();
			cleanUp();
			String duration = measureDuration(start);
			logInfoMessage("Finished execution. Total time=" + duration);
		}
		if (exception != null)
			throw exception;
		return result;
	}

	private String measureDuration(Instant start) {
		String result = "";
		Duration duration = Duration.between(start, Instant.now());
		result += duration.toHours() > 0L ? String.format("%02d", duration.toHours()) + ":" : "00:";
		result += duration.toMinutes() > 0L ? String.format("%02d", duration.toMinutes()) + ":" : "00:";
		result += String.format("%02d", (duration.getSeconds() % 60L));
		return result;
	}

	private void mapResults(Map<String, Object> results) {
		iterateResultCache().forEachRemaining(e -> results.put((String)e.getKey(), e.getValue()));
	}

	private void mapResultTypes(Map<String, String> resultTypes) {
		Iterator<Entry<Object, Object>> iterator = iterateResultCache();
		while (iterator.hasNext()) {
			Cache.Entry<Object, Object> entry = (Cache.Entry<Object, Object>)iterator.next();
			String valueType = entry.getValue() == null ? null : entry.getValue().getClass().getName();
			resultTypes.put((String)entry.getKey(), valueType);
		}
	}

	@Override
	public void cancel() {
		canceled = true;
	}

	/**
	 * validation & initialization
	 * @throws Exception
	 */
	protected void validatialization() throws Exception {
	}

	protected abstract void run() throws Exception;

	protected void postCancel() {
	}

	protected void setResults() {
	}

	public Boolean isCanceled() {
		return canceled;
	}

	public void checkCanceled() {
		if (canceled)
			throw new CanceledException();
	}

	private String getCacheName(String name) {
		return StringUtils.isEmpty(ruleInstanceId) ? name : ruleInstanceId + InternalVariable.CACHE_SEPARATOR + name;
	}

	private IgniteCache<Object, Object> findCache(String name) {
		IgniteCache<Object, Object> result = null;
		// Rule-Level caches
		for (int i = 0; i < instanceChain.size(); i++) {
			String cache = instanceChain.get(instanceChain.size() - 1 - i) + InternalVariable.CACHE_SEPARATOR + name;
			result = ignite.cache(cache);
			if (result != null)
				break;
		}
		// Job-Level caches
		if (result == null) {
			logDebugMessage("No rule-level cache named '" + name + "' exists! Trying job-level.");
			result = ignite.cache(name);
		}
		return result;
	}

	private String getVariableName(String name) {
		return StringUtils.isEmpty(ruleInstanceId) ? name : ruleInstanceId + InternalVariable.VARIABLE_SEPARATOR + jobInstanceId + InternalVariable.VARIABLE_SEPARATOR + name;
	}

	private IgniteAtomicLong findLongVariable(String name) {
		IgniteAtomicLong result = null;
		// Rule Level variables
		for (int i = 0; i < instanceChain.size(); i++) {
			String cache = instanceChain.get(instanceChain.size() - 1 - i) + InternalVariable.CACHE_SEPARATOR + name;
			result = ignite.atomicLong(cache, 0, false);
			if (result != null)
				break;
		}
		// Job Level variables
		if (result == null) {
			logDebugMessage("No rule-level variable named '" + name + "' exists! Trying job-level.");
			result = ignite.atomicLong(name, 0, false);
		}
		return result;
	}

	private IgniteAtomicReference<Object> findReferenceVariable(String name) {
		IgniteAtomicReference<Object> result = null;
		// Rule Level variables
		for (int i = 0; i < instanceChain.size(); i++) {
			String cache = instanceChain.get(instanceChain.size() - 1 - i) + InternalVariable.CACHE_SEPARATOR + name;
			result = ignite.atomicReference(cache, null, false);
			if (result != null)
				break;
		}
		// Job Level variables
		if (result == null) {
			logDebugMessage("No rule-level variable named '" + name + "' exists! Trying job-level.");
			result = ignite.atomicReference(name, null, false);
		}
		return result;
	}

	protected void cleanUp() {
	}

	protected final void sendEmail(Collection<String> to, Collection<String> cc, String subject, String text) {
		mailSender.send(createSimpleMail(to, cc, subject, text));
	}

	private SimpleMailMessage createSimpleMail(Collection<String> to, Collection<String> cc, String subject, String text) {
		SimpleMailMessage result = new SimpleMailMessage();
		result.setFrom("noreply@icc-mailer");
		result.setTo(to.toArray(new String[to.size()]));
		result.setCc(cc.toArray(new String[cc.size()]));
		result.setSubject(subject);
		result.setText(text);
		return result;
	}

	public String getLastDataset() {
		return lastDataset;
	}

}

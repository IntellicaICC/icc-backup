package net.intellica.icc.template.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

public abstract class DatabaseUtil {

	private JdbcTemplate jdbc;

	public DatabaseUtil(JdbcTemplate jdbc) {
		this.jdbc = jdbc;
	}

	public List<Row> query(String connection, QueryLoggerFunction logger, Runnable cancelCheck, String query) {
		if (logger != null)
			logger.log(connection, query);
		AtomicLong index = new AtomicLong(0L);
		return jdbc.query(query, (rs, rowNum) -> {
			cancelCheck.run();
			adaptFetchSize(rs, index.getAndIncrement());
			Row row;
			ResultSetMetaData metaData = rs.getMetaData();
			row = new Row(metaData);
			for (int i = 0; i < metaData.getColumnCount(); i++)
				row.put(i, rs.getObject(i + 1));
			return row;
		});
	}

	public <T> T queryScalar(String connection, QueryLoggerFunction logger, String query, Class<T> clazz) {
		if (logger != null)
			logger.log(connection, query);
		return jdbc.queryForObject(query, clazz);
	}

	public void execute(String connection, QueryLoggerFunction logger, String query) {
		if (logger != null)
			logger.log(connection, query);
		jdbc.execute(query);
	}

	public void executePrepared(String connection, QueryLoggerFunction logger, String query, Object... args) {
		if (logger != null)
			logger.log(connection, query);
		jdbc.update(query, args);
	}

	public Long queryCount(String connection, QueryLoggerFunction logger, String table) {
		return queryScalar(connection, logger, generateCountQuery(table), Long.class);
	}

	protected abstract String generateCountQuery(String table);

	public void iterateQuery(String connection, QueryLoggerFunction logger, Runnable cancelCheck, String query, Consumer<Row> rowHandler) {
		if (logger != null)
			logger.log(connection, query);
		jdbc.query(query, new ResultSetExtractor<Object>() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				RowIterator iterator = new RowIterator(rs);
				while (iterator.hasNext()) {
					cancelCheck.run();
					Row row = (Row)iterator.next();
					rowHandler.accept(row);
				}
				return null;
			}
		});
	}

	public void dropTable(String connection, QueryLoggerFunction logger, String table) {
		execute(connection, logger, generateDropQuery(table));
	}

	protected abstract String generateDropQuery(String table);

	public void createTable(String connection, QueryLoggerFunction logger, String table, Row row) {
		execute(connection, logger, generateCreateQuery(table, row));
	}

	protected abstract String generateCreateQuery(String table, Row row);

	public void insertToTable(String connection, QueryLoggerFunction logger, String table, Row row) {
		executePrepared(connection, logger, generateInsertQuery(table, row), row.getValues().toArray());
	}

	protected abstract String generateInsertQuery(String table, Row row);

	public void batchInsertToTable(String connection, QueryLoggerFunction logger, Runnable cancelCheck, String table, Row... rows) {
		String query = generateInsertQuery(table, rows[0]);
		if (logger != null)
			logger.log(connection, query);
		jdbc.batchUpdate(query, new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				cancelCheck.run();
				Row currentRow = rows[i];
				for (int j = 0; j < currentRow.getRowMetadata().getColumns().size(); j++) {
					ps.setObject(j + 1, currentRow.get(j), currentRow.getRowMetadata().getTypes().get(j).getVendorTypeNumber());
				}
			}

			@Override
			public int getBatchSize() {
				return rows.length;
			}
		});
	}

	protected abstract void validateTableName(String table);

	protected abstract void validateQualifiedTableName(String table);

	protected abstract void validateColumnName(String column);

	private void adaptFetchSize(ResultSet resultSet, Long index) throws SQLException {
		if (index == 1e3 || index == 1e4 || index == 1e5 || index == 1e6)
			resultSet.setFetchSize(index.intValue());
	}

}

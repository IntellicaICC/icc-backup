package net.intellica.icc.template.model;

import java.io.Serializable;
import javax.cache.Cache.Entry;

public interface CacheProcessor extends Serializable {

	void process(Iterable<Entry<Object, Object>> entries);

}

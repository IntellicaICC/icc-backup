package net.intellica.icc.template.model;

import java.util.Iterator;
import javax.cache.Cache.Entry;

public class EmptyIterator implements Iterator<Entry<Object, Object>> {

	@Override
	public boolean hasNext() {
		return false;
	}

	@Override
	public Entry<Object, Object> next() {
		return null;
	}

}

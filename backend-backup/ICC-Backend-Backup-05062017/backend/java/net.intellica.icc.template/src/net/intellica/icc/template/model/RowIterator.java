package net.intellica.icc.template.model;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Iterator;

public class RowIterator implements Iterator<Row> {

	private ResultSet resultSet;
	private ResultSetMetaData metaData;
	private Boolean hasNext;
	private Boolean iterated;

	public RowIterator(ResultSet resultSet) throws SQLException {
		this.resultSet = resultSet;
		this.metaData = resultSet.getMetaData();
		hasNext = false;
		iterated = false;
	}

	@Override
	public boolean hasNext() {
		Boolean result;
		try {
			if (!iterated) {
				hasNext = resultSet.next();
				iterated = true;
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		result = hasNext;
		return result;
	}

	@Override
	public Row next() {
		Row result = null;
		if (iterated || hasNext())
			result = fillRow();
		return result;
	}

	private Row fillRow() {
		Row result = null;
		try {
			result = new Row(metaData);
			for (int i = 0; i < metaData.getColumnCount(); i++)
				result.put(i, resultSet.getObject(i + 1));
			iterated = false;
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

}

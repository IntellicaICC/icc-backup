package net.intellica.icc.template.exception;

public class InactiveException extends SimpleRuntimeException {

	private static final long serialVersionUID = 1L;

	public InactiveException() {
		super();
	}

	public InactiveException(String message) {
		super(message);
	}

	public InactiveException(String message, Throwable cause) {
		super(message, cause);
	}

	public InactiveException(Throwable cause) {
		super(cause);
	}

}

package net.intellica.icc.template.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@jsonId")
public class WorkItem implements Serializable {

	private static final long serialVersionUID = 1L;

	private Map<String, String> arguments;
	private Map<String, String> parameters;
	private Map<String, DataLink> links;
	private Map<String, String> mailParameters;
	private List<String> instanceChain;
	private String lastDataset;

	public WorkItem() {
		arguments = new HashMap<>();
		parameters = new HashMap<>();
		links = new HashMap<>();
		mailParameters = new HashMap<>();
		instanceChain = new ArrayList<>();
	}

	public Map<String, String> getArguments() {
		return arguments;
	}

	public void setArguments(Map<String, String> arguments) {
		this.arguments = arguments;
	}

	public Map<String, String> getParameters() {
		return parameters;
	}

	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}

	public Map<String, DataLink> getLinks() {
		return links;
	}

	public void setLinks(Map<String, DataLink> links) {
		this.links = links;
	}

	public Map<String, String> getMailParameters() {
		return mailParameters;
	}

	public void setMailParameters(Map<String, String> mailParameters) {
		this.mailParameters = mailParameters;
	}

	public List<String> getInstanceChain() {
		return instanceChain;
	}

	public void setInstanceChain(List<String> workChain) {
		this.instanceChain = workChain;
	}

	public String getLastDataset() {
		return lastDataset;
	}

	public void setLastDataset(String lastDataset) {
		this.lastDataset = lastDataset;
	}

}

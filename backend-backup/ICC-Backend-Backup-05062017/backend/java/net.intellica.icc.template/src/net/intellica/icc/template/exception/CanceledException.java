package net.intellica.icc.template.exception;

public class CanceledException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CanceledException() {
		super();
	}

	public CanceledException(String message) {
		super(message);
	}

	public CanceledException(String message, Throwable cause) {
		super(message, cause);
	}

	public CanceledException(Throwable cause) {
		super(cause);
	}

}

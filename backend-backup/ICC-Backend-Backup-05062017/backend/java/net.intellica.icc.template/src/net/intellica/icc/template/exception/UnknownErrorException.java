package net.intellica.icc.template.exception;

public class UnknownErrorException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UnknownErrorException() {
		super();
	}

	public UnknownErrorException(String message) {
		super(message);
	}

	public UnknownErrorException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnknownErrorException(Throwable cause) {
		super(cause);
	}

}

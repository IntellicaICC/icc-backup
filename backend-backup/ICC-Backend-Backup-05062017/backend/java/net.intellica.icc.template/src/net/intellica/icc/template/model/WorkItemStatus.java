package net.intellica.icc.template.model;

public enum WorkItemStatus {

	Executing,
	Finished,
	Error,
	Canceled

}

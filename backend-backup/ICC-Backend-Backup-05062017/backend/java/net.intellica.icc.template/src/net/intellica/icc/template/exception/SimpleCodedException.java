package net.intellica.icc.template.exception;

public abstract class SimpleCodedException extends SimpleRuntimeException {

	private static final long serialVersionUID = 1L;
	private static final String CODE_UNKNOWN = "ICC-???";
	private static final String CODE_FORMAT = "ICC-%03d";
	private static final String MESSAGE_FORMAT = "%s - %s";

	private final Integer code;

	public SimpleCodedException(Integer code) {
		super(getCodeString(code));
		this.code = code;
	}

	public SimpleCodedException(Integer code, String message, Throwable cause) {
		super(getMessageString(code, message), cause);
		this.code = code;
	}

	public SimpleCodedException(Integer code, String message) {
		super(getMessageString(code, message));
		this.code = code;
	}

	public SimpleCodedException(Integer code, Throwable cause) {
		super(getCodeString(code), cause);
		this.code = code;
	}

	public String getCode() {
		return getCodeString(code);
	}

	private static String getMessageString(Integer code, String message) {
		return String.format(MESSAGE_FORMAT, getCodeString(code), message);
	}

	private static String getCodeString(Integer code) {
		return (code == null ? CODE_UNKNOWN : String.format(CODE_FORMAT, code));
	}

}

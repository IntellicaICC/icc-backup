package net.intellica.icc.template.exception;

public class CompareException extends SimpleRuntimeException {

	private static final long serialVersionUID = 1L;

	public CompareException() {
		super();
	}

	public CompareException(String message) {
		super(message);
	}

	public CompareException(String message, Throwable cause) {
		super(message, cause);
	}

	public CompareException(Throwable cause) {
		super(cause);
	}

}

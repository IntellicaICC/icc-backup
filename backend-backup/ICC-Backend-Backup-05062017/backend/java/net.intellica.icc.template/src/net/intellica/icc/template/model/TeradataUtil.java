package net.intellica.icc.template.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import net.intellica.icc.template.exception.DatabaseException;
import net.intellica.icc.template.exception.TemplateException;
import net.intellica.icc.template.exception.ValidationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.StringUtils;

public class TeradataUtil extends DatabaseUtil {

	private static final String TABLE_NAME_REGEX = "[a-zA-Z]\\w*";
	private static final String QUALIFIED_TABLE_NAME_REGEX = "[a-zA-Z]\\w*\\.[a-zA-Z]\\w*";
	private static final String COLUMN_NAME_REGEX = "[a-zA-Z]\\w*";
	private static final String COLUMN_REGEX = "(\\w+)";

	public TeradataUtil(JdbcTemplate jdbc) {
		super(jdbc);
		try {
			Class.forName("com.teradata.jdbc.TeraDriver");
		}
		catch (Exception e) {
			throw new DatabaseException("Can not find Teradata JDBC driver!");
		}
	}

	@Override
	protected String generateCountQuery(String table) {
		return "SELECT COUNT(*) FROM " + table;
	}

	@Override
	public String generateDropQuery(String table) {
		return "DROP TABLE " + table;
	}

	@Override
	protected String generateCreateQuery(String table, Row row) {
		String result = "";
		List<String> columnExpressions = new ArrayList<>();
		for (int i = 0; i < row.getRowMetadata().getColumns().size(); i++) {
			Integer columnLength = row.getRowMetadata().getLengths().get(i);
			String columnExpression = row.getRowMetadata().getColumns().get(i);
			if (!columnExpression.matches(COLUMN_REGEX))
				throw new TemplateException("Invalid column name '" + columnExpression + "'.");
			switch (row.getRowMetadata().getTypes().get(i)) {
				case NUMERIC:
					columnExpression += " DECIMAL";
					if (columnLength > 0)
						columnExpression += "(" + columnLength + ")";
					break;
				case VARCHAR:
					columnExpression += " VARCHAR(" + columnLength + ")";
					break;
				case DATE:
					columnExpression += " DATE";
					break;
				case TIMESTAMP:
					columnExpression += " TIMESTAMP";
					break;
				default:// TODO diger sutun tipleri
					columnExpression += " VARCHAR(" + columnLength + ")";
					break;
			}
			columnExpressions.add(columnExpression);
		}
		if (columnExpressions.size() > 0) {
			Set<String> keys = row.getKeyColumns();
			if (keys.size() > 0) {
				String keyString = StringUtils.collectionToCommaDelimitedString(keys);
				columnExpressions.add("PRIMARY KEY (" + keyString + ")");
			}
			String columnString = StringUtils.collectionToCommaDelimitedString(columnExpressions);
			result = "CREATE TABLE " + table + " (" + columnString + ")";
		}
		return result;
	}

	@Override
	protected String generateInsertQuery(String table, Row row) {
		String result = "INSERT INTO %s (%s) VALUES (%s)";
		String columns = StringUtils.collectionToCommaDelimitedString(row.getRowMetadata().getColumns());
		String parameters = StringUtils.collectionToCommaDelimitedString(
			row.getRowMetadata().getColumns().stream()
				.map(s -> "?")
				.collect(Collectors.toList())//
		);
		result = String.format(result, table, columns, parameters);
		return result;
	}

	@Override
	protected void validateTableName(String table) {
		if (!table.matches(TABLE_NAME_REGEX))
			throw new ValidationException(null, "Invalid table name'" + table + "'!");
	}

	@Override
	protected void validateQualifiedTableName(String table) {
		if (!table.matches(QUALIFIED_TABLE_NAME_REGEX))
			throw new ValidationException(null, "Invalid table name '" + table + "'!");
	}

	@Override
	protected void validateColumnName(String column) {
		if (!column.matches(COLUMN_NAME_REGEX))
			throw new ValidationException(null, "Invalid column name '" + column + "'!");
	}

}

package net.intellica.icc.template.exception;

public class MisfireException extends SimpleRuntimeException {

	private static final long serialVersionUID = 1L;

	public MisfireException() {
		super();
	}

	public MisfireException(String message) {
		super(message);
	}

	public MisfireException(String message, Throwable cause) {
		super(message, cause);
	}

	public MisfireException(Throwable cause) {
		super(cause);
	}

}

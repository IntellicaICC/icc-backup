package net.intellica.icc.template.exception;

public class DuplicateNodeException extends SimpleRuntimeException {

	private static final long serialVersionUID = 1L;

	public DuplicateNodeException() {
		super();
	}

	public DuplicateNodeException(String message) {
		super(message);
	}

	public DuplicateNodeException(String message, Throwable cause) {
		super(message, cause);
	}

	public DuplicateNodeException(Throwable cause) {
		super(cause);
	}

}

package net.intellica.icc.template.model;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.sql.JDBCType;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.util.ObjectUtils;

public class Row implements Externalizable {

	private Set<String> keys;
	private RowMetadata rowMetadata;
	private Object[] values;

	public Row() {
		keys = new LinkedHashSet<>();
		rowMetadata = new RowMetadata();
		values = new Object[0];
	}

	public Row(ResultSetMetaData metaData) throws SQLException {
		keys = new LinkedHashSet<>();
		rowMetadata = new RowMetadata(metaData);
		values = new Object[metaData.getColumnCount()];
	}

	public Row(Row row) {
		keys = row.keys;
		rowMetadata = new RowMetadata(row.rowMetadata);
		values = Arrays.copyOf(row.values, row.values.length);
	}

	public String getKeyString() {
		Map<String, Object> map = new LinkedHashMap<>();
		keys.forEach(k -> map.put(k, get(k)));
		return map.toString();
	}

	public Set<String> getKeyColumns() {
		return keys;
	}

	public List<Object> getKeyValues() {
		return keys.stream().map(k -> get(k)).collect(Collectors.toList());
	}

	public RowMetadata getRowMetadata() {
		return rowMetadata;
	}

	public Row addColumn(String name, JDBCType type, Integer length, Object value) {
		rowMetadata.addColumn(name, type, length);
		values = Arrays.copyOf(values, values.length + 1);
		values[values.length - 1] = value;
		return this;
	}

	public List<Object> getValues() {
		return Arrays.asList(values);
	}

	public Object get(Integer index) {
		return values[index];
	}

	public Object get(String column) {
		return values[rowMetadata.getColumns().indexOf(column)];
	}

	public Object getKey(Integer index) {
		Object result = null;
		Iterator<String> iterator = keys.iterator();
		Integer current = 0;
		while (iterator.hasNext()) {
			String key = (String)iterator.next();
			if (current == index) {
				result = get(key);
				break;
			}
			current++;
		}
		return result;
	}

	public Object getKey(String column) {
		return get(column);
	}

	public void put(Integer index, Object data) {
		values[index] = data;
	}

	public void put(String column, Object data) {
		values[rowMetadata.getColumns().indexOf(column)] = data;
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeObject(keys);
		out.writeObject(rowMetadata);
		out.writeObject(values);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		keys = (Set<String>)in.readObject();
		rowMetadata = (RowMetadata)in.readObject();
		values = (Object[])in.readObject();
	}

	@Override
	public boolean equals(Object obj) {
		Boolean result = super.equals(obj);
		if (!result && obj instanceof Row) {
			Row row = (Row)obj;
			result = //ObjectUtils.nullSafeEquals(keys, row.keys) &&
				//ObjectUtils.nullSafeEquals(rowMetadata, row.rowMetadata) &&
				ObjectUtils.nullSafeEquals(values, row.values);
		}
		return result;
	}

}

package net.intellica.icc.template.exception;

public abstract class Messages {

	public static abstract class Generic {
		public static final String DATABASE = "Database failed to fulfill the request!";
		public static final String NOT_FOUND = "Requested object does not exist!";
		public static final String VALIDATION = "Request validation failed!";
		public static final String TECHNICAL = "An unknown technical error occured!";
		public static final String FORBIDDEN = "Requested operation is forbidden!";
		public static final String ACCESS_DENIED = "You don't have sufficient rights to %s this resource!";

		public static final Integer CODE_DATABASE = 101;
		public static final Integer CODE_NOT_FOUND = 102;
		public static final Integer CODE_VALIDATION = 103;
		public static final Integer CODE_TECHNICAL = 104;
		public static final Integer CODE_FORBIDDEN = 105;
		public static final Integer CODE_ACCESS_DENIED = 106;
	}

	public static abstract class License {
		public static final String THANKS = "Thank you for registering ICC. You may now start to use ICC Services.";
		public static final String INVALID_LICENSE = "The license you entered appears to be invalid. Please re-check your license key.";
	}

	public static abstract class Login {
		public static final String EMPTY = "Username and password are required!";
		public static final String INVALID = "Invalid username/password!";
		public static final String LOCKED = "The account is locked!";
		public static final String PASSIVE = "The account is not active!";
		public static final String BLOCKED = "The account is blocked!";
	}

	public static abstract class Operation {
	}

	public static abstract class Query {
	}

	public static abstract class Template {
		public static final String ERROR_EDITING = "Template editing is disabled!";
	}

	public static abstract class Validation {
		public static final String ERROR_SETUP = "Alteration of system objects is allowed within setup mode only!";
	}

}

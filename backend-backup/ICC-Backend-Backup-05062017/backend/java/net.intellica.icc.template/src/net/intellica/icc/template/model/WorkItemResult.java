package net.intellica.icc.template.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@jsonId")
public class WorkItemResult implements Serializable {

	private static final long serialVersionUID = 1L;

	private Boolean canceled;
	private Map<String, Object> results;
	private Map<String, String> resultTypes;
	private Exception error;
	private List<QueryLog> queryLogs;

	public WorkItemResult() {
		canceled = false;
		results = new HashMap<>();
		resultTypes = new HashMap<>();
		queryLogs = new ArrayList<>();
	}

	public Boolean isSuccessful() {
		return error == null && !canceled;
	}

	public Boolean isCanceled() {
		return canceled;
	}

	public void setCanceled(Boolean canceled) {
		this.canceled = canceled;
	}

	public Map<String, Object> getResults() {
		return results;
	}

	public void setResults(Map<String, Object> results) {
		this.results = results;
	}

	public Map<String, String> getResultTypes() {
		return resultTypes;
	}

	public void setResultTypes(Map<String, String> resultTypes) {
		this.resultTypes = resultTypes;
	}

	public Exception getError() {
		return error;
	}

	public void setError(Exception error) {
		this.error = error;
	}

	public List<QueryLog> getQueryLogs() {
		return queryLogs;
	}

	public void setQueryLogs(List<QueryLog> queryLogs) {
		this.queryLogs = queryLogs;
	}

	public WorkItemStatus getStatus() {
		WorkItemStatus result;
		if (canceled)
			result = WorkItemStatus.Canceled;
		else if (isSuccessful())
			result = WorkItemStatus.Finished;
		else
			result = WorkItemStatus.Error;
		return result;
	}

}

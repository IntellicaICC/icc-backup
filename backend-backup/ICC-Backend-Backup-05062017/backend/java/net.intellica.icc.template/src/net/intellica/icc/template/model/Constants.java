package net.intellica.icc.template.model;

import java.sql.JDBCType;
import java.util.Arrays;
import java.util.List;

public abstract class Constants {

	public static abstract class User {

		public static final String ANONYMOUS_USER_ID = "anonymousUser";
		public static final String ANONYMOUS_USER_NAME = "Anonymous User";

	}

	public static abstract class Email {

		public static final String HOST = "host";
		public static final String PROTOCOL = "protocol";
		public static final String USERNAME = "username";
		public static final String PASSWORD = "password";

	}

	public static abstract class JDBC {

		public static final List<JDBCType> CHAR_TYPES = Arrays.asList(
			JDBCType.CHAR,
			JDBCType.NCHAR,
			JDBCType.VARCHAR,
			JDBCType.NVARCHAR,
			JDBCType.LONGVARCHAR,
			JDBCType.LONGNVARCHAR//
		);

		public static final List<JDBCType> NUMERIC_TYPES = Arrays.asList(
			JDBCType.SMALLINT,
			JDBCType.INTEGER,
			JDBCType.FLOAT,
			JDBCType.REAL,
			JDBCType.NUMERIC,
			JDBCType.DECIMAL//
		);

		public static final List<JDBCType> DATE_TYPES = Arrays.asList(
			JDBCType.DATE,
			JDBCType.TIMESTAMP//
		);

	}

}

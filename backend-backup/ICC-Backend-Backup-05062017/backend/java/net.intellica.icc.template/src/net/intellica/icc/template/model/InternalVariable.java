package net.intellica.icc.template.model;

public abstract class InternalVariable {

	public static final String DATASET_PREFIX = "[DS]";
	public static final String CACHE_SEPARATOR = "_";
	public static final String VARIABLE_SEPARATOR = "__";
	public static final String RESULTSET_PREFIX = "[R]";
	public static final String QUERYSET_PREFIX = "[Q]";

}

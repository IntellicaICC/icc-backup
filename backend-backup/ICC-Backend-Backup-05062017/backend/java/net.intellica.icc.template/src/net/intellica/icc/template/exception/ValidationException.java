package net.intellica.icc.template.exception;

public class ValidationException extends SimpleCodedException {

	private static final long serialVersionUID = 1L;

	public ValidationException(Integer code, String message, Throwable cause) {
		super(code, message, cause);
	}

	public ValidationException(Integer code, String message) {
		super(code, message);
	}

	public ValidationException(Integer code, Throwable cause) {
		super(code, cause);
	}

	public ValidationException(Integer code) {
		super(code);
	}

}

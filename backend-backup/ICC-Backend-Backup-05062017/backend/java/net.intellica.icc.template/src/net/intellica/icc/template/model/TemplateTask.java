package net.intellica.icc.template.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.apache.ignite.IgniteException;
import org.apache.ignite.compute.ComputeJob;
import org.apache.ignite.compute.ComputeJobResult;
import org.apache.ignite.compute.ComputeJobResultPolicy;
import org.apache.ignite.compute.ComputeTaskSplitAdapter;

public abstract class TemplateTask extends ComputeTaskSplitAdapter<Object, Object> {

	private static final long serialVersionUID = 1L;

	@Override
	protected Collection<? extends ComputeJob> split(int gridSize, Object arg) throws IgniteException {
		// is otomatik bolunuyor
		return Arrays.asList(getJob(arg));
	}

	protected abstract TemplateJob getJob(Object arg);

	@Override
	public Object reduce(List<ComputeJobResult> results) throws IgniteException {
		return (WorkItemResult)results.get(0).getData();
	}

	@Override
	public ComputeJobResultPolicy result(ComputeJobResult res, List<ComputeJobResult> rcvd) throws IgniteException {
		return super.result(res, rcvd);
	}

}

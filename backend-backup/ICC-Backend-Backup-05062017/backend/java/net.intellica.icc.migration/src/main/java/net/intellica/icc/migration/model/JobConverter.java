package net.intellica.icc.migration.model;

import com.fasterxml.jackson.databind.util.StdConverter;

public class JobConverter extends StdConverter<String, Job> {

	@Override
	public Job convert(String value) {
		Job result = new Job();
		result.setId(value);
		return result;
	}

}

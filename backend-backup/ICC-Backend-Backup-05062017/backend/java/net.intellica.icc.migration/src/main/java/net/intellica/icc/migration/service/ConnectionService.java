package net.intellica.icc.migration.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.intellica.icc.migration.model.Connection;
import net.intellica.icc.migration.model.wrapper.ConnectionListRW;
import net.intellica.icc.migration.util.HttpUtil;
import net.intellica.icc.migration.util.MigrationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConnectionService {

	@Autowired private MigrationConfig config;
	@Autowired private HttpUtil http;

	public Connection getConnection(String connectionName) throws Exception {
		Connection result = null;
		String url = config.getService_url() + "/connection/list?name={name}";
		Map<String, Object> variables = new HashMap<>();
		variables.put("name", connectionName);
		ConnectionListRW wrapper = http.httpGet(url, ConnectionListRW.class, variables);
		if (wrapper.getObject() != null) {
			List<Connection> list = wrapper.getObject();
			if (list.size() == 1)
				result = list.get(0);
		}
		return result;
	}

}

package net.intellica.icc.migration.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.intellica.icc.migration.model.Folder;
import net.intellica.icc.migration.model.wrapper.FolderListRW;
import net.intellica.icc.migration.model.wrapper.GenericRW;
import net.intellica.icc.migration.util.HttpUtil;
import net.intellica.icc.migration.util.MigrationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FolderService {

	@Autowired private MigrationConfig config;
	@Autowired private HttpUtil http;

	public Folder getFolderByName(Folder parent, String name) throws Exception {
		Folder result = null;
		String url = config.getService_url() + "/folder/explore?id={id}";
		Map<String, Object> variables = new HashMap<>();
		if (parent != null)
			variables.put("id", parent.getId());
		else
			variables.put("id", null);
		FolderListRW wrapper = http.httpGet(url, FolderListRW.class, variables);
		if (wrapper.getObject() != null) {
			List<Folder> list = wrapper.getObject();
			for (Folder folder : list) {
				if (name.equals(folder.getProps().getName())) {
					result = folder;
					break;
				}
			}
		}
		return result;
	}

	public Folder createFolder(Folder parent, String name, String description) throws Exception {
		Folder result = new Folder();
		result.setParentFolder(parent);
		result.getProps().setName(name);
		result.getProps().setDescription(description);
		String url = config.getService_url() + "/folder/save";
		Map<String, Object> variables = new HashMap<>();
		GenericRW wrapper = http.httpPost(url, GenericRW.class, result, variables);
		if (wrapper.getObject() != null) {
			String folderId = (String)wrapper.getObject();
			result.setId(folderId);
		}
		System.out.println("Created folder '" + name + (parent == null ? "'" : "' under '" + parent.getProps().getName() + "'"));
		return result;
	}

}

package net.intellica.icc.migration.model;

import com.fasterxml.jackson.databind.util.StdConverter;
import java.util.List;
import java.util.Map;

public class ObjectConverter extends StdConverter<Object, Object> {

	@Override
	public Object convert(Object value) {
		Object result = null;
		try {
			if (value instanceof List<?>) {

			}
			else if (value instanceof Map<?, ?>) {

			}
			else
				result = value;
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}

}

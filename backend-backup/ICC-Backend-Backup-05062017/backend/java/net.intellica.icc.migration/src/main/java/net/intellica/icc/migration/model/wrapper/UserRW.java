package net.intellica.icc.migration.model.wrapper;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import net.intellica.icc.migration.model.User;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserRW {

	private User object;
	private String errorCode;
	private String errorMessage;

	public User getObject() {
		return object;
	}

	public void setObject(User object) {
		this.object = object;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}

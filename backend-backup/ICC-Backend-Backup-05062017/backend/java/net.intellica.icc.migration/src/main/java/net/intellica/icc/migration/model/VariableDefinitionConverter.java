package net.intellica.icc.migration.model;

import com.fasterxml.jackson.databind.util.StdConverter;

public class VariableDefinitionConverter extends StdConverter<String, VariableDefinition> {

	@Override
	public VariableDefinition convert(String value) {
		VariableDefinition result = new VariableDefinition();
		result.setId(value);
		return result;
	}

}

package net.intellica.icc.migration.onetoone;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import net.intellica.icc.migration.model.Connection;
import net.intellica.icc.migration.model.Folder;
import net.intellica.icc.migration.model.Job;
import net.intellica.icc.migration.model.Rule;
import net.intellica.icc.migration.service.ConnectionService;
import net.intellica.icc.migration.service.FolderService;
import net.intellica.icc.migration.service.JobService;
import net.intellica.icc.migration.service.RuleService;
import net.intellica.icc.migration.util.MigrationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@SuppressWarnings("unused")
@Component
public class OneToOneMigrator implements CommandLineRunner {

	private static final String ORACLE_DRIVER = "oracle.jdbc.driver.OracleDriver";

	@Autowired private MigrationConfig config;
	@Autowired private FolderService folderService;
	@Autowired private JobService jobService;
	@Autowired private RuleService ruleService;
	@Autowired private ConnectionService connectionService;

	@Override
	public void run(String... args) {
		try {
			List<OneToOneRule> legacyModels = readLegacyModels();
			if (legacyModels.size() > 0) {
				String migrationName = "MIGRATION " + new SimpleDateFormat("dd.MM.yyyy HH:mm").format(new Date());
				Folder migrationFolder = folderService.getFolderByName(null, migrationName);
				if (migrationFolder == null)
					migrationFolder = folderService.createFolder(null, migrationName, null);
				for (OneToOneRule legacyRule : legacyModels) {
					Connection connection = connectionService.getConnection(legacyRule.connectionName);
					// check whether category folder exists, or create new folder for category
					Folder categoryFolder = folderService.getFolderByName(migrationFolder, legacyRule.categoryName);
					if (categoryFolder == null)
						categoryFolder = folderService.createFolder(migrationFolder, legacyRule.categoryName, legacyRule.categoryDescription);
					// check whether rule folder exists, or create new folder for rule
					Folder ruleFolder = folderService.getFolderByName(categoryFolder, legacyRule.name);
					if (ruleFolder == null)
						ruleFolder = folderService.createFolder(categoryFolder, legacyRule.name, legacyRule.description);
					// create left read job
					Job readLeftJob = jobService.createReadJob(ruleFolder, connection, legacyRule.leftTable, legacyRule.leftColumn, legacyRule.leftCondition);
					// create right read job
					Job readRightJob = jobService.createReadJob(ruleFolder, connection, legacyRule.rightTable, legacyRule.rightColumn, legacyRule.rightCondition);
					// create one-to-one job
					Job compareJob = jobService.createOneToOneJob(ruleFolder, readLeftJob, readRightJob);
					// create write job
					Job writeJob = jobService.createWriteJob(ruleFolder, compareJob, connection, legacyRule.reportSchema, legacyRule.reportTable);
					// create rule from above
					Rule rule = ruleService.createRule(ruleFolder, readLeftJob, readRightJob, compareJob, writeJob, legacyRule.name, legacyRule.description);
				}
			}
			else
				System.out.println("No legacy rules found!");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private List<OneToOneRule> readLegacyModels() throws Exception {
		List<OneToOneRule> result = new ArrayList<>();
		List<OneToOneRule> temp = new ArrayList<>();
		Class.forName(ORACLE_DRIVER);
		try (java.sql.Connection c = DriverManager.getConnection(config.getLegacy_url(), config.getLegacy_username(), config.getLegacy_password())) {
			try (Statement s1 = c.createStatement()) {
				try (ResultSet r1 = s1.executeQuery("SELECT RULE_ID, RULESTR FROM ICC_RULETEMPLATE WHERE RULESTR LIKE 'One2OneSimpleCheck;%' ORDER BY EXE_ORDER")) {
					while (r1.next()) {
						Integer ruleId = r1.getInt("RULE_ID");
						String ruleString = r1.getString("RULESTR");
						String[] ruleVariables = ruleString.split(";", -1);
						try (Statement s2 = c.createStatement()) {
							try (ResultSet r2 = s2.executeQuery("SELECT RULENAME, RULEDESCRIPTION, CATEGORYID FROM ICC_RULE WHERE RULEID=" + ruleId)) {
								if (r2.next()) {
									String ruleName = r2.getString("RULENAME");
									String ruleDescription = r2.getString("RULEDESCRIPTION");
									Integer categoryId = r2.getInt("CATEGORYID");
									try (Statement s3 = c.createStatement()) {
										try (ResultSet r3 = s3.executeQuery("SELECT CATEGORYNAME, CATEGORYDESCRIPTION FROM ICC_CATEGORY WHERE CATEGORYID=" + categoryId)) {
											if (r3.next()) {
												String categoryName = r3.getString("CATEGORYNAME");
												String categoryDescription = r3.getString("CATEGORYDESCRIPTION");
												OneToOneRule legacyRule = new OneToOneRule();
												legacyRule.categoryName = categoryName;
												legacyRule.categoryDescription = categoryDescription;
												legacyRule.name = ruleName;
												legacyRule.description = ruleDescription;
												legacyRule.leftTable = ruleVariables[1];
												legacyRule.rightTable = ruleVariables[2];
												legacyRule.leftColumn = ruleVariables[3];
												legacyRule.rightColumn = ruleVariables[4];
												legacyRule.leftCondition = ruleVariables[5];
												legacyRule.rightCondition = ruleVariables[6];
												legacyRule.connectionName = ruleVariables[7];
												legacyRule.reportSchema = ruleVariables[8];
												legacyRule.reportTable = ruleVariables[9];
												temp.add(legacyRule);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		result.addAll(temp);
		System.out.println("Successfully read " + result.size() + " rules from legacy database.");
		return result;
	}

}

package net.intellica.icc.migration.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Variable {

	private String id;
	private VariableDefinition definition;
	private String value;
	private Job job;

	public Variable() {
		definition = new VariableDefinition();
		value = "";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonSerialize(converter = IdConverter.class)
	@JsonDeserialize(converter = VariableDefinitionConverter.class)
	public VariableDefinition getDefinition() {
		return definition;
	}

	public void setDefinition(VariableDefinition definition) {
		this.definition = definition;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@JsonSerialize(converter = IdConverter.class)
	@JsonDeserialize(converter = JobConverter.class)
	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

}

package net.intellica.icc.migration.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import net.intellica.icc.migration.model.Connection;
import net.intellica.icc.migration.model.Folder;
import net.intellica.icc.migration.model.Job;
import net.intellica.icc.migration.model.Rights;
import net.intellica.icc.migration.model.Template;
import net.intellica.icc.migration.model.User;
import net.intellica.icc.migration.model.Variable;
import net.intellica.icc.migration.model.VariableDefinition;
import net.intellica.icc.migration.model.wrapper.GenericRW;
import net.intellica.icc.migration.util.HttpUtil;
import net.intellica.icc.migration.util.MigrationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class JobService {

	@Autowired private MigrationConfig config;
	@Autowired private HttpUtil http;
	@Autowired private TemplateService templateService;
	@Autowired private User profile;

	public Job createWriteJob(Folder parent, Job compareJob, Connection connection, String schema, String table) throws Exception {
		Job result = new Job();
		Template writeTemplate = templateService.getTemplateByName("WriteData");
		Set<VariableDefinition> definitions = templateService.getTemplateVariables(writeTemplate.getId());
		if (!StringUtils.isEmpty(schema) && !StringUtils.isEmpty(schema.trim()))
			table = schema.trim() + "." + table;
		result.getProps().setName("Report to Database");
		result.setFolder(parent);
		result.setTemplate(writeTemplate);
		for (VariableDefinition d : definitions) {
			Variable v = new Variable();
			v.setDefinition(d);
			switch (d.getProps().getName()) {
				case "Table":
					v.setValue(table);
					break;
				case "ReportFormat":
					v.setValue("true");
					break;
				case "DropBeforeCreate":
					v.setValue("true");
					break;
				case "Connection":
					v.setValue(connection.getId());
					break;
				case "DataSet":
					v.setValue(compareJob.getId());
					break;
				default:
					break;
			}
			result.getVariables().add(v);
		}
		Rights rights = new Rights();
		rights.setOwner(profile.getId());
		result.setRights(rights);
		String url = config.getService_url() + "/job/save";
		Map<String, Object> variables = new HashMap<>();
		GenericRW wrapper = http.httpPost(url, GenericRW.class, result, variables);
		if (wrapper.getObject() != null) {
			String jobId = (String)wrapper.getObject();
			result.setId(jobId);
		}
		System.out.println("Created job '" + result.getProps().getName() + "' under folder '" + parent.getProps().getName() + "'");
		return result;
	}

	public Job createOneToOneJob(Folder parent, Job firstJob, Job secondJob) throws Exception {
		Job result = new Job();
		Template oneToOneTemplate = templateService.getTemplateByName("OneToOne");
		Set<VariableDefinition> definitions = templateService.getTemplateVariables(oneToOneTemplate.getId());
		result.getProps().setName("Compare One-To-One");
		result.setFolder(parent);
		result.setTemplate(oneToOneTemplate);
		for (VariableDefinition d : definitions) {
			Variable v = new Variable();
			v.setDefinition(d);
			switch (d.getProps().getName()) {
				case "FirstDataSet":
					v.setValue(firstJob.getId());
					break;
				case "SecondDataSet":
					v.setValue(secondJob.getId());
					break;
				case "ReportIdentical":
					v.setValue("false");
					break;
				default:
					break;
			}
			result.getVariables().add(v);
		}
		Rights rights = new Rights();
		rights.setOwner(profile.getId());
		result.setRights(rights);
		String url = config.getService_url() + "/job/save";
		Map<String, Object> variables = new HashMap<>();
		GenericRW wrapper = http.httpPost(url, GenericRW.class, result, variables);
		if (wrapper.getObject() != null) {
			String jobId = (String)wrapper.getObject();
			result.setId(jobId);
		}
		System.out.println("Created job '" + result.getProps().getName() + "' under folder '" + parent.getProps().getName() + "'");
		return result;
	}

	public Job createReadJob(Folder parent, Connection connection, String table, String column, String condition) throws Exception {
		Job result = new Job();
		Template readTemplate = templateService.getTemplateByName("ReadData");
		Set<VariableDefinition> definitions = templateService.getTemplateVariables(readTemplate.getId());
		String sql = "SELECT " + column + " FROM " + table;
		if (!StringUtils.isEmpty(condition) && !StringUtils.isEmpty(condition.trim())) {
			condition = condition.trim();
			if (condition.toLowerCase().startsWith("where"))
				sql += condition;
			else
				sql += " WHERE " + condition;
		}
		result.getProps().setName("Read " + table);
		result.setFolder(parent);
		result.setTemplate(readTemplate);
		for (VariableDefinition d : definitions) {
			Variable v = new Variable();
			v.setDefinition(d);
			switch (d.getProps().getName()) {
				case "Connection":
					v.setValue(connection.getId());
					break;
				case "Query":
					v.setValue(sql);
					break;
				case "Keys":
					v.setValue(column);
					break;
				default:
					break;
			}
			result.getVariables().add(v);
		}
		Rights rights = new Rights();
		rights.setOwner(profile.getId());
		result.setRights(rights);
		String url = config.getService_url() + "/job/save";
		Map<String, Object> variables = new HashMap<>();
		GenericRW wrapper = http.httpPost(url, GenericRW.class, result, variables);
		if (wrapper.getObject() != null) {
			String jobId = (String)wrapper.getObject();
			result.setId(jobId);
		}
		System.out.println("Created job '" + result.getProps().getName() + "' under folder '" + parent.getProps().getName() + "'");
		return result;
	}

}

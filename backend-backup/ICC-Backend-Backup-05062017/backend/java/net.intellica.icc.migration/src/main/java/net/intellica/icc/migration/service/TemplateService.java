package net.intellica.icc.migration.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.intellica.icc.migration.model.Template;
import net.intellica.icc.migration.model.VariableDefinition;
import net.intellica.icc.migration.model.wrapper.TemplateListRW;
import net.intellica.icc.migration.model.wrapper.VariableDefinitionSetRW;
import net.intellica.icc.migration.util.HttpUtil;
import net.intellica.icc.migration.util.MigrationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TemplateService {

	@Autowired private MigrationConfig config;
	@Autowired private HttpUtil http;

	public Template getTemplateByName(String name) throws Exception {
		Template result = null;
		String url = config.getService_url() + "/template/list?name={name}";
		Map<String, Object> variables = new HashMap<>();
		variables.put("name", name);
		TemplateListRW wrapper = http.httpGet(url, TemplateListRW.class, variables);
		if (wrapper.getObject() != null) {
			List<Template> list = wrapper.getObject();
			for (Template template : list) {
				if (name.equals(template.getName())) {
					result = template;
					break;
				}
			}
		}
		return result;
	}

	public Set<VariableDefinition> getTemplateVariables(String id) throws Exception {
		Set<VariableDefinition> result = null;
		String url = config.getService_url() + "/template/{id}/variables";
		Map<String, Object> variables = new HashMap<>();
		variables.put("id", id);
		VariableDefinitionSetRW wrapper = http.httpGet(url, VariableDefinitionSetRW.class, variables);
		if (wrapper.getObject() != null)
			result = wrapper.getObject();
		return result;
	}

}

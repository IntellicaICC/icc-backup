package net.intellica.icc.migration.model.wrapper;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import net.intellica.icc.migration.model.Folder;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FolderListRW extends GenericRW {

	private List<Folder> object;
	private String errorCode;
	private String errorMessage;

	public List<Folder> getObject() {
		return object;
	}

	public void setObject(List<Folder> object) {
		this.object = object;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}

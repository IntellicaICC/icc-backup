package net.intellica.icc.migration.service;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import net.intellica.icc.migration.model.Folder;
import net.intellica.icc.migration.model.Job;
import net.intellica.icc.migration.model.Rights;
import net.intellica.icc.migration.model.Rule;
import net.intellica.icc.migration.model.User;
import net.intellica.icc.migration.model.wrapper.GenericRW;
import net.intellica.icc.migration.util.HttpUtil;
import net.intellica.icc.migration.util.MigrationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RuleService {

	@Autowired private MigrationConfig config;
	@Autowired private HttpUtil http;
	@Autowired private User profile;

	public Rule createRule(Folder parent, Job readJob1, Job readJob2, Job compareJob, Job writeJob, String name, String description) throws Exception {
		Rule result = new Rule();
		String detail;
		try (Scanner scanner = new Scanner(new FileInputStream("./rdt.json"))) {
			detail = scanner.useDelimiter("\\A").next();
		}
		detail = detail
			.replace("jobId1", readJob1.getId())
			.replace("jobId2", readJob2.getId())
			.replace("jobId3", compareJob.getId())
			.replace("jobId4", writeJob.getId())
			.replace("jobName1", readJob1.getProps().getName())
			.replace("jobName2", readJob2.getProps().getName())
			.replace("jobName3", compareJob.getProps().getName())
			.replace("jobName4", writeJob.getProps().getName());
		result.setDetail(detail);
		result.getProps().setName(name);
		result.getProps().setDescription(description);
		result.setFolder(parent);
		Rights rights = new Rights();
		rights.setOwner(profile.getId());
		result.setRights(rights);
		String url = config.getService_url() + "/rule/save";
		Map<String, Object> variables = new HashMap<>();
		GenericRW wrapper = http.httpPost(url, GenericRW.class, result, variables);
		if (wrapper.getObject() != null) {
			String jobId = (String)wrapper.getObject();
			result.setId(jobId);
		}
		System.out.println("Created rule '" + result.getProps().getName() + "' under folder '" + parent.getProps().getName() + "'");
		return result;
	}

}

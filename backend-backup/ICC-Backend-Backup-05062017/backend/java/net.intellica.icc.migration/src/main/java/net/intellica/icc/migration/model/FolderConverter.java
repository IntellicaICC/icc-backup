package net.intellica.icc.migration.model;

import com.fasterxml.jackson.databind.util.StdConverter;

public class FolderConverter extends StdConverter<String, Folder> {

	@Override
	public Folder convert(String value) {
		Folder result = new Folder();
		result.setId(value);
		return result;
	}

}

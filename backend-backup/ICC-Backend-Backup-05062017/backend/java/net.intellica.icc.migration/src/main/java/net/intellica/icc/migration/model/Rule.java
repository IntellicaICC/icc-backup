package net.intellica.icc.migration.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Rule {

	private String id;
	private CommonProperties props;
	private String detail;
	private String image;
	private Folder folder;
	private Rights rights;

	public Rule() {
		props = new CommonProperties();
		rights = new Rights();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public CommonProperties getProps() {
		return props;
	}

	public void setProps(CommonProperties props) {
		this.props = props;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@JsonSerialize(converter = IdConverter.class)
	@JsonDeserialize(converter = FolderConverter.class)
	public Folder getFolder() {
		return folder;
	}

	public void setFolder(Folder folder) {
		this.folder = folder;
	}

	public Rights getRights() {
		return rights;
	}

	public void setRights(Rights rights) {
		this.rights = rights;
	}

}

package net.intellica.icc.migration.onetoone;

public class OneToOneRule {
	
	public String categoryName;
	public String categoryDescription;
	public String name;
	public String description;
	public String leftTable;
	public String rightTable;
	public String leftColumn;
	public String rightColumn;
	public String leftCondition;
	public String rightCondition;
	public String connectionName;
	public String reportSchema;
	public String reportTable;
	
}

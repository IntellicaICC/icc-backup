package net.intellica.icc.migration.model;

import com.fasterxml.jackson.databind.util.StdConverter;

public class TemplateConverter extends StdConverter<String, Template> {

	@Override
	public Template convert(String value) {
		Template result = new Template();
		result.setId(value);
		return result;
	}

}

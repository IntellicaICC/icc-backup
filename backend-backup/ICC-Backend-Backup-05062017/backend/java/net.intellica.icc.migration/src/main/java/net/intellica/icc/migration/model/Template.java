package net.intellica.icc.migration.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.HashSet;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Template {

	private String id;
	private Boolean active;
	private String description;
	private String name;
	private Boolean hasDataset;
	private Set<VariableDefinition> variables;

	public Template() {
		active = true;
		description = "";
		name = "";
		hasDataset = false;
		variables = new HashSet<>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean isActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getHasDataset() {
		return hasDataset;
	}

	public void setHasDataset(Boolean hasDataset) {
		this.hasDataset = hasDataset;
	}

	public Set<VariableDefinition> getVariables() {
		return variables;
	}

	public void setVariables(Set<VariableDefinition> variables) {
		this.variables = variables;
	}

}

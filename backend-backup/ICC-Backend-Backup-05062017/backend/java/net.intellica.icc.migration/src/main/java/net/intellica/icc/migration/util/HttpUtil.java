package net.intellica.icc.migration.util;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class HttpUtil {

	@Autowired private RestTemplate rest;

	public <T> T httpGet(String url, Class<T> clazz, Map<String, Object> variables) throws Exception {
		T result = null;
		ResponseEntity<T> response = rest.getForEntity(url, clazz, variables);
		if (response.getStatusCode() == HttpStatus.OK)
			result = response.getBody();
		return result;
	}

	public <T> T httpPost(String url, Class<T> clazz, Object object, Map<String, Object> variables) throws Exception {
		T result = null;
		ResponseEntity<T> response = rest.postForEntity(url, object, clazz, variables);
		if (response.getStatusCode() == HttpStatus.OK)
			result = response.getBody();
		return result;
	}

}

package net.intellica.icc.migration.util;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "icc.migration")
public class MigrationConfig {

	private String service_url;
	private String service_username;
	private String service_password;
	private String legacy_url;
	private String legacy_username;
	private String legacy_password;

	public String getService_url() {
		return service_url;
	}

	public void setService_url(String service_url) {
		this.service_url = service_url;
	}

	public String getService_username() {
		return service_username;
	}

	public void setService_username(String service_username) {
		this.service_username = service_username;
	}

	public String getService_password() {
		return service_password;
	}

	public void setService_password(String service_password) {
		this.service_password = service_password;
	}

	public String getLegacy_url() {
		return legacy_url;
	}

	public void setLegacy_url(String legacy_url) {
		this.legacy_url = legacy_url;
	}

	public String getLegacy_username() {
		return legacy_username;
	}

	public void setLegacy_username(String legacy_username) {
		this.legacy_username = legacy_username;
	}

	public String getLegacy_password() {
		return legacy_password;
	}

	public void setLegacy_password(String legacy_password) {
		this.legacy_password = legacy_password;
	}

}

package net.intellica.icc.migration.model;

public enum VariableType {

	Standard,
	Connection,
	SQL,
	Boolean,
	List,
	Dataset

}

package net.intellica.icc.migration;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import net.intellica.icc.migration.model.User;
import net.intellica.icc.migration.service.ProfileService;
import net.intellica.icc.migration.util.MigrationConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class Application {

	public static void main(String args[]) {
		try {
			TrustManager[] manager = new TrustManager[] { new X509TrustManager() {
				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}
			} };
			SSLContext context = SSLContext.getInstance("SSL");
			context.init(null, manager, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier((h, v) -> true);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		SpringApplication.run(Application.class);
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder, MigrationConfig config) {
		return builder
			.basicAuthorization(config.getService_username(), config.getService_password())
			.build();
	}

	@Bean
	public ObjectMapper mapper() {
		return new ObjectMapper();
	}

	@Bean
	public User profile(ProfileService profileService) throws Exception {
		return profileService.getProfile();
	}

}
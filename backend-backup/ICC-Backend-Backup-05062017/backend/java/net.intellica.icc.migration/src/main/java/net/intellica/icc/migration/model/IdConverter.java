package net.intellica.icc.migration.model;

import com.fasterxml.jackson.databind.util.StdConverter;

public class IdConverter extends StdConverter<Object, String> {

	@Override
	public String convert(Object value) {
		String result = null;
		if (value instanceof Connection)
			result = ((Connection)value).getId();
		else if (value instanceof Folder)
			result = ((Folder)value).getId();
		else if (value instanceof Job)
			result = ((Job)value).getId();
		else if (value instanceof LocalParameter)
			result = ((LocalParameter)value).getId();
		else if (value instanceof Rights)
			result = ((Rights)value).getId();
		else if (value instanceof Rule)
			result = ((Rule)value).getId();
		else if (value instanceof Template)
			result = ((Template)value).getId();
		else if (value instanceof Variable)
			result = ((Variable)value).getId();
		else if (value instanceof VariableDefinition)
			result = ((VariableDefinition)value).getId();
		return result;
	}

}

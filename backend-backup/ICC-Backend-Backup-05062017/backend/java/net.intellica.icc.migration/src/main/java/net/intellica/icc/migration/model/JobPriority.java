package net.intellica.icc.migration.model;

public enum JobPriority {

	Low,
	Normal,
	High

}

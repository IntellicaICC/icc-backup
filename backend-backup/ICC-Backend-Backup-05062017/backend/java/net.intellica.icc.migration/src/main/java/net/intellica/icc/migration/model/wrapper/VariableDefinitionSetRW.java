package net.intellica.icc.migration.model.wrapper;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Set;
import net.intellica.icc.migration.model.VariableDefinition;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VariableDefinitionSetRW extends GenericRW {

	private Set<VariableDefinition> object;
	private String errorCode;
	private String errorMessage;

	public Set<VariableDefinition> getObject() {
		return object;
	}

	public void setObject(Set<VariableDefinition> object) {
		this.object = object;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}

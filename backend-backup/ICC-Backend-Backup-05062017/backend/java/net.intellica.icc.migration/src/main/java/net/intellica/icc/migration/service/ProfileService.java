package net.intellica.icc.migration.service;

import java.util.HashMap;
import java.util.Map;
import net.intellica.icc.migration.model.User;
import net.intellica.icc.migration.model.wrapper.UserRW;
import net.intellica.icc.migration.util.HttpUtil;
import net.intellica.icc.migration.util.MigrationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProfileService {

	@Autowired private MigrationConfig config;
	@Autowired private HttpUtil http;

	public User getProfile() throws Exception {
		User result = null;
		String url = config.getService_url() + "/profile";
		Map<String, Object> variables = new HashMap<>();
		UserRW wrapper = http.httpGet(url, UserRW.class, variables);
		if (wrapper.getObject() != null)
			result = wrapper.getObject();
		return result;
	}

}

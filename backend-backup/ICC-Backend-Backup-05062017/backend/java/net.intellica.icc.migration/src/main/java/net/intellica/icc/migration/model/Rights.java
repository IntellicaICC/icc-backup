package net.intellica.icc.migration.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Rights {

	public static final Byte READ_RIGHT = 0b1;
	public static final Byte WRITE_RIGHT = 0b10;
	public static final Byte EXECUTE_RIGHT = 0b100;

	private String id;
	private String owner;
	private Map<String, Byte> roleRights;
	private Map<String, Byte> userRights;
	private Boolean inheritsFromParent;

	public Rights() {
		roleRights = new HashMap<>();
		userRights = new HashMap<>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Map<String, Byte> getRoleRights() {
		return roleRights;
	}

	public void setRoleRights(Map<String, Byte> roleRights) {
		this.roleRights = roleRights;
	}

	public Map<String, Byte> getUserRights() {
		return userRights;
	}

	public void setUserRights(Map<String, Byte> userRights) {
		this.userRights = userRights;
	}

	public Boolean getInheritsFromParent() {
		return inheritsFromParent;
	}

	public void setInheritsFromParent(Boolean inheritsFromParent) {
		this.inheritsFromParent = inheritsFromParent;
	}

}

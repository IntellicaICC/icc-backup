DELETE FROM	ICC_SETTING	;
DELETE FROM	ICC_NOTIFICATION	;
DELETE FROM	ICC_COMMAND_LOG	;
DELETE FROM	ICC_OPERATION	;
DELETE FROM	ICC_OPERATION_RESULT	;
DELETE FROM	ICC_OPERATION_LOG	;
DELETE FROM	ICC_OPERATION_DETAIL	;
DELETE FROM	ICC_ERROR_LOG	;
DELETE FROM	ICC_LICENSE	;
DELETE FROM	ICC_RULE_AUD	;
DELETE FROM	ICC_RULE	;
DELETE FROM	ICC_PARAMETER_LOCAL_AUD	;
DELETE FROM	ICC_PARAMETER_LOCAL	;
DELETE FROM	ICC_VARIABLE_AUD	;
DELETE FROM	ICC_VARIABLE	;
DELETE FROM	ICC_JOB_AUD	;
DELETE FROM	ICC_JOB	;
DELETE FROM	ICC_FOLDER_AUD	;
DELETE FROM	ICC_FOLDER	;
DELETE FROM	ICC_ROLEUSER_AUD	;
DELETE FROM	ICC_ROLEUSER	;
DELETE FROM	ICC_RIGHTS_USER_AUD	;
DELETE FROM	ICC_RIGHTS_USER	;
DELETE FROM	ICC_RIGHTS_ROLE_AUD	;
DELETE FROM	ICC_RIGHTS_ROLE	;
DELETE FROM	ICC_ROLE_AUD	;
DELETE FROM	ICC_ROLE	;
DELETE FROM	ICC_RIGHTS_AUD	;
DELETE FROM	ICC_RIGHTS	;
DELETE FROM	ICC_PARAMETER_GLOBAL_AUD	;
DELETE FROM	ICC_PARAMETER_GLOBAL	;
DELETE FROM	ICC_CONNECTION_AUD	;
DELETE FROM	ICC_CONNECTION	;
DELETE FROM	ICC_VARIABLEDEF_AUD	;
DELETE FROM	ICC_VARIABLEDEF	;
DELETE FROM	ICC_TEMPLATE_AUD	;
DELETE FROM	ICC_TEMPLATE	;
DELETE FROM	ICC_SETTINGDEF	;
DELETE FROM	ICC_USER_AUD	;
DELETE FROM	ICC_USER	;
DELETE FROM	REVINFO	;
DELETE FROM	QRTZ_TRIGGERS	;
DELETE FROM	QRTZ_SIMPROP_TRIGGERS	;
DELETE FROM	QRTZ_SIMPLE_TRIGGERS	;
DELETE FROM	QRTZ_SCHEDULER_STATE	;
DELETE FROM	QRTZ_PAUSED_TRIGGER_GRPS	;
DELETE FROM	QRTZ_LOCKS	;
DELETE FROM	QRTZ_JOB_DETAILS	;
DELETE FROM	QRTZ_FIRED_TRIGGERS	;
DELETE FROM	QRTZ_CRON_TRIGGERS	;
DELETE FROM	QRTZ_CALENDARS	;
DELETE FROM	QRTZ_BLOB_TRIGGERS	;

COMMIT;

INSERT INTO	QRTZ_BLOB_TRIGGERS	 SELECT * FROM 	QRTZ_BLOB_TRIGGERS	@ICCW_07;
INSERT INTO	QRTZ_CALENDARS	 SELECT * FROM 	QRTZ_CALENDARS	@ICCW_07;
INSERT INTO	QRTZ_CRON_TRIGGERS	 SELECT * FROM 	QRTZ_CRON_TRIGGERS	@ICCW_07;
INSERT INTO	QRTZ_FIRED_TRIGGERS	 SELECT * FROM 	QRTZ_FIRED_TRIGGERS	@ICCW_07;
INSERT INTO	QRTZ_JOB_DETAILS	 SELECT * FROM 	QRTZ_JOB_DETAILS	@ICCW_07;
INSERT INTO	QRTZ_LOCKS	 SELECT * FROM 	QRTZ_LOCKS	@ICCW_07;
INSERT INTO	QRTZ_PAUSED_TRIGGER_GRPS	 SELECT * FROM 	QRTZ_PAUSED_TRIGGER_GRPS	@ICCW_07;
INSERT INTO	QRTZ_SCHEDULER_STATE	 SELECT * FROM 	QRTZ_SCHEDULER_STATE	@ICCW_07;
INSERT INTO	QRTZ_SIMPLE_TRIGGERS	 SELECT * FROM 	QRTZ_SIMPLE_TRIGGERS	@ICCW_07;
INSERT INTO	QRTZ_SIMPROP_TRIGGERS	 SELECT * FROM 	QRTZ_SIMPROP_TRIGGERS	@ICCW_07;
INSERT INTO	QRTZ_TRIGGERS	 SELECT * FROM 	QRTZ_TRIGGERS	@ICCW_07;
INSERT INTO	REVINFO	 SELECT * FROM 	REVINFO	@ICCW_07;
INSERT INTO	ICC_USER	 SELECT * FROM 	ICC_USER	@ICCW_07;
INSERT INTO	ICC_USER_AUD	 SELECT * FROM 	ICC_USER_AUD	@ICCW_07;
INSERT INTO	ICC_SETTINGDEF	 SELECT * FROM 	ICC_SETTINGDEF	@ICCW_07;
INSERT INTO	ICC_TEMPLATE	 SELECT * FROM 	ICC_TEMPLATE	@ICCW_07;
INSERT INTO	ICC_TEMPLATE_AUD	 SELECT * FROM 	ICC_TEMPLATE_AUD	@ICCW_07;
INSERT INTO	ICC_VARIABLEDEF	 SELECT * FROM 	ICC_VARIABLEDEF	@ICCW_07;
INSERT INTO	ICC_VARIABLEDEF_AUD	 SELECT * FROM 	ICC_VARIABLEDEF_AUD	@ICCW_07;
INSERT INTO	ICC_CONNECTION	 SELECT * FROM 	ICC_CONNECTION	@ICCW_07;
INSERT INTO	ICC_CONNECTION_AUD	 SELECT * FROM 	ICC_CONNECTION_AUD	@ICCW_07;
INSERT INTO	ICC_PARAMETER_GLOBAL	 SELECT * FROM 	ICC_PARAMETER_GLOBAL	@ICCW_07;
INSERT INTO	ICC_PARAMETER_GLOBAL_AUD	 SELECT * FROM 	ICC_PARAMETER_GLOBAL_AUD	@ICCW_07;
INSERT INTO	ICC_RIGHTS	 SELECT * FROM 	ICC_RIGHTS	@ICCW_07;
INSERT INTO	ICC_RIGHTS_AUD	 SELECT * FROM 	ICC_RIGHTS_AUD	@ICCW_07;
INSERT INTO	ICC_ROLE	 SELECT * FROM 	ICC_ROLE	@ICCW_07;
INSERT INTO	ICC_ROLE_AUD	 SELECT * FROM 	ICC_ROLE_AUD	@ICCW_07;
INSERT INTO	ICC_RIGHTS_ROLE	 SELECT * FROM 	ICC_RIGHTS_ROLE	@ICCW_07;
INSERT INTO	ICC_RIGHTS_ROLE_AUD	 SELECT * FROM 	ICC_RIGHTS_ROLE_AUD	@ICCW_07;
INSERT INTO	ICC_RIGHTS_USER	 SELECT * FROM 	ICC_RIGHTS_USER	@ICCW_07;
INSERT INTO	ICC_RIGHTS_USER_AUD	 SELECT * FROM 	ICC_RIGHTS_USER_AUD	@ICCW_07;
INSERT INTO	ICC_ROLEUSER	 SELECT * FROM 	ICC_ROLEUSER	@ICCW_07;
INSERT INTO	ICC_ROLEUSER_AUD	 SELECT * FROM 	ICC_ROLEUSER_AUD	@ICCW_07;
INSERT INTO	ICC_FOLDER	 SELECT * FROM 	ICC_FOLDER	@ICCW_07;
INSERT INTO	ICC_FOLDER_AUD	 SELECT * FROM 	ICC_FOLDER_AUD	@ICCW_07;
INSERT INTO	ICC_JOB	 SELECT * FROM 	ICC_JOB	@ICCW_07;
INSERT INTO	ICC_JOB_AUD	 SELECT * FROM 	ICC_JOB_AUD	@ICCW_07;
INSERT INTO	ICC_VARIABLE	 SELECT * FROM 	ICC_VARIABLE	@ICCW_07;
INSERT INTO	ICC_VARIABLE_AUD	 SELECT * FROM 	ICC_VARIABLE_AUD	@ICCW_07;
INSERT INTO	ICC_PARAMETER_LOCAL	 SELECT * FROM 	ICC_PARAMETER_LOCAL	@ICCW_07;
INSERT INTO	ICC_PARAMETER_LOCAL_AUD	 SELECT * FROM 	ICC_PARAMETER_LOCAL_AUD	@ICCW_07;
INSERT INTO	ICC_RULE	 SELECT * FROM 	ICC_RULE	@ICCW_07;
INSERT INTO	ICC_RULE_AUD	 SELECT * FROM 	ICC_RULE_AUD	@ICCW_07;
INSERT INTO	ICC_LICENSE	 SELECT * FROM 	ICC_LICENSE	@ICCW_07;
INSERT INTO	ICC_ERROR_LOG	 SELECT * FROM 	ICC_ERROR_LOG	@ICCW_07;
INSERT INTO	ICC_OPERATION_DETAIL	 SELECT * FROM 	ICC_OPERATION_DETAIL	@ICCW_07;
INSERT INTO	ICC_OPERATION_LOG	 SELECT * FROM 	ICC_OPERATION_LOG	@ICCW_07;
INSERT INTO	ICC_OPERATION_RESULT	 SELECT * FROM 	ICC_OPERATION_RESULT	@ICCW_07;
INSERT INTO	ICC_OPERATION	 SELECT * FROM 	ICC_OPERATION	@ICCW_07;
INSERT INTO	ICC_COMMAND_LOG	 SELECT * FROM 	ICC_COMMAND_LOG	@ICCW_07;
INSERT INTO	ICC_NOTIFICATION	 SELECT * FROM 	ICC_NOTIFICATION	@ICCW_07;
INSERT INTO	ICC_SETTING	 SELECT * FROM 	ICC_SETTING	@ICCW_07;

COMMIT;




--------------------------------------------------------
--  DDL for Table ICC_COMMAND_LOG
--------------------------------------------------------

  CREATE TABLE "ICC_COMMAND_LOG" 
   (	"ID" VARCHAR2(255 CHAR), 
	"COMMAND" VARCHAR2(255 CHAR), 
	"COMMAND_DATE" TIMESTAMP (6), 
	"OPERATION_ID" VARCHAR2(255 CHAR), 
	"USER_ID" VARCHAR2(255 CHAR), 
	"USER_NAME" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_CONNECTION
--------------------------------------------------------

  CREATE TABLE "ICC_CONNECTION" 
   (	"ID" VARCHAR2(255 CHAR), 
	"PASSWORD" VARCHAR2(255 CHAR), 
	"URL" VARCHAR2(255 CHAR), 
	"USERNAME" VARCHAR2(255 CHAR), 
	"ACTIVE" NUMBER(1,0), 
	"CREATED_BY" VARCHAR2(255 CHAR), 
	"CREATION_DATE" TIMESTAMP (6), 
	"DESCRIPTION" VARCHAR2(2000 CHAR), 
	"MODIFICATION_DATE" TIMESTAMP (6), 
	"MODIFIED_BY" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_CONNECTION_AUD
--------------------------------------------------------

  CREATE TABLE "ICC_CONNECTION_AUD" 
   (	"ID" VARCHAR2(255 CHAR), 
	"REV" NUMBER(10,0), 
	"REVTYPE" NUMBER(3,0), 
	"PASSWORD" VARCHAR2(255 CHAR), 
	"URL" VARCHAR2(255 CHAR), 
	"USERNAME" VARCHAR2(255 CHAR), 
	"ACTIVE" NUMBER(1,0), 
	"CREATED_BY" VARCHAR2(255 CHAR), 
	"CREATION_DATE" TIMESTAMP (6), 
	"DESCRIPTION" VARCHAR2(2000 CHAR), 
	"MODIFICATION_DATE" TIMESTAMP (6), 
	"MODIFIED_BY" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_ERROR_LOG
--------------------------------------------------------

  CREATE TABLE "ICC_ERROR_LOG" 
   (	"ID" VARCHAR2(255 CHAR), 
	"CAUSED_BY" VARCHAR2(255 CHAR), 
	"ERROR_CLASS" VARCHAR2(2000 CHAR), 
	"ERROR_DATE" TIMESTAMP (6), 
	"MESSAGE" CLOB, 
	"ERROR_TYPE" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_FOLDER
--------------------------------------------------------

  CREATE TABLE "ICC_FOLDER" 
   (	"ID" VARCHAR2(255 CHAR), 
	"ACTIVE" NUMBER(1,0), 
	"CREATED_BY" VARCHAR2(255 CHAR), 
	"CREATION_DATE" TIMESTAMP (6), 
	"DESCRIPTION" VARCHAR2(2000 CHAR), 
	"MODIFICATION_DATE" TIMESTAMP (6), 
	"MODIFIED_BY" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR), 
	"PARENT_FOLDER_ID" VARCHAR2(255 CHAR), 
	"RIGHTS_ID" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_FOLDER_AUD
--------------------------------------------------------

  CREATE TABLE "ICC_FOLDER_AUD" 
   (	"ID" VARCHAR2(255 CHAR), 
	"REV" NUMBER(10,0), 
	"REVTYPE" NUMBER(3,0), 
	"ACTIVE" NUMBER(1,0), 
	"CREATED_BY" VARCHAR2(255 CHAR), 
	"CREATION_DATE" TIMESTAMP (6), 
	"DESCRIPTION" VARCHAR2(2000 CHAR), 
	"MODIFICATION_DATE" TIMESTAMP (6), 
	"MODIFIED_BY" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR), 
	"PARENT_FOLDER_ID" VARCHAR2(255 CHAR), 
	"RIGHTS_ID" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_JOB
--------------------------------------------------------

  CREATE TABLE "ICC_JOB" 
   (	"ID" VARCHAR2(255 CHAR), 
	"PRIORITY" VARCHAR2(255 CHAR), 
	"ACTIVE" NUMBER(1,0), 
	"CREATED_BY" VARCHAR2(255 CHAR), 
	"CREATION_DATE" TIMESTAMP (6), 
	"DESCRIPTION" VARCHAR2(2000 CHAR), 
	"MODIFICATION_DATE" TIMESTAMP (6), 
	"MODIFIED_BY" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR), 
	"FOLDER_ID" VARCHAR2(255 CHAR), 
	"RIGHTS_ID" VARCHAR2(255 CHAR), 
	"TEMPLATE_ID" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_JOB_AUD
--------------------------------------------------------

  CREATE TABLE "ICC_JOB_AUD" 
   (	"ID" VARCHAR2(255 CHAR), 
	"REV" NUMBER(10,0), 
	"REVTYPE" NUMBER(3,0), 
	"PRIORITY" VARCHAR2(255 CHAR), 
	"ACTIVE" NUMBER(1,0), 
	"CREATED_BY" VARCHAR2(255 CHAR), 
	"CREATION_DATE" TIMESTAMP (6), 
	"DESCRIPTION" VARCHAR2(2000 CHAR), 
	"MODIFICATION_DATE" TIMESTAMP (6), 
	"MODIFIED_BY" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR), 
	"FOLDER_ID" VARCHAR2(255 CHAR), 
	"RIGHTS_ID" VARCHAR2(255 CHAR), 
	"TEMPLATE_ID" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_NOTIFICATION
--------------------------------------------------------

  CREATE TABLE "ICC_NOTIFICATION" 
   (	"ID" VARCHAR2(255 CHAR), 
	"NOTIFICATION_DATE" TIMESTAMP (6), 
	"READ" NUMBER(1,0), 
	"READ_DATE" TIMESTAMP (6), 
	"SOURCE" VARCHAR2(255 CHAR), 
	"SOURCE_ID" VARCHAR2(255 CHAR), 
	"SUBJECT" VARCHAR2(2000 CHAR), 
	"TARGET_USER_ID" VARCHAR2(255 CHAR), 
	"TEXT" VARCHAR2(2000 CHAR), 
	"TYPE" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_OPERATION
--------------------------------------------------------

  CREATE TABLE "ICC_OPERATION" 
   (	"ID" VARCHAR2(255 CHAR), 
	"INSTANCE_ID" VARCHAR2(255 CHAR), 
	"MODEL_ID" VARCHAR2(255 CHAR), 
	"MODEL_NAME" VARCHAR2(255 CHAR), 
	"NEXT_RUN_DATE" TIMESTAMP (6), 
	"FIRST_RUN_DATE" TIMESTAMP (6), 
	"INTERVAL" VARCHAR2(255 CHAR), 
	"PERIOD" NUMBER(10,0), 
	"SET_DATE" TIMESTAMP (6), 
	"SKIP" NUMBER(1,0), 
	"SUMMARY" VARCHAR2(255 CHAR), 
	"TRIGGER_ID" VARCHAR2(255 CHAR), 
	"TYPE" VARCHAR2(255 CHAR), 
	"USER_ID" VARCHAR2(255 CHAR), 
	"USER_NAME" VARCHAR2(255 CHAR), 
	"DETAIL_ID" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_OPERATION_DETAIL
--------------------------------------------------------

  CREATE TABLE "ICC_OPERATION_DETAIL" 
   (	"ID" VARCHAR2(255 CHAR), 
	"ERROR_DETAIL" CLOB, 
	"QUERY_DETAIL" CLOB, 
	"RULE_DETAIL" CLOB
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_OPERATION_LOG
--------------------------------------------------------

  CREATE TABLE "ICC_OPERATION_LOG" 
   (	"ID" VARCHAR2(255 CHAR), 
	"OPERATION_DATE" TIMESTAMP (6), 
	"OPERATION_ID" VARCHAR2(255 CHAR), 
	"STATUS" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_OPERATION_RESULT
--------------------------------------------------------

  CREATE TABLE "ICC_OPERATION_RESULT" 
   (	"ID" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR), 
	"TYPE" VARCHAR2(255 CHAR), 
	"VALUE" VARCHAR2(255 CHAR), 
	"DETAIL_ID" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_PARAMETER_GLOBAL
--------------------------------------------------------

  CREATE TABLE "ICC_PARAMETER_GLOBAL" 
   (	"ID" VARCHAR2(255 CHAR), 
	"ACTIVE" NUMBER(1,0), 
	"CREATED_BY" VARCHAR2(255 CHAR), 
	"CREATION_DATE" TIMESTAMP (6), 
	"DESCRIPTION" VARCHAR2(2000 CHAR), 
	"MODIFICATION_DATE" TIMESTAMP (6), 
	"MODIFIED_BY" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR), 
	"VALUE" VARCHAR2(2000 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_PARAMETER_GLOBAL_AUD
--------------------------------------------------------

  CREATE TABLE "ICC_PARAMETER_GLOBAL_AUD" 
   (	"ID" VARCHAR2(255 CHAR), 
	"REV" NUMBER(10,0), 
	"REVTYPE" NUMBER(3,0), 
	"ACTIVE" NUMBER(1,0), 
	"CREATED_BY" VARCHAR2(255 CHAR), 
	"CREATION_DATE" TIMESTAMP (6), 
	"DESCRIPTION" VARCHAR2(2000 CHAR), 
	"MODIFICATION_DATE" TIMESTAMP (6), 
	"MODIFIED_BY" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR), 
	"VALUE" VARCHAR2(2000 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_PARAMETER_LOCAL
--------------------------------------------------------

  CREATE TABLE "ICC_PARAMETER_LOCAL" 
   (	"ID" VARCHAR2(255 CHAR), 
	"ACTIVE" NUMBER(1,0), 
	"CREATED_BY" VARCHAR2(255 CHAR), 
	"CREATION_DATE" TIMESTAMP (6), 
	"DESCRIPTION" VARCHAR2(2000 CHAR), 
	"MODIFICATION_DATE" TIMESTAMP (6), 
	"MODIFIED_BY" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR), 
	"VALUE" VARCHAR2(2000 CHAR), 
	"FOLDER_ID" VARCHAR2(255 CHAR), 
	"RIGHTS_ID" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_PARAMETER_LOCAL_AUD
--------------------------------------------------------

  CREATE TABLE "ICC_PARAMETER_LOCAL_AUD" 
   (	"ID" VARCHAR2(255 CHAR), 
	"REV" NUMBER(10,0), 
	"REVTYPE" NUMBER(3,0), 
	"ACTIVE" NUMBER(1,0), 
	"CREATED_BY" VARCHAR2(255 CHAR), 
	"CREATION_DATE" TIMESTAMP (6), 
	"DESCRIPTION" VARCHAR2(2000 CHAR), 
	"MODIFICATION_DATE" TIMESTAMP (6), 
	"MODIFIED_BY" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR), 
	"VALUE" VARCHAR2(2000 CHAR), 
	"FOLDER_ID" VARCHAR2(255 CHAR), 
	"RIGHTS_ID" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_RIGHTS
--------------------------------------------------------

  CREATE TABLE "ICC_RIGHTS" 
   (	"ID" VARCHAR2(255 CHAR), 
	"INHERIT" NUMBER(1,0), 
	"OWNER_ID" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_RIGHTS_AUD
--------------------------------------------------------

  CREATE TABLE "ICC_RIGHTS_AUD" 
   (	"ID" VARCHAR2(255 CHAR), 
	"REV" NUMBER(10,0), 
	"REVTYPE" NUMBER(3,0), 
	"INHERIT" NUMBER(1,0), 
	"OWNER_ID" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_RIGHTS_ROLE
--------------------------------------------------------

  CREATE TABLE "ICC_RIGHTS_ROLE" 
   (	"RIGHTS_ID" VARCHAR2(255 CHAR), 
	"RIGHTS" NUMBER(3,0), 
	"ROLE_ID" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_RIGHTS_ROLE_AUD
--------------------------------------------------------

  CREATE TABLE "ICC_RIGHTS_ROLE_AUD" 
   (	"REV" NUMBER(10,0), 
	"RIGHTS_ID" VARCHAR2(255 CHAR), 
	"RIGHTS" NUMBER(3,0), 
	"ROLE_ID" VARCHAR2(255 CHAR), 
	"REVTYPE" NUMBER(3,0)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_RIGHTS_USER
--------------------------------------------------------

  CREATE TABLE "ICC_RIGHTS_USER" 
   (	"RIGHTS_ID" VARCHAR2(255 CHAR), 
	"RIGHTS" NUMBER(3,0), 
	"USER_ID" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_RIGHTS_USER_AUD
--------------------------------------------------------

  CREATE TABLE "ICC_RIGHTS_USER_AUD" 
   (	"REV" NUMBER(10,0), 
	"RIGHTS_ID" VARCHAR2(255 CHAR), 
	"RIGHTS" NUMBER(3,0), 
	"USER_ID" VARCHAR2(255 CHAR), 
	"REVTYPE" NUMBER(3,0)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_ROLE
--------------------------------------------------------

  CREATE TABLE "ICC_ROLE" 
   (	"ID" VARCHAR2(255 CHAR), 
	"ACTIVE" NUMBER(1,0), 
	"CREATED_BY" VARCHAR2(255 CHAR), 
	"CREATION_DATE" TIMESTAMP (6), 
	"DESCRIPTION" VARCHAR2(2000 CHAR), 
	"MODIFICATION_DATE" TIMESTAMP (6), 
	"MODIFIED_BY" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_ROLEUSER
--------------------------------------------------------

  CREATE TABLE "ICC_ROLEUSER" 
   (	"ROLE_ID" VARCHAR2(255 CHAR), 
	"USER_ID" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_ROLEUSER_AUD
--------------------------------------------------------

  CREATE TABLE "ICC_ROLEUSER_AUD" 
   (	"REV" NUMBER(10,0), 
	"ROLE_ID" VARCHAR2(255 CHAR), 
	"USER_ID" VARCHAR2(255 CHAR), 
	"REVTYPE" NUMBER(3,0)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_ROLE_AUD
--------------------------------------------------------

  CREATE TABLE "ICC_ROLE_AUD" 
   (	"ID" VARCHAR2(255 CHAR), 
	"REV" NUMBER(10,0), 
	"REVTYPE" NUMBER(3,0), 
	"ACTIVE" NUMBER(1,0), 
	"CREATED_BY" VARCHAR2(255 CHAR), 
	"CREATION_DATE" TIMESTAMP (6), 
	"DESCRIPTION" VARCHAR2(2000 CHAR), 
	"MODIFICATION_DATE" TIMESTAMP (6), 
	"MODIFIED_BY" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_RULE
--------------------------------------------------------

  CREATE TABLE "ICC_RULE" 
   (	"ID" VARCHAR2(255 CHAR), 
	"DETAIL" CLOB, 
	"IMAGE" CLOB, 
	"ACTIVE" NUMBER(1,0), 
	"CREATED_BY" VARCHAR2(255 CHAR), 
	"CREATION_DATE" TIMESTAMP (6), 
	"DESCRIPTION" VARCHAR2(2000 CHAR), 
	"MODIFICATION_DATE" TIMESTAMP (6), 
	"MODIFIED_BY" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR), 
	"FOLDER_ID" VARCHAR2(255 CHAR), 
	"RIGHTS_ID" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_RULE_AUD
--------------------------------------------------------

  CREATE TABLE "ICC_RULE_AUD" 
   (	"ID" VARCHAR2(255 CHAR), 
	"REV" NUMBER(10,0), 
	"REVTYPE" NUMBER(3,0), 
	"DETAIL" CLOB, 
	"IMAGE" CLOB, 
	"ACTIVE" NUMBER(1,0), 
	"CREATED_BY" VARCHAR2(255 CHAR), 
	"CREATION_DATE" TIMESTAMP (6), 
	"DESCRIPTION" VARCHAR2(2000 CHAR), 
	"MODIFICATION_DATE" TIMESTAMP (6), 
	"MODIFIED_BY" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR), 
	"FOLDER_ID" VARCHAR2(255 CHAR), 
	"RIGHTS_ID" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_TEMPLATE
--------------------------------------------------------

  CREATE TABLE "ICC_TEMPLATE" 
   (	"ID" VARCHAR2(255 CHAR), 
	"ACTIVE" NUMBER(1,0), 
	"DESCRIPTION" VARCHAR2(2000 CHAR), 
	"HAS_DATASET" NUMBER(1,0), 
	"NAME" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_TEMPLATE_AUD
--------------------------------------------------------

  CREATE TABLE "ICC_TEMPLATE_AUD" 
   (	"ID" VARCHAR2(255 CHAR), 
	"REV" NUMBER(10,0), 
	"REVTYPE" NUMBER(3,0), 
	"ACTIVE" NUMBER(1,0), 
	"DESCRIPTION" VARCHAR2(2000 CHAR), 
	"HAS_DATASET" NUMBER(1,0), 
	"NAME" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_USER
--------------------------------------------------------

  CREATE TABLE "ICC_USER" 
   (	"ID" VARCHAR2(255 CHAR), 
	"EMAIL" VARCHAR2(255 CHAR), 
	"FIRST_NAME" VARCHAR2(255 CHAR), 
	"LAST_NAME" VARCHAR2(255 CHAR), 
	"LOGIN_TYPE" VARCHAR2(255 CHAR), 
	"PASSWORD" VARCHAR2(255 CHAR), 
	"ACTIVE" NUMBER(1,0), 
	"CREATED_BY" VARCHAR2(255 CHAR), 
	"CREATION_DATE" TIMESTAMP (6), 
	"DESCRIPTION" VARCHAR2(2000 CHAR), 
	"MODIFICATION_DATE" TIMESTAMP (6), 
	"MODIFIED_BY" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR), 
	"USER_TYPE" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_USER_AUD
--------------------------------------------------------

  CREATE TABLE "ICC_USER_AUD" 
   (	"ID" VARCHAR2(255 CHAR), 
	"REV" NUMBER(10,0), 
	"REVTYPE" NUMBER(3,0), 
	"EMAIL" VARCHAR2(255 CHAR), 
	"FIRST_NAME" VARCHAR2(255 CHAR), 
	"LAST_NAME" VARCHAR2(255 CHAR), 
	"LOGIN_TYPE" VARCHAR2(255 CHAR), 
	"PASSWORD" VARCHAR2(255 CHAR), 
	"ACTIVE" NUMBER(1,0), 
	"CREATED_BY" VARCHAR2(255 CHAR), 
	"CREATION_DATE" TIMESTAMP (6), 
	"DESCRIPTION" VARCHAR2(2000 CHAR), 
	"MODIFICATION_DATE" TIMESTAMP (6), 
	"MODIFIED_BY" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR), 
	"USER_TYPE" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_VARIABLE
--------------------------------------------------------

  CREATE TABLE "ICC_VARIABLE" 
   (	"ID" VARCHAR2(255 CHAR), 
	"VALUE" CLOB, 
	"DEFINITION_ID" VARCHAR2(255 CHAR), 
	"JOB_ID" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_VARIABLEDEF
--------------------------------------------------------

  CREATE TABLE "ICC_VARIABLEDEF" 
   (	"ID" VARCHAR2(255 CHAR), 
	"DEFAULT_VALUE" VARCHAR2(255 CHAR), 
	"POSITION" NUMBER(10,0), 
	"ACTIVE" NUMBER(1,0), 
	"CREATED_BY" VARCHAR2(255 CHAR), 
	"CREATION_DATE" TIMESTAMP (6), 
	"DESCRIPTION" VARCHAR2(2000 CHAR), 
	"MODIFICATION_DATE" TIMESTAMP (6), 
	"MODIFIED_BY" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR), 
	"REQUIRED" NUMBER(1,0), 
	"TYPE" VARCHAR2(255 CHAR), 
	"TEMPLATE_ID" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_VARIABLEDEF_AUD
--------------------------------------------------------

  CREATE TABLE "ICC_VARIABLEDEF_AUD" 
   (	"ID" VARCHAR2(255 CHAR), 
	"REV" NUMBER(10,0), 
	"REVTYPE" NUMBER(3,0), 
	"DEFAULT_VALUE" VARCHAR2(255 CHAR), 
	"POSITION" NUMBER(10,0), 
	"ACTIVE" NUMBER(1,0), 
	"CREATED_BY" VARCHAR2(255 CHAR), 
	"CREATION_DATE" TIMESTAMP (6), 
	"DESCRIPTION" VARCHAR2(2000 CHAR), 
	"MODIFICATION_DATE" TIMESTAMP (6), 
	"MODIFIED_BY" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR), 
	"REQUIRED" NUMBER(1,0), 
	"TYPE" VARCHAR2(255 CHAR), 
	"TEMPLATE_ID" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table ICC_VARIABLE_AUD
--------------------------------------------------------

  CREATE TABLE "ICC_VARIABLE_AUD" 
   (	"ID" VARCHAR2(255 CHAR), 
	"REV" NUMBER(10,0), 
	"REVTYPE" NUMBER(3,0), 
	"VALUE" CLOB, 
	"DEFINITION_ID" VARCHAR2(255 CHAR), 
	"JOB_ID" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Table USER_REVISION_ENTITY
--------------------------------------------------------

  CREATE TABLE "USER_REVISION_ENTITY" 
   (	"ID" NUMBER(10,0), 
	"TIMESTAMP" NUMBER(19,0), 
	"USER_ID" VARCHAR2(255 CHAR), 
	"USER_NAME" VARCHAR2(255 CHAR)
   ) ;
/
--------------------------------------------------------
--  DDL for Index UK_AT8L1O8883TP6OEB6QULXTO4B
--------------------------------------------------------

  CREATE UNIQUE INDEX "UK_AT8L1O8883TP6OEB6QULXTO4B" ON "ICC_PARAMETER_GLOBAL" ("NAME") 
  ;
/
--------------------------------------------------------
--  DDL for Index UK_BV8D7EVFV9N3ASSQ36KMC1TTN
--------------------------------------------------------

  CREATE UNIQUE INDEX "UK_BV8D7EVFV9N3ASSQ36KMC1TTN" ON "ICC_CONNECTION" ("NAME") 
  ;
/
--------------------------------------------------------
--  DDL for Index UK_4LYIUF73F46RS69H9MXY2JR
--------------------------------------------------------

  CREATE UNIQUE INDEX "UK_4LYIUF73F46RS69H9MXY2JR" ON "ICC_ROLE" ("NAME") 
  ;
/
--------------------------------------------------------
--  DDL for Index UK_42LH3SWH1D5GYLRHIUYB2XNMC
--------------------------------------------------------

  CREATE UNIQUE INDEX "UK_42LH3SWH1D5GYLRHIUYB2XNMC" ON "ICC_USER" ("NAME") 
  ;
/
--------------------------------------------------------
--  Constraints for Table ICC_RIGHTS_ROLE
--------------------------------------------------------

  ALTER TABLE "ICC_RIGHTS_ROLE" ADD PRIMARY KEY ("RIGHTS_ID", "ROLE_ID") ENABLE;
  ALTER TABLE "ICC_RIGHTS_ROLE" MODIFY ("ROLE_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_RIGHTS_ROLE" MODIFY ("RIGHTS_ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_RIGHTS_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_RIGHTS_AUD" ADD PRIMARY KEY ("ID", "REV") ENABLE;
  ALTER TABLE "ICC_RIGHTS_AUD" MODIFY ("REV" NOT NULL ENABLE);
  ALTER TABLE "ICC_RIGHTS_AUD" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_VARIABLE_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_VARIABLE_AUD" ADD PRIMARY KEY ("ID", "REV") ENABLE;
  ALTER TABLE "ICC_VARIABLE_AUD" MODIFY ("REV" NOT NULL ENABLE);
  ALTER TABLE "ICC_VARIABLE_AUD" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_OPERATION_DETAIL
--------------------------------------------------------

  ALTER TABLE "ICC_OPERATION_DETAIL" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "ICC_OPERATION_DETAIL" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_PARAMETER_LOCAL
--------------------------------------------------------

  ALTER TABLE "ICC_PARAMETER_LOCAL" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "ICC_PARAMETER_LOCAL" MODIFY ("RIGHTS_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_PARAMETER_LOCAL" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "ICC_PARAMETER_LOCAL" MODIFY ("MODIFICATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_PARAMETER_LOCAL" MODIFY ("CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_PARAMETER_LOCAL" MODIFY ("ACTIVE" NOT NULL ENABLE);
  ALTER TABLE "ICC_PARAMETER_LOCAL" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_ROLEUSER
--------------------------------------------------------

  ALTER TABLE "ICC_ROLEUSER" ADD PRIMARY KEY ("ROLE_ID", "USER_ID") ENABLE;
  ALTER TABLE "ICC_ROLEUSER" MODIFY ("USER_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_ROLEUSER" MODIFY ("ROLE_ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_OPERATION
--------------------------------------------------------

  ALTER TABLE "ICC_OPERATION" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "ICC_OPERATION" MODIFY ("USER_NAME" NOT NULL ENABLE);
  ALTER TABLE "ICC_OPERATION" MODIFY ("USER_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_OPERATION" MODIFY ("TYPE" NOT NULL ENABLE);
  ALTER TABLE "ICC_OPERATION" MODIFY ("TRIGGER_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_OPERATION" MODIFY ("SKIP" NOT NULL ENABLE);
  ALTER TABLE "ICC_OPERATION" MODIFY ("SET_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_OPERATION" MODIFY ("FIRST_RUN_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_OPERATION" MODIFY ("NEXT_RUN_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_OPERATION" MODIFY ("MODEL_NAME" NOT NULL ENABLE);
  ALTER TABLE "ICC_OPERATION" MODIFY ("MODEL_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_OPERATION" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_PARAMETER_LOCAL_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_PARAMETER_LOCAL_AUD" ADD PRIMARY KEY ("ID", "REV") ENABLE;
  ALTER TABLE "ICC_PARAMETER_LOCAL_AUD" MODIFY ("REV" NOT NULL ENABLE);
  ALTER TABLE "ICC_PARAMETER_LOCAL_AUD" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_RULE
--------------------------------------------------------

  ALTER TABLE "ICC_RULE" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "ICC_RULE" MODIFY ("RIGHTS_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_RULE" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "ICC_RULE" MODIFY ("MODIFICATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_RULE" MODIFY ("CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_RULE" MODIFY ("ACTIVE" NOT NULL ENABLE);
  ALTER TABLE "ICC_RULE" MODIFY ("DETAIL" NOT NULL ENABLE);
  ALTER TABLE "ICC_RULE" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_RIGHTS
--------------------------------------------------------

  ALTER TABLE "ICC_RIGHTS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "ICC_RIGHTS" MODIFY ("OWNER_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_RIGHTS" MODIFY ("INHERIT" NOT NULL ENABLE);
  ALTER TABLE "ICC_RIGHTS" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_RIGHTS_ROLE_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_RIGHTS_ROLE_AUD" ADD PRIMARY KEY ("REV", "RIGHTS_ID", "RIGHTS", "ROLE_ID") ENABLE;
  ALTER TABLE "ICC_RIGHTS_ROLE_AUD" MODIFY ("ROLE_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_RIGHTS_ROLE_AUD" MODIFY ("RIGHTS" NOT NULL ENABLE);
  ALTER TABLE "ICC_RIGHTS_ROLE_AUD" MODIFY ("RIGHTS_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_RIGHTS_ROLE_AUD" MODIFY ("REV" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_ROLE
--------------------------------------------------------

  ALTER TABLE "ICC_ROLE" ADD CONSTRAINT "UK_4LYIUF73F46RS69H9MXY2JR" UNIQUE ("NAME") ENABLE;
  ALTER TABLE "ICC_ROLE" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "ICC_ROLE" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "ICC_ROLE" MODIFY ("MODIFICATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_ROLE" MODIFY ("CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_ROLE" MODIFY ("ACTIVE" NOT NULL ENABLE);
  ALTER TABLE "ICC_ROLE" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_OPERATION_LOG
--------------------------------------------------------

  ALTER TABLE "ICC_OPERATION_LOG" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "ICC_OPERATION_LOG" MODIFY ("STATUS" NOT NULL ENABLE);
  ALTER TABLE "ICC_OPERATION_LOG" MODIFY ("OPERATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_OPERATION_LOG" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_OPERATION_RESULT
--------------------------------------------------------

  ALTER TABLE "ICC_OPERATION_RESULT" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "ICC_OPERATION_RESULT" MODIFY ("DETAIL_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_OPERATION_RESULT" MODIFY ("TYPE" NOT NULL ENABLE);
  ALTER TABLE "ICC_OPERATION_RESULT" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "ICC_OPERATION_RESULT" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_FOLDER_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_FOLDER_AUD" ADD PRIMARY KEY ("ID", "REV") ENABLE;
  ALTER TABLE "ICC_FOLDER_AUD" MODIFY ("REV" NOT NULL ENABLE);
  ALTER TABLE "ICC_FOLDER_AUD" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_RIGHTS_USER
--------------------------------------------------------

  ALTER TABLE "ICC_RIGHTS_USER" ADD PRIMARY KEY ("RIGHTS_ID", "USER_ID") ENABLE;
  ALTER TABLE "ICC_RIGHTS_USER" MODIFY ("USER_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_RIGHTS_USER" MODIFY ("RIGHTS_ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_PARAMETER_GLOBAL
--------------------------------------------------------

  ALTER TABLE "ICC_PARAMETER_GLOBAL" ADD CONSTRAINT "UK_AT8L1O8883TP6OEB6QULXTO4B" UNIQUE ("NAME") ENABLE;
  ALTER TABLE "ICC_PARAMETER_GLOBAL" MODIFY ("ACTIVE" NOT NULL ENABLE);
  ALTER TABLE "ICC_PARAMETER_GLOBAL" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_PARAMETER_GLOBAL" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "ICC_PARAMETER_GLOBAL" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "ICC_PARAMETER_GLOBAL" MODIFY ("MODIFICATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_PARAMETER_GLOBAL" MODIFY ("CREATION_DATE" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_USER_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_USER_AUD" ADD PRIMARY KEY ("ID", "REV") ENABLE;
  ALTER TABLE "ICC_USER_AUD" MODIFY ("REV" NOT NULL ENABLE);
  ALTER TABLE "ICC_USER_AUD" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_CONNECTION
--------------------------------------------------------

  ALTER TABLE "ICC_CONNECTION" ADD CONSTRAINT "UK_BV8D7EVFV9N3ASSQ36KMC1TTN" UNIQUE ("NAME") ENABLE;
  ALTER TABLE "ICC_CONNECTION" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "ICC_CONNECTION" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "ICC_CONNECTION" MODIFY ("MODIFICATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_CONNECTION" MODIFY ("CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_CONNECTION" MODIFY ("ACTIVE" NOT NULL ENABLE);
  ALTER TABLE "ICC_CONNECTION" MODIFY ("USERNAME" NOT NULL ENABLE);
  ALTER TABLE "ICC_CONNECTION" MODIFY ("URL" NOT NULL ENABLE);
  ALTER TABLE "ICC_CONNECTION" MODIFY ("PASSWORD" NOT NULL ENABLE);
  ALTER TABLE "ICC_CONNECTION" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_JOB
--------------------------------------------------------

  ALTER TABLE "ICC_JOB" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "ICC_JOB" MODIFY ("TEMPLATE_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_JOB" MODIFY ("RIGHTS_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_JOB" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "ICC_JOB" MODIFY ("MODIFICATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_JOB" MODIFY ("CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_JOB" MODIFY ("ACTIVE" NOT NULL ENABLE);
  ALTER TABLE "ICC_JOB" MODIFY ("PRIORITY" NOT NULL ENABLE);
  ALTER TABLE "ICC_JOB" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_CONNECTION_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_CONNECTION_AUD" ADD PRIMARY KEY ("ID", "REV") ENABLE;
  ALTER TABLE "ICC_CONNECTION_AUD" MODIFY ("REV" NOT NULL ENABLE);
  ALTER TABLE "ICC_CONNECTION_AUD" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_ROLE_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_ROLE_AUD" ADD PRIMARY KEY ("ID", "REV") ENABLE;
  ALTER TABLE "ICC_ROLE_AUD" MODIFY ("REV" NOT NULL ENABLE);
  ALTER TABLE "ICC_ROLE_AUD" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_ROLEUSER_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_ROLEUSER_AUD" ADD PRIMARY KEY ("REV", "ROLE_ID", "USER_ID") ENABLE;
  ALTER TABLE "ICC_ROLEUSER_AUD" MODIFY ("USER_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_ROLEUSER_AUD" MODIFY ("ROLE_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_ROLEUSER_AUD" MODIFY ("REV" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_TEMPLATE_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_TEMPLATE_AUD" ADD PRIMARY KEY ("ID", "REV") ENABLE;
  ALTER TABLE "ICC_TEMPLATE_AUD" MODIFY ("REV" NOT NULL ENABLE);
  ALTER TABLE "ICC_TEMPLATE_AUD" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_COMMAND_LOG
--------------------------------------------------------

  ALTER TABLE "ICC_COMMAND_LOG" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "ICC_COMMAND_LOG" MODIFY ("USER_NAME" NOT NULL ENABLE);
  ALTER TABLE "ICC_COMMAND_LOG" MODIFY ("USER_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_COMMAND_LOG" MODIFY ("COMMAND_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_COMMAND_LOG" MODIFY ("COMMAND" NOT NULL ENABLE);
  ALTER TABLE "ICC_COMMAND_LOG" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_VARIABLEDEF_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_VARIABLEDEF_AUD" ADD PRIMARY KEY ("ID", "REV") ENABLE;
  ALTER TABLE "ICC_VARIABLEDEF_AUD" MODIFY ("REV" NOT NULL ENABLE);
  ALTER TABLE "ICC_VARIABLEDEF_AUD" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_NOTIFICATION
--------------------------------------------------------

  ALTER TABLE "ICC_NOTIFICATION" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "ICC_NOTIFICATION" MODIFY ("TYPE" NOT NULL ENABLE);
  ALTER TABLE "ICC_NOTIFICATION" MODIFY ("TEXT" NOT NULL ENABLE);
  ALTER TABLE "ICC_NOTIFICATION" MODIFY ("TARGET_USER_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_NOTIFICATION" MODIFY ("SUBJECT" NOT NULL ENABLE);
  ALTER TABLE "ICC_NOTIFICATION" MODIFY ("SOURCE_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_NOTIFICATION" MODIFY ("SOURCE" NOT NULL ENABLE);
  ALTER TABLE "ICC_NOTIFICATION" MODIFY ("READ" NOT NULL ENABLE);
  ALTER TABLE "ICC_NOTIFICATION" MODIFY ("NOTIFICATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_NOTIFICATION" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_FOLDER
--------------------------------------------------------

  ALTER TABLE "ICC_FOLDER" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "ICC_FOLDER" MODIFY ("RIGHTS_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_FOLDER" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "ICC_FOLDER" MODIFY ("MODIFICATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_FOLDER" MODIFY ("CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_FOLDER" MODIFY ("ACTIVE" NOT NULL ENABLE);
  ALTER TABLE "ICC_FOLDER" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table USER_REVISION_ENTITY
--------------------------------------------------------

  ALTER TABLE "USER_REVISION_ENTITY" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "USER_REVISION_ENTITY" MODIFY ("TIMESTAMP" NOT NULL ENABLE);
  ALTER TABLE "USER_REVISION_ENTITY" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_JOB_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_JOB_AUD" ADD PRIMARY KEY ("ID", "REV") ENABLE;
  ALTER TABLE "ICC_JOB_AUD" MODIFY ("REV" NOT NULL ENABLE);
  ALTER TABLE "ICC_JOB_AUD" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_VARIABLE
--------------------------------------------------------

  ALTER TABLE "ICC_VARIABLE" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "ICC_VARIABLE" MODIFY ("JOB_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_VARIABLE" MODIFY ("DEFINITION_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_VARIABLE" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_VARIABLEDEF
--------------------------------------------------------

  ALTER TABLE "ICC_VARIABLEDEF" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "ICC_VARIABLEDEF" MODIFY ("TEMPLATE_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_VARIABLEDEF" MODIFY ("TYPE" NOT NULL ENABLE);
  ALTER TABLE "ICC_VARIABLEDEF" MODIFY ("REQUIRED" NOT NULL ENABLE);
  ALTER TABLE "ICC_VARIABLEDEF" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "ICC_VARIABLEDEF" MODIFY ("MODIFICATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_VARIABLEDEF" MODIFY ("CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_VARIABLEDEF" MODIFY ("ACTIVE" NOT NULL ENABLE);
  ALTER TABLE "ICC_VARIABLEDEF" MODIFY ("POSITION" NOT NULL ENABLE);
  ALTER TABLE "ICC_VARIABLEDEF" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_ERROR_LOG
--------------------------------------------------------

  ALTER TABLE "ICC_ERROR_LOG" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "ICC_ERROR_LOG" MODIFY ("ERROR_TYPE" NOT NULL ENABLE);
  ALTER TABLE "ICC_ERROR_LOG" MODIFY ("ERROR_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_ERROR_LOG" MODIFY ("ERROR_CLASS" NOT NULL ENABLE);
  ALTER TABLE "ICC_ERROR_LOG" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_RULE_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_RULE_AUD" ADD PRIMARY KEY ("ID", "REV") ENABLE;
  ALTER TABLE "ICC_RULE_AUD" MODIFY ("REV" NOT NULL ENABLE);
  ALTER TABLE "ICC_RULE_AUD" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_RIGHTS_USER_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_RIGHTS_USER_AUD" ADD PRIMARY KEY ("REV", "RIGHTS_ID", "RIGHTS", "USER_ID") ENABLE;
  ALTER TABLE "ICC_RIGHTS_USER_AUD" MODIFY ("USER_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_RIGHTS_USER_AUD" MODIFY ("RIGHTS" NOT NULL ENABLE);
  ALTER TABLE "ICC_RIGHTS_USER_AUD" MODIFY ("RIGHTS_ID" NOT NULL ENABLE);
  ALTER TABLE "ICC_RIGHTS_USER_AUD" MODIFY ("REV" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_TEMPLATE
--------------------------------------------------------

  ALTER TABLE "ICC_TEMPLATE" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "ICC_TEMPLATE" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "ICC_TEMPLATE" MODIFY ("HAS_DATASET" NOT NULL ENABLE);
  ALTER TABLE "ICC_TEMPLATE" MODIFY ("DESCRIPTION" NOT NULL ENABLE);
  ALTER TABLE "ICC_TEMPLATE" MODIFY ("ACTIVE" NOT NULL ENABLE);
  ALTER TABLE "ICC_TEMPLATE" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_PARAMETER_GLOBAL_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_PARAMETER_GLOBAL_AUD" ADD PRIMARY KEY ("ID", "REV") ENABLE;
  ALTER TABLE "ICC_PARAMETER_GLOBAL_AUD" MODIFY ("REV" NOT NULL ENABLE);
  ALTER TABLE "ICC_PARAMETER_GLOBAL_AUD" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Constraints for Table ICC_USER
--------------------------------------------------------

  ALTER TABLE "ICC_USER" ADD CONSTRAINT "UK_42LH3SWH1D5GYLRHIUYB2XNMC" UNIQUE ("NAME") ENABLE;
  ALTER TABLE "ICC_USER" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "ICC_USER" MODIFY ("USER_TYPE" NOT NULL ENABLE);
  ALTER TABLE "ICC_USER" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "ICC_USER" MODIFY ("MODIFICATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_USER" MODIFY ("CREATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "ICC_USER" MODIFY ("ACTIVE" NOT NULL ENABLE);
  ALTER TABLE "ICC_USER" MODIFY ("LOGIN_TYPE" NOT NULL ENABLE);
  ALTER TABLE "ICC_USER" MODIFY ("ID" NOT NULL ENABLE);
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_CONNECTION_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_CONNECTION_AUD" ADD CONSTRAINT "FK_3X0W4307PR6T61DRVXRJCCIPN" FOREIGN KEY ("REV")
	  REFERENCES "USER_REVISION_ENTITY" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_FOLDER
--------------------------------------------------------

  ALTER TABLE "ICC_FOLDER" ADD CONSTRAINT "FK_AJ654542N1AXB4BS9W7U9LB1V" FOREIGN KEY ("RIGHTS_ID")
	  REFERENCES "ICC_RIGHTS" ("ID") ENABLE;
  ALTER TABLE "ICC_FOLDER" ADD CONSTRAINT "FK_M1HXFYE73QSG0YP6V4S59V151" FOREIGN KEY ("PARENT_FOLDER_ID")
	  REFERENCES "ICC_FOLDER" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_FOLDER_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_FOLDER_AUD" ADD CONSTRAINT "FK_9Y9U70RHF29LE15U8NTRA0JJP" FOREIGN KEY ("REV")
	  REFERENCES "USER_REVISION_ENTITY" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_JOB
--------------------------------------------------------

  ALTER TABLE "ICC_JOB" ADD CONSTRAINT "FK_14H0JJDSR9M37I9QTYAVY0YUL" FOREIGN KEY ("FOLDER_ID")
	  REFERENCES "ICC_FOLDER" ("ID") ENABLE;
  ALTER TABLE "ICC_JOB" ADD CONSTRAINT "FK_1QY90FHWHA24MV9AW9HMTVQ53" FOREIGN KEY ("RIGHTS_ID")
	  REFERENCES "ICC_RIGHTS" ("ID") ENABLE;
  ALTER TABLE "ICC_JOB" ADD CONSTRAINT "FK_94NCTY5H8D7B1IWQPMG5XL9EP" FOREIGN KEY ("TEMPLATE_ID")
	  REFERENCES "ICC_TEMPLATE" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_JOB_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_JOB_AUD" ADD CONSTRAINT "FK_PUJCSOHWCR9JJYCF1PBMVVGDR" FOREIGN KEY ("REV")
	  REFERENCES "USER_REVISION_ENTITY" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_OPERATION
--------------------------------------------------------

  ALTER TABLE "ICC_OPERATION" ADD CONSTRAINT "FK_P5W9P40CATFEV4CGF5L02SY0C" FOREIGN KEY ("DETAIL_ID")
	  REFERENCES "ICC_OPERATION_DETAIL" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_OPERATION_RESULT
--------------------------------------------------------

  ALTER TABLE "ICC_OPERATION_RESULT" ADD CONSTRAINT "FK_91EL9A9YX2MK18RUNDO31LG60" FOREIGN KEY ("DETAIL_ID")
	  REFERENCES "ICC_OPERATION_DETAIL" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_PARAMETER_GLOBAL_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_PARAMETER_GLOBAL_AUD" ADD CONSTRAINT "FK_IG07RSMPET7FTL4W4C93LU8O9" FOREIGN KEY ("REV")
	  REFERENCES "USER_REVISION_ENTITY" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_PARAMETER_LOCAL
--------------------------------------------------------

  ALTER TABLE "ICC_PARAMETER_LOCAL" ADD CONSTRAINT "FK_B84YUFUOS2JX3C2612B328LN8" FOREIGN KEY ("FOLDER_ID")
	  REFERENCES "ICC_FOLDER" ("ID") ENABLE;
  ALTER TABLE "ICC_PARAMETER_LOCAL" ADD CONSTRAINT "FK_F7IA950MRQFM4NSAG9XOB43W2" FOREIGN KEY ("RIGHTS_ID")
	  REFERENCES "ICC_RIGHTS" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_PARAMETER_LOCAL_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_PARAMETER_LOCAL_AUD" ADD CONSTRAINT "FK_S3KYDACLBR5PJWQ7K6LRRO1EJ" FOREIGN KEY ("REV")
	  REFERENCES "USER_REVISION_ENTITY" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_RIGHTS_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_RIGHTS_AUD" ADD CONSTRAINT "FK_8NB40WYER4HYUFVKGK0UWN18U" FOREIGN KEY ("REV")
	  REFERENCES "USER_REVISION_ENTITY" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_RIGHTS_ROLE
--------------------------------------------------------

  ALTER TABLE "ICC_RIGHTS_ROLE" ADD CONSTRAINT "FK_8Q8UQ6RYGX9C5S8I8MMA0BXBR" FOREIGN KEY ("RIGHTS_ID")
	  REFERENCES "ICC_RIGHTS" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_RIGHTS_ROLE_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_RIGHTS_ROLE_AUD" ADD CONSTRAINT "FK_4ETOHXC0LS2AVI4AQH0OUO58X" FOREIGN KEY ("REV")
	  REFERENCES "USER_REVISION_ENTITY" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_RIGHTS_USER
--------------------------------------------------------

  ALTER TABLE "ICC_RIGHTS_USER" ADD CONSTRAINT "FK_9078W1DM0BXQ0KTPTPMJEIE2H" FOREIGN KEY ("RIGHTS_ID")
	  REFERENCES "ICC_RIGHTS" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_RIGHTS_USER_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_RIGHTS_USER_AUD" ADD CONSTRAINT "FK_DM71RT5OH9IGIDQI46GOXMM1S" FOREIGN KEY ("REV")
	  REFERENCES "USER_REVISION_ENTITY" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_ROLEUSER
--------------------------------------------------------

  ALTER TABLE "ICC_ROLEUSER" ADD CONSTRAINT "FK_C8HWBYVSY15D3QCPPU7I8PUC4" FOREIGN KEY ("USER_ID")
	  REFERENCES "ICC_USER" ("ID") ENABLE;
  ALTER TABLE "ICC_ROLEUSER" ADD CONSTRAINT "FK_PQ85YMQKDGQEN4A2JFWER7NE5" FOREIGN KEY ("ROLE_ID")
	  REFERENCES "ICC_ROLE" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_ROLEUSER_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_ROLEUSER_AUD" ADD CONSTRAINT "FK_5PWXKJQ7GM7G9GJ1LJ4V8ULXL" FOREIGN KEY ("REV")
	  REFERENCES "USER_REVISION_ENTITY" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_ROLE_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_ROLE_AUD" ADD CONSTRAINT "FK_R759AFUQOF55O28C0V503BY10" FOREIGN KEY ("REV")
	  REFERENCES "USER_REVISION_ENTITY" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_RULE
--------------------------------------------------------

  ALTER TABLE "ICC_RULE" ADD CONSTRAINT "FK_2B502Q7AYCRSU5KDYNL3473GI" FOREIGN KEY ("FOLDER_ID")
	  REFERENCES "ICC_FOLDER" ("ID") ENABLE;
  ALTER TABLE "ICC_RULE" ADD CONSTRAINT "FK_Q7WCYRJVGIJHB8L1PYK51MAB" FOREIGN KEY ("RIGHTS_ID")
	  REFERENCES "ICC_RIGHTS" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_RULE_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_RULE_AUD" ADD CONSTRAINT "FK_PLYQA87V2JMN8WNEYAY99PCIP" FOREIGN KEY ("REV")
	  REFERENCES "USER_REVISION_ENTITY" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_TEMPLATE_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_TEMPLATE_AUD" ADD CONSTRAINT "FK_AHXIBM295I0DU8I90W0FTD0DU" FOREIGN KEY ("REV")
	  REFERENCES "USER_REVISION_ENTITY" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_USER_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_USER_AUD" ADD CONSTRAINT "FK_T5JJFKG5QSVFVOH52KMCN9UAH" FOREIGN KEY ("REV")
	  REFERENCES "USER_REVISION_ENTITY" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_VARIABLE
--------------------------------------------------------

  ALTER TABLE "ICC_VARIABLE" ADD CONSTRAINT "FK_A8KFI01IFJBF83WQGGMSPQEA1" FOREIGN KEY ("DEFINITION_ID")
	  REFERENCES "ICC_VARIABLEDEF" ("ID") ENABLE;
  ALTER TABLE "ICC_VARIABLE" ADD CONSTRAINT "FK_QJU9GHQ4APDOS0ACYBGSHJJRC" FOREIGN KEY ("JOB_ID")
	  REFERENCES "ICC_JOB" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_VARIABLEDEF
--------------------------------------------------------

  ALTER TABLE "ICC_VARIABLEDEF" ADD CONSTRAINT "FK_EOX8OQD2FX0OT6BUY74NOK2L" FOREIGN KEY ("TEMPLATE_ID")
	  REFERENCES "ICC_TEMPLATE" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_VARIABLEDEF_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_VARIABLEDEF_AUD" ADD CONSTRAINT "FK_RS8V72RRMU2TYFWILG5F3BP7I" FOREIGN KEY ("REV")
	  REFERENCES "USER_REVISION_ENTITY" ("ID") ENABLE;
/
--------------------------------------------------------
--  Ref Constraints for Table ICC_VARIABLE_AUD
--------------------------------------------------------

  ALTER TABLE "ICC_VARIABLE_AUD" ADD CONSTRAINT "FK_HARBYM3X7CCPOTJHDJL755D4E" FOREIGN KEY ("REV")
	  REFERENCES "USER_REVISION_ENTITY" ("ID") ENABLE;
/











--------------------------------------------------------
--  DDL for Sequence HIBERNATE_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "HIBERNATE_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 81 CACHE 20 NOORDER  NOCYCLE ;
/

--------------------------------------------------------
--  QUARTZ
--------------------------------------------------------

CREATE TABLE qrtz_job_details
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    JOB_NAME  VARCHAR2(200) NOT NULL,
    JOB_GROUP VARCHAR2(200) NOT NULL,
    DESCRIPTION VARCHAR2(250) NULL,
    JOB_CLASS_NAME   VARCHAR2(250) NOT NULL, 
    IS_DURABLE VARCHAR2(1) NOT NULL,
    IS_NONCONCURRENT VARCHAR2(1) NOT NULL,
    IS_UPDATE_DATA VARCHAR2(1) NOT NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NOT NULL,
    JOB_DATA BLOB NULL,
    CONSTRAINT QRTZ_JOB_DETAILS_PK PRIMARY KEY (SCHED_NAME,JOB_NAME,JOB_GROUP)
);
CREATE TABLE qrtz_triggers
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    JOB_NAME  VARCHAR2(200) NOT NULL, 
    JOB_GROUP VARCHAR2(200) NOT NULL,
    DESCRIPTION VARCHAR2(250) NULL,
    NEXT_FIRE_TIME NUMBER(13) NULL,
    PREV_FIRE_TIME NUMBER(13) NULL,
    PRIORITY NUMBER(13) NULL,
    TRIGGER_STATE VARCHAR2(16) NOT NULL,
    TRIGGER_TYPE VARCHAR2(8) NOT NULL,
    START_TIME NUMBER(13) NOT NULL,
    END_TIME NUMBER(13) NULL,
    CALENDAR_NAME VARCHAR2(200) NULL,
    MISFIRE_INSTR NUMBER(2) NULL,
    JOB_DATA BLOB NULL,
    CONSTRAINT QRTZ_TRIGGERS_PK PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    CONSTRAINT QRTZ_TRIGGER_TO_JOBS_FK FOREIGN KEY (SCHED_NAME,JOB_NAME,JOB_GROUP) 
      REFERENCES QRTZ_JOB_DETAILS(SCHED_NAME,JOB_NAME,JOB_GROUP) 
);
CREATE TABLE qrtz_simple_triggers
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    REPEAT_COUNT NUMBER(7) NOT NULL,
    REPEAT_INTERVAL NUMBER(12) NOT NULL,
    TIMES_TRIGGERED NUMBER(10) NOT NULL,
    CONSTRAINT QRTZ_SIMPLE_TRIG_PK PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    CONSTRAINT QRTZ_SIMPLE_TRIG_TO_TRIG_FK FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) 
	REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE qrtz_cron_triggers
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    CRON_EXPRESSION VARCHAR2(120) NOT NULL,
    TIME_ZONE_ID VARCHAR2(80),
    CONSTRAINT QRTZ_CRON_TRIG_PK PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    CONSTRAINT QRTZ_CRON_TRIG_TO_TRIG_FK FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) 
      REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE qrtz_simprop_triggers
  (          
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    STR_PROP_1 VARCHAR2(512) NULL,
    STR_PROP_2 VARCHAR2(512) NULL,
    STR_PROP_3 VARCHAR2(512) NULL,
    INT_PROP_1 NUMBER(10) NULL,
    INT_PROP_2 NUMBER(10) NULL,
    LONG_PROP_1 NUMBER(13) NULL,
    LONG_PROP_2 NUMBER(13) NULL,
    DEC_PROP_1 NUMERIC(13,4) NULL,
    DEC_PROP_2 NUMERIC(13,4) NULL,
    BOOL_PROP_1 VARCHAR2(1) NULL,
    BOOL_PROP_2 VARCHAR2(1) NULL,
    CONSTRAINT QRTZ_SIMPROP_TRIG_PK PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    CONSTRAINT QRTZ_SIMPROP_TRIG_TO_TRIG_FK FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) 
      REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE qrtz_blob_triggers
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    BLOB_DATA BLOB NULL,
    CONSTRAINT QRTZ_BLOB_TRIG_PK PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    CONSTRAINT QRTZ_BLOB_TRIG_TO_TRIG_FK FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) 
        REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE qrtz_calendars
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    CALENDAR_NAME  VARCHAR2(200) NOT NULL, 
    CALENDAR BLOB NOT NULL,
    CONSTRAINT QRTZ_CALENDARS_PK PRIMARY KEY (SCHED_NAME,CALENDAR_NAME)
);
CREATE TABLE qrtz_paused_trigger_grps
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_GROUP  VARCHAR2(200) NOT NULL, 
    CONSTRAINT QRTZ_PAUSED_TRIG_GRPS_PK PRIMARY KEY (SCHED_NAME,TRIGGER_GROUP)
);
CREATE TABLE qrtz_fired_triggers 
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    ENTRY_ID VARCHAR2(95) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    INSTANCE_NAME VARCHAR2(200) NOT NULL,
    FIRED_TIME NUMBER(13) NOT NULL,
    SCHED_TIME NUMBER(13) NOT NULL,
    PRIORITY NUMBER(13) NOT NULL,
    STATE VARCHAR2(16) NOT NULL,
    JOB_NAME VARCHAR2(200) NULL,
    JOB_GROUP VARCHAR2(200) NULL,
    IS_NONCONCURRENT VARCHAR2(1) NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NULL,
    CONSTRAINT QRTZ_FIRED_TRIGGER_PK PRIMARY KEY (SCHED_NAME,ENTRY_ID)
);
CREATE TABLE qrtz_scheduler_state 
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    INSTANCE_NAME VARCHAR2(200) NOT NULL,
    LAST_CHECKIN_TIME NUMBER(13) NOT NULL,
    CHECKIN_INTERVAL NUMBER(13) NOT NULL,
    CONSTRAINT QRTZ_SCHEDULER_STATE_PK PRIMARY KEY (SCHED_NAME,INSTANCE_NAME)
);
CREATE TABLE qrtz_locks
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    LOCK_NAME  VARCHAR2(40) NOT NULL, 
    CONSTRAINT QRTZ_LOCKS_PK PRIMARY KEY (SCHED_NAME,LOCK_NAME)
);

create index idx_qrtz_j_req_recovery on qrtz_job_details(SCHED_NAME,REQUESTS_RECOVERY);
create index idx_qrtz_j_grp on qrtz_job_details(SCHED_NAME,JOB_GROUP);

create index idx_qrtz_t_j on qrtz_triggers(SCHED_NAME,JOB_NAME,JOB_GROUP);
create index idx_qrtz_t_jg on qrtz_triggers(SCHED_NAME,JOB_GROUP);
create index idx_qrtz_t_c on qrtz_triggers(SCHED_NAME,CALENDAR_NAME);
create index idx_qrtz_t_g on qrtz_triggers(SCHED_NAME,TRIGGER_GROUP);
create index idx_qrtz_t_state on qrtz_triggers(SCHED_NAME,TRIGGER_STATE);
create index idx_qrtz_t_n_state on qrtz_triggers(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP,TRIGGER_STATE);
create index idx_qrtz_t_n_g_state on qrtz_triggers(SCHED_NAME,TRIGGER_GROUP,TRIGGER_STATE);
create index idx_qrtz_t_next_fire_time on qrtz_triggers(SCHED_NAME,NEXT_FIRE_TIME);
create index idx_qrtz_t_nft_st on qrtz_triggers(SCHED_NAME,TRIGGER_STATE,NEXT_FIRE_TIME);
create index idx_qrtz_t_nft_misfire on qrtz_triggers(SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME);
create index idx_qrtz_t_nft_st_misfire on qrtz_triggers(SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME,TRIGGER_STATE);
create index idx_qrtz_t_nft_st_misfire_grp on qrtz_triggers(SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME,TRIGGER_GROUP,TRIGGER_STATE);

create index idx_qrtz_ft_trig_inst_name on qrtz_fired_triggers(SCHED_NAME,INSTANCE_NAME);
create index idx_qrtz_ft_inst_job_req_rcvry on qrtz_fired_triggers(SCHED_NAME,INSTANCE_NAME,REQUESTS_RECOVERY);
create index idx_qrtz_ft_j_g on qrtz_fired_triggers(SCHED_NAME,JOB_NAME,JOB_GROUP);
create index idx_qrtz_ft_jg on qrtz_fired_triggers(SCHED_NAME,JOB_GROUP);
create index idx_qrtz_ft_t_g on qrtz_fired_triggers(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP);
create index idx_qrtz_ft_tg on qrtz_fired_triggers(SCHED_NAME,TRIGGER_GROUP);

-- admin insert
INSERT INTO ICC_USER (ID,EMAIL,FIRST_NAME,LAST_NAME,LOGIN_TYPE,PASSWORD,ACTIVE,CREATION_DATE,DESCRIPTION,MODIFICATION_DATE,NAME,USER_TYPE,CREATED_BY,MODIFIED_BY) VALUES ('184431757886694','admin@intellica.net','admin','admin','Default','5kVTCiXRHXyyLVE/riWTlg==',1,TO_TIMESTAMP('11-JAN-16 02.43.47.649000000 PM','DD-MON-RR HH.MI.SS.FF AM'),NULL,TO_TIMESTAMP('11-JAN-16 02.43.47.649000000 PM','DD-MON-RR HH.MI.SS.FF AM'),'admin','Admin',NULL,NULL);
COMMIT;
